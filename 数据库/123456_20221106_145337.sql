-- MySQL dump 10.13  Distrib 5.6.51, for Win64 (x86_64)
--
-- Host: localhost    Database: 123456
-- ------------------------------------------------------
-- Server version	5.6.51-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `track_id` (`track_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,2,1,'uploaded_track',1559831969),(2,2,1,'commented_track',1559963777),(3,2,2,'uploaded_track',1559966648),(4,2,2,'shared_track',1559966785),(5,1,1,'liked_track',1560903131),(6,2,3,'uploaded_track',1560905011),(7,2,4,'uploaded_track',1560905356),(8,2,3,'commented_track',1560906446),(9,2,5,'uploaded_track',1560908525),(10,2,6,'uploaded_track',1560909321);
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` varchar(16) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(200) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `registered` varchar(15) NOT NULL DEFAULT '00/0000',
  `price` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `album_id` (`album_id`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`),
  KEY `price` (`price`),
  KEY `time` (`time`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcement`
--

DROP TABLE IF EXISTS `announcement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `time` int(32) NOT NULL DEFAULT '0',
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcement`
--

LOCK TABLES `announcement` WRITE;
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcement_views`
--

DROP TABLE IF EXISTS `announcement_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `announcement_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `announcement_id` (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcement_views`
--

LOCK TABLES `announcement_views` WRITE;
/*!40000 ALTER TABLE `announcement_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artist_requests`
--

DROP TABLE IF EXISTS `artist_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artist_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(100) NOT NULL DEFAULT '',
  `details` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(150) NOT NULL DEFAULT '',
  `passport` varchar(150) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artist_requests`
--

LOCK TABLES `artist_requests` WRITE;
/*!40000 ALTER TABLE `artist_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `artist_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banned_ip`
--

DROP TABLE IF EXISTS `banned_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_ip`
--

LOCK TABLES `banned_ip` WRITE;
/*!40000 ALTER TABLE `banned_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `banned_ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `blocked_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `blocked_id` (`blocked_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cateogry_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tracks` int(11) NOT NULL DEFAULT '0',
  `color` varchar(20) NOT NULL DEFAULT '#333',
  `background_thumb` varchar(120) NOT NULL DEFAULT '',
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cateogry_name` (`cateogry_name`),
  KEY `tracks` (`tracks`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Other',0,'#000000','upload/photos/2019/04/FaS2oOegTOBm5OpFJiCK_17_6ad5d4edf1fb542961a2a64a8d0768e7_image.jpg',0),(9,'喊唛现场',0,'#000000','upload/photos/2019/06/927oH9mkj4RRuoGd1U3G_08_47bbf21d7cec2aa1f2a80522bd4898e5_image.jpg',1559965380),(10,'中文Class',0,'#000000','upload/photos/2019/06/lGadYKJ6keyHErlUtEpx_08_c075ecb6fbb89dbeac076250d7ca9686_image.jpg',1559965427),(11,'慢摇串烧',0,'#000000','upload/photos/2019/06/1DivfdBA6pmgDZzadWxu_08_4d4a3b0b131c2124e35fbd1fed028047_image.jpg',1559965453),(12,'酒吧串烧',0,'#000000','upload/photos/2019/06/NMhSFHTQPBasb1DUgHcy_08_45c2e0b11f4a9436f38e64aae4b35bd6_image.jpg',1559965467),(13,'慢歌连版',0,'#000000','upload/photos/2019/06/6a8C7ETEyUjGlRps3ETk_08_076abf97137fceefd4609e794ffce730_image.jpg',1559965483),(14,'外文CLUB',0,'#000000','upload/photos/2019/06/hRQZGzXN2WDcKFx1TVPe_08_e16e6893c92f3c648edd633191dee7f1_image.jpg',1559965501),(15,'开场Opening',0,'#000000','upload/photos/2019/06/c5Y1okzCWNsGCvg5TYwi_08_bc031e44a6b4bbd666cefc5a7f46b3e6_image.jpg',1559965517),(16,'电音HOUSE',0,'#000000','upload/photos/2019/06/EEb1V4EDviifceGkqpMw_08_67479f89705ea217079895c1c3a2cf9b_image.jpg',1559965533),(17,'酒吧潮歌',0,'#000000','upload/photos/2019/06/EEPf3VnxyUD19gO6QX2W_08_804be4eb9030a655bf868b0eabcab0b3_image.jpg',1559965551),(18,'暖场/蓝调',0,'#000000','upload/photos/2019/06/yp5i3pFbLiWV2Ju7Bzj3_08_e680f4f6b88b7e0ce5884c2f7d139be7_image.jpg',1559965567),(19,'HipHop/Rnb',0,'#000000','upload/photos/2019/06/OhBx9cZv3tK5NqJuoQBc_08_be851d0d469688e06af7968cf5edb62d_image.jpg',1559965583),(20,'外文DISCO',0,'#000000','upload/photos/2019/06/QZNewVW8CyCE1z4Hf93M_08_333bb9f75860ec42b427f4d9f4e80fd2_image.jpg',1559965604),(21,'交谊舞曲',0,'#000000','upload/photos/2019/06/8okKtrHs5U1txVgn9zMx_08_73b5c0e0a55061e36a5c23ca90d79428_image.jpg',1559965642),(22,'其他',0,'#000000','upload/photos/2019/06/2I3m29MlWpYUYuzu6Eol_08_29c462f0d3c106584e9791cf2c7c981f_image.jpg',1559965749);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `songseconds` float NOT NULL DEFAULT '0',
  `songpercentage` float NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,1,2,'666666666666666666666',223.144,0.785061,1559963777),(2,3,2,'??',0,0,1560906446);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `value` (`value`(255))
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'theme','default'),(2,'censored_words',''),(3,'title','游牛音乐'),(4,'name','游牛音乐'),(5,'keyword','deepsound,video sharing'),(6,'email','40299055@qq.com'),(7,'description','DeepSound is a PHP Audio Sharing Script, DeepSound is the best way to start your own audiosharing script!'),(8,'validation','off'),(9,'recaptcha','off'),(10,'recaptcha_key',''),(11,'language','china'),(12,'google_app_ID',''),(13,'google_app_key',''),(14,'facebook_app_ID',''),(15,'facebook_app_key',''),(16,'twitter_app_ID',''),(17,'twitter_app_key',''),(21,'smtp_or_mail','mail'),(22,'smtp_host',''),(23,'smtp_username',''),(24,'smtp_password',''),(25,'smtp_encryption','ssl'),(26,'smtp_port',''),(27,'delete_account','on'),(36,'last_admin_collection','1606566223'),(37,'user_statics','[{\"month\":\"January\",\"new_users\":0},{\"month\":\"February\",\"new_users\":0},{\"month\":\"March\",\"new_users\":0},{\"month\":\"April\",\"new_users\":0},{\"month\":\"May\",\"new_users\":0},{\"month\":\"June\",\"new_users\":0},{\"month\":\"July\",\"new_users\":0},{\"month\":\"August\",\"new_users\":0},{\"month\":\"September\",\"new_users\":0},{\"month\":\"October\",\"new_users\":0},{\"month\":\"November\",\"new_users\":0},{\"month\":\"December\",\"new_users\":0}]'),(38,'audio_statics','[{&quot;month&quot;:&quot;January&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;February&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;March&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;April&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;May&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;June&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;July&quot;,&quot;new_videos&quot;:15},{&quot;month&quot;:&quot;August&quot;,&quot;new_videos&quot;:18},{&quot;month&quot;:&quot;September&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;October&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;November&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;December&quot;,&quot;new_videos&quot;:0}]'),(45,'user_registration','on'),(46,'verification_badge','on'),(49,'fb_login','off'),(50,'tw_login','off'),(51,'plus_login','off'),(52,'wowonder_app_ID',''),(53,'wowonder_app_key',''),(54,'wowonder_domain_uri',''),(56,'wowonder_login','off'),(57,'wowonder_img',''),(58,'google',''),(59,'last_created_sitemap','22-03-2019'),(60,'is_ok','1'),(63,'go_pro','on'),(64,'paypal_id',''),(65,'paypal_secret',''),(66,'paypal_mode','sandbox'),(67,'last_backup','22-03-2019'),(68,'user_ads','on'),(70,'max_upload','512000000'),(71,'s3_upload','off'),(72,'s3_bucket_name',''),(73,'amazone_s3_key',''),(74,'amazone_s3_s_key',''),(75,'region','us-east-1'),(81,'apps_api_key','cfdb063a99d830ee9e3c1f1e49174f44d04f439d'),(82,'ffmpeg_system','off'),(83,'ffmpeg_binary_file','./ffmpeg/ffmpeg'),(84,'user_max_upload','512000000'),(86,'convert_speed','faster'),(87,'night_mode','night'),(90,'ftp_host','localhost'),(91,'ftp_port','21'),(92,'ftp_username',''),(93,'ftp_password',''),(94,'ftp_upload','off'),(95,'ftp_endpoint','storage.wowonder.com'),(96,'ftp_path','./'),(111,'currency','USD'),(112,'commission','50'),(113,'pro_upload_limit','6'),(114,'pro_price','9'),(115,'server_key',''),(116,'facebook_url',''),(117,'twitter_url',''),(118,'google_url',''),(119,'currency_symbol','$'),(120,'maintenance_mode','off'),(121,'auto_friend_users','admin,gws110'),(122,'waves_color','#f98f1d'),(123,'total_songs','6'),(124,'total_albums','0'),(125,'total_plays','15'),(126,'total_sales','0.00'),(127,'total_users','22'),(128,'total_artists','2'),(129,'total_playlists','1'),(130,'total_unactive_users','0'),(131,'user_statics','[{\"month\":\"January\",\"new_users\":0},{\"month\":\"February\",\"new_users\":0},{\"month\":\"March\",\"new_users\":0},{\"month\":\"April\",\"new_users\":0},{\"month\":\"May\",\"new_users\":0},{\"month\":\"June\",\"new_users\":0},{\"month\":\"July\",\"new_users\":0},{\"month\":\"August\",\"new_users\":0},{\"month\":\"September\",\"new_users\":0},{\"month\":\"October\",\"new_users\":0},{\"month\":\"November\",\"new_users\":0},{\"month\":\"December\",\"new_users\":0}]'),(132,'songs_statics','[{\"month\":\"January\",\"new_songs\":0},{\"month\":\"February\",\"new_songs\":0},{\"month\":\"March\",\"new_songs\":0},{\"month\":\"April\",\"new_songs\":0},{\"month\":\"May\",\"new_songs\":0},{\"month\":\"June\",\"new_songs\":0},{\"month\":\"July\",\"new_songs\":0},{\"month\":\"August\",\"new_songs\":0},{\"month\":\"September\",\"new_songs\":0},{\"month\":\"October\",\"new_songs\":0},{\"month\":\"November\",\"new_songs\":0},{\"month\":\"December\",\"new_songs\":0}]'),(133,'version','1.0.4');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversations`
--

DROP TABLE IF EXISTS `conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one` int(11) NOT NULL DEFAULT '0',
  `user_two` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_one` (`user_one`),
  KEY `user_two` (`user_two`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversations`
--

LOCK TABLES `conversations` WRITE;
/*!40000 ALTER TABLE `conversations` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `copyrights`
--

DROP TABLE IF EXISTS `copyrights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyrights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copyrights`
--

LOCK TABLES `copyrights` WRITE;
/*!40000 ALTER TABLE `copyrights` DISABLE KEYS */;
/*!40000 ALTER TABLE `copyrights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads`
--

DROP TABLE IF EXISTS `downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `fingerprint` varchar(120) NOT NULL DEFAULT '',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`),
  KEY `track_id` (`track_id`),
  KEY `fingerprint` (`fingerprint`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads`
--

LOCK TABLES `downloads` WRITE;
/*!40000 ALTER TABLE `downloads` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `track_id` (`track_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
INSERT INTO `favorites` VALUES (1,2,4,1560905818);
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) NOT NULL DEFAULT '0',
  `following_id` int(11) NOT NULL DEFAULT '0',
  `artist_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `follower_id` (`follower_id`),
  KEY `following_id` (`following_id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `followers`
--

LOCK TABLES `followers` WRITE;
/*!40000 ALTER TABLE `followers` DISABLE KEYS */;
INSERT INTO `followers` VALUES (1,2,1,0,0),(2,1,2,0,0),(3,3,2,0,0),(4,4,2,0,0),(5,5,2,0,0),(6,6,2,0,0),(7,7,2,0,0),(8,8,2,0,0),(9,9,2,0,0),(10,10,2,0,0);
/*!40000 ALTER TABLE `followers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `langs`
--

DROP TABLE IF EXISTS `langs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_key` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `english` text,
  `arabic` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `dutch` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `french` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `german` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `russian` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `spanish` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `turkish` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `china` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `lang_key` (`lang_key`(255))
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `langs`
--

LOCK TABLES `langs` WRITE;
/*!40000 ALTER TABLE `langs` DISABLE KEYS */;
INSERT INTO `langs` VALUES (1,'signup','Signup','سجل','Inschrijven','S\'inscrire','Anmelden','Зарегистрироваться','Regístrate','Kaydol','注册'),(2,'get_access_to_your_music__playlists_and_account','Get access to your music, playlists and account','احصل على حق الوصول إلى الموسيقى وقوائم التشغيل والحساب','Krijg toegang tot je muziek, afspeellijsten en account','Accédez à votre musique, à vos playlists et à votre compte','Erhalten Sie Zugriff auf Ihre Musik, Wiedergabelisten und Ihr Konto','Получите доступ к своей музыке, плейлистам и аккаунту','Accede a tu música, listas de reproducción y cuenta.','Müziğinize, çalma listelerinize ve hesabınıza erişin','访问您的音乐、歌单和帐户'),(3,'full_name','Full Name','الاسم الكامل','Voor-en achternaam','Nom complet','Vollständiger Name','ФИО','Nombre completo','Ad Soyad','昵称'),(4,'username','Username','اسم المستخدم','Gebruikersnaam','Nom d\'utilisateur','Nutzername','имя пользователя','Nombre de usuario','Kullanıcı adı','用户名'),(5,'email_address','Email address','عنوان بريد الكتروني','E-mailadres','Adresse électronique','E-Mail-Addresse','Адрес электронной почты','Dirección de correo electrónico','E','电子邮件地址'),(6,'password','Password','كلمه السر','Wachtwoord','Mot de passe','Passwort','пароль','Contraseña','Parola','密码'),(7,'confirm_password','Confirm Password','تأكيد كلمة المرور','bevestig wachtwoord','Confirmez le mot de passe','Passwort bestätigen','Подтвердите Пароль','Confirmar contraseña','Şifreyi Onayla','确认密码'),(8,'already_have_an_account_','Already have an account?','هل لديك حساب؟','Heb je al een account?','Vous avez déjà un compte?','Hast du schon ein Konto?','Уже есть аккаунт?','¿Ya tienes una cuenta?','Zaten hesabınız var mı?','已经有账号？'),(9,'login','Login','تسجيل الدخول','Log in','S\'identifier','Anmeldung','Авторизоваться','Iniciar sesión','Oturum aç','登录'),(10,'by_signing_up__you_agree_to_our','By signing up, you agree to our','بالتسجيل ، أنت توافق على موقعنا','Door je aan te melden, ga je akkoord met onze','En vous inscrivant, vous acceptez notre','Mit der Anmeldung stimmen Sie unserem zu','Регистрируясь, вы соглашаетесь с нашими','Al registrarte, aceptas nuestra','Kaydolarak kabul etmiş sayılırsınız.','通过注册，即表示您同意我们的条款'),(11,'terms','Terms','شروط','Voorwaarden','termes','Bedingungen','термины','Condiciones','şartlar','条款'),(12,'and','and','و','en','et','und','а также','y','ve','和'),(13,'privacy_policy','Privacy Policy','سياسة خاصة','Privacybeleid','Politique de confidentialité','Datenschutz-Bestimmungen','политика конфиденциальности','Política de privacidad','Gizlilik Politikası','隐私政策'),(14,'please_wait..','Please wait..','ارجوك انتظر..','Even geduld aub..','S\'il vous plaît, attendez..','Warten Sie mal..','Пожалуйста, подождите..','Por favor espera..','Lütfen bekle..','请耐心等待..'),(15,'search_for_songs__artists__playlists_and_more','Search for songs, artists, playlists and more..','البحث عن الأغاني والفنانين وقوائم التشغيل والمزيد ..','Zoeken naar nummers, artiesten, afspeellijsten en meer ..','Recherchez des chansons, des artistes, des playlists et plus encore.','Suchen Sie nach Liedern, Künstlern, Wiedergabelisten und mehr ..','Поиск песен, исполнителей, плейлистов и многое другое ..','Busca canciones, artistas, listas de reproducción y más.','Şarkıları, sanatçıları, çalma listelerini ve daha fazlasını arayın ..','搜索歌曲，歌手，歌单等。'),(16,'trending_now','Trending Now','تتجه الآن','Nu populair','À la mode maintenant','Gerade angesagt','Актуальные','Siendo tendencia ahora','Bu aralar moda','现在趋势'),(17,'advanced_search','Advanced Search','البحث المتقدم','geavanceerd zoeken','Recherche Avancée','Erweiterte Suche','Расширенный поиск','Búsqueda Avanzada','gelişmiş Arama','高级搜索'),(18,'feed','Feed','تغذية','Voeden','Alimentation','Futter','Кормить','Alimentar','besleme','订阅'),(19,'upload','Upload','رفع','Uploaden','Télécharger','Hochladen','Загрузить','Subir','Yükleme','上传'),(20,'dashboard','Dashboard','لوحة القيادة','Dashboard','Tableau de bord','Instrumententafel','Приборная доска','Tablero','gösterge paneli','仪表板'),(21,'settings','Settings','الإعدادات','instellingen','Réglages','die Einstellungen','настройки','Ajustes','Ayarlar','设置'),(22,'recently_played','Recently Played','لعبت مؤخرا','Recent gespeeld','Joué récemment','Kürzlich gespielt','Недавно играл','Recientemente jugado','En Son Oynanan','播放记录'),(23,'my_playlists','My Playlists','قوائم التشغيل الخاصة بي','Mijn afspeellijsten','Mes playlists','Meine Wiedergabelisten','Мои плейлисты','Mis playlists','Oynatma Listelerim','我的歌单'),(24,'favourites','Favourites','المفضلة','favorieten','Favoris','Favoriten','Избранные','Favoritos','Favoriler','我喜欢的'),(25,'logout','Logout','الخروج','Uitloggen','Connectez - Out','Ausloggen','Выйти','Cerrar sesión','Çıkış Yap','注销'),(26,'register','Register','تسجيل','Registreren','registre','Registrieren','регистр','Registro','Kayıt olmak','注册'),(27,'forgot_your_password_','Forgot your password?','نسيت رقمك السري؟','uw wachtwoord vergeten?','Mot de passe oublié?','Haben Sie Ihr Passwort vergessen?','Забыли пароль?','¿Olvidaste tu contraseña?','Parolanızı mı unuttunuz?','忘记密码？'),(28,'don_t_have_an_account_','Don&#039;t have an account?','ليس لديك حساب؟','Heb je geen account?','Vous n\'avez pas de compte?','Ich habe noch kein Konto','У вас нет аккаунта?','¿No tienes una cuenta?','Hesabınız yok mu?','没有帐户？'),(29,'sign_up','Sign Up','سجل','Inschrijven','S\'inscrire','Anmelden','Подписаться','Regístrate','Kaydol','注册'),(30,'or','OR','أو','OF','OU','ODER','ИЛИ ЖЕ','O','VEYA','或'),(31,'login_with_facebook','Login with Facebook','تسجيل الدخول باستخدام الفيسبوك','Inloggen met Facebook','Se connecter avec Facebook','Mit Facebook einloggen','Войти с Facebook','Iniciar sesión con Facebook','Facebook ile giriş','用Facebook登录'),(32,'login_with_twitter','Login with Twitter','تسجيل الدخول مع التغريد','Inloggen met Twitter','Se connecter avec Twitter','Mit Twitter anmelden','Войти через Twitter','Inicia sesión con Twitter','Twitter ile giriş yap','用Twitter登录'),(33,'login_with_google','Login with Google','تسجيل الدخول مع جوجل','Inloggen met Google','Connectez-vous avec Google','Mit Google anmelden','Войти через Google','Inicia sesión con Google','Google ile giriş yap','用Google登录'),(34,'login_with_vk','Login with VK','تسجيل الدخول مع VK','Inloggen met VK','Se connecter avec VK','Loggen Sie sich mit VK ein','Войти через ВКонтакте','Iniciar sesión con VK','VK ile giriş yap','用VK登录'),(35,'this_e-mail_is_already_taken','This e-mail is already taken','الايميل أخذ مسبقا','Dit e-mailadres is al in gebruik','Cet e-mail est déjà pris','Diese E-Mail ist schon vergeben','Это электронная почта уже используется','este correo electrónico ya está en uso','Bu e-posta zaten alınmış','此电子邮件已被占用'),(36,'incorrect_username_or_password','Incorrect username or password','اسم المستخدم أو كلمة المرور غير صحيحة','foute gebruikersnaam of wachtwoord','identifiant ou mot de passe incorrect','Falscher Benutzername oder Passwort','Неверное имя пользователя или пароль','Nombre de usuario o contraseña incorrecta','Yanlış kullanıcı adı ya da parola','用户名或密码错误'),(37,'registration_successful__we_have_sent_you_an_email__please_check_your_inbox_spam_to_verify_your_account.','Registration successful! We have sent you an email, Please check your inbox/spam to verify your account.','تم التسجيل بنجاح لقد أرسلنا لك رسالة بريد إلكتروني ، يرجى التحقق من البريد الوارد / البريد العشوائي للتحقق من حسابك.','Registratie gelukt! We hebben je een e-mail gestuurd. Controleer je inbox / spam om je account te verifiëren.','Inscription réussi! Nous vous avons envoyé un courrier électronique. Veuillez vérifier votre boîte de réception / spam pour vérifier votre compte.','Registrierung erfolgreich! Wir haben Ihnen eine E-Mail gesendet. Bitte überprüfen Sie Ihren Posteingang / Spam, um Ihr Konto zu bestätigen.','Регистрация прошла успешно! Мы отправили вам письмо, пожалуйста, проверьте свой почтовый ящик / спам, чтобы подтвердить свой аккаунт.','¡Registro exitoso! Le hemos enviado un correo electrónico. Verifique su bandeja de entrada / correo no deseado para verificar su cuenta.','Kayıt başarılı! Size bir e-posta gönderdik, hesabınızı doğrulamak için lütfen gelen kutunuzu / spam’nizi kontrol edin.','注册成功！ 我们已向您发送了一封电子邮件，请检查您的收件箱/垃圾箱以验证您的帐户。'),(38,'this_username_is_already_taken','This username is already taken','أسم المستخدم مأخوذ مسبقا','Deze gebruikersnaam is al in gebruik','Ce nom d\'utilisateur est déjà pris','Dieser Benutzername ist bereits vergeben','Это имя пользователя уже занято','Este nombre de usuario ya está en uso','Bu kullanıcı adı zaten alınmış','该用户名已被注册'),(39,'your_account_is_not_activated_yet__please_check_your_inbox_for_the_activation_link','Your account is not activated yet, please check your inbox for the activation link','لم يتم تنشيط حسابك بعد ، يرجى التحقق من صندوق الوارد الخاص بك لمعرفة رابط التنشيط','Uw account is nog niet geactiveerd, controleer uw inbox voor de activatielink','Votre compte n\'est pas encore activé, veuillez vérifier dans votre boîte de réception le lien d\'activation.','Ihr Konto ist noch nicht aktiviert. Bitte überprüfen Sie Ihren Posteingang auf den Aktivierungslink','Ваша учетная запись еще не активирована, пожалуйста, проверьте свой почтовый ящик на ссылку активации','Su cuenta aún no está activada, por favor revise su bandeja de entrada para el enlace de activación','Hesabınız henüz aktif değil, lütfen aktivasyon bağlantısı için gelen kutunuzu kontrol edin.','您的帐户尚未激活，请在收件箱中查看激活链接'),(40,'enter_your_email_to_get_password_reset_link.','Enter your email to get password reset link.','أدخل بريدك الإلكتروني للحصول على رابط إعادة تعيين كلمة المرور.','Voer je e-mailadres in om de link voor het opnieuw instellen van je wachtwoord te krijgen','Entrez votre email pour obtenir le lien de réinitialisation de mot de passe.','Geben Sie Ihre E-Mail-Adresse ein, um den Link zum Zurücksetzen des Passworts zu erhalten.','Введите адрес электронной почты, чтобы получить ссылку для сброса пароля.','Ingrese su correo electrónico para obtener el enlace de restablecimiento de contraseña.','Parola sıfırlama bağlantısını almak için e-postanızı girin.','输入您的电子邮件以获取密码重置链接。'),(41,'send_link','Send Link','أرسل الرابط','Stuur link','Envoyer un lien','Link senden','Отправить ссылку','Enviar un enlace','Link gönder','发送链接'),(42,'this_e-mail_not_found','This E-mail not found','هذا البريد الإلكتروني غير موجود','Deze e-mail is niet gevonden','Cet e-mail introuvable','Diese E-Mail wurde nicht gefunden','Этот E-mail не найден','Este correo electrónico no encontrado','Bu E-posta bulunamadı','此电子邮件未注册'),(43,'this_e-mail_is_not_found','This e-mail is not found','لم يتم العثور على هذا البريد الإلكتروني','Deze e-mail kan niet gevonden worden','Cet e-mail est introuvable','Diese E-Mail wird nicht gefunden','Этот e-mail не найден','Este correo electrónico no se encuentra','Bu e-posta bulunamadı','找不到此电子邮件'),(44,'reset_password','Reset Password','إعادة تعيين كلمة المرور','Reset wachtwoord','réinitialiser le mot de passe','Passwort zurücksetzen','Сброс пароля','Restablecer la contraseña','Şifreyi yenile','重置密码'),(45,'error_found_while_sending_the_reset_link__please_try_again_later.','Error found while sending the reset link, please try again later.','تم العثور على خطأ أثناء إرسال رابط إعادة التعيين ، يرجى المحاولة مرة أخرى لاحقًا.','Fout gevonden tijdens het verzenden van de reset-link. Probeer het later opnieuw.','Erreur détectée lors de l\'envoi du lien de réinitialisation, veuillez réessayer ultérieurement.','Beim Senden des Reset-Links wurde ein Fehler gefunden. Bitte versuchen Sie es später erneut.','Обнаружена ошибка при отправке ссылки на сброс, повторите попытку позже.','Se ha encontrado un error al enviar el enlace de restablecimiento. Inténtalo de nuevo más tarde.','Sıfırlama bağlantısını gönderirken hata bulundu, lütfen daha sonra tekrar deneyin.','发送重置链接时发现错误，请稍后再试。'),(46,'please_check_your_inbox___spam_folder_for_the_reset_email.','Please check your inbox / spam folder for the reset email.','يرجى التحقق من مجلد البريد الوارد / البريد العشوائي للحصول على إعادة تعيين البريد الإلكتروني.','Controleer alstublieft uw inbox / spam-map voor het e-mailadres voor het opnieuw instellen.','Veuillez vérifier votre boîte de réception / courrier indésirable pour le courrier électronique de réinitialisation.','Bitte überprüfen Sie Ihren Posteingang / Spam-Ordner auf die zurückgesetzte E-Mail.','Пожалуйста, проверьте папку «Входящие» / «Спам», чтобы узнать адрес электронной почты для сброса.','Por favor, revise su carpeta de bandeja de entrada / correo no deseado para el correo electrónico de restablecimiento.','Sıfırlama e-postası için lütfen gelen kutunuzu / spam klasörünüzü kontrol edin.','请检查您的收件箱/垃圾箱查找重置电子邮件。'),(47,'please_check_your_details','Please check your details','يرجى التأكد من تفاصيل معلوماتك','Kijk alsjeblieft je gegevens na','S\'il vous plaît vérifier vos informations','Bitte überprüfe deine Details','Пожалуйста, проверьте ваши данные','Por favor comprueba tus detalles','Lütfen bilgilerinizi kontrol edin','请核对一下你的详细资料'),(48,'reset_your_password','Reset your password','اعد ضبط كلمه السر','Stel je wachtwoord opnieuw in','réinitialisez votre mot de passe','Setze dein Passwort zurück','Сбросить пароль','Restablecer su contraseña','Şifrenizi sıfırlayın','重置你的密码'),(49,'enter_new_password_to_proceed.','Enter new password to proceed.','أدخل كلمة مرور جديدة للمتابعة.','Voer een nieuw wachtwoord in om door te gaan.','Entrez un nouveau mot de passe pour continuer.','Geben Sie ein neues Passwort ein, um fortzufahren.','Введите новый пароль, чтобы продолжить.','Introduzca la nueva contraseña para continuar.','Devam etmek için yeni şifre girin.','输入新密码以继续。'),(50,'new_password','New Password','كلمة السر الجديدة','nieuw paswoord','nouveau mot de passe','Neues Kennwort','новый пароль','Nueva contraseña','Yeni Şifre','新密码'),(51,'reset','Reset','إعادة تعيين','Reset','Réinitialiser','Zurücksetzen','Сброс','Reiniciar','Reset','重置'),(52,'passwords_don_t_match','Passwords don&#039;t match','كلمات المرور غير متطابقة','Wachtwoorden komen niet overeen','Les mots de passe ne correspondent pas','Passwörter stimmen nicht überein','Пароли не совпадают','Las contraseñas no coinciden','Şifreler uyuşmuyor','密码不匹配'),(58,'about_us','About Us','معلومات عنا','Over ons','À propos de nous','Über uns','Насчет нас','Sobre nosotros','Hakkımızda','关于我们'),(59,'contact','Contact','اتصل','Contact','Contact','Kontakt','контакт','Contacto','Temas','联系'),(60,'copyright_____date___name_.','Copyright © |DATE| |NAME|.','حقوق النشر © |DATE| |NAME|.','Copyright © |DATE| |NAME|.','Copyright © |DATE| |NAME|.','Copyright © |DATE| |NAME|.','Copyright © |DATE| |NAME|.','Copyright © |DATE| |NAME|.','Telif Hakkı © |DATE| |NAME|.','Copyright © |DATE| |NAME|.'),(61,'contact_us','Contact Us','اتصل بنا','Neem contact met ons op','Contactez nous','Kontaktiere uns','Связаться с нами','Contáctenos','Bizimle iletişime geçin','联系我们'),(62,'let_us_help_you.','Let us help you.','دعنا نساعدك.','Laat ons je helpen.','Laissez-nous vous aider.','Lass uns dir helfen.','Позвольте нам помочь вам.','Dejanos ayudarte.','Sana yardım edelim.','让我们帮助你。'),(63,'write_here_your_message','Write here your message','أكتب هنا رسالتك','Schrijf hier je bericht','Ecrivez ici votre message','Schreiben Sie hier Ihre Nachricht','Напишите здесь ваше сообщение','Escribe aquí tu mensaje','Mesajını buraya yaz','在这里写下你的信息'),(64,'send','Send','إرسال','Sturen','Envoyer','Senden','послать','Enviar','göndermek','发送'),(65,'e-mail_sent_successfully','E-mail sent successfully','تم إرسال البريد الإلكتروني بنجاح','E-mail succesvol verzonden','E-mail envoyé avec succès','Email wurde erfolgreich Versendet','Письмо успешно отправлено','E-mail enviado correctamente','E-posta başarıyla gönderildi','邮件发送成功'),(66,'followers','Followers','متابعون','Volgers','Suiveurs','Anhänger','Читают','Seguidores','İzleyiciler','关注'),(67,'following','Following','التالية','Volgend op','Suivant','Folgenden','Следующий','Siguiendo','Takip etme','粉丝'),(68,'all','All','الكل','Allemaal','Tout','Alles','Все','Todos','Herşey','全部'),(69,'songs','Songs','الأغاني','songs','Chansons','Lieder','песни','Canciones','Şarkılar','歌曲'),(70,'albums','Albums','ألبومات','albums','Albums','Alben','Альбомы','Los álbumes','albümler','专辑'),(71,'playlists','Playlists','قوائم التشغيل','afspeellijsten','Playlists','Wiedergabelisten','Плейлисты','Listas de reproducción','Çalma listeleri','播放列表'),(72,'follow','Follow','إتبع','Volgen','Suivre','Folgen','следить','Seguir','Takip et','跟随'),(73,'edit_profile','Edit Profile','تعديل الملف الشخصي','Bewerk profiel','Editer le profil','Profil bearbeiten','Редактировать профиль','Editar perfil','Profili Düzenle','编辑资料'),(74,'confirm_your_account','Confirm your account','اكد حسابك','Bevestig je account','Confirmez votre compte','Bestätigen Sie ihr Konto','Подтвердите свой аккаунт','Confirme su cuenta','Hesabını onayla','确认您的帐户'),(75,'general_settings','General Settings','الاعدادات العامة','Algemene instellingen','réglages généraux','Allgemeine Einstellungen','общие настройки','Configuración general','Genel Ayarlar','常规设置'),(76,'email','Email','البريد الإلكتروني','E-mail','Email','Email','Эл. адрес','Email','E-posta','邮箱'),(77,'country','Country','بلد','land','Pays','Land','Страна','País','ülke','国家'),(78,'age','Age','عمر','Leeftijd','Âge','Alter','Возраст','Años','Yaş','年龄'),(79,'gender','Gender','جنس','Geslacht','Le sexe','Geschlecht','Пол','Género','Cinsiyet','性别'),(80,'save','Save','حفظ','Opslaan','sauvegarder','sparen','Сохранить','Salvar','Kayıt etmek','保存'),(81,'delete_account','Delete Account','حذف الحساب','Account verwijderen','Supprimer le compte','Konto löschen','Удалить аккаунт','Borrar cuenta','Hesabı sil','删除帐户'),(82,'are_you_sure_you_want_to_delete_your_account__all_content_including_published_songs__will_be_permanetly_removed_','Are you sure you want to delete your account? All content including published songs, will be permanetly removed!','هل انت متأكد انك تريد حذف حسابك؟ ستتم إزالة جميع المحتويات بما في ذلك الأغاني المنشورة نهائيًا!','Weet je zeker dat je je account wilt verwijderen? Alle inhoud, inclusief gepubliceerde nummers, wordt permanent verwijderd!','Êtes-vous sûr de vouloir supprimer votre compte? Tout le contenu, y compris les chansons publiées, sera définitivement supprimé!','Möchten Sie Ihr Konto wirklich löschen? Alle Inhalte einschließlich der veröffentlichten Songs werden dauerhaft entfernt!','Вы уверены, что хотите удалить свой аккаунт? Весь контент, включая опубликованные песни, будет окончательно удален!','¿Estás seguro de que quieres eliminar tu cuenta? Todo el contenido, incluidas las canciones publicadas, se eliminará permanentemente!','Hesabınızı silmek istediğinizden emin misiniz? Yayınlanan şarkılar dahil tüm içerikler kalıcı olarak kaldırılacak!','您确定要删除您的帐户吗？所有内容，包括已发表的歌曲，将永久删除！'),(83,'current_password','Current Password','كلمة المرور الحالي','huidig ​​wachtwoord','Mot de passe actuel','derzeitiges Passwort','Текущий пароль','contraseña actual','Şimdiki Şifre','当前密码'),(84,'delete','Delete','حذف','Verwijder','Effacer','Löschen','удалять','Borrar','silmek','删除'),(85,'change_password','Change Password','غير كلمة السر','Wachtwoord wijzigen','Changer le mot de passe','Ändere das Passwort','Изменить пароль','Cambia la contraseña','Şifre değiştir','更改密码'),(86,'repeat_new_password','Repeat New Password','كرر كلمة المرور الجديدة','Herhaal nieuw wachtwoord','Répété le nouveau mot de passe','Wiederhole das neue Passwort','Повторите новый пароль','Repita la nueva contraseña','Yeni şifreyi tekrar girin','重置新密码'),(87,'change','Change','يتغيرون','Verandering','Changement','Veränderung','+ Изменить','Cambio','Değişiklik','更改'),(88,'profile_settings','Profile Settings','إعدادات الملف الشخصي','Profielinstellingen','Paramètres de profil','Profileinstellungen','Настройки профиля','Configuración de perfil','Profil ayarları','资料设置'),(89,'about_me','About Me','عني','Over mij','À propos de moi','Über mich','Обо мне','Sobre mi','Benim hakkımda','关于我'),(90,'facebook_username','Facebook Username','اسم مستخدم Facebook','Facebook gebruikersnaam','Nom d\'utilisateur Facebook','Facebook-Benutzername','Facebook Имя пользователя','Nombre de usuario de Facebook','Facebook Kullanıcı Adı','Facebook用户名'),(91,'website','Website','موقع الكتروني','Website','Site Internet','Webseite','Веб-сайт','Sitio web','Web sitesi','网站'),(94,'male','Male','الذكر','Mannetje','Mâle','Männlich','мужчина','Masculino','Erkek','男'),(95,'female','Female','إناثا','Vrouw','Femelle','Weiblich','женский','Hembra','Kadın','女'),(96,'settings_successfully_updated_','Settings successfully updated!','تم تحديث الإعدادات بنجاح!','Instellingen succesvol bijgewerkt!','Paramètres mis à jour avec succès!','Einstellungen erfolgreich aktualisiert!','Настройки успешно обновлены!','Configuraciones exitosamente actualizadas!','Ayarlar başarıyla güncellendi!','设置已成功更新！'),(97,'no_notifications_found','No notifications found','لا توجد إخطارات','Geen meldingen gevonden','Aucune notification trouvée','Keine Benachrichtigungen gefunden','Уведомления не найдены','No se encontraron notificaciones','Bildirim bulunamadı','没有找到通知'),(98,'year','year','عام','jaar','année','Jahr','год','año','yıl','年'),(99,'month','month','شهر','maand','mois','Monat','месяц','mes','ay','月'),(100,'day','day','يوم','dag','journée','Tag','день','día','gün','天'),(101,'hour','hour','ساعة','uur','heure','Stunde','час','hora','saat','小时'),(102,'minute','minute','اللحظة','minuut','minute','Minute','минут','minuto','dakika','分钟'),(103,'second','second','ثانيا','tweede','seconde','zweite','второй','segundo','ikinci','秒'),(104,'years','years','سنوات','jaar','années','Jahre','лет','años','yıl','年'),(105,'months','months','الشهور','maanden','mois','Monate','месяцы','meses','ay','月'),(106,'days','days','أيام','dagen','journées','Tage','дней','dias','günler','天'),(107,'hours','hours','ساعات','uur','heures','Std','часов','horas','saatler','小时'),(108,'minutes','minutes','الدقائق','notulen','minutes','Protokoll','минут','minutos','dakika','分钟'),(109,'seconds','seconds','ثواني','seconden','secondes','Sekunden','секунд','segundos','saniye','秒'),(110,'ago','ago','منذ','geleden','depuis','vor','тому назад','hace','önce','前'),(111,'started_following_you.','started following you.','بدات الاحقك.','begon jou te volgen.','commencé à te suivre.','Begann dir zu folgen.','начал следить за тобой.','empecé a seguirte.','seni takip etmeye başladım.','开始关注您。'),(112,'profile_successfully_updated_','Profile successfully updated!','تم تحديث الملف الشخصي بنجاح!','Profiel met succes bijgewerkt!','Profil mis à jour avec succès!','Profil erfolgreich aktualisiert!','Профиль успешно обновлен!','Perfil actualizado con éxito!','Profil başarıyla güncellendi!','资料已成功更新！'),(113,'invalid_website_url__format_allowed__http_s_____.___','Invalid website url, format allowed: http(s)://*.*/*','عنوان URL غير صالح لموقع الويب ، التنسيق المسموح به: http (s): //*.*/*','Ongeldige website-URL, toegestane indeling: http (s): //*.*/*','URL de site Web non valide, format autorisé: http (s): //*.*/*','Ungültige Website-URL, Format zulässig: http (s): //*.*/*','Неверный URL сайта, допустимый формат: http (s): //*.*/*','URL del sitio web no válida, formato permitido: http (s): //*.*/*','Geçersiz web sitesi URL\'si, biçime izin verilir: http: s: //*.*/*','网站网址无效，格式允许: http(s)://*.*/*'),(114,'invalid_facebook_username__urls_are_not_allowed','Invalid facebook username, urls are not allowed','اسم مستخدم facebook غير صالح ، غير مسموح باستخدام عناوين url','Ongeldige facebook-gebruikersnaam, URL\'s zijn niet toegestaan','Nom d\'utilisateur facebook non valide, les URL ne sont pas autorisées','Ungültiger Facebook-Benutzername, URLs sind nicht zulässig','Неверное имя пользователя в Facebook, URL не разрешены','Nombre de usuario de Facebook no válido, las URL no están permitidas','Geçersiz facebook kullanıcı adı, URL\'lere izin verilmiyor','Facebook用户名无效，不允许使用网址'),(115,'new_password_is_too_short','New password is too short','كلمة المرور الجديدة قصيرة جدًا','Nieuw wachtwoord is te kort','Le nouveau mot de passe est trop court','Neues Passwort ist zu kurz','Новый пароль слишком короткий','La nueva contraseña es demasiado corta','Yeni şifre çok kısa','新密码太短'),(116,'your_current_password_is_invalid','Your current password is invalid','كلمة المرور الحالية غير صالحة','Uw huidige wachtwoord is ongeldig','Votre mot de passe actuel est invalide','Ihr aktuelles Passwort ist ungültig','Ваш текущий пароль недействителен','Tu contraseña actual no es válida','Mevcut şifreniz geçersiz','您当前的密码无效'),(117,'your_password_was_successfully_updated_','Your password was successfully updated!','تم تحديث كلمة مرورك بنجاح!','Uw wachtwoord is succesvol bijgewerkt!','Votre mot de passe a été mis à jour avec succès!','Ihr Passwort wurde erfolgreich aktualisiert!','Ваш пароль был успешно обновлен!','Su contraseña fue actualizada con éxito!','Şifreniz başarıyla güncellendi!','您的密码已成功更新！'),(118,'your_account_was_successfully_deleted__please_wait..','Your account was successfully deleted, please wait..','تم حذف حسابك بنجاح ، يرجى الانتظار ..','Uw account is succesvol verwijderd, even geduld aub ..','Votre compte a bien été supprimé, veuillez patienter ..','Ihr Konto wurde erfolgreich gelöscht. Bitte warten Sie ..','Ваша учетная запись была успешно удалена, пожалуйста, подождите ..','Su cuenta fue eliminada exitosamente, por favor espere ..','Hesabınız başarıyla silindi, lütfen bekleyin ..','您的帐户已成功删除，请稍候..'),(119,'select_files_to_upload','Select files to upload','حدد الملفات المراد تحميلها','Selecteer bestanden om te uploaden','Sélectionnez les fichiers à télécharger','Wählen Sie die Dateien zum Hochladen aus','Выберите файлы для загрузки','Seleccionar archivos para subir','Yüklenecek dosyaları seçin','选择要上传的文件'),(120,'or_drag___drop_files_here','or drag &amp; drop files here','أو اسحب &amp; قم بوضع الملفات هنا','of sleep &amp; zet hier bestanden neer','ou faites glisser &amp; Déposez les fichiers ici','oder ziehen Sie &amp; Dateien hier ablegen','или перетащите &amp; перетащите файлы сюда','o arrastrar &amp; soltar archivos aquí','veya sürükle &amp; dosyaları buraya bırak','或者在这里拖放文件'),(121,'title','Title','عنوان','Titel','Titre','Titel','заглавие','Título','Başlık','标题'),(122,'your_song_title__2_-_55_characters','Your song title, 2 - 55 characters','عنوان أغنيتك ، 2 - 55 حرفًا','De titel van je nummer, 2 - 55 tekens','Le titre de votre chanson, 2 - 55 caractères','Ihr Songtitel, 2 - 55 Zeichen','Название вашей песни, 2 - 55 символов','El título de tu canción, 2 - 55 caracteres.','Şarkının adı, 2 - 55 karakter','你的歌名，2-55个字符'),(123,'description','Description','وصف','Omschrijving','La description','Beschreibung','Описание','Descripción','Açıklama','描述'),(124,'tags','Tags','الكلمات','Tags','Mots clés','Stichworte','Теги','Etiquetas','Etiketler','标签'),(125,'add_tags_to_describe_more_about_your_track','Add tags to describe more about your track','أضف علامات لوصف المزيد عن المسار الخاص بك','Voeg tags toe om meer over je nummer te beschrijven','Ajoutez des tags pour décrire plus en détail votre piste','Fügen Sie Tags hinzu, um mehr über Ihren Track zu beschreiben','Добавьте теги, чтобы описать больше о вашем треке','Agrega etiquetas para describir más sobre tu pista','Parçanız hakkında daha fazla açıklama yapmak için etiketler ekleyin','添加标签以描述有关您的音乐的更多信息'),(126,'genre','Genre','نوع أدبي','Genre','Genre','Genre','Жанр','Género','Tür','类型'),(127,'availability','Availability','توفر','Beschikbaarheid','Disponibilité','Verfügbarkeit','Доступность','Disponibilidad','Kullanılabilirlik','是否公开'),(128,'public','Public','عامة','Openbaar','Publique','Öffentlichkeit','общественного','Público','halka açık','公开'),(129,'private','Private','نشر','Privaat','Privé','Privatgelände','Частный','Privado','Özel','私人'),(130,'age_restriction','Age Restriction','شرط العمر أو السن','Leeftijdsbeperking','Restriction d\'âge','Altersbeschränkung','Ограничение по возрасту','Restricción de edad','Yaş kısıtlaması','年龄限制'),(131,'all_ages_can_listen_this_song','All ages can listen this song','يمكن لجميع الأعمار الاستماع إلى هذه الأغنية','Alle leeftijden kunnen dit liedje beluisteren','Tous les âges peuvent écouter cette chanson','Alle Altersgruppen können dieses Lied hören','Все возрасты могут слушать эту песню','Todas las edades pueden escuchar esta canción.','Her şarkı bu şarkıyı dinleyebilir','所有年龄的人都可以听这首歌'),(132,'only__18','Only +18','فقط +18','Alleen +18','Seulement +18','Nur +18','Только +18','Solo +18','Sadece +18','18岁以上可以听'),(133,'price','Price','السعر','Prijs','Prix','Preis','Цена','Precio','Fiyat','价格'),(134,'publish','Publish','نشر','Publiceren','Publier','Veröffentlichen','Публиковать','Publicar','Yayınla','发布'),(135,'audio_file_not_found__please_refresh_the_page_and_try_again.','Audio file not found, please refresh the page and try again.','لم يتم العثور على الملف الصوتي ، يرجى تحديث الصفحة والمحاولة مرة أخرى.','Audiobestand niet gevonden, vernieuw de pagina en probeer het opnieuw.','Fichier audio non trouvé, veuillez actualiser la page et réessayer.','Audiodatei nicht gefunden. Aktualisieren Sie die Seite und versuchen Sie es erneut.','Аудио файл не найден, обновите страницу и попробуйте снова.','No se encontró el archivo de audio, por favor actualice la página y vuelva a intentarlo.','Ses dosyası bulunamadı, lütfen sayfayı yenileyin ve tekrar deneyin.','音频文件未找到，请刷新页面并重试。'),(136,'something_went_wrong_please_try_again_later_','Something went wrong Please try again later!','هناك شئ خاطئ، يرجى المحاولة فى وقت لاحق!','Er is iets misgegaan Probeer het later opnieuw!','Quelque chose c\'est mal passé. Merci d\'essayer plus tard!','Etwas ist schief gelaufen. Bitte versuchen Sie es später noch einmal!','Что-то пошло не так. Пожалуйста, повторите попытку позже!','Algo salió mal Por favor, intente de nuevo más tarde!','Bir şeyler yanlış oldu. Lütfen sonra tekrar deneyiniz!','发生错误。请稍后再试！'),(138,'please_wait__your_track_is_being_coverted_to_mp3_audio_file._this_might_take_a_few_minutes.','Please wait, your track is being coverted to mp3 audio file. This might take a few minutes.','الرجاء الانتظار ، يتم إخفاء المسار الخاص بك إلى ملف صوتي MP3. وهذا قد يستغرق بضع دقائق.','Een ogenblik geduld, je nummer wordt omspoeld naar mp3-audiobestand. Dit kan een paar minuten duren.','Veuillez patienter, votre piste est convertie en fichier audio mp3. Ceci pourrait prendre quelques minutes.','Bitte warten Sie, Ihr Track wird in eine MP3-Audiodatei umgewandelt. Dies könnte ein paar Minuten dauern.','Пожалуйста, подождите, ваш трек записывается в mp3-файл. Это может занять несколько минут.','Por favor espere, su pista está siendo cubierta a un archivo de audio mp3. Esto puede tardar unos minutos.','Lütfen bekleyin, parçanız mp3 ses dosyasına dönüştürülüyor. Bu, birkaç dakika sürebilir.','请稍等，您的音乐正在被转换成mp3音频文件。这可能需要几分钟。'),(139,'invalid_file_format__only_mp3_is_allowed','Invalid file format, only mp3 is allowed','تنسيق ملف غير صالح ، يُسمح بتنسيق mp3 فقط','Ongeldige bestandsindeling, alleen mp3 is toegestaan','Format de fichier invalide, seul le format mp3 est autorisé','Ungültiges Dateiformat, nur MP3 ist zulässig','Неверный формат файла, разрешен только mp3','Formato de archivo inválido, solo se permite mp3','Geçersiz dosya formatı, sadece mp3 izin verilir','无效的文件格式，只允许mp3'),(140,'invalid_file_format__only_jpg__jpeg__png_are_allowed','Invalid file format, only jpg, jpeg, png are allowed','تنسيق ملف غير صالح ، يُسمح فقط بتنسيق jpg و jpeg و png','Ongeldige bestandsindeling, alleen jpg, jpeg, png zijn toegestaan','Format de fichier invalide, seuls les formats jpg, jpeg, png sont autorisés','Ungültiges Dateiformat, nur jpg, jpeg, png sind zulässig','Неверный формат файла, разрешены только jpg, jpeg, png','Formato de archivo no válido, solo se permiten jpg, jpeg, png','Geçersiz dosya formatı, sadece jpg, jpeg, png izin verilir','无效的文件格式，只允许jpg, jpeg, png'),(141,'error_found_while_uploading_your_image__please_try_again_later.','Error found while uploading your image, please try again later.','تم العثور على خطأ أثناء تحميل صورتك ، يرجى إعادة المحاولة لاحقًا.','Fout gevonden tijdens het uploaden van uw afbeelding. Probeer het later opnieuw.','Une erreur a été détectée lors du téléchargement de votre image. Veuillez réessayer ultérieurement.','Beim Hochladen des Bildes wurde ein Fehler gefunden. Bitte versuchen Sie es später erneut.','При загрузке изображения обнаружена ошибка. Повторите попытку позже.','Se ha encontrado un error al cargar tu imagen, inténtalo de nuevo más tarde.','Resminizi yüklerken hata bulundu, lütfen daha sonra tekrar deneyin.','上传图片时发现错误，请稍后重试。'),(142,'error_found_while_uploading_your_track__please_try_again_later.','Error found while uploading your track, please try again later.','تم العثور على خطأ أثناء تحميل المسار ، يرجى المحاولة مرة أخرى لاحقًا.','Fout gevonden tijdens het uploaden van je nummer, probeer het later opnieuw.','Une erreur a été détectée lors du téléchargement de votre piste. Veuillez réessayer ultérieurement.','Fehler beim Hochladen Ihres Tracks. Bitte versuchen Sie es später erneut.','При загрузке трека обнаружена ошибка. Повторите попытку позже.','Se ha encontrado un error al cargar la pista. Inténtalo de nuevo más tarde.','Parça yüklenirken hata bulundu, lütfen daha sonra tekrar deneyin.','上传音乐时发现错误，请稍后重试。'),(143,'invalid_file_format__only_mp3__ogg__wav__and_mpeg_is_allowed','Invalid file format, only mp3, ogg, wav, and mpeg is allowed','يُسمح بتنسيق ملف غير صالح وملفات mp3 و ogg و wav و mpeg فقط','Ongeldige bestandsindeling, alleen mp3, ogg, wav en mpeg is toegestaan','Format de fichier invalide, seuls les fichiers mp3, ogg, wav et mpeg sont autorisés','Ungültiges Dateiformat, nur mp3, ogg, wav und mpeg ist zulässig','Неверный формат файла, разрешены только mp3, ogg, wav и mpeg','Formato de archivo no válido, solo se permite mp3, ogg, wav y mpeg','Geçersiz dosya formatı, yalnızca mp3, ogg, wav ve mpeg dosyalarına izin verilir','文件格式无效，只允许使用mp3，ogg，wav和mpeg'),(144,'sorry__page_not_found_','Sorry, page not found!','عذرا، لم يتم العثور على الصفحة!','Sorry, pagina niet gevonden!','Désolé, page non trouvée!','Entschuldigung, Seite nicht gefunden!','Извините, страница не найдена!','Lo sentimos, la página no se encuentra!','Üzgünüz, sayfa bulunamadı!','对不起，页面没有找到!'),(145,'the_page_you_are_looking_for_could_not_be_found._please_check_the_link_you_followed_to_get_here_and_try_again.','The page you are looking for could not be found. Please check the link you followed to get here and try again.','لا يمكن العثور على الصفحة التي تبحث عنها. يرجى التحقق من الرابط الذي اتبعته للوصول إلى هنا والمحاولة مرة أخرى.','De pagina waarnaar u op zoek bent, kon niet worden gevonden. Controleer de link die je hebt gevolgd om hier te komen en probeer het opnieuw.','La page que vous recherchez n\'a pu être trouvée. Veuillez vérifier le lien que vous avez suivi pour arriver ici et réessayer.','Die von Ihnen gesuchte Seite wurde nicht gefunden. Bitte überprüfen Sie den Link, den Sie folgten, um hierher zu gelangen und es erneut zu versuchen.','Страница, которую вы ищете, не может быть найдена. Пожалуйста, проверьте ссылку, по которой вы перешли, и попробуйте снова.','La página que estás buscando no se pudo encontrar. Por favor revise el enlace que siguió para llegar aquí e intente nuevamente.','Aradığınız sayfa bulunamadı. Lütfen buraya gelip tekrar denediğiniz bağlantıyı kontrol edin.','找不到您要查找的页面。 请检查您所关注的链接，然后再试一次。'),(146,'home','Home','الصفحة الرئيسية','Huis','Accueil','Zuhause','Главная','Casa','Ev','首页'),(147,'become_an_artist','Become an artist','تصبح فنانا','Word een artiest','Devenir artiste','Künstler werden','Стать художником','Conviértete en un artista','Bir sanatçı ol','成为一个歌手'),(148,'info','Info','معلومات','info','Info','Info','Информация','Información','Bilgi','信息'),(149,'located_in','Located in','يقع في','Gevestigd in','Situé dans','Gelegen in','Находится в','Situado en','Konumlanmış','位于'),(150,'bio','Bio','السيرة الذاتية','Bio','Bio','Bio','Bio','Bio','biyo','个人简历'),(151,'social_links','Social Links','روابط اجتماعية','Sociale links','Liens sociaux','Soziale Links','Социальные ссылки','vínculos sociales','Sosyal bağlantılar','社交链接'),(152,'__user_gender','{{USER gender}}','{{USER gender}}','{{USER gender}}','{{USER gender}}','{{USER gender}}','{{USER gender}}','{{USER gender}}','{{USER gender}}','{{USER gender}}'),(153,'release_date','Release date','يوم الاصدار','Datum van publicatie','Date de sortie','Veröffentlichungsdatum','Дата выхода','Fecha de lanzamiento','Yayın tarihi','发布日期'),(154,'uploaded_new_song','Uploaded new song','تم تحميل أغنية جديدة','Nieuw liedje geüpload','Nouvelle chanson téléchargée','Neues Lied hochgeladen','Загрузил новую песню','Subido nueva canción','Yeni şarkı yüklendi','上传了新歌'),(155,'report','Report','أبلغ عن','Verslag doen van','rapport','Bericht','отчет','Informe','Rapor','报告'),(156,'delete_track','Delete Track','حذف المسار','Track verwijderen','Supprimer la piste','Track löschen','Удалить трек','Eliminar pista','Parçayı Sil','删除音乐'),(157,'edit_info','Edit Info','تحرير المعلومات','Bewerk informatie','Modifier les informations','Bearbeitungs Information','Изменить информацию','Editar información','Bilgi düzenle','编辑信息'),(158,'pin','Pin','دبوس','Pin','Épingle','Stift','Штырь','Alfiler','Toplu iğne','Pin'),(159,'no_tracks_found','No tracks found','لم يتم العثور على المسارات','Geen nummers gevonden','Aucune piste trouvée','Keine Tracks gefunden','Треки не найдены','No se encontraron pistas','Hiçbir parça bulunamadı','找不到音乐'),(160,'load_more','Load More','تحميل المزيد','Meer laden','Charger plus','Mehr laden','Загрузи больше','Carga más','Daha fazla yükle','加载更多'),(161,'no_more_tracks_found','No more tracks found','لم يتم العثور على المزيد من المسارات','Geen nummers meer gevonden','Pas plus de pistes trouvées','Keine weiteren Titel gefunden','Больше треков не найдено','No se encontraron más pistas','Başka parça bulunamadı','找不到更多音乐'),(162,'like','Like','مثل','Net zoals','Comme','Mögen','подобно','Me gusta','Sevmek','喜欢'),(163,'share','Share','شارك','Delen','Partager','Aktie','Поделиться','Compartir','Pay','分享'),(164,'more','More','أكثر من','Meer','Plus','Mehr','Больше','Más','Daha','更多'),(165,'add_to_playlist','Add to Playlist','أضف إلى قائمة التشغيل','Toevoegen aan afspeellijst','Ajouter à la playlist','Zur Titelliste hinzufügen','Добавить в плейлист','Agregar a la lista de reproducción','Oynatma listesine ekle','添加到歌单'),(166,'add_to_queue','Add to Queue','إضافة إلى قائمة الانتظار','Toevoegen aan wachtrij','Ajouter à la liste','Zur Warteschlange hinzufügen','Добавить в очередь','Añadir a la cola','Sıraya ekle','添加到队列'),(167,'edit','Edit','تصحيح','Bewerk','modifier','Bearbeiten','редактировать','Editar','Düzenle','编辑'),(168,'download','Download','تحميل','Download','Télécharger','Herunterladen','Скачать','Descargar','İndir','下载'),(169,'purchase','Purchase','شراء','Aankoop','achat','Kauf','покупка','Compra','Satın alma','购买'),(170,'save_track','Save Track','حفظ المسار','Track opslaan','Enregistrer la piste','Track speichern','Сохранить трек','Guardar track','Parçayı Kaydet','保存记录'),(171,'the_new_track_details_are_updated__please_wait..','The new track details are updated, please wait..','يتم تحديث تفاصيل المسار الجديد ، يرجى الانتظار ..','De nieuwe trackdetails zijn bijgewerkt, even geduld aub ..','Les détails de la nouvelle piste sont mis à jour, veuillez patienter ..','Die neuen Track-Details werden aktualisiert, bitte warten ..','Обновлены подробности нового трека, пожалуйста, подождите ..','Los nuevos detalles de la pista se actualizan, por favor espere ...','Yeni parça detayları güncellendi, lütfen bekleyin ..','新的曲目详细信息已更新，请稍候。'),(172,'liked_your_song.','liked your song.','اعجبتك اغنيتك','vond je liedje leuk.','aimé votre chanson.','mochte dein Lied','понравилась твоя песня','Me gustó tu canción.','şarkını beğendim','喜欢你的歌。'),(173,'liked','Liked','احب','vond','Aimé','Gefallen','Понравилось','Gustó','sevilen','喜欢'),(174,'write_a_comment_and_press_enter','Write a comment and press enter','اكتب تعليقًا واضغط على إدخال','Schrijf een opmerking en druk op Enter','Écrivez un commentaire et appuyez sur Entrée','Schreiben Sie einen Kommentar und drücken Sie die Eingabetaste','Напишите комментарий и нажмите ввод','Escribe un comentario y presiona enter','Bir yorum yaz ve enter tuşuna basın','写下评论并按回车键'),(175,'delete_your_track','Delete your track','حذف المسار الخاص بك','Wis je nummer','Supprimer votre piste','Löschen Sie Ihre Spur','Удалить свой трек','Borra tu pista','Parçanı sil','删除你的音乐'),(176,'are_you_sure_you_want_to_delete_this_track_','Are you sure you want to delete this track?','هل تريد بالتأكيد حذف هذا المسار؟','Weet je zeker dat je deze track wilt verwijderen?','Êtes-vous sûr de vouloir supprimer cette piste?','Möchten Sie diesen Titel wirklich löschen?','Вы уверены, что хотите удалить этот трек?','¿Seguro que quieres borrar esta pista?','Bu parçayı silmek istediğinize emin misiniz?','你确定要删除这首歌吗?'),(177,'cancel','Cancel','إلغاء','annuleren','Annuler','Stornieren','отменить','Cancelar','İptal etmek','取消'),(178,'share_this_song','Share this Song','شارك هذه الأغنية','Deel deze song','Partager cette chanson','Teile diesen Song','Поделитесь этой песней','Comparte esta canción','Bu Şarkıyı Paylaş','分享这首歌'),(179,'close','Close','قريب','Dichtbij','Fermer','Schließen','близко','Cerrar','Kapat','关闭'),(180,'tracks','Tracks','المسارات','Sporen','Des pistes','Spuren','Дорожки','Pistas','raylar','歌曲'),(181,'recently_played_music','Recently Played Music','الموسيقى التي تم تشغيلها مؤخرًا','Onlangs gespeelde muziek','Musique récemment jouée','Kürzlich gespielte Musik','Недавно сыгранная музыка','Música recientemente reproducida','Son Çalınan Müzik','最近播放的音乐'),(182,'no_tracks_found__try_to_listen_more____','No tracks found, try to listen more? ;)','لم يتم العثور على مقطوعات ، حاول الاستماع أكثر؟ ؛)','Geen nummers gevonden, probeer je meer te luisteren? ;)','Aucune piste trouvée, essayez d\'écouter plus? ;)','Keine Titel gefunden, versuchen Sie mehr zu hören? ;)','Треков не найдено, попробуйте послушать больше? ;)','No se han encontrado pistas, intenta escuchar más? ;)','Hiçbir parça bulunamadı, daha fazla dinlemeye çalışın mı? ;)','找不到音乐，想多听点吗？ ;)'),(183,'repeat','Repeat','كرر','Herhaling','Répéter','Wiederholen','Повторение','Repetir','Tekrar et','重复'),(184,'shuffle','Shuffle','خلط','schuifelen','Mélanger','Mischen','шарканье','Barajar','Karıştır','随机播放'),(185,'queue','Queue','طابور','Wachtrij','Queue','Warteschlange','Очередь','Cola','kuyruk','队列'),(186,'clear','Clear','واضح','Duidelijk','Clair','klar','Очистить','Claro','Açık','清除'),(187,'just_now','Just now','في هذة اللحظة','Net nu','Juste maintenant','Gerade jetzt','Прямо сейчас','Justo ahora','Şu anda','现在'),(188,'no_comments_found','No comments found','لم يتم العثور على تعليقات','Geen reacties gevonden','Aucun commentaire trouvé','Keine Kommentare gefunden','Комментариев не найдено','No se encontraron comentarios','Yorum bulunamadı','找不到评论'),(189,'delete_comment','Delete comment','حذف تعليق','Reactie verwijderen','Supprimer le commentaire','Kommentar löschen','Удалить комментарий','Eliminar comentario','Yorumu sil','删除评论'),(190,'are_you_sure_you_want_to_delete_this_comment_','Are you sure you want to delete this comment?','هل أنت متأكد أنك تريد حذف هذا التعليق؟','Weet je zeker dat je deze reactie wilt verwijderen?','êtes-vous sûr de vouloir supprimer ce commentaire?','Möchten Sie diesen Kommentar wirklich löschen?','Вы уверенны, что хотите удалить этот комментарий?','¿Estás seguro de que quieres eliminar este comentario?','Bu yorumu silmek istediğinize emin misiniz?','你确定要删除这条评论吗?'),(191,'report_comment','Report Comment','تقرير التعليق','Reactie melden','Signaler un commentaire','Kommentar melden','Пожаловаться на комментарий','Reportar comentario','Yorum Bildir','报告评论'),(192,'no_more_comments_found','No more comments found','لم يتم العثور على المزيد من التعليقات','Geen reacties meer gevonden','Aucun autre commentaire trouvé','Keine weiteren Kommentare gefunden','Больше комментариев не найдено','No se han encontrado más comentarios.','Başka yorum bulunamadı','没有发现更多评论'),(213,'cateogry_1','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧'),(215,'in','in','في','in','dans','im','в','en','içinde','在'),(216,'other','Other','آخر','anders','Autre','Andere','Другой','Otro','Diğer','其他'),(217,'more_tracks','More Tracks','المزيد من المسارات','Meer nummers','Plus de pistes','Weitere Tracks','Больше треков','Más pistas','Daha Fazla Parça','更多音乐'),(218,'purchase_track','Purchase track','شراء المسار','Aankoop track','Piste d\'achat','Kaufspur','Трек покупки','Pista de compra','Satın alma parça','购买记录'),(219,'error_found_while_creating_the_payment__please_try_again_later.','Error found while creating the payment, please try again later.','تم العثور على خطأ أثناء إنشاء الدفع ، يرجى إعادة المحاولة لاحقًا.','Fout gevonden tijdens het maken van de betaling. Probeer het later opnieuw.','Erreur trouvée lors de la création du paiement, veuillez réessayer ultérieurement.','Beim Erstellen der Zahlung wurde ein Fehler gefunden. Bitte versuchen Sie es später erneut.','При создании платежа обнаружена ошибка. Повторите попытку позже.','Se ha encontrado un error al crear el pago, inténtalo de nuevo más tarde.','Ödeme oluşturulurken hata bulundu, lütfen daha sonra tekrar deneyin.','付款时发现错误，请稍后重试。'),(220,'purchase_required','Purchase Required','شراء المطلوبة','Aankoop vereist','Achat requis','Kauf erforderlich','Требуется покупка','Compra Requerida','Satın Alma Gerekli','购买必读'),(221,'to_continue_listening_to_this_track__you_need_to_purchase_the_song.','To continue listening to this track, you need to purchase the song.','لمتابعة الاستماع إلى هذا المسار ، تحتاج إلى شراء الأغنية.','Als je naar deze track wilt blijven luisteren, moet je het nummer kopen.','Pour continuer à écouter cette piste, vous devez acheter la chanson.','Um diesen Titel weiter anzuhören, müssen Sie den Song kaufen.','Чтобы продолжить прослушивание этого трека, вам необходимо приобрести песню.','Para continuar escuchando esta pista, necesitas comprar la canción.','Bu parçayı dinlemeye devam etmek için, şarkıyı satın almanız gerekir.','要继续听这首歌，你需要购买这首歌。'),(222,'purchased','Purchased','اشترى','Gekocht','Acheté','Gekauft','купленный','Comprado','satın alındı','我的购买'),(223,'no_purchased_tracks_found','No purchased tracks found','لم يتم العثور على المسارات المشتراة','Geen gekochte nummers gevonden','Aucune piste achetée trouvée','Keine gekauften Titel gefunden','Купленные треки не найдены','No se encontraron pistas compradas','Satın alınan hiçbir parça bulunamadı','未找到购买音乐'),(224,'purchased_songs','Purchased Songs','أغاني تم شراؤها','Gekochte liedjes','Chansons achetées','Gekaufte Lieder','Купленные песни','Canciones compradas','Satın Alınan Şarkılar','购买歌曲'),(225,'my_purchases','My Purchases','مشترياتي','Mijn aankopen','Mes achats','Meine Einkäufe','Мои покупки','Mis compras','Satın alımlarım','我的购买'),(226,'purchased_on','Purchased on','تم شراؤها على','Gekocht op','Acheté le','Gekauft am','Куплен на','Comprado en','Tarihinde satın alındı','购买日期'),(227,'purchased_your_song.','purchased your song.','اشتريت أغنيتك.','heb je nummer gekocht.','acheté votre chanson.','kaufte dein Lied.','купил вашу песню.','compré tu canción.','şarkını satın aldı.','购买你的歌。'),(229,'go_pro_to_download','Go PRO To Download','الذهاب للمحترفين للتحميل','Ga PRO om te downloaden','Go PRO Pour Télécharger','Zum Herunterladen gehen Sie auf PRO','Go PRO скачать','Ir PRO para descargar','Indirmek için PRO git','去专业版下载'),(230,'generating_waves..','Generating waves..','توليد الأمواج ..','Golven genereren ..','Générer des vagues ..','Wellen erzeugen ..','Генерация волн ..','Generando olas ..','Dalgalar üretiliyor ..','生成波. .'),(231,'name','Name','اسم','Naam','prénom','Name','название','Nombre','isim','名字'),(232,'your_full_name_as_showing_on_your_id','Your full name as showing on your ID','اسمك الكامل كما هو موضح على هويتك','Uw volledige naam wordt weergegeven op uw ID','Votre nom complet comme indiqué sur votre identifiant','Ihr vollständiger Name, wie auf Ihrer ID angegeben','Ваше полное имя показывается на вашем удостоверении личности','Su nombre completo como se muestra en su identificación','Kimliğinizde gösterildiği gibi tam adınız','您的全名显示在您的ID上'),(233,'upload_documents','Upload documents','تحميل الوثائق','Upload documenten','Télécharger des documents','Dokumente hochladen','Загрузить документы','Subir documentos','Belgeleri Yükle','上传文件'),(234,'please_upload_a_photo_with_your_passport___id___your_distinct_photo.','Please upload a photo with your passport / ID &amp; your distinct photo.','يرجى تحميل صورة مع جواز سفرك / معرفك &amp; ؛ صورتك المميزة.','Upload een foto met uw paspoort / ID &amp; jouw duidelijke foto.','Veuillez télécharger une photo avec votre passeport / ID &amp; votre photo distincte.','Bitte laden Sie ein Foto mit Ihrem Pass / ID &amp; Ihr unterschiedliches Foto.','Пожалуйста, загрузите фотографию с вашим паспортом / ID &amp; твое отличное фото.','Suba una foto con su pasaporte / ID &amp; tu foto distinta','Lütfen pasaportunuz / kimliğinizle bir fotoğraf yükleyin &amp; senin farklı fotoğrafın.','请上传一张带护照/身份证的照片和您的清晰照片。'),(235,'your_personal_photo','Your Personal Photo','صورتك الشخصية','Uw persoonlijke foto','Votre photo personnelle','Ihr persönliches Foto','Ваше личное фото','Tu foto personal','Kişisel Fotoğrafın','你的个人照片'),(236,'passport___id_card','Passport / ID card','جواز السفر / بطاقة الهوية','Paspoort / ID-kaart','Passeport / carte d\'identité','Reisepass / ID-Karte','Паспорт / удостоверение личности','Pasaporte / DNI','Pasaport / kimlik kartı','护照/身份证'),(237,'additional_details','Additional details','تفاصيل اضافية','Aanvullende details','Détails supplémentaires','Weitere Details','Дополнительные детали','Detalles adicionales','Ek detaylar','其他详细信息'),(238,'we_will_review_your_request_within_24_hours__you_ll_be_informed_shourtly.','We will review your request within 24 hours, you&#039;ll be informed shourtly.','سنقوم بمراجعة طلبك خلال 24 ساعة ، وسيتم إبلاغك فورًا.','We zullen uw verzoek binnen 24 uur beoordelen, u wordt op de hoogte gebracht.','Nous examinerons votre demande dans les 24 heures, vous en serez informé.','Wir werden Ihre Anfrage innerhalb von 24 Stunden prüfen. Sie werden schnell informiert.','Мы рассмотрим ваш запрос в течение 24 часов, вы будете проинформированы.','Revisaremos su solicitud dentro de las 24 horas, se le informará brevemente.','İsteğinizi 24 saat içinde inceleyeceğiz, titizlikle bilgilendirileceksiniz.','我们将在24小时内审查您的请求，我们会及时通知您。'),(240,'additional_details_about_your_self__optinal_','Additional details about your self (Optinal)','تفاصيل إضافية عن نفسك (Optinal)','Aanvullende informatie over jezelf (Optinal)','Détails supplémentaires sur vous-même (Optinal)','Zusätzliche Angaben zu Ihrer Person (Optinal)','Дополнительная информация о себе (Optinal)','Detalles adicionales sobre tu auto (Optinal)','Kendin hakkında ek ayrıntılar (Optinal)','有关您自己的其他详细信息（可选）'),(241,'website__optional_','Website (Optional)','صفحة انترنت (اختياري)','Website (optioneel)','Site Internet (optionnel)','Webseite (optional)','Веб-сайт (необязательно)','Sitio web (opcional)','Web sitesi (İsteğe bağlı)','网站（可选）'),(242,'thank_you._your_request_has_been_sent__we_will_get_back_to_you_shourtly.','Thank you. Your request has been sent, we will get back to you shourtly.','شكرا لكم. تم إرسال طلبك ، وسنقوم بالرد عليك بسرعة.','Dank je. Uw verzoek is verzonden, wij nemen u zo snel mogelijk terug.','Je vous remercie. Votre demande a été envoyée, nous vous répondrons dans les meilleurs délais.','Vielen Dank. Ihre Anfrage wurde gesendet, wir melden uns umgehend bei Ihnen.','Спасибо. Ваша заявка отправлена, мы ответим вам срочно.','Gracias. Su solicitud ha sido enviada, nos pondremos en contacto con usted en breve.','Teşekkür ederim. İsteğiniz gönderildi, size sert bir şekilde geri döneceğiz.','谢谢您。你的请求已经发出，我们会及时回复你。'),(243,'error_found_while_processing_your_request__please_try_again_later.','Error found while processing your request, please try again later.','تم العثور على خطأ أثناء معالجة طلبك ، يرجى المحاولة مرة أخرى لاحقًا.','Fout gevonden tijdens het verwerken van uw verzoek. Probeer het later opnieuw.','Une erreur a été détectée lors du traitement de votre demande. Veuillez réessayer ultérieurement.','Fehler beim Verarbeiten Ihrer Anfrage, bitte versuchen Sie es später erneut.','При обработке вашего запроса обнаружена ошибка. Повторите попытку позже.','Se ha encontrado un error al procesar su solicitud, inténtelo de nuevo más tarde.','İsteğiniz işlenirken hata oluştu, lütfen daha sonra tekrar deneyin.','处理您的请求时发现错误，请稍后再试。'),(244,'your_request_has_been_already_sent__we_will_get_back_to_you_shourtly.','Your request has been already sent, we will get back to you shourtly.','تم إرسال طلبك بالفعل ، وسنقوم بالرد عليك بسرعة.','Uw verzoek is al verzonden, we nemen u zo snel mogelijk terug.','Votre demande a déjà été envoyée, nous vous recontacterons dans les meilleurs délais.','Ihre Anfrage wurde bereits gesendet, wir melden uns umgehend bei Ihnen.','Ваша заявка уже отправлена, мы ответим вам срочно.','Su solicitud ya ha sido enviada, nos pondremos en contacto con usted en breve.','İsteğiniz zaten gönderildi, size sert bir şekilde geri döneceğiz.','Your request has been already sent, we will get back to you shourtly.'),(245,'get_verified__upload_more_songs__get_more_space__sell_your_songs__get_a_special_looking_profile_and_get_famous_on_our_platform_','Get verified, upload more songs, get more space, sell your songs, get a special looking profile and get famous on our platform!','الحصول على التحقق ، تحميل المزيد من الأغاني ، الحصول على المزيد من المساحة ، بيع أغانيك ، الحصول على ملف تعريف ذو مظهر خاص والحصول على شهرة على نظامنا الأساسي!','Verifieer, upload meer nummers, krijg meer ruimte, verkoop je liedjes, krijg een speciaal uitziend profiel en word beroemd op ons platform!','Faites-vous vérifier, téléchargez plus de chansons, gagnez de l\'espace, vendez vos chansons, obtenez un profil spécial et devenez célèbre sur notre plateforme!','Lassen Sie sich verifizieren, laden Sie mehr Songs hoch, erhalten Sie mehr Platz, verkaufen Sie Ihre Songs, erhalten Sie ein spezielles Profil und werden Sie auf unserer Plattform berühmt!','Пройдите проверку, загрузите больше песен, получите больше места, продайте свои песни, получите специальный профиль и станьте известным на нашей платформе!','¡Verifíquese, cargue más canciones, obtenga más espacio, venda sus canciones, obtenga un perfil de aspecto especial y sea famoso en nuestra plataforma!','Doğrulayın, daha fazla şarkı yükleyin, daha fazla alan kazanın, şarkılarınızı satın, özel bir görünüm elde edin ve platformumuzda ünlü olun!','获得验证，上传更多歌曲，获得更多空间，出售您的歌曲，获得一个个性简介，并在我们的平台上出名！'),(246,'play_all','Play All','لعب كل','Speel alles','Jouer à tous','Alle wiedergeben','Играть все','Jugar todo','Hepsini Oynat','播放全部'),(247,'latest_songs','Latest Songs','أحدث الأغاني','Laatste liedjes','Dernières chansons','Neueste Songs','Последние песни','Canciones más recientes','Son Şarkılar','最新歌曲'),(248,'special_songs','Special Songs','أغاني خاصة','Speciale liedjes','Chansons Spéciales','Spezielle Lieder','Специальные песни','Canciones especiales','Özel Şarkılar','特别歌曲'),(249,'top_songs','Top Songs','أفضل أغاني','Topnummers','Top chansons','Top-Songs','Лучшие песни','Mejores canciones','En Çok Okunan Şarkı Sözleri','歌曲排行榜'),(250,'similar_artists','Similar Artists','فنانون متشابهون','Gelijkaardige kunstenaars','Artistes similaires','ähnliche Künstler','Похожие исполнители','Artistas similares','benzer sanatçılar','类似的歌手'),(251,'artists','artists','الفنانين','kunstenaars','artistes','Künstler','художники','artistas','sanatçılar','歌手'),(252,'artist','artist','فنان','artiest','artiste','Künstler','художник','artista','sanatçı','艺人'),(253,'store','Store','متجر','Op te slaan','le magasin','Geschäft','хранить','Almacenar','mağaza','商店'),(254,'congratulations__your_request_to_become_an_artist_was_approved.','Congratulations! Your request to become an artist was approved.','تهانينا! تمت الموافقة على طلبك لتصبح فنانا.','Gefeliciteerd! Uw verzoek om artiest te worden is goedgekeurd.','Toutes nos félicitations! Votre demande de devenir artiste a été approuvée.','Herzliche Glückwünsche! Ihre Anfrage, Künstler zu werden, wurde genehmigt.','Поздравляем! Ваш запрос стать художником был одобрен.','¡Felicidades! Su solicitud para convertirse en un artista fue aprobada.','Tebrikler! Sanatçı olma isteğiniz onaylandı.','恭喜你!你想成为歌手的请求被批准了。'),(255,'sadly__your_request_to_become_an_artist_was_declined.','Sadly, Your request to become an artist was declined.','للأسف ، تم رفض طلبك لتصبح فنانا.','Helaas is je verzoek om artiest te worden afgewezen.','Malheureusement, votre demande de devenir artiste a été refusée.','Leider wurde Ihre Bitte, Künstler zu werden, abgelehnt.','К сожалению, ваша просьба стать художником была отклонена.','Lamentablemente, su solicitud para convertirse en un artista fue rechazada.','Ne yazık ki, sanatçı olma isteğiniz reddedildi.','很遗憾，你想成为歌手的请求被拒绝了。'),(256,'activities','Activities','أنشطة','Activiteiten','Activités','Aktivitäten','мероприятия','Ocupaciones','faaliyetler','活动'),(259,'re_post','Re Post','أعادة ارسال','opnieuw posten','republier','Umbuchen','репост','volver a publicar','Yeniden Gönder','重新发布'),(260,'the_song_was_successfully_shared_on_your_timeline.','The song was successfully shared on your timeline.','تمت مشاركة الأغنية بنجاح على الجدول الزمني الخاص بك.','Het nummer is met succes gedeeld op je tijdlijn.','La chanson a été partagée avec succès sur votre timeline.','Der Song wurde erfolgreich auf Ihrer Timeline geteilt.','Песня была успешно опубликована на вашей временной шкале.','La canción fue compartida con éxito en su línea de tiempo.','Şarkı zaman çizelgenizde başarıyla paylaşıldı.','歌曲已在您的时间表上成功共享。'),(261,'no_activties_found','No activties found','لم يتم العثور على الأنشطة','Geen activiteiten gevonden','Aucune activité trouvée','Keine Aktivitäten gefunden','Активности не найдены','No se han encontrado actividades.','Etkinlik bulunamadı','找不到任何活动'),(272,'delete_post','Delete Post','حذف آخر','Verwijder gepost bericht','Supprimer le message','Beitrag löschen','Удалить сообщение','Eliminar mensaje','Gönderiyi Sil','删除帖子'),(273,'no_more_activities_found','No more activities found','لم يتم العثور على المزيد من الأنشطة','Geen activiteiten meer gevonden','Aucune autre activité trouvée','Keine weiteren Aktivitäten gefunden','Больше никаких действий не найдено','No se han encontrado más actividades.','Başka etkinlik bulunamadı','没有找到更多活动'),(274,'weekly_top_tracks','Weekly Top Tracks','المسارات الأسبوعية الأعلى','Wekelijkse toptracks','Top titres hebdomadaires','Wöchentliche Top-Tracks','Лучшие треки за неделю','Top pistas semanales','Haftalık En Çok İzlenen Parçalar','每周热门歌曲'),(275,'delete_your_post','Delete your post','حذف مشاركتك','Verwijder je bericht','Supprimer votre post','Löschen Sie Ihren Beitrag','Удалить свой пост','Borra tu publicación','Yayınınızı silin','删除你的帖子'),(276,'are_you_sure_you_want_to_delete_this_post_','Are you sure you want to delete this post?','هل أنت متأكد أنك تريد حذف هذه المشاركة؟','Weet je zeker dat je dit bericht wilt verwijderen?','Es-tu sur de vouloir supprimer cette annonce?','Möchten Sie diesen Beitrag wirklich löschen?','Вы уверены, что хотите удалить эту запись?','¿Estás seguro de que quieres eliminar esta publicación?','Bu yayını silmek istediğinize emin misiniz?','你确定你要删除这个帖子？'),(277,'uploaded_a_new_song.','Uploaded a new song.','تم تحميل أغنية جديدة.','Een nieuw nummer ge-upload.','Téléchargé une nouvelle chanson.','Einen neuen Song hochgeladen.','Загрузил новую песню.','Subido una nueva canción.','Yeni bir şarkı yükledim.','上传了一首新歌。'),(278,'artists_to_follow','Artists to Follow','الفنانين لمتابعة','Kunstenaars om te volgen','Artistes à suivre','Künstler zu folgen','Исполнители, чтобы следовать','Artistas a seguir','İzlenecek Sanatçılar','要关注的艺术家'),(279,'likes','Likes','الإعجابات','sympathieën','Aime','Likes','Нравится','Gustos','Seviyor','喜欢'),(280,'plays','Plays','يلعب','Plays','Pièces','Theaterstücke','Пьесы','Obras de teatro','oynatır','播放'),(281,'no_favourite_tracks_found','No favourite tracks found','لم يتم العثور على المسارات المفضلة','Geen favoriete nummers gevonden','Aucune piste favorite trouvée','Keine Lieblingstitel gefunden','Любимые треки не найдены','No se encontraron pistas favoritas','Favori parça bulunamadı','没有找到最喜欢的歌曲'),(282,'my_favourites','My Favourites','المفضلة','Mijn favorieten','Mes préférés','Meine Favoriten','Мои любимые','Mis favoritos','Favorilerim','我最喜欢的'),(283,'you_currently_have__c__favourite_songs','You currently have |c| favourite songs','لديك حاليًا |c| الأغاني المفضلة','Je hebt momenteel |c| favoriete liedjes','Vous avez actuellement |c| chansons préférées','Sie haben derzeit |c| Lieblingslieder','У вас есть |c| любимые песни','Actualmente tienes |c| canciones favoritas','Şu anda |c| favori şarkılar','你现在有 |c| 最喜欢的歌曲'),(285,'you_currently_have__c__playlists.','You currently have |c| playlists.','لديك حاليًا |c| قوائم التشغيل.','Je hebt momenteel |c| afspeellijsten.','Vous avez actuellement |c| listes de lecture.','Sie haben derzeit |c| Wiedergabelisten.','У вас есть |c| плейлисты.','Actualmente tienes |c| playlists','Şu anda |c| çalma listeleri.','您当前有 |c| 歌单。'),(286,'create','Create','خلق','creëren','Créer','Erstellen','Создайте','Crear','yaratmak','创建'),(287,'create_playlist','Create Playlist','إنشاء قائمة التشغيل','Maak afspeellijst','Créer une playlist','Wiedergabeliste erstellen','Создать плейлист','Crear lista de reproducción','Oynatma listesi yarat','创建歌单'),(288,'playlist_name','Playlist name','اسم قائمة التشغيل','Naam afspeellijst','Nom de la playlist','Playlistenname','Название плейлиста','Nombre de la lista de reproducción','Oynatma listesi adı','歌单名称'),(289,'error_found_while_uploading_the_playlist_avatar__please_try_again_later.','Error found while uploading the playlist avatar, Please try again later.','تم العثور على خطأ أثناء تحميل الصورة الرمزية لقائمة التشغيل ، يرجى المحاولة مرة أخرى لاحقًا.','Fout gevonden tijdens het uploaden van de avatar van de afspeellijst. Probeer het later opnieuw.','Une erreur a été détectée lors du téléchargement de l’avatar de la liste de lecture. Veuillez réessayer ultérieurement.','Fehler beim Hochladen des Wiedergabelisten-Avatars gefunden. Bitte versuchen Sie es später erneut.','При загрузке аватара плейлиста обнаружена ошибка. Повторите попытку позже.','Se ha encontrado un error al cargar el avatar de la lista de reproducción. Inténtalo de nuevo más tarde.','Oynatma listesi avatarını yüklerken hata bulundu, Lütfen daha sonra tekrar deneyin.','上传歌单封面时发现错误，请稍后重试。'),(291,'edit_playlist','Edit Playlist','تحرير قائمة التشغيل','Bewerk afspeellijst','Editer la playlist','Wiedergabeliste bearbeiten','Изменить плейлист','Editar lista de reproducción','Oynatma Listesini Düzenle','编辑歌单'),(292,'delete_playlist','Delete Playlist','حذف قائمة التشغيل','Verwijder afspeellijst','Supprimer la playlist','Playlist löschen','Удалить плейлист','Eliminar lista de reproducción','Oynatma Listesini Sil','删除歌单'),(293,'delete_your_playlist','Delete your playlist','حذف قائمة التشغيل الخاصة بك','Verwijder je afspeellijst','Supprimer votre playlist','Löschen Sie Ihre Playlist','Удалить свой плейлист','Eliminar tu lista de reproducción','Oynatma listenizi silin','删除歌单'),(294,'are_you_sure_you_want_to_delete_this_playlist_','Are you sure you want to delete this playlist?','هل أنت متأكد من أنك تريد حذف قائمة التشغيل هذه؟','Weet je zeker dat je deze afspeellijst wilt verwijderen?','Êtes-vous sûr de vouloir supprimer cette playlist?','Möchten Sie diese Playlist wirklich löschen?','Вы уверены, что хотите удалить этот плейлист?','¿Estás seguro de que deseas eliminar esta lista de reproducción?','Bu oynatma listesini silmek istediğinize emin misiniz?','您确定要删除此歌单吗？'),(295,'share_this_playlist','Share this Playlist','مشاركة قائمة التشغيل هذه','Deel deze afspeellijst','Partager cette playlist','Teilen Sie diese Playlist','Поделиться этим плейлистом','Comparte esta lista de reproducción','Bu Oynatma Listesini paylaş','分享此歌单'),(296,'play','Play','لعب','Spelen','Jouer','abspielen','Играть','Jugar','Oyun','播放'),(297,'no_songs_on_this_playlist.','No songs on this playlist.','لا توجد أغاني في قائمة التشغيل هذه.','Geen nummers in deze afspeellijst.','Aucune chanson sur cette playlist.','Keine Songs auf dieser Playlist.','В этом плейлисте нет песен.','No hay canciones en esta lista de reproducción.','Bu çalma listesinde şarkı yok.','歌单中没有歌曲。'),(298,'no_songs_on_this_playlist_yet.','No songs on this playlist yet.','لا توجد أغاني في قائمة التشغيل هذه حتى الآن.','Nog geen nummers in deze afspeellijst.','Aucune chanson sur cette playlist pour le moment.','Keine Songs in dieser Playlist.','В этом плейлисте еще нет песен.','No hay canciones en esta lista de reproducción todavía.','Bu çalma listesinde henüz şarkı yok.','此歌单上还没有歌曲。'),(299,'select_playlists','Select playlists','حدد قوائم التشغيل','Selecteer afspeellijsten','Sélectionner des playlists','Wiedergabelisten auswählen','Выберите плейлисты','Seleccionar listas de reproducción','Oynatma listelerini seç','选择歌单'),(300,'add','Add','إضافة','Toevoegen','Ajouter','Hinzufügen','добавлять','Añadir','Eklemek','创建'),(301,'please_select_which_playlist_you_want_to_add_this_song_to.','Please select which playlist you want to add this song to.','الرجاء تحديد قائمة التشغيل التي تريد إضافة هذه الأغنية إليها.','Selecteer alstublieft aan welke afspeellijst u deze song wilt toevoegen.','Veuillez sélectionner la playlist à laquelle vous souhaitez ajouter cette chanson.','Bitte wählen Sie die Playlist aus, zu der Sie diesen Song hinzufügen möchten.','Пожалуйста, выберите, в какой плейлист вы хотите добавить эту песню.','Seleccione la lista de reproducción a la que desea agregar esta canción.','Lütfen bu şarkıyı hangi şarkıya eklemek istediğinizi seçin.','请选择要添加此歌曲的歌单。'),(302,'no_playlists_found','No playlists found','لم يتم العثور على قوائم التشغيل','Geen afspeellijsten gevonden','Aucune liste de lecture trouvée','Keine Wiedergabelisten gefunden','Плейлисты не найдены','No se encontraron listas de reproducción','Oynatma listesi bulunamadı','找不到歌单'),(303,'new','New','الجديد','nieuwe','Nouveau','Neu','новый','Nuevo','Yeni','新'),(304,'no_more_playlists_found','No more playlists found','لم يتم العثور على المزيد من قوائم التشغيل','Geen afspeellijsten meer gevonden','Plus de playlists trouvées','Keine weiteren Wiedergabelisten gefunden','Больше не найдено плейлистов','No se encontraron más listas de reproducción','Daha fazla oynatma listesi bulunamadı','找不到更多歌单'),(305,'discover','Discover','اكتشف','Ontdekken','Découvrir','Entdecken','Обнаружить','Descubrir','keşfedin','发现音乐'),(306,'show_all','Show All','عرض الكل','Toon alles','Montre tout','Zeige alles','Показать все','Mostrar todo','Hepsini Göster ↓','显示所有'),(307,'new_releases','New Releases','الإصدارات الجديدة','Nieuwe uitgaven','Nouvelles versions','Neue Veröffentlichungen','Новые релизы','Nuevos lanzamientos','Yeni sürümler','最新发布'),(308,'most_popular_this_week','Most Popular This Week','الأكثر شعبية هذا الأسبوع','Meest populair deze week','Le plus populaire cette semaine','Am beliebtesten diese Woche','Самые популярные на этой неделе','Más popular esta semana','Bu Hafta En Popüler','本周最热门'),(309,'most_recommended','Most Recommended','الأكثر الموصى بها','Meest aanbevolen','Le plus recommandé','Am meisten empfohlen','Самые рекомендуемые','Más recomendado','En çok önerilen','最佳推荐'),(310,'recommended','Recommended','موصى به','Aanbevolen','conseillé','Empfohlen','рекомендуемые','Recomendado','Tavsiye edilen','推荐'),(311,'new_music','New Music','موسيقى جديدة','Nieuwe muziek','Nouvelle musique','Neue Musik','Новая музыка','Música nueva','Yeni müzik','最新音乐'),(312,'best_new_releases','Best New Releases','أفضل الإصدارات الجديدة','Beste nieuwe releases','Meilleures nouveautés','Beste Neuerscheinungen','Лучшие новые релизы','Mejores lanzamientos nuevos','En İyi Yeni Çıkanlar','优秀新音乐'),(313,'latest_music','Latest Music','أحدث الموسيقى','Nieuwste muziek','Dernières musiques','Neueste Musik','Последняя музыка','La ultima musica','En Son Müzik','最新音乐'),(315,'top_music','Top Music','الموسيقى كبار','Topmuziek','Top musique','Top-Musik','Топ Музыка','La mejor música','En İyi Müzik','热门音乐'),(316,'see_all','See All','اظهار الكل','Alles zien','Voir tout','Alles sehen','Увидеть все','Ver todo','Tümünü Gör','查看全部'),(317,'top_albums','Top Albums','أفضل الألبومات','Topalbums','Top Albums','Top-Alben','Лучшие альбомы','Top albumes','En İyi Albümler','热门专辑'),(318,'top','Top','أعلى','Top','Haut','oben','верхний','Parte superior','Üst','Top'),(319,'top_50','Top 50','أعلى 50','Top 50','Top 50','Top 50','50 лучших','Top 50','En iyi 50','Top 50'),(320,'browse_music','Browse Music','استعراض الموسيقي','Muziek doorzoeken','Parcourir la musique','Musik durchsuchen','Просмотр музыки','Buscar música','Müziğe Göz At','浏览音乐'),(321,'genres','Genres','الجنس الأدبي','Genres','Les genres','Genres','Жанры','Géneros','Türler','音乐类型'),(322,'your_music','Your Music','الموسيقى الخاصة بك','Jouw muziek','Ta musique','Deine Musik','Ваша музыка','Tu musica','Senin müziğin','你的音乐'),(323,'latest_songs_in','Latest Songs In','أحدث الأغاني في','Laatste liedjes in','Dernières chansons en','Neueste Songs in','Последние песни в','Últimas canciones en','Son Şarkılar','最新的歌曲'),(324,'age_restricted_track','Age restricted track','العمر مقيد المسار','Leeftijd beperkte track','Piste d\'âge restreint','Altersbeschränkte Spur','Возрастная дорожка','Pista de edad restringida','Yaş kısıtlamalı parkur','年龄限制的音乐'),(325,'this_track_is_age_restricted_for_viewers_under__18','This track is age restricted for viewers under +18','هذا المسار مقيد بحسب العمر للمشاهدين الذين تقل أعمارهم عن 18 عامًا','Deze track is leeftijdsbeperkend voor kijkers onder +18','Ce titre est réservé aux moins de 18 ans.','Dieser Titel ist für Zuschauer unter 18 Jahren altersbeschränkt','Этот трек ограничен по возрасту для зрителей до +18','Esta pista tiene restricciones de edad para los espectadores menores de 18 años.','Bu parça +18 yaşın altındaki izleyiciler için yaş sınırlaması var','这首歌的年龄限制在18岁以上'),(326,'create_an_account_or_login_to_confirm_your_age.','Create an account or login to confirm your age.','إنشاء حساب أو تسجيل الدخول لتأكيد عمرك.','Maak een account aan of log in om uw leeftijd te bevestigen.','Créez un compte ou connectez-vous pour confirmer votre âge.','Erstellen Sie ein Konto oder melden Sie sich an, um Ihr Alter zu bestätigen.','Создайте аккаунт или войдите, чтобы подтвердить свой возраст.','Crea una cuenta o inicia sesión para confirmar tu edad.','Yaşınızı onaylamak için bir hesap oluşturun veya giriş yapın.','创建帐户或登录以确认您的年龄。'),(327,'this_track_is_age_restricted_for_viewers_under_18','This track is age restricted for viewers under 18','هذا المسار مقيد بحسب العمر للمشاهدين دون سن 18 عامًا','Deze track is leeftijdsbeperkend voor kijkers onder de 18 jaar','Ce titre est réservé aux moins de 18 ans.','Dieser Titel ist für Zuschauer unter 18 Jahren altersbeschränkt','Этот трек ограничен по возрасту для зрителей младше 18 лет','Esta pista tiene restricciones de edad para los espectadores menores de 18 años.','Bu parça 18 yaşın altındaki izleyiciler için yaş sınırlıdır','该音乐的年龄限制为18岁以上的观众'),(328,'upgrade_to_pro','Upgrade To PRO','التطور للاحترافية','Upgraden naar Pro','Passer à Pro','Upgrade auf PRO','Обновление до PRO','Actualizar a PRO','Pro\'ya yükselt','升级到专业版'),(329,'go_pro_','Go Pro!','الذهاب برو!','Ga Pro!','Go Pro!','Gehen Sie Pro!','Go Pro!','¡Vaya Pro!','Git Pro!','Go Pro!'),(330,'discover_more_features_with_our_premium_package_','Discover more features with our Premium package!','اكتشف المزيد من الميزات مع باقة Premium الخاصة بنا!','Ontdek meer functies met ons Premium-pakket!','Découvrez plus de fonctionnalités avec notre forfait Premium!','Entdecken Sie weitere Funktionen mit unserem Premium-Paket!','Откройте для себя больше возможностей с нашим пакетом Premium!','¡Descubre más características con nuestro paquete Premium!','Premium paketimizle daha fazla özellik keşfedin!','使用我们的高级套餐了解更多功能！'),(331,'free_plan','Free Plan','خطة مجانية','Gratis abonnement','Plan gratuit','Kostenloser Plan','Бесплатный план','Plan gratis','Ücretsiz Plan','免费套餐'),(332,'upload_songs_up_to','Upload songs up to','تحميل الأغاني تصل إلى','Upload nummers tot','Télécharger des chansons jusqu\'à','Songs hochladen bis','Загрузить песни до','Sube canciones hasta','Şarkıları en fazla yükle','上传歌曲大小'),(333,'pro_badge','Pro badge','شارة الموالية','Pro-badge','Badge pro','Pro Abzeichen','Про значок','Insignia pro','Pro rozeti','专业标志'),(334,'download_songs','Download songs','تحميل اغاني','Liedjes downloaden','Télécharger des chansons','Songs herunterladen','Скачать песни','Descargar canciones','Şarkı indir','下载歌曲'),(335,'turn_off_comments_download','Turn off comments/download','قم بإيقاف تشغيل التعليقات / التنزيل','Schakel opmerkingen / downloaden uit','Désactiver les commentaires / télécharger','Kommentare deaktivieren / herunterladen','Отключить комментарии / скачать','Desactivar comentarios / descargar','Yorumları kapat / indir','关闭评论/下载'),(336,'current_plan','Current Plan','الخطه الحاليه','Huidige plan','Plan actuel','Derzeitiger Plan','Текущий план','Plan actual','Mevcut Plan','当前套餐'),(337,'pro_plan','Pro Plan','خطة الموالية','Pro Plan','Plan Pro','Pro Plan','Pro План','Pro Plan','Pro Plan','专业套餐'),(338,'per_month','per month','كل شهر','per maand','par mois','pro Monat','в месяц','por mes','her ay','每月'),(339,'p_month','p/month','ص / شهر','p / maand','p / mois','p / Monat','р / месяц','p / mes','p / ay','p/月'),(340,'monthly','monthly','شهريا','maandelijks','mensuel','monatlich','ежемесячно','mensual','aylık','每月一次'),(341,'upload_unlimited_songs','Upload unlimited songs','تحميل الأغاني غير محدودة','Upload onbeperkte nummers','Télécharger des chansons illimitées','Lade unbegrenzt Songs hoch','Загрузить неограниченное количество песен','Sube canciones ilimitadas','Sınırsız şarkı yükle','上传无限歌曲'),(342,'upgrade','Upgrade','تطوير','Upgrade','Améliorer','Aktualisierung','Обновить','Mejorar','Yükselt','升级'),(343,'secured_payment_transaction','Secured payment transaction','معاملة الدفع المضمونة','Beveiligde betalingstransactie','Opération de paiement sécurisée','Gesicherte Zahlungstransaktion','Защищенная платежная операция','Transacción de pago garantizado','Güvenli ödeme işlemi','担保支付交易'),(344,'redirecting..','Redirecting..','إعادة توجيه..','Wordt omgeleid ..','Redirection ..','Umleiten ..','Перенаправление ..','Redireccionando ..','Yönlendiriliyor ..','重定向..'),(345,'oops__an_error_found.','Oops, an error found.','عفوًا ، تم العثور على خطأ.','Oeps, er is een fout gevonden.','Oups, une erreur trouvée.','Ups, ein Fehler wurde gefunden.','К сожалению, ошибка найдена.','Vaya, se ha encontrado un error.','Hata! Bir hata bulundu.','糟糕，发现错误。'),(346,'you_are_a_pro_','You are a pro!','أنت محترف!','Je bent een pro!','Tu es un pro!','Du bist ein Profi!','Вы профессионал!','Eres un profesional!','Sen bir profesyonelsin!','你是专业版！'),(347,'unexpected_error_found_while_processing_your_payment__please_try_again_later.','Unexpected error found while processing your payment, please try again later.','تم العثور على خطأ غير متوقع أثناء معالجة دفعتك ، يرجى إعادة المحاولة لاحقًا.','Onverwachte fout gevonden tijdens het verwerken van uw betaling, probeer het later opnieuw.','Une erreur inattendue a été détectée lors du traitement de votre paiement. Veuillez réessayer ultérieurement.','Bei der Verarbeitung Ihrer Zahlung wurde ein unerwarteter Fehler gefunden. Bitte versuchen Sie es später erneut.','При обработке вашего платежа обнаружена непредвиденная ошибка, повторите попытку позже.','Se ha encontrado un error inesperado al procesar su pago. Inténtelo de nuevo más tarde.','Ödemeniz işlenirken beklenmeyen bir hata bulundu, lütfen daha sonra tekrar deneyin.','处理支付时发现意外错误，请稍后再试。'),(348,'payment_error','Payment Error','خطأ الدفع','Betalingsfout','Erreur de paiement','Zahlungsfehler','Ошибка платежа','Error en el pago','Ödeme hatası','支付错误'),(353,'you_have_reached_your_upload_limit___link__to_upload_unlimited_songs.','You have reached your upload limit, |link| to upload unlimited songs.','لقد وصلت إلى الحد الأقصى للتحميل ، |link| لتحميل الأغاني غير محدودة.','Je hebt je uploadlimiet bereikt, |link| om onbeperkte nummers te uploaden.','Vous avez atteint votre limite de téléchargement, |link| télécharger des chansons illimitées.','Sie haben Ihr Upload-Limit erreicht, |link| unbegrenzt Songs hochladen.','Вы достигли лимита загрузки, |link| загружать неограниченное количество песен.','Has alcanzado tu límite de subida, |link| para subir canciones ilimitadas.','Yükleme sınırınıza ulaştınız, |link| sınırsız şarkı yüklemek için.','您已达到上传限制, |link| 上传无限歌曲。'),(354,'get_verified__sell_your_songs__get_a_special_looking_profile_and_get_famous_on_our_platform_','Get verified, sell your songs, get a special looking profile and get famous on our platform!','الحصول على التحقق ، بيع أغانيك ، والحصول على ملف تعريف خاص المظهر والحصول على شهرة على منصة لدينا!','Wordt geverifieerd, verkoop je liedjes, krijg een speciaal uitziend profiel en word beroemd op ons platform!','Faites-vous vérifier, vendez vos chansons, obtenez un profil spécial et devenez célèbre sur notre plateforme!','Lassen Sie sich verifizieren, verkaufen Sie Ihre Songs, erhalten Sie ein speziell aussehendes Profil und werden Sie auf unserer Plattform berühmt!','Пройдите проверку, продайте свои песни, получите специальный профиль и станьте известным на нашей платформе!','¡Verifíquese, venda sus canciones, obtenga un perfil especial y vuélvase famoso en nuestra plataforma!','Doğrulayın, şarkılarınızı satın, özel bir görünüme sahip olun ve platformumuzda ünlü olun!','获得验证，出售您的歌曲，获得一个特殊的外观，并在我们的平台上出名！'),(355,'pro_memeber','PRO Member','عضو محترف','PRO-lid','Membre PRO','PRO Mitglied','PRO Член','Miembro PRO','PRO Üyesi','专业版会员'),(356,'manage_my_songs','Manage My Songs','إدارة أغانيي','Beheer mijn liedjes','Gérer mes chansons','Meine Songs verwalten','Управляй моими песнями','Manejar mis canciones','Şarkılarımı Yönet','管理我的歌曲'),(357,'published','Published','نشرت','Gepubliceerd','Publié','Veröffentlicht','опубликованный','Publicado','Yayınlanan','发布时间'),(358,'total_songs','Total Songs','مجموع الأغاني','Totaal aantal nummers','Nombre total de chansons','Songs insgesamt','Всего песен','Canciones totales','Toplam şarkı','总歌曲'),(359,'total_plays','Total Plays','مجموع المسرحيات','Totale spelen','Nombre total de lectures','Gesamtanzahl der Spiele','Всего игр','Jugadas totales','Toplam oyun','总播放量'),(360,'total_downloads','Total Downloads','إجمالي التنزيلات','Totaal aantal downloads','Total des téléchargements','Downloads insgesamt','Всего загрузок','Descargas totales','Toplam indirme','总下载量'),(361,'total_sales','Total Sales','إجمالي المبيعات','Totale verkoop','Ventes totales','Gesamtumsatz','Тотальная распродажа','Ventas totales','Toplam satış','总销售额'),(362,'total_sales_this_month','Total Sales This Month','إجمالي المبيعات هذا الشهر','Totale omzet deze maand','Total des ventes ce mois-ci','Gesamtumsatz in diesem Monat','Всего продаж в этом месяце','Total de ventas este mes','Bu Ayın Toplam Satışı','本月总销售额'),(363,'total_sales_this_today','Total Sales This Today','إجمالي المبيعات هذا اليوم','Totale omzet dit vandaag','Total des ventes aujourd\'hui','Gesamtumsatz heute','Общий объем продаж сегодня','Ventas Totales Este Hoy','Bugünün Toplam Satışı','今天的总销售量'),(364,'total_sales_today','Total Sales Today','إجمالي المبيعات اليوم','Totale verkoop vandaag','Total des ventes aujourd\'hui','Gesamtumsatz heute','Всего продаж сегодня','Ventas totales hoy','Toplam Satış Bugün','今日总销售额'),(365,'downloads','Downloads','التنزيلات','downloads','Téléchargements','Downloads','Загрузки','Descargas','İndirilenler','下载'),(366,'sales','Sales','مبيعات','verkoop','Ventes','Der Umsatz','Продажи','Ventas','Satış','销售'),(367,'most_played_songs','Most played songs','معظم الأغاني لعبت','Meest gespeelde nummers','Les chansons les plus jouées','Meist gespielte Lieder','Самые популярные песни','Las canciones más jugadas','En çok çalınan şarkılar','大多数人都在播放'),(368,'no_songs_found','No songs found','لا توجد أغاني','Geen nummers gevonden','Aucune chanson trouvée','Keine Songs gefunden','Песни не найдены','No se encontraron canciones','Şarkı bulunamadı','没有发现歌曲'),(369,'most_commented_songs','Most commented songs','معظم الأغاني علق','Meest besproken nummers','La plupart des chansons commentées','Meist kommentierte Songs','Самые комментируемые песни','Canciones más comentadas','En çok yorum yapılan şarkılar','评论最多的歌曲'),(370,'most_liked_songs','Most liked songs','معظم الأغاني يحب','De meesten vonden nummers leuk','Les chansons les plus aimées','Meistgeliebte Lieder','Самые любимые песни','Las canciones mas gustadas','En çok sevilen şarkılar','最喜欢的歌曲'),(371,'most_downloaded_songs','Most downloaded songs','معظم الأغاني التي تم تنزيلها','Meest gedownloade nummers','Les chansons les plus téléchargées','Die meisten heruntergeladenen Songs','Самые скачиваемые песни','Las canciones más descargadas','En çok indirilen şarkılar','下载最多的歌曲'),(372,'recent_sales','Recent sales','المبيعات الأخيرة','Recente verkopen','Ventes récentes','Letzte Verkäufe','Недавние продажи','Ventas recientes','Son satışlar','近期销售额'),(373,'no_sales_found','No sales found','لا توجد مبيعات','Geen verkopen gevonden','Aucune vente trouvée','Kein Umsatz gefunden','Продажи не найдены','No se encontraron ventas','Satış bulunamadı','没有销售额'),(374,'listened_by','Listened by','استمع بواسطة','Geluisterd door','Écouté par','Gehört von','Слушал','Escuchado por','Tarafından dinlendi','听了'),(375,'recently_listened_by','Recently Listened by','استمع مؤخرا من قبل','Onlangs beluisterd door','Récemment écouté par','Kürzlich gehört von','Недавно прослушал','Recientemente escuchado por','Son Dinleyenler','最近听的'),(376,'songs_i_liked','Songs I Liked','أغاني أعجبتني','Liedjes die ik leuk vond','Chansons que j\'ai aimé','Songs, die ich mochte','Песни, которые мне понравились','Canciones que me gustaron','Sevdiğim Şarkılar','我喜欢的歌曲'),(377,'block','Block','منع','Blok','Bloc','Block','блок','Bloquear','Blok','Block'),(378,'are_you_sure_you_want_to_block_this_user','Are you sure you want to block this user','هل أنت متأكد أنك تريد حظر هذا المستخدم','Weet je zeker dat je deze gebruiker wilt blokkeren?','Êtes vous sûr de vouloir bloquer cet utilisateur','Möchten Sie diesen Benutzer wirklich blockieren?','Вы уверены, что хотите заблокировать этого пользователя','Estás seguro de que quieres bloquear a este usuario','Bu kullanıcıyı engellemek istediğinize emin misiniz?','您确定要阻止此用户吗？'),(379,'unblock','Unblock','رفع الحظر','deblokkeren','Débloquer','Blockierung aufheben','открыть','Desatascar','engeli kaldırmak','解除封锁'),(380,'blocked_users','Blocked Users','مستخدمين محجوبين','Geblokkeerde gebruikers','Utilisateurs bloqués','Blockierte Benutzer','Заблокированные пользователи','Usuarios bloqueados','Engellenmiş kullanıcılar','被阻止的用户'),(381,'no_blocked_users_found','No blocked users found','لم يتم العثور على المستخدمين المحظورين','Geen geblokkeerde gebruikers gevonden','Aucun utilisateur bloqué trouvé','Keine blockierten Benutzer gefunden','Заблокированные пользователи не найдены','No se encontraron usuarios bloqueados','Engellenen kullanıcı bulunamadı','没有阻止的用户'),(382,'album_title','Album Title','عنوان الألبوم','Album titel','Titre de l\'album','Albumtitel','Название альбома','Título del álbum','Albüm başlığı','专辑标题'),(383,'your_album_title__2_-_55_characters','Your album title, 2 - 55 characters','عنوان الألبوم الخاص بك ، 2 - 55 حرفا','De titel van je album, 2 - 55 tekens','Le titre de votre album, 2 - 55 caractères','Ihr Albumtitel, 2 - 55 Zeichen','Название вашего альбома, 2 - 55 символов','El título de tu álbum, 2 - 55 caracteres.','Albümünüzün başlığı, 2 - 55 karakter','您的专辑标题，2-55个字符'),(384,'album_description','Album Description','وصف الألبوم','Albumbeschrijving','Description de l\'album','Beschreibung des Albums','Описание альбома','Descripción del Album','Albüm Açıklaması','专辑描述'),(385,'album_price','Album Price','سعر الألبوم','Albumprijs','Prix ​​de l\'album','Albumpreis','Цена альбома','Precio del album','Albüm Fiyatı','专辑价格'),(386,'add_song','Add Song','أضف أغنية','Voeg lied toe','Ajouter une chanson','Song hinzufügen','Добавить песню','Añadir canción','Şarkı ekle','添加歌曲'),(387,'successfully_uploaded','Successfully uploaded','تم الرفع بنجاح','Succesvol geüpload','Téléchargé avec succès','Erfolgreich hochgeladen','Успешно загружено','Cargado con éxito','Başarıyla yüklendi','成功上传'),(388,'album_thumbnail_is_required.','Album thumbnail is required.','مطلوب صورة الألبوم.','Er is een miniatuur van het album vereist.','La vignette de l\'album est requise.','Album-Miniaturansicht ist erforderlich.','Требуется миниатюра альбома.','Se requiere la miniatura del álbum.','Albüm küçük resmi gereklidir.','专辑封面是必需的。'),(389,'your_album_was_successfully_created__please_wait..','Your album was successfully created, please wait..','تم إنشاء ألبومك بنجاح ، يرجى الانتظار ..','Je album is succesvol gemaakt, even geduld aub ..','Votre album a été créé avec succès, veuillez patienter ..','Ihr Album wurde erfolgreich erstellt. Bitte warten Sie ..','Ваш альбом был успешно создан, пожалуйста, подождите ..','Su álbum fue creado exitosamente, por favor espere ..','Albümünüz başarıyla oluşturuldu, lütfen bekleyin ..','您的专辑已成功创建，请稍候..'),(390,'album_title_is_required.','Album title is required.','عنوان الألبوم مطلوب.','Albumtitel is verplicht.','Le titre de l\'album est requis.','Albumtitel sind erforderlich.','Название альбома обязательно.','El título del álbum es obligatorio.','Albüm başlığı gerekli.','专辑标题是必需的。'),(391,'album_description_is_required.','Album description is required.','وصف الألبوم مطلوب.','Albumbeschrijving is verplicht.','La description de l\'album est obligatoire.','Die Beschreibung des Albums ist erforderlich.','Требуется описание альбома.','Se requiere la descripción del álbum.','Albüm açıklaması gerekli.','专辑说明是必需的。'),(392,'are_you_sure_you_want_to_delete_this_song_','Are you sure you want to delete this song?','هل أنت متأكد أنك تريد حذف هذه الأغنية؟','Weet je zeker dat je dit nummer wilt verwijderen?','Êtes-vous sûr de vouloir supprimer cette chanson?','Möchten Sie diesen Song wirklich löschen?','Вы уверены, что хотите удалить эту песню?','¿Seguro que quieres borrar esta canción?','Bu şarkıyı silmek istediğinize emin misiniz?','你确定要删除这首歌吗？'),(393,'top_50_albums','Top 50 Albums','أفضل 50 ألبومات','Top 50 albums','Top 50 albums','Top 50 Alben','50 лучших альбомов','Top 50 de los álbumes','En İyi 50 Albüm','前50的专辑'),(394,'no_songs_on_this_album_yet.','No songs on this album yet.','لا توجد أغاني في هذا الألبوم حتى الآن.','Nog geen nummers op dit album.','Aucune chanson sur cet album pour le moment.','Noch keine Songs auf diesem Album.','В этом альбоме пока нет песен.','No hay canciones en este álbum todavía.','Bu albümde henüz şarkı yok.','这张专辑还没有歌曲。'),(395,'you_may_also_like','You may also like','ربما يعجبك أيضا','Dit vind je misschien ook leuk','Tu pourrais aussi aimer','Sie können auch mögen','Вам также может понравиться','También te puede interesar','Şunlar da hoşunuza gidebilir','您可能还喜欢'),(396,'your_album_was_successfully_updated__please_wait..','Your album was successfully updated, please wait..','تم تحديث ألبومك بنجاح ، يرجى الانتظار ..','Uw album is succesvol bijgewerkt, even geduld aub ..','Votre album a été mis à jour avec succès, veuillez patienter ..','Ihr Album wurde erfolgreich aktualisiert. Bitte warten Sie ..','Ваш альбом был успешно обновлен, пожалуйста, подождите ..','Su álbum fue actualizado con éxito, por favor espere ...','Albümünüz başarıyla güncellendi, lütfen bekleyin ..','您的专辑已成功更新，请稍候。'),(397,'in_album_','in album:','في الألبوم:','in album:','dans l\'album:','in album:','в альбоме:','en el álbum:','albümde:','在专辑:'),(398,'delete_your_album','Delete your album','احذف ألبومك','Verwijder je album','Supprimer votre album','Löschen Sie Ihr Album','Удалить свой альбом','Borra tu álbum','Albümünü sil','删除你的专辑'),(399,'are_you_sure_you_want_to_delete_this_album_','Are you sure you want to delete this album?','هل أنت متأكد أنك تريد حذف هذا الألبوم؟','Weet je zeker dat je dit album wilt verwijderen?','Êtes-vous sûr de vouloir supprimer cet album?','Möchten Sie dieses Album wirklich löschen?','Вы уверены, что хотите удалить этот альбом?','¿Estás seguro de que quieres eliminar este álbum?','Bu albümü silmek istediğinden emin misin?','你确定要删除这张专辑吗?'),(400,'yes__but_keep_the_songs','Yes, But Keep The Songs','نعم ، لكن احتفظ بالأغاني','Ja, maar houd de liedjes','Oui, mais gardez les chansons','Ja, aber behalten Sie die Lieder','Да, но сохраняй песни','Sí, pero conserva las canciones','Evet, Ama Şarkıları Sakla','是的，但是保留这些歌'),(401,'yes__delete_everything','Yes, Delete Everything','نعم ، احذف كل شيء','Ja, verwijder alles','Oui, tout effacer','Ja, alles löschen','Да, Удалить все','Si, eliminar todo','Evet, Her Şeyi Sil','是的,删除一切'),(402,'my_songs','My Songs','أغنيتي','Mijn liedjes','Mes chansons','Meine Lieder','Мои песни','Mis canciones','Benim Şarkılarım','我的歌曲'),(403,'my_albums','My Albums','ألبوماتي','Mijn albums','Mes albums','Meine Alben','Мои альбомы','Mis albumes','Albümlerim','我的专辑'),(404,'create_copyright_dmca_take_down_notice','Create copyright DMCA take down notice','إنشاء حقوق الطبع والنشر DMCA إنزال إشعار','Maak een auteursrechtvermelding voor DMCA-verwijderingen','Créer un copyright DMCA prendre note','Erstellen Sie Copyright-DMCA-Hinweisbenachrichtigung','Создать авторское право DMCA снять уведомление','Crear derechos de autor DMCA quitar aviso','Telif hakkı DMCA oluştur aşağı bildirimde bulunmak','创建版权DMCA记录通知'),(405,'report_coopyright','Report Copyright','تقرير حقوق الطبع والنشر','Meld Copyright','Signaler le droit d\'auteur','Copyright melden','Сообщить об авторском праве','Informe de derechos de autor','Rapor Telif Hakkı','报告版权'),(406,'report_copyright','Report Copyright','تقرير حقوق الطبع والنشر','Meld Copyright','Signaler le droit d\'auteur','Copyright melden','Сообщить об авторском праве','Informe de derechos de autor','Rapor Telif Hakkı','报告版权'),(407,'create_dmca_take_down_notice','Create DMCA take down notice','إنشاء DMCA إنزال إشعار','Maak een DMCA-kennisgeving voor verwijdering','Créer un avis de DMCA','Erstellen Sie eine DMCA-Benachrichtigung','Создать DMCA снять уведомление','Crear aviso de eliminación de DMCA','DMCA oluşturma bildirimi al','创建版权法取消通知'),(408,'i_have_a_good_faith_belief_that_use_of_the_copyrighted_work_described_above_is_not_authorized_by_the_copyright_owner__its_agent_or_the_law','I have a good faith belief that use of the copyrighted work described above is not authorized by the copyright owner, its agent or the law','لدي اعتقاد حسن النية بأن استخدام العمل المحمي بحقوق الطبع والنشر الموضح أعلاه غير مصرح به من قبل مالك حقوق الطبع والنشر أو وكيله أو القانون','Ik ben er te goeder trouw van overtuigd dat het gebruik van het auteursrechtelijk beschermde werk zoals hierboven beschreven niet is geautoriseerd door de eigenaar van het auteursrecht, diens vertegenwoordiger of de wet','J\'ai la conviction de bonne foi que l\'utilisation de l\'œuvre protégée par le droit d\'auteur décrite ci-dessus n\'est pas autorisée par le titulaire du droit d\'auteur, son agent ou la loi','Ich bin fest davon überzeugt, dass die Verwendung des oben beschriebenen urheberrechtlich geschützten Werks nicht vom Inhaber des Urheberrechts, seinem Vertreter oder dem Gesetz genehmigt wird','Я добросовестно полагаю, что использование авторских прав, описанных выше, не разрешено владельцем авторских прав, его агентом или законом','Creo de buena fe que el uso del trabajo con derechos de autor descrito anteriormente no está autorizado por el propietario de los derechos de autor, su agente o la ley','Yukarıda açıklanan telif hakkı alınmış çalışmanın kullanımının telif hakkı sahibi, vekili veya yasa tarafından yetkilendirilmediğine dair iyi bir inancım var.','我确信使用上述受版权保护的作品未经版权所有者，其代理人或法律授权'),(409,'i_confirm_that_i_am_the_copyright_owner_or_am_authorised_to_act_on_behalf_of_the_owner_of_an_exclusive_right_that_is_allegedly_infringed.','I confirm that I am the copyright owner or am authorised to act on behalf of the owner of an exclusive right that is allegedly infringed.','أؤكد أنني مالك حقوق الطبع والنشر أو مفوض بالتصرف نيابة عن مالك الحق الحصري المزعوم انتهاكه.','Ik bevestig dat ik de eigenaar van het auteursrecht ben of bevoegd ben om namens de eigenaar van een exclusief recht te handelen dat mogelijk is geschonden.','Je confirme que je suis le titulaire du droit d\'auteur ou que je suis autorisé à agir pour le compte du titulaire d\'un droit exclusif prétendument violé.','Ich bestätige, dass ich der Inhaber des Urheberrechts bin oder befugt bin, im Namen des Inhabers eines ausschließlichen Rechts zu handeln, das angeblich verletzt wird.','Я подтверждаю, что являюсь владельцем авторских прав или уполномочен действовать от имени владельца исключительного права, которое предположительно нарушено.','Confirmo que soy el propietario de los derechos de autor o que estoy autorizado para actuar en nombre del propietario de un derecho exclusivo que presuntamente se ha infringido.','Telif hakkı sahibinin ya da ihlal edildiği iddia edilen münhasır bir hak sahibi adına hareket etmeye yetkili olduğumu onaylarım.','我确认我是版权所有人，或被授权代表据称被侵犯的专有权所有人。'),(410,'submit','Submit','خضع','voorleggen','Soumettre','einreichen','Отправить','Enviar','Gönder','提交'),(411,'please_describe_your_request_carefully_and_as_much_as_you_can__note_that_false_dmca_requests_can_lead_to_account_termination.','Please describe your request carefully and as much as you can, note that false DMCA requests can lead to account termination.','يرجى وصف طلبك بعناية وبقدر ما تستطيع ، لاحظ أن طلبات قانون الألفية الجديدة لحقوق طبع ونشر المواد الرقمية يمكن أن تؤدي إلى إنهاء الحساب.','Beschrijf uw verzoek zorgvuldig en voorzover mogelijk, kunnen valse DMCA-verzoeken leiden tot beëindiging van uw account.','Veuillez décrire votre demande avec soin et, dans la mesure du possible, notez que les fausses demandes DMCA peuvent entraîner la fermeture du compte.','Bitte beschreiben Sie Ihre Anfrage sorgfältig und so oft Sie können. Beachten Sie, dass falsche DMCA-Anfragen zur Kontoauflösung führen können.','Пожалуйста, опишите ваш запрос внимательно и как можно больше, обратите внимание, что ложные запросы DMCA могут привести к удалению аккаунта.','Describa su solicitud con cuidado y tanto como pueda, tenga en cuenta que las solicitudes falsas de DMCA pueden llevar a la cancelación de la cuenta.','Lütfen isteğinizi dikkatlice ve mümkün olduğunca açıklayın, yanlış DMCA taleplerinin hesabınızın feshine yol açabileceğini unutmayın.','请尽可能详细地描述您的请求，请注意，错误的版权违法请求可能导致封号。'),(412,'please_describe_your_request.','Please describe your request.','يرجى وصف طلبك.','Beschrijf alstublieft uw verzoek.','S\'il vous plaît décrire votre demande.','Bitte beschreiben Sie Ihre Anfrage.','Пожалуйста, опишите ваш запрос.','Por favor describa su solicitud.','Lütfen isteğinizi tanımlayın.','请描述您的要求。'),(413,'please_select_the_checkboxs_below_if_you_own_the_copyright.','Please select the checkboxs below if you own the copyright.','يرجى تحديد مربعات الاختيار أدناه إذا كنت تملك حقوق الطبع والنشر.','Selecteer de selectievakjes hieronder als u de eigenaar bent van het auteursrecht.','Veuillez cocher les cases ci-dessous si vous possédez le droit d\'auteur.','Bitte wählen Sie die folgenden Kontrollkästchen aus, wenn Sie das Urheberrecht besitzen.','Пожалуйста, установите флажки ниже, если вы являетесь владельцем авторских прав.','Por favor, seleccione las casillas de verificación a continuación si posee los derechos de autor.','Telif hakkına sahipseniz lütfen aşağıdaki onay kutularını seçin.','如果您拥有版权，请选中下面的多选框。'),(414,'spotlight','Spotlight','ضوء كشاف','Spotlight','Projecteur','Scheinwerfer','Прожектор','Destacar','spot','我关注的'),(415,'no_spotlight_tracks_found','No spotlight tracks found','لم يتم العثور على مسارات الأضواء','Geen spotlighttracks gevonden','Aucune piste de projecteur trouvée','Keine Spotlight-Spuren gefunden','Не найдено ни одного трека','No se encontraron pistas de foco','Spot ışığı bulunamadı','没有找到关注的音乐'),(416,'spotlight_your_songs','Spotlight your songs','تسليط الضوء على أغانيك','Breng je nummers in de schijnwerpers','Mettez en lumière vos chansons','Bringen Sie Ihre Songs zum Leuchten','Подчеркните свои песни','Destaca tus canciones','Şarkılarınızı gösterin','关注你的歌曲'),(417,'spotlight_your_songs__feature_','Spotlight your songs (feature)','تسليط الضوء على أغانيك (ميزة)','Breng uw liedjes onder de aandacht (functie)','Mettez en lumière vos chansons (fonctionnalité)','Spotlight deine Songs (Funktion)','Подчеркните свои песни (функция)','Destacar tus canciones (característica)','Şarkılarınızı vurgulayın (özellik)','关注你的歌曲（专题）'),(418,'spotlight_your_songs__featured_','Spotlight your songs (featured)','تسليط الضوء على أغانيك (مميزة)','Breng je liedjes onder de aandacht (aanbevolen)','Mettez en lumière vos chansons (en vedette)','Bringen Sie Ihre Songs zum Vorschein (vorgestellt)','Подчеркните свои песни (лучшее)','Destaca tus canciones (destacadas)','Şarkılarınızı gösterin (özellikli)','聚焦你的歌曲（精选）'),(419,'embed','Embed','تضمين','insluiten','Intégrer','Einbetten','встраивать','Empotrar','Göm','插入'),(420,'browse','Browse','تصفح','Blader','Feuilleter','Durchsuche','Просматривать','Vistazo','Araştır','随便看看'),(421,'no_songs_found_on_this_store.','No songs found on this store.','لم يتم العثور على أغاني في هذا المتجر.','Geen nummers gevonden in deze winkel.','Aucune chanson trouvée sur ce magasin.','Keine Songs in diesem Shop gefunden.','В этом магазине не найдено ни одной песни.','No se encontraron canciones en esta tienda.','Bu mağazada şarkı bulunamadı.','这家商店没有找到歌曲。'),(422,'top_seller','Top Seller','أعلى بائع','Top verkoper','Meilleur vendeur','Topseller','Лучший продавец','Mejor vendedor','En çok satan','最畅销'),(423,'no_more_followers_found','No more followers found','لم يتم العثور على المزيد من المتابعين','Geen volgers meer gevonden','Aucun autre abonné trouvé','Keine weiteren Anhänger gefunden','Подписчики больше не найдены','No se han encontrado más seguidores.','Başka takipçi bulunamadı','没有发现更多的粉丝'),(424,'no_followers_found','No followers found','لم يتم العثور على متابعين','Geen volgers gevonden','Aucun abonné trouvé','Keine Follower gefunden','Подписчики не найдены','No se encontraron seguidores','Takipçi bulunamadı','找不到关注者'),(425,'no_more_following_found','No more following found','لا مزيد من التالية وجدت','Niet meer volgen gevonden','Aucun autre suivi trouvé','Keine weiteren gefunden','Следующие не найдены','No se han encontrado más seguidores.','Başka takip bulunamadı','找不到更多'),(426,'no_following_found','No following found','لم يتم العثور على التالي','Geen volgend gevonden','Aucune suite trouvée','Kein folgendes gefunden','Следующие не найдены','No se han encontrado los siguientes','Hiçbir takip bulunamadı','没有找到以下内容'),(427,'is_pro_user','Is Pro user','هو برو المستخدم','Is Pro-gebruiker','Est l\'utilisateur Pro','Ist Pro-Benutzer','Про профессионал','Es usuario pro','Pro kullanıcısı','是专业版用户'),(428,'verified','Verified','التحقق','geverifieerd','Vérifié','Verifiziert','проверенный','Verificado','Doğrulanmış','已证实的'),(429,'pro_user','Pro user','برو المستخدم','Pro gebruiker','Utilisateur pro','Pro Benutzer','Про пользователь','Usuario pro','Pro kullanıcı','专业版用户'),(430,'normal_user','Normal user','مستخدم عادي','Normale gebruiker','Utilisateur normal','Normaler Benutzer','Обычный пользователь','Usuario normal','Normal kullanıcı','普通用户'),(431,'unverified','Unverified','غير مثبت عليه','geverifieerde','Non vérifié','Nicht verifiziert','непроверенный','Inconfirmado','doğrulanmamış','未验证'),(432,'featured','Featured','متميز','Uitgelicht','En vedette','Vorgestellt','Рекомендуемые','Destacados','Öne çıkan','精选'),(433,'yes','Yes','نعم فعلا','Ja','Oui','Ja','да','Sí','Evet','是'),(434,'no','No','لا','Nee','Non','Nein','нет','No','Yok hayır','否'),(435,'like_comment','Like Comment','مثل تعليق','Vind ik leuk Reactie','Comme commentaire','Wie Kommentar','Лайкнуть комментарий','Me gusta comentar','Beğen Yorum','喜欢评论'),(436,'liked_your_comment.','liked your comment.','أعجبك تعليقك.','vond je reactie leuk','aimé votre commentaire.','mochte dein Kommentar.','понравился твой комментарий.','Me gustó tu comentario.','yorumunu beğendim.','喜欢你的评论。'),(437,'unlike_comment','UnLike Comment','على عكس التعليق','UnLike commentaar','UnLike Comment','Unähnlicher Kommentar','UnLike Комментарий','No te gusta comentar','Yorum beğenmek','不喜欢评论'),(438,'report_comment.','Report comment.','الإبلاغ عن تعليق.','Rapporteer commentaar.','Signaler un commentaire.','Kommentar melden','Пожаловаться на комментарий.','Reportar comentario.','Yorum bildir.','报告发表评论。'),(439,'please_describe_whey_you_want_to_report_this_comment.','Please describe whey you want to report this comment.','يرجى وصف مصل اللبن الذي تريد الإبلاغ عن هذا التعليق.','Geef een beschrijving van wei waarin u deze opmerking wilt melden.','Veuillez décrire le lactosérum pour lequel vous souhaitez signaler ce commentaire.','Bitte beschreiben Sie, wann Sie diesen Kommentar melden möchten.','Пожалуйста, опишите, почему вы хотите сообщить об этом комментарии.','Por favor describa si quiere reportar este comentario.','Lütfen bu yorumu bildirmek istediğiniz peynir altı suyunu açıklayın.','请说明您要报告此评论的原因。'),(440,'unreport_comment','UnReport Comment','إلغاء تقرير التعليق','Reactie annuleren','Unporter un commentaire','Kommentar nicht melden','Отменить комментарий','UnReport Comment','Yorumun Raporunu Kaldır','取消报告评论'),(441,'the_comment_report_was_successfully_deleted.','The comment report was successfully deleted.','تم حذف تقرير التعليق بنجاح.','Het commentaarrapport is succesvol verwijderd.','Le rapport de commentaire a été supprimé avec succès.','Der Kommentarbericht wurde erfolgreich gelöscht.','Сообщение о комментарии успешно удалено.','El informe de comentarios fue eliminado con éxito.','Yorum raporu başarıyla silindi.','评论报告已成功删除。'),(442,'unreport','Un Report','تقرير الامم المتحدة','VN rapport','Un rapport','Un-Bericht','отчитаться','Un informe','Raporun Kaldırılması','一份报告'),(443,'the_track_report_was_successfully_deleted.','The track report was successfully deleted.','تم حذف تقرير المسار بنجاح.','Het trackrapport is succesvol verwijderd.','Le rapport de suivi a été supprimé avec succès.','Der Streckenbericht wurde erfolgreich gelöscht.','Отчет о треке был успешно удален.','El informe de seguimiento se ha eliminado correctamente.','Parça raporu başarıyla silindi.','曲目报告被成功删除。'),(444,'track_comment.','Track comment.','تتبع التعليق.','Volg commentaar.','Suivre le commentaire.','Kommentar verfolgen','Отслеживать комментарии.','Seguir el comentario.','Yorum takip et.','曲目评论。'),(445,'please_describe_whey_you_want_to_report_this_track.','Please describe whey you want to report this track.','يرجى وصف مصل اللبن الذي تريد الإبلاغ عن هذا المسار.','Beschrijf aub de wei die u deze track wilt melden.','Veuillez décrire le lactosérum que vous souhaitez signaler sur cette piste.','Bitte beschreiben Sie, wann Sie diesen Track melden möchten.','Пожалуйста, опишите, почему вы хотите сообщить об этом треке.','Describa por qué quiere informar de esta pista.','Lütfen bu parçayı bildirmek istediğiniz peynir altı suyunu açıklayın.','请说明您要报告此曲目的原因。'),(446,'report_track.','Report track.','تقرير المسار.','Verslag bijhouden.','Rapport de piste.','Bericht verfolgen','Сообщить трек.','Informe de seguimiento.','İzi rapor et.','报告曲目。'),(447,'results_for_','results for:','نتائج لـ:','resultaten voor:','résultats pour:','Ergebnisse für:','результаты для:','resultados para:','için sonuçlar:','结果为:'),(448,'what_are_looking_for__','What are looking for ?','عن ماذا تبحث ؟','Waar ben je naar opzoek ?','Que recherchons-nous?','Was suchst du ?','Что ищете?','Lo que están buscando ?','Neye bakıyorsunuz ?','在寻找什么？'),(449,'no_more_artists_found','No more artists found','لم يتم العثور على المزيد من الفنانين','Geen artiesten meer gevonden','Pas plus d\'artistes trouvés','Keine weiteren Künstler gefunden','Художники больше не найдены','No se han encontrado más artistas.','Başka sanatçı bulunamadı','没有更多的歌手了'),(450,'no_more_albums_found','No more albums found','لم يتم العثور على المزيد من الألبومات','Geen albums meer gevonden','Aucun autre album trouvé','Keine weiteren Alben gefunden','Больше не найдено альбомов','No se han encontrado más álbumes.','Başka albüm bulunamadı','没有找到专辑'),(452,'please_wait','please_wait','ارجوك انتظر','even geduld aub','S\'il vous plaît, attendez','Warten Sie mal','подождите пожалуйста','por favor espera','lütfen bekle','请耐心等待'),(454,'admin_panel','Admin Panel','لوحة الادارة','Administratie Paneel','panneau d\'administration','Administrationsmenü','Панель администратора','Panel de administrador','Admin Paneli','管理面板'),(455,'no_messages_found_channel','no messages found channel','لم يتم العثور على رسائل قناة','geen berichten gevonden kanaal','aucun message trouvé channel','Keine Nachrichten gefunden Kanal','Канал сообщений не найден','canal no encontrado','kanal bulunamadı','未找到消息'),(456,'search','search','بحث','zoeken','chercher','Suche','поиск','buscar','arama','搜索'),(457,'write_message','Write message','اكتب رسالة','Schrijf een bericht','Écrire un message','Nachricht schreiben','Напиши сообщение','Escribe un mensaje','Mesaj Yaz','写信息'),(458,'are_you_sure_you_want_delete_chat','Are you sure you want delete chat','هل أنت متأكد أنك تريد حذف الدردشة','Weet je zeker dat je de chat wilt verwijderen?','Êtes-vous sûr de vouloir supprimer le chat?','Möchten Sie den Chat wirklich löschen?','Вы уверены, что хотите удалить чат','¿Estás seguro de que quieres eliminar el chat?','Sohbeti silmek istediğinize emin misiniz?','您确定要删除聊天吗？'),(459,'messages','messages','رسائل','berichten','messages','Mitteilungen','Сообщения','mensajes','mesajları','消息'),(460,'no_messages_were_found__please_choose_a_channel_to_chat.','No messages were found, please choose a channel to chat.','لم يتم العثور على رسائل ، يرجى اختيار قناة للدردشة.','Er zijn geen berichten gevonden. Kies een kanaal om te chatten.','Aucun message n\'a été trouvé, veuillez choisir un canal pour discuter.','Es wurden keine Nachrichten gefunden. Bitte wählen Sie einen Kanal zum Chatten.','Сообщения не найдены, пожалуйста, выберите канал для чата.','No se encontraron mensajes, por favor elige un canal para chatear.','Mesaj bulunamadı, lütfen sohbet etmek için bir kanal seçin.','未找到任何消息，请选择要聊天的好友。'),(461,'no_messages_were_found__say_hi_','No messages were found, say Hi!','لم يتم العثور على رسائل ، قل مرحبا!','Er zijn geen berichten gevonden, bijvoorbeeld Hallo!','Aucun message n\'a été trouvé, dites bonjour!','Es wurden keine Nachrichten gefunden, sagen Sie Hallo!','Сообщения не найдены, скажи привет!','No se encontraron mensajes, saludos!','Mesaj bulunamadı, merhaba deyin!','找不到消息，打个招呼！'),(462,'load_more_messages','Load more messages','تحميل المزيد من الرسائل','Laad meer berichten','Charger plus de messages','Laden Sie weitere Nachrichten','Загрузить больше сообщений','Cargar mas mensajes','Daha fazla mesaj yükle','加载更多信息'),(463,'message','message','رسالة','bericht','message','Botschaft','сообщение','mensaje','mesaj','信息'),(464,'no_match_found','No match found','لا يوجد تطابق','Geen overeenkomst gevonden','Pas de résultat trouvé','Keine Übereinstimmung gefunden','Совпадение не найдено','No se encontraron coincidencias','Eşleşme bulunamadı','没有发现匹配'),(465,'buy','Buy','يشترى','Kopen','Acheter','Kaufen','купить','Comprar','satın almak','购买'),(466,'you_have_bought_this_album.','You have bought this album.','لقد اشتريت هذا الألبوم.','Je hebt dit album gekocht.','Vous avez acheté cet album.','Du hast dieses Album gekauft.','Вы купили этот альбом.','Usted ha comprado este álbum.','Bu albümü satın aldınız.','你买了这张专辑。'),(467,'price_range','Price range','نطاق السعر','Prijsklasse','Échelle des prix','Preisklasse','Ценовой диапазон','Rango de precios','Fiyat aralığı','价格范围'),(468,'you_have_bought_this_track.','You have bought this track.','لقد اشتريت هذا المسار.','Je hebt deze track gekocht.','Vous avez acheté cette piste.','Sie haben diesen Titel gekauft.','Вы купили этот трек.','Has comprado esta canción.','Bu parçayı aldın.','你已经买了这首歌。'),(469,'no_more_albums','No more albums','لا مزيد من الألبومات','Geen albums meer','Plus d\'albums','Keine weiteren Alben','Больше нет альбомов','No mas discos','Başka albüm yok','没有更多的专辑'),(470,'no_more_songs','No more songs','لا مزيد من الأغاني','Geen nummers meer','Pas plus de chansons','Keine Lieder mehr','Больше нет песен','No mas canciones','Başka şarkı yok','没有更多的歌曲'),(471,'no_results_found','No results found','لا توجد نتائج','geen resultaten gevonden','Aucun résultat trouvé','keine Ergebnisse gefunden','результаты не найдены','No se han encontrado resultados','Sonuç bulunamadı','未找到结果'),(472,'no_albums_found','No albums found','لم يتم العثور على ألبومات','Geen albums gevonden','Aucun album trouvé','Keine Alben gefunden','Альбомы не найдены','No se encontraron álbumes','Albüm bulunamadı','没有找到专辑'),(473,'send_as_message','Send as message','إرسال كرسالة','Verzend als bericht','Envoyer comme message','Als Nachricht senden','Отправить как сообщение','Enviar como mensaje','Mesaj olarak gönder','发送消息'),(474,'add_a_maximum_of_10_friends_and_send_them_this_track','Add a maximum of 10 friends and send them this track','أضف 10 أصدقاء كحد أقصى وأرسلهم إلى هذا المسار','Voeg maximaal 10 vrienden toe en stuur ze deze track','Ajoutez un maximum de 10 amis et envoyez-leur ce morceau','Füge maximal 10 Freunde hinzu und sende ihnen diesen Track','Добавьте максимум 10 друзей и отправьте им этот трек','Agrega un máximo de 10 amigos y envíales esta pista','En fazla 10 arkadaş ekle ve bu parçayı gönder','最多添加10个朋友并将其发送给他们'),(475,'get_started','Get Started','البدء','Begin','Commencer','Loslegen','Начать','Empezar','Başlamak','开始使用'),(476,'message_sent_successfully','Message sent successfully','تم إرسال الرسالة بنجاح','Bericht succesvol verzonden','message envoyé avec succès','Nachricht erfolgreich gesendet','Сообщение успешно отправлено','Mensaje enviado con éxito','Mesaj başarıyla gönderildi','消息成功发送'),(477,'password_is_too_short','Password is too short','كلمة المرور قصيرة جدا','Wachtwoord is te kort','Le mot de passe est trop court','Das Passwort ist zu kurz','Пароль слишком короткий','La contraseña es demasiado corta','Şifre çok kısa','密码太短'),(478,'username_length_must_be_between_5___32','Username length must be between 5 / 32','يجب أن يتراوح طول اسم المستخدم بين 5/32','De lengte van de gebruikersnaam moet tussen 5/32 zijn','La longueur du nom d\'utilisateur doit être comprise entre 5/32','Die Länge des Benutzernamens muss zwischen 5/32 liegen','Длина имени пользователя должна быть между 5/32','La longitud del nombre de usuario debe estar entre 5/32','Kullanıcı adı uzunluğu 5/32 arasında olmalıdır','用户名长度必须介于5/32之间'),(479,'invalid_username_characters','Invalid username characters','أحرف اسم المستخدم غير صالحة','Ongeldige gebruikersnaamtekens','Nom d\'utilisateur invalide','Ungültige Zeichen für den Benutzernamen','Неверные символы имени пользователя','Caracteres de usuario inválidos','Geçersiz kullanıcı adı karakterleri','用户名字符无效'),(480,'this_e-mail_is_invalid','This e-mail is invalid','هذا البريد الإلكتروني غير صالح','Deze email is ongeldig','Cette adresse email est invalide','Diese E-Mail ist nicht gültig','Этот адрес электронной почты недействителен','Este email es invalido','Bu email geçersizdir','此邮箱无效'),(481,'you_ain_t_logged_in_','You ain&#039;t logged in!','لم تقم بتسجيل الدخول!','U bent niet ingelogd!','Vous n\'êtes pas connecté!','Du bist nicht eingeloggt!','Вы не авторизованы!','Usted no ha iniciado sesión!','Giriş yapmadınız!','你没有登录!'),(482,'invalid_user_id','Invalid user ID','هوية مستخدم غير صالحه','Ongeldige gebruikersnaam','Identifiant invalide','Ungültige Benutzer-Id','Неверный идентификатор пользователя','Identificación de usuario inválida','Geçersiz kullanıcı kimliği','无效的用户ID'),(483,'no_new_releases_found','No new releases found','لم يتم العثور على إصدارات جديدة','Geen nieuwe releases gevonden','Aucune nouvelle version trouvée','Keine neuen Versionen gefunden','Новые выпуски не найдены','No se han encontrado nuevos lanzamientos.','Yeni sürüm bulunamadı','找不到新音乐'),(484,'light_mode','Light mode','وضع الضوء','Lichtmodus','Mode lumière','Lichtmodus','Легкий режим','Modo de luz','Işık modu','灯光模式'),(485,'____date___name_','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|','© |DATE| |NAME|'),(486,'chat','Chat','دردشة','babbelen','Bavarder','Plaudern','чат','Charla','Sohbet','交谈'),(487,'from_now','from now','من الان','vanaf nu','à partir de maintenant','in','отныне','desde ahora','şu andan itibaren','从现在开始'),(488,'any_moment_now','any moment now','في اي لحظة الان','elk moment nu','n\'importe quel moment maintenant','jeden Moment jetzt','в любой момент','cualquier momento ahora','her an şimdi','现在随时都可以'),(489,'about_a_minute_ago','about a minute ago','منذ دقيقة واحدة','ongeveer een minuut geleden','Il y a environ une minute','vor ungefähr einer Minute','около минуты назад','hace alrededor de un minuto','yaklaşık bir dakika önce','大约一分钟前'),(490,'_d_minutes_ago','%d minutes ago','منذ %d دقائق','%d minuten geleden','il y %d minutes','vor %d Minuten','%d минут назад','Hace %d minutos','%d dakika önce','%d 分钟前'),(491,'about_an_hour_ago','about an hour ago','منذ ساعة تقريبا','ongeveer een uur geleden','il y a à peu près une heure','vor ungefähr einer Stunde','около часа назад','Hace aproximadamente una hora','yaklaşık bir saat önce','大约一小时前'),(492,'_d_hours_ago','%d hours ago','منذ %d ساعة','%d uur geleden','il y a %d heures','Vor %d Stunden','%d часов назад','Hace %d horas','%d saat önce','%d 小时前'),(493,'a_day_ago','a day ago','قبل يوم واحد','een dag geleden','il y a un jour','vor einem Tag','день назад','Hace un día','bir gün önce','一天前'),(494,'_d_days_ago','%d days ago','منذ %d أيام','%d dagen geleden','il y a %d jours','vor %d Tagen','%d дней назад','Hace %d días','%d gün önce','%d 天前'),(495,'about_a_month_ago','about a month ago','منذ شهر تقريبا','ongeveer een maand geleden','il y a environ un mois','vor ungefähr einem Monat','Около месяца назад','Hace más o menos un mes','yaklaşık bir ay önce','大约一个月前'),(496,'_d_months_ago','%d months ago','منذ %d أشهر','%d maanden geleden','il y a %d mois','vor %d Monaten','%d месяцев назад','Hace %d meses','%d ay önce','%d 月前'),(497,'about_a_year_ago','about a year ago','قبل نحو سنة','ongeveer een jaar geleden','il y a un an à peu près','vor etwa einem Jahr','около года назад','Hace un año','yaklaşık bir yıl önce','大约一年前'),(498,'_d_years_ago','%d years ago','%d سنوات مضت','%d jaar geleden','il y a %d années','Vor %d Jahren','%d лет назад','hace %d años','%d yıl önce','%d 年前'),(504,'no_data_to_show','No data to show','لا توجد بيانات لإظهارها','Geen gegevens om te tonen','Aucune donnée à afficher','Keine Daten zum Anzeigen','Нет данных для отображения','No hay datos para mostrar','Gösterilecek veri yok','没有数据显示'),(505,'listen_to_songs','Listen to Songs','اسمع الاغاني','Luister naar liedjes','Ecouter des chansons','Lieder hören','Слушать песни','Escuchar canciones','Şarkıları dinle','听歌曲'),(506,'discover__stream__and_share_a_constantly_expanding_mix_of_music_from_emerging_and_major_artists_around_the_world.','Discover, stream, and share a constantly expanding mix of music from emerging and major artists around the world.','اكتشف مجموعة من الموسيقى المتنامية باستمرار من فنانين ناشئين وكبار في جميع أنحاء العالم ، وقم بدفقها ومشاركتها.','Ontdek, stream en deel een constant groeiende mix van muziek van opkomende en grote artiesten over de hele wereld.','Découvrez, diffusez en streaming et partagez un mélange sans cesse croissant de musique d\'artistes émergents et majeurs du monde entier.','Entdecken, streamen und teilen Sie eine ständig wachsende Mischung aus Musik von aufstrebenden und bedeutenden Künstlern auf der ganzen Welt.','Откройте для себя, транслируйте и делитесь постоянно растущим миксом музыки от начинающих и крупных артистов по всему миру.','Descubra, transmita y comparta una mezcla de música en constante expansión de artistas emergentes e importantes de todo el mundo.','Dünyadaki yeni ve büyük sanatçıların sürekli genişleyen bir müzik karışımını keşfedin, yayınlayın ve paylaşın.','发现、传播和分享来自世界各地新兴和主要歌手的不断扩大的音乐组合。'),(507,'signup_now','Signup Now','أفتح حساب الأن','Registreer nu','S\'inscrire maintenant','Jetzt registrieren','Войти Сейчас','Regístrate ahora','Şimdi kayıt ol','立即注册'),(508,'explore','Explore','يكتشف','onderzoeken','Explorer','Erkunden','Проводить исследования','Explorar','keşfetmek','发现音乐'),(509,'listen_music_everywhere__anytime','Listen Music Everywhere, Anytime','استمع إلى الموسيقى في كل مكان وفي أي وقت','Luister overal en altijd muziek','Écouter de la musique partout, à tout moment','Hören Sie überall und jederzeit Musik','Слушайте музыку везде, в любое время','Escuchar música en cualquier lugar, en cualquier momento','Her Zaman, Her Yerde Müzik Dinle','随时随地听音乐'),(510,'lorem_ipsum_dolor_sit_amet__consectetur_adipiscing_elit__sed_do_eiusmod_tempor_incididunt_ut_labore_et_dolore_magna_aliqua.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Lorem ipsum dolor sit amet، consectetur adipiscing elit، sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Lorem ipsum dolor sit amet, consectetur elit adipiscing, sed eiusmod tempor incidid ut labore et dolore magna aliqua.','Lorem ipsum dolor sitzen amet, consectetur adipiscing elit, sed do eiusmod temporary incididunt ut labore und dolore magna aliqua.','Lorem Ipsum Dolor Sit Amet, Concetetur Adipiscing Elit, Sed do EiusMod Tempor Incididunt U Labore et Dolore Magna Aliqua.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Lorem ipsum dolor amet sitet, adipiscing elit elit, sed do eiusmod tempor inci labunt ve dolore magna aliqua incididunt.','洛雷姆·伊普索姆·多尔坐在阿米特的座位上，他是一位敬业的精英，他用自己的节奏煽动着劳勃和多洛尔·马格纳·阿利夸尔。'),(511,'create_playlists_with_any_song__on-the-go','Create Playlists with any song, On-The-Go','إنشاء قوائم التشغيل مع أي أغنية ، على الحركة والتنقل','Maak afspeellijsten met elk nummer, On-the-Go','Créez des listes de lecture avec n\'importe quelle chanson, On-The-Go','Erstellen Sie Wiedergabelisten mit einem beliebigen Song, On-The-Go','Создание плейлистов с любой песней, On-The-Go','Crea listas de reproducción con cualquier canción, On-The-Go','On-The-Go ile herhangi bir şarkıyla Çalma listeleri oluşturma','随时随地用任何歌曲创建歌单'),(512,'top_trending_artists','Top Trending Artists','كبار الفنانين تتجه','Top trending artiesten','Artistes les plus en vogue','Top Trending Künstler','Лучшие тренды художников','Los mejores artistas de tendencias','En Popüler Sanatçılar','热门歌手'),(513,'calling_all_creators','Calling all creators','استدعاء جميع المبدعين','Oproep aan alle makers','Appel à tous les créateurs','Alle Schöpfer anrufen','Обращение ко всем создателям','Llamando a todos los creadores','Tüm yaratıcıları aramak','召集所有创造者'),(514,'get_on__0__to_connect_with_fans__share_your_sounds__and_grow_your_audience.','Get on {0} to connect with fans, share your sounds, and grow your audience.','احصل على {0} للتواصل مع المعجبين ومشاركة الأصوات وزيادة عدد جمهورك.','Ga op {0} om contact te maken met fans, deel uw geluiden en laat uw publiek groeien.','Accédez au {0} pour entrer en contact avec les fans, partager vos sons et développer votre audience.','Holen Sie sich auf {0}, um sich mit Fans zu verbinden, Ihre Sounds zu teilen und Ihr Publikum zu vergrößern.','Зайдите на {0}, чтобы общаться с фанатами, делиться своими звуками и расширять аудиторию.','Súbete a {0} para conectarte con los fanáticos, compartir tus sonidos y aumentar tu audiencia.','Hayranlarla bağlantı kurmak, seslerinizi paylaşmak ve izleyicilerinizi büyütmek için {0} alın.','上 {0} 与粉丝交流，分享你的声音，增加你的观众。'),(515,'upload_songs','Upload Songs','تحميل الأغاني','Songs uploaden','Télécharger des chansons','Songs hochladen','Загрузить песни','Subir canciones','Şarkı Yükle','上传歌曲'),(516,'check_stats','Check Stats','تحقق الإحصائيات','Controleer statistieken','Vérifier les statistiques','Statistiken überprüfen','Проверить статистику','Comprobar estadísticas','İstatistikleri kontrol et','检查统计数据'),(517,'ready_to_rock_your_world.','Ready to rock your world.','على استعداد لهز العالم الذي تعيشون فيه.','Klaar om je wereld te rocken.','Prêt à basculer votre monde.','Bereit, deine Welt zu rocken.','Готов раскачивать свой мир.','Listo para sacudir tu mundo.','Dünyanızı sarsmaya hazır.','准备好摇滚你的世界。'),(518,'search_for_artists__tracks','Search for artists, tracks','البحث عن الفنانين والمسارات','Zoeken naar artiesten, nummers','Rechercher des artistes, des pistes','Suche nach Künstlern, Tracks','Поиск артистов, треков','Búsqueda de artistas, pistas','Sanatçı, parça ara','搜索歌手，歌曲'),(520,'day_mode','Day mode','وضع اليوم','Dagmodus','Mode jour','Tagesmodus','Дневной режим','Modo día','Gün modu','白天模式'),(521,'night_mode','Night mode','وضع الليل','Nachtstand','Mode nuit','Nacht-Modus','Ночной режим','Modo nocturno','Gece modu','黑夜模式'),(522,'interest','Interest','فائدة','Interesseren','Intérêt','Interesse','Интерес','Interesar','Faiz','兴趣'),(523,'select_your_music_preference','Select your music preference','حدد تفضيلات الموسيقى الخاصة بك','Selecteer je muziekvoorkeur','Sélectionnez votre préférence de musique','Wählen Sie Ihre Musikeinstellung aus','Выберите ваши музыкальные предпочтения','Seleccione su preferencia musical','Müzik tercihinizi seçin','选择您的音乐偏好'),(524,'choose_below_to_start','Choose below to start','اختر أدناه للبدء','Kies hieronder om te starten','Choisissez ci-dessous pour commencer','Wählen Sie unten aus, um zu beginnen','Выберите ниже, чтобы начать','Elija a continuación para comenzar','Başlamak için aşağıdan seçin','从下面选择开始'),(525,'next','Next','التالى','volgende','Suivant','Nächster','следующий','Siguiente','Sonraki','下一个'),(526,'you_have_to_choose_your_favorites_genres_below','You have to choose your favorites genres below','يجب عليك اختيار الأنواع المفضلة لديك أدناه','Je moet hieronder je favoriete genres kiezen','Vous devez choisir vos genres favoris ci-dessous','Sie müssen unten Ihre Lieblingsgenres auswählen','Вы должны выбрать свои любимые жанры ниже','Debes elegir tus géneros favoritos a continuación','Sık kullandığınız türleri aşağıda seçmelisiniz','你必须在下面选择你最喜欢的类型'),(527,'maintenance','Maintenance','اعمال صيانة','Onderhoud','Entretien','Instandhaltung','техническое обслуживание','Mantenimiento','Bakım','维护'),(528,'website_maintenance_mode_is_active__login_for_user_is_forbidden','Website maintenance mode is active, Login for user is forbidden','وضع صيانة موقع الويب نشط ، ويُحظر تسجيل الدخول للمستخدم','Website onderhoudsmodus is actief, Inloggen voor gebruiker is verboden','Le mode de maintenance du site Web est actif, la connexion de l\'utilisateur est interdite.','Website-Wartungsmodus ist aktiv, Login für Benutzer ist untersagt','Режим обслуживания сайта активен, вход для пользователя запрещен','El modo de mantenimiento del sitio web está activo, el inicio de sesión para usuarios está prohibido','Web sitesi bakım modu etkin, Kullanıcı girişi yapması yasaktır','网站正在维护，禁止用户登录'),(529,'website_maintenance_mode_is_active','Website maintenance mode is active','وضع صيانة الموقع نشط','De onderhoudsmodus voor de website is actief','Le mode de maintenance du site est actif','Der Website-Wartungsmodus ist aktiv','Режим обслуживания сайта активен','El modo de mantenimiento del sitio web está activo','Web sitesi bakım modu etkin','网站维护模式已激活'),(530,'we___ll_be_back_soon_','We’ll be back soon!','سنعود قريبا!','We zullen snel terug zijn!','Nous reviendrons bientôt!','Wir werden bald zurück sein!','Мы скоро вернемся!','¡Estaremos de vuelta pronto!','Yakında döneceğiz!','我们很快就回来！'),(531,'sorry_for_the_inconvenience_but_we_rsquo_re_performing_some_maintenance_at_the_moment._if_you_need_help_you_can_always','Sorry for the inconvenience but we&amp;rsquo;re performing some maintenance at the moment. If you need help you can always','نأسف للإزعاج لكننا نقوم ببعض الصيانة في الوقت الحالي. إذا كنت بحاجة إلى مساعدة يمكنك دائما','Onze excuses voor het ongemak, maar we voeren momenteel wat onderhoud uit. Als je hulp nodig hebt, kan dat altijd','Désolé pour le désagrément occasionné mais nous effectuons actuellement des travaux de maintenance. Si vous avez besoin d\'aide, vous pouvez toujours','Wir entschuldigen uns für die Unannehmlichkeiten, aber wir führen momentan einige Wartungsarbeiten durch. Wenn Sie Hilfe brauchen, können Sie das immer tun','Приносим извинения за неудобства, но в настоящее время мы проводим техническое обслуживание. Если вам нужна помощь, вы всегда можете','Disculpe las molestias, pero estamos realizando algunas tareas de mantenimiento en este momento. Si necesitas ayuda siempre puedes','Verdiğimiz rahatsızlıktan dolayı üzgünüz, ancak şu anda bazı bakımlar yapıyoruz. Yardıma ihtiyacınız olursa her zaman','很抱歉给您带来不便，但我们目前正在进行一些维护。如果你需要帮助，你可以'),(532,'otherwise_we_rsquo_ll_be_back_online_shortly_','otherwise we&amp;rsquo;ll be back online shortly!','وإلا سنعود إلى الإنترنت قريبًا!','anders zijn we binnenkort terug online!','sinon, nous serons de nouveau en ligne sous peu!','Andernfalls sind wir in Kürze wieder online!','в противном случае мы скоро вернемся в онлайн!','De lo contrario, estaremos de nuevo en línea próximamente.','Aksi halde, kısa süre sonra tekrar çevrimiçi olacağız!','我们很快就会重新上线！'),(533,'sorry_for_the_inconvenience_but_we_performing_some_maintenance_at_the_moment._if_you_need_help_you_can_always','Sorry for the inconvenience but we performing some maintenance at the moment. If you need help you can always','نأسف للإزعاج لكننا نقوم ببعض الصيانة في الوقت الحالي. إذا كنت بحاجة إلى مساعدة يمكنك دائما','Excuses voor het ongemak maar we voeren momenteel wat onderhoud uit. Als je hulp nodig hebt, kan dat altijd','Désolé pour le désagrément, mais nous effectuons quelques travaux de maintenance pour le moment. Si vous avez besoin d\'aide, vous pouvez toujours','Wir entschuldigen uns für die Unannehmlichkeiten, aber wir führen momentan einige Wartungsarbeiten durch. Wenn Sie Hilfe brauchen, können Sie das immer tun','Приносим извинения за неудобства, но в данный момент мы выполняем техническое обслуживание. Если вам нужна помощь, вы всегда можете','Disculpe las molestias pero estamos realizando algunas tareas de mantenimiento en este momento. Si necesitas ayuda siempre puedes','Verdiğimiz rahatsızlıktan dolayı üzgünüz, ancak şu anda biraz bakım yapıyoruz. Yardıma ihtiyacınız olursa her zaman','很抱歉给您带来不便，但我们目前正在执行一些维护工作。 如果您需要帮助，您可以随时'),(534,'otherwise_we_will_be_back_online_shortly_','otherwise we will be back online shortly!','وإلا فإننا سوف يعود عبر الإنترنت قريبا!','anders zijn we binnenkort weer online!','sinon nous serons de nouveau en ligne sous peu!','Ansonsten sind wir in Kürze wieder online!','в противном случае мы вернемся онлайн в ближайшее время!','De lo contrario, volveremos a estar en línea pronto.','Aksi takdirde kısa süre sonra tekrar çevrimiçi olacağız!','我们很快就会重新上线！'),(535,'views','views','الآراء','keer bekeken','vues','Ansichten','Просмотры','puntos de vista','görünümler','观点'),(536,'hide','hide','إخفاء','verbergen','cacher','verbergen','скрывать','esconder','saklamak','隐藏'),(538,'your_selection_saved_successfully.','Your selection has been updated successfully.','تم تحديث اختيارك بنجاح.','Uw selectie is succesvol bijgewerkt.','Votre sélection a été mise à jour avec succès.','Ihre Auswahl wurde erfolgreich aktualisiert.','Ваш выбор был успешно обновлен.','Su selección ha sido actualizada con éxito.','Seçiminiz başarıyla güncellendi.','您的选择已成功更新。'),(539,'please_wait...','Please wait...','ارجوك انتظر...','Even geduld aub...','S\'il vous plaît, attendez...','Warten Sie mal...','Пожалуйста, подождите...','Por favor espera...','Lütfen bekle...','请耐心等待...'),(540,'liked__auser__song_','liked |auser| song,','أعجبني |auser|  أغنية،','leuk |auser| lied,','a aimé |auser| chanson,','mochte |auser| Lied,','понравился |auser| песня,','gustado |auser| canción,','beğendi |auser| şarkı,','喜欢 |auser| 的歌曲,'),(541,'shared__auser__song_','shared |auser| song,','مشترك |auser| أغنية','gedeeld |auser| lied,','partagé |auser| chanson,','geteilt |auser| Lied,','поделился |auser| песня,','compartido |auser| canción,','paylaşılan |auser| şarkı,','分享 |auser| 的歌曲,'),(542,'commented_on__auser__song_','commented on |auser| song,','وعلق على |auser| أغنية،','heeft gereageerd op |auser| lied,','a commenté sur |auser| chanson,','kommentiert |auser| Lied,','прокомментировал |auser| песня,','ha comentado en |auser| canción,','|auser| hakkında yorum yaptı şarkı,','评论 |auser| 的歌曲'),(543,'uploaded_a_new_song_','Uploaded a new song,','تم تحميل أغنية جديدة ،','Een nieuw nummer ge-upload,','Téléchargé une nouvelle chanson,','Einen neuen Song hochgeladen,','Загрузил новую песню,','Subido una nueva canción,','Yeni bir şarkı yükledi','上传了一首新歌，'),(544,'comments','comments','تعليقات','opmerkingen','commentaires','Bemerkungen','Комментарии','comentarios','yorumlar','评论'),(545,'upload_single_song','Upload single song','تحميل أغنية واحدة','Upload een nummer','Télécharger une seule chanson','Einzelnes Lied hochladen','Загрузить одну песню','Subir una sola canción','Tek şarkı yükle','上传单曲'),(546,'upload_an_album','Upload an album','تحميل ألبوم','Upload een album','Télécharger un album','Lade ein Album hoch','Загрузить альбом','Subir un álbum','Bir albüm yükle','上传专辑'),(547,'thanks_for_your_submission__we_will_review_your_request_shortly.','Thanks for your submission, we will review your request shortly.','شكرًا على إرسالك ، سنراجع طلبك قريبًا.','Bedankt voor uw inzending, we zullen uw verzoek binnenkort beoordelen.','Merci pour votre soumission, nous examinerons votre demande sous peu.','Vielen Dank für Ihre Eingabe. Wir werden Ihre Anfrage in Kürze prüfen.','Спасибо за ваше представление, мы вскоре рассмотрим ваш запрос.','Gracias por su envío, revisaremos su solicitud en breve.','Gönderdiğiniz için teşekkür ederiz, isteğinizi kısa bir süre sonra gözden geçireceğiz.','感谢您的提交，我们会尽快审核您的请求。'),(548,'user_type','User Type','نوع المستخدم','Gebruikerstype','Type d\'utilisateur','Benutzertyp','Тип пользователя','Tipo de usuario','Kullanıcı tipi','用户类型'),(549,'years_old','years old','عمر','jaar oud','ans','Jahre alt\n','лет','años','yaşında','岁'),(550,'login_with_wowonder','Login with WoWonder','تسجيل الدخول مع Wowonder\n','Log in met Wowonder\n','Se connecter avec Wowonder\n','Mit Wowonder anmelden\n','Войти с Wowonder\n','Iniciar sesión con Wowonder\n','Wowonder ile giriş yap\n','使用WoWonder登录'),(551,'balance','Balance','توازن','Balans','Équilibre','Balance','Остаток средств','Equilibrar','Denge','余额'),(552,'available_balance','Available balance','الرصيد المتوفر','Beschikbaar saldo','Solde disponible','Verfügbares Guthaben','Доступные средства','Saldo disponible','Kalan bakiye','可用余额'),(553,'withdrawals','Withdrawals','السحب','onttrekkingen','Retraits','Abhebungen','Изъятия','Retiros','Çekme','提现'),(554,'paypal_e-mail','PayPal E-mail','بريد باي بال','Paypal E-mail','Email Paypal','Paypal Email','PayPal E-mail','E-mail de Paypal','PayPal E-posta','贝宝电子邮件'),(555,'amount','Amount','كمية','Bedrag','Montant','Menge','Количество','Cantidad','Miktar','合计'),(556,'min','Min','دقيقة','min','Min','Mindest','Min','Min','Min','Min'),(557,'submit_withdrawal_request','Submit withdrawal request','تقديم طلب السحب','Verzoek tot opname indienen','Soumettre une demande de retrait','Auszahlungsantrag stellen','Отправить запрос на вывод средств','Enviar solicitud de retiro','Para çekme isteği gönder','提交提现请求'),(558,'status','Status','الحالة','staat','Statut','Status','Статус','Estado','durum','状态'),(559,'your_withdrawal_request_has_been_successfully_sent_','Your withdrawal request has been successfully sent!','تم إرسال طلب السحب الخاص بك بنجاح!','Uw opnameverzoek is met succes verzonden!','Votre demande de retrait a été envoyée avec succès!','Ihre Auszahlungsanfrage wurde erfolgreich gesendet!','Ваш запрос на вывод средств был успешно отправлен!','Su solicitud de retiro ha sido enviada con éxito!','Para çekme isteğiniz başarıyla gönderildi!','您的提现请求已成功发送！'),(560,'requested_on','Requested on','طلب على','Aangevraagd op','Demandé le','Beantragt am','Запрошено на','Solicitado en','İstenildi','请求'),(561,'accepted','Completed','منجز','Voltooid','Terminé','Abgeschlossen','Завершенный','Terminado','Tamamlanan','已完成'),(562,'rejected','Rejected','مرفوض','Verworpen','Rejeté','Abgelehnt','Отклонено','Rechazado','Reddedilen','被拒绝'),(563,'pending','Pending','قيد الانتظار','In afwachting','en attendant','steht aus','в ожидании','Pendiente','kadar','等待'),(564,'the_amount_exceeded_your_current_balance.','The amount exceeded your current balance.','تجاوز المبلغ رصيدك الحالي.','Het bedrag heeft uw huidige saldo overschreden.','Le montant a dépassé votre solde actuel.','Der Betrag hat Ihr aktuelles Guthaben überschritten.','Сумма превысила ваш текущий баланс.','La cantidad superó su saldo actual.','Tutar, mevcut bakiyenizi aştı.','金额超过了您当前的余额。'),(565,'minimum_amount_required_is_50.','Minimum amount required is 50.','الحد الأدنى للمبلغ المطلوب هو 50.','Minimum vereiste hoeveelheid is 50.','Le montant minimum requis est de 50.','Mindestbetrag ist 50.','Минимальная необходимая сумма составляет 50.','La cantidad mínima requerida es de 50.','Gereken minimum miktar 50.','最低金额为50。'),(566,'second_lorem_ipsum_dolor_sit_amet__consectetur_adipiscing_elit__sed_do_eiusmod_tempor_incididunt_ut_labore_et_dolore_magna_aliqua__2','Second Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, 2','Second Lorem ipsum dolor sit amet، consectetur adipiscing elit، sed do eiusmod tempor incididunt ut labore et dolore magna aliqua، 2','Tweede Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, 2','Deuxième Lorem ipsum dolor sit amet, consectetur elit adipiscing, séduit temporairement incididunt ut labore et dolore magna aliqua, 2','Zweite Lorem ipsum dolor sitzen amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore und dolore magna aliqua, 2','Second Lorem Ipsum Dolor Sit Amet, Concetetur Adipiscing Elit, Sed do EiusMod Tempor Incididunt U Labore et Dolore Magna Aliqua, 2','Segundo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, 2','İkinci Lorem ipsum dolor amet sitet, adipiscing adipiscing elit, sed do eiusmod tempor incidire ve labore ve dolore magna aliqua, 2','第二个洛雷姆·伊普索姆·多尔·西特·阿梅特（Lorem Ipsum Dolor Sit Amet），圣洁的和平精英，塞德·多·埃乌斯莫德·提珀（Sed Do Eiusmod Tempor Incidedunt ut Labore et Dolore Magna Aliqua），2'),(567,'you_can_not_submit_withdrawal_request_until_the_previous_requests_has_been_approved___rejected','You can not submit withdrawal request until the previous requests has been approved / rejected','لا يمكنك إرسال طلب السحب حتى تتم الموافقة على / رفض الطلبات السابقة','U kunt geen opnameverzoek indienen totdat de vorige verzoeken zijn goedgekeurd / afgewezen','Vous ne pouvez pas soumettre de demande de retrait avant que les demandes précédentes aient été approuvées / rejetées','Sie können eine Auszahlungsanforderung erst absenden, wenn die vorherigen Anforderungen genehmigt / abgelehnt wurden','Вы не можете отправить запрос на снятие средств, пока предыдущие запросы не были одобрены / отклонены','No puede enviar una solicitud de retiro hasta que las solicitudes anteriores hayan sido aprobadas / rechazadas','Önceki istekler onaylanıp reddedilene kadar para çekme isteği gönderemezsiniz.','在以前的申请被批准/拒绝之前，您不能提交撤回申请。'),(568,'cateogry_2','喊麦现场','喊麦现场','喊麦现场','喊麦现场','喊麦现场','喊麦现场','喊麦现场','喊麦现场','喊麦现场'),(569,'cateogry_3','串烧舞曲','串烧舞曲','串烧舞曲','串烧舞曲','串烧舞曲','串烧舞曲','串烧舞曲','串烧舞曲','串烧舞曲'),(570,'cateogry_4','中文舞曲','中文舞曲','中文舞曲','中文舞曲','中文舞曲','中文舞曲','中文舞曲','中文舞曲','中文舞曲'),(571,'cateogry_5','国外舞曲','国外舞曲','国外舞曲','国外舞曲','国外舞曲','国外舞曲','国外舞曲','国外舞曲','国外舞曲'),(572,'cateogry_6','电音House','电音House','电音House','电音House','电音House','电音House','电音House','电音House','电音House'),(573,'cateogry_7','酒吧外文','酒吧外文','酒吧外文','酒吧外文','酒吧外文','酒吧外文','酒吧外文','酒吧外文','酒吧外文'),(574,'cateogry_8','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧','的高串烧'),(575,'cateogry_9','喊唛现场','喊唛现场','喊唛现场','喊唛现场','喊唛现场','喊唛现场','喊唛现场','喊唛现场','喊唛现场'),(576,'cateogry_10','中文Class','中文Class','中文Class','中文Class','中文Class','中文Class','中文Class','中文Class','中文Class'),(577,'cateogry_11','慢摇串烧','慢摇串烧','慢摇串烧','慢摇串烧','慢摇串烧','慢摇串烧','慢摇串烧','慢摇串烧','慢摇串烧'),(578,'cateogry_12','酒吧串烧','酒吧串烧','酒吧串烧','酒吧串烧','酒吧串烧','酒吧串烧','酒吧串烧','酒吧串烧','酒吧串烧'),(579,'cateogry_13','慢歌连版','慢歌连版','慢歌连版','慢歌连版','慢歌连版','慢歌连版','慢歌连版','慢歌连版','慢歌连版'),(580,'cateogry_14','外文CLUB','外文CLUB','外文CLUB','外文CLUB','外文CLUB','外文CLUB','外文CLUB','外文CLUB','外文CLUB'),(581,'cateogry_15','开场Opening','开场Opening','开场Opening','开场Opening','开场Opening','开场Opening','开场Opening','开场Opening','开场Opening'),(582,'cateogry_16','电音HOUSE','电音HOUSE','电音HOUSE','电音HOUSE','电音HOUSE','电音HOUSE','电音HOUSE','电音HOUSE','电音HOUSE'),(583,'cateogry_17','酒吧潮歌','酒吧潮歌','酒吧潮歌','酒吧潮歌','酒吧潮歌','酒吧潮歌','酒吧潮歌','酒吧潮歌','酒吧潮歌'),(584,'cateogry_18','暖场/蓝调','暖场/蓝调','暖场/蓝调','暖场/蓝调','暖场/蓝调','暖场/蓝调','暖场/蓝调','暖场/蓝调','暖场/蓝调'),(585,'cateogry_19','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb','HipHop/Rnb'),(586,'cateogry_20','外文DISCO','外文DISCO','外文DISCO','外文DISCO','外文DISCO','外文DISCO','外文DISCO','外文DISCO','外文DISCO'),(587,'cateogry_21','交谊舞曲','交谊舞曲','交谊舞曲','交谊舞曲','交谊舞曲','交谊舞曲','交谊舞曲','交谊舞曲','交谊舞曲'),(588,'cateogry_22','其他','其他','其他','其他','其他','其他','其他','其他','其他');
/*!40000 ALTER TABLE `langs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (1,1,2,1,1559963781),(2,1,1,0,1560903131);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL DEFAULT '0',
  `to_id` int(11) NOT NULL DEFAULT '0',
  `text` text,
  `seen` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `from_deleted` int(11) NOT NULL DEFAULT '0',
  `to_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `from_id` (`from_id`),
  KEY `to_id` (`to_id`),
  KEY `seen` (`seen`),
  KEY `time` (`time`),
  KEY `from_deleted` (`from_deleted`),
  KEY `to_deleted` (`to_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_admin`
--

DROP TABLE IF EXISTS `mq_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `state` int(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `login_time` int(11) DEFAULT NULL,
  `ip` varchar(100) NOT NULL,
  `old_ip` varchar(100) NOT NULL DEFAULT '',
  `old_time` int(11) DEFAULT NULL,
  `diction` int(11) NOT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `regtime` int(11) DEFAULT NULL,
  `eid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_admin`
--

LOCK TABLES `mq_admin` WRITE;
/*!40000 ALTER TABLE `mq_admin` DISABLE KEYS */;
INSERT INTO `mq_admin` VALUES (1,'admin','$2y$10$8THARSTaMe6L.OwKNkoyeuAXw1.JwzqYPbfAvQxxhF5S.1Hn1VUA2',1,NULL,'::1','::1',NULL,8,'13333333334','3233133033@qq.com',1561458762,''),(4,'123456','$2y$10$Wf01guk23JnEOnqsN1KwJeDIom5TDejruMQyd3jr4/n9zagNK/dkK',1,NULL,'111.197.22.83','',NULL,8,'15711012454','123456@qq.com',1606223117,'');
/*!40000 ALTER TABLE `mq_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_auth_group`
--

DROP TABLE IF EXISTS `mq_auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_auth_group` (
  `group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '全新ID',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '标题',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态',
  `rules` text COMMENT '规则',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员分组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_auth_group`
--

LOCK TABLES `mq_auth_group` WRITE;
/*!40000 ALTER TABLE `mq_auth_group` DISABLE KEYS */;
INSERT INTO `mq_auth_group` VALUES (8,'超级管理员',1,'[\"1\",\"2\",\"13\",\"12\",\"11\",\"3\",\"16\",\"15\",\"14\",\"17\",\"4\",\"18\",\"19\",\"20\",\"5\",\"34\",\"33\",\"37\",\"32\",\"31\",\"36\",\"30\",\"35\",\"21\",\"29\",\"28\",\"27\",\"26\",\"25\",\"24\",\"22\",\"23\",\"6\",\"42\",\"43\",\"40\",\"41\",\"39\",\"38\",\"7\",\"44\",\"45\",\"46\",\"47\",\"48\",\"8\",\"49\",\"55\",\"54\",\"53\",\"52\",\"51\",\"50\",\"9\",\"59\",\"56\",\"58\",\"57\",\"10\",\"60\",\"66\",\"62\",\"61\",\"67\",\"68\",\"63\",\"65\",\"64\",\"10\",\"60\",\"66\",\"62\",\"61\",\"67\",\"68\",\"63\",\"65\",\"64\"]',1558452568),(9,'维护',1,'{\"14\":\"5\",\"15\":\"34\",\"16\":\"33\",\"17\":\"37\",\"18\":\"32\",\"19\":\"31\",\"20\":\"36\",\"21\":\"30\",\"22\":\"35\",\"23\":\"21\",\"24\":\"29\",\"25\":\"28\",\"26\":\"27\",\"27\":\"26\",\"28\":\"25\",\"29\":\"24\",\"30\":\"22\",\"31\":\"23\",\"32\":\"6\",\"33\":\"42\",\"34\":\"43\",\"35\":\"40\",\"36\":\"41\",\"37\":\"39\",\"38\":\"38\",\"39\":\"7\",\"40\":\"44\",\"41\":\"45\",\"42\":\"46\",\"43\":\"47\",\"44\":\"48\",\"45\":\"7\",\"46\":\"44\",\"47\":\"45\",\"48\":\"46\",\"49\":\"47\",\"50\":\"48\"}',1561446997),(10,'工作人员',1,'[\"1\",\"2\",\"13\",\"12\",\"11\",\"3\",\"16\",\"15\",\"14\",\"17\",\"4\",\"18\",\"19\",\"20\",\"5\",\"34\",\"33\",\"37\",\"32\",\"31\",\"36\",\"30\",\"35\",\"21\",\"29\",\"28\",\"27\",\"26\",\"25\",\"24\",\"22\",\"23\",\"6\",\"42\",\"43\",\"40\",\"41\",\"39\",\"38\",\"7\",\"44\",\"45\",\"46\",\"47\",\"48\",\"8\",\"49\",\"55\",\"54\",\"53\",\"52\",\"51\",\"50\",\"9\",\"59\",\"56\",\"58\",\"57\",\"10\",\"60\",\"66\",\"62\",\"61\",\"67\",\"68\",\"69\",\"63\",\"65\",\"64\",\"10\",\"60\",\"66\",\"62\",\"61\",\"67\",\"68\",\"69\",\"63\",\"65\",\"64\"]',1606266109);
/*!40000 ALTER TABLE `mq_auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_auth_rule`
--

DROP TABLE IF EXISTS `mq_auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `href` char(80) NOT NULL DEFAULT '' COMMENT '权限点',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '名称',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型（拓展字段）',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `authopen` tinyint(2) NOT NULL DEFAULT '1',
  `icon` varchar(20) DEFAULT NULL COMMENT '样式',
  `condition` char(100) DEFAULT '',
  `pid` int(5) NOT NULL DEFAULT '0' COMMENT '父栏目ID',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `menustatus` tinyint(1) DEFAULT NULL COMMENT '菜单状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='权限节点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_auth_rule`
--

LOCK TABLES `mq_auth_rule` WRITE;
/*!40000 ALTER TABLE `mq_auth_rule` DISABLE KEYS */;
INSERT INTO `mq_auth_rule` VALUES (1,'Admin','权限管理',1,1,0,'layui-icon-set','',0,0,1561293029,1),(2,'Admin/adminlist','管理员列表',1,1,0,'','',1,1,1561293073,1),(3,'Admin/adminrole','角色管理',1,1,0,'','',1,2,1561293519,1),(4,'Admin/adminrule','权限设置',1,1,0,'','',1,3,1561293591,1),(5,'member','会员管理',1,1,0,'layui-icon-username','',0,1,1561293648,1),(6,'card','通道管理',1,1,1,'layui-icon-survey','',0,2,1561293750,1),(7,'domain','域名管理',1,1,1,'layui-icon-website','',0,3,1561293793,1),(8,'ewm','收款账号',1,1,1,'layui-icon-dollar','',0,4,1561293843,1),(9,'Webjs','网站结算',1,1,1,'layui-icon-cart','',0,5,1561293923,1),(10,'system','系统配置',1,1,1,'layui-icon-util','',0,6,1561294003,1),(11,'Admin/adminadd','操作-添加',1,1,0,'','',2,0,1561294110,1),(12,'Admin/adminedit','操作-编辑',1,1,0,'','',2,0,1561294162,1),(13,'Admin/edits','操作-启用',1,1,0,'','',2,0,1561294232,1),(14,'Admin/roleadd','操作-添加',1,1,0,'','',3,0,1561294300,1),(15,'Admin/rolekg','操作-启用',1,1,0,'','',3,0,1561294368,1),(16,'Admin/groupdel','操作-删除',1,1,0,'','',3,0,1561294442,1),(17,'Admin/rolecreate','操作-添加',1,1,0,'','',3,0,1561294636,1),(18,'Admin/ruleedit','操作-启用',1,1,0,'','',4,0,1561294730,1),(19,'Admin/dels','操作-删除',1,1,0,'','',4,0,1561294781,1),(20,'Admin/ruletz','操作-可视',1,1,0,'','',4,0,1561294928,1),(21,'Member/memberlist','会员列表',1,1,0,'','',5,0,1561294974,1),(22,'Member/memberedit','操作-编辑',1,1,0,'','',21,0,1561294998,1),(23,'Member/memberadd','操作-添加',1,1,0,'','',21,0,1561295014,1),(24,'Member/userdel','操作-删除',1,1,1,'','',21,0,1561295035,1),(25,'Member/memberfl','操作-费率',1,1,1,'','',21,0,1561295060,1),(26,'Member/memberloginlog','查看登陆记录',1,1,1,'','',21,0,1561295100,1),(27,'Member/logindel','操作-删除登陆记录',1,1,1,'','',21,0,1561295137,1),(28,'Member/membermxlog','查看资金明细',1,1,1,'','',21,0,1561295180,1),(29,'Member/mxdel','删除记录',1,1,1,'','',21,0,1561295218,1),(30,'Member/membermoneyorder','充值记录',1,1,1,'','',5,0,1561295314,1),(31,'Member/membertxorder','汇款记录',1,1,1,'','',5,0,1561295361,1),(32,'Member/memberviporder','开通记录',1,1,1,'','',5,0,1561295408,1),(33,'Member/memberwithorder','提现审核',1,1,1,'','',5,0,1561295429,1),(34,'Member/memberyongjin','佣金列表',1,1,0,'','',5,0,1561295452,1),(35,'Member/orderdel','操作-删除',1,1,1,'','',30,0,1561295636,1),(36,'Member/txdel','操作-删除',1,1,1,'','',31,0,1561295785,1),(37,'Member/withedit','操作-审核',1,1,1,'','',33,0,1561295832,1),(38,'Message/mverify','模版审核',1,1,1,'','',6,0,1561445629,1),(39,'Message/msgedit','操作-编辑',1,1,1,'','',6,0,1561445671,1),(40,'Message/msgorder','购买列表',1,1,1,'','',6,0,1561445735,1),(41,'Message/msdel','操作-删除',1,1,1,'','',40,0,1561445758,1),(42,'Message/msgsend','发送列表',1,1,1,'','',6,0,1561445789,1),(43,'Message/dxrest','操作-重发',1,1,1,'','',42,0,1561445828,1),(44,'Domain/dolist','域名列表',1,1,1,'','',7,0,1561445903,1),(45,'Domian/doset','操作-启用',1,1,1,'','',44,0,1561445937,1),(46,'Domain/dodel','操作-删除',1,1,1,'','',44,0,1561445964,1),(47,'Domain/xflist','续费列表',1,1,1,'','',7,0,1561446022,1),(48,'Domain/doedit','操作-编辑',1,1,1,'','',47,0,1561446053,1),(49,'Ewm/pay','账号列表',1,1,1,'','',8,0,1561446081,1),(50,'Ewm/eset','操作-编辑',1,1,1,'','',49,0,1561446109,1),(51,'Ewm/ewmedit','操作-编辑',1,1,1,'','',49,0,1561446147,1),(52,'Ewm/edel','操作-删除',1,1,1,'','',49,0,1561446173,1),(53,'Ewm/ewmlist','二维码列表',1,1,1,'','',49,0,1561446218,1),(54,'Ewm/upfiel','二维码上传',1,1,1,'','',49,0,1561446261,1),(55,'Ewm/todel','操作-删除二维码',1,1,1,'','',49,0,1561446292,1),(56,'Webjs/index','结算列表',1,1,1,'','',9,0,1561446325,1),(57,'Webjs/delet','操作-删除',1,1,1,'','',56,0,1561446348,1),(58,'Webjs/jiesuan','操作-结算',1,1,1,'','',56,0,1561446431,1),(59,'Webjs/datadel','删除数据',1,1,1,'','',9,0,1561446462,1),(60,'System/index','网站设置',1,1,1,'','',10,1,1561446516,1),(61,'System/weixin','微信设置',1,1,1,'','',10,2,1561446539,1),(62,'System/fenzhan','分站设置',1,1,1,'','',10,2,1561446569,1),(63,'System/news','新闻设置',1,1,1,'','',10,5,1561446598,1),(64,'System/editnews','操作-编辑',1,1,1,'','',63,0,1561446627,1),(65,'System/delnews','操作-删除',1,1,1,'','',63,0,1561446658,1),(66,'System/upload','操作-上传logo',1,1,1,'','',60,1,1561446705,1),(67,'System/uploadfile','操作-上传证书',1,1,1,'','',61,2,1561446760,1),(68,'System/email','邮箱配置',1,1,1,'','',10,3,1561446804,1),(69,'System/banner','轮播图设置',1,1,1,'','',10,4,1561446804,1);
/*!40000 ALTER TABLE `mq_auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_banks`
--

DROP TABLE IF EXISTS `mq_banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_banks` (
  `id` mediumint(18) NOT NULL AUTO_INCREMENT,
  `abbr` varchar(100) DEFAULT NULL,
  `account` varchar(50) DEFAULT NULL,
  `state` int(1) NOT NULL DEFAULT '1',
  `bankName` varchar(150) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=218 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_banks`
--

LOCK TABLES `mq_banks` WRITE;
/*!40000 ALTER TABLE `mq_banks` DISABLE KEYS */;
INSERT INTO `mq_banks` VALUES (1,'ICBC','',1,'中国工商银行',0,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gsyh.png'),(2,'ABC','',1,'中国农业银行',1,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nyyh.png'),(3,'BOC','',1,'中国银行',2,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zgyh.png'),(4,'CCB','',1,'中国建设银行',3,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zgjsyh.png'),(5,'BOCO','',1,'交通银行',4,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jtyh.png'),(6,'ECITIC','',1,'中信银行',5,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zxyh.png'),(7,'CEB','',1,'中国光大银行',6,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gdyh.png'),(8,'HXB','',1,'华夏银行',7,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hxyh.png'),(9,'CGB','',1,'广发银行',8,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gfyh.png'),(10,'PINGANBANK','',1,'平安银行',9,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_payh.png'),(11,'CMBCHINA','',1,'招商银行',10,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zsyh.png'),(12,'CIB','',1,'兴业银行',11,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xyyh.png'),(13,'SPDB','',1,'上海浦东发展银行',12,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shpdfzyh.png'),(14,'BCCB','',1,'北京银行',13,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bjyh.png'),(15,'TJCB','',1,'天津银行',14,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_tjyh.png'),(16,'HEBB','',1,'河北银行',15,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hbyh.png'),(17,'RCB','',1,'邯郸市商业银行',16,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hdssyyh.png'),(18,'XTBK','',1,'邢台银行',17,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xtyh.png'),(19,'RCB','',1,'张家口农村商业银行',18,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zhangjkncsyyh.png'),(20,'CDB','',1,'承德银行',19,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_cdeyh.png'),(21,'BOCZ','',1,'沧州银行',20,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_czyh.png'),(22,'LFB','',1,'廊坊银行',21,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_langfyh.png'),(23,'BHS','',1,'衡水银行',22,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hengsyh.png'),(24,'JSB','',1,'晋商银行',23,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jsyh.png'),(25,'JCB','',1,'晋城银行',24,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jcyh.png'),(26,'JZB','',1,'晋中银行',25,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jinzyh.png'),(27,'BOIMC','',1,'内蒙古银行',26,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nnmgyyh.png'),(28,'BSB','',1,'包商银行',27,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bsyh.png'),(29,'BOWH','',1,'乌海银行',28,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_wuhyhyh.png'),(30,'ORDOSB','',1,'鄂尔多斯银行',29,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_eredsyh.png'),(31,'DLCB','',1,'大连银行',30,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dlyh.png'),(32,'ASRCB','',1,'鞍山农村商业银行',31,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ansncsyyh.png'),(33,'JZCB','',1,'锦州银行',32,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jzyh.png'),(34,'BOHLD','',1,'葫芦岛银行',33,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_huhdyh.png'),(35,'YKB','',1,'营口银行',34,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ykyh.png'),(36,'FXB','',1,'阜新银行',35,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fuxyh.png'),(37,'JLCB','',1,'吉林银行',36,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jlyh.png'),(38,'HBCB','',1,'哈尔滨银行',37,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hebyh.png'),(39,'LJB','',1,'龙江银行',38,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ljyh.png'),(40,'NJCB','',1,'南京银行',39,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_njyh.png'),(41,'BOJS','',1,'江苏银行',40,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiangsyh.png'),(42,'BOSZ','',1,'苏州银行',41,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_szyh.png'),(43,'CJCCB','',1,'江苏长江商业银行',42,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiangscjssyh.png'),(44,'HZCB','',1,'杭州银行',43,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_yhyh.png'),(45,'NBCB','',1,'宁波银行',44,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nbyh.png'),(46,'NBCMB','',1,'宁波通商银行',45,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nbtsyhgfyxgs.png'),(47,'WZCB','',1,'温州银行',46,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_wzyh.png'),(48,'JXCCB','',1,'嘉兴银行',47,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jxyh.png'),(49,'HZCCB','',1,'湖州银行',48,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hzyh.png'),(50,'SXCCB','',1,'绍兴银行',49,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_sxyh.png'),(51,'JHB','',1,'金华银行',50,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jhyhgfyxgs.png'),(52,'CZCB','',1,'浙江稠州商业银行',51,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zjczsyyh.png'),(53,'TZB','',1,'台州银行',52,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_tzyh.png'),(54,'TLCB','',1,'浙江泰隆商业银行',53,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zjtlsyyh.png'),(55,'ZJMTCB','',1,'浙江民泰商业银行',54,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zjmtsyyh.png'),(56,'FJHXB','',1,'福建海峡银行',55,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fujhxyh.png'),(57,'XMCCB','',1,'厦门银行',56,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xmyh.png'),(58,'QZCCB','',1,'泉州银行',57,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_quangzyh.png'),(59,'NCCB','',1,'南昌银行',58,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ncyh.png'),(60,'JJCCB','',1,'九江银行',59,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jjyhgfyxgs.png'),(61,'GZCCB','',1,'赣州银行',60,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ganzyh.png'),(62,'SRB','',1,'上饶银行',61,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_sryh.png'),(63,'QLB','',1,'齐鲁银行',62,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qlyh.png'),(64,'QDCB','',1,'青岛银行',63,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qdyh.png'),(65,'ZCCB','',1,'齐商银行',64,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qisyh.png'),(66,'ZZB','',1,'枣庄银行',65,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zaozhuangyh.png'),(67,'DYB','',1,'东营银行',66,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dyingyh.png'),(68,'YTB','',1,'烟台银行',67,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ytyh.png'),(69,'WFCB','',1,'潍坊银行',68,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_wfyh.png'),(70,'JNBANK','',1,'济宁银行',69,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jnyh.png'),(71,'','',1,'泰安市商业银行',70,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_tassyyh.png'),(72,'LSBCHINA','',1,'莱商银行',71,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_laisyh.png'),(73,'WHCCB','',1,'威海市商业银行',72,''),(74,'DZBCHINA','',1,'德州银行',73,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dzyh.png'),(75,'LSB','',1,'临商银行',74,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_lsyh.png'),(76,'BORZ','',1,'日照银行',75,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_rzyh.png'),(77,'CBZZ','',1,'郑州银行',76,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zzyh.png'),(78,'ZYB','',1,'中原银行',77,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zyyh.png'),(79,'BOLY','',1,'洛阳银行',78,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_luoyyh.png'),(80,'BPS','',1,'平顶山银行',79,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_pdsyh.png'),(81,'','',1,'焦作市商业银行',80,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiaozssyyh.png'),(82,'HKBCHINA','',1,'汉口银行',81,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hkyh.png'),(83,'HBC','',1,'湖北银行',82,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hubeiyh.png'),(84,'HRXJB','',1,'华融湘江银行',83,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_huarxjyh.png'),(85,'CCCB','',1,'长沙银行',84,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_csyh.png'),(86,'GZCB','',1,'广州银行',85,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gzyh.png'),(87,'CRB','',1,'珠海华润银行',86,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zhhryh.png'),(88,'GDHXB','',1,'广东华兴银行',87,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gdhxyh.png'),(89,'GDNYB','',1,'广东南粤银行',88,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gdnyyh.png'),(90,'DGCB','',1,'东莞银行',89,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dgyh.png'),(91,'BOBBG','',1,'广西北部湾银行',90,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gxbbwyh.png'),(92,'BOLZ','',1,'柳州银行',91,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_liuzyh.png'),(93,'GLB','',1,'桂林银行',92,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_guilyh.png'),(94,'CDCB','',1,'成都银行',93,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_cdyh.png'),(95,'CQCB','',1,'重庆银行',94,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_cqyh.png'),(96,'ZGCB','',1,'自贡农村商业银行',95,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zigongncsyyh.png'),(97,'PZHCCB','',1,'攀枝花市商业银行',96,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fanzhsyyh.png'),(98,'DYCC','',1,'德阳银行',97,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dyyh.png'),(99,'MYCCB','',1,'绵阳市商业银行',98,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jyssyyh.png'),(100,'RCB','',1,'南充农村商业银行',99,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nancncsyyh.png'),(101,'GYCB','',1,'贵阳银行',100,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gyyh.png'),(102,'KMCB','',1,'富滇银行',101,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fdyh.png'),(103,'QJCCB','',1,'曲靖市商业银行',102,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qqssyyh.png'),(104,'YXCCB','',1,'玉溪市商业银行',103,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_yuxssyyh.png'),(105,'XAB','',1,'西安银行',104,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xayh.png'),(106,'CAB','',1,'长安银行',105,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_changanyh.png'),(107,'LZCB','',1,'兰州银行',106,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_lanzhouyh.png'),(108,'BQH','',1,'青海银行',107,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qinhaiyh.png'),(109,'YCCCB','',1,'宁夏银行',108,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ninxiayh.png'),(110,'UQCB','',1,'乌鲁木齐市商业银行',109,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_wulmqsyyh.png'),(111,'KLB','',1,'昆仑银行',110,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_klyh.png'),(112,'WRCB','',1,'无锡农村商业银行',111,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_wuxncsyyh.png'),(113,'JYRB','',1,'江阴农村商业银行',112,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiangyncsyyh.png'),(114,'TCRCB','',1,'太仓农村商业银行',113,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_taicncsyyh.png'),(115,'KSRB','',1,'昆山农村商业银行',114,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_kunsncsyyh.png'),(116,'WJRCB','',1,'吴江农村商业银行',115,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_wujncsyyh.png'),(117,'CSRCBANK','',1,'常熟农村商业银行',116,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_csncsyyh.png'),(118,'ZJRB','',1,'张家港农村商业银行',117,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zjgncsyyh.png'),(119,'GRCB','',1,'广州农村商业银行',118,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gzncsyyh.png'),(120,'SDEBANK','',1,'顺德农村商业银行',119,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shundncsyyh.png'),(121,'UB','',1,'海口联合农村商业银行',120,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hklhncsyyh.png'),(122,'CDRCB','',1,'成都农村商业银行',121,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_cduncsyyh.png'),(123,'CQRCB','',1,'重庆农村商业银行',122,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_cqncsyyh.png'),(124,'EBCL','',1,'恒丰银行',123,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_henfengyh.png'),(125,'CZSB','',1,'浙商银行',124,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zeshangyh.png'),(126,'TRCB','',1,'天津农村商业银行',125,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_tjnsyh.png'),(127,'CBHB','',1,'渤海银行',126,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bhyh.png'),(128,'HSCB','',1,'徽商银行',127,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_huishangyh.png'),(129,'SYYZB','',1,'北京顺义银座村镇银行',128,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bjsyyzyh.png'),(130,'JNYZB','',1,'浙江景宁银座村镇银行',129,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zejjnyzyh.png'),(131,'SMYZB','',1,'浙江三门银座村镇银行',130,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zejsmyzyh.png'),(132,'GZYZB','',1,'江西赣州银座村镇银行',131,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiangxgzzyyhh.png'),(133,'DYLSB','',1,'东营莱商村镇银行股份有限公司',132,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dylsyhggfyxgsyh.png'),(134,'FTYZB','',1,'深圳福田银座村镇银行',133,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dylsyhggfyxgsyh.png'),(135,'YBYZB','',1,'重庆渝北银座村镇银行',134,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_chongqybyzyh.png'),(136,'QJYZB','',1,'重庆黔江银座村镇银行',135,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_chongqybyzyh.png'),(137,'SRCB','',1,'上海农村商业银行',136,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shnnyh.png'),(138,'','',1,'深圳前海微众银行',137,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shenzqhwzyh.png'),(139,'SHB','',1,'上海银行',138,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shanghaiyh.png'),(140,'BRCB','',1,'北京农村商业银行',139,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bjncsyyh.png'),(141,'JLPRCU','',1,'吉林农村信用社',140,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jilncxys.png'),(142,'JSRCU','',1,'江苏省农村信用社联合社',141,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiangssncxyslhs.png'),(143,'ZJRCC','',1,'浙江省农村信用社',142,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zejsncxys.png'),(144,'BEEB','',1,'宁波市鄞州国民村镇银行',143,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ningbsyzgmczyh.png'),(145,'AHRCU','',1,'安徽省农村信用社联合社',144,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_anhsncxyslhs.png'),(146,'FJNX','',1,'福建省农村信用社',145,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fujsncxys.png'),(147,'','',1,'农村信用社',146,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ncxys.png'),(148,'RCCOSD','',1,'山东省农村信用社联合社',147,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_sandsncxyslhs.png'),(149,'HBXH','',1,'湖北省农村信用社联合社',148,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hubsncxyslhs.png'),(150,'WHRCB','',1,'武汉农村商业银行',149,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hubsncxyslhs.png'),(151,'GDRCC','',1,'广东省农村信用社联合社',150,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gdsncxyslhs.png'),(152,'SZRB','',1,'深圳农村商业银行',151,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shenzncsyyh.png'),(153,'DGCU','',1,'东莞农村商业银行',152,''),(154,'GXNX','',1,'广西农村信用社',153,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gxncxys.png'),(155,'HNB','',1,'海南省农村信用社',154,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gxncxys.png'),(156,'SCRCU','',1,'四川省农村信用社联合社',155,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shicsncxyslhs.png'),(157,'GZNXB','',1,'贵州省农村信用社联合社',156,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gzsncxyslhs.png'),(158,'YNRCC','',1,'云南省农村信用社',157,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_yunnsncxys.png'),(159,'SXNXS','',1,'陕西省农村信用社联合社',158,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shanxsncxyslhs.png'),(160,'YRRCB','',1,'黄河农村商业银行',159,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_huanghncsyyh.png'),(161,'POST','',1,'中国邮政储蓄银行',160,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zhonggyzcxyh.png'),(162,'HKBEA','',1,'东亚银行（中国）有限公司',161,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dyayyxgsyh.png'),(163,'WB','',1,'友利银行',162,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dyayyxgsyh.png'),(164,'SHBC','',1,'新韩银行中国',163,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xinhzgyh.png'),(165,'IBK','',1,'企业银行',164,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xinhzgyh.png'),(166,'HANABANK','',1,'韩亚银行',165,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hanyyh.png'),(167,'XIB','',1,'厦门国际银行',166,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xiamgjyh.png'),(168,'FSB','',1,'富邦华一银行',167,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fubhyh.png'),(169,'AAAAA','',1,'其他',-2,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qitayh.png'),(170,'CMBC','',1,'民生银行',-1,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qitayh.png'),(171,'RCB','',1,'青岛农村商业银行',168,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qingdncsyyh.png'),(172,'RCB','',1,'陕西神木农村商业银行',169,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shaxsmncsyyh.png'),(173,'RCB','',1,'福建石狮农村商业银行',200,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fujssncsyyh.png'),(174,'YACCB','',1,'雅安市商业银行',201,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_yaanssyyh.png'),(175,'YQCCB','',1,'阳泉市商业银行',202,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_yangqssyyh.png'),(176,'BOTL','',1,'铁岭银行',203,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_tielyh.png'),(177,'CB','',1,'重庆璧山工银村镇银行',204,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_chongqbsgyyh.png'),(178,'CCQTGB','',1,'重庆三峡银行',205,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_chongqsxyh.png'),(179,'ZYCCB','',1,'遵义市商业银行',206,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zhunyisyyh.png'),(180,'SNCCB','',1,'遂宁市商业银行',207,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_suinssyyh.png'),(181,'BXCBL','',1,'达州市商业银行',208,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_suinssyyh.png'),(182,'BOTB','',1,'西藏银行',209,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xizangyh.png'),(183,'YKYHCB','',1,'营口沿海银行',210,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_yinkyhyh.png'),(184,'BOSZS','',1,'石嘴山银行',211,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shizuisyh.png'),(185,'SJB','',1,'盛京银行',212,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_shengjyh.png'),(186,'PJCB','',1,'盘锦市商业银行',213,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_panjssyyh.png'),(187,'BOGS','',1,'甘肃银行',214,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gansyh.png'),(188,'RCB','',1,'珠海农村商业银行',215,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zhuhncsyyh.png'),(189,'CB','',1,'湖北嘉鱼吴江村镇银行',216,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hubjyyjczyh.png'),(190,'CB','',1,'浙江平湖工银村镇银行',217,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_pinhgyczyh.png'),(191,'LZCCB','',1,'泸州市商业银行',218,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_huzssyyh.png'),(192,'JNRCB','',1,'江南农村商业银行',219,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jiangnnncsyyh.png'),(193,'BXCBL','',1,'本溪市商业银行',220,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bxssyyh.png'),(194,'CYCB','',1,'朝阳银行',221,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_zaoyyh.png'),(195,'JYRCB','',1,'晋中市榆次融信村镇银行',222,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_jinzsycrxyh.png'),(196,'DBS','',1,'星展银行',223,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xingzanyh.png'),(197,'BOHH','',1,'新疆汇和银行',224,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_xinjhhyh.png'),(198,'BOFS','',1,'抚顺银行',225,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_fusyh.png'),(199,'HSBL','',1,'恒生银行',226,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hengsyh.png'),(200,'RCB','',1,'广东南海农村商业银行',227,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_gdnhncsyyh.png'),(201,'RCB','',1,'平凉农村商业银行',228,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_pinlncsyyh.png'),(202,'YBCCB','',1,'宜宾市商业银行',229,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ningbdhyh.png'),(203,'NBNHB','',1,'宁波东海银行',230,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_ningbdhyh.png'),(204,'RCB','',1,'天津滨海农村商业银行',231,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_tianjbhncsyyh.png'),(205,'BXCBL','',1,'哈密市商业银行',232,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hamssyyh.png'),(206,'NVCB','',1,'南阳村镇银行',233,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nanyczyh.png'),(207,'LSZSH','',1,'凉山州商业银行',234,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_liangszsyyh.png'),(208,'LSCCB','',1,'乐山市商业银行',235,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_lesssyyh.png'),(209,'DDB','',1,'丹东银行',236,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_dandyh.png'),(210,'CB','',1,'三水珠江村镇银行',237,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_sanszjczyh.png'),(211,'RCB','',1,'青海西宁农村商业银行',238,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_qingdxnncsyyh.png'),(212,'RCC','',1,'博白县农村信用合作联社',239,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bbxncxyslhs.png'),(213,'RCB','',1,'内蒙古呼和浩特金谷农村商业银行',240,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_nmghehrjgsyyh.png'),(214,'HEBNX','',1,'河北省农村信用社联合社',241,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_hebsncxyslhs.png'),(215,'RCC','',1,'哈尔滨农村商业银行',242,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_haerbnsyh.png'),(216,'NMGNX','',1,'内蒙古自治区农村信用社联合社',243,'http://suxiaoka-resource.oss-cn-shenzhen.aliyuncs.com/template/memberCenter/images/common/bank/74x74/logo_bbxncxyslhs.png'),(217,'RCB','',1,'兰州农村商业银行',244,'');
/*!40000 ALTER TABLE `mq_banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_category`
--

DROP TABLE IF EXISTS `mq_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) DEFAULT NULL,
  `iconUrl` varchar(1024) DEFAULT NULL,
  `imgs` varchar(255) NOT NULL COMMENT 'img2',
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_category`
--

LOCK TABLES `mq_category` WRITE;
/*!40000 ALTER TABLE `mq_category` DISABLE KEYS */;
INSERT INTO `mq_category` VALUES (15,'游戏卡回收','/static/uploads/20221105/d2dbfa7c9bf4718bd655bceafebff6ac.png','/static/uploads/20221105/559648c2bf72ce4b10f111b7f66435e5.png','2021-06-16 21:11:42'),(13,'话费卡回收','/static/uploads/20221106/e7a148a59c8b9483fa43b6ecf90af5f5.png','/static/uploads/20221106/ef7c2a397122d0e0e00f0fd4f017b499.png','2021-06-16 17:15:40'),(14,'加油卡回收','/static/uploads/20221105/9dedc8c5c787eea093f74d041af75b59.png','/static/uploads/20221105/19083f4bbac52ea4d475c197c8572ff5.png','2021-06-16 17:16:13'),(16,'电商购物','/static/uploads/20221105/7d7fbbebdf3104c9ff77637ca7e4c221.png','/static/uploads/20221105/f00851aed88c4b497169ed8ab1b3b368.png','2021-10-20 15:28:35'),(17,'美食出行','/static/uploads/20221105/6d093136a399f9e0661683f13701702c.png','/static/uploads/20221105/b5d04c00488701b5d6b1a1807e5187f6.png','2021-10-20 15:40:02'),(18,'视频影音','/static/uploads/20221106/3f24ed9678a90893d8f1c3b227909bb0.png','/static/uploads/20221106/0fd69e751456d26290f8f8a87643c3d0.png','2021-10-20 15:40:14');
/*!40000 ALTER TABLE `mq_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_channel`
--

DROP TABLE IF EXISTS `mq_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_channel` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `fiel` char(50) DEFAULT NULL COMMENT '字段',
  `start` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
  `type` varchar(100) NOT NULL COMMENT '小类',
  `payrate` decimal(10,2) NOT NULL COMMENT '合作费率',
  `sysrate` decimal(10,2) NOT NULL COMMENT '系统费率',
  `oper` int(10) NOT NULL DEFAULT '0' COMMENT '运营商ID',
  `amt` varchar(255) DEFAULT NULL,
  `rule` varchar(200) DEFAULT NULL,
  `stype` int(2) DEFAULT NULL,
  `class` int(1) NOT NULL DEFAULT '1',
  `iconUrl` varchar(255) DEFAULT NULL,
  `phoneRecycleIcon` varchar(255) DEFAULT NULL,
  `auto` int(1) NOT NULL DEFAULT '0',
  `qian` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_channel`
--

LOCK TABLES `mq_channel` WRITE;
/*!40000 ALTER TABLE `mq_channel` DISABLE KEYS */;
INSERT INTO `mq_channel` VALUES (143,'骏卡一卡通','yrCl143',1,'542',86.00,86.00,12,'[{\"cardNumberLength\":\"卡号为16位，卡密为16位\",\"cardPasswordLength\":\"0\"}]','5|6|10|15|30|50|100|120|200|300|400|500|800|1000',NULL,15,'/static/uploads/20210616/66a1d07a14e5bda97d7bfcd378bb103c.png','/static/uploads/20210616/a019d9da2a66ee7194ae6e73884f68cd.png',1,0),(138,'移动卡密','aO2U138',1,'86',98.87,98.87,12,'[{\"cardNumberLength\":\"卡号17位，密码18位，卡密纯数字\",\"cardPasswordLength\":\"0\"}]','30|50|100',NULL,13,'/static/uploads/20210616/f5d5cdfd327e5e42c127af85c98b19f2.png','/static/uploads/20210616/cb775f640fc280b695c878f65c2e032c.png',1,0),(139,'联通卡密','cVxy139',1,'6',98.86,98.86,12,'[{\"cardNumberLength\":\"卡号15位，密码19位纯数字\",\"cardPasswordLength\":\"0\"}]','30|50|100',NULL,13,'/static/uploads/20210616/081d9be50e3c6b6f93974e66841b06d1.png','/static/uploads/20210616/b2ca2321b408e9f5469a715330551af7.png',0,0),(140,'电信话费卡','dbVB140',1,'76',98.85,98.85,12,'[{\"cardNumberLength\":\"卡号19位，密码18位纯数字\",\"cardPasswordLength\":\"0\"}]','30|50|100',NULL,13,'/static/uploads/20210616/fd77c05060f6fc3b0ac5ad7a0a1c16b5.png','/static/uploads/20210616/507a8af6cde5012db467041ff979d1c2.png',1,0),(141,'中国石化加油卡','b2Xq141',1,'43',96.00,96.00,12,'[{\"cardNumberLength\":\"20位纯数字卡密\",\"cardPasswordLength\":\"0\"}]','100|200|500|1000',NULL,14,'/static/uploads/20210616/00f9d0a0d8f9f2839a830931c88e615c.png','/static/uploads/20210616/44786a3588a4cd06df4619e8e2ba1cd2.png',1,0),(142,'中国石油加油卡','5jdd142',1,'232',95.50,95.50,12,'[{\"cardNumberLength\":\"17位卡号、卡密19位\",\"cardPasswordLength\":\"0\"}]','100|200|500|1000',NULL,14,'/static/uploads/20210616/75ef799de3becf781c18d0455e1e14a2.png','/static/uploads/20210616/300ca8a7067ab3aea298a9bd26cc15db.png',1,0),(144,'100元','YsRx144',1,'5',96.00,98.00,12,'[{\"cardNumberLength\":\"此卡16为\",\"cardPasswordLength\":\"\"}]','50|100|150',NULL,16,'/static/uploads/20221105/51262d638959bbe204a600c1d2dbc697.jpg','/static/uploads/20221106/6755586a445e815cb85b84b93452893b.png',1,0);
/*!40000 ALTER TABLE `mq_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_config`
--

DROP TABLE IF EXISTS `mq_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_config` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(50) DEFAULT NULL COMMENT '配置的key键名',
  `value` longtext COMMENT '配置的val值',
  `inc_type` varchar(64) DEFAULT 'fenzhan' COMMENT '配置分组',
  `desc` varchar(50) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_config`
--

LOCK TABLES `mq_config` WRITE;
/*!40000 ALTER TABLE `mq_config` DISABLE KEYS */;
INSERT INTO `mq_config` VALUES (1,'title','云回收','system','网站名称'),(2,'keywords','网站关键词','system','关键字'),(3,'description','网站描述','system','简介'),(4,'kefu','1112121','system','客服QQ'),(9,'dxpid','314732607','system','短信pid'),(10,'dxkey','7022d6ade8abdddae129a0bc7130e003','system','短信KEY'),(11,'dxxf','1,30|2,60|3,80|6,140|9,200|12,240','system','短信购买'),(73,'dlbl','5','system','佣金比例'),(72,'dlnum','3','system','代理几级'),(20,'kfreg','on','system','开放注册：'),(21,'webopen','on','system','网站开关：'),(71,'phone','123456789','system','客服电话'),(23,'wclose','455                                                      ','system','关站提示'),(24,'logo','/static/uploads/20210615/ca5e716a05ce6311ee4fd382c77375f4.png','system','logo'),(25,'wxpid','4545','weixin','微信PID'),(26,'wxkey','不能用','weixin','微信KEY'),(27,'wxcert','不能用','weixin','证书1'),(28,'wxmach','不能用','weixin','证书2'),(70,'tj',NULL,'systerm','统计代码'),(66,'adminvi','off','system','后台登陆验证'),(63,'bmsgEmail','<div style=\"width:800px;margin: 0 auto;\"><table width=\"800\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#fefcfa\" style=\"border-radius:5px; overflow:hidden; border-top:4px solid #00c3b6; border-right:1px solid #dbd1ce; border-bottom:1px solid #dbd1ce; border-left:1px solid #dbd1ce;font-family:微软雅黑;\"><tbody><tr><td><table width=\"800\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" height=\"48\"><tbody><tr><td width=\"74\" height=\"35\" border=\"0\" align=\"center\" valign=\"middle\" style=\"padding-left:20px;\"><a href=\"http://{url}\" target=\"_blank\" style=\"text-decoration: none;\" rel=\"noopener\"><span style=\"vertical-align:middle;font-size:20px;line-height:32px;white-space:nowrap;\">{title}</span></a></td><td width=\"703\" height=\"48\" colspan=\"2\" align=\"right\" valign=\"middle\" style=\"color:#333; padding-right:20px;font-size:14px;font-family:微软雅黑\"><a style=\"padding:0 10px;text-decoration:none;\" target=\"_blank\" href=\"https://{url}\" rel=\"noopener\">首页</a></td></tr></tbody></table></td></tr><tr><td><div style=\"padding:10px 20px;font-size:14px;color:#333333;border-top:1px solid #dbd1ce;font-family:微软雅黑\"><p>你好！你的账号在异地登陆！</p><p>请确定是您自己的登录，以防别人攻击！登录信息如下：</p><p>登陆地区：{address}</p><p></p><p>登录时间：<span style=\"border-bottom:1px dashed #ccc;\" t=\"5\" times=\" 22:38\">{dt}</span> {dh}</p><p></p><p>登录IP：{ip}</p><p></p><p style=\"padding:10px 0;margin-top:30px;margin-bottom:0;color:#a8979a;font-size:12px;border-top:1px dashed #dbd1ce;\">此为系统邮件请勿回复<span style=\"float:right\">{title}</span></p></div></td></tr></tbody></table></div>','email','邮件模版b'),(59,'emailName','3233133033@qq.com','email','用户名'),(60,'emailKey','kedixlposkokfccf','email','邮件key'),(61,'regEmail','<div style=\"width:800px;margin: 0 auto;\"><table width=\"800\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#fefcfa\" style=\"border-radius:5px; overflow:hidden; border-top:4px solid #00c3b6; border-right:1px solid #dbd1ce; border-bottom:1px solid #dbd1ce; border-left:1px solid #dbd1ce;font-family:微软雅黑;\"><tbody><tr><td><table width=\"800\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" height=\"48\"><tbody><tr><td width=\"74\" height=\"35\" border=\"0\" align=\"center\" valign=\"middle\" style=\"padding-left:20px;\"><a href=\"http://{url}\" target=\"_blank\" style=\"text-decoration: none;\" rel=\"noopener\"><span style=\"vertical-align:middle;font-size:20px;line-height:32px;white-space:nowrap;\">{title}</span></a></td><td width=\"703\" height=\"48\" colspan=\"2\" align=\"right\" valign=\"middle\" style=\"color:#333; padding-right:20px;font-size:14px;font-family:微软雅黑\"><a style=\"padding:0 10px;text-decoration:none;\" target=\"_blank\" href=\"https://{url}\" rel=\"noopener\">首页</a></td></tr></tbody></table></td></tr><tr><td><div style=\"padding:10px 20px;font-size:14px;color:#333333;border-top:1px solid #dbd1ce;font-family:微软雅黑\"><p>你好！你正在修改通知邮箱！</p><p>请确定是您自己的操作，以防别人攻击！登录信息如下：</p><p>验证码：{code}</p><p></p><p>操作时间：<span style=\"border-bottom:1px dashed #ccc;\" t=\"5\" times=\" 22:38\">{dt}</span> {dh}</p><p></p><p>登录IP：{ip}</p><p></p><p style=\"padding:10px 0;margin-top:30px;margin-bottom:0;color:#a8979a;font-size:12px;border-top:1px dashed #dbd1ce;\">此为系统邮件请勿回复<span style=\"float:right\">{title}</span></p></div></td></tr></tbody></table></div>','email','修改邮箱验证码'),(62,'msgEmail','<div style=\"width:800px;margin: 0 auto;\"><table width=\"800\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#fefcfa\" style=\"border-radius:5px; overflow:hidden; border-top:4px solid #00c3b6; border-right:1px solid #dbd1ce; border-bottom:1px solid #dbd1ce; border-left:1px solid #dbd1ce;font-family:微软雅黑;\"><tbody><tr><td><table width=\"800\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" height=\"48\"><tbody><tr><td width=\"74\" height=\"35\" border=\"0\" align=\"center\" valign=\"middle\" style=\"padding-left:20px;\"><a href=\"http://{url}\" target=\"_blank\" style=\"text-decoration: none;\" rel=\"noopener\"><span style=\"vertical-align:middle;font-size:20px;line-height:32px;white-space:nowrap;\">{title}</span></a></td><td width=\"703\" height=\"48\" colspan=\"2\" align=\"right\" valign=\"middle\" style=\"color:#333; padding-right:20px;font-size:14px;font-family:微软雅黑\"><a style=\"padding:0 10px;text-decoration:none;\" target=\"_blank\" href=\"https://{url}\" rel=\"noopener\">首页</a></td></tr></tbody></table></td></tr><tr><td><div style=\"padding:10px 20px;font-size:14px;color:#333333;border-top:1px solid #dbd1ce;font-family:微软雅黑\"><p>你好！你的后台登陆验证码！</p><p>请确定是您自己的登录，以防别人攻击！登录信息如下：</p><p>验证码：{code}</p><p></p><p>登录时间：<span style=\"border-bottom:1px dashed #ccc;\" t=\"5\" times=\" 22:38\">{dt}</span> {dh}</p><p></p><p>登录IP：{ip}</p><p></p><p style=\"padding:10px 0;margin-top:30px;margin-bottom:0;color:#a8979a;font-size:12px;border-top:1px dashed #dbd1ce;\">此为系统邮件请勿回复<span style=\"float:right\">{title}</span></p></div></td></tr></tbody></table></div>\n\n','email','邮件模版1'),(58,'emailDuam','465','email','邮件端口'),(69,'beian','备案号','systerm','备案'),(68,'coltd','Copyright © 云回收 版权所有 ','systerm','版权'),(100,'alikey','不能用','fenzhan',NULL),(101,'alixian','1239','fenzhan',NULL),(102,'alitxopen','off','fenzhan',NULL),(99,'alimach','不能用','fenzhan',NULL),(98,'alipid','不能用','fenzhan',NULL),(97,'txopen','off','fenzhan',NULL),(96,'wxxian','200','fenzhan',NULL),(95,'wxfkey','不能用','fenzhan',NULL),(94,'wxsecret','不能用','fenzhan',NULL),(93,'wxappid','不能用','fenzhan',NULL),(103,'emailAps','smtp.qq.com','fenzhan',NULL),(104,'bmtpl','<div class=\"wrapper\" style=\"margin: 20px auto 0; width: 500px; padding-top:16px; padding-bottom:10px;\"><br style=\"clear:both; height:0\"><div class=\"content\" style=\"background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E9E9E9; margin: 2px 0 0; padding: 30px;\"><p>您好: </p><p style=\"border-top: 1px solid #DDDDDD;margin: 15px 0 25px;padding: 15px;\">你的验证码为: {code}</p><p style=\"border-top: 1px solid #DDDDDD; padding-top:6px; margin-top:25px; color:#838383;\"><p>请勿回复本邮件, 此邮箱未受监控, 您不会得到任何回复。</p><p>请不要将验证码告诉他人。</p></p></div></div>','fenzhan',NULL),(105,'bin','【云回收】您的验证码为{code}，在5分钟内有效','fenzhan',NULL),(106,'reg','【云回收】您的验证码为{code}，在5分钟内有效','fenzhan',NULL),(107,'stoken','不能用','fenzhan',NULL),(108,'saeskey','不能用','fenzhan',NULL),(109,'wxyan','off','fenzhan',NULL),(110,'kbank','on','fenzhan',NULL),(111,'kali','on','fenzhan',NULL),(112,'kweixin','off','fenzhan',NULL),(116,'bannerimg1','/static/uploads/20201110/d202921f629668de6c11a1d10743c50e.png','banner','轮播图1'),(113,'bankxian','1000','fenzhan',NULL),(114,'djkj','http://qld.waa.cn','system','低价卡劵'),(115,'wxgzhewm','/static/uploads/20210325/546778e4b03e1aee4b76ec9707f50719.jpg','system','微信公众号二维码'),(117,'bannerimg2','/static/uploads/20201110/3c66f63a7ce884f19c5875462d67de6f.png','banner','轮播图2'),(118,'bannerimg3','/static/uploads/20201110/cc3ba2349afdc7effc8d170d0a4bd0ec.png','banner','轮播图3'),(119,'bannerurl1','1-1','banner','轮播图1链接'),(120,'bannerurl2','2-2','banner','轮播图2链接'),(121,'bannerurl3','3-3','bannerurl3','轮播图3链接');
/*!40000 ALTER TABLE `mq_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_gaoji`
--

DROP TABLE IF EXISTS `mq_gaoji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_gaoji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `license` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `idjust` varchar(150) DEFAULT NULL,
  `idback` varchar(150) DEFAULT NULL,
  `state` int(1) NOT NULL DEFAULT '0',
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_gaoji`
--

LOCK TABLES `mq_gaoji` WRITE;
/*!40000 ALTER TABLE `mq_gaoji` DISABLE KEYS */;
INSERT INTO `mq_gaoji` VALUES (1,2,'/static/uploads/real/20200605/81f121ff28f059ce1ac253fd02050022.png',1591356248,'/static/uploads/real/20200605/ec34d227f41242f9d30e3e9637c4af34.png','/static/uploads/real/20200605/aea8ac1e15b9bdf8110de4d3fe6df29f.png',1,'审核通过'),(2,13,'/static/uploads/real/20201103/fbec2404be5a7f2f57e1419aabefe56f.png',1604383446,'/static/uploads/real/20201103/a462614be189090b573d125d04ab6802.png','/static/uploads/real/20201103/b2d1204b68e5cccd670772854ca6d9fb.png',1,'审核通过'),(3,21,'/static/uploads/real/20201124/5a85761470744b10269255a2f00f67ef.png',1606229369,'/static/uploads/real/20201124/bb1b64bdbfe178e8d5942fbc2dabe7e2.png','/static/uploads/real/20201124/323cbf8faf50f90311754488f0c8f9e0.png',1,'审核通过'),(4,31,'/static/uploads/real/20210615/48a76f7c9c3a07b897163fc09c5d56ba.jpg',1623750731,'/static/uploads/real/20210615/cead08be4409186656095bcc1d86826d.jpg','/static/uploads/real/20210615/169219afb0cc5e5ab9591f566870193a.jpg',1,'审核通过');
/*!40000 ALTER TABLE `mq_gaoji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_login_log`
--

DROP TABLE IF EXISTS `mq_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `data` text,
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1登陆2退出3其他',
  `ip` varchar(200) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=375 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_login_log`
--

LOCK TABLES `mq_login_log` WRITE;
/*!40000 ALTER TABLE `mq_login_log` DISABLE KEYS */;
INSERT INTO `mq_login_log` VALUES (344,28,1620636919,'登陆成功',1,'223.72.88.87【北京市】','北京市'),(343,28,1620636831,'登陆成功',1,'112.96.232.23【广东省】','广东省'),(342,28,1620636770,'登陆成功',1,'58.213.23.162【江苏省南京市】','江苏省南京市'),(341,28,1620636740,'密码错误',1,'58.213.23.162【江苏省南京市】','江苏省南京市'),(340,29,1620635901,'密码错误',1,'122.96.47.17【江苏省南京市】','江苏省南京市'),(339,29,1620635871,'密码错误',1,'122.96.47.17【江苏省南京市】','江苏省南京市'),(338,29,1620635863,'密码错误',1,'122.96.47.17【江苏省南京市】','江苏省南京市'),(337,29,1620635855,'密码错误',1,'122.96.47.17【江苏省南京市】','江苏省南京市'),(336,28,1620632090,'登陆成功',1,'112.96.232.23【广东省】','广东省'),(335,28,1620455716,'登陆成功',1,'112.96.232.156【广东省】','广东省'),(334,28,1620455373,'登陆成功',1,'112.96.232.156【广东省】','广东省'),(333,25,1620401187,'登陆成功',1,'124.130.118.23【山东省临沂市】','山东省临沂市'),(332,27,1617331360,'登陆成功',1,'106.109.67.191【贵州省贵阳市】','贵州省贵阳市'),(331,26,1617094622,'登陆成功',1,'223.104.103.224【河北省邯郸市】','河北省邯郸市'),(330,26,1617093363,'登陆成功',1,'121.24.178.238【河北省邯郸市】','河北省邯郸市'),(329,24,1617092363,'登陆成功',1,'223.104.91.73【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(328,22,1617092356,'密码错误',1,'223.104.91.73【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(327,25,1617092154,'登陆成功',1,'112.251.163.141【山东省临沂市】','山东省临沂市'),(326,23,1617091991,'退出成功',2,'112.251.163.141【山东省临沂市】','山东省临沂市'),(325,23,1617091884,'登陆成功',1,'112.251.163.141【山东省临沂市】','山东省临沂市'),(324,23,1617091867,'退出成功',2,'112.251.163.141【山东省临沂市】','山东省临沂市'),(323,23,1617091657,'登陆成功',1,'112.251.163.141【山东省临沂市】','山东省临沂市'),(322,23,1617091406,'退出成功',2,'112.251.163.141【山东省临沂市】','山东省临沂市'),(321,24,1617091319,'退出成功',2,'223.104.91.73【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(320,24,1617091148,'登陆成功',1,'223.104.91.73【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(319,23,1617090707,'登陆成功',1,'112.251.163.141【山东省临沂市】','山东省临沂市'),(345,22,1623747795,'密码错误',1,'223.104.22.88【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(346,22,1623747796,'密码错误',1,'223.104.22.88【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(347,22,1623747810,'密码错误',1,'223.104.22.88【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(348,22,1623747872,'登陆成功',1,'223.104.22.88【广西壮族自治区南宁市】','广西壮族自治区南宁市'),(349,30,1623748022,'登陆成功',1,'182.239.93.63【香港特别行政区】','香港特别行政区'),(350,30,1623750518,'退出成功',2,'182.239.93.63【香港特别行政区】','香港特别行政区'),(351,31,1623750634,'登陆成功',1,'182.239.93.63【香港特别行政区】','香港特别行政区'),(352,31,1623751080,'登陆成功',1,'182.239.93.63【香港特别行政区】','香港特别行政区'),(353,32,1623800595,'登陆成功',1,'36.170.34.250【四川省成都市】','四川省成都市'),(354,32,1623800655,'登陆成功',1,'36.170.34.250【四川省成都市】','四川省成都市'),(355,31,1623808233,'登陆成功',1,'182.239.93.63【香港特别行政区】','香港特别行政区'),(356,33,1624007643,'登陆成功',1,'36.7.150.37【安徽省合肥市】','安徽省合肥市'),(357,19,1624020444,'退出成功',2,'112.32.2.178【安徽省合肥市】','安徽省合肥市'),(358,34,1624371108,'登陆成功',1,'120.227.43.56【湖南省长沙市】','湖南省长沙市'),(359,34,1635301270,'登陆成功',1,'39.78.51.112【山东省济南市】','山东省济南市'),(360,34,1635301551,'退出成功',2,'39.78.51.112【山东省济南市】','山东省济南市'),(361,35,1635312201,'登陆成功',1,'112.228.205.186【山东省临沂市】','山东省临沂市'),(362,38,1650369124,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(363,25,1667231178,'密码错误',1,'127.0.0.1【未知地址】','未知地址'),(364,25,1667231181,'密码错误',1,'127.0.0.1【未知地址】','未知地址'),(365,25,1667231185,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(366,25,1667658014,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(367,25,1667658070,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(368,25,1667701001,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(369,25,1667701166,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(370,39,1667702732,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(371,39,1667702740,'退出成功',2,'127.0.0.1【未知地址】','未知地址'),(372,39,1667702751,'登陆成功',1,'127.0.0.1【未知地址】','未知地址'),(373,39,1667704203,'退出成功',2,'127.0.0.1【未知地址】','未知地址'),(374,39,1667704398,'登陆成功',1,'127.0.0.1【未知地址】','未知地址');
/*!40000 ALTER TABLE `mq_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_member_rate`
--

DROP TABLE IF EXISTS `mq_member_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_member_rate` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `Cayw56` varchar(50) NOT NULL DEFAULT '98|1',
  `W6mk57` varchar(50) NOT NULL DEFAULT '98|1',
  `8aT861` varchar(50) NOT NULL DEFAULT '98|1',
  `uvcg62` varchar(50) NOT NULL DEFAULT '98|1',
  `HldP66` varchar(50) NOT NULL DEFAULT '98|1',
  `MYzP69` varchar(50) NOT NULL DEFAULT '98|1',
  `Kvt270` varchar(50) NOT NULL DEFAULT '98|1',
  `Aq3C74` varchar(50) NOT NULL DEFAULT '98|1',
  `hHHa75` varchar(50) NOT NULL DEFAULT '98|1',
  `dt2k77` varchar(50) NOT NULL DEFAULT '98|1',
  `tqk786` varchar(50) NOT NULL DEFAULT '98|1',
  `80sO87` varchar(50) NOT NULL DEFAULT '98|1',
  `Cd1T88` varchar(50) NOT NULL DEFAULT '95|1',
  `D0mH89` varchar(50) NOT NULL DEFAULT '98|1',
  `V45j90` varchar(50) NOT NULL DEFAULT '95|1',
  `c9Mi91` varchar(50) NOT NULL DEFAULT '95|1',
  `FjHK92` varchar(50) NOT NULL DEFAULT '95|1',
  `k0sf93` varchar(50) NOT NULL DEFAULT '95|1',
  `HPBw94` varchar(50) NOT NULL DEFAULT '95|1',
  `vKln95` varchar(50) NOT NULL DEFAULT '95|1',
  `P0Oz96` varchar(50) NOT NULL DEFAULT '95|1',
  `cBm897` varchar(50) NOT NULL DEFAULT '95|1',
  `gdav98` varchar(50) NOT NULL DEFAULT '95|1',
  `zMi099` varchar(50) NOT NULL DEFAULT '94|1',
  `8eFx100` varchar(50) NOT NULL DEFAULT '95|1',
  `onrr115` varchar(50) NOT NULL DEFAULT '95|1',
  `ucAw117` varchar(50) NOT NULL DEFAULT '98|1',
  `aO2U138` varchar(50) NOT NULL DEFAULT '98.87|1',
  `cVxy139` varchar(50) NOT NULL DEFAULT '98.86|1',
  `dbVB140` varchar(50) NOT NULL DEFAULT '98.85|1',
  `b2Xq141` varchar(50) NOT NULL DEFAULT '9.6|1',
  `5jdd142` varchar(50) NOT NULL DEFAULT '95.5|1',
  `yrCl143` varchar(50) NOT NULL DEFAULT '86|1',
  `YsRx144` varchar(50) NOT NULL DEFAULT '96|1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_member_rate`
--

LOCK TABLES `mq_member_rate` WRITE;
/*!40000 ALTER TABLE `mq_member_rate` DISABLE KEYS */;
INSERT INTO `mq_member_rate` VALUES (37,2,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.09|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(50,5,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(51,6,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(52,7,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(53,8,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(54,9,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(55,10,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(56,11,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(57,12,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(58,13,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(59,14,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(60,15,'92.00|1','89.00|1','80.00|1','88.00|1','85.00|1','95.00|1','85.00|1','89.00|1','97.00|1','86.00|1','86.00|1','98.00|1','95.00|1','98.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','95.00|1','94.00|1','95.00|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(61,16,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(62,17,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95.00|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(63,21,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(64,20,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(65,22,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(66,23,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(67,24,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(68,25,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(69,26,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(70,27,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(71,28,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(72,29,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(73,30,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(74,31,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(75,32,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','9.6|1','95.5|1','86|1','96|1'),(76,33,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96|1'),(77,34,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96|1'),(78,35,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96.00|1'),(79,36,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96.00|1'),(80,37,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96.00|1'),(81,38,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96.00|1'),(82,39,'98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','98|1','95|1','98|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','95|1','94|1','95|1','95|1','98|1','98.87|1','98.86|1','98.85|1','96.00|1','95.50|1','86.00|1','96.00|1');
/*!40000 ALTER TABLE `mq_member_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_money_log`
--

DROP TABLE IF EXISTS `mq_money_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_money_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `data` text,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '变动金额',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '1单卡对换2批量3提现4加减金额5佣金',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `orderno` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_money_log`
--

LOCK TABLES `mq_money_log` WRITE;
/*!40000 ALTER TABLE `mq_money_log` DISABLE KEYS */;
INSERT INTO `mq_money_log` VALUES (143,31,1623750954,'[商户售卡],98.9元',98.90,2,98.90,'C16237509101014151'),(144,31,1623750954,'[商户售卡],98.9元',98.90,2,197.80,'C16237509090054410'),(145,31,1623750954,'[商户售卡],98.9元',98.90,2,296.70,'C16237509099209580'),(146,31,1623750954,'[商户售卡],98.9元',98.90,2,395.60,'C16237509097902712'),(147,31,1623750954,'[商户售卡],98.9元',98.90,2,494.50,'C16237509097069041'),(148,31,1623750954,'[商户售卡],98.9元',98.90,2,593.40,'C16237509096084800'),(149,31,1623750954,'[商户售卡],98.9元',98.90,2,692.30,'C16237509094927873'),(150,31,1623750965,'[商户售卡],98.9元',98.90,2,791.20,'C16237509113734220'),(151,31,1623750965,'[商户售卡],98.9元',98.90,2,890.10,'C16237509112219580'),(152,31,1623750966,'[商户售卡],98.9元',98.90,2,989.00,'C16237509111251610'),(153,31,1623750966,'[商户售卡],98.9元',98.90,2,1087.90,'C16237509110366221'),(154,31,1623750966,'[商户售卡],98.9元',98.90,2,1186.80,'C16237509109862040'),(155,31,1623750966,'[商户售卡],98.9元',98.90,2,1285.70,'C16237509109057553'),(156,31,1623750966,'[商户售卡],98.9元',98.90,2,1384.60,'C16237509108581321'),(157,31,1623750966,'[商户售卡],98.9元',98.90,2,1483.50,'C16237509107645713'),(158,31,1623750966,'[商户售卡],98.9元',98.90,2,1582.40,'C16237509107205933'),(159,31,1623750966,'[商户售卡],98.9元',98.90,2,1681.30,'C16237509106438633'),(160,31,1623750966,'[商户售卡],98.9元',98.90,2,1780.20,'C16237509106027082'),(161,31,1623750966,'[商户售卡],98.9元',98.90,2,1879.10,'C16237509105066962'),(162,31,1623750966,'[商户售卡],98.9元',98.90,2,1978.00,'C16237509104180532'),(163,31,1623750966,'[商户售卡],98.9元',98.90,2,2076.90,'C16237509103244302'),(164,31,1623750966,'[商户售卡],98.9元',98.90,2,2175.80,'C16237509102428593'),(165,32,1634869478,'[商户售卡],22.2525元',22.25,2,22.25,'C16238013314302521'),(166,32,1634869494,'[订单重设],22.25元',-22.25,4,0.00,'C16238013314302521'),(167,35,1635312472,'[商户售卡],29.334729元',29.33,2,29.33,'C16353123941957421'),(168,35,1635312485,'[商户售卡],29.334729元',29.33,2,58.66,'C16353123941957421'),(169,35,1635312485,'[商户售卡],29.334729元',29.33,2,87.99,'C16353123941058580'),(170,35,1635312485,'[商户售卡],29.334729元',29.33,2,117.32,'C16353123940230933'),(171,35,1635312485,'[商户售卡],29.334729元',29.33,2,146.65,'C16353123939374311'),(172,35,1635312485,'[商户售卡],29.334729元',29.33,2,175.98,'C16353123938324622'),(173,35,1650274130,'[商户售卡],29.334729元',29.33,2,205.31,'C16353123941957421'),(174,32,1650274135,'[商户售卡],22.2525元',22.25,2,1686.84,'C16238013314302521'),(175,32,1650274153,'[商户售卡],3.9375元',3.94,2,1690.78,'C16238010803617632');
/*!40000 ALTER TABLE `mq_money_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_money_order`
--

DROP TABLE IF EXISTS `mq_money_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_money_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(100) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `uid` int(15) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0未支付1支付2过期',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ysk_jiaoyi` varchar(100) DEFAULT NULL COMMENT '微信订单号',
  `ysk_order` decimal(15,2) DEFAULT NULL COMMENT '生成支付金额',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `receivable` int(10) NOT NULL COMMENT '收款账号',
  `umoney` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_money_order`
--

LOCK TABLES `mq_money_order` WRITE;
/*!40000 ALTER TABLE `mq_money_order` DISABLE KEYS */;
INSERT INTO `mq_money_order` VALUES (29,'C15608275261712612','wxpay',2,1560827526,2,12.00,NULL,12.04,0.00,1,0.00),(28,'C15608274984496752','wxpay',2,1560827498,2,12.00,NULL,12.02,0.00,1,0.00),(27,'C15608273944257251','wxpay',2,1560827394,2,12.00,NULL,12.01,0.00,1,0.00),(26,'C15608273250267562','wxpay',2,1560827325,2,12.00,NULL,12.03,0.00,1,0.00),(25,'C15608273091178463','wxpay',2,1560827309,2,12.00,NULL,12.00,0.00,1,0.00),(24,'C15608271680867790','wxpay',2,1560827168,2,12.00,NULL,12.02,0.00,1,0.00),(23,'C15608270570454282','wxpay',2,1560827057,2,12.00,NULL,12.01,0.00,1,0.00),(22,'C15608269169284142','wxpay',2,1560826916,2,12.00,NULL,12.00,0.00,1,0.00),(21,'C15608266588516532','wxpay',2,1560826658,2,12.00,NULL,12.01,0.00,1,0.00),(20,'C15608264406261712','wxpay',2,1560826440,2,12.00,NULL,12.00,0.00,1,0.00),(19,'C15608253722310620','wxpay',2,1560825372,2,12.00,NULL,12.00,0.00,1,0.00),(18,'C15607846442126230','wxpay',2,1560784644,2,12.00,NULL,12.02,0.00,1,0.00),(17,'C15607844603361063','wxpay',2,1560784460,2,12.00,NULL,12.01,0.00,1,0.00),(16,'C15607844104182502','wxpay',2,1560784410,2,12.00,NULL,12.00,0.00,1,0.00),(30,'C15608280658981312','wxpay',2,1560828065,2,12.00,NULL,12.00,0.00,1,0.00),(31,'C15608389101033841','wxpay',2,1560838910,2,12.00,NULL,12.00,0.00,1,0.00),(32,'C15608389256512741','wxpay',2,1560838925,2,12.00,NULL,12.01,0.00,1,0.00),(33,'C15608389372599381','wxpay',2,1560838937,2,12.00,NULL,12.02,0.00,1,0.00),(34,'C15608389477975402','wxpay',2,1560838947,2,50.00,NULL,50.00,0.00,1,0.00),(35,'C15608389548009410','wxpay',2,1560838954,2,50.00,NULL,50.01,0.00,1,0.00),(36,'C15608389845226413','wxpay',2,1560838984,2,50.00,NULL,50.02,0.00,1,0.00),(37,'C15608389919690670','wxpay',2,1560838991,2,50.00,NULL,50.03,0.00,1,0.00),(38,'C15608389987284532','wxpay',2,1560838998,2,50.00,NULL,50.04,0.00,1,0.00),(39,'C15608391043354940','wxpay',2,1560839104,2,50.00,NULL,50.05,0.00,1,0.00),(40,'C15608391797998103','wxpay',2,1560839179,2,150.00,NULL,150.00,0.00,1,0.00),(41,'C15608391987008910','wxpay',2,1560839198,2,150.00,NULL,150.01,0.00,1,0.00),(42,'C15608392399412500','wxpay',2,1560839239,2,150.00,NULL,150.02,0.00,1,0.00),(43,'C15608393620752363','wxpay',2,1560839362,2,150.00,NULL,150.03,0.00,1,0.00),(44,'C15608394260668960','wxpay',2,1560839426,2,50.00,NULL,50.00,0.00,1,0.00),(45,'C15608394424238312','wxpay',2,1560839442,2,50.00,NULL,50.01,0.00,1,0.00),(46,'C15608395445466722','wxpay',2,1560839544,2,50.00,NULL,50.02,0.00,1,0.00),(47,'C15608395770605321','wxpay',2,1560839577,2,12.00,NULL,12.00,0.00,1,0.00),(48,'C15608395856540240','wxpay',2,1560839585,2,50.00,NULL,50.03,0.00,1,0.00),(49,'C15608395968656652','wxpay',2,1560839596,2,150.00,NULL,150.00,0.00,1,0.00),(50,'C15608396024009820','wxpay',2,1560839602,2,150.00,NULL,150.01,0.00,1,0.00),(51,'C15608396210710492','wxpay',2,1560839621,2,150.00,NULL,150.02,0.00,1,0.00),(52,'C15608656210321623','wxpay',2,1560865621,2,12.00,NULL,12.00,0.00,1,0.00),(53,'C15608669014693990','wxpay',2,1560866901,2,12.00,NULL,12.00,0.00,1,0.00),(54,'C15608669606637851','wxpay',2,1560866960,2,12.00,NULL,12.01,0.00,1,0.00),(55,'C15608669937636782','wxpay',2,1560866993,2,12.00,NULL,12.02,0.00,1,0.00),(56,'C15608670861059602','wxpay',2,1560867086,2,12.00,NULL,12.03,0.00,1,0.00),(57,'C15608672343244373','wxpay',2,1560867234,2,12.00,NULL,12.00,0.00,1,0.00),(58,'C15608680126469553','wxpay',2,1560868012,2,12.00,NULL,12.00,0.00,1,0.00),(59,'C15608683287810373','wxpay',2,1560868328,2,12.00,NULL,12.00,0.00,1,0.00),(60,'C15608684376962661','wxpay',2,1560868437,2,12.00,NULL,12.00,0.00,1,0.00),(61,'C15608686194936641','wxpay',2,1560868619,2,12.00,NULL,12.00,0.00,1,0.00),(62,'C15608686486133302','wxpay',2,1560868648,2,12.00,NULL,12.01,0.00,1,0.00),(63,'C15608686919048061','wxpay',2,1560868691,2,12.00,NULL,12.02,0.00,1,0.00),(64,'C15608687265507880','wxpay',2,1560868726,2,12.00,NULL,12.03,0.00,1,0.00),(65,'C15613724876299793','wxpay',2,1561372487,2,12.00,NULL,12.00,0.00,1,0.00),(66,'C15615529678433923','wxpay',2,1561552967,2,12.00,NULL,12.00,0.00,1,0.00);
/*!40000 ALTER TABLE `mq_money_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_news`
--

DROP TABLE IF EXISTS `mq_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `contents` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '1公告6行业资讯7常见问题7',
  `url` varchar(255) DEFAULT NULL,
  `class` int(11) NOT NULL DEFAULT '0' COMMENT '小类',
  `lai` varchar(255) DEFAULT NULL,
  `keyword` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_news`
--

LOCK TABLES `mq_news` WRITE;
/*!40000 ALTER TABLE `mq_news` DISABLE KEYS */;
INSERT INTO `mq_news` VALUES (7,'卖卡流程',NULL,'<p><img src=\"http://rrxk-upload.oss-cn-shenzhen.aliyuncs.com/upload/image/2018/06/08/1528440474218021098.jpg\" title=\"1528440474218021098.jpg\" alt=\"QQ截图20180608144641.jpg\"></p>\n',1,52,NULL,0,NULL,''),(9,'注册协议',NULL,'<p style=\"margin: 10px 0px; padding: 0px; line-height: 4em;\"><font color=\"#333333\" face=\"微软雅黑\"><span style=\"font-size: 18px; background-color: rgb(255, 255, 255);\">我是注册协议，在后台修改</span></font></p>',0,55,NULL,0,NULL,''),(10,'联系我们',NULL,'我是联系我们在后台文章修改',1,50,NULL,0,NULL,''),(11,'交易方式',NULL,'<h3 style=\"margin: 0px; padding: 10px 0px 0px; font-weight: 400; font-size: 18px; color: rgb(0, 132, 0);\">全国交易方式</h3><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 5px 0px;\">不管您身处在哪里，都可以在本站提交本站所支持的卡类进行回收。如遇到问题可随时联系本站客服！</p><p><br></p>',1,51,NULL,0,NULL,''),(12,'隐私政策',NULL,'<p><font color=\"#333333\" face=\"微软雅黑\"><span>我是隐私政策，在后台文章修改</span></font></p>',0,54,NULL,0,NULL,''),(13,'商家合作',NULL,'<p style=\"margin-top: 0px; margin-bottom: 0px; padding: 5px 0px; color: rgb(51, 51, 51); font-family: \" microsoft=\"\" yahei\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 15px;=\"\" white-space:=\"\" normal;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">我是商家合作，在后台文章修改</p><p><br></p>',0,56,NULL,0,NULL,''),(14,'免责声明',NULL,'<p><b>我是免责声明，在后台修改</b></p>',1,57,NULL,0,NULL,''),(20,'更换注册手机号',1604402849,'<p><span>注册我司销卡账号后，如需要更换手机号，可在账号中心进行操作。具体流程：账号中心--资料管理--绑定手机--修改手机号即可</span></p>',1,8,NULL,0,NULL,'注册我司销卡账号后，如需要更换手机号，可在账号中心进行操作。具体流程：账号中心--资料管理--绑定手机--修改手机号即可'),(21,'卡密提交错误怎么办',1604554997,'<p><span>话费卡提交错误可自行在账户中心--卖卡记录--撤销销卡撤销或修改。游戏卡等其他特殊卡种不可撤销，若提交错误请尽快联系在线客服。请谨慎提交卡密，确认无误后再提交，以免给您造成不必要的麻烦！！</span></p>',1,8,NULL,0,NULL,'话费卡提交错误可自行在账户中心--卖卡记录--撤销销卡撤销或修改。游戏卡等其他特殊卡种不可撤销，若提交错误请尽快联系在线客服。请谨慎提交卡密，确认无误后再提交，以免给您造成不必要的麻烦！！'),(22,'公告测试',1604559648,'灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌灌',1,0,NULL,0,NULL,''),(23,'提现额度与手续费',1604559741,'<p>销卡单笔提现限额20万，单笔提现手续费1元，每日销卡不限量，提现不限次数</p>',1,8,NULL,0,NULL,'销卡单笔提现限额20万，单笔提现手续费1元，每日销卡不限量，提现不限次数'),(24,'1111',1604590617,'11111111',1,0,NULL,0,NULL,''),(25,'1111',1604590639,'111111',1,0,NULL,0,NULL,''),(37,'游戏卡与话费卡的不同',1604987160,'<p>游戏卡与话费卡提交后都为系统自动处理，请提交卡密时认真核对卡号卡密，正确选择相对应的面值，如实际面值与提交面值不符合，则需要人工进行修改，不仅费时耗力，还会影响您的后续各项操作，请谨慎操作！</p>',1,8,NULL,0,NULL,'游戏卡与话费卡提交后都为系统自动处理，请提交卡密时认真核对卡号卡密，正确选择相对应的面值，如实际面值与提交面值不符合，则需要人工进行修改，不仅费时耗力，还会影响您的后续各项操作，请谨慎操作！'),(38,'213123',1667231233,'12321',1,5,NULL,0,NULL,'123213'),(39,'转让协议',1667231282,'3123',1,9,NULL,0,NULL,'12321');
/*!40000 ALTER TABLE `mq_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_operator`
--

DROP TABLE IF EXISTS `mq_operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_operator` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `class` varchar(100) NOT NULL,
  `start` int(11) NOT NULL DEFAULT '1',
  `des` text NOT NULL,
  `diction` int(11) NOT NULL DEFAULT '8',
  `url` varchar(200) DEFAULT NULL,
  `qq` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_operator`
--

LOCK TABLES `mq_operator` WRITE;
/*!40000 ALTER TABLE `mq_operator` DISABLE KEYS */;
INSERT INTO `mq_operator` VALUES (12,'手动软件','sendAuto',1,'a:3:{s:7:\"pay_pid\";s:0:\"\";s:7:\"pay_key\";s:0:\"\";s:7:\"pay_3de\";s:0:\"\";}',8,'','1918726988');
/*!40000 ALTER TABLE `mq_operator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_order`
--

DROP TABLE IF EXISTS `mq_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_order` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL COMMENT '运营商',
  `class` int(5) NOT NULL COMMENT '通道',
  `orderno` char(20) NOT NULL COMMENT '订单',
  `tmporder` char(20) DEFAULT NULL COMMENT '来源订单',
  `custom` varchar(255) DEFAULT NULL COMMENT '自定义参数',
  `uid` int(11) NOT NULL COMMENT '商户',
  `notify` varchar(255) DEFAULT NULL COMMENT '异步通知',
  `source` varchar(100) NOT NULL COMMENT '来源网址',
  `money` decimal(10,2) NOT NULL COMMENT '金额',
  `ip` char(20) NOT NULL,
  `state` int(4) NOT NULL DEFAULT '0' COMMENT '0处理中1成功2失败',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '提交时间',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '结算金额',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '返回更新时间',
  `status` int(4) NOT NULL DEFAULT '0' COMMENT '错误状态码',
  `notify_time` timestamp NULL DEFAULT NULL,
  `qiang` int(10) NOT NULL DEFAULT '0',
  `card_no` varchar(200) NOT NULL COMMENT '卡号',
  `card_key` varchar(200) NOT NULL COMMENT '密码',
  `settle_amt` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '实际金额',
  `area` varchar(100) DEFAULT NULL COMMENT '卡地区',
  `remarks` longtext,
  `fenlei` varchar(255) DEFAULT NULL COMMENT '不为空是批量提交',
  `profit` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '利润',
  `consuming` int(11) NOT NULL DEFAULT '0' COMMENT '耗时',
  `kanei` int(1) NOT NULL DEFAULT '0',
  `single` int(1) NOT NULL DEFAULT '0' COMMENT '1批量0单卡',
  `read` int(1) NOT NULL DEFAULT '0',
  `day` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2462 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_order`
--

LOCK TABLES `mq_order` WRITE;
/*!40000 ALTER TABLE `mq_order` DISABLE KEYS */;
INSERT INTO `mq_order` VALUES (2433,12,54,'C16237509094927873',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:09',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750103 193720828854850990','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,45,0,1,1,'2021-06-15'),(2434,12,54,'C16237509096084800',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:09',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750104 193913151420189330','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,45,0,1,1,'2021-06-15'),(2435,12,54,'C16237509097069041',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:09',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750105 193358137649902601','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,45,0,1,1,'2021-06-15'),(2436,12,54,'C16237509097902712',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:09',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750106 193936780306155307','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,45,0,1,1,'2021-06-15'),(2437,12,54,'C16237509099209580',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:09',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750107 193338295751784883','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,45,0,1,1,'2021-06-15'),(2438,12,54,'C16237509090054410',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:09',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750108 193640286470598189','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,45,0,1,1,'2021-06-15'),(2439,12,54,'C16237509101014151',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:55:54',0,NULL,1,'21938190991750109 193955334645387289','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,44,0,1,1,'2021-06-15'),(2440,12,54,'C16237509102428593',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750110 193608389901872237','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2441,12,54,'C16237509103244302',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750111 193945046452202047','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2442,12,54,'C16237509104180532',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750112 193986899619777919','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2443,12,54,'C16237509105066962',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750113 193690571486856047','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2444,12,54,'C16237509106027082',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750114 193840664549373112','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2445,12,54,'C16237509106438633',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750115 193402801932086742','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2446,12,54,'C16237509107205933',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750116 193575083692582968','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2447,12,54,'C16237509107645713',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750117 193643643442795829','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2448,12,54,'C16237509108581321',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750118 193857612916188057','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2449,12,54,'C16237509109057553',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750119 193843167597890773','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2450,12,54,'C16237509109862040',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:10',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750120 193909075622766042','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,56,0,1,1,'2021-06-15'),(2451,12,54,'C16237509110366221',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:11',98.90,'2021-06-15 09:56:06',0,NULL,1,'21938190991750121 193186294466875233','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,55,0,1,1,'2021-06-15'),(2452,12,54,'C16237509111251610',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:11',98.90,'2021-06-15 09:56:05',0,NULL,1,'21938190991750122 193830168949296772','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,54,0,1,1,'2021-06-15'),(2453,12,54,'C16237509112219580',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:11',98.90,'2021-06-15 09:56:05',0,NULL,1,'21938190991750123 193118587458408112','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,54,0,1,1,'2021-06-15'),(2454,12,54,'C16237509113734220',NULL,NULL,31,NULL,'localhost',98.90,'182.239.93.63',1,'2021-06-15 09:55:11',98.90,'2021-06-15 09:56:05',0,NULL,1,'21938190991750124 193141272116462657','',98.90,'香港特别行政区','成功','M16237509094829571',0.00000,54,0,1,1,'2021-06-15'),(2455,12,564,'C16238010803617632',NULL,NULL,32,NULL,'localhost',5.25,'36.170.34.250',1,'2021-06-15 23:51:20',3.94,'2022-04-18 09:29:13',0,NULL,1,'02468875666669 65565556655','',5.25,'四川省成都市','成功','M16238010803509122',-3.93750,26473073,0,1,1,'2021-06-16'),(2456,12,54,'C16238013314302521',NULL,NULL,32,NULL,'localhost',29.67,'36.170.34.250',1,'2021-06-15 23:55:31',22.25,'2022-04-18 09:28:55',0,NULL,1,'288668569669988566   6585655966','',29.67,'四川省成都市','成功','M16238013314200363',-22.25250,26472804,0,1,1,'2021-06-16'),(2457,12,86,'C16353123938324622',NULL,NULL,35,NULL,'localhost',29.67,'112.228.205.186',1,'2021-10-27 05:26:33',29.33,'2021-10-27 05:28:05',0,NULL,1,'11111111111111111','',29.67,'山东省临沂市','成功','M16353123938205002',0.00000,92,0,1,1,'2021-10-27'),(2458,12,86,'C16353123939374311',NULL,NULL,35,NULL,'localhost',29.67,'112.228.205.186',1,'2021-10-27 05:26:33',29.33,'2021-10-27 05:28:05',0,NULL,1,'11111111111111112','',29.67,'山东省临沂市','成功','M16353123938205002',0.00000,92,0,1,1,'2021-10-27'),(2459,12,86,'C16353123940230933',NULL,NULL,35,NULL,'localhost',29.67,'112.228.205.186',1,'2021-10-27 05:26:34',29.33,'2021-10-27 05:28:05',0,NULL,1,'11111111111111113','',29.67,'山东省临沂市','成功','M16353123938205002',0.00000,91,0,1,1,'2021-10-27'),(2460,12,86,'C16353123941058580',NULL,NULL,35,NULL,'localhost',29.67,'112.228.205.186',1,'2021-10-27 05:26:34',29.33,'2021-10-27 05:28:05',0,NULL,1,'11111111111111114','',29.67,'山东省临沂市','成功','M16353123938205002',0.00000,91,0,1,1,'2021-10-27'),(2461,12,86,'C16353123941957421',NULL,NULL,35,NULL,'localhost',29.67,'112.228.205.186',1,'2021-10-27 05:26:34',29.33,'2022-04-18 09:28:50',0,NULL,1,'11111111111111115','',29.67,'山东省临沂市','成功','M16353123938205002',0.00000,14961736,0,1,1,'2021-10-27');
/*!40000 ALTER TABLE `mq_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_pay_order`
--

DROP TABLE IF EXISTS `mq_pay_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_pay_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(100) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `uid` int(15) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `coum` varchar(32) DEFAULT NULL,
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `name` varchar(64) DEFAULT NULL,
  `ysk_jiaoyi` char(50) DEFAULT NULL COMMENT '交易号',
  `ysk_order` decimal(10,2) DEFAULT '0.00' COMMENT '实际金额',
  `website` varchar(200) NOT NULL,
  `class` varchar(100) DEFAULT NULL,
  `notify` varchar(200) NOT NULL,
  `return` varchar(200) NOT NULL,
  `fl` decimal(10,2) NOT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `order` varchar(150) DEFAULT NULL,
  `receivable` int(10) NOT NULL COMMENT '收款账号',
  `count` text,
  `custom` varchar(250) DEFAULT NULL COMMENT '自定义数据',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_pay_order`
--

LOCK TABLES `mq_pay_order` WRITE;
/*!40000 ALTER TABLE `mq_pay_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `mq_pay_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_qiye`
--

DROP TABLE IF EXISTS `mq_qiye`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_qiye` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `number` varchar(100) DEFAULT NULL,
  `license` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `idjust` varchar(150) DEFAULT NULL,
  `idback` varchar(150) DEFAULT NULL,
  `idCardStartDate` timestamp NULL DEFAULT NULL,
  `idCardEndDate` timestamp NULL DEFAULT NULL,
  `state` int(1) NOT NULL DEFAULT '0',
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_qiye`
--

LOCK TABLES `mq_qiye` WRITE;
/*!40000 ALTER TABLE `mq_qiye` DISABLE KEYS */;
/*!40000 ALTER TABLE `mq_qiye` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_user`
--

DROP TABLE IF EXISTS `mq_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `key` varchar(32) DEFAULT NULL,
  `old_login_time` int(11) DEFAULT NULL,
  `state` int(1) DEFAULT '1' COMMENT '0未审核1正常2冻结',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `addtime` int(11) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0' COMMENT '是否开通提现',
  `dx` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(100) NOT NULL,
  `assets` int(11) DEFAULT NULL,
  `password` text,
  `ip` varchar(100) DEFAULT NULL,
  `old_ip` varchar(150) DEFAULT NULL,
  `login_time` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '冻结金额',
  `fenzhan` int(1) NOT NULL DEFAULT '0' COMMENT '是否开通分站',
  `email` varchar(200) DEFAULT NULL,
  `opapi` int(1) NOT NULL DEFAULT '1' COMMENT '总Api开关',
  `yongjin` decimal(15,2) NOT NULL DEFAULT '0.00',
  `numip` int(1) NOT NULL DEFAULT '1' COMMENT 'ip白名单数量',
  `token` varchar(255) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  `adress` varchar(150) DEFAULT NULL,
  `real` int(11) NOT NULL DEFAULT '0' COMMENT '实名认证id',
  `username` varchar(200) DEFAULT NULL,
  `idcard` varchar(150) DEFAULT NULL,
  `jiaoyimima` varchar(255) DEFAULT NULL,
  `realtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_user`
--

LOCK TABLES `mq_user` WRITE;
/*!40000 ALTER TABLE `mq_user` DISABLE KEYS */;
INSERT INTO `mq_user` VALUES (13,'15711012878','kw8kVMh0zueGclYY',1606447171,1,9551.43,1604381560,0,0,'15711012878',NULL,'$2y$10$NV6YM0ZYiYgx.RAXkKIz9OYYbeYAS.lYiIS/o8TczAUTnBFuTypEy','111.197.22.83','111.197.22.83',1606489717,0.00,0,'1918726988@qq.com',1,0.00,1,'0e878d56fc89509f4dba8e2ff182487d877124b7',1606576117,'1918726988','北京市',1,'昊天','130724199812244115','$2y$10$8n.0Gfqe7WbwmlHIwOlZCe7JJCWU4gfDOu3p3jUeup3LDMhZ/fP3u',1604383424),(18,'17691398359','VRmZX2OEJa8YC5Ot',NULL,1,68945.13,1606140132,0,0,'17691398359',NULL,'$2y$10$X2awRbPvygOasC5pRkDiY.Q6LL9yiAHR7sJivVbawLLUcHCbUrHw6','123.139.82.120','123.139.82.120',1606140154,0.00,0,NULL,1,0.00,1,'a878656955d9deb5a7c92f6efe22023dcf189d72',1606226554,NULL,'陕西省西安市',0,NULL,NULL,NULL,NULL),(19,'13065182660','LBZzieVCQfyluyXn',NULL,1,52974.47,NULL,0,0,'13065182660',NULL,'$2y$10$1G5zBsK26swPGtCeKt82IesY64ln5Gmb.rpwCRJDYysmNvmsEFTsy','220.175.61.200',NULL,NULL,0.00,0,'',1,0.00,1,'',0,'','江西省南昌市',0,NULL,NULL,NULL,NULL),(20,'15662231489','7zddx2p5i4yqQtHs',NULL,1,179653.85,NULL,0,0,'15662231489',NULL,'$2y$10$If3EIJPT9QKfSZ/tSp.jnunppfKt0DZU85SHxXtM686WfIdlIc3Zq','111.31.55.46','111.31.55.46',1606142932,0.00,0,'',1,0.00,1,'1d105f3eb82d945f43541e3bfaff1739c2cd5694',1606229332,'','天津市',0,NULL,NULL,NULL,NULL),(21,'17633639936','w5QaDBD49QUgBq3a',1606313645,1,846.23,1606228436,0,0,'17633639936',NULL,'$2y$10$9VavhQFhCGJFpKf1auvfVufjCuNj/TXu0WdT3R7TQScjCfR.Em0Re','111.197.22.83','111.197.22.83',1606397874,0.00,0,'',1,0.00,1,'4ed1d9d34a96e4e1cd7e84a810b76b1a97e75cd3',1606484274,'','北京市',1,'冯海','411329198202102818','$2y$10$X8PrGTW7/h211.SBf7pdyeFhUwS2UGtiq.m.4aLBLXuhx4v53zj6W',1606229246),(22,'15012345678','KaqbR0s3OVmi9aHF',NULL,1,15607.81,NULL,0,0,'15012345678',NULL,'$2y$10$8Qz12b0Qif2lqGBnsgoJnOPEGQ8GdbAVY5BefDNbZbisAjsmg2wvi','223.104.22.88','223.104.91.73',1623747871,0.00,0,'',1,0.00,1,'384035f3da4fdcaa78d2c733928eb68e2dfc276b',1623834271,'','广西壮族自治区南宁市',0,NULL,NULL,NULL,NULL),(24,'15078728282','5o8zmoDmEjYV2lAH',1617091148,1,1745.25,1617090726,0,0,'15078728282',NULL,'$2y$10$wbIPV2R/3cxvGTn.5ruQWu3dqzDArttAdVrdVXlAGKHcXr8.aa99i','223.104.91.73','223.104.91.73',1617092363,0.00,0,NULL,1,0.00,1,'55290c4bba24d0642f9aea7b1778c03b8d6d4878',1617178763,NULL,'广西壮族自治区南宁市',0,NULL,NULL,NULL,NULL),(25,'13764694699','1U9sZVMKHBncFzvb',1667701000,1,962.18,1617092147,0,0,'13764694699',NULL,'$2y$10$fWOwf7Mwl3P1tz3pqU0eTe5RAkU99JNO51dffFAS.OaXRKKIofoJO','127.0.0.1','127.0.0.1',1667701165,0.00,0,NULL,1,0.00,1,'1b5acc0a2465dc283867da78bad8c36982b50d7c',1667787565,NULL,'山东省临沂市',0,NULL,NULL,NULL,NULL),(26,'15511279506','ZxOMPhdzrxwPa176',1617093363,1,4326.47,1617093356,0,0,'15511279506',NULL,'$2y$10$xIUnl7XO3T/EJP0dMXzVle9nPEGaKGCVqpTPeNJStD.qPq6C281m2','223.104.103.224','121.24.178.238',1617094622,0.00,0,NULL,1,0.00,1,'3adbac64260df219132b6e6e40afb8661ea9c854',1617181022,NULL,'河北省邯郸市',0,NULL,NULL,NULL,NULL),(27,'13315318016','Q4t7iCtKD3AGO4mS',NULL,1,23846.85,NULL,0,0,'15616194980',NULL,'$2y$10$bphRu/5d9q0cIJZJK1yYAOtDwbTrIl1s49ixOkuyDsCpIrPnWeAoe','106.109.67.191','106.109.67.191',1617331360,0.00,0,'',1,0.00,1,'b87d29b68790270d05c2702310e6166fd9762224',1617417760,'','贵州省贵阳市',0,NULL,NULL,NULL,NULL),(28,'16604744164','7fQD9HCKXjOzg3RB',1620636831,1,7921.24,1620455359,0,0,'16604744164',NULL,'$2y$10$jVNYgqPFYnhbCK08nxNZke34v4UkGHTI0qrqEWYdaUJkiCAUZoNYC','223.72.88.87','112.96.232.23',1620636919,0.00,0,NULL,1,0.00,1,'10f9d61c6a87c83904db25893850fb351cd81258',1620723319,NULL,'广东省',1,'郭学光','150421198009120627',NULL,1620455971),(29,'18652943639','9eGbmd7F3ArJqwUq',NULL,1,2279.32,NULL,0,0,'18652943639',NULL,'$2y$10$O13VOWOQT2XEWDhdz.mMXeimxAN6dAFoyRaGSavtfaybZ6Od1Pn2K','117.65.182.68',NULL,NULL,0.00,0,'',1,0.00,1,NULL,NULL,'','安徽省马鞍山市',0,NULL,NULL,NULL,NULL),(31,'15363509448','00XxOKHzctzHOjlm',1623751079,1,2175.80,1623750622,0,0,'15363509448',NULL,'$2y$10$LRQqm7sWnzrX0r09kz2noejSkGKZKtWAi4O6lCd36b5ziGa2YRILy','182.239.93.63','182.239.93.63',1623808233,0.00,0,NULL,1,0.00,1,'90ffedabc17101eacf436331d57d63b9d89f4e0a',1623894633,NULL,'香港特别行政区',1,'宋晓波','510521200010041873','$2y$10$uUV2EHvIYvDJhPWThPq2dubwOoRgiYE2LhF2YLuBOQN8MDUxON58O',1623750707),(32,'13488907005','MuOoJfA42sINzdDY',1623800595,1,1690.78,1623800591,0,0,'13488907005',NULL,'$2y$10$Rn5o.6FQxn0M.B7rVF4axORdF8FmE2B12EY7X9LtEKaUv6oQVU.r6','36.170.34.250','36.170.34.250',1623800655,0.00,0,NULL,1,0.00,1,'e69e9bb911421b7ca0c9afbbceb40c8379ba535d',1623887055,NULL,'四川省成都市',1,'王勇','510524199104104154','$2y$10$oa5ge1qNHEsn4M9ex8ynROZqQzYT0Q1lHH6cmmxvZoHJm4WnlAUQ.',1623801040),(33,'15156326112','ZSwadV4nGuqfqX97',NULL,1,521.14,NULL,0,0,'15156326112',NULL,'$2y$10$KdjGhrt8NDjp6Tz1PE/dRekSh95qiDvVssokZf6gi/u6x0ZcH8/CK','36.7.150.37','36.7.150.37',1624007642,0.00,0,'',1,0.00,1,'af7837c7a832c7b4fd07c399243d33a28faa5338',1624094042,'','安徽省合肥市',0,NULL,NULL,NULL,NULL),(34,'15580905678','7lBV1BjNryzDX7SR',1624371108,1,5623.15,NULL,0,0,'15580905678',NULL,'$2y$10$5Wmm0RDEUQJ0TqELHGGSz.2HDxsEm7l2Ru4Ha6cHW0JODVsIdgxzS','39.78.51.112','120.227.43.56',1635301269,0.00,0,'',1,0.00,1,'',0,'','湖南省长沙市',1,'喻凯','430124198709152996',NULL,1624371264),(35,'13181223071','JSCZaPtbgfrP6YyO',NULL,1,205.31,1635312193,0,0,'13181223071',NULL,'$2y$10$8p..34eI/HMgvl5BilWanOCLwo/xfXKfCWnUf1sjPbdFB7zDdOi4e','112.228.205.186','112.228.205.186',1635312201,0.00,0,'',1,0.00,1,'65084d6502fce25b62484ed00ed4a5912466b787',1635398601,'','山东省临沂市',1,'杜雨泽','371324199010065285',NULL,1635312380),(36,'15715937779',NULL,NULL,1,3690.06,1635316832,0,0,'15715937779',NULL,'$2y$10$trdBf4lWY0noiR6QecG/ueYsxKRQWCY1TAd2GegiLgwhkLZ7S4kHy',NULL,NULL,NULL,0.00,0,'95212545@qq.com',1,0.00,1,NULL,NULL,'2793908586',NULL,0,NULL,NULL,NULL,NULL),(37,'13001702030',NULL,NULL,1,2851.17,1635317085,0,0,'13001702030',NULL,'$2y$10$Xn9rvqOuYoLNzj0LdS2IG.MmIWYMyGhnFYD9Rv8wFc18uo7KeAH1G',NULL,NULL,NULL,0.00,0,'651255511@163.com',1,0.00,1,NULL,NULL,'13001702030',NULL,0,NULL,NULL,NULL,NULL),(38,'13315888642','',NULL,1,3276.23,1635317154,0,0,'13315318016',NULL,'$2y$10$n/eWitIDSmq5Shy0o61R3udmgrEScfBrsskPeLx3R/GhAViJF.Toi','127.0.0.1',NULL,1650369124,0.00,0,'36254562@qq.com',1,0.00,1,'ab7ce0034af002667074f67c34db987c0d3f3bb5',1650455524,'36254562','未知地址',0,NULL,NULL,NULL,NULL),(39,'13333333333',NULL,1667702751,1,0.00,1667702723,0,0,'13333333333',NULL,'$2y$10$oR.cJe6B4l1yYYkoceq2y.rNW3HFQO38BOECy7YbDWGyd3MjCwhay','127.0.0.1','127.0.0.1',1667704397,0.00,0,'3123@qq.com',1,0.00,1,'17bc56d648e6f0edb5eef642d6e3c6e60059f5bf',1667790797,'123','未知地址',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `mq_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_userbank`
--

DROP TABLE IF EXISTS `mq_userbank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_userbank` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `bankname` varchar(100) NOT NULL,
  `accounts` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `bankid` int(10) NOT NULL DEFAULT '2',
  `isdel` int(1) NOT NULL DEFAULT '1' COMMENT '0删除1存在',
  `create_time` int(11) NOT NULL,
  `hread` varchar(255) DEFAULT NULL,
  `zhibank` varchar(200) DEFAULT NULL,
  `province` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `county` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_userbank`
--

LOCK TABLES `mq_userbank` WRITE;
/*!40000 ALTER TABLE `mq_userbank` DISABLE KEYS */;
INSERT INTO `mq_userbank` VALUES (54,2,'支付宝','99156787@qq.com','桁架',2,1,1576563510,NULL,NULL,NULL,NULL,NULL),(62,2,'中国工商银行','4561325616351561685','桁架',2,1,1591664836,NULL,'户撒解决空军空军陆军哈','河北省','承德市','滦平镇'),(55,2,'建设银行.龙卡储蓄卡','6227003276530128999','桁架',2,1,1590850152,NULL,NULL,NULL,NULL,NULL),(59,2,'平安银行','6227003276530121111','桁架',2,1,1590851052,NULL,'454654654564','上海市','上海市','长宁区'),(61,2,'支付宝','31111@qq.com','桁架',2,1,1591663958,NULL,NULL,NULL,NULL,NULL),(63,17,'支付宝','13777689208@163.com','许艳婷',2,1,1604473316,NULL,NULL,NULL,NULL,NULL),(64,13,'支付宝','44154236@qq.com','昊天',2,1,1604981624,NULL,NULL,NULL,NULL,NULL),(65,21,'支付宝','3465665161@qq.com','冯海',2,1,1606229464,NULL,NULL,NULL,NULL,NULL),(66,31,'支付宝','15355556666','宋晓波',2,1,1623750839,NULL,NULL,NULL,NULL,NULL),(67,31,'中国工商银行','6222666677778888987','宋晓波',2,1,1623750871,NULL,'搞活动搞的','山东省','临沂市','费城镇');
/*!40000 ALTER TABLE `mq_userbank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_userpay`
--

DROP TABLE IF EXISTS `mq_userpay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_userpay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `quota` int(20) NOT NULL DEFAULT '0' COMMENT '限额',
  `state` int(1) NOT NULL DEFAULT '1' COMMENT '0：关闭1：正常',
  `account` varchar(255) NOT NULL,
  `addtime` int(11) DEFAULT NULL,
  `isxt` int(1) NOT NULL DEFAULT '0' COMMENT '0普通1系统',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0正常1额度限制',
  `bid` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_userpay`
--

LOCK TABLES `mq_userpay` WRITE;
/*!40000 ALTER TABLE `mq_userpay` DISABLE KEYS */;
INSERT INTO `mq_userpay` VALUES (1,1,'wxpay',2000,1,'a123456',1560743243,1,0,72),(7,2,'alipay',500,1,'超级管理员',1561455767,0,0,78),(8,2,'wxpay',5000,1,'一往无前',1561457524,0,0,79);
/*!40000 ALTER TABLE `mq_userpay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_webjs`
--

DROP TABLE IF EXISTS `mq_webjs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_webjs` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sum_recaharge` decimal(10,2) NOT NULL COMMENT '总充值',
  `rech_fl` decimal(10,2) NOT NULL COMMENT '充值手续费',
  `sum_tx` decimal(10,2) NOT NULL COMMENT '总提现',
  `tx_fl` decimal(10,2) NOT NULL COMMENT '提现手续费',
  `kt_fl` decimal(10,2) NOT NULL COMMENT '开通总费用',
  `domain_fl` decimal(10,2) NOT NULL COMMENT '域名续费',
  `dxpay` decimal(10,0) NOT NULL COMMENT '短信购买',
  `addtime` int(11) DEFAULT NULL,
  `payfl` decimal(10,2) NOT NULL COMMENT '购买费率',
  `feng` decimal(10,2) NOT NULL COMMENT '开通分站',
  `lirun` decimal(10,2) NOT NULL COMMENT '利润',
  `yongjin` decimal(10,2) NOT NULL COMMENT '佣金',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_webjs`
--

LOCK TABLES `mq_webjs` WRITE;
/*!40000 ALTER TABLE `mq_webjs` DISABLE KEYS */;
INSERT INTO `mq_webjs` VALUES (53,0.00,0.00,0.00,0.00,440.00,60.00,2,1561553801,0.00,0.00,502.00,0.00);
/*!40000 ALTER TABLE `mq_webjs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_withdraw`
--

DROP TABLE IF EXISTS `mq_withdraw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `money` decimal(10,2) NOT NULL,
  `type` int(1) NOT NULL,
  `zhname` varchar(50) NOT NULL DEFAULT 'null',
  `zh` varchar(200) NOT NULL DEFAULT 'null',
  `shtime` int(11) DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `txtype` int(1) NOT NULL DEFAULT '1' COMMENT '1余额2佣金',
  `umoney` decimal(10,2) NOT NULL DEFAULT '0.00',
  `read` int(1) NOT NULL DEFAULT '0',
  `dizhi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_withdraw`
--

LOCK TABLES `mq_withdraw` WRITE;
/*!40000 ALTER TABLE `mq_withdraw` DISABLE KEYS */;
/*!40000 ALTER TABLE `mq_withdraw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mq_yongjin_log`
--

DROP TABLE IF EXISTS `mq_yongjin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mq_yongjin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `data` text,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '变动金额',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '1充值2汇款3短信4域名5开通',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '佣金',
  `formID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mq_yongjin_log`
--

LOCK TABLES `mq_yongjin_log` WRITE;
/*!40000 ALTER TABLE `mq_yongjin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `mq_yongjin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notifier_id` int(11) NOT NULL DEFAULT '0',
  `recipient_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `text` text,
  `url` varchar(3000) NOT NULL DEFAULT '',
  `seen` varchar(50) NOT NULL DEFAULT '0',
  `time` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `type` (`type`),
  KEY `seen` (`seen`),
  KEY `notifier_id` (`notifier_id`),
  KEY `time` (`time`),
  KEY `music_id` (`track_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,2,1,0,0,'follow_user',NULL,'','1560842550','1559829934'),(2,1,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(3,3,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(4,4,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(5,5,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(6,6,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(7,7,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(8,8,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(9,9,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(10,10,2,0,0,'follow_user',NULL,'','1562759305','1559963666'),(11,1,2,1,0,'liked_track',NULL,'track/yuxt4fdaEoevvmk','1562759305','1560903131');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_songs`
--

DROP TABLE IF EXISTS `playlist_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `playlist_id` (`playlist_id`),
  KEY `song_id` (`track_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_songs`
--

LOCK TABLES `playlist_songs` WRITE;
/*!40000 ALTER TABLE `playlist_songs` DISABLE KEYS */;
INSERT INTO `playlist_songs` VALUES (1,1,1,2,1559963151);
/*!40000 ALTER TABLE `playlist_songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists`
--

DROP TABLE IF EXISTS `playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `privacy` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(120) NOT NULL,
  `uid` varchar(12) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `user_id` (`user_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists`
--

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;
INSERT INTO `playlists` VALUES (1,'山山水水',2,0,'upload/photos/2019/06/iv8DKoXiXHVRH1ZliMno_08_5a22617ae7d84a90ca2fe237a3a0629c_image.png','e164tcNoVBKE',1559963133);
/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `track_owner_id` int(11) NOT NULL DEFAULT '0',
  `final_price` float NOT NULL DEFAULT '0',
  `commission` float NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `track_id` (`track_id`),
  KEY `timestamp` (`timestamp`),
  KEY `time` (`time`),
  KEY `track_owner_id` (`track_owner_id`),
  KEY `final_price` (`final_price`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `time` int(11) NOT NULL DEFAULT '0',
  `seen` int(11) unsigned DEFAULT '0',
  `ignored` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `comment_id` (`comment_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searches`
--

DROP TABLE IF EXISTS `searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(250) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searches`
--

LOCK TABLES `searches` WRITE;
/*!40000 ALTER TABLE `searches` DISABLE KEYS */;
INSERT INTO `searches` VALUES (1,'道姑',16,1559832633),(2,'尼姑',2,1559882318),(3,'洛雷姆·伊普索姆·多尔坐在阿米特的座位上，他是一位敬业的精英，他用自己的节奏煽动着劳勃和多洛尔·马格纳·阿利夸尔。奎斯-伊普桑怀疑地心引力。里苏斯·康莫多·维韦拉·梅塞纳斯·阿库姆桑·拉库斯·维尔的面部表情。',3,1561516791);
/*!40000 ALTER TABLE `searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `platform` varchar(30) NOT NULL DEFAULT 'web',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `platform` (`platform`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (23,'cdd53720b3b8a61c83d8c5c41a92e0aaa30f40b21563520471b3ff82970aa1c3889a5a690689eeda05',1,'web',1563520471),(24,'9282a5e9292f660364bb35c5b88e8f3705c2fcf41606566148a7342226428cd581bcb3dd032e2e41d0',1,'web',1606566148);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ads`
--

DROP TABLE IF EXISTS `site_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placement` varchar(50) NOT NULL DEFAULT '',
  `code` text,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `placement` (`placement`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ads`
--

LOCK TABLES `site_ads` WRITE;
/*!40000 ALTER TABLE `site_ads` DISABLE KEYS */;
INSERT INTO `site_ads` VALUES (1,'header','',0),(2,'footer','',0),(3,'side_bar','',0);
/*!40000 ALTER TABLE `site_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_price`
--

DROP TABLE IF EXISTS `song_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` decimal(20,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_price`
--

LOCK TABLES `song_price` WRITE;
/*!40000 ALTER TABLE `song_price` DISABLE KEYS */;
INSERT INTO `song_price` VALUES (1,1.99),(2,2.99),(3,4.99),(4,9.99),(5,19.99);
/*!40000 ALTER TABLE `song_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs`
--

DROP TABLE IF EXISTS `songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `audio_id` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `tags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `thumbnail` varchar(150) NOT NULL DEFAULT 'default',
  `availability` int(11) NOT NULL DEFAULT '0',
  `age_restriction` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `artist_id` int(11) NOT NULL DEFAULT '0',
  `album_id` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `duration` varchar(12) NOT NULL,
  `demo_duration` varchar(10) NOT NULL DEFAULT '0:0',
  `audio_location` varchar(120) NOT NULL DEFAULT '',
  `demo_track` varchar(200) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `registered` varchar(12) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `dark_wave` varchar(120) NOT NULL DEFAULT '',
  `light_wave` varchar(120) NOT NULL DEFAULT '',
  `shares` int(11) NOT NULL DEFAULT '0',
  `spotlight` int(11) NOT NULL DEFAULT '0',
  `ffmpeg` int(11) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`),
  KEY `views` (`views`),
  KEY `artist_id` (`artist_id`),
  KEY `album_id` (`album_id`),
  KEY `price` (`price`),
  KEY `audio_id` (`audio_id`),
  KEY `registered` (`registered`),
  KEY `spotlight` (`spotlight`),
  KEY `category_id` (`category_id`),
  KEY `ffmpeg` (`ffmpeg`),
  KEY `age_restriction` (`age_restriction`),
  KEY `time` (`time`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `tags` (`tags`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs`
--

LOCK TABLES `songs` WRITE;
/*!40000 ALTER TABLE `songs` DISABLE KEYS */;
INSERT INTO `songs` VALUES (1,2,'yuxt4fdaEoevvmk','我的一个道姑朋友','恋恋故人难 / 双笙','我的一个道姑朋友','upload/photos/2019/06/r84PfYUqeJ3mDo8kAI9Z_06_9b1d053f29b90d08b62853fd37d18292_image.jpg',0,0,1559831969,0,0,0,0,'4:44','0:0','upload/audio/2019/06/ZEeqEiKc6fY3Fbed9xX6_06_44090948cba2ce01cbcd647c7a0160de_audio.mp3','',1,'2019/6',4546604,'upload/waves/2019/06/8cqLOxxEXGjAxxDG2ultmKNzHUc1McWuYu2nnd5B_dark.png','upload/waves/2019/06/uTddV88x1Q6BIy1W61XK6HJvQNPwFmmSXQWmyagH_light.png',3,0,0),(2,2,'WFrcYdpBESw4Jx1','南昌Djkevin-打造多想平庸的拥抱你只是太爱你嗨串送徒弟小柚子','南昌Djkevin-打造多想平庸的拥抱你只是太爱你嗨串送徒弟小柚子','南昌Djkevin','upload/photos/2019/06/KXVYha21tD2UtrmLaFya_08_aab8a03f51dc97ee81cc6acf43c86a26_image.png',0,0,1559966648,0,0,0,0,'1:05:05','0:0','upload/audio/2019/06/KTA2WDcdZEEbUjvUWH21_08_d75759899c9cd0f4cd89fea9fa9162e6_audio.mp3','',1,'2019/6',31237146,'upload/waves/2019/06/LbMCh9BC6Dv6N5xYEEXAWrL4m89u7DPoG11ollCw_dark.png','upload/waves/2019/06/L4J33PB86gXaoRA6hKFTcTtsHqLAT7Vy6553aZH3_light.png',0,0,0),(3,2,'QbLig76kQDPoD8E','小奶瓶 - 唯美曲儿循环百遍（第2季）','小奶瓶 - 唯美曲儿循环百遍（第2季）','小奶瓶','upload/photos/2019/06/wEfUjuY4tJqDfduhKrEK_19_a1f269d260ba623358c6b94489ee50c7_image.jpg',0,0,1560905011,0,0,0,0,'2:25','0:0','upload/audio/2019/06/olDyIaEOk1hWJm3BmMer_19_c833744d29e5e004c61470125749c8cf_audio.mp3','',11,'2019/6',6512674,'upload/waves/2019/06/MCSiAPt19shfyl8t7jtadEaPK7ZMTxtjeRbffkEq_dark.png','upload/waves/2019/06/rOkQk9t5oMVqaz547BkVTjqBsYqc8jxuY9y9m33K_light.png',0,0,0),(4,2,'Ehblbp6dhUN3bst','小奶瓶 - 气质电音超嗨享受','小奶瓶 - 气质电音超嗨享受','电音,小奶瓶','upload/photos/2019/06/Fpzb7EvOefmqNpgcCTbR_19_e1e42c55b92ad45796506b31767a7e37_image.jpg',0,0,1560905356,0,0,0,0,'2:54','0:0','upload/audio/2019/06/zLhfJjl3p9LTXIzsLYJS_19_383a21bd1a611619d684f4e6fe301aeb_audio.mp3','',11,'2019/6',7671430,'upload/waves/2019/06/pyUlM2lGhDFyg9waMXsZgqtK9BFWlamMCpjEZhWY_dark.png','upload/waves/2019/06/6A7aNropOdcUmRAzMFvvtBB9sjmTABoqMMGZI8Jq_light.png',0,0,0),(5,2,'Pu6l55J6yD6ZGDf','小奶瓶 - 超爽车载串烧开场必秒','小奶瓶 - 超爽车载串烧开场必秒','小奶瓶,超爽车载,串烧','upload/photos/2019/06/EZ8DJxCVR8QjpOMP5r52_19_ea4cdf00ffa1b3db6bc6dce2774fc95c_image.jpg',0,0,1560908525,0,0,0,0,'34:01','0:0','upload/audio/2019/06/so7GBvvyLiyvXSABvQ2P_19_4e94f22554d7e05b87d4e9d53d7a4c9a_audio.mp3','',1,'2019/6',82378784,'upload/waves/2019/06/u3aOT2ECgOfG6K285BEQ5xDfU8plmyPqJQGeakwk_dark.png','upload/waves/2019/06/8PYbPhR4TtxDpiGeDOw28BNPdCbWeUWxQqdyFnhY_light.png',0,0,0),(6,2,'sN3O17s3ixFHHk6','战鼓-经典越南鼓','战鼓-经典越南鼓','战鼓,经典越南鼓','upload/photos/2019/06/LFrAMfdGtAHcAjrR4zsC_19_d1a487f1eca25b261c3dd012001acfad_image.png',0,0,1560909321,0,0,0,0,'54:25','0:0','upload/audio/2019/06/EvXFyY1EhVSHxu7lPoPu_19_9e1feff8550816d1a30a66b288093039_audio.mp3','',0,'2019/6',130719856,'','',0,1,0);
/*!40000 ALTER TABLE `songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terms`
--

DROP TABLE IF EXISTS `terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL DEFAULT '',
  `content` longtext,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terms`
--

LOCK TABLES `terms` WRITE;
/*!40000 ALTER TABLE `terms` DISABLE KEYS */;
INSERT INTO `terms` VALUES (1,'terms','&lt;h4&gt;1- Write your Terms Of Use here.&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adisdpisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis sdnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt; &lt;br&gt;    &lt;h4&gt;2- Random title&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt;'),(2,'about','&lt;h4&gt;1- Write your About us here.&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adisdpisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis sdnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt; &lt;br&gt;                &lt;br&gt; &lt;br&gt;                &lt;h4&gt;2- Random title&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt;'),(3,'privacy','&lt;h4&gt;1- Write your Privacy Policy here.&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adisdpisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis sdnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt; &lt;br&gt;                &lt;br&gt; &lt;br&gt;                &lt;h4&gt;2- Random title&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt;');
/*!40000 ALTER TABLE `terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_interest`
--

DROP TABLE IF EXISTS `user_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_interest`
--

LOCK TABLES `user_interest` WRITE;
/*!40000 ALTER TABLE `user_interest` DISABLE KEYS */;
INSERT INTO `user_interest` VALUES (1,2,1),(2,1,9),(3,1,19);
/*!40000 ALTER TABLE `user_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `ip_address` varchar(150) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `password` varchar(150) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT 'male',
  `email_code` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `language` varchar(22) CHARACTER SET latin1 NOT NULL DEFAULT 'english',
  `avatar` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'upload/photos/d-avatar.jpg',
  `cover` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'upload/photos/d-cover.jpg',
  `src` varchar(22) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `age` int(11) NOT NULL DEFAULT '0',
  `about` text COLLATE utf8_unicode_ci,
  `google` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `facebook` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `twitter` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `instagram` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL DEFAULT '0',
  `verified` int(11) NOT NULL DEFAULT '0',
  `last_active` int(11) NOT NULL DEFAULT '0',
  `registered` varchar(40) CHARACTER SET latin1 NOT NULL DEFAULT '0000/00',
  `uploads` float NOT NULL DEFAULT '0',
  `wallet` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `balance` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `website` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `artist` int(11) NOT NULL DEFAULT '0',
  `is_pro` int(11) NOT NULL DEFAULT '0',
  `pro_time` int(11) NOT NULL DEFAULT '0',
  `last_follow_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `password` (`password`),
  KEY `last_active` (`last_active`),
  KEY `admin` (`admin`),
  KEY `active` (`active`),
  KEY `registered` (`registered`),
  KEY `wallet` (`wallet`),
  KEY `balance` (`balance`),
  KEY `pro_time` (`pro_time`),
  KEY `country_id` (`country_id`),
  KEY `verified` (`verified`),
  KEY `artist` (`artist`),
  KEY `is_pro` (`is_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','yunmazhijia@qq.com','','$2y$10$XJYNSUb.dHvAb7RtPjNXCe0tUzDqtVbkRs9DiFxEn9i5Pb4cTzZ8G','','male','','china','upload/photos/d-avatar.jpg','upload/photos/d-cover.jpg','',0,0,NULL,'','','','',1,1,1,1559829896,'2019/6',0,'0','0','',1,1,1559829896,0),(2,'gws110','27543711@qq.com','36.5.49.187','$2y$10$navqp4m7fkPHme6B9TwMsusAQGJ42pR1ouxdVfHjs6/ISYpz2fsGq','那人','male','5a1e168d85e2cf7bc9b69aae1120fc4d3033eb08','china','upload/photos/d-avatar.jpg','upload/photos/d-cover.jpg','',45,23,NULL,'','','','',1,0,0,1559829934,'2019/6',0,'0','0','',1,1,0,10),(3,'rolfson.gertrude_417','hugh32_129@yahoo.com','','gws.110','','female','cd69c9cd017666f5c0d1716026e2c829','english','upload/photos/2019/06/BWruJs6TxX6hjfxcpPf1_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962510,'0000/00',0,'0','0','',0,0,0,0),(4,'schamberger.bettye_170','uschulist_933@yahoo.com','','gws.110','','female','910ce18d093615770f8c0a2ea032fd24','english','upload/photos/2019/06/jjbbYstU8gMB7yasSTfd_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962512,'0000/00',0,'0','0','',0,0,0,0),(5,'mills.myles_880','arianna_kutch_297@yahoo.com','','gws.110','','male','df846bead2e1a7c34468ff65c5d2450e','english','upload/photos/2019/06/wYuaGHUWETijsT6M2Xcq_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962513,'0000/00',0,'0','0','',0,0,0,0),(6,'gleichner.sabrina_237','trudie09_905@yahoo.com','','gws.110','','female','393cac1610c7dfc049115516a36b7eb9','english','upload/photos/2019/06/ZGluy2ev2Jyajz8BPnIh_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962515,'0000/00',0,'0','0','',0,0,0,0),(7,'treinger_682','alysa_hane_800@yahoo.com','','gws.110','','female','1f8a2966c264119b5896b4061cb431a6','english','upload/photos/2019/06/HLMgvs1jwsyTxCfOQy5l_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962516,'0000/00',0,'0','0','',0,0,0,0),(8,'ashanahan_842','kertzmann_brown_147@yahoo.com','','gws.110','','male','a0e80d126baf84bc9e917ca4b7a231a1','english','upload/photos/2019/06/xeYXTGnABjAF99vAaI7K_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962517,'0000/00',0,'0','0','',0,0,0,0),(9,'alisa.windler_732','norberto_schmidt_637@yahoo.com','','gws.110','','female','d086f77c1ff9e77dfd0baa8cf07edbce','english','upload/photos/2019/06/w7dFieK6D5HsChlS3JaH_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962519,'0000/00',0,'0','0','',0,0,0,0),(10,'amos17_290','pagac_adriana_897@yahoo.com','','gws.110','','female','f716c5d717982875a8739ef8daf992ae','english','upload/photos/2019/06/rJltA4X49Sheq5mL7ib2_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962520,'0000/00',0,'0','0','',0,0,0,0),(11,'ignatius.hagenes_319','katherine87_625@yahoo.com','','gws.110','','female','3ad280e7a37c5433e493d4e793203b02','english','upload/photos/2019/06/pDKZflwpmoKO4qGTiZN4_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962522,'0000/00',0,'0','0','',0,0,0,0),(12,'ojones_618','leannon_fredrick_153@yahoo.com','','gws.110','','male','3f32e5ebb877d78b5ac36108fa8ab47c','english','upload/photos/2019/06/wpvw99dHbTgM4x1s2V7Q_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962523,'0000/00',0,'0','0','',0,0,0,0),(13,'chester52_771','abbie_howe_402@yahoo.com','','gws.110','','male','6330435c9539d5319df123d21fa4d620','english','upload/photos/2019/06/JTxHI4h86G9ojnfVGfgQ_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962651,'0000/00',0,'0','0','',0,0,0,0),(14,'eroob_624','nichole_schowalter_451@yahoo.com','','gws.110','','female','9dd69ebea569bc484ad6da58087f9fe2','english','upload/photos/2019/06/GIBw9onXLl8OaYvoEZ4z_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962653,'0000/00',0,'0','0','',0,0,0,0),(15,'keeling.barton_768','lucy_fisher_914@yahoo.com','','gws.110','','female','ae36bc9ea30c64881df1f19a1132a6f5','english','upload/photos/2019/06/EtvK3n5X9hq2sUUJFGBW_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962654,'0000/00',0,'0','0','',0,0,0,0),(16,'leuschke.ava_926','pgleason_712@yahoo.com','','gws.110','','female','23ea1929236cf85edc0b5302b06f76cd','english','upload/photos/2019/06/jFXf3WaMVcr9VyiD6V7R_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962656,'0000/00',0,'0','0','',0,0,0,0),(17,'lockman.dallin_560','kiel_tremblay_647@yahoo.com','','gws.110','','male','e61582414028104dc27b17346060aef7','english','upload/photos/2019/06/gQW2jODRRMp7rCWwHacg_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962657,'0000/00',0,'0','0','',0,0,0,0),(18,'deckow.garett_344','friesen_ethel_502@yahoo.com','','gws.110','','male','616afe8582cc935023e0ba80fd2a54ee','english','upload/photos/2019/06/69YmbviZaLHJZCkY37Wy_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962658,'0000/00',0,'0','0','',0,0,0,0),(19,'sigrid.brakus_494','klesch_344@yahoo.com','','gws.110','','female','5a35a972811a1188f5f10bf5281b378b','english','upload/photos/2019/06/cWiYR3lgxIJkdkPFozwb_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962660,'0000/00',0,'0','0','',0,0,0,0),(20,'qleannon_875','everette82_766@yahoo.com','','gws.110','','male','0c821d315094a72f6e03bc59ca91ed52','english','upload/photos/2019/06/JrMKPTRdTbvf3BcWPYxM_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962661,'0000/00',0,'0','0','',0,0,0,0),(21,'caterina44_285','bartoletti_serena_641@yahoo.com','','gws.110','','male','692fdbad8b8254b5cbdc493022ff602e','english','upload/photos/2019/06/EjVFvelzjvLbuMn6gCpR_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962663,'0000/00',0,'0','0','',0,0,0,0),(22,'constance67_271','mauricio_luettgen_538@yahoo.com','','gws.110','','female','147ac5d51123de3fa75f85e9889663fc','english','upload/photos/2019/06/DEq3x32oi5XL91CE3UJO_url_image.jpg','upload/photos/d-cover.jpg','Fake',0,0,NULL,'','','','',1,0,0,1559962664,'0000/00',0,'0','0','',0,0,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `views`
--

DROP TABLE IF EXISTS `views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `album_id` int(11) NOT NULL DEFAULT '0',
  `fingerprint` varchar(50) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `fingerprint` (`fingerprint`),
  KEY `user_id` (`user_id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `views`
--

LOCK TABLES `views` WRITE;
/*!40000 ALTER TABLE `views` DISABLE KEYS */;
INSERT INTO `views` VALUES (2,1,0,'c5fa3600461d1263e1f5a7dea2a52590f67ea48c',0,1559832649),(11,2,0,'01c51c0fa6fcc616dbaf8565a8d20b8b522df1c5',0,1559974549),(12,1,0,'01c51c0fa6fcc616dbaf8565a8d20b8b522df1c5',0,1559974623),(13,2,0,'1de76144bfa0c5cc2e89874fcfa7518ab4047140',0,1560868020),(14,1,0,'e7048374949fe46642bf7112ef6690520e4c042d',0,1560868806),(15,1,0,'1acdaf318a8c116dfc2c495074efdb4cdae9caa5',0,1560869822),(16,1,0,'d7edd1090aa75db9f8c0484d2fa187b205efceca',1,1560902978),(17,2,0,'d7edd1090aa75db9f8c0484d2fa187b205efceca',2,1560904592),(22,4,0,'9081593fbcabe6e84871e08f74a24020c076d435',2,1560906139),(23,4,0,'c09315d5f3f0d95b1dfb170b0ffb3d85537fa486',0,1560906193),(24,3,0,'c09315d5f3f0d95b1dfb170b0ffb3d85537fa486',0,1560906347),(25,1,0,'c09315d5f3f0d95b1dfb170b0ffb3d85537fa486',2,1560906468),(26,5,0,'4b071e89e7fdde735e6d76b6bcd22c2f4448eda9',2,1560908610),(27,1,0,'72301708334fbe8a3052e45d0b736c6832287457',0,1561516840),(28,3,0,'75bf57285559d3b18839a84770f9d612c66952e8',2,1562758777),(29,5,0,'09efe3f7d30f543fa9a6686ab63f0955168e3791',1,1606566349),(30,1,0,'c2a32da26a82e27951fc9a0bb5a21078f53f0f55',0,1606566436),(31,2,0,'c2a32da26a82e27951fc9a0bb5a21078f53f0f55',0,1606566451);
/*!40000 ALTER TABLE `views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdrawal_requests`
--

DROP TABLE IF EXISTS `withdrawal_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withdrawal_requests` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '',
  `amount` varchar(100) NOT NULL DEFAULT '0',
  `currency` varchar(20) NOT NULL DEFAULT '',
  `requested` varchar(100) NOT NULL DEFAULT '',
  `status` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdrawal_requests`
--

LOCK TABLES `withdrawal_requests` WRITE;
/*!40000 ALTER TABLE `withdrawal_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdrawal_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database '123456'
--

--
-- Dumping routines for database '123456'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-06 14:53:38
