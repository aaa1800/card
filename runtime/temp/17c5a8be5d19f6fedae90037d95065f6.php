<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:84:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\userinfo.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
        <li  class="active"    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
   <!--  <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
    <li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
</ul>                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-6 center-box">
                                    <div class="alert alert-warning">
                                        <?php echo C('title'); ?>不会以任何形式公开您的个人隐私。完善个人资料，更能轻松快速销卡哦~
                                    </div>
                                </div>
                            </div>

                            <form class="form-horizontal m-t">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">用户编号：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder"><?php echo $u['id']; ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">用户名：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
                                                                                        <!--有昵称-->
                                            <span><?php echo $u['user']; ?></span>
                                            <!-- <a class="text-green text-underline m-l-md" href="javascript:openNickNameModal('修改用户昵称');">修改</a> -->
                                                                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">真实姓名：</label>
                                    <div class="col-sm-7">
									<?php if($u['real'] > '0'): ?>
									  <span class="form-control noBorder height-auto">
                                                                                        <!--已认证 认证后不能修改 点击“查看”进入到“实名认证”页面-->
                                            <span><?php echo $u['username']; ?></span>
                                            <a class="btn btn-green btn-xs m-l-md" href="javascript:void(0);">已认证</a>
                                            <a class="text-green text-underline m-l-md" href="javascript:gotoRealnamePage();">查看</a>
                                                                                    </span>

									<?php else: ?>
                                        <span class="form-control noBorder height-auto">
                                                                                        <!--未认证-->
                                            <span class="text-red m-r-md">未知</span><a class="text-blue text-underline" href="javascript:gotoRealnamePage();">马上去实名认证</a>
                                                                                    </span>
																					<?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">联系QQ：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
                                                   <?php echo $u['qq']; ?>                                     <!--没有设置QQ-->
                                            <a class="text-blue pos-rela" href="javascript:openQQnumberModal('设置QQ号码');"><?php if(empty($u['qq']) || (($u['qq'] instanceof \think\Collection || $u['qq'] instanceof \think\Paginator ) && $u['qq']->isEmpty())): ?>设置<?php else: ?>修改<?php endif; ?>QQ号码</a>
											<?php if(empty($u['qq']) || (($u['qq'] instanceof \think\Collection || $u['qq'] instanceof \think\Paginator ) && $u['qq']->isEmpty())): ?>
                                            <p class="help-block color999">请设置您的QQ号码，方便我们在处理您提交的卡密有问题时能及时联系到您。</p><?php endif; ?>
                                                                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">手机绑定：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
										   <?php if(empty($u['photo']) || (($u['photo'] instanceof \think\Collection || $u['photo'] instanceof \think\Paginator ) && $u['photo']->isEmpty())): ?>
                                                                                          <!--没有绑定-->
                                            <span class="color999">手机未绑定</span>
                                            <a class="text-green text-underline m-l-md" href="javascript:gotoPhoneSet();">立即绑定&gt;&gt;</a>


										   <?php else: ?>
                                                                                          <!--已经绑定-->
                                            <span><?php echo tfen($u['photo'],3,4); ?></span><a class="text-green text-underline m-l-md" href="javascript:gotoPhoneSet();">更换手机</a>
											<?php endif; ?>
                                                                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">邮箱绑定：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
                                                                                         <!--没有绑定-->
                                            <?php if(empty($u['email']) || (($u['email'] instanceof \think\Collection || $u['email'] instanceof \think\Paginator ) && $u['email']->isEmpty())): ?>
                                                                                          <!--没有绑定-->
                                            <span class="color999">邮箱未绑定</span>
                                            <a class="text-green text-underline m-l-md" href="javascript:gotoMailSet();">立即绑定&gt;&gt;</a>


										   <?php else: ?>
                                                  <!--已经绑定-->
                                            <span><?php echo tfen($u['email'],3,4); ?></span><a class="text-green text-underline m-l-md" href="javascript:gotoMailSet();">更换邮箱</a>
											<?php endif; ?>
                                                                                    </span>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-sm-5 control-label color9">QQ绑定：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
                                                                                         
                                            <span>已绑定</span><a class="text-green text-underline m-l-md" href="javascript:gotoQQbindPage();">更换绑定</a>
                                                                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">微信绑定：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
                                                                                       
                                            <span class="color999">微信未绑定</span>
                                            <a class="text-green text-underline m-l-md" href="javascript:gotoWXbindPage();">立即绑定&gt;&gt;</a>
                                            <p class="help-block color999">必须先绑定微信，才可使用微信提现</p>
                                                                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label color9">关注公众号：</label>
                                    <div class="col-sm-7">
                                        <span class="form-control noBorder height-auto">
                                                                                       
                                            <span class="color999">未关注</span>
                                            <a class="text-green text-underline m-l-md" href="javascript:gotoFollowWxPage();">立即关注&gt;&gt;</a>
                                            <p class="help-block color999">必须先关注公众号，才可使用微信提现</p>
                                                                                    </span>
                                    </div>
                                </div> -->

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--设置或修改用户昵称-->
<div class="modal inmodal" id="setOrModifyNickNameModal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" placeholder="昵称为2-20个字符，汉字、字母、数字、字符组成" class="form-control" id="nickName">
                </div>
                <div class="form-group">
                    <div class="errorTip color_f00"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue btn-w-full" onclick="updateNickName();">保存</button>
            </div>
        </div>
    </div>
</div>

<!--设置或修改QQ号码-->
<div class="modal inmodal" id="setOrModifyQQModal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group"><input type="text" placeholder="请输入QQ号码" class="form-control" id="qqNumber"></div>
                <div class="form-group"><div class="errorTip color_f00"></div></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue btn-w-full" onclick="updateQqNumber();">保存</button>
            </div>
        </div>
    </div>
</div>



<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = {domain: "",_csrf:"",staticDomain:""};
	var qqurl="<?php echo U('home/member/addqq'); ?>";
</script>
<script src="/static/home/js/common/profileManage/common_profile.js?v=10"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/profileManage/baseProfile/baseProfile.js?v=16"></script>


</body>
</html>
