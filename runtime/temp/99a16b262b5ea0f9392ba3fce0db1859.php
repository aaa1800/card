<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:74:"D:\wwwroot\127.0.0.1\public/../application/simple\view\category\opaer.html";i:1667656648;s:62:"D:\wwwroot\127.0.0.1\application\Simple\view\public\heard.html";i:1667656648;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
	     <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">通道管理</a>
                <a>
                    <cite>类型列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
							 <button class="layui-btn" type="button"  onclick="xadmin.open('新增类型','/cedit',850,500)" ><i class="layui-icon"></i>新增类型</button> 
                        </div>
						 
                        <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
	<script type="text/html" id="caoz">
		<div class="layui-btn-group">
		<button class="layui-btn" onclick="xadmin.open('编辑','/cedit?id={{d.id}}',850,500)" >编辑</button>
		<button class="layui-btn layui-btn-danger"  lay-event="del">删除</button>
		</div>
   </script>
	
    <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "/copaer",
            method: 'post',
			page:true,
            cols: [[
			    {type: 'checkbox',width:30,minWidth:30},
				{field:'id',title:"ID", align: 'center', sort: true,},
			    {field:'name',title: '类型名',align: 'center'},
				{field:'addtime',title: '添加时间',align: 'center',sort: true,},
                { align: 'center',title:'操作',toolbar: '#caoz'}
            ]],
			limit:15,
			limits:[10,20,50,60]
        });		
		table.on('tool(mlist)', function(obj){
            var data = obj.data;
			console.log(obj.event);
			switch(obj.event){
               case 'del':
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("/cdel",{id:data.id,tab:'Operator'},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
			  break;
			  return false;
            }
        });
      });
	  
    </script>
</html>