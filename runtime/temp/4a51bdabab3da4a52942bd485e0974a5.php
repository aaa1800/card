<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:90:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\index\mobile\retrieve.html";i:1616590069;s:79:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\mobile\footer.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/enterpriseRecycle/enterpriseRecycle.css">
	
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a href="javascript:void(0);"></a>
    <a class="title" href="javascript:void(0);">企业回收</a>
    <a class="w44" href="<?php echo U('home/index/myMessage'); ?>">
        <img src="/static/home/images/index/system_news_icon.png">
    </a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap t44-b50-space">
    <div class="minirefresh-scroll bg-transparent">
        <main class="enterprise-recycle-wrap w-wrap">
            <div class="top-bg"></div>

            <a href="tel:400-888-9259" class="btn btn-green cooperation-btn">我是企业，合作咨询</a>

            <div class="advantage-box">
                <h3 class="list-header">企业回收优势</h3>
                <div class="media">
                    <img class="w100 align-self-center" src="/static/home/images/enterpriseRecycle/firm_serv_icon.png">
                    <div class="media-body">
                        <h5 class="tit">一对一服务</h5>
                        <p class="desc">我们出色的专业服务团队，确保交易安全放心满意</p>
                    </div>
                </div>
                <div class="media">
                    <img class="w100 align-self-center" src="/static/home/images/enterpriseRecycle/firm_kind_icon.png">
                    <div class="media-body">
                        <h5 class="tit">种类丰富齐全</h5>
                        <p class="desc">提供回收类型覆盖线上线下等几十种回收卡卡券</p>
                    </div>
                </div>
                <div class="media">
                    <img class="w100 align-self-center" src="/static/home/images/enterpriseRecycle/firm_way_icon.png">
                    <div class="media-body">
                        <h5 class="tit">交易方式多样</h5>
                        <p class="desc">提供线上、线下、API接口，为您定制多种交易方式</p>
                    </div>
                </div>
                <div class="media">
                    <img class="w100 align-self-center" src="/static/home/images/enterpriseRecycle/firm_money_icon.png">
                    <div class="media-body">
                        <h5 class="tit">极速资金回流</h5>
                        <p class="desc">我们在验卡成功后立即打款资金回流速度超乎想象</p>
                    </div>
                </div>
            </div>

            <a href="tel:400-888-9259" class="btn btn-orange cooperation-btn">联系客服咨询</a>
        </main>
    </div>
</div>

<!--固定底部-->
<footer class="navbar fixed-bottom bg-white w-wrap">
    <div class="row">
        <div class="col">
            <a class="d-block  <?php if(($acti == 'index') and ($cont == 'Index')): ?>active<?php endif; ?>" href="/">
                <div class="card border-0 rounded-0">
                    <i class="home"></i>
                    <div class="card-body">
                        <p class="card-text text-center">首页</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col">
            <a class="d-block <?php if($acti == 'recycle'): ?>active<?php endif; ?>" href="<?php echo U('home/index/recycle'); ?>" >
                <div class="card border-0 rounded-0">
                    <i class="category"></i>
                    <div class="card-body">
                        <p class="card-text text-center">分类</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col">
            <a class="d-block" href="<?php echo U('home/member/sellCard'); ?>">
                <div class="card border-0 rounded-0">
                    <i class="sell"></i>
                </div>
            </a>
        </div>
        <div class="col">
            <a class="d-block <?php if($acti == 'retrieve'): ?>active<?php endif; ?>" href="<?php echo U('home/index/retrieve'); ?>">
                <div class="card border-0 rounded-0">
                    <i class="enterprise"></i>
                    <div class="card-body">
                        <p class="card-text text-center">企业</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col">
        	<!-- <a class="d-block" href="http://m.renrenxiaoka.com/passport/login/index.html"> -->
            <a class="d-block <?php if($cont == 'Member'): ?>active<?php endif; ?>" href="<?php echo U('home/member/index'); ?>">
                <div class="card border-0 rounded-0">
                    <i class="mine"></i>
                    <div class="card-body">
                        <p class="card-text text-center">我的</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</footer>
<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"",staticDomain:""};
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/enterpriseRecycle/index.js?v=10"></script>
</body>
</html>