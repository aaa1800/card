<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:92:"C:\wwwroot\yhs.haochiyidian.top\public/../application/home\view\member\authen_init_qiye.html";i:1616590069;s:72:"C:\wwwroot\yhs.haochiyidian.top\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css?v=18" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                 <ul class="nav nav-tabs">
					<li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
					<li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
					<li  class="active"><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
					<!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>-->
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
 					<!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
					<!-- <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
					<li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
				</ul>               
				<div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
						  <div class="clearfix memu_tap">
              			  		<a  class="menu_one left_ra " href="<?php echo U('home/member/AuthenInit'); ?>">个人认证</a> 
								<a class="menu_one center_w" href="<?php echo U('home/member/AuthenInitGaoji'); ?>">高级认证</a>  
					            <a class="menu_one right_ra bg" href="<?php echo U('home/member/AuthenInitQiye'); ?>">企业认证</a>  
					        </div>
						  <?php switch($ok): case "-1": ?>
                            <!--②、账户未实名，但已绑定手机-->
							<div class="header-box">
		                        <div class="header-box-info">
									<p>企业认证必须保证个人认证的是企业法人。</p>
								</div>
                        	</div>
                            <div class="w-1205">
									<form id="intermediateAuthenForm" enctype="multipart/form-data" method="post">
										<div class="clearfix">
											<div class="info_list">
												<div class="list_left">法人&emsp;&emsp;&emsp;姓名：</div>
												<div class="list_right">
													<input class="natureInput" readonly="readonly" type="text" name="name" id="name" value="<?php echo $u['username']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">身份&emsp;&emsp;&emsp;证号：</div>
												<div class="list_right">
													<input class="natureInput" readonly="readonly" type="text" name="idCard" id="idCard" value="<?php echo $u['idcard']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">企业&emsp;&emsp;&emsp;名称：</div>
												<div class="list_right">
													<input class="some_class"  type="text" name="name" id="idname" value="">
												</div>
												<div class="timea-error display-none" style="margin-top: 72px;">
													<span><i class="error-icon"></i>请填写企业名称</span>
												</div>
											 </div>
											    
											 <div class="info_list">
												<div class="list_left">企业营业执照号：</div>
												<div class="list_right">
													<input class="some_class"  type="text" name="number" id="idnumber" value="">
												</div>
												<div class="timeb-error display-none" style="margin-top: 72px;">
													<span><i class="error-icon"></i>请填写营业执照号</span>
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">证件&emsp;颁发日期：</div>
												<div class="list_right" id="timeStart">
													<input type="text" class="some_class" name="idCardStartDate" value="" id="some_class_1" placeholder="YYYY-MM-DD">
													<i class="atime-icon" onclick="time_icon1(this)"></i>
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">证件&emsp;到期日期：</div>
												<div class="list_right" id="timeEnd">
													<input type="text" class="some_class" name="idCardEndDate" value="" id="some_class_2" placeholder="YYYY-MM-DD">
													<i class="atime-icon" onclick="time_icon2(this)"></i>
												</div>
											 </div>
										</div>
										<div class="timeS-error display-none">
											<span><i class="error-icon"></i>你当前未选择日期，请选择日期</span>
										</div>
										<div class="timeE-error display-none">
											<span><i class="error-icon"></i>你当前未选择日期，请选择日期</span>
										</div>
									
									<!-- 整体 -->
									<div class="page-m">
									 	<!-- 正面 -->
										<div class="id_img_wp">	
											<div class="img_intro w_175">身份证&emsp;&emsp;正面：</div>
											<input type="file" accept="image/png,image/jpeg,image/jpg" onchange="getzImg(this)" id="img_z" style="display:none"  name="image[]">
											<div class="img_wp" onclick="zhengmian()">
												<div class="xs one">
													<i class="blue-icon1"></i>
													<i class="right-icon"></i>
													<img src="/static/home/images/profileManage/white-bg.png" id="zmz">
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
											
											<div class="img_wp">
												<img src="/static/home/images/profileManage/upload-front.png">
											</div>
											<div class="cf"></div>
											<textarea class="textarea_text" readonly="readonly">证件上的文字清晰可识别 一定要看见证件号和姓名</textarea>
											
										</div>
										<div class="zmz-error display-none">
											<span><i class="error-icon"></i>你当前未上传正面照，请先选择正面照</span>
										</div>
										<!-- 反面 -->
										<div class="id_img_wp">
											<input type="file"  accept="image/png,image/jpeg,image/jpg" onchange="getfImg(this)" id="img_f" style="display:none" name="image[]" >
											<div class="img_intro w_175">身份证&emsp;反面：</div>
											<div class="img_wp" onclick="fanmian()">
												<div class="xs two">
												<i class="blue-icon2"></i>
												<i class="right-icon"></i>
													<img src="/static/home/images/profileManage/white-bg.png" id="fmz" >
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
											
											<div class="img_wp">
												<img src="/static/home/images/profileManage/upload-rear.png">
											</div>
											<div class="cf"></div>
											<textarea class="textarea_text" readonly="readonly">证件上的文字清晰可识别 须能看见国徽和起始日期</textarea>
											
										 </div>
									 	<div class="fmz-error display-none">
											<span><i class="error-icon"></i>你当前未上传反面照，请先选择反面照</span>
										</div>
										<!-- 手持照 -->	 
										<div class="id_img_wp">
											<input type="file" accept="image/png,image/jpeg,image/jpg" onchange="getsImg(this)" id="img_s" style="display:none" name="image[]">
											<div class="img_intro w_175">企业营业执照：</div>
											<div class="img_wp" onclick="shoucmian()">
												<div class="xs three">
												<i class="blue-icon3"></i>
												<i class="right-icon"></i>
													<img src="/static/home/images/profileManage/white-bg.png" id="scz">
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
											
											<div class="img_wp">
												<img src="/static/home/images/profileManage/upload-hand.png">
											</div>
											<div class="cf"></div>													
											<textarea class="textarea_text" readonly="readonly">证件文字清晰可识别，证件信息正确</textarea>
										</div>
									</div> 
									
									<div class="text-left wz_p">
								        <p>1、照片内容真实有效，不得做任何修改 </p>
								        <p>2、支持jpg、jpeg、png，最大不超过5M；</p>       
								        <p>3、证件上的所有文字清晰可见，均无涂改无遮挡；</p>
								        <p>4、照片需免冠，建议未化妆，手持证件人五官清晰可见，完整露出双臂和证件信息；</p>
								        <p>5、照片仅作为人人销卡网实名认证依据，我们不会保留原始图片及泄露用户任何隐私</p>
								    </div>
									    
								    <div class="sczm-error display-none"><span><i class="error-icon"></i>你当前未上传手持照，请选择手持照</span></div>
									
									<p class= "center-w">
										<input type="button" value="立即提交" class="save_btn" onclick="checkForm()">
									</p> 
									</form>	
								<div class="w-1205">
                        	    </div>	
                        </div>
							<?php break; case "0": ?>
							 <div class="status-box realname-fail" >
                                <div class="status-box-icon pull-left"><i class="ico140 sand-glass"></i></div>
                                <div class="status-box-text">
                                    <h3 class="text-orange">当前状态：实名认证审核中...</h3>
                                    <p class="color999">我们将在工作日（9:00-18:00）的30分钟之内完成认证</p>
                                    <p class="color999">如有问题，请联系客服咨询</p>
                                    <a class="btn btn-blue" href="<?php echo U('home/member/index'); ?>">返回账户总览</a>
                                </div>
                            </div>
							<?php break; case "2": ?>
							  <!--②、账户未实名，但已绑定手机-->
							<div class="header-box">
		                        <div class="header-box-info" style="border-color: red;background-color: #fff;color: red;">
									<p><?php echo $da['remarks']; ?></p>
								</div>
                        	</div>
                            <div class="w-1205">
									<form id="intermediateAuthenForm" enctype="multipart/form-data" method="post">
										<div class="clearfix">
											<div class="info_list">
												<div class="list_left">法人&emsp;&emsp;&emsp;姓名：</div>
												<div class="list_right">
													<input class="natureInput" readonly="readonly" type="text" name="name" id="name" value="<?php echo $u['username']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">身份&emsp;&emsp;&emsp;证号：</div>
												<div class="list_right">
													<input class="natureInput" readonly="readonly" type="text" name="idCard" id="idCard" value="<?php echo $u['idcard']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">企业&emsp;&emsp;&emsp;名称：</div>
												<div class="list_right">
													<input class="some_class"  type="text" name="name" id="idname" value="<?php echo $da['name']; ?>">
												</div>
												<div class="timea-error display-none" style="margin-top: 72px;">
													<span><i class="error-icon"></i>请填写企业名称</span>
												</div>
											 </div>
											    
											 <div class="info_list">
												<div class="list_left">企业营业执照号：</div>
												<div class="list_right">
													<input class="some_class"  type="text" name="number" id="idnumber" value="<?php echo $da['number']; ?>">
												</div>
												<div class="timeb-error display-none" style="margin-top: 72px;">
													<span><i class="error-icon"></i>请填写营业执照号</span>
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">证件&emsp;颁发日期：</div>
												<div class="list_right" id="timeStart">
													<input type="text" class="some_class" name="idCardStartDate" value="<?php echo date('Y/m/d',strtotime($da['idCardStartDate'])); ?>" id="some_class_1" placeholder="YYYY-MM-DD">
													<i class="atime-icon" onclick="time_icon1(this)"></i>
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">证件&emsp;到期日期：</div>
												<div class="list_right" id="timeEnd">
													<input type="text" class="some_class" name="idCardEndDate" value="<?php echo date('Y/m/d',strtotime($da['idCardEndDate'])); ?>" id="some_class_2" placeholder="YYYY-MM-DD">
													<i class="atime-icon" onclick="time_icon2(this)"></i>
												</div>
											 </div>
										</div>
										<div class="timeS-error display-none">
											<span><i class="error-icon"></i>你当前未选择日期，请选择日期</span>
										</div>
										<div class="timeE-error display-none">
											<span><i class="error-icon"></i>你当前未选择日期，请选择日期</span>
										</div>
									
									<!-- 整体 -->
									<div class="page-m">
									 	<!-- 正面 -->
										<div class="id_img_wp">	
											<div class="img_intro w_175">身份证&emsp;&emsp;正面：</div>
											<input type="file" accept="image/png,image/jpeg,image/jpg" onchange="getzImg(this)" id="img_z"  style="display:none"  name="image[]">
											<div class="img_wp" onclick="zhengmian()">
												<div class="xs one">
													<i class="blue-icon1"></i>
													<i class="right-icon"></i>
													<img src="/static/home/images/profileManage/white-bg.png" id="zmz">
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
											
											<div class="img_wp">
												<img src="/static/home/images/profileManage/upload-front.png">
											</div>
											<div class="cf"></div>
											<textarea class="textarea_text" readonly="readonly">证件上的文字清晰可识别 一定要看见证件号和姓名</textarea>
											
										</div>
										<div class="zmz-error display-none">
											<span><i class="error-icon"></i>你当前未上传正面照，请先选择正面照</span>
										</div>
										<!-- 反面 -->
										<div class="id_img_wp">
											<input type="file"  accept="image/png,image/jpeg,image/jpg" onchange="getfImg(this)" id="img_f"  style="display:none" name="image[]" >
											<div class="img_intro w_175">身份证&emsp;反面：</div>
											<div class="img_wp" onclick="fanmian()">
												<div class="xs two">
												<i class="blue-icon2"></i>
												<i class="right-icon"></i>
													<img src="/static/home/images/profileManage/white-bg.png" id="fmz" >
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
											
											<div class="img_wp">
												<img src="/static/home/images/profileManage/upload-rear.png">
											</div>
											<div class="cf"></div>
											<textarea class="textarea_text" readonly="readonly">证件上的文字清晰可识别 须能看见国徽和起始日期</textarea>
											
										 </div>
									 	<div class="fmz-error display-none">
											<span><i class="error-icon"></i>你当前未上传反面照，请先选择反面照</span>
										</div>
										<!-- 手持照 -->	 
										<div class="id_img_wp">
											<input type="file" accept="image/png,image/jpeg,image/jpg" onchange="getsImg(this)" id="img_s" style="display:none" name="image[]">
											<div class="img_intro w_175">企业营业执照：</div>
											<div class="img_wp" onclick="shoucmian()">
												<div class="xs three">
												<i class="blue-icon3"></i>
												<i class="right-icon"></i>
													<img src="/static/home/images/profileManage/white-bg.png" id="scz">
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
											
											<div class="img_wp">
												<img src="/static/home/images/profileManage/upload-hand.png">
											</div>
											<div class="cf"></div>													
											<textarea class="textarea_text" readonly="readonly">证件文字清晰可识别，证件信息正确</textarea>
										</div>
									</div> 
									
									<div class="text-left wz_p">
								        <p>1、照片内容真实有效，不得做任何修改 </p>
								        <p>2、支持jpg、jpeg、png，最大不超过5M；</p>       
								        <p>3、证件上的所有文字清晰可见，均无涂改无遮挡；</p>
								        <p>4、照片需免冠，建议未化妆，手持证件人五官清晰可见，完整露出双臂和证件信息；</p>
								        <p>5、照片仅作为人人销卡网实名认证依据，我们不会保留原始图片及泄露用户任何隐私</p>
								    </div>
									    
								    <div class="sczm-error display-none"><span><i class="error-icon"></i>你当前未上传手持照，请选择手持照</span></div>
									
									<p class= "center-w">
										<input type="button" value="重新提交" class="save_btn" onclick="checkForm()">
									</p> 
									</form>	
								<div class="w-1205">
                        	    </div>	
                        </div>
							<?php break; default: ?>
							<div class="header-box-info" style="border-color: #1ab394;background-color: #fff;color: #1ab394;">
									<p><?php echo $da['remarks']; ?></p>
								</div>
                            <div class="w-1205">
										<div class="clearfix">
											<div class="info_list">
												<div class="list_left">法人&emsp;&emsp;&emsp;姓名：</div>
												<div class="list_right">
													<input class="natureInput" readonly="readonly" type="text" name="name" id="name" value="<?php echo $u['username']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">身份&emsp;&emsp;&emsp;证号：</div>
												<div class="list_right">
													<input class="natureInput" readonly="readonly" type="text" name="idCard" id="idCard" value="<?php echo $u['idcard']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">企业&emsp;&emsp;&emsp;名称：</div>
												<div class="list_right">
													<input class="natureInput"  type="text" readonly="readonly" name="name" id="idname" value="<?php echo $da['name']; ?>">
												</div>
											 </div>
											    
											 <div class="info_list">
												<div class="list_left">企业营业执照号：</div>
												<div class="list_right">
													<input class="natureInput"  type="text" readonly="readonly" name="name" id="idname" value="<?php echo $da['number']; ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">证件&emsp;颁发日期：</div>
												<div class="list_right">
												    <input class="natureInput"  type="text" readonly="readonly" name="name"  value="<?php echo date('Y/m/d',strtotime($da['idCardStartDate'])); ?>">
												</div>
											 </div>
											 <div class="info_list">
												<div class="list_left">证件&emsp;到期日期：</div>
												<div class="list_right">
												    <input class="natureInput"  type="text" readonly="readonly" name="name"  value="<?php echo date('Y/m/d',strtotime($da['idCardEndDate'])); ?>">
												</div>
												
											 </div>
										</div>
									<!-- 整体 -->
									<div class="page-m">
									 	<!-- 正面 -->
										<div class="id_img_wp">	
											<div class="img_intro w_175">身份证&emsp;&emsp;正面：</div>
											<input type="file" accept="image/png,image/jpeg,image/jpg" onchange="getzImg(this)" id="img_z"  style="display:none"  name="image[]">
											<div class="img_wp" onclick="zhengmian()">
												<div class="xs one">
													<i class="blue-icon1"></i>
													<i class="right-icon"></i>
													<img src="<?php echo $da['idjust']; ?>" id="zmz">
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
										</div>
										<!-- 反面 -->
										<div class="id_img_wp">
											<input type="file"  accept="image/png,image/jpeg,image/jpg" onchange="getfImg(this)"  id="img_f" style="display:none" name="image[]" >
											<div class="img_intro w_175">身份证&emsp;反面：</div>
											<div class="img_wp" onclick="fanmian()">
												<div class="xs two">
												<i class="blue-icon2"></i>
												<i class="right-icon"></i>
													<img src="<?php echo $da['idback']; ?>" id="fmz" >
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
										 </div>
										<!-- 手持照 -->	 
										<div class="id_img_wp">
											<input type="file" accept="image/png,image/jpeg,image/jpg" onchange="getsImg(this)" id="img_s"  style="display:none" name="image[]">
											<div class="img_intro w_175">企业营业执照：</div>
											<div class="img_wp" onclick="shoucmian()">
												<div class="xs three">
												<i class="blue-icon3"></i>
												<i class="right-icon"></i>
													<img src="<?php echo $da['license']; ?>" id="scz">
													<a href="javascript:void(0);">重新上传</a>
												</div>
											</div>
										</div>
									</div>  
                        </div>
						<?php endswitch; ?>
							
							
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = "<?php echo U('home/member/Qiyeup'); ?>";
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/profileManage/realNameAuthen/intermediateAuthen/bindIdCardRealNameAuthenInit.js?v=26"></script>
<script src="/static/home/js/profileManage/realNameAuthen/intermediateAuthen/photoCompress.js?v=21"></script>
<script src="/static/home/js/profileManage/realNameAuthen/intermediateAuthen/laydate.js?v=20180830111"></script>

<script>
 var ok="<?php echo $u['real']; ?>";
 if(ok==0 || ok=="" || ok==null){
    layer.msg("请先实名认证", {icon : 2,shade : [ 0.4, '#000' ],time : 2000},function(index){
	     location.href="<?php echo U('home/member/AuthenInit'); ?>";
	});
 }
</script>


</body>
</html>
