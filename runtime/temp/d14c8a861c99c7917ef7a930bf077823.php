<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\wwwroot\yhs.haochiyidian.top\public/../application/home\view\member\authen_init.html";i:1623499678;s:72:"C:\wwwroot\yhs.haochiyidian.top\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css?v=12" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                 <ul class="nav nav-tabs">
					<li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
					<li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
					<li  class="active"><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
					<!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li> -->
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
					<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
					<!-- <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
					<li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
				</ul>               
				<div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
						  <div class="clearfix memu_tap">
              			  		<a  class="menu_one left_ra bg" href="<?php echo U('home/member/AuthenInit'); ?>">个人认证</a> 
								<a class="menu_one center_w" href="<?php echo U('home/member/AuthenInitGaoji'); ?>">高级认证</a>  
					            <a class="menu_one right_ra " href="<?php echo U('home/member/AuthenInitQiye'); ?>">企业认证</a>  
					        </div>
						  <?php if($u['real'] == '0'): if(empty($u['photo']) || (($u['photo'] instanceof \think\Collection || $u['photo'] instanceof \think\Paginator ) && $u['photo']->isEmpty())): ?>
							<div class="status-box">
                                <div class="status-box-icon pull-left"><i class="ico140 red-phone"></i></div>
                                <div class="status-box-text">
                                    <h3 class="text-red">您还未绑定手机！</h3>
                                    <p class="color999">实名认证前必须先进行手机绑定</p>
                                    <p class="color999">如有问题，请联系客服咨询</p>
                                    <a class="btn btn-blue phone" href="javascript:gotoPhoneSet();"><i class="phone-ico"></i>绑定手机</a>
                                </div>
                            </div>
							<?php else: ?>
							
                            <!--②、账户未实名，但已绑定手机-->
                            <div class="header-box">
		                        <div class="header-box-info">
									<p><?php echo C('title'); ?>不会以任何形式公开您的个人隐私。只有实名认证后才可进行提现操作~</p>
								</div>

                        	</div>
                            <form class="form-horizontal m-t" id="realNameForm" action="#" method="post">
                                <input type="hidden" value="e73ba083-c73f-4c4c-b2f2-9f51170855b7" name="_csrf">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">真实姓名：</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="name" value="" class="form-control" placeholder="请输入与身份证一致的真实姓名" id="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">身份证号：</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="idCard" value="" class="form-control" placeholder="请输入您的身份证号码" id="idCard">
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="red-tip"><i class="red-ico"></i>提现也必须是这个身份证所开的银行卡、微信、支付宝，请注意填写</div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitRealNameFormBtn">立即提交</a>
                                    </div>
                                </div>
                            </form><?php endif; else: ?>
                            <div class="status-box has-realname">
                                <div class="status-box-icon pull-left"><i class="ico140 real-name"></i></div>
                                <div class="status-box-text">
                                    <h3><?php echo $u['username']; ?><a class="btn btn-green btn-xs m-l-xs" href="javascript:void(0);">已实名</a></h3>
                                    <p><span class="color999">证件类型：</span>身份证</p>
                                    <p><span class="color999">证件号码：</span><?php echo tfen($u['idcard'],3,4); ?></p>
                                    <p>
                                        <span class="color999">绑定手机：</span><?php echo tfen($u['photo'],3,4); ?>
                                        <a class="text-blue text-underline m-l-xs" href="javascript:gotoPhoneSet();">更换</a>
                                    </p>
                                    <p><span class="color999">认证渠道：</span><?php echo C('title'); ?>实名认证</p>
                                    <p><span class="color999">认证日期：</span><?php echo $u['realtime']; ?></p>
                                </div>
                            </div>
                        </div>
						<?php endif; ?>
							
							
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = {domain: "",_csrf:"e73ba083-c73f-4c4c-b2f2-9f51170855b7",staticDomain:""};
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/profileManage/baseProfile/baseProfile.js"></script>


</body>
</html>
