<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:89:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\alipay.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/withdrawAccount/withdrawAccount.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">支付宝账号</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap t44-b0-space">
    <div class="minirefresh-scroll bg-transparent">
        <main class="aliAccount-list-wrap">
            <!--支付宝账号列表-->
            <ul class="bankCard-list list-unstyled" id="aliAccountList"></ul>

            <!--有3个支付宝账号则需隐藏按钮-->
            <a id="addAccountBtn" class="btn btn-blue full-btn mt-4 d-none" href="<?php echo U('home/member/addali',['ty'=>1]); ?>">继续添加</a>

        </main>
    </div>
</div>

<!--删除支付宝账号确认交易密码模态框-->
<div class="modal fade" id="sureTradePwdModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">删除支付宝账号 - 确认交易密码</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="password" placeholder="请输入交易密码" class="form-control" id="tradePassword">
                </div>

                <div class="form-group mt-0 clearfix">
                    <!--没有设置交易密码-->
                                        <a class="tradePass-related color-red" href="<?php echo U('home/member/resetTradePasswordInit'); ?>">忘记交易密码？</a>
                                    </div>

                <div class="form-group mt-0">
                    <div class="errorTip"></div>
                </div>
                <input type="hidden" value="" id="delId">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue full-btn" onclick="sureDelSeltedAliPayAccount();">确定</button>
            </div>
        </div>
    </div>

</div>

<input type="hidden" value="<?php echo $u['jiaoyimima']; ?>" id="isOpenTradePaaword"><!--是否开启交易密码-->

<script>
    var ctxPath = {domain: "",domainapi: "",csrf:"86715189-06c0-46a8-a7ce-9fe2f37590ea",staticDomain:""};
	var editl="<?php echo U('home/member/addali'); ?>",delurl="<?php echo U('home/member/deleteBankCard'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/withdrawAccount/aliAccountList.js"></script>
</body>
</html>