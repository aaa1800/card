<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:89:"C:\wwwroot\hs.qulaida.top\public/../application/simple\view\member\member_with_order.html";i:1638015884;s:67:"C:\wwwroot\hs.qulaida.top\application\Simple\view\public\heard.html";i:1638015884;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">会员中心</a>
                <a>
                    <cite>提现列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
						<form class="layui-form layui-col-space5">
						 <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn layui-btn-danger" id="ney"> 总金额:￥0.00 </button>
									<button class="layui-btn layui-btn-danger" type="button" id="delAll"><i class="layui-icon"></i>批量删除</button>
									</div>
                               <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="开始日期" name="start" id="start">
								</div>
								<div class="layui-inline layui-show-xs-block">
									<input class="layui-input"  autocomplete="off" placeholder="截止日期" name="end" id="end">
								</div>
								<div class="layui-inline layui-show-xs-block">
								 <select name="type" id="selt" lay-skin="select">
								     <option value="">请选择</option>
									 <option value="uid">UID</option>
									 <option value="out_trade_no|order">订单号</option>
									 <option value="money">金额</option>
								 </select>
                                    </div>
									<div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="username" placeholder="搜索内容" autocomplete="off" class="layui-input"></div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn" type="button" lay-submit="" lay-filter="sreach" lay-skin="select" lay-filter="type">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
								</form>
                        </div>
						 
                        <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
	<script type="text/html" id="money">
	  <span style='color:red'>{{d.money}}</span>
	</script>
	<script type="text/html" id="price">
	  {{#  if(d.txtype == 1){ }}
	  <span style='color:red'>余额提现</span>
	  {{#  } else { }}
	  <span style='color:blue'>佣金提现</span>
	  {{#  } }}
	</script>
	<script type="text/html" id="status">
    {{#  if(d.status == 1){ }}
      <p class="layui-bg-red">已审核</p>
     {{# }else if(d.status==2){ }}
     <p class="layui-bg-black">退回</p>
     {{#  } else { }}
	  <p class="layui-bg-blue">未审核</p>
	 {{#  } }}
    </script>
	<script type="text/html" id="action">
	<div class="layui-btn-group">
    <button class="layui-btn" onclick="xadmin.open('审核','<?php echo U('simple/Member/withEdit'); ?>?id={{d.id}}')" >审核</button>
    <button class="layui-btn layui-btn-danger"  lay-event="del">删除</button>
	</div>
   </script>
    <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "<?php echo U('simple/member/memberWithOrder'); ?>",
            method: 'post',
			page:true,
			totalRow:true,
			parseData: function(res){
			  $("#ney").text("总金额:￥"+res.money);
			},
            cols: [[
			    {type: 'checkbox',width:30,minWidth:30},
				{field:'id',title:"ID", width:50,align: 'center', sort: true,totalRowText: '合计：'},
			    {field:'uid',title: '用户ID',align: 'center', width:70},
				{field:'user',title: '用户名',align: 'center', width:100},
                {field:'order', title: '订单号' ,align: 'center', width:180} ,
				{field:'type', align: 'center',title:'账号类型', width:100},
				{field:'zh', align: 'center',title:'收款账户', width:190},
				{field:'money',align: 'center',title:'提现金额', width:80,totalRow: true,toolbar: '#money'},
				{field:'txtype',align: 'center',title:'提现类型', width:80,totalRow: true,toolbar: '#price'},
				{field: 'state',align: 'center', title: '审核状态',sort: true,toolbar: '#status',width:90},
				{field: 'addtime',align: 'center', title: '提交时间',width:150},
				{field: 'content',align: 'center', title: '备注',width:150},
                {field: 'shtime',align: 'center', title: '审核时间',width:150},
                {title: '操作',align: 'center', toolbar: '#action',width:144}
            ]],
			limit:20,
			limits:[20,50,60]
        });	
		var nowTime=new Date();    
    var startTime=laydate.render({
    	elem:'#start',
    	type:'datetime',
    	btns: ['confirm'],
    	max:'nowTime',//默认最大值为当前日期
    	done:function(value,date){
    	    endTime.config.min={    	    		
    	    	year:date.year,
    	    	month:date.month-1,//关键
                date:date.date,
                hours:date.hours,
                minutes:date.minutes,
                seconds:date.seconds
    	    };
    	    
    	}
    })
    var endTime=laydate.render({
    	elem:'#end',
    	type:'datetime',
    	btns: ['confirm'],
    	max:'nowTime',
    	done:function(value,date){   	   
    	    startTime.config.max={
    	    		year:date.year,
        	    	month:date.month-1,//关键
                    date:date.date,
                    hours:date.hours,
                    minutes:date.minutes,
                    seconds:date.seconds
    	    }
    	    
    	}
    })		
		form.on('submit(sreach)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var name=$("input[name=username]").val(),kk=$("#selt").val(),st=$("#start").val(),se=$("#end").val();
			
			var index = layer.load(3, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
					});
                 tableIn.reload({
					page:{curr:1
						 },
                     where:{
						 name:name,
						 kk:kk,
						 st:st,
						 se:se
                     }
                 });
				 layer.close(index);
        });
		table.on('tool(mlist)', function(obj){
            var data = obj.data;
			console.log(obj.event);
            if(obj.event === 'del'){
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/card/msDel'); ?>",{id:data.id,tab:'Withdraw'},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                });
            }
        });
		$("#delAll").click(function(){
            layer.confirm('确认要删除选中信息吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('mlist'); //test即为参数id设定的值
				console.log(checkStatus,table);
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo U('simple/card/msDel'); ?>", {ids: ids,tab:'Withdraw'}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        });
		
      });
    </script>
</html>