<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\wwwroot\shouka.paimaivip.net\public/../application/simple\view\admin\admin_role.html";i:1616590069;s:73:"C:\wwwroot\shouka.paimaivip.net\application\Simple\view\public\heard.html";i:1616590069;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="/">首页</a>
            <a href="">系统管理</a>
            <a>
              <cite>角色管理</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('添加用户','<?php echo U('simple/admin/roleAdd'); ?>')"><i class="layui-icon"></i>添加权限</button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                              <thead>
                                <tr>

                                  <th>ID</th>
                                  <th>角色名</th>
                                  <th>状态</th>
								  <th>时间</th>
                                  <th>操作</th>
                              </thead>
                              <tbody>
							  <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$k): $mod = ($i % 2 );++$i;?>
                                <tr>
                                  <td><?php echo $k['group_id']; ?></td>
                                  <td><?php echo $k['title']; ?></td>
                                  <td><?php if($k['status'] == '1'): ?><span class="layui-btn layui-btn-normal layui-btn-mini">已启用</span><?php else: ?><span class="layui-btn layui-btn-danger layui-btn-mini">已停用</span><?php endif; ?></td>
                                  <td><?php echo date("Y/m/d H:i:s",$k['addtime']); ?></td>
                                  <td class="td-manage">
                                    <a onclick="member_stop(this,<?php echo $k['group_id']; ?>)" href="javascript:;"  title="<?php if($k['status'] == '1'): ?>启用<?php else: ?>停用<?php endif; ?>">
                                      <?php if($k['status'] == '1'): ?><i class="icon iconfont">&#xe719;</i><?php else: ?><i class="icon iconfont">&#xe71a;</i><?php endif; ?>
                                    </a>
                                    <a title="编辑"  onclick="xadmin.open('编辑','<?php echo U('simple/admin/roleAdd',['id'=>$k['group_id']]); ?>')" href="javascript:;">
                                      <i class="layui-icon">&#xe642;</i>
                                    </a>
                                    <a title="删除" onclick="member_del(this,<?php echo $k['group_id']; ?>)" href="javascript:;">
                                      <i class="layui-icon">&#xe640;</i>
                                    </a>
                                  </td>
                                </tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
                              </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                                <?php echo $list->render(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
    <script>
	
      /*用户-停用*/
      function member_stop(obj,id){
	         var s=1,str="启用",icon="&#xe719;",msg="已启用";
			 if($(obj).attr('title')=='启用'){
			    s=0,str="停用",icon="&#xe71a;",msg="已停用";
			 }
          layer.confirm('确认要'+str+'吗？',function(index){
		    
			 $.post("<?php echo U('simple/admin/roleKg'); ?>",{id:id,state:s},function(p){
			    if(p.code==1){
				    $(obj).attr('title',str);
					$(obj).find('i').html(icon);
					$(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html(msg);
					layer.msg(msg,{icon: 6,time:1000},function(){
					   location.reload();
					});
					
				}else{
				    layer.msg("修改失败！",{icon: 5,time:1000});
				}
			 })
          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              $.post('<?php echo U("simple/admin/groupDel"); ?>',{id:id},function(e){
				  $(obj).parents("tr").remove();
				  layer.msg('已删除!',{icon:1,time:1000});
			  })
              
          });
      }

    </script>
</html>