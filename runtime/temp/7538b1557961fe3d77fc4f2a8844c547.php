<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:93:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\member\mobile\withdraw_account.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css">
</head>
<body class="bg-gray setting-index-page">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">提现账号</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="setting-wrap w-wrap">
            <div class="setting-modules">
               <!--<a class="media" href="<?php if($num > '0'): ?><?php echo U('home/member/addweixin'); else: ?><?php echo U('home/member/weixin'); endif; ?>">-->
               <!--     <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/draw_wx_icon.png">-->
               <!--     <div class="media-body">-->
               <!--         <p class="clearfix">-->
               <!--             <span class="float-left left-txt">微信账号</span>-->
               <!--         </p>-->
               <!--     </div>-->
               <!-- </a>-->
                <a class="media" href="<?php echo U('home/member/bankAccount'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/draw_yhk_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">银行卡账号</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/aliPay'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/draw_zfb_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">支付宝账号</span>
                        </p>
                    </div>
                </a>
            </div>

        </main>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
</body>
</html>