<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:78:"D:\wwwroot\127.0.0.1\public/../application/simple\view\member\member_edit.html";i:1666142284;s:62:"D:\wwwroot\127.0.0.1\application\Simple\view\public\heard.html";i:1666142284;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form" id="addBankCardForm">
                    <div class="layui-form-item">
                        <label for="L_username" class="layui-form-label">
                            <span class="x-red">*</span>用户名</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_username" name="name" required="" value="<?php echo $u['user']; ?>" lay-verify="nikename" autocomplete="off" class="layui-input"></div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>将会成为您唯一的登入名</div></div>
                    <div class="layui-form-item">
                        <label for="L_email" class="layui-form-label">
                            <span class="x-red">*</span>邮箱</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_email" name="email" required="" value="<?php echo $u['email']; ?>" autocomplete="off" class="layui-input"></div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_photo" class="layui-form-label">
                            <span class="x-red">*</span>QQ号码</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_photo" name="qq" required=""   value="<?php echo $u['qq']; ?>" autocomplete="off" class="layui-input"></div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_photo" class="layui-form-label">
                            <span class="x-red">*</span>手机号</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_photo" name="photo" required="" value="<?php echo $u['photo']; ?>" lay-verify="photo" autocomplete="off" class="layui-input"></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_pass" class="layui-form-label">
                            <span class="x-red">*</span>密码</label>
                        <div class="layui-input-inline">
                            <input type="password" id="L_pass" name="pass" required="" lay-verify="pass" autocomplete="off" class="layui-input"></div>
                        <div class="layui-form-mid layui-word-aux">不修改留空</div></div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>确认密码</label>
                        <div class="layui-input-inline">
                            <input type="password" id="L_repass" name="repass" required="" lay-verify="repass" autocomplete='new-password'   class="layui-input"></div>
							<div class="layui-form-mid layui-word-aux">不修改留空</div>
                    </div>
					 <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>交易确认密码</label>
                        <div class="layui-input-inline">
                            <input type="password" id="L_repass" name="password" required="" lay-verify="password"    class="layui-input"></div>
							<div class="layui-form-mid layui-word-aux">不修改留空</div>
                    </div>
					 <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>API密钥</label>
                        <div class="layui-input-inline">
                            <input type="password" id="L_repass" name="key" required="" value="<?php echo $u['key']; ?>"   class="layui-input"></div>
							<div class="layui-form-mid layui-word-aux">不修改留空</div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red"></span>账户状态</label>
                        <div class="layui-input-inline">
                            <select name="state" lay-verify="" >
							  <option value="1" <?php if($u['state'] == '1'): ?>selected<?php endif; ?>>正常  </option>
							  <option value="0" <?php if($u['state'] == '0'): ?>selected<?php endif; ?>>未审核</option>
							  <option value="2" <?php if($u['state'] == '2'): ?>selected<?php endif; ?>>冻结</option>
							</select></div>
                    </div>
					<!--<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red"></span>提现权限</label>
                        <div class="layui-input-inline">
						 <select name="active" lay-verify="">
                           <option value="0" <?php if($u['active'] == '0'): ?>selected<?php endif; ?>>未开通</option>
							  <option value="1" <?php if($u['active'] == '1'): ?>selected<?php endif; ?>>已开通   </option></select>
                          </div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red"></span>分站权限</label>
                        <div class="layui-input-inline">
						 <select name="fenzhan" lay-verify="">
                           <option value="0" <?php if($u['fenzhan'] == '0'): ?>selected<?php endif; ?>>未开通</option>
							  <option value="1" <?php if($u['fenzhan'] == '1'): ?>selected<?php endif; ?>>已开通   </option></select></div>
                    </div>-->
                    

                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>开户行</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_bankname" name="bankname" required="" lay-verify="bankname"    class="layui-input" value="<?php echo $res['bankname']; ?>"></div>
							<div class="layui-form-mid layui-word-aux"></div>
                    </div>
                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>省/直辖市</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_province" name="province" required="" lay-verify="province"    class="layui-input" value="<?php echo $res['province']; ?>"></div>
							<div class="layui-form-mid layui-word-aux"></div>
                    </div>
                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>地级市</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_city" name="city" required="" lay-verify="city"    class="layui-input" value="<?php echo $res['city']; ?>"></div>
							<div class="layui-form-mid layui-word-aux"></div>
                    </div>
                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>市/县</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_county" name="county" required="" lay-verify="county"    class="layui-input" value="<?php echo $res['county']; ?>"></div>
							<div class="layui-form-mid layui-word-aux"></div>
                    </div>
                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>支行名称</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_zhibank" name="zhibank" required="" lay-verify="zhibank"    class="layui-input" value="<?php echo $res['zhibank']; ?>"></div>
							<div class="layui-form-mid layui-word-aux"></div>
                    </div>
                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>银行卡号</label>
                        <div class="layui-input-inline">
                            <input type="text" id="accounts" name="accounts" required="" lay-verify="accounts"    class="layui-input" value="<?php echo $res['accounts']; ?>"></div>
							<div class="layui-form-mid layui-word-aux"></div>
                    </div>
                    
                    
                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label"></label>
						<?php echo token(); ?>
						<input type="hidden" name="id" value="<?php echo $u['id']; ?>">
                        <button class="layui-btn" lay-filter="add" lay-submit="">修改</button></div>
                </form>
            </div>
        </div>
        <script>layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;
				layer.ready(function(){
				   var ok="<?php echo $ok; ?>";
				   if(!ok){
						layer.msg("参数错误",{time:1000,icon:2},function(){
								 xadmin.close();
							});
					}
				  });
                //自定义验证规则
                form.verify({
                    nikename: function(value) {
                        if (value.length < 5) {
                            return '用户名至少得5个字符啊';
                        }
                    },
                    //pass: [/(.+){6,12}$/, '密码必须6到12位'],
                    repass: function(value) {
                        if ($('#L_pass').val() != $('#L_repass').val()) {
                            return '两次密码不一致';
                        }
                    }
                });

                //监听提交
                form.on('submit(add)',
                   function(data) {
						var loading =layer.load(3, {shade: [0.1,'#fff']});
						$.post("<?php echo U('simple/member/memberEdit'); ?>",data.field,function(res){
								layer.close(loading);
								if (res.code==1) {
									layer.alert(res.msg,{icon:1},function(){
									  xadmin.close();
						              xadmin.father_reload();
									});
								}else{
									layer.alert(res.msg,{icon:2});
								}
						})
						return false;
                  
                });

            });</script>

    </body>
</html>