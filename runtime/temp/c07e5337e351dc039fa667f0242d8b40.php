<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:92:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\login\mobile\registered.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/form.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">注册</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="register-wrap w-wrap">
    <div class="form-pannel t44">
        <form class="" action="<?php echo U('home/login/regCode'); ?>" method="post" id="registerForm">
            <div class="form-group">
                <i class="user"></i>
                <input type="text" name="photo" class="form-control" id="registerName" placeholder="手机号" maxlength="50">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>

            <div class="form-group">
                <i class="shield"></i>
                <input type="text" name="code" class="form-control code" id="registerCode" placeholder="验证码" maxlength="4">
                <a class="right89 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="resend-code color-green" href="javascript:void(0);" id="getCode">发送验证码</a>
            </div>

            <div class="form-group">
                <i class="lock"></i>
                <input type="password" name="pass" class="form-control" id="registerPass" placeholder="设置登录密码" maxlength="20">
                <a class="right37 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="right isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
            </div>
            
            <div class="form-group">
                <i class="number"></i>
                <input type="text" name="inviteNumber" class="form-control" id="inviteNumber" value="<?php echo $gr; ?>" placeholder="推广员用户编号（选填）" maxlength="20">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>
        </form>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="registerBtn">注册并登录</a>

        <!--协议-->
        <div class="agreement-box">
            <label class="isAgree">
                <em class="checkbox active" state="on"></em>我已阅读并同意
            </label>
            <a class="agreement color-blue" href="<?php echo U('home/index/newsDetail',['id'=>9]); ?>">《<?php echo C('title'); ?>用户协议》</a>
        </div>

        <!--<div class="login_way">-->
        <!--    <p></p><span>第三方登录</span>-->
        <!--</div>-->
        <!--<div class="row justify-content-center third-login">-->
            <!-- <a class="col-3" href="#">
        <!--        <span class="w80 weixinIcon"></span>-->
        <!--    </a> -->
        <!--    <a class="col-3" href="http://api.renrenxiaoka.com/m/passport/qq/login.do">-->
        <!--        <span class="w80 qqIcon"></span>-->
        <!--    </a>-->
        <!--</div>-->
    </div>
</main>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/register/register.js?v=15"></script>
<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:"",error:""};
	var regurl="<?php echo U('home/api/sendsms'); ?>",regnum="<?php echo U('home/login/verifyInviteNumber'); ?>";
</script>

</body>
</html>