<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:106:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\order\mobile\getSellCardOrdersDetails.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/detail.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">订单详情</a>
    <a class="w44" href="<?php echo U('home/member/index'); ?>">
        <img src="/static/home/images/common/return_me_icon.png">
    </a>
</header>

<!--中间内容部分-->
<main class="w-wrap sellcard-detail-wrap">
	<input type="hidden" value="<?php echo $da['type']; ?>" id="orderType" />
	<input type="hidden" value="<?php echo $da['id']; ?>" id="orderId" />

	<div id="detailCon"></div>

    <!--只有当订单处于“已完成”状态，并且该订单有“回收失败”状态下的卡密，才会有这个按钮-->
    <input type="hidden" id="canBeChangeSubmitType" value="" />
    <a href="javascript:void(0);" class="btn btn-orange btn-full d-none" id="submitAllFailCard">一键提交失败卡密</a>
    <div class="list-box mb-3 d-none">
        <div class="list-title">卡密列表</div>
        <div class="list-content">
            <ul>

            </ul>
        </div>

    </div>

</main>
<!--返回顶部按钮-->
<a href="javascript:void(0);" class="totop" id="totop"></a>
<script>
    var ctxPath = {domain: "",domainapi: "",csrf:"",staticDomain:""};
	var faurl="<?php echo U('home/order/getSellCardOrdersDetailsa'); ?>";
	var oUrl="<?php echo U('home/api/retijiao'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<script src="/static/home/js/mobile//common/clipboard.min.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/common.js"></script>
<script src="/static/home/js/mobile/sellCardRecord/sellCardOrdersDetail.js?v=200729"></script>
</body>
</html>