<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:76:"D:\wwwroot\127.0.0.1\public/../application/simple\view\admin\admin_list.html";i:1667656648;s:62:"D:\wwwroot\127.0.0.1\application\Simple\view\public\heard.html";i:1667656648;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">系统管理</a>
            <a>
              <cite>管理员列表</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('添加用户','<?php echo U('simple/Admin/adminAdd'); ?>')"><i class="layui-icon"></i>添加</button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form" id="list" lay-filter="list">
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div> 
    </body>
	<script type="text/html" id="status">
    <input type="checkbox" name="state" value="{{d.id}}" lay-skin="switch" lay-text="已启用|已停止" lay-filter="state" {{ d.state == 1 ? 'checked' : '' }}>
    </script>
	<script type="text/html" id="action">
    <a onclick="xadmin.open('编辑权限','<?php echo U('simple/Admin/adminEdit'); ?>?id={{d.id}}')" class="layui-btn layui-btn-xs">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
   </script>
    <script>
	layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery;
        tableIn = table.render({
            elem: '#list',
            url: "<?php echo U('simple/admin/adminList'); ?>",
            method: 'post',
            cols: [[
			     {field: 'id', align: 'center',title: 'ID',width:80},
                {field: 'user', align: 'center',title: '登陆名', width:150},
                {field: 'tel', align: 'center',title: '手机号',width:150},
                {field: 'email',align: 'center', title: '邮箱',width:150},
                {field: 'title',align: 'center', title: '角色',width:150},
				{field: 'regtime',align: 'center', title: '加入时间',width:200},
                {field: 'state',align: 'center',title: '状态', toolbar: '#status',width:150},
                {title: '操作',align: 'center', toolbar: '#action',width:600}
            ]]
        }); 
		form.on('switch(state)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = this.value;
            var authopen = obj.elem.checked===true?1:0;
			var str="启用";
			 if(authopen==1){
			    str="停用";
			 }
				$.post('<?php echo U("simple/admin/editS"); ?>',{'id':id,'authopen':authopen},function (res) {
					layer.close(loading);
					if (res.code==1) {
						tableIn.reload();
					}else{
						layer.msg(res.msg,{time:1000,icon:2});
						return false;
					}
				})
        });
		table.on('tool(list)', function(obj){
            var data = obj.data;
			console.log(data);
            if(obj.event === 'del'){
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/admin/delS'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
            }
        });
      });
    </script>
</html>