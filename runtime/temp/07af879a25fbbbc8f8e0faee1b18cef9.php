<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:72:"D:\wwwroot\127.0.0.1\public/../application/home\view\member\ali_pay.html";i:1667656648;s:61:"D:\wwwroot\127.0.0.1\application\Home\view\layout\member.html";i:1667656648;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
    <li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>
    <li class="active" ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
     <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
    <!-- <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
    <li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
</ul>                <div class="tab-content">
                    <div class="tab-pane active">
					<?php if($u['real'] > '0'): ?>
						  <div class="panel-body card-list-body">
                                                      <!--③、已经添加成功银行卡-->
							<?php if(is_array($li) || $li instanceof \think\Collection || $li instanceof \think\Paginator): if( count($li)==0 ) : echo "" ;else: foreach($li as $key=>$p): ?>						  
                            <div class="card-list-item">
                                <div class="card-item-icon pull-left"><i class="round-ali-ico"></i></div>
                                <div class="status-box-text">
                                    <h3><?php echo $u['username']; ?><a class="btn btn-green btn-xs m-l-xs" href="javascript:void(0);">已实名</a></h3>
                                    <p><span class="color999">所属银行：</span><?php echo $p['bankname']; ?></p>
                                    <p><span class="color999">银行卡号：</span><?php echo tfen($p['accounts'],6,4); ?></p>
                                </div>
                                <input type="hidden" value="<?php echo $p['id']; ?>" name="id">
                                <a class="update-ico">修改</a>
                                <i class="delete-ico"></i>
                            </div>
							<?php endforeach; endif; else: echo "" ;endif; if($num < '5'): ?>
                                                        <!--有5张银行卡则需隐藏按钮-->
                            <a class="btn btn-blue" href="javascript:addAliPayAccount();">继续添加支付宝</a>
                            <?php endif; ?>
                        </div>

						<?php else: ?>
                        <div class="panel-body">
                            <!--①、未实名，且未添加银行卡-->
                            <div class="status-box">
                                <div class="status-box-icon pull-left"><i class="ico140 red-card"></i></div>
                                <div class="status-box-text">
                                    <h3 class="text-red">您还没有进行实名认证，无法添加银行卡！</h3>
                                    <p class="color999">添加银行卡前必须先进行实名认证</p>
                                    <p class="color999">如有问题，请联系客服咨询</p>
                                    <a class="btn btn-blue phone" href="javascript:gotoRealnamePage();"><i class="id-card-ico"></i>实名认证</a>
                                </div>
                            </div>
                        </div>
						<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--删除银行卡确认交易密码模态框-->
<div class="modal inmodal" id="sureTradePwdModal">
    <div class="modal-dialog modal-dialog-w500">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">删除支付宝 - 确认交易密码</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="password" placeholder="请输入交易密码" class="form-control inline-block" id="tradePassword">

                    <!--没有设置交易密码-->
                                        <a class="text-red" href="javascript:gotoTradePwdPageFromModel();">忘记交易密码？</a>
                    
                </div>
                <div class="form-group">
                    <div class="errorTip color_f00"></div>
                </div>
                <input type="hidden" value="" id="delId">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue btn-w-full" onclick="sureDelSeltedAliPayAccount();">确定</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="<?php if(!(empty($u['jiaoyimima']) || (($u['jiaoyimima'] instanceof \think\Collection || $u['jiaoyimima'] instanceof \think\Paginator ) && $u['jiaoyimima']->isEmpty()))): ?><?php echo md5($u['jiaoyimima']); endif; ?>" id="isOpenTradePaaword"><!--是否开启交易密码-->

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var addbank = "<?php echo U('home/member/addali'); ?>",jiyi="<?php echo U('home/member/editPass'); ?>",del="<?php echo U('home/member/deleteBankCard'); ?>";
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/profileManage/aliPayAccount/noAddAliPayAccount.js?v=12"></script>



</body>
</html>
