<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:86:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\member\mobile\edit_bank.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>


    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/withdrawAccount/withdrawAccount.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);"><?php if($type == '1'): ?>支付宝账号<?php else: ?>银行卡账号<?php endif; ?></a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="w-wrap">
    <!--继续添加支付宝账号表单-->
    <div class="addAliAccount-warp">
        <p class="top-tip tip-ico-parent"><?php if($type == '1'): ?>支付宝账号<?php else: ?>银行卡账号<?php endif; ?>用户名，必须与认证的姓名一致，否则无法提现</p>
        <div class="setting-modules">
            <form class="" action="" method="post" id="addAliAccountForm">
                <div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                    <div class="media-body no-bg">
                        <p class="d-flex clearfix">
                            <span class="left-txt">真实姓名</span>
                            <span class="right-txt"><?php echo $u['username']; ?></span>
                        </p>
                    </div>
                </div>
                <div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                    <div class="media-body no-bg">
                        <p class="d-flex clearfix">
                            <span class="left-txt">身份证号</span>
                            <span class="right-txt"><?php echo tfen($u['idcard'],4,8); ?></span>
                        </p>
                    </div>
                </div>
                
				<?php if($type != '1'): ?>
				<div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_bank_icon.png">
                    <div class="media-body no-bg">
                        <div class="d-flex clearfix">
                            <span class="left-txt">开户银行</span>
                            <div class="form-group right-txt">
		                        <select class="form-control" name="bankname" id="bankNameSel" >
								  <option value="" >请选择</option>
								  <?php if(is_array($banks) || $banks instanceof \think\Collection || $banks instanceof \think\Paginator): $i = 0; $__LIST__ = $banks;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$p): $mod = ($i % 2 );++$i;if($res['bankname'] == $p['bankName']): ?>
									 <option value="<?php echo $p['bankName']; ?>" selected><?php echo $p['bankName']; ?></option>
									<?php else: ?>
									<option value="<?php echo $p['bankName']; ?>" ><?php echo $p['bankName']; ?></option>
									<?php endif; endforeach; endif; else: echo "" ;endif; ?>
		                        </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_provin_icon.png">
                    <div class="media-body no-bg">
                        <div class="d-flex clearfix">
                            <span class="left-txt">省/直辖市</span>
                            <div class="form-group right-txt">
                            	
                            	<select class="form-control" name="province" id="province" >
		                        	<option value="" code="">请选择</option>
		                        </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_city_icon.png">
                    <div class="media-body no-bg">
                        <div class="d-flex clearfix">
                            <span class="left-txt">地级市：</span>
                            <div class="form-group right-txt">
                            	<select class="form-control" name="city" id="city" >
		                        	<option value="" code="">请选择</option>
		                        </select>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_city_icon.png">
                    <div class="media-body no-bg">
                        <div class="d-flex clearfix">
                            <span class="left-txt">市/县</span>
                            <div class="form-group right-txt">
                            	<select class="form-control" name="county" id="county" >
		                        	<option value="" code="">请选择</option>
		                        </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_bank_icon.png">
                    <div class="media-body no-bg">
                        <div class="d-flex clearfix">
                            <span class="left-txt">支行名称</span>
                            <div class="form-group right-txt">
                               <input name="zhibank" id="zhibank" type="text" value="<?php echo $res['zhibank']; ?>" class="form-control bg-white" placeholder="请填写支行名称" >
                            </div>
                        </div>
                    </div>
                </div>
				<?php endif; ?>
				<div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/<?php if($type == '1'): ?>aliPayAccount/person_zfb_icon.png<?php else: ?>bankAccount/card_bcard_icon.png<?php endif; ?>">
                    <div class="media-body no-bg">
                        <p class="d-flex clearfix">
                            <span class="left-txt"><?php if($type == '1'): ?>支付宝账号<?php else: ?>银行卡账号<?php endif; ?></span>
                            <span class="form-group right-txt">
                                <input name="account" type="text" value="<?php echo $res['accounts']; ?>" class="form-control bg-white" placeholder="请填写您的<?php if($type == '1'): ?>支付宝账号<?php else: ?>银行卡账号<?php endif; ?>" id="aliPayAccount" readonly onfocus="this.removeAttribute('readonly');">
                                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="media">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_pass_icon.png">
                    <div class="media-body no-bg">
                        <p class="d-flex clearfix">
                            <span class="left-txt">交易密码</span>
                            <span class="form-group right-txt">
                                <input name="tradePassword" type="password" value="" class="form-control bg-white" placeholder="请填写您的交易密码" id="tradePassword" readonly onfocus="this.removeAttribute('readonly');">
                                <a class="right52 delete" href="javascript:void(0);"><b class="w30"></b></a>
                                <a class="right isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
                            </span>
                        </p>
                    </div>
                </div>
				<input name="atype" type="hidden" value="<?php echo $type; ?>">
				<input name="id" type="hidden" value="<?php echo $id; ?>">
            </form>
        </div>

        <div class="clearfix">
            <a class="float-right forgot-password" href="<?php echo U('home/member/resetTradePasswordInit'); ?>">忘记交易密码</a>
        </div>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="submitAliPayAccountFormBtn"> 立即更新 </a>
    </div>

</main>

<script>
    var ctxPath = {domain: "",domainapi: "",csrf:"660c07bb-2f57-4539-a87d-7808ca39a90e",staticDomain:""};
</script>
<?php if($type == '1'): ?>
<script>
    var ctxurl = "<?php echo U('home/member/addali'); ?>";
</script>
<?php else: ?>
<script>
    var ctxurl = "<?php echo U('home/member/addbank'); ?>";
</script>
<?php endif; ?>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/withdrawAccount/aliPayAccount/addAliPayAccountInit.js?v=784"></script>
<script class="resources library" src="/static/home/js/syit.js" type="text/javascript"></script>
<script type="text/javascript">_init_area();preselect('<?php echo $res['province']; ?>','<?php echo $res['city']; ?>','<?php echo $res['county']; ?>');</script>
</body>
</html>