<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:90:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\setting.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css">
	
</head>
<body class="bg-gray setting-index-page">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">设置</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="setting-wrap w-wrap">
            <div class="setting-modules">
                <a class="media" href="<?php echo U('home/member/info'); ?>"><!--跳转到个人信息页面-->
                    <img class="w40 align-self-center" src="/static/home/images/setting/list_mune_safe.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">账户安全</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/index/help'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/setting/list_mune_help.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">帮助中心</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="javascript:void(0);" onclick="clearCache();">
                    <img class="w40 align-self-center" src="/static/home/images/setting/list_mune_clean.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">清理缓存</span>
                            <span class="float-right right-txt"></span>
                        </p>
                    </div>
                </a>
            </div>

            <a href="javascript:loginOut()" class="btn btn-blue btn-full">退出账号</a>
        </main>
    </div>
</div>

<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
	var loginout="<?php echo U('home/login/loginout'); ?>",index="<?php echo U('home/index/index'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/setting/setting.js"></script>
</body>
</html>