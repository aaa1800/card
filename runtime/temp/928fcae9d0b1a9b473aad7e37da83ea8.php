<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:85:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\sell_card.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 首页内容(账户总览)</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
    <link href="/static/home/css/common/common.css?v=20181220" rel="stylesheet"/>
    <link href="/static/home/js/common/layer/skin/layer.css" rel="stylesheet"/>
    <link href="/static/home/css/index/index.css?v=20190729" rel="stylesheet"/>
    <link href="/static/home/css/recycle/recycle.css?v=20181212" rel="stylesheet"/>
    <link href="/static/home/css/sellCard/sellCard.css" rel="stylesheet"/>
    <script>var part="<?php echo U('home/index/retrieve'); ?>";</script>
    <script language="javascript" type="text/javascript" src="/static/home/js/common/jquery-1.9.min.js"></script>
    <script type="text/javascript" src="/static/home/js/common/layer/layer.js"></script>
    <script type="text/javascript" src="/static/home/js/common/common.js?v=20181224"></script>
	<script type="text/javascript" src="/static/home/js/common/jquery.cookie.js"></script>
    <script type="text/javascript" src="/static/home/js/common/recycle-cookie.js"></script>
    <script type="text/javascript" src="/static/home/js/recycle/recycle-data.js?v=251"></script>
    <script type="text/javascript" src="/static/home/js/recycle/recycle-comm.js?v=201906181254"></script>
    <script type="text/javascript" src="/static/home/js/index/index.js"></script>
    <script type="text/javascript" src="/static/home/js/common/cardRecycle-comm.js?v=20191202"></script>
    <script src="/static/home/js/common/iframe_refresh.js"></script>


	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body>
<div class="warp sell-card-page">
    <form id="cardForm" name="cardForm" action="<?php echo U('home/api/batchCard'); ?>" method="post">
<div class="card-recovery">
	<div class="w1320">
		<div class="card-recovery-inner">
			<input type="hidden" id="submitPlatform" name="submitFrom" value="1" /><!-- 提交平台：1、PC端	3、手机端 -->
			
						<div class="recovery-card-type">
    <div class="card-recovery-tit"><p>选择卡类：</p></div>
    <div class="card-recovery-cont">
						<div class="card-recovery-list <?php if($type == '0'): ?>active<?php endif; ?>" id="0">
						<div id="yellowTag">
			<!-- <em class = "yellow-tag"></em> -->
			</div>
			
			<div class="card-recovery-bg card-recovery-bill">
							
				<b class="mini-ico"></b>
				<i class="max-icon icon-treat"></i>
				<a href="<?php echo U('home/member/sellCard',['type'=>0]); ?>">话费卡回收</a>
			</div>
		</div>
						<div class="card-recovery-list <?php if($type == '1'): ?>active<?php endif; ?>" id="1">
						<div class="card-recovery-bg card-recovery-game">
							
				<b class="mini-ico"></b>
				<i class="max-icon icon-treat"></i>
				<a href="<?php echo U('home/member/sellCard',['type'=>1]); ?>">游戏卡回收</a>
			</div>
		</div>
						<div class="card-recovery-list <?php if($type == '3'): ?>active<?php endif; ?>" id="3">
						<div class="card-recovery-bg card-recovery-fuel">
							
				<b class="mini-ico"></b>
				<i class="max-icon icon-treat"></i>
				<a href="<?php echo U('home/member/sellCard',['type'=>3]); ?>">加油卡回收</a>
			</div>
		</div>
						<div class="card-recovery-list <?php if($type == '4'): ?>active<?php endif; ?>" id="4">
						<div class="card-recovery-bg card-recovery-ecommerce">
							
				<b class="mini-ico"></b>
				<i class="max-icon icon-treat"></i>
				<a href="<?php echo U('home/member/sellCard',['type'=>4]); ?>">商超卡回收</a>
			</div>
		</div>
						<div class="card-recovery-list <?php if($type == '5'): ?>active<?php endif; ?>" id="5">
						<div class="card-recovery-bg card-recovery-video">
							
				<b class="mini-ico"></b>
				<i class="max-icon icon-treat"></i>
				<a href="<?php echo U('home/member/sellCard',['type'=>5]); ?>">视频卡回收</a>
			</div>
		</div>
						<div class="card-recovery-list <?php if($type == '6'): ?>active<?php endif; ?>" id="6">
						<div class="card-recovery-bg card-recovery-ebusiness">
							
				<b class="mini-ico"></b>
				<i class="max-icon icon-treat"></i>
				<a href="<?php echo U('home/member/sellCard',['type'=>6]); ?>">电商卡回收</a>
			</div>
		</div>
						<input type="hidden" id="productClassifyId" name="productClassifyId" value="<?php echo $type; ?>" />

	</div>

</div>						
			<!-- 话费卡回收 -->
			<div class="recovery-card-major active">
				<div class="recovery-card-mode">
					<div class="card-recovery-tit">
						<p>选择卡种：</p>
					</div>
					<div class="card-recovery-cont" id="operatorsWrap">
					</div>
					<input type="hidden" id="operatorId" name="channelid" value="" />
					<input type="hidden" id="chooseOperatorId" name="chooseOperatorId" value="" />
				</div>
				<div class="recovery-card-par">
					<div class="card-recovery-tit">
						<p>选择面值：</p>
					</div>
					<div class="card-recovery-cont" id="priceWrap"></div>
					<div class="card-recovery-duration" id="recentConsumeDuration"></div>
					<div class="batch-consume-duration" id="bathConsumeDuration"></div>
					<input type="hidden" id="price" name="money" value="" />
				</div>
				
				<div class="submit-mode-inner hide" id="discountWrap">
	    			<span class="submit-mode-tit">
	    				供货折扣：
	    			</span>
	    			<span class="submit-mode-input">
	    				<input type="text" name="discount" id="cardDiscount" class="submit-mode-i" placeholder="允许输入的折扣是0%到0%" />
	    			</span>
	    			<span class="submit-mode-float">
	    				<div class="submit-float">
	    					<i class="submit-float-left"></i><span id="discountTip">可以输入的折扣值：0% ~ 0%，数值越高回收越慢</span>
	    				</div>
	    				<p class="submit-mode-green">
	    					根据您输入的折扣，该卡￥500面值，回收价格是￥460元
	    				</p>
	    			</span>
	    		</div>

				<div class="recovery-card-mode recovery-card-submit">
					<div class="card-recovery-tit">
						<p>提交方式：</p>
					</div>
					<div class="card-recovery-cont">
						<div class="recovery-mode-list active">
							<i class="card-icon icon-batch"></i>
							<i class="max-icon icon-treat"></i>
							批量提交
						</div>
						<div class="recovery-mode-list">
							<i class="card-icon icon-single-card"></i>
							<i class="max-icon icon-treat"></i>
							单卡提交
						</div>
						<!--<div class="recovery-mode-list" onclick="location='/autoSendme.html'">
							<i class="card-icon icon-single-card"></i>
							<i class="max-icon icon-treat"></i>
							智能提交
						</div>-->
						<!--&lt;!&ndash; 三角形 &ndash;&gt;-->
												<div id="activityTips">
							<!-- <div class="triangle_border_left">
							    <span></span>
							</div>
							<span class="retrieve-span">
								新用户注册，首张话费卡按面值回收
							</span>
							<a class="retrieve-detail" href="http://www.renrenxiaoka.com/retrieveActivity/retrieve.html"><span>查看活动详情</span></a> -->
						</div>	
						
											</div>
					<div class="recovery-mode-error" id="recycleRulesWrap">
						【卡密规则】 <span id="cardRules"></span><span id="cardTips">；面值一定要选择正确，如造成损失后果自负！</span>
					</div>
					<input type="hidden" id="type" name="type" value="2" />
					<input type="hidden" id="cardNumRule" value="" />
					<input type="hidden" id="cardPassRule" value="" />
			    </div>
				<div class="recovery-submit-mode">
					<!-- <input type="hidden" name="type" value="" />
				  	<input type="hidden" name="cardtype" value="0" />
				  	<input type="hidden" name="cardprice" value="0" /> -->
			    	<!-- 批量提交 -->
			    	<div class="submit-mode-cont active" id="batch">
			    		<div class="card-recovery-tit">
							<p>卡号/卡密：</p>
						</div>
						
						<div class="card-recovery-cont">
							<div class="batch-cont">
								<textarea class="form-control" name="tempcards"  id="batchCardlist" ></textarea>
								<input type="hidden" name="cards" id="cards" />
								<div class="batch-textarea-erro">
									<i class="comm-icon icon-error2"></i>
									<p class="txt-red"></p>
								</div>
							</div>
							<div class="batch-prompt">
								<!-- <div class="batch-prompt-error">
									<i class="comm-icon icon-error"></i>【卡密规则】 卡号为19位，密码18位；面值一定要选择正确，如造成损失后果自负！
								</div> -->
								<div class="batch-prompt-info">
									<div class="batch-info">
										<i class="service-arrow-left"> </i>
										卡号与卡密之间请用英文或者中文逗号或者空格隔开，每张
										卡占用一行用“回车键”隔开，例如：
										0111001702060546320，100304016583597830
										0111001702060546321，105754083583500026	
									</div>
									<div class="batch-info nocode">
										<i class="service-arrow-left"> </i>
										此卡种无需卡号，只需填写卡密，<br>每张一行用<strong class="text-orange">"回车(Enter键)"</strong>隔开！
									</div>
								</div>

							</div>
										
							<div class="card-recovery-operation">
								<div class="card-operation">
									<a href="javascript:void(0);" class="card-operation-btn">整理卡号/卡密</a>
									<p class="card-operation-txt">已经输入<i class="txt-red">0</i>张面值<i class="txt-orange">￥<span id="priceTip">0</span></i>元的卡，每次最多可提交<span id="maxSubmitCount">0</span>张</p>
									<p class="card-operation-txt card-recovery-limit"></p>
								</div>
								<div class="card-recovery-agree">
									<i class="max-icon icon-agree-up icon-agree-off"></i>
									我已阅读，理解并同意<a href="<?php echo U('home/index/help',['type'=>58]); ?>" target="_blank">《转让协议》</a>									
								</div>
								<div class="card-recovery-submit">
									<input type="hidden" id="isAgree2" name="isAgree2" value="1"/>
									<a href="javascript:void(0);" class="card-submit-btn" id="cardBatchBtn">确定批量提交</a>
								</div>
							</div>
						</div>
						
			    	</div>

			    	<!-- 单卡提交 -->
			    	<div class="submit-mode-cont" id="single"> 
			    		<div class="submit-mode-inner">
			    			<span class="submit-mode-tit">
			    				输入卡号：
			    			</span>
			    			<span class="submit-mode-input">
			    				<input type="text" class="submit-mode-i" name="cardNumber" id="cardNum" placeholder="请输入充值卡号" />
			    			</span>
			    			<span class="submit-mode-tit">
			    				输入卡密：
			    			</span>
			    			<span class="submit-mode-input">
			    				<input type="text" class="submit-mode-i" id="cardPass" placeholder="请输入充值卡密" />
								<input type="hidden" name="cardPassword" id="cardPassHid">
			    			</span>
			    		</div>
			    		<div class="mode-inner-error">
			    			<i class="comm-icon icon-error2"></i><p class="txt-red"></p>
			    		</div>
			    		<div class="card-recovery-cont submit-mode-padd">
			    			<div class="card-recovery-operation">
								<!-- <div class="card-operation">
									<a href="javascript:void(0);" class="card-operation-btn">整理卡号/卡密</a>
									<p class="card-operation-txt">已经输入<i class="txt-red">0</i>张面值<i class="txt-orange">￥0</i>元的卡，每次最多可提交100张</p>
								</div> -->
								<div class="card-recovery-agree">
									<i class="max-icon icon-agree-up icon-agree-off"></i>
									我已阅读，理解并同意<a href="<?php echo U('home/index/help',['type'=>58]); ?>" target="_blank">《转让协议》</a>
									<span class="card-recovery-limit"></span>
								</div>
								<div class="card-recovery-submit">
									
								  	<input type="hidden" id="isAgree1" name="isAgree1" value="1"/>
								  	<input type="hidden" id="csrf" name="_csrf" value="e73ba083-c73f-4c4c-b2f2-9f51170855b7"/>
									<a href="javascript:void(0);" class="card-submit-btn" id="cardOneBtn">确定单卡提交</a>
								</div>
							</div>
			    			
			    		</div>
						
			    	</div>
			    	
			    				    						<input type="hidden" id="memberNumber" value="90532" /><!-- 用户编号 -->
										
					<input type="hidden" id="memberTodayLimit" value="" /><!--用户今日限额-->
					<input type="hidden" id="discountHidFromId" value="" />
					<input type="hidden" id="discountHidEndId" value="" />
			    </div>

			</div>

		</div>
	</div>
</div>
</form>

<div class="main trade-flow-main">
	<div class="main-notice">
		<div class="w1320 recycle-main">
			<h3>交易流程</h3>
			<div class="notice-list">
				<ul>
					<li>
						<i class="max-icon icon-user">
						</i>
						<p>
							1. 注册帐号，登录帐户
						</p>
					</li>
					<li class="notice-list-spot">
						<span></span>
						<span></span>
						<span></span>
					</li>
					<li>
						<i class="max-icon icon-card">
						</i>
						<p>
							2. 提交卡密，等待验卡
						</p>
					</li>
					<li class="notice-list-spot">
						<span></span>
						<span></span>
						<span></span>
					</li>
					<li>
						<i class="max-icon icon-money">
						</i>
						<p>
							3. 验卡成功，资金到账
						</p>
					</li>
					<li class="notice-list-spot">
						<span></span>
						<span></span>
						<span></span>
					</li>
					<li>
						<i class="max-icon icon-withdrawals">
						</i>
						<p>
							4. 账户提现，交易成功
						</p>
					</li>
				</ul>
			</div>
			<div class="recycle-main-cont">

				<div class="recycle-explain-list recycle-list1  active">
					<i class="recycle-up"></i>
					<div class="recycle-main-explain">
						
						<p>
							1、提交卡密之前请先登录您的账户，您当前已登录；
						</p>
						<p>
							2、如您还没有账号，可先前往<a href="<?php echo U('home/login/registered'); ?>">注册</a>；
						</p>
					</div>
				</div>
				<div class="recycle-explain-list recycle-list2">
					<i class="recycle-up"></i>
					<div class="recycle-main-explain">
						
						<p>
							1、请务必按照格式提交正确的卡号卡密，以免浪费您的时间并给您造成不必要的困扰；
						</p>
						<p>
							2、请绑定手机号码，或者填写正确的联系QQ，以便出现问题时可以及时和您联系；
						</p>
					</div>
				</div>
				<div class="recycle-explain-list recycle-list3">
					<i class="recycle-up"></i>
					<div class="recycle-main-explain">
						
						<p>
							1、验证成功之后会提交到后台进行销卡处理；
						</p>
						<p>
							2、如果您的充值卡之前还没有被消耗，则处理成功之后金额会直接存入你的零钱或余额账户中；
						</p>
					</div>
				</div>
				<div class="recycle-explain-list recycle-list4">
					<i class="recycle-up"></i>
					<div class="recycle-main-explain">
						
						<p>
							1、销卡成功之后，如果金额是存入你的余额账户中，则您可以进行提现处理；
						</p>
						<p>
							2、提现方式目前支持微信零钱提现和银行卡提现，因为某些原因，某个提现方式可能会临时关闭维护；
						</p>
					</div>
				</div>
				

			</div>

		</div>
	</div>
	
</div>

<div class="card-height">

</div>


    <!-- 返回顶部 -->
    <div class="back-top">
        <a href="javascript:void(0);" id="btnTop"></a>
    </div>
</div>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

</body>
</html>
