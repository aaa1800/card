<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:87:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\mima.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css">
</head>
<body class="bg-gray setting-index-page">
<!--固定头部-->

<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">密码管理</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="setting-wrap w-wrap">
            <div class="setting-modules">
                <a class="media" href="<?php echo U('home/member/editPass',['cz'=>1]); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/passwordManage/log_password_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">登录密码</span>
                            <span class="float-right right-txt" id="loginPwdStatus"></span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/editPass',['cz'=>2]); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/passwordManage/tran_password_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">交易密码</span>
                            <span class="float-right right-txt" id="tradePwdStatus"></span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/resetTradePasswordInit'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/passwordManage/forget_password_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">忘记交易密码</span>
                            <span class="float-right right-txt" id="forgetTradePwdStatus"></span>
                        </p>
                    </div>
                </a>
            </div>

        </main>
    </div>
</div>

<script>
	var ctxPath = {domain: "",domainapi: "",csrf:"660c07bb-2f57-4539-a87d-7808ca39a90e",staticDomain:""};	
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/profileManage/passwordManage/index.js"></script>
</body>
</html>