<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:75:"C:\wwwroot\8.210.135.237\public/../application/simple\view\gaoji\index.html";i:1605265943;s:66:"C:\wwwroot\8.210.135.237\application\Simple\view\public\heard.html";i:1605265943;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<style>
.site-doc-icon{
    margin-bottom: 10px;
	height: auto;
	display: inline-block;
}

</style>
    <body>
	<div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">会员管理</a>
            <a>
              <cite>高级认证管理</cite></a>
          </span>
		  <span style="float:right">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a></span>
        </div>
       <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
						<form class="layui-form layui-col-space5">
                                <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="开始日期" name="start" id="start">
								</div>
								<div class="layui-inline layui-show-xs-block">
									<input class="layui-input"  autocomplete="off" placeholder="截止日期" name="end" id="end">
								</div>
								<div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="tdata" placeholder="UID" autocomplete="off" class="layui-input">
								</div>
								<div class="layui-inline layui-show-xs-block">
								 <select name="type" id="selt" lay-skin="select">
								     <option value="">请选择状态</option>
									 <option value="3">审核中</option>
									 <option value="1">审核通过</option>
									 <option value="2">审核失败</option>
								 </select>
                                    </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn" type="button" lay-submit="" lay-filter="sreach" lay-skin="select" lay-filter="type">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
								</form>
                        </div>
						 
                        <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
	
	<script type="text/html" id="state">
    {{#  if(d.state == 1){ }}
      <p class="layui-bg-red">已审核</p>
     {{# }else if(d.state==2){ }}
     <p class="layui-bg-black">退回</p>
     {{#  } else { }}
	  <p class="layui-bg-blue">未审核</p>
	 {{#  } }}
    </script>
	<script type="text/html" id="pic">
	  <div class="layui-btn-group">
		<button class="layui-btn" onclick="xadmin.open('证件图片','<?php echo U('simple/gaoji/pic'); ?>?id={{d.id}}')">证件图片</button>
	  </div>
	</script>
	</script>
	<script type="text/html" id="action">
		<div class="layui-btn-group">
			<button class="layui-btn" lay-event="success" >通过</button>
			<button class="layui-btn layui-btn-normal" lay-event="error" >退回</button>
			<button class="layui-btn layui-btn-danger"  lay-event="del">删除</button>
		</div>
		</script>
	 <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "<?php echo U('simple/gaoji/index'); ?>",
            method: 'post',
			page:true,
            cols: [[
			    {type: 'checkbox',width:50,minWidth:30},
				{field:'id',title:"ID", width:60,align: 'center', sort: true},
			    {field:'uid',title: '用户ID',align: 'center', width:120},
				{field:'username',title: '真实姓名',align: 'center'},
				{align: 'center',toolbar: '#pic',title:'证件照片'},
				{field: 'state',align: 'center', title: '状态',sort: true,toolbar: '#state',width:120},
				{field: 'remarks',align: 'center', title: '备注',width:150,edit:"#text"},
                {title: '操作',align: 'center', toolbar: '#action',width:180}
            ]],
			limit:20,
			limits:[20,50,60]
        });	
		var nowTime=new Date();    
    var startTime=laydate.render({
    	elem:'#start',
    	type:'datetime',
    	btns: ['confirm'],
    	max:'nowTime',//默认最大值为当前日期
    	done:function(value,date){
    	    endTime.config.min={    	    		
    	    	year:date.year,
    	    	month:date.month-1,//关键
                date:date.date,
                hours:date.hours,
                minutes:date.minutes,
                seconds:date.seconds
    	    };
    	    
    	}
    })
    var endTime=laydate.render({
    	elem:'#end',
    	type:'datetime',
    	btns: ['confirm'],
    	max:'nowTime',
    	done:function(value,date){   	   
    	    startTime.config.max={
    	    		year:date.year,
        	    	month:date.month-1,//关键
                    date:date.date,
                    hours:date.hours,
                    minutes:date.minutes,
                    seconds:date.seconds
    	    }
    	    
    	}
    })		
		form.on('submit(sreach)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var name=$("input[name=tdata]").val(),kk=$("#selt").val(),st=$("#start").val(),se=$("#end").val();
			
			var index = layer.load(3, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
					});
                 tableIn.reload({
					page:{curr:1
						 },
                     where:{
						 name:name,
						 kk:kk,
						 st:st,
						 se:se
                     }
                 });
				 layer.close(index);
        });
		table.on('edit(mlist)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = obj.data.id,str = obj.value,field=obj.field;
            $.post('<?php echo U("simple/gaoji/doEdit"); ?>',{'id':id,'count':str,'field':field},function (res) {
                layer.close(loading);
				if(res.code==1){
				  layer.msg(res.msg,{time:1000,icon:1});
				}else{
					layer.msg(res.msg,{time:1000,icon:2});
					return false;
				}
            })		  
		});
		
		table.on('tool(mlist)', function(obj){
            var data = obj.data;
			console.log(obj.event);
            switch(obj.event){
			  case 'del':
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/card/msDel'); ?>",{id:data.id,tab:'Gaoji'},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                });
			break;
			case 'success':
			     var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/gaoji/edit'); ?>",{id:data.id},function(res){
                        layer.close(loading);
						var icon=2;
                        if(res.code==1){
                            icon=1;
							tableIn.reload();
                        }
						layer.msg(res.msg,{time:1000,icon:icon});
                    });
			break;
			case 'error':
			    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/gaoji/edit'); ?>",{id:data.id,remarks:data.remarks},function(res){
                        layer.close(loading);
                        var icon=2;
                        if(res.code==1){
                            icon=1;
							tableIn.reload();
                        }
						layer.msg(res.msg,{time:1000,icon:icon});
                    });
			break;
            }
        });

      });
	  function tips(q,e){
		layer.tips("<img  src='"+q+"' style='width:240px;height:240px;'>", e, {
		  tips: [1, '#3595CC'],
		  time: 4000
		});
		}
    </script>
</html>