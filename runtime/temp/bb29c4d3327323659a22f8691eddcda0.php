<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:75:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\member\index.html";i:1638015884;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <title>用户中心 - <?php echo C('title'); ?></title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/index.css?v=004" rel="stylesheet">
   
</head>

<body class="fixed-sidebar full-height-layout gray-bg fixed-nav w_warp">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side">
        <div class="sidebar-collapse">
            <ul class="nav pt20" id="side-menu">

                <li class="active">
                    <a class="J_menuItem menuItem" href="<?php echo U('home/member/maina'); ?>">
                        <i class="ico24 ico_account_view"></i>
                        <span class="nav-label">账户总览</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem menuItem" href="<?php echo U('home/member/moneylog'); ?>">
                        <i class="ico24 ico_account_finance"></i>
                        <span class="nav-label">账户流水</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem menuItem" href="<?php echo U('home/member/tixian'); ?>">
                        <i class="ico24 ico_draw"></i>
                        <span class="nav-label">我要提现</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem menuItem" href="<?php echo U('home/member/recharge'); ?>">
                        <i class="ico24 ico_draw_list"></i>
                        <span class="nav-label">提现记录</span>
                    </a>
                </li>
                <!--<li>-->
                <!--    <a class="J_menuItem menuItem" href="<?php echo U('home/member/sellCard'); ?>">-->
                <!--        <i class="ico24 ico_sell_card"></i>-->
                <!--        <span class="nav-label">我要卖卡</span>-->
                <!--    </a>-->
                <!--</li>-->
                <!--<li>
                    <a class="J_menuItem menuItem" href="http://www.renrenxiaoka.com/member/sellCardRecord/index.do">
                        <i class="ico24 ico_sell_list"></i>
                        <span class="nav-label">卖卡记录</span>
                    </a>
                </li>-->
                <li>
                    <a class="menuItem" href="#">
                        <i class="ico24 ico_sell_list"></i>
                        <span class="nav-label">卖卡记录</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a class="J_menuItem" href="<?php echo U('home/order/batchCardIndex'); ?>">批量订单</a></li>
                        <li><a class="J_menuItem" href="<?php echo U('home/order/singleCardIndex'); ?>">单卡订单</a></li>
                    </ul>
                </li>

                <!--<li>-->
                <!--    <a class="J_menuItem menuItem" href="<?php echo U('home/daili/daili'); ?>">-->
                <!--        <i class="ico24 ico_invite_manage"></i>-->
                <!--        <span class="nav-label">邀请管理</span>-->
                <!--    </a>-->
                <!--</li>-->
                <li>
                    <a class="J_menuItem menuItem" href="<?php echo U('home/member/userinfo'); ?>">
                        <i class="ico24 ico_data_manage"></i>
                        <span class="nav-label">资料管理</span>
                    </a>
                </li>

                <!--<a class="contact_qq" href="http://wpa.qq.com/msgrd?v=3&amp;uin=4008889259&amp;site=qq&amp;menu=yes" target="_blank">
                    <i class="ico_qq"></i>
                </a>
                <div class="contact_qq">
                    <script charset="utf-8" type="text/javascript" src="http://wpa.b.qq.com/cgi/wpa.php?key=XzkzODE3Nzg5NV80ODI3OTNfNDAwODg4OTI1OV8"></script>
                </div>-->


                <div class="contact_phone">
                    <h5>客服电话</h5>
                    <p><?php echo C('phone'); ?></p>
                    <small>（周一至周日 8:00-24:00）</small>
                </div>

            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->

    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <!--固定头部-->
            <nav class="navbar navbar-fixed-top">
                <div class="head-top clearfix">
                    <div class="pull-left"><span class="welcome">欢迎来到<?php echo C('title'); ?></span></div>
                    <ul class="navbar-nav navbar-customer clearfix">
                        <li class="navbar-item navbar-user"><!-- 显示信息，加open类-->
                            <a class="dropdown-toggle navbar-link" data-toggle="dropdown" href="javascript:void(0);">
							<?php if(empty($u['qq']) || (($u['qq'] instanceof \think\Collection || $u['qq'] instanceof \think\Paginator ) && $u['qq']->isEmpty())): ?><img class="userimg" width="28" height="28" src="/static/home/images/heard.png" class="login-img" alt="" /><?php else: ?><img src="http://q4.qlogo.cn/headimg_dl?dst_uin=<?php echo $u['qq']; ?>&spec=100" class="userimg" width="28" height="28" /><?php endif; ?>
                                
                                <span class="username">
                                                                            <?php echo empty($u['user'])?$u['photo']:$u['user']; ?>
                                                                    </span>
                                <s class="caret"></s>
                            </a>
                            <div class="dropdown-menu">
                                <div class="navbar-account">
                                    <div class="navbar-account-name">
                                        <a class="J_menuItem" href="<?php echo U('home/member/index'); ?>">
										<?php if(empty($u['qq']) || (($u['qq'] instanceof \think\Collection || $u['qq'] instanceof \think\Paginator ) && $u['qq']->isEmpty())): ?><img class="avatar" width="64" height="64" src="/static/home/images/heard.png" class="login-img" alt="" /><?php else: ?><img class="avatar" width="64" height="64" src="http://q4.qlogo.cn/headimg_dl?dst_uin=<?php echo $u['qq']; ?>&spec=100"></a><?php endif; ?>
                                        <h4 class="ellipsis">
                                            <a href="<?php echo U('home/member/index'); ?>" class="J_menuItem text-blue username">
                                                                                                    <?php echo empty($u['user'])?$u['photo']:$u['user']; ?>
                                                                                            </a>
                                        </h4>
                                        <p class="h6 pt5">尊敬的<?php echo C('title'); ?>用户，<br>欢迎回来</p>
                                    </div>
                                    <div class="navbar-account-nav">
                                        <ul class="clearfix">
                                            <li class="b-r-1"><a class="J_menuItem color666" href="<?php echo U('home/member/sellCard'); ?>">我要卖卡</a></li>
                                            <li><a class="J_menuItem color666" href="<?php echo U('home/member/batchCardIndex'); ?>">卖卡记录</a></li>
                                        </ul>
                                        <p>
                                            <a class="J_menuItem" href="<?php echo U('home/member/userinfo'); ?>">资料管理</a>
                                            <span class="sep">|</span>
                                            <a class="J_menuItem" href="<?php echo U('home/member/tixian'); ?>">我要提现</a>
                                            <span class="sep">|</span>
                                            <a href="javascript:void(0)" onclick="loginout()">退出</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="navbar-divider"></li>
                        <li class="navbar-item navbar-login-out">
                            <a class="navbar-link" href="javascript:void(0)" onclick="loginout()">注销</a>
                        </li>
                        <li class="navbar-divider"></li>
                        <li class="navbar-item navbar-help"><!-- 显示信息，加open类-->
                            <a class="dropdown-toggle navbar-link" data-toggle="dropdown" href="javascript:void(0);">
                                <b class="ico14 ico_help"></b><span class="m-l-md color80">帮助</span><s class="caret"></s>
                            </a>
                            <div class="dropdown-menu">
                                <p class="link">
                                    <a href="<?php echo U('home/index/help'); ?>" target="_blank">帮助中心</a>
                                    <em class="sep">|</em>
                                    <a href="<?php echo U('home/index/help',['type'=>52]); ?>" target="_blank">卖卡流程</a>
                                </p>
                                <p class="tel">
                                    <span class="color666">或直接拨打客服热线</span><br>
                                    <span class="text-red"><?php echo C('phone'); ?></span><br>
                                    <small class="text-light">（周一至周日 8:00-24:00）</small>
                                </p>
                                <p class="link"><a class="text-blue" href="http://wpa.qq.com/msgrd?v=3&amp;uin=<?php echo C('kefu'); ?>&amp;site=qq&amp;menu=yes" target="_blank">联系在线客服</a></p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="head-logo">
                    <a href="/"><img src="<?php echo C('logo'); ?>" width="230px" height="65px" alt="logo"></a>
                </div>
                <div class="head-nav clearfix">
                    <div class="all-type pull-left" id="allRecycleType">
                        <div class="all-type-title">
                            <i class="ico24 ico-all"></i>
                            <span>全部回收类型</span>
                        </div>
                        <div class="all-type-sort">
                            <ul>
                                                                <li>
                                    <a href="<?php echo U('home/index/recycle',['type'=>0]); ?>">
                                                                                <i class="list-icon-comm icon-iphone"></i>
                                                                                <i class="right-arrow"></i>
                                        影视会员
                                    </a>
                                </li>
                                                                <li>
                                    <a href="<?php echo U('home/index/recycle',['type'=>1]); ?>">
                                                                                <i class="list-icon-comm icon-game"></i>
                                                                                <i class="right-arrow"></i>
                                        音乐读书
                                    </a>
                                </li>
                                                                <li>
                                    <a href="<?php echo U('home/index/recycle',['type'=>3]); ?>">
                                                                                <i class="list-icon-comm icon-refueling-card"></i>
                                                                                <i class="right-arrow"></i>
                                        美食出行
                                    </a>
                                </li>
                                                                <li>
                                    <a href="<?php echo U('home/index/recycle',['type'=>4]); ?>">
                                                                                <i class="list-icon-comm icon-ecommerce-card"></i>
                                                                                <i class="right-arrow"></i>
                                        联通卡密
                                    </a>
                                </li>
                                                                <li>
                                    <a href="<?php echo U('home/index/recycle',['type'=>5]); ?>">
                                        										<i class="list-icon-comm icon-video"></i>
										                                        <i class="right-arrow"></i>
                                        其他卡类
                                    </a>
                                </li>
                                <!--                                <li>-->
                                <!--    <a href="<?php echo U('home/index/recycle',['type'=>6]); ?>">-->
                                <!--        										<i class="list-icon-comm icon-ebusiness"></i>-->
                                <!--                                                <i class="right-arrow"></i>-->
                                <!--        电商卡回收-->
                                <!--    </a>-->
                                <!--</li>-->
                                
                               
                            </ul>
                        </div>
                    </div>
                    <div class="web-static-nav pull-left">
                        <ul class="m-b-none clearfix">
                            <li><!--class="active"-->
                                <a href="/">首页</a>
                            </li>
                            <li>
                                <a href="/recycle.html">我要卖卡</a>
                            </li>
                            <li>
                                <!--<a href="<?php echo U('home/index/inviteFriends'); ?>" target="_blank">邀请有礼</a>-->
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="row J_mainContent" id="content-main">
            <!--<div class="spiner-example"style="position:absolute;top:40%;left:55%;height:0;padding:0;z-index:0" >
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
            </div>-->

            <iframe id="main_frame" class="J_iframe" name="iframe0" width="100%" height="100%" src="<?php echo $url; ?>" frameborder="0" data-id="<?php echo U('home/member/maina'); ?>" seamless></iframe>

        </div>

        <!--底部-->
        <div class="footer">
            <div class="text-center">
                <p class="line">
                                                                    <a href="<?php echo U('home/index/help',['type'=>50]); ?>" target="_blank">关于我们</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>51]); ?>" target="_blank">联系我们</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>52]); ?>" target="_blank">交易方式</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>53]); ?>" target="_blank">卖卡流程</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>54]); ?>" target="_blank">商户API</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>55]); ?>" target="_blank">隐私政策</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>56]); ?>" target="_blank">注册协议</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>57]); ?>" target="_blank">行业资讯</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>58]); ?>" target="_blank">商家合作</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>59]); ?>" target="_blank">网站公告</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>50]); ?>" target="_blank">常见问题</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>57]); ?>" target="_blank">免责声明</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>58]); ?>" target="_blank">转让协议</a><span class="sep">|</span>																								<a href="<?php echo U('home/index/help',['type'=>59]); ?>" target="_blank">礼品卡回收说明</a>                                                            </p>
                <p><?php echo C('coltd'); ?> </p>
            </div>
        </div>
    </div>
    <!--右侧部分结束-->

</div>

<script>
    var ctxPath = {domain: "http://127.0.0.10",_csrf:"8a7cab01-e4ec-472b-ae81-3663ed64aa23",staticDomain:"https://renrenxiaoka-resource.oss-cn-shenzhen.aliyuncs.com"};
</script>
<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/jquery.metisMenu.js"></script>
<script src="/static/home/js/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/common/jquery.cookie.js"></script>

<!-- 自定义js -->
<script src="/static/home/js/hplus.js?v=4.1.0"></script>
<script src="/static/home/js/common/contabs.js"></script>
<script src="/static/home/js/index.js"></script>
<script src="/static/home/js/headNotice.js?v=1204"></script>
<script src="/static/home/js/common/layer/layer.js"></script>
<script>
 function loginout(){
     var index = layer.load(0, {shade: false});
     $.post('<?php echo U('home/login/loginout'); ?>',{},function(e){
	   layer.close(index);
	   layer.alert(e.msg, {
		  skin: 'layui-layer-molv' //样式类名
		  ,closeBtn: 0
		}, function(){
		   if(e.code==1){
		   location.href="/";
		   }
		});
	   
	 })
 }
</script>
</body>
</html>
