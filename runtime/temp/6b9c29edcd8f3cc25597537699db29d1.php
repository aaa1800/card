<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:86:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\index\mobile\my_message.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/myMessage/myMessage.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">我的消息</a>
        <a class="return" href="/">
        <img src="/static/home/images/common/return_home_icon.png" style="width: 25px;height: 25px;top:10px;position: absolute;right: 15px;">
    </a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="myMessage-wrap w-wrap mt-44">
            <ul class="list-unstyled">
                <li class="bg-white">
                    <a class="media" href="<?php echo U('home/gong/index'); ?>">
                        <img class="w80 align-self-center" alt="40x40" src="/static/home/images/myMessage/news_notice_icon.png">
                        <div class="media-body">
                            <h5 class="text-truncate">平台公告</h5>
                        </div>
                        <img class="right-arrow align-self-center" alt="10x14" src="/static/home/images/common/gray_details_btn.png">
                    </a>
                </li>
                <li class="bg-white">
                    <a class="media" href="<?php echo U('home/index/help'); ?>">
                        <img class="w80 align-self-center" alt="40x40" src="/static/home/images/myMessage/news_notice_icon.png">
                        <div class="media-body">
                            <h5 class="text-truncate">帮助中心</h5>
                        </div>
                        <img class="right-arrow align-self-center" alt="10x14" src="/static/home/images/common/gray_details_btn.png">
                    </a>
                </li>
            </ul>
        </main>
    </div>
</div>

<!--返回顶部按钮-->
<a href="javascript:void(0);" class="totop" id="totop"></a>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-3.2.1.slim.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.bundle.min.js"></script><!--包含popper-->
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>

<!--自定义-->
<script src="/static/home/js/mobile/common/common.js"></script>
<script src="/static/home/js/mobile/myMessage/myMessage.js"></script>
</body>
</html>