<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:92:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\editemail.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/loginBind.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">邮箱</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="w-wrap">
<?php if(empty($u['email']) || (($u['email'] instanceof \think\Collection || $u['email'] instanceof \think\Paginator ) && $u['email']->isEmpty())): ?>
   <div class="mailSetting-warp">
        <div class="setting-modules">
            <!--没有绑定邮箱，直接绑定邮箱账号-->
            <form action="http://api.renrenxiaoka.com/m/member/profileManage/mailSetting/vertifyBindMailCode.do" method="post" id="bindMailForm">
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/common/form/login_pho_icon.png">
                    <div class="media-body no-bg">
                        <p class="d-flex clearfix">
                            <span class="form-group mr-0">
                                <input type="text" value="" name="mail" class="form-control pr-5" placeholder="请输入要绑定的邮箱" id="mail">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                        </p>
                    </div>
                </div>
                
                <!--                 <div class="media">
                    <img class="align-self-center" src="/static/home/images/common/form/login_pass_icon.png">
                    <div class="media-body no-bg">
                        <p class="d-flex clearfix">
                            <span class="form-group mr-0">
                                <input type="password" value="" name="password" class="form-control pr-5" placeholder="请输入6-20位包含数字字母的登录密码" id="password">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                        </p>
                    </div>
                </div> -->
                                
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/common/form/login_veri_icon.png">
                    <div class="media-body no-bg">
                        <div class="d-flex clearfix">
                            <span class="form-group">
                                <input type="text" value="" name="code" class="form-control" placeholder="请输入邮箱验证码" id="mailCode">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                            <a class="resend-code color-green" href="javascript:void(0);" id="getNewMailCode">发送邮箱验证码</a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_csrf" value="7bf10423-7740-4db0-99a4-a07ca22fcd1f" />
            </form>
        </div>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="submitBindMailFormBtn">立即绑定</a>
    </div>
<?php else: ?>
    <div class="mailSetting-warp">
        <div class="setting-modules">
            <form action="" method="post" id="modifyMailVerifyCodeForm">

                <div class="media" id="verifyWayMedia">
                    <img class="align-self-center" src="/static/home/images/common/form/login_pho_icon.png">

                    <input type="hidden" value="mail" id="selectedVerifyWay"><!--默认邮箱验证 -->
                    <div class="media-body">
                        <!--不管有没有绑定手机，都默认邮箱验证 -->
                        <p class="d-flex clearfix">
                            <span class="left-txt">通过已绑定邮箱认证</span>
                            <span class="right-txt"><?php echo tfen($u['email'],3,4); ?></span>
                        </p>
                    </div>
                </div>
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/common/form/login_veri_icon.png">
                    <div class="media-body no-bg">
                        <!--
					                        不管有没有绑定手机，都默认邮箱验证，隐藏手机验证码
					                        只有用户选择手机验证的时候才显示手机验证码
                        -->
                        <div class="d-flex clearfix" id="mailCodeBox">
                            <span class="form-group">
                                <input type="text" value="" class="form-control" placeholder="请输入邮箱验证码" id="mailCode">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                            <a class="resend-code color-green" href="javascript:void(0);" id="getMailCode">发送验证码</a>
                        </div>

                        <div class="d-flex clearfix d-none" id="phoneCodeBox">
                            <span class="form-group">
                                <input type="text" value="" class="form-control" placeholder="请输入手机验证码" id="phoneCode">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                            <a class="resend-code color-green" href="javascript:void(0);" id="getPhoneCode">发送验证码</a>
                        </div>

                    </div>
                </div>
            </form>
        </div>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="submitModifyMailVerifyCodeBtn">下一步</a>
    </div>
<?php endif; ?>
</main>

<input type="hidden" value="<?php echo tfen($u['photo'],3,4); ?>" id="phone"><!--用户绑定的手机号-->
<input type="hidden" value="<?php echo tfen($u['email'],3,4); ?>" id="mail"><!--用户绑定的邮箱号-->

<script>
    var ctxPath = "<?php echo U('home/api/jiaoyimima'); ?>",editTran="<?php echo U('home/member/editTran'); ?>",precode="<?php echo U('home/api/preCode'); ?>",curll="<?php echo U('home/member/addemail'); ?>",sendEmail="<?php echo U('home/api/sendEmail'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/mailSetting/updateMailInit.js"></script>
</body>
</html>