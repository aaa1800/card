<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:96:"C:\wwwroot\8.210.135.237\public/../application/home\view\order\get_sell_card_orders_details.html";i:1605265943;s:65:"C:\wwwroot\8.210.135.237\application\Home\view\layout\member.html";i:1605265943;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 首页内容(账户总览)</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
 <link href="/static/home/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
	<link href="/static/home/css/common/detail.css" rel="stylesheet">
    <link href="/static/home/js/common/layer/skin/layer.css" rel="stylesheet"/>
	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="white-bg sell-card-detail">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-caption">
                <a class="pull-right text-blue return" href="javascript:void(0);" id="returnSellCardRecord">返回</a>
                <h2>单号：<?php echo $order; ?><span class="sep">|</span><span class="text-muted">提交时间：<?php echo $time; ?></span></h2>
            </div>

            <div class="alert alert-info">
                此次交易共<span class="text-orange"><?php echo $d['count']; ?> 张</span>卡，交易成功：<?php echo $d['ok']; ?>张，
                失败 <span class="text-red"><?php echo $d['no']; ?> 张</span>，您共获得： <span class="text-orange"><?php echo $d['money']; ?>元</span>，
                款项已经打入您的账户余额中，你可以到账户总览查看。
            </div>

            <!--表格-->
            <table id="dataTable" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>卡种</th>
                        <th>卡号</th>
                        <th>卡密</th>
                        <th>归属地</th>
                        <th>提交面值</th>
                        <th>实际面值</th>
                        <th>实际可得</th>
                        <th>状态</th>
                        <th>备注</th>
                        <th>提交时间</th>
                        <th>处理时间</th>
                    </tr>
                </thead>
                <tbody class="dataWraper">
                                    
					<?php if(is_array($res) || $res instanceof \think\Collection || $res instanceof \think\Paginator): if( count($res)==0 ) : echo "" ;else: foreach($res as $key=>$p): ?>
						<tr>
                        <td><?php echo $p['operatorName']; ?></td>                       
                        <td class="copy-btn" title="<?php echo $p['card_no']; ?>（点击可复制）" data-clipboard-text="<?php echo $p['card_no']; ?>"><?php echo $p['card_no']; ?></td>
                        <td class="copy-btn" title="<?php echo $p['card_key']; ?>（点击可复制）" data-clipboard-text="<?php echo $p['card_key']; ?>"><?php echo $p['card_key']; ?></td>
                        <td><?php echo $p['area']; ?></td>
                        <td><span class="text-orange">￥<?php echo $p['money']; ?></span></td>
                        <td><span class="text-orange">￥<?php echo $p['settle_amt']; ?></span></td>
                        <td><span class="text-dark-green">￥<?php echo $p['amount']; ?></span></td>
                        <td><?php echo $p['sta']; ?></td>
                        <td><span class="text-red"><?php echo $p['remarks']; ?></span></td>
                        <td><?php echo $p['create_time']; ?></td>
                        <td><?php if($p['state'] > '0'): ?><?php echo $p['update_time']; else: ?>--<?php endif; ?></td>
                    </tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
                                </tbody>

            </table>
			<input type="hidden" id="orderId" value="<?php echo $order; ?>" />
            <input type="hidden" id="orderType" value="1" />
                        <input type="hidden" id="canBeChangeSubmitType" value="true" />
            <a class="btn btn-blue btn-sm sell-card" href="javascript:void(0);" id="resubmit"><i class="submit-card-ico"></i>一键提交失败卡密</a>
                       	            <a class="btn btn-blue btn-sm sell-card" href="javascript:void(0);" id="gotoSellCardPage"><i class="sell-card-ico"></i>我要卖卡</a>
        </div>
    </div>

</div>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="/static/home/js/content.js?v=1.0.0"></script>

<!-- Data Tables -->
<script src="/static/home/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="/static/home/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="/static/home/js/common/layer/layer.js"></script>
<script type="text/javascript" src="/static/home/js/common/clipboard.min.js"></script>

<script src="/static/home/js/sellCardRecord/sellCardDetail.js?v=13"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script>
var tiji="<?php echo U('home/api/retijiao'); ?>";
    $(function(){

        function closeCurFrameFn(){
            var curHref = window.location.href;
            closeFrame(curHref);
        }

        //移除卖卡记录详情页面，显示卖卡记录页面  注意顺序   先显示再关闭
        $("#returnSellCardRecord").click(function () {
            $('#side-menu .J_menuItem',parent.document).each(function () {
                var curTabHref = $(this).attr("href");
                if ($(this).parents('li').hasClass("active")) {
                    showFrame(curTabHref);
                }
            });
            // var sellCardRecordHref = ctxPath.domain+'/member/sellCardRecord/index.do';
            // showFrame(sellCardRecordHref);

            closeCurFrameFn();
        });

        //我要卖卡
        $("#gotoSellCardPage").click(function () {
            var sellCardHref = "<?php echo U('home/member/sellCard'); ?>";
            addFrame(sellCardHref, '3', '我要卖卡');

            closeCurFrameFn();
        });
        
        var clipboard = new ClipboardJS('.copy-btn');
        clipboard.on('success', function(e) {           
            layer.msg("复制成功！", {icon : 1,shade : [ 0.4, '#000' ],time : 2000});
            e.clearSelection();
        });

        clipboard.on('error', function(e) {
            layer.msg("复制失败可能浏览器内核不支持，请手动复制", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        });

    });
</script>

</body>
</html>
