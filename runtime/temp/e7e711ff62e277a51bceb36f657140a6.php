<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:86:"C:\wwwroot\yhs.haochiyidian.top\public/../application/home\view\member\get_detail.html";i:1616590069;s:72:"C:\wwwroot\yhs.haochiyidian.top\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 提现详情</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
    <link href="/static/home/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12 horizontal-center-box text-center">
            <div class="panel panel-default">
                <div class="panel-heading text-center">提现详情</div>
                <div class="panel-body">
                    <div class="row">
                        <!--左边-->
                        <div class="col-sm-6 content-part">
                            <div class="row">
                                <div class="col-sm-6 text-right">提现单号：</div>
                                <div class="col-sm-6 text-left"><?php echo $da['order']; ?></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-right">姓名：</div>
                                <div class="col-sm-6 text-left"><?php echo $da['username']; ?></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-right">提现金额：</div>
                                <div class="col-sm-6 text-left">￥<?php echo $da['money']; ?></div>
                            </div>
                                                        <div class="row">
                                <div class="col-sm-6 text-right">处理时间：</div>
                                <div class="col-sm-6 text-left text-warning"><?php echo $da['shtime']; ?></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-right">提交IP：</div>
                                <div class="col-sm-6 text-left text-warning">/</div>
                            </div>
                        </div>

                        <!--右边-->
                        <div class="col-sm-6 content-part">
                            <div class="row">
                                <div class="col-sm-6 text-right">提现方式：</div>
                                <div class="col-sm-6 text-left">
                                                                        <?php echo $da['type']; ?>
                                                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-right">
                                                                        <?php echo $da['zhname']; ?>
                                                                    </div>
                                <div class="col-sm-6 text-left">

                                                                        <?php echo $da['zh']; ?>
                                                                    </div>
                            </div>
                                                        <div class="row">
                                <div class="col-sm-6 text-right">申请时间：</div>
                                <div class="col-sm-6 text-left"><?php echo $da['addtime']; ?></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-right">状态：</div>
                                <div class="col-sm-6 text-left"><?php switch($da['status']): case "1": ?>提现成功<?php break; case "2": ?>退款<?php break; default: ?>
								 审核中
								<?php endswitch; ?>
                                                                    </div>
                            </div>

                            
                        </div>
                    </div>
                    <div class="row m-t-xl">
                        <div class="col-sm-12">
                            <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="backToWithdrawCashRecord">返回</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="/static/home/js/content.js?v=1.0.0"></script>

<script src="/static/home/js/common/conttab_child.js"></script>
<script>
    $(function(){
        //移除提现详情页面，显示提现记录页面  注意顺序   先显示再关闭
        $("#backToWithdrawCashRecord").click(function () {
            $('#side-menu .J_menuItem',parent.document).each(function () {
                var curTabHref = $(this).attr("href");
                if ($(this).parents('li').hasClass("active")) {
                    showFrame(curTabHref);
                }
            });

            var curHref = window.location.href;
            closeFrame(curHref);
        });
    });
</script>


</body>
</html>
