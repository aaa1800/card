<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:84:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\card\m_verify.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
	  <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">通道管理</a>
                <a>
                    <cite>卡类列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
								<div class="layui-inline layui-show-xs-block">
										<button class="layui-btn" type="button"  onclick="xadmin.open('新增卡类','<?php echo U('simple/card/cardAdd'); ?>')" ><i class="layui-icon"></i>新增卡类</button> 
										<button class="layui-btn layui-btn-default" type="button"  id="upAll"><i class="layui-icon layui-icon-refresh"></i>一键更新个人费率</button>
										<button class="layui-btn layui-btn-danger" id="delAll"><i class="layui-icon"></i>批量删除</button>
										
                                </div>
                        </div>
						 
                        <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
	<script type="text/html" id="status">
    <input type="checkbox" name="state" value="{{d.id}}" lay-skin="switch" lay-text="开启|关闭" lay-filter="state" {{ d.start == 1 ? 'checked' : '' }}>
    </script>
	
	<script type="text/html" id="icon">
		<img src="{{d.iconUrl}}">
   </script>
   <script type="text/html" id="icon2">
		<img style="width:50%;height:150%" src="{{d.phoneRecycleIcon}}">
   </script>
	<script type="text/html" id="caoz">
		<div class="layui-btn-group">
		<button class="layui-btn" onclick="xadmin.open('编辑','<?php echo U('simple/card/msgEdit'); ?>?id={{d.id}}')" >编辑</button>
		<button class="layui-btn layui-btn-danger"  lay-event="del">删除</button>
		</div>
   </script>
    <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "<?php echo U('simple/card/mVerify'); ?>",
            method: 'post',
			page:true,
			totalRow:true,
            cols: [[
			    {type: 'checkbox',width:30,minWidth:30},
				{field:'id',title:"ID", align: 'center', sort: true,width:50},
				{field:'name',title:"卡名称", align: 'center'},
				{field:'class',title: '类型',align: 'center'},
			    {field:'type',title: '通道代码',align: 'center'},
				{field:'payrate',title: '合作费率',align: 'center'},
				{field:'sysrate',title: '系统费率',align: 'center'},
				{field:'oper',title: '运营商',align: 'center',width:100},
				{field:'amt',title: '卡密规则',align: 'center',width:200},
				{field:'rule',title: '卡面值',align: 'center',width:200},
				{field:'iconUrl',title: '图标1',align: 'center',toolbar: '#icon',width:80},
				{field:'phoneRecycleIcon',title:'图标2', align: 'center',toolbar: '#icon2',width:150},
				{field:'start',title: '状态',align: 'center',toolbar: '#status',width:100},
				{ align: 'center',title:'操作',toolbar: '#caoz'}
            ]],
			limit:15,
			limits:[10,20,50,60]
        });		
		table.on('tool(mlist)', function(obj){
            var data = obj.data;
			console.log(obj.event);
			switch(obj.event){
               case 'del':
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/card/msDel'); ?>",{id:data.id,tab:'Channel',ziduan:data.fiel},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
			  break;
			  return false;
            }
        });
		form.on('switch(state)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = this.value;
            var authopen = obj.elem.checked===true?1:0;
			var str="开启";
			 if(authopen==1){
			    str="关闭";
			 }
				$.post('<?php echo U("simple/card/opencard"); ?>',{'id':id,'start':authopen},function (res) {
					layer.close(loading);
					if (res.code==1) {
						tableIn.reload();
					}else{
						layer.msg(res.msg,{time:1000,icon:2});
						return false;
					}
				})
        });
		$("#delAll").click(function(){
            layer.confirm('确认要删除选中信息吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('mlist'); //test即为参数id设定的值
				console.log(checkStatus,table);
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo U('simple/member/mxDel'); ?>", {ids: ids}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        });
		$("#upAll").click(function(){
            layer.confirm('确认要更新费率到个人吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('mlist'); //test即为参数id设定的值
				console.log(checkStatus,table);
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo U('simple/member/upall'); ?>", {ids: ids}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        });
		
      });
    </script>
</html>