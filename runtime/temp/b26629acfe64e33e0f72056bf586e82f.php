<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:85:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\member\mobile\moneylog.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/dropload/dropload.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/tab-list.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="<?php echo U('home/member/index'); ?>">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">账户流水</a>
    <a href="javascript:void(0);"></a>
</header>
<ul class="nav nav-pills fixed-top justify-content-around top44" id="accountFinance-record-tab">
    <li class="nav-item">
        <a class="nav-link active" href="javascript:void(0);">全部</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">销卡</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">提现</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">佣金</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">加/扣款</a>
    </li>
</ul>

<!--中间内容部分-->
<main class="account-fanace-wrap w-wrap mt87">    
    <div class="tab-content">   
        <ul class="data-list"></ul>
        <ul class="data-list d-none"></ul>
        <ul class="data-list d-none"></ul>
        <ul class="data-list d-none"></ul>
        <ul class="data-list d-none"></ul>
    </div>
</main>

<!--返回顶部按钮-->
<a href="javascript:void(0);" class="totop" id="totop"></a>

<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
	var moneylog="<?php echo U('home/moneylog/mgetDa'); ?>",geren="<?php echo U('home/member/index'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/dropload/dropload.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/common.js"></script>
<script src="/static/home/js/mobile/accountFinance/index.js?v=25"></script>
</body>
</html>