<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:92:"C:\wwwroot\shouka.paimaivip.net\public/../application/simple\view\member\member_yongjin.html";i:1616590069;s:73:"C:\wwwroot\shouka.paimaivip.net\application\Simple\view\public\heard.html";i:1616590069;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
	  <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">会员中心</a>
                <a>
                    <cite>佣金列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
					
                        <div class="layui-card-body ">
						<form class="layui-form layui-col-space5" >
								<div class="layui-inline layui-show-xs-block">
										<button class="layui-btn layui-btn-danger" id="money"> 总佣金:￥<?php echo $money; ?> </button><button class="layui-btn layui-btn-danger" id="delAll"><i class="layui-icon"></i>批量删除</button>
                                </div>
								<div class="layui-inline layui-show-xs-block">
								 <select name="type" id="selt" lay-skin="select">
									 <option value="uid">UID</option>
									 <option value="price">成功金额</option>
								 </select>
                                    </div>
									<div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="username" placeholder="搜索内容" autocomplete="off" class="layui-input"></div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn" type="button" lay-submit="" lay-filter="sreach" lay-skin="select" lay-filter="type">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
								</form>
                        </div>
						 
                        <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
	<script type="text/html" id="caoz">
		<div class="layui-btn-group">
		<button class="layui-btn layui-btn-danger"  lay-event="del">删除</button>
		</div>
   </script>
    <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "<?php echo U('simple/member/memberYongjin'); ?>",
            method: 'post',
			page:true,
			totalRow:true,
            cols: [[
			    {type: 'checkbox',width:60,minWidth:30},
				{field:'id',title:"ID", align: 'center', sort: true,width:80},
				{field:'uid',title:"用户ID", align: 'center',totalRowText: '合计：'},
			    {field:'type',title: '类型',align: 'center'},
				{field:'price',title: '金额',align: 'center',totalRow:true},
				{field:'money',title: '佣金',align: 'center',totalRow:true},
				{field:'user',title: '下级',align: 'center'},
				{field:'data',title: '说明',align: 'center'},
				{field:'addtime', align: 'center',title:'时间'},
				{align:'center',title:'操作',toolbar: '#caoz'}
            ]],
			limit:15,
			limits:[10,20,50,60]
        });
        form.on('submit(sreach)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var name=$("input[name=username]").val();
			var kk=$("#selt").val();
			var index = layer.load(3, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
					});
                 tableIn.reload({
					page:{curr:1
						 },
                     where:{
						 name:name,
						 kk:kk
                     }
                 });
				 layer.close(index);
        });		
		table.on('tool(mlist)', function(obj){
            var data = obj.data;
			console.log(obj.event);
			switch(obj.event){
               case 'del':
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/member/txDel'); ?>",{id:data.id,tab:'Viporder'},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
			  break;
			  return false;
            }
        });
		
		$("#delAll").click(function(){
            layer.confirm('确认要删除选中信息吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('mlist'); //test即为参数id设定的值
				console.log(checkStatus,table);
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo U('simple/member/txDel'); ?>", {ids: ids,tab:'Viporder'}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        });
		
      });
    </script>
</html>