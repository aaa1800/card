<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:82:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\webjs\delet.html";i:1616590070;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
	 <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">系统设置</a>
            <a>
              <cite>数据删除</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
		<form class="layui-form" action="">
          <fieldset class="layui-elem-field">
			  <legend>删除数据</legend>
			  <div class="layui-field-box">
				 <div class="layui-form-item">
					<label class="layui-form-label" style="width:120px">清除会员数据：</label>
					<div class="layui-input-block">
					  <input type="radio" name="tian" value="60" title="60天未登陆">
						<input type="radio" name="tian" value="90" title="90天未登陆" >
						<input type="radio" name="tian" value="180" title="180天未登陆" checked><button type="button" class="layui-btn" lay-submit="" lay-filter="shan">立即清除</button>
					</div>
				 </div>
				 <div class="layui-form-item">
					<label class="layui-form-label" style="width:120px">清除历史订单：</label>
					<div class="layui-input-block">
					  <input type="radio" name="titi" value="1" title="一个月前">
						<input type="radio" name="titi" value="2" title="2个月前" >
						<input type="radio" name="titi" value="6" title="6个月前" checked><button type="button" class="layui-btn" lay-submit="" lay-filter="del">立即清除</button>
					</div>
				 </div>
			  </div>
		  </fieldset>
		</form>
	</div>
</div>
	</div>
</div>
	</div>

    </body>
<script>

layui.use(['form'], function(){
  var form = layui.form
  ,layer = layui.layer;
 
  //监听提交
  form.on('submit(shan)', function(data){
   layer.confirm('您确定要删除会员记录吗？', function(index){
		var tian=$("input[name=tian]").val();
		var loading =layer.load(3, {shade: [0.1,'#fff']});
		$.post("<?php echo U('simple/webjs/delet'); ?>",{str:tian,type:"user"},function(res){
				layer.close(loading);
				if (res.code==1) {
					layer.msg(res.msg,{time:1000,icon:1});
				}else{
					layer.msg(res.msg,{time:1000,icon:2});
				}
		})
      return false;
	})
  });
   //监听提交
  form.on('submit(del)', function(data){
  layer.confirm('您确定要删除历史记录吗？', function(index){
    var tian=$("input[name=titi]").val();
    var loading =layer.load(3, {shade: [0.1,'#fff']});
    $.post("<?php echo U('simple/webjs/delet'); ?>",{str:tian,type:"data"},function(res){
	        layer.close(loading);
			if (res.code==1) {
				layer.msg(res.msg,{time:1000,icon:1});
			}else{
				layer.msg(res.msg,{time:1000,icon:2});
			}
	})
    return false;
	})
  });
  
 
 
  
});
</script>
</html>