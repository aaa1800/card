<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:92:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\sell_card.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css?v=20190703">
    <link rel="stylesheet" href="/static/home/css/mobile/recycle/recycle.css?v=20181224">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="<?php echo U('home/member/index'); ?>">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">我要卖卡</a>
    <a class="w44" href="<?php echo U('home/index/help'); ?>"><!--跳转到回收说明页面-->
        <img src="/static/home/images/recycle/sale_help_icon.png">
    </a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap t44-b47-space">

    <div class="minirefresh-scroll bg-transparent">
        <main class="recycle-wrap w-wrap">
            <form action="<?php echo U('home/api/batchCard'); ?>" method="post" id="cardForm">
                <div class="card-select">
                	<input type="hidden" id="submitPlatform" name="submitFrom" value="3" /><!-- 提交平台：1、PC端	3、手机端 -->
                    <!--卡类选择-->
                    <div class="media" id="cardTypeMedia">
                        <input type="hidden" id="productClassifyId" name="productClassifyId" value="<?php echo $type; ?>"><!--等同于链接中的productType参数-->

                        <label class="item-label">卡类：</label>
                        <div id="productClassifiesList" class="media-body">
                        </div>
                        <img class="right-arrow align-self-center" src="/static/home/images/common/gray_details_btn.png" />
                    </div>

                    <!--卡种选择 默认选择第一项-->
                    <div class="media" id="cardSortMedia">
                        <input type="hidden" id="operatorId" name="channelid" value="">
                        <input type="hidden" id="chooseOperatorId" name="chooseOperatorId" value=""><!--等同于链接中的operatorId参数-->

                        <label class="item-label">卡种：</label>
                        <div class="media-body">
                            <div class="clearfix card-sort-selected"></div>
                        </div>
                        <img class="right-arrow align-self-center" src="/static/home/images/common/gray_details_btn.png" />
                    </div>

                    <!--面值选择 默认不选择-->
                    <div class="media" id="cardFaceValueMedia">
                        <input type="hidden" id="price" name="money" value="">

                        <label class="item-label">面值：</label>
                        <div class="media-body">
                            <div class="clearfix card-faceValue-selected">
                                <div class="float-left" id="facevalue-selected">请选择面值</div>
                            </div>
                        </div>
                        <img class="right-arrow align-self-center" src="/static/home/images/common/gray_details_btn.png" />
                    </div>

                    <!--方式选择 默认批量提交-->
                    <!--<div class="media border-bottom-0" id="cardSubmitWayMedia">-->
                    <!--    <input type="hidden" id="type" name="type" value="2">提交方式：1、单卡提交  2、批量提交-->

                    <!--    <label class="item-label">方式：</label>-->
                    <!--    <div class="media-body">-->
                    <!--        <div class="clearfix card-submitWay-selected">-->
                    <!--            <div class="float-left" id="submitWay-selected">-->
                    <!--                <p submitWay="2">批量提交</p>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <img class="right-arrow align-self-center" src="/static/home/images/common/gray_details_btn.png" />-->
                    <!--</div>-->

                    <!--加油卡慢销才有的折扣输入框-->
                    <div class="media d-none" id="cardDiscountMedia">
                        <label class="item-label">折扣</label>
                        <div class="media-body">
                            <div class="clearfix">
                                <input type="text" name="discount" id="cardDiscount" class="inpt" placeholder="">
                            </div>
                        </div>
                        <img class="delete-ico align-self-center" src="/static/home/images/common/form/com_dele_icon.png" />
                        <img class="discount-tip-ico align-self-center" src="/static/home/images/recycle/sale_hint_icon.png" />
                    </div>

                </div>

                <!--消耗时长-->
                <p class="card-recycle-duration" id="recentConsumeDuration"></p>
                <p class="batch-consume-duration" id="bathConsumeDuration"></p>

                <div class="card-sample-box clearfix">
                	<!--<a class="float-left card-recovery-limit" id="todayLimit" href="javascript:void(0);"></a>-->
                	<a class="float-right color-blue" id="viewCardSample" href="javascript:void(0);">查看卡密示例</a>
               	</div>

                <!--批量提交输入框-->
                <div class="batch-mode" id="batchMode">
                    <div class="card-list-box">
                        <textarea class="form-control" name="tempcards" id="batchCardlist" cols="20" rows="5" placeholder="请粘贴充值卡卡号卡密，卡号与卡密间请用空格隔开"></textarea>
                        <input type="hidden" name="cards" id="cards"><!--最终传给后台的值-->
                    </div>

                    <div class="card-input-info clearfix">
                        <!--<a class="float-left btn btn-red tidy-btn" id="cardlist-tidy-btn" href="javascript:void(0);">整理卡号卡密</a>-->
                        <span class="">已经输入<strong id="cardnum" class="color-red">0</strong>张卡，可提交<strong id="maxSubmitCount">？</strong>张/每次</span>
                        <!--<span class="float-right">已经输入<strong id="cardnum" class="color-red">0</strong>张卡，可提交<strong id="maxSubmitCount">？</strong>张/每次</span>-->
                    </div>
                </div>
				 <!--智能提交输入框-->
                <div class="batch-mode" id="abatchMode" style="display:none">
                    <div class="card-list-box">
                        <textarea class="form-control" id="dbatchCardlist" cols="20" rows="12" placeholder="请粘贴充值卡卡号卡密，卡号与卡密间请用空格隔开"></textarea>
                    </div>

                    <div class="card-input-info clearfix">
                        <!--<a class="float-left btn btn-red tidy-btn" onclick="mach()" href="javascript:void(0);">整理卡号卡密</a>-->
                        <span class="float-right">已经输入<strong id="cardnumm" class="color-red">0</strong>张卡，可提交<strong id="maxSubmitCounta">？</strong>张/每次</span>
                    </div>
                </div>

                <!--单卡提交输入框-->
                <div class="single-mode" id="singleMode">
                    <div class="media">
                        <label class="item-label">卡号</label>
                        <div class="media-body">
                            <div class="clearfix">
                                <input type="text" name="cardNumber" id="cardNumber" class="inpt" placeholder="请输入充值卡卡号/账号">
                            </div>
                        </div>
                        <img class="delete-ico align-self-center" src="/static/home/images/common/form/com_dele_icon.png" />
                    </div>
                    <div class="media">
                        <label class="item-label">卡密</label>
                        <div class="media-body">
                            <div class="clearfix">
                                <input type="text" id="cardPass" class="inpt" placeholder="请输入充值卡卡密/登陆密码">
                                <input type="hidden" name="cardPassword" id="cardPassHid">
                            </div>
                        </div>
                        <img class="delete-ico align-self-center" src="/static/home/images/common/form/com_dele_icon.png" />
                    </div>
                </div>

                <!--卡密规则-->
                <div class="card-rules-tip" id="recycleRulesWrap">
                    【卡密规则】 <span id="cardRules"></span><span id="cardTips">，面值一定要选择正确！不能有误，提交大量错卡一律封号不予解封！</span>
                </div>
 
                <input type="hidden" id="isAgree1" name="isAgree1" value="1"><!--服务协议1（1：同意 0：不同意）-->
                <input type="hidden" id="isAgree2" name="isAgree2" value="1"><!--回收说明（1：同意 0：不同意）-->
                <input type="hidden" id="discountHidFromId" value="" />
				<input type="hidden" id="discountHidEndId" value="" />
				<input type="hidden" id="cardNumRule" value="" />
				<input type="hidden" id="cardPassRule" value="" />
            </form>
        </main>
    </div>
</div>

<!--固定底部-->
<div class="fixed-bottom recycle-footer">
    <dov class="row">
        <!--<div class="col-8 left-agreement clearfix">-->
        <!--    <label class="float-left isAgree"><em class="checkbox active" state="on"></em></label>-->
        <!--    <div class="float-left d-inline-block agree-txt" style="font-size: 1.2rem;">-->
        <!--        <a class="color-blue" href="<?php echo U('home/index/help',['type'=>58]); ?>">《转让协议》</a>-->
        <!--    </div>-->
        <!--</div>-->
        <div class="btn btn-blue right-commit-btn" id="sellSubmitBtn">确认提交</div>
    </dov>
</div>

<input type="hidden" value="true" id="isRealNameAuthen"><!--是否实名认证-->
<input type="hidden" value="" id="memberTodayLimit" /><!--用户今日限额-->
<input type="hidden" id="actualCardNum" /><!--实际提交的卡密张数-->

<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"",staticDomain:""};
	var tjurl="<?php echo U('home/api/apiti'); ?>";
	
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js?v=20190605"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/jquery.cookie.js?v=20190703"></script>
<script src="/static/home/js/mobile/headNotice.js?v=20190703"></script>
<!--自定义-->
<script src="/static/home/js/mobile/recycle/recycle-func.js?v=2019457"></script>
<script src="/static/home/js/mobile/recycle/recycle-data.js?v=20190"></script>
<script>
function mach(){
  $.post("<?php echo U('home/api/mach'); ?>",{card:$('#dbatchCardlist').val()},function(e){
       $("#cardnumm").text(e.num);
       $("#dbatchCardlist").val(e.msg);
  });
}
</script>
</body>
</html>