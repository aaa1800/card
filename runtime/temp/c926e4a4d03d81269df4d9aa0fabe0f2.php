<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:99:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\authen_init_qiye.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/Calendar/lCalendar.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/realNameAuthen/realNameAuthen.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/withdrawAccount/withdrawAccount.css">

</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">实名认证</a>
    <a class="w44" href="javascript:void(0);"></a>
</header>
<ul class="nav nav-pills fixed-top justify-content-around top44">
     <li class="nav-item">
        <a class="nav-link " href="<?php echo U('home/member/AuthenInit'); ?>">初级认证</a>
    </li>
	<li class="nav-item">
        <a class="nav-link " href="<?php echo U('home/member/AuthenInitGaoji'); ?>">高级认证</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="<?php echo U('home/member/AuthenInitQiye'); ?>">企业认证</a>
    </li>
</ul>
<!--中间内容部分-->

    <?php switch($ok): case "-1": ?>
	<main class="realName-wrap w-wrap top87">
    <p class="top-tip tip-ico-parent">您的信息仅在审核时作为<?php echo C('title'); ?>网实名认证依据</p>
    <div class="setting-modules">	
        <form method="post" id="mediumAuthenInitForm" enctype="multipart/form-data">
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">法人姓名</span>
                        <span class="right-txt"><?php echo $u['username']; ?></span>
                    </p>
                </div>
            </div>
            <div class="media">         
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">身份证号</span>
                        <span class="right-txt"><?php echo $u['idcard']; ?></span>                                        
                    </p>
                </div>
            </div>
			<div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">企业名称</span>
                        <input name="name" id="name" type="text" class="form-control bg-white" value="">
                    </p>
                </div>
            </div>
            <div class="media">         
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                <div class="media-body no-bg">
                	
                    <p class="d-flex clearfix">
                        <span class="left-txt">营业执照号</span>
                        <input name="number" id="number" type="text" class="form-control bg-white" value="">                                       
                    </p>
                </div>
            </div>
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_date_icon.png">
                <div class="media-body">
                    <p class="d-flex clearfix">
                        <span class="left-txt">证件颁发日期</span>
                        <span class="form-group right-txt">
                            <input name="idCardStartDate" type="text" class="form-control bg-white" placeholder="请选择日期" id="issuanceDate" readonly>
                        </span>
                    </p>
                </div>
            </div>
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_date_icon.png">
                <div class="media-body">
                    <p class="d-flex clearfix">
                        <span class="left-txt">证件到期日期</span>
                        <span class="form-group right-txt">
                            <input name="idCardEndDate" type="text" class="form-control bg-white" placeholder="请选择日期" id="expireDate" readonly>
                        </span>
                    </p>
                </div>
            </div>
            <div class="media" id="uploadPicMedia">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_photo_icon.png">
                <div class="media-body border-bottom-0">
                    <p class="d-flex clearfix">
                        <span class="left-txt">上传证件照片</span>
                        <span class="form-group right-txt">
                            <input type="text" class="form-control bg-white" placeholder="需证件照的所有文字清晰可见" id="cardPicInpt" readonly>
                        </span>
                    </p>
                </div>
            </div>
		
		
		<div class="upload-wrap d-none">
			<!-- 身份证正面照 -->    
			<div class="upload-item">
				<a class="upload-rules" href="<?php echo U('home/member/realhelp'); ?>">
			        <img src="/static/home/images/profileManage/realNameAuthen/new/sale_help_icon.png">
			    </a>
		    
		        <label for="upload_frontpic_btn" class="upload-input">
		            <input id="upload_frontpic_btn" type="file" name="image[]" accept="image/*" onchange="getImgVal(this);">
		        </label>
		
		        <!-- 上传前 -->
		        <img class="upload-tips" src="/static/home/images/profileManage/realNameAuthen/new/upload_front_bj.png">
		
		        <!-- 上传后的图片 -->
		        <div class="upload-sucimg-wrap">
		            <img src="" alt="">
		        </div>
		    </div>
		    
		    <!-- 身份证反面照 -->  
		    <div class="upload-item">
		        <label for="upload_backpic_btn" class="upload-input">
		            <input id="upload_backpic_btn" type="file" name="image[]" accept="image/*" onchange="getImgVal(this);">
		        </label>
		
		        <!-- 上传前 -->
		        <img class="upload-tips" src="/static/home/images/profileManage/realNameAuthen/new/upload_rear_bj.png">
		
		        <!-- 上传后的图片 -->
		        <div class="upload-sucimg-wrap">
		            <img src="" alt="">
		        </div>
		
		    </div>

			<!-- 手持身份证照 -->  
		    <div class="upload-item">
		        <label for="upload_handpic_btn" class="upload-input">
		            <input id="upload_handpic_btn" name="image[]" type="file" accept="image/*" onchange="getImgVal(this);">
		        </label>
		
		        <!-- 上传前 -->
		        <img class="upload-tips" src="/static/home/images/profileManage/realNameAuthen/new/upload_hand_bj.png">
		
		        <!-- 上传后的图片 -->
		        <div class="upload-sucimg-wrap">			           
		            <img src="" alt="">
		        </div>
		    </div>
   
	    </div>
		</form>
    </div>
	    	    
    <a href="javascript:void(0);" class="btn btn-blue full-btn" id="submitMediumAuthenInitFormBtn">立即认证</a>
	</main>
    <?php break; case "0": ?>
	<main class="w-wrap">

	 <div class="null_data">
        <i class="com_wait_icon"></i>
        <p class="color-blue">正在加急审核中，最迟24小时内返回结果！</p>
        <a href="<?php echo U('home/member/index'); ?>" class="btn btn-blue btn-sm">返回个人中心</a>
        
               <!--  <p class="identificate_tip">完成认证级别越高，每天可以兑换的额度越高，完成中级认证，可用额度为200000元 </p> -->
            </div>
    </main>
	<?php break; case "2": ?>
	<main class="realName-wrap w-wrap top87">
    <p class="top-tip tip-ico-parent"><?php echo $da['remarks']; ?></p>
	  <div class="setting-modules">	
        <form method="post" id="mediumAuthenInitForm" enctype="multipart/form-data">
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">法人姓名</span>
                        <span class="right-txt"><?php echo $u['username']; ?></span>
                    </p>
                </div>
            </div>
            <div class="media">         
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                <div class="media-body no-bg">
                	<input name="idCard" type="hidden" value="<?php echo $u['idcard']; ?>">
                    <p class="d-flex clearfix">
                        <span class="left-txt">身份证号</span>
                        <span class="right-txt"><?php echo $u['idcard']; ?></span>                                        
                    </p>
                </div>
            </div>
			<div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">企业名称</span>
                        <input name="name" id="name" type="text" class="form-control bg-white" value="<?php echo $da['name']; ?>">
                    </p>
                </div>
            </div>
            <div class="media">         
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                <div class="media-body no-bg">
                	
                    <p class="d-flex clearfix">
                        <span class="left-txt">营业执照号</span>
                        <input name="number" id="number" type="text" class="form-control bg-white" value="<?php echo $da['number']; ?>">                                       
                    </p>
                </div>
            </div>
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_date_icon.png">
                <div class="media-body">
                    <p class="d-flex clearfix">
                        <span class="left-txt">证件颁发日期</span>
                        <span class="form-group right-txt">
                            <input name="idCardStartDate" value="<?php echo date('Y/m/d',strtotime($da['idCardStartDate'])); ?>" type="text" class="form-control bg-white" placeholder="请选择日期" id="issuanceDate" readonly>
                        </span>
                    </p>
                </div>
            </div>
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_date_icon.png">
                <div class="media-body">
                    <p class="d-flex clearfix">
                        <span class="left-txt">证件到期日期</span>
                        <span class="form-group right-txt">
                            <input name="idCardEndDate" type="text" value="<?php echo date('Y/m/d',strtotime($da['idCardStartDate'])); ?>" class="form-control bg-white" placeholder="请选择日期" id="expireDate" readonly>
                        </span>
                    </p>
                </div>
            </div>
            <div class="media" id="uploadPicMedia">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_photo_icon.png">
                <div class="media-body border-bottom-0">
                    <p class="d-flex clearfix">
                        <span class="left-txt">上传证件照片</span>
                        <span class="form-group right-txt">
                            <input type="text" class="form-control bg-white" placeholder="需证件照的所有文字清晰可见" id="cardPicInpt" readonly>
                        </span>
                    </p>
                </div>
            </div>
	
		
		<div class="upload-wrap d-none">
			<!-- 身份证正面照 -->    
			<div class="upload-item">
				<a class="upload-rules" href="<?php echo U('home/member/realhelp'); ?>">
			        <img src="/static/home/images/profileManage/realNameAuthen/new/sale_help_icon.png">
			    </a>
		    
		        <label for="upload_frontpic_btn" class="upload-input">
		            <input id="upload_frontpic_btn" type="file" name="image[]" accept="image/*" onchange="getImgVal(this);">
		        </label>
		
		        <!-- 上传前 -->
		        <img class="upload-tips" src="/static/home/images/profileManage/realNameAuthen/new/upload_front_bj.png">
		
		        <!-- 上传后的图片 -->
		        <div class="upload-sucimg-wrap">
		            <img src="" alt="">
		        </div>
		    </div>
		    
		    <!-- 身份证反面照 -->  
		    <div class="upload-item">
		        <label for="upload_backpic_btn" class="upload-input">
		            <input id="upload_backpic_btn" type="file" name="image[]" accept="image/*" onchange="getImgVal(this);">
		        </label>
		
		        <!-- 上传前 -->
		        <img class="upload-tips" src="/static/home/images/profileManage/realNameAuthen/new/upload_rear_bj.png">
		
		        <!-- 上传后的图片 -->
		        <div class="upload-sucimg-wrap">
		            <img src="" alt="">
		        </div>
		
		    </div>

			<!-- 手持身份证照 -->  
		    <div class="upload-item">
		        <label for="upload_handpic_btn" class="upload-input">
		            <input id="upload_handpic_btn" name="image[]" type="file" accept="image/*" onchange="getImgVal(this);">
		        </label>
		
		        <!-- 上传前 -->
		        <img class="upload-tips" src="/static/home/images/profileManage/realNameAuthen/new/upload_hand_bj.png">
		
		        <!-- 上传后的图片 -->
		        <div class="upload-sucimg-wrap">			           
		            <img src="" alt="">
		        </div>
		    </div>
   
	    </div>
			</form>
    </div>	    
    <a href="javascript:void(0);" class="btn btn-blue full-btn" id="submitMediumAuthenInitFormBtn">重新认证</a>
	</main>
	<?php break; default: ?>
	<main class="realName-wrap w-wrap top87">
    <p class="top-tip tip-ico-parent">认证成功</p>
	  <div class="setting-modules">	
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">法人姓名</span>
                        <span class="right-txt"><?php echo $u['username']; ?></span>
                    </p>
                </div>
            </div>
            <div class="media">         
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                <div class="media-body no-bg">
                	<input name="idCard" type="hidden" value="<?php echo $u['idcard']; ?>">
                    <p class="d-flex clearfix">
                        <span class="left-txt">身份证号</span>
                        <span class="right-txt"><?php echo $u['idcard']; ?></span>                                        
                    </p>
                </div>
            </div>
			<div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_name_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt">企业名称</span>
						<span class="right-txt"><?php echo $da['name']; ?></span>  

                    </p>
                </div>
            </div>
            <div class="media">         
                <img class="w40 align-self-center" src="/static/home/images/profileManage/withdrawAccount/bankAccount/card_idcard_icon.png">
                <div class="media-body no-bg">
                	
                    <p class="d-flex clearfix">
                        <span class="left-txt">营业执照号</span><span class="right-txt"><?php echo $da['number']; ?></span>                                       
                    </p>
                </div>
            </div>
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_date_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt" style="width:4.5rem">证件颁发日期</span>
						<span class="right-txt"><?php echo date('Y/m/d',strtotime($da['idCardStartDate'])); ?></span>  

                    </p>
                </div>
            </div>
            <div class="media">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_date_icon.png">
                <div class="media-body no-bg">
                    <p class="d-flex clearfix">
                        <span class="left-txt" style="width:4.5rem">证件到期日期</span>
                        <span class="form-group right-txt">
                            <?php echo date('Y/m/d',strtotime($da['idCardEndDate'])); ?>
                        </span>
                    </p>
                </div>
            </div>
            <div class="media" id="uploadPicMedia">
                <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/new/onset_photo_icon.png">
                <div class="media-body border-bottom-0">
                    <p class="d-flex clearfix">
                        <span class="left-txt">证件照片</span>
                    </p>
                </div>
            </div>
		<div class="upload-wrap ">
			<!-- 身份证正面照 -->    
			<div class="upload-item">
		        <!-- 上传前 -->
		        <img class="upload-tips" src="<?php echo $da['idjust']; ?>">
		    </div>
		    
		    <!-- 身份证反面照 -->  
		    <div class="upload-item">
		        <img class="upload-tips" src="<?php echo $da['idback']; ?>">
		    </div>

			<!-- 手持身份证照 -->  
		    <div class="upload-item">
		        <img class="upload-tips" src="<?php echo $da['license']; ?>">
		    </div>
   
	    </div>
    </div>
	</main>
	<?php endswitch; ?>
        


<script>
    var ctxPath = "<?php echo U('home/member/Qiyeup'); ?>";
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/lCalendar/lCalendar.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/profileManage/realNameAuthen/photoCompress.js"></script>
<script src="/static/home/js/mobile/profileManage/realNameAuthen/mediumAuthenInit.js"></script>
<script>
 var ok="<?php echo $u['real']; ?>";
 if(ok==0 || ok=="" || ok==null){
    layer.open({
                content: '<p class="ico ico_right">请先实名认证</p>'
                ,shadeClose: false
                ,btn: ['我知道了']
                ,yes: function(index){
                    layer.close(index);
					location.href="<?php echo U('home/member/AuthenInit'); ?>";
                }
            });
 }
</script>
</body>
</html>