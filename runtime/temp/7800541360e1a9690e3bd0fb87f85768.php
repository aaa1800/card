<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:76:"C:\wwwroot\hs.qulaida.top\public/../application/simple\view\index\login.html";i:1638015884;}*/ ?>
<!doctype html>
<html  class="x-admin-sm">
<head>
	<meta charset="UTF-8">
	<title>后台登录-<?php echo $title; ?></title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="/static/simple/css/font.css">
    <link rel="stylesheet" href="/static/simple/css/login.css">
	  <link rel="stylesheet" href="/static/simple/css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up">
        <div class="message"><?php echo $title; ?>-管理登录</div>
        <div id="darkbannerwrap"></div>
        
        <form method="post" class="layui-form" >
            <input name="user" placeholder="用户名"  type="text" lay-verify="required" autocomplete="off" class="layui-input" >
            <hr class="hr15">
            <input name="password" lay-verify="required" placeholder="密码"  type="password"  class="layui-input">
            <hr class="hr15">
			<?php if(C('adminvi') == 'on'): ?>
			<input name="code" placeholder="验证码" autocomplete="off" type="text" class="layui-input" style="width:51%;float: left;margin-right: 2.5rem;"> <button style="width:40%;height:50px" type="button" lay-submit lay-filter="send" class="layui-btn layui-btn-lg layui-btn-normal">获取验证码</button><?php endif; ?>
            <hr class="hr15">
			<?php echo token(); ?>
            <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="button">
            <hr class="hr20" >
        </form>
    </div>

    <script>
        $(function  () {
            layui.use('form', function(){
              var form = layui.form,$ = layui.jquery;
			  form.on('submit(send)', function(data){
			      var index = layer.load(0, {shade: false});
			      $.post("<?php echo U('simple/index/sendEmail'); ?>",data.field,function(e){
				      let ni = e.code == 1 ? 1 : 2;
				      layer.close(index);
					  if(e.token){
					    $("input[name=__token__]").val(e.token);
					  }
					  layer.msg(e.msg, {
					  icon: ni,
					  time: 1000 //2秒关闭（如果不配置，默认是3秒）
					  });
				  });
                return false;
              });
              form.on('submit(login)', function(data){
			     var code=$("input[name=code]").val(),ok="<?php echo C('adminvi'); ?>";
				 if((code=="" || code==null) && ok=="on" ){
				     layer.msg("请输入验证码", {
					  icon: 2,
					  time: 1000 //2秒关闭（如果不配置，默认是3秒）
					  });
					  return false;
				 }
                 var index = layer.load(0, {shade: false});
			      $.post("<?php echo U('simple/index/login'); ?>",data.field,function(e){
				      let ni = e.code == 1 ? 1 : 2;
				      layer.close(index);
					  if(e.token){
					    $("input[name=__token__]").val(e.token);
					  }
					  layer.msg(e.msg, {
					  icon: ni,
					  time: 1000},function(){
					   if(ni==1)location.href="<?php echo U('simple/admin/index'); ?>";
					  });
				  });
                return false;
              });
            });
        })
    </script>
</body>
</html>