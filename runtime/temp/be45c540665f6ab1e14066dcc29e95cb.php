<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:83:"D:\wwwroot\127.0.0.1\public/../application/home\view\member\mobile\authen_init.html";i:1667656648;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/realNameAuthen/realNameAuthen.css">
	  <link rel="stylesheet" href="/static/home/css/mobile/common/form.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">实名认证</a>
    <a class="w44" href="javascript:void(0);"></a>
</header>
<ul class="nav nav-pills fixed-top justify-content-around top44">
    <li class="nav-item">
        <a class="nav-link active" href="<?php echo U('home/member/AuthenInit'); ?>">初级认证</a>
    </li>
	<li class="nav-item">
        <a class="nav-link " href="<?php echo U('home/member/AuthenInitGaoji'); ?>">高级认证</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="<?php echo U('home/member/AuthenInitQiye'); ?>">企业认证</a>
    </li>
</ul>
<!--中间内容部分-->

<?php if($u['real'] > '0'): ?>
<main class="realName-wrap w-wrap top87">

<img class="success_pic" src="/static/home/images/profileManage/realNameAuthen/new/primary_approve_icon.png" alt="初级认证成功">

  <div class="setting-modules">
        <a class="media" href="javascript:void(0);">
            <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/fill_name_icon.png">
            <div class="media-body no-bg">
                <p class="clearfix">
                    <span class="float-left left-txt color3">真实姓名</span>
                    <span class="float-right right-txt"><?php echo $u['username']; ?></span>
                </p>
            </div>
        </a>
        <a class="media" href="javascript:void(0);">
            <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/fill_idcard_icon.png">
            <div class="media-body no-bg">
                <p class="clearfix">
                    <span class="float-left left-txt color3">身份证号</span>
                    <span class="float-right right-txt"><?php echo tfen($u['idcard'],3,6); ?></span>
                </p>
            </div>
        </a>
        <a class="media" href="javascript:void(0);">
            <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/fill_login_icon.png">
            <div class="media-body no-bg">
                <p class="clearfix">
                    <span class="float-left left-txt color3">认证状态</span>
                    <span class="float-right right-txt color-green">已初级认证</span>
                </p>
            </div>
        </a>
        <a class="media" href="javascript:void(0);">
            <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/fill_day_icon.png">
            <div class="media-body no-bg">
                <p class="clearfix">
                    <span class="float-left left-txt color3">认证日期</span>
                    <span class="float-right right-txt"><?php echo $u['realtime']; ?></span>
                </p>
            </div>
        </a>
        <a class="media" href="javascript:void(0);">
            <img class="w40 align-self-center" src="/static/home/images/profileManage/realNameAuthen/fill_way_icon.png">
            <div class="media-body no-bg">
                <p class="clearfix">
                    <span class="float-left left-txt color3">认证渠道</span>
                    <span class="float-right right-txt"><?php echo C('title'); ?>网实名认证</span>
                </p>
            </div>
        </a>
    </div>
</main>
<?php else: ?>
<main class="w-wrap">
    <main class="realName-wrap w-wrap top87">
    <p class="top-tip tip-ico-parent">提现也必须是这个身份证所开的银行卡、微信、支付宝，请注意填写</p>
    <div class="form-pannel t44">
        <form method="post" id="realNameForm">
            <div class="form-group">
                <i class="name"></i>
                <input type="text" name="name" class="form-control" id="name" placeholder="与身份证一致的真实姓名">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>

            <div class="form-group">
                <i class="idCard"></i>
                <input type="text" name="idCard" class="form-control" id="idCard" placeholder="身份证号">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>
        </form>

        <a href="javascript:void(0);" class="btn btn-blue full-btn mt-43 mb-0" id="submitRealNameFormBtn">立即提交</a>
		
		<!--        <p class="identificate_tip">完成认证级别越高，每天可以兑换的额度越高，完成身份证认证，可用额度为100元 </p>
        -->
    </div>
</main>
	<?php endif; ?>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/realNameAuthen/alreadyBindPhoneRealNameAuthenInit.js"></script>
<script>var ren="<?php echo U('home/member/AuthenInit'); ?>";</script>
</body>
</html>