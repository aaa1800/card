<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:72:"D:\wwwroot\127.0.0.1\public/../application/simple\view\system\index.html";i:1667656648;s:62:"D:\wwwroot\127.0.0.1\application\Simple\view\public\heard.html";i:1667656648;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
	 <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">系统设置</a>
            <a>
              <cite>网站设置</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
		<form class="layui-form" action="">
		  <div class="layui-form-item">
			<label class="layui-form-label">网站名称：</label>
			<div class="layui-input-block">
			  <input type="text" name="title" style="width:50%" value="<?php echo C('title'); ?>" lay-verify="" autocomplete="off" placeholder="请输入标题" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">关键字：</label>
			<div class="layui-input-block">
			  <input type="text" name="keywords" style="width:50%" value="<?php echo C('keywords'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		   <div class="layui-form-item">
			<label class="layui-form-label">网站描述：</label>
			<div class="layui-input-block">
			  <input type="text" name="description" style="width:50%" value="<?php echo C('description'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">客服QQ：</label>
			<div class="layui-input-block">
			  <input type="text" name="kefu" style="width:50%" value="<?php echo C('kefu'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">客服电话：</label>
			<div class="layui-input-block">
			  <input type="text" name="phone" style="width:50%" value="<?php echo C('phone'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">自定义网站：</label>
			<div class="layui-input-block">
			  <input type="text" name="djkj" style="width:50%" value="<?php echo C('djkj'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		 
		  <div class="layui-form-item">
			<label class="layui-form-label">网站LOGO 160*50：</label>
			<div class="layui-input-block">
			  <img id="logo" src="<?php echo C('logo'); ?>" width="200" height="200">
			  <input type="hidden" name="logo" value="<?php echo C('logo'); ?>">
			  <button type="button"   class="layui-btn" id="test1">
				  <i class="layui-icon">&#xe67c;</i>上传图片
				</button>
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">微信公二维码140*140：</label>
			<div class="layui-input-block">
			  <img id="wxgzhewm" src="<?php echo C('wxgzhewm'); ?>" width="200" height="200">
			  <input type="hidden" name="wxgzhewm" value="<?php echo C('wxgzhewm'); ?>">
			  <button type="button"   class="layui-btn" id="test2">
				  <i class="layui-icon">&#xe67c;</i>上传图片
				</button>
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">密钥（可不填）：</label>
			<div class="layui-input-block">
			  <input type="text" name="dxkey" style="width:50%" value="<?php echo C('dxkey'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			  </div>
			<div class="layui-form-mid layui-word-aux">短信申请地址：https://www.smsbao.com/</div>
		  </div>
		  <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>通用短信</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="bin" ><?php echo C('bin'); ?></textarea>
						</div>
          </div>
		   <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>注册短信</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="reg" ><?php echo C('reg'); ?></textarea>
						</div>
          </div>
		  

		  <div class="layui-form-item">
			<label class="layui-form-label">代理几级分佣</label>
			<div class="layui-input-inline">
			  <input type="text" name="dlnum" lay-verify="required" value="<?php echo C('dlnum'); ?>" autocomplete="off" class="layui-input">
			</div>
			<div class="layui-form-mid layui-word-aux">代理可以获得下线几级的佣金</div>
			<div class="layui-inline">
			  <label class="layui-form-label">佣金比率</label>
			  <div class="layui-input-inline">
				<input type="tel" name="dlbl" lay-verify="required" value="<?php echo C('dlbl'); ?>" autocomplete="off" class="layui-input">
			  </div>
			</div>
			
		  </div>
		  
		  
		  <div class="layui-form-item">
			<label class="layui-form-label">开放注册：</label>
			<div class="layui-input-block">
			  <input type="checkbox" <?php if(C('kfreg') == 'on'): ?>checked=""<?php endif; ?> name="kfreg" lay-skin="switch" lay-text="ON|OFF">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">网站开关：</label>
			<div class="layui-input-block">
			  <input type="checkbox" <?php if(C('webopen') == 'on'): ?>checked=""<?php endif; ?> name="webopen"   lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">银行卡提现开关：</label>
			<div class="layui-input-block">
			  <input type="checkbox" <?php if(C('kbank') == 'on'): ?>checked=""<?php endif; ?> name="kbank" lay-skin="switch" lay-text="ON|OFF">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">微信提现开关：</label>
			<div class="layui-input-block">
			  <input type="checkbox" <?php if(C('kweixin') == 'on'): ?>checked=""<?php endif; ?> name="kweixin" lay-skin="switch" lay-text="ON|OFF">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">支付宝提现开关：</label>
			<div class="layui-input-block">
			  <input type="checkbox" <?php if(C('kali') == 'on'): ?>checked=""<?php endif; ?> name="kali" lay-skin="switch" lay-text="ON|OFF">
			</div>
		  </div>
		   <div class="layui-form-item">
			<label class="layui-form-label">后台登陆验证：</label>
			<div class="layui-input-block">
			  <input type="checkbox" <?php if(C('adminvi') == 'on'): ?>checked=""<?php endif; ?> name="adminvi" lay-skin="switch" lay-text="ON|OFF">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">备案信息：</label>
			<div class="layui-input-block">
			  <input type="text" name="beian" style="width:50%" value="<?php echo C('beian'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">版权信息：</label>
			<div class="layui-input-block">
			  <input type="text" name="coltd" style="width:50%" value="<?php echo C('coltd'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item layui-form-text">
			<label class="layui-form-label">关闭网站提示：</label>
			<div class="layui-input-block">
			  <textarea placeholder="请输入内容" name="wclose" class="layui-textarea"><?php echo C('wclose'); ?> </textarea>
			</div>
		  </div>
	
		  <div class="layui-form-item">
			<div class="layui-input-block">
			  <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
			  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		  </div>
		</form>
	</div>
</div>
	</div>
</div>
	</div>

    </body>
<script>
layui.use('upload', function(){
  var upload = layui.upload,layer = layui.layer;
  //执行实例
  var uploadInst = upload.render({
     elem: '#test1' //绑定元素
    ,url: '<?php echo U("simple/system/upload"); ?>' //上传接口
    ,done: function(res){
		 if(res.code==1){
		    $("#logo").attr('src',res.url);
			$("input[name=logo]").val(res.url);
			layer.msg(res.info,{time:1000,icon:1});
		 }else{
		    layer.msg(res.info,{time:1000,icon:2});
		 }
    }
    ,error: function(){
      //请求异常回调
    }
  });
});
layui.use('upload', function(){
  var upload = layui.upload,layer = layui.layer;
  //执行实例
  var uploadInst = upload.render({
     elem: '#test2' //绑定元素
    ,url: '<?php echo U("simple/system/upload"); ?>' //上传接口
    ,done: function(res){
		 if(res.code==1){
		    $("#wxgzhewm").attr('src',res.url);
			$("input[name=wxgzhewm]").val(res.url);
			layer.msg(res.info,{time:1000,icon:1});
		 }else{
		    layer.msg(res.info,{time:1000,icon:2});
		 }
    }
    ,error: function(){
      //请求异常回调
    }
  });
});
layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
   
  //自定义验证规则
  form.verify({
    title: function(value){
      if(value.length < 5){
        return '标题至少得5个字符啊';
      }
    }
    ,content: function(value){
      layedit.sync(editIndex);
    }
  });
  
  
  //监听提交
  form.on('submit(demo1)', function(data){
     var loading =layer.load(3, {shade: [0.1,'#fff']});
    $.post("<?php echo U('simple/system/index'); ?>",data.field,function(res){
	        layer.close(loading);
			if (res.code==1) {
				layer.msg(res.msg,{time:1000,icon:1});
			}else{
				layer.msg(res.msg,{time:1000,icon:2});
			}
	})
    return false;
  });  
});
</script>
</html>