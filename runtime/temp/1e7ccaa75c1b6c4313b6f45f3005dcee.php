<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:76:"D:\wwwroot\127.0.0.1\public/../application/home\view\login\mobile\index.html";i:1667656648;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/form.css">
	
</head>
<body class="bg-gray" onload="dealJump();">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="/">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">登录</a>
    <a href="javascript:void(0);"></a>
</header>
<!--中间内容部分-->
<main class="login-wrap w-wrap">
	<ul class="nav nav-pills fixed-top justify-content-around" id="login-pannel-tab">
    	        <li class="nav-item">
            <a class="nav-link active" href="javascript:void(0);" otarget="account-login-pannel" style="color: #6c757d;">账号密码登录</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="javascript:void(0);" otarget="phone-login-pannel" style="color: #6c757d;">手机验证登录</a>
        </li>
                
            </ul>

    <div class="tab-content form-pannel">
        <!--销卡账号登录-->
        <form class="tab-pane fade show active" action="/" method="post" id="account-login-pannel">
        	<input type="hidden" name="type" id="loginPwdType"></input>
        	
            <div class="form-group">
                <i class="user"></i>
                <input type="text" value="" name="username" class="form-control" id="userName" placeholder="手机号/邮箱" maxlength="50">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>
            <div class="form-group">
                <i class="lock"></i>
                <input type="password" name="password" class="form-control" id="password" placeholder="登录密码" maxlength="20">
                <a class="right37 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="right isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
            </div>
            <div style="display:none"><input type="checkbox" name="remember-me" checked="checked"/></div>
            <input type="hidden" id="platform" name="platform" value="2"/>
			<?php echo token(); ?>
        </form>

        <!--手机验证登录-->
        <form class="tab-pane fade" action="/" method="post" id="phone-login-pannel">
        	<input type="hidden" name="type" id="loginPhoType"></input>
       	
            <div class="form-group">
                <i class="phone"></i>
                <input type="text" value="" name="phone" class="form-control" id="phone" placeholder="手机号" maxlength="11">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>
            <div class="form-group">
                <i class="shield"></i>
                <input type="text" name="code" class="form-control code" id="code" placeholder="手机验证码" maxlength="4">
                <a class="right104 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="resend-code color-green" href="javascript:void(0);" id="getPhoneCode">获取验证码</a>
            </div>
            <div style="display:none"><input type="checkbox" name="remember-me" checked="checked"/></div>
            <input type="hidden" id="platform" name="platform" value="2"/>
			<?php echo token(); ?>
        </form>

        <div class="clearfix">
            <a class="float-left forgot-password" href="<?php echo U('home/login/registered'); ?>" style="margin: .25rem .75rem .25rem .75rem;">注册新用户</a>
            <a class="float-right forgot-password" href="<?php echo U('home/login/retrieve'); ?>">忘记密码？</a>
        </div>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="login_btn">登录</a>
        <!--<a href="<?php echo U('home/login/registered'); ?>" class="btn btn-orange full-btn">注册新用户</a>-->

        <!--<div class="login_way">
            <p></p><span>第三方登录</span>
        </div>
         <div class="row justify-content-center third-login">
            <a class="col-3" href="http://api.renrenxiaoka.com/m/passport/weixin/login.do">
                <span class="w80 weixinIcon"></span>
            </a>
            <a class="col-3" href="http://api.renrenxiaoka.com/m/passport/qq/login.do">
                <span class="w80 qqIcon"></span>
            </a>
        </div> -->
    </div>
    
    <input type="hidden" id="loginType" name="type" value="1" /><!-- 登录类型：1、账号密码登录   2、手机验证码登录 3、手机号没有注册，直接登录-->
</main>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:"",error:""};
	var member="<?php echo U('home/member/index'); ?>",regurl="<?php echo U('home/api/sendsms'); ?>";
	var loginType="1";
	if(loginType=="2")
	{
		$("#loginType").val(loginType);
	}
</script>

<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/login/login.js"></script>


</body>
</html>