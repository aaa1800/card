<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:88:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\index.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css?v=20190703">
    <link rel="stylesheet" href="/static/home/css/mobile/index.css?v=20190703">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="fixed-top w-wrap">
    <div class="member-info-box">
        <div class="navbar">
            <a class="w44" href="<?php echo U('home/member/setting'); ?>">
                <img src="/static/home/images/index/system_set_icon.png">
            </a>
            <a class="w44" href="<?php echo U('home/index/myMessage'); ?>">
                <img src="/static/home/images/index/system_news_icon.png">
            </a>
        </div>

        <a class="media member-info" href="<?php echo U('home/member/info'); ?>" id="memberInfo">
            <img class="align-self-top rounded-circle user-photo" src="/static/home/images/index/heard.png">
            <div class="media-body">
                <h5 class="user-nickname"></h5>              
                <p class="user-number text-truncate"></p>
                <!--<p class="user-level level-ico"></p>-->
                <p style="font-size: 13px;color: #f8f9fa;">欢迎登陆<?php echo C('title'); ?></p>
            </div>
            <span class="align-self-top edit">编辑</span>
            <img class="right-arrow align-self-top" src="/static/home/images/common/recycle_det_btn.png">
        </a>

        <!--<a href="<?php echo U('home/index/inviteFriends'); ?>" class="level-rules-entry">邀请级别规则>></a>-->
    </div>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="member-center-warp w-wrap">
            <!--账户余额-->
            <div class="account-balance-box">
                <p class="account-balance-tit">账户余额</p>
                <p class="account-balance-con clearfix">
                    <span class="balance" id="balance"></span>
                    <a href="<?php echo U('home/member/AuthenInit'); ?>" class="btn btn-sm" id="realNameAuth"></a>                    
                </p>
            </div>

            <div class="member-modules-box">
                <a class="media modules-item" href="<?php echo U('home/member/Moneylog'); ?>">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_deta.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">账户流水</span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <a class="media modules-item" href="javascript:gotoWithdrawCash();">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_draw.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">我要提现</span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <a class="media modules-item" href="<?php echo U('home/member/Recharge'); ?>">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_record.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">提现记录</span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <a class="media modules-item" href="<?php echo U('home/order/batchCardIndex'); ?>">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_card.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">单卡订单</span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <a class="media modules-item" href="<?php echo U('home/order/singleCardIndex'); ?>">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_card.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">批量订单</span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <!--<a class="media modules-item" href="<?php echo U('home/daili/daili'); ?>">-->
                <!--    <img class="w44 align-self-center" src="/static/home/images/index/list_menu_card.png">-->
                <!--    <div class="media-body">-->
                <!--        <p class="clearfix">-->
                <!--            <span class="float-left left-txt">邀请管理</span>-->
                <!--        </p>-->
                <!--    </div>-->
                <!--    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">-->
                <!--</a>-->
                <a class="media modules-item" href="<?php echo U('home/member/info'); ?>">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_menu_infor.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">资料管理</span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <a class="media modules-item" href="tel:<?php echo C('phone'); ?>">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_phone.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">客服电话</span>
                            <span class="float-right right-txt" id="customerPhone"></span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </a>
                <!--<div class="media modules-item" id="copy_qq_btn">
                    <img class="w44 align-self-center" src="/static/home/images/index/list_mune_qq.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">微信公众号客服</span>
                            <span class="float-right right-txt" id="clientqq"></span>
                        </p>
                    </div>
                    <img class="right-arrow align-self-center" src="/static/home/images/index/sale_details_btn.png">
                </div>   -->             
            </div>
        </main>
    </div>
</div>

<div class="myfooter" id="myfooter">
	<div class="tabs floorbar">
		<a class="tab-item" href="/">
			<span class="icon icon-sell">
				<img class="menu_img" src="/static1/home/images/mobile/index/sy.png">
			</span>
			<span>首页</span></a>
		<a class="tab-item " href="/sell.html">
			<span class="icon icon-sell">
				<img class="menu_img" src="/static1/home/images/mobile/index/mk.png">
			</span>
		<span>卖卡</span></a>
		<a class="tab-item " href="<?php echo C('djkj'); ?>">
			<span class="icon icon-sell">
				<img class="menu_img" src="/static1/home/images/mobile/index/mk1.png">
			</span>
			<span>买卡</span></a>
		<a class="tab-item active" href="<?php echo U('home/member/index'); ?>">
			<span class="icon icon-sell">
				<img class="menu_img" src="/static1/home/images/mobile/index/wd1.png">
		</span>
		<span>我的</span></a>
	</div>
</div>
<style>
.myheader, .myfooter, .myaction, .myfixed {
    position: fixed;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 15;
    overflow: hidden;
    border-radius: .5rem .5rem 0 0;
    background-color: #fff;
    box-shadow: 0 0 0.1rem rgba(44,44,44,.1);
}	
.floorbar {
    position: relative;
    width: 100%;
    height: 49px;
    background-color: #fff;
}
.floorbar > .tab-item.active {
    color: #007dfe;
}
.floorbar > .tab-item {
    z-index: 10;
    height: 49px;
    line-height: 1;
    font-size: 10px;
    color: #666;
}

.tabs > .tab-item {
    position: relative;
    display: block;
    text-align: center;
    -webkit-box-flex: 1;
    -webkit-flex: 1;
    flex: 1;
}
.floorbar > .tab-item > .icon {
    display: block;
    height: 28px;
    line-height: 28px;
    font-size: 28px;
    margin-top: 2px;
    margin-bottom: 1px;
}
.tabs {
    position: relative;
    background-color: #fff;
    display: -webkit-box;
    display: -webkit-flex;
    /* display: flex; */
    -webkit-box-align: center;
    -webkit-align-items: center;
    /* align-items: center; */
}
</style>
<input type="hidden" value="" id="isRealNameAuthen"><!--是否实名认证-->
<input type="hidden" value="" id="isSetTradePass"><!--是否设置了交易密码-->
<script>
var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
var tx="<?php echo U('home/member/tixian'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<script src="/static/home/js/mobile/common/clipboard.min.js"></script>

<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/jquery.cookie.js?v=20190703"></script>
<script src="/static/home/js/mobile/index.js?v=23"></script>
<script src="/static/home/js/mobile/headNotice.js?v=20190703"></script>
</body>
</html>