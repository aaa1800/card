<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:73:"D:\wwwroot\127.0.0.1\public/../application/home\view\member\domember.html";i:1667656648;s:61:"D:\wwwroot\127.0.0.1\application\Home\view\layout\member.html";i:1667656648;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 首页内容(账户总览)</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/accountOverview/account_view.css" rel="stylesheet">
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg account-view-page">
	
    <div class="wrapper wrapper-content animated fadeInRight">
        <!--公告-->
        <div class="box notice">
            <i class="iconimg notice_iconimg"></i>
            <span class="pull-right"><a class="text-blue" href="<?php echo U('home/index/gongg'); ?>" target="_blank">查看更多&gt;&gt;</a></span>
            <span class="notice-time"><?php echo $news['addtime']; ?></span>
            <span class="sep">|</span>
            <span>
                <a class="notice-title text-red" href="<?php echo U('home/index/newsDetail',['id'=>$news['id']]); ?>" target="_blank">【<?php echo C('title'); ?>】<?php echo $news['title']; ?></a>：
                <span class="notice-text">
                   <?php echo strfen($news['contents'],8); ?>
                </span>
            </span>
        </div>
        <div class="box">
            <div class="row part-one">
                <div class="col-sm-4">
                    <div class="contact-box">
                        <div class="pull-left">
                            <div class="m-t-md m-r-md">
							<?php if(empty($u['qq']) || (($u['qq'] instanceof \think\Collection || $u['qq'] instanceof \think\Paginator ) && $u['qq']->isEmpty())): ?><img src="/static/home/images/heard.png" alt="用户头像" class="img-circle" /><?php else: ?> <img alt="用户头像" class="img-circle" src="http://q4.qlogo.cn/headimg_dl?dst_uin=<?php echo $u['qq']; ?>&spec=100" width="64px" height="64px"><?php endif; ?>
                            </div>
                        </div>

                        <div class="pull-left">
                            <h3 class="m-t-lg"><strong class="text-blue">
                                                                   <?php echo empty($u['user'])?$u['photo']:$u['user']; ?>
                                                            </strong></h3>
                            <p class="m-b-xs"><span>用户编号：</span><?php echo $u['id']; ?></p>
                            <div class="user-security">
                                <label>安全等级：</label>
                                <div class="user-security-progress">
                                    <div class="progress"><div class="progress-bar securelevel-<?php echo level($u['id']); ?>"></div></div>
                                </div>
                            </div>
							<p><span>推广员级别：</span>普通推广员</p>
							<a class="btn btn-w-80 btn-danger exit-btn" href="javascript:void(0)" onclick="loginout()">退出</a>
							<?php if(empty($u['jiaoyimima']) || (($u['jiaoyimima'] instanceof \think\Collection || $u['jiaoyimima'] instanceof \think\Paginator ) && $u['jiaoyimima']->isEmpty())): ?><a class="user-security-tiplink" href="javascript:gotoSetTradePassword();">完成设置交易密码 即可增强账户安全 点击去完成>> </a><?php endif; ?>
							<!-- <div class="user-security">
                                <label>API密钥：</label>
                                <div class="user-security-progress">
                                    <?php echo $u['key']; ?>
                                </div>
                            </div> -->
						</div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-box">
                        <div class="pull-left">
                            <div class="m-t-md m-r-md">
                                <img alt="image" class="img-circle" src="/static/home/images/accountOverview/money.png" width="64px" height="64px">
                            </div>
                        </div>

                        <div class="pull-left">
                            <p class="m-t-lg">账户余额（元）：</p>
                            <p class="m-b-none text-orange">￥<?php echo $u['money']; ?></p>
                            <a class="btn btn-w-80 btn-blue m-t-md" href="javascript:gotoAccountFinance();">查看明细</a>
							<!-- <a class="btn btn-w-80 btn-blue m-t-md" style="background:#f60;border:#f60" href="/zhifudemo.zip">下载API</a> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-box border-none">
                        <div class="pull-left">
                            <div class="m-t-md m-r-md">
                                <img alt="image" class="img-circle" src="/static/home/images/accountOverview/cash.png" width="64px" height="64px">
                            </div>
                        </div>

                        <div class="pull-left">
                            <p class="m-t-lg">提现处理中的金额：<a class="text-blue" href="javascript:gotoWithdrawCashRecord();">提现记录&gt;&gt;</a></p>
                            <p class="m-b-none text-green">￥<?php echo $u['price']; ?></p>
                            <a class="btn btn-w-80 btn-blue m-t-md m-l-md" href="javascript:gotoWithdrawCash();">我要提现</a>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>
            <ul class="security-group clearfix">
                <!--未绑定手机-->
				<?php if(empty($u['photo']) || (($u['photo'] instanceof \think\Collection || $u['photo'] instanceof \think\Paginator ) && $u['photo']->isEmpty())): ?><li class="security-item">
                    <div class="security-item-bg phone">
                        <i class="security-item-icon icon-phone"></i>
                        <h5>绑定手机可增强账户安全</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindPhone();"><i class="ico20 ico-phone"></i>绑定手机</a>
                    </div>
                </li><?php else: ?>
                                <!--已绑定手机-->
                      <li class="security-item has-bind">
                    <div class="security-item-bg phone">
                        <i class="security-item-icon icon-phone"></i>
                        <h5><?php echo tfen($u['photo'],3,4); ?></h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindPhone();"><i class="ico20 ico-phone"></i>更换手机</a>
                    </div>
                </li><?php endif; ?>
                <!--未实名认证-->
				<?php if($u['real'] > '0'): ?>
				<li class="security-item has-bind">
                    <div class="security-item-bg idcard">
                        <i class="security-item-icon icon-idcard"></i>
                        <h5>该账号已实名认证</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoRealName();"><i class="ico20 ico-idcard"></i>已实名认证</a>
                    </div>
                </li>
                
				<?php else: ?>
                   <li class="security-item">
                    <div class="security-item-bg">
                        <i class="security-item-icon icon-idcard"></i>
                        <h5>实名认证后才可提现</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoRealName();"><i class="ico20 ico-idcard"></i>实名认证</a>
                    </div>
                </li>
				<?php endif; ?>
                
                <!--登录密码和交易密码都没有设置 ,点击设置密码进入登录密码设置页面-->
                                <!--登录密码已设置，交易密码没有设置，点击设置密码进入交易密码管理页面-->
					<?php if(empty($u['password']) || (($u['password'] instanceof \think\Collection || $u['password'] instanceof \think\Paginator ) && $u['password']->isEmpty())): ?>
                    <li class="security-item">
                    <div class="security-item-bg lock">
                        <i class="security-item-icon icon-lock"></i>
                        <h5>交易密码未设置</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoSetTradePassword();"><i class="ico20 ico-lock"></i>设置密码</a>
                    </div>
                </li>
				<?php else: ?>
				   <li class="security-item no-set">
                    <div class="security-item-bg lock">
                        <i class="security-item-icon icon-lock"></i>
                        <h5>密码设置完成</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoSetLoginPassword();"><i class="ico20 ico-lock"></i>修改密码</a>
                    </div>
                </li>
				<?php endif; ?>
                                <!--交易密码已设置，登录密码没有设置，点击设置密码进入登录密码设置页面-->
                                <!--登录密码和交易密码都已设置-->
                
                <!--未绑定邮箱-->
				<?php if(empty($u['email']) || (($u['email'] instanceof \think\Collection || $u['email'] instanceof \think\Paginator ) && $u['email']->isEmpty())): ?>
                  <li class="security-item">
                    <div class="security-item-bg">
                        <i class="security-item-icon icon-envelope"></i>
                        <h5>绑定邮箱可增强账户安全</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindMail();"><i class="ico20 ico-envelope"></i>绑定邮箱</a>
                    </div>
                </li>
				<?php else: ?>
				  <li class="security-item has-bind">
                    <div class="security-item-bg envelope">
                        <i class="security-item-icon icon-envelope"></i>
                        <h5><?php echo tfen($u['email'],3,4); ?></h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindMail();"><i class="ico20 ico-envelope"></i>更换邮箱</a>
                    </div>
                </li>
				<?php endif; ?>
                
                                <!--未绑定微信-->
               <!-- <li class="security-item">
                    <div class="security-item-bg">
                        <i class="security-item-icon icon-wx"></i>
                        <h5>绑定微信后可使用微信登录</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindWeiXin();"><i class="ico20 ico-wx"></i>绑定微信</a>
                    </div>
                </li>-->
                
                <!--未绑定QQ-->
                                <!--已绑定QQ-->
                <!--<li class="security-item has-bind">
                    <div class="security-item-bg qq">
                        <i class="security-item-icon icon-qq"></i>
                        <h5>该账号已绑定QQ</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindQQ();"><i class="ico20 ico-qq"></i>更改绑定</a>
                    </div>
                </li>-->
                
                                <!--未绑定微信-->
                <!--<li class="security-item">
                    <div class="security-item-bg">
                        <i class="security-item-icon icon-code"></i>
                        <h5>请先绑定微信才可关注</h5>
                    </div>
                    <div class="security-item-action">
                        <a class="btn btn-primary" href="javascript:gotoBindWeiXin();"><i class="ico20 ico-wx"></i>绑定微信</a>
                    </div>
                </li>-->
                            </ul>
        </div>
    </div>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>


    <script src="/static/home/js/common/conttab_child.js"></script>
    <script src="/static/home/js/common/gotoAnotherPage.js"></script>
    <script src="/static/home/js/accountOverview/accountOverview.js"></script>
	<script>
 function loginout(){
     var index = layer.load(0, {shade: false});
     $.post('<?php echo U('home/login/loginout'); ?>',{},function(e){
	   layer.close(index);
	   layer.alert(e.msg, {
		  skin: 'layui-layer-molv' //样式类名
		  ,closeBtn: 0
		}, function(){
		   if(e.code==1){
		   location.href="/";
		   }
		});
	 })
 }
</script>

</body>
</html>
