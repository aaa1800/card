<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:84:"C:\wwwroot\shouka.paimaivip.net\public/../application/simple\view\admin\welcome.html";i:1616590069;s:73:"C:\wwwroot\shouka.paimaivip.net\application\Simple\view\public\heard.html";i:1616590069;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<style>
#tf{
 width: 80%;
    float: left;
    margin-top: 8px;
}
#tf h3, #tf cite{
color:#FFF
}
#bf{
 width: 80%;
    float: left;
    margin-top: 8px;
}
#fft h3,#fft cite{
color:#FFF
}
#liw{
  width:50%
}
@media screen and (min-width: 992px){
	#liw{
	 width:12.5%
	}
}
</style>
    <body>
	<div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">后台首页</a>
            <a>
              <cite>首页</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">欢迎管理员：
                                <span class="x-red"><?php echo session('adname'); ?></span>！当前时间:<?php echo date("Y-m-d H:i:s"); ?> <span class="x-red"> V1.6</span>
                            </blockquote>
                        </div>
                    </div>
                </div>
				 
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">数据统计</div>
                        <div class="layui-card-body ">
                            <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
							 <li class="layui-col-md2 layui-col-xs6">
                                   <a href="javascript:;" class="x-admin-backlog-body" id="tf" style="background:#ff5722">
                                        <h3>总会员</h3>
                                        <p>
                                            <cite><?php echo $f['user']; ?></cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="tf" style="background:#1e9fff">
                                        <h3>销卡总计</h3>
                                        <p>
                                            <cite>￥<?php echo $f['card_num']; ?>/<?php echo $f['card_count']; ?>笔</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="tf" style="background:#32c5d2">
                                        <h3>成功总计</h3>
                                        <p>
                                            <cite>￥<?php echo $f['secc_num']; ?>/<?php echo $f['secc_count']; ?>笔</cite></p>
                                    </a>
									
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="tf" style="background:#8E44AD">
                                        <h3>失败总计</h3>
                                        <p>
                                            <cite>￥<?php echo $f['err_num']; ?>/<?php echo $f['err_count']; ?>笔</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="tf" style="background:#322ece">
                                        <h3>利润总计</h3>
                                        <p>
                                            <cite>￥<?php echo $f['profit']; ?></cite></p>
                                    </a>
                                </li>
                               
                                
                            </ul>
                        </div>
                    </div>
                </div>
				<div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">今日数据统计</div>
                        <div class="layui-card-body ">
                            <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
							<li class="layui-col-md2 layui-col-xs6">
                                   <a href="javascript:;" class="x-admin-backlog-body"  >
                                        <h3 style="color:#ff5722">今日注册</h3>
                                        <p>
                                            <cite style="color:#ff5722"><?php echo $f['d_user']; ?></cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="bf">
                                        <h3 style="color:#1e9fff">销卡总计</h3>
                                        <p>
                                            <cite style="color:#1e9fff">￥<?php echo $f['d_card_num']; ?>/<?php echo $f['d_card_count']; ?>笔</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="bf">
                                        <h3 style="color:#32c5d2">成功总计</h3>
                                        <p>
                                            <cite style="color:#32c5d2">￥<?php echo $f['d_secc_num']; ?>/<?php echo $f['d_secc_count']; ?>笔</cite></p>
                                    </a>
									
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="bf" >
                                        <h3 style="color:#8E44AD">失败总计</h3>
                                        <p>
                                            <cite style="color:#8E44AD">￥<?php echo $f['d_err_num']; ?>/<?php echo $f['d_err_count']; ?>笔</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body" id="bf" >
                                        <h3 style="color:#322ece">利润总计</h3>
                                        <p>
                                            <cite style="color:#322ece">￥<?php echo $f['d_profit']; ?></cite></p>
                                    </a>
                                </li>
                               
                                
                            </ul>
                        </div>
                    </div>
                </div>
				   <div class="layui-row layui-col-space15">

                <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">一月数据统计</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main1" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div>
              <!--   <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">最新一周PV/UV量</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main2" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div>
                <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">用户来源</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main3" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div> -->
                <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">硬盘使用量</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main4" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div>
            </div>
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">系统信息</div>
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                    <tr>
                                        <th>二次美化开发</th>
                                        <td>昊天网络工作室</td></tr>
                                    <tr>
                                        <th>服务器地址</th>
                                        <td><?php echo GetHostByName($_SERVER['SERVER_NAME']); ?></td></tr>
                                    <tr>
                                        <th>操作系统</th>
                                        <td><?php echo php_uname('s'); ?></td></tr>
                                    <tr>
                                        <th>运行环境</th>
                                        <td><?php echo $_SERVER["SERVER_SOFTWARE"]; ?></td></tr>
                                    <tr>
                                        <th>PHP版本</th>
                                        <td><?php echo PHP_VERSION; ?></td></tr>
                                    <tr>
                                        <th>PHP运行方式</th>
                                        <td><?php echo php_sapi_name(); ?></td></tr>
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        </div>
    </body>
	<script src="/static/simple/js/jquery.min.js"></script>
	<script src="/static/simple/js/echarts.min.js"></script>
        <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        
		$.post("<?php echo U('simple/admin/getDa'); ?>",{},function(e){
		       // 指定图表的配置项和数据
			   var myChart = echarts.init(document.getElementById('main1'));
			var option = {
				grid: {
					top: '15%',
					right: '1%',
					left: '1%',
					bottom: '5%',
					containLabel: true
				},
				tooltip: {
					trigger: 'axis'
				},
				xAxis: {
					type: 'category',
					data: e.data.t
				},
				yAxis: {
					type: 'value'
				},
				legend: {
					data: ['用户','兑换','成功','利润']
				},
				series: [{
					name:'用户',
					data: e.data.user,
					type: 'line',
					smooth: true
				},{
					name:'兑换',
					data: e.data.rech,
					type: 'line',
					smooth: true
				},{
					name:'成功',
					data: e.data.hui,
					type: 'line',
					smooth: true
				},{
					name:'利润',
					data: e.data.pay,
					type: 'line',
					smooth: true
				}]
			};


			// 使用刚指定的配置项和数据显示图表。
			myChart.setOption(option);
		});
		
        
       <!--  // 基于准备好的dom，初始化echarts实例
       /* var myChart = echarts.init(document.getElementById('main2'));
        // 指定图表的配置项和数据
        var option = {
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            grid: {
                top: '15%',
                right: '2%',
                left: '1%',
                bottom: '10%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : ['周一','周二','周三','周四','周五','周六','周日']
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
			legend: {
                data: ['PV','UV']
            },
            series : [
                {
                    name:'PV',
                    type:'line',
                    areaStyle: {normal: {}},
                    data:[120, 132, 101, 134, 90, 230, 210],
                    smooth: true
                },
                {
                    name:'UV',
                    type:'line',
                    areaStyle: {normal: {}},
                    data:[45, 182, 191, 234, 290, 330, 310],
                    smooth: true,

                }
            ]
        };


        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);


        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main3'));

        // 指定图表的配置项和数据
        var option = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
            },
            series : [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {value:335, name:'直接访问'},
                        {value:310, name:'邮件营销'},
                        {value:234, name:'联盟广告'},
                        {value:135, name:'视频广告'},
                        {value:1548, name:'搜索引擎'}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };



        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);*/
         // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main4'));

        // 指定图表的配置项和数据
        var option = {
            tooltip : {
                formatter: "{a} <br/>{b} : {c}%"
            },
            series: [
                {
                    name: '硬盘使用量',
                    type: 'gauge',
                    detail: {formatter:'{value}%'},
                    data: [{value: '<?php echo GetDisk(); ?>', name: '已使用'}]
                }
            ]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
</html>