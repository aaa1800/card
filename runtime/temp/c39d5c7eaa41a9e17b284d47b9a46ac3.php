<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:80:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\index\mobile\help.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">帮助中心</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="setting-wrap setting-help-warp w-wrap">
            <div class="setting-modules" id="module1">
                <a class="media" href="javascript:void(0);">
                    <img class="w44 align-self-center" src="/static/home/images/setting/help/help_about_me.png">
                    <div class="media-body no-bg">
                        <p class="clearfix">
                            <span class="float-left left-txt">公司相关</span>
                        </p>
                    </div>
                </a>
                <!-- <a class="media" href="http://m.renrenxiaoka.com/article/article-detail.html">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">公司简介</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="javascript:void(0);">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">联系我们</span>
                        </p>
                    </div>
                </a> -->
            </div>

            <div class="setting-modules" id="module2">
                <a class="media" href="javascript:void(0);">
                    <img class="w44 align-self-center" src="/static/home/images/setting/help/help_solve_pro.png">
                    <div class="media-body no-bg">
                        <p class="clearfix">
                            <span class="float-left left-txt">帮助中心</span>
                        </p>
                    </div>
                </a>
                <!-- <a class="media" href="http://m.renrenxiaoka.com/setting/faq/index.html">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">常见问题</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="javascript:void(0);">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">平台公告</span>
                        </p>
                    </div>
                </a> -->
            </div>

            <div class="setting-modules" id="module3">
                <a class="media" href="javascript:void(0);">
                    <img class="w44 align-self-center" src="/static/home/images/setting/help/help_trade_mess.png">
                    <div class="media-body no-bg">
                        <p class="clearfix">
                            <span class="float-left left-txt">行业相关</span>
                        </p>
                    </div>
                </a>
                <!-- <a class="media" href="http://m.renrenxiaoka.com/member/setting/help/index.html">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">行业服务</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="javascript:void(0);">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt">平台效益</span>
                        </p>
                    </div>
                </a> -->
            </div>
        </main>
    </div>
</div>

<script>
    var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/setting/help/helpCenter.js?v=10"></script>
</body>
</html>