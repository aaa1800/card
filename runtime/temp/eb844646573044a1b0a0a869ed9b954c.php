<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:76:"C:\wwwroot\hs.qulaida.top\public/../application/simple\view\admin\index.html";i:1638015884;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-<?php echo C('title'); ?></title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
		 <link rel="stylesheet" href="\static\simple\lib\layui\css\notice.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="index">
        <!-- 顶部开始 -->
        <div class="container">
            <div class="logo">
                <a href="<?php echo U('simple/admin/index'); ?>">后台中心</a></div>
            <div class="left_open">
                <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
            </div>
            <!-- <ul class="layui-nav left fast-add" lay-filter="">
                <li class="layui-nav-item">
                    <a href="javascript:;">+新增</a>
                    <dl class="layui-nav-child">
                        <!-- 二级菜单 -->
                        <!--<dd>
                            <a onclick="xadmin.open('最大化','http://www.baidu.com','','',true)">
                                <i class="iconfont">&#xe6a2;</i>弹出最大化</a></dd>
                        <dd>
                            <a onclick="xadmin.open('弹出自动宽高','http://www.baidu.com')">
                                <i class="iconfont">&#xe6a8;</i>弹出自动宽高</a></dd>
                        <dd>
                            <a onclick="xadmin.open('弹出指定宽高','http://www.baidu.com',500,300)">
                                <i class="iconfont">&#xe6a8;</i>弹出指定宽高</a></dd>
                        <dd>
                            <a onclick="xadmin.add_tab('在tab打开','member-list.html')">
                                <i class="iconfont">&#xe6b8;</i>在tab打开</a></dd>
                        <dd>
                            <a onclick="xadmin.add_tab('在tab打开刷新','member-del.html',true)">
                                <i class="iconfont">&#xe6b8;</i>在tab打开刷新</a></dd>
                    </dl>
                </li>
            </ul> -->
            <ul class="layui-nav right" lay-filter="">
			   <li class="layui-nav-item to-index" style="margin-right:12px"> 
			    <i class="layui-icon layui-icon-rmb"></i><span id="distillcount">0</span>
			   </li>
			    <li class="layui-nav-item to-index" style="margin-right:12px"> 
			    <i class="layui-icon layui-icon-form"></i><span id="tcount">0</span>
			   </li>
                <li class="layui-nav-item">
                    <a href="javascript:;"><?php echo $ad; ?></a>
                    <dl class="layui-nav-child">
                        <!-- 二级菜单 -->
                        <!--<dd>
                            <a onclick="xadmin.open('个人信息','http://www.baidu.com')">个人信息</a></dd>
                        <dd>
                             <a onclick="xadmin.open('切换帐号','http://www.baidu.com')">切换帐号</a></dd>
                        <dd> -->
                            <a href="javascript:void(0)" id="btnout">退出</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item to-index">
                    <a href="/">前台首页</a></li>
            </ul>
        </div>
        <!-- 顶部结束 -->
        <!-- 中部开始 -->
        <!-- 左侧菜单开始 -->
        <div class="left-nav">
            <div id="side-nav">
                <ul id="nav">
				 <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="权限管理"><i class="layui-icon layui-icon-set"></i></i>
                            <cite>权限管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('管理员列表','<?php echo U('simple/admin/adminList'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>管理员列表</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('角色管理','<?php echo U('simple/admin/adminRole'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>角色管理</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('权限管理','<?php echo U('simple/admin/adminRule'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>权限管理</cite></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="会员管理">&#xe6b8;</i>
                            <cite>会员管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                           
                            <li>
                                <a onclick="xadmin.add_tab('会员列表','<?php echo U('simple/member/memberlist'); ?>',true)">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员列表</cite></a>
                            </li>
                            <!--<li>
                                <a onclick="xadmin.add_tab('兑卡记录','<?php echo U('simple/member/memberMoneyOrder'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>兑卡记录</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('本站充值','<?php echo U('simple/member/memberPayOrder'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>本站充值</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('汇款记录','<?php echo U('simple/member/memberTxOrder'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>汇款记录</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('开通记录','<?php echo U('simple/member/memberVipOrder'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>开通记录</cite></a>
                            </li>-->
							<li>
                                <a onclick="xadmin.add_tab('提现审核','<?php echo U('simple/member/memberWithOrder'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>提现审核</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('佣金列表','<?php echo U('simple/member/memberYongjin'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>佣金列表</cite></a>
                            </li>
                        </ul>
                    </li>
					<li >
                        <a  onclick="xadmin.add_tab('高级认证','<?php echo U('simple/gaoji/index'); ?>')">
                            <i class="iconfont left-nav-li" lay-tips="高级认证">&#xe71c;</i>
                            <cite>高级认证</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li>
					<li >
                        <a  onclick="xadmin.add_tab('企业认证','<?php echo U('simple/qiye/index'); ?>')">
                            <i class="iconfont left-nav-li" lay-tips="企业认证">&#xe71c;</i>
                            <cite>企业认证</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li>
					
					<li >
                        <a  onclick="xadmin.add_tab('订单管理','<?php echo U('simple/member/memberMoneyOrder'); ?>')">
                            <i class="iconfont left-nav-li" lay-tips="订单管理">&#xe6fc;</i>
                            <cite>订单管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li>
					
					<!--<li >-->
     <!--                   <a  onclick="xadmin.add_tab('报表统计','<?php echo U('simple/Orders/index'); ?>')">-->
     <!--                       <i class="iconfont left-nav-li" lay-tips="报表统计">&#xe724;</i>-->
     <!--                       <cite>报表统计</cite>-->
     <!--                       <i class="iconfont nav_right">&#xe697;</i></a>-->
     <!--               </li>-->
					<!--<li >-->
     <!--                   <a  onclick="xadmin.add_tab('管理员报表','<?php echo U('simple/Orders/adminorder'); ?>')">-->
     <!--                       <i class="iconfont left-nav-li" lay-tips="管理员报表计">&#xe724;</i>-->
     <!--                       <cite>管理员报表</cite>-->
     <!--                       <i class="iconfont nav_right">&#xe697;</i></a>-->
     <!--               </li>-->
					
                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="通道管理">&#xe723;</i>
                            <cite>通道管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('卡类管理','<?php echo U('simple/card/mVerify'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>卡类管理</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('通道管理','<?php echo U('simple/card/opaer'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>通道管理</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('类型管理','/copaer')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>类型管理</cite></a>
                            </li>
                            <!--<li>-->
                            <!--    <a onclick="xadmin.add_tab('发送记录','<?php echo U('simple/card/msgSend'); ?>')">-->
                            <!--        <i class="iconfont">&#xe6a7;</i>-->
                            <!--        <cite>发送记录</cite></a>-->
                            <!--</li>-->
							 
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="系统设置">&#xe723;</i>
                            <cite>系统设置</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('网站设置','<?php echo U('simple/system/index'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>网站设置</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('支付宝设置','<?php echo U('simple/system/alipay'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>支付宝设置</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('微信设置','<?php echo U('simple/system/weixin'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>微信设置</cite></a>
                            </li>
							
							<li>
                                <a onclick="xadmin.add_tab('邮件配置','<?php echo U('simple/system/email'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>邮件配置</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('轮播图设置','<?php echo U('simple/system/banner'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>轮播图设置</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('文章管理','<?php echo U('simple/system/news'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>文章管理</cite></a>
                            </li>
							<!--<li>-->
       <!--                         <a onclick="xadmin.add_tab('软件管理','<?php echo U('simple/system/Software'); ?>')">-->
       <!--                             <i class="iconfont">&#xe6a7;</i>-->
       <!--                             <cite>软件管理</cite></a>-->
       <!--                     </li>-->
                        </ul>
                    </li>
                    <!--<li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="域名管理">&#xe723;</i>
                            <cite>域名管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('域名列表','<?php echo U('simple/domain/doList'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>域名列表</cite></a>
                            </li>
							<li>
                                <a onclick="xadmin.add_tab('费用列表','<?php echo U('simple/domain/xfList'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>费用列表</cite></a>
                            </li>
                        </ul>
                    </li>
                   
                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="收款账号">&#xe6ce;</i>
                            <cite>收款账号</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('系统账户','<?php echo U('simple/ewm/pay'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>系统账户</cite></a>
                            </li>
                        </ul>
                    </li>-->
                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="网站结算">&#xe6b4;</i>
                            <cite>数据管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <!--<li>
                                <a onclick="xadmin.add_tab('结算中心','<?php echo U('simple/webjs/index'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>结算中心</cite></a>
                            </li>-->
							<li>
                                <a onclick="xadmin.add_tab('数据删除','<?php echo U('simple/webjs/delet'); ?>')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>数据删除</cite></a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
        <!-- <div class="x-slide_left"></div> -->
        <!-- 左侧菜单结束 -->
        <!-- 右侧主体开始 -->
        <div class="page-content">
            <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
                <ul class="layui-tab-title">
                    <li class="home">
                        <i class="layui-icon">&#xe68e;</i>我的桌面</li></ul>
                <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
                    <dl>
                        <dd data-type="this">关闭当前</dd>
                        <dd data-type="other">关闭其它</dd>
                        <dd data-type="all">关闭全部</dd></dl>
                </div>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <iframe src="<?php echo U('simple/admin/welcome'); ?>" frameborder="0" scrolling="yes" class="x-iframe"></iframe>
                    </div>
                </div>
                <div id="tab_show"></div>
            </div>
        </div>
        <div class="page-content-bg"></div>
        <style id="theme_style"></style>  
		<div id="audio"></div>
    </body>
	  <script>
	  layui.config({
			base: '/static/simple/lib/layui/lay/modules/'
		}).extend({
			notice: 'notice'
		});
		layui.use(['notice', 'jquery', 'layer'], function () {
        var notice = layui.notice;
        var layer = layui.layer;
        var $ = layui.jquery;

         // 初始化配置，同一样式只需要配置一次，非必须初始化，有默认配置
         notice.options = {
            closeButton:true,//显示关闭按钮
            debug:false,//启用debug
            positionClass:"toast-top-right",//弹出的位置,
            showDuration:"300",//显示的时间
            hideDuration:"1000",//消失的时间
            timeOut:"2000",//停留的时间
            extendedTimeOut:"1000",//控制时间
            showEasing:"swing",//显示时的动画缓冲方式
            hideEasing:"linear",//消失时的动画缓冲方式
            iconClass: 'toast-info', // 自定义图标，有内置，如不需要则传空 支持layui内置图标/自定义iconfont类名
            onclick: function(e){
				var str=e.currentTarget.innerText;
				console.log(e.currentTarget.innerText); // 点击关闭回调
				if(str.indexOf("订单") != -1 ){
				     $.post("<?php echo U('simple/member/guanbi'); ?>",{},function(){
						   xadmin.add_tab('订单记录',"<?php echo U('simple/member/memberMoneyOrder'); ?>");
						});
                  }else{
				   $.post("<?php echo U('simple/member/guanbiti'); ?>",{},function(){
				     xadmin.add_tab('提现审核',"<?php echo U('simple/member/memberWithOrder'); ?>");
					 })
               }				  
			}
        };
		
        window.setInterval(function () {
		 $.post("<?php echo U('simple/member/ssedistill'); ?>",function(e){
		       $("#tcount").html(e.order);
			   $("#distillcount").html(e.cash);
			   if(e.cash>0){
			      $("#audio").html('<audio id="chatAudio"><source src="/static/simple/music/notify.ogg" type="audio/ogg"><source src="/static/simple/music/notify.mp3" type="audio/mpeg"><source src="/static/simple/music/notify.wav" type="audio/wav"></audio>');
                  $('#chatAudio')[0].play();
				  notice.success("待处理提现" + e.cash + "笔","提现提醒");
			   }
			   if(e.order>0){
			      $("#audio").html('<audio id="chatAudio"><source src="/static/simple/music/notify.ogg" type="audio/ogg"><source src="/static/simple/music/notify.mp3" type="audio/mpeg"><source src="/static/simple/music/notify.wav" type="audio/wav"></audio>');
                  $('#chatAudio')[0].play();
				  notice.info("新提交订单" + e.order + "笔","订单提醒",12);
			   }
		 })
        },3000);

    });

    </script>
<script>

		layui.use('form', function(){
				  var form = layui.form,$ = layui.jquery;
				  $("#btnout").click(function(){
					  var index = layer.load(0, {shade: false});
					  $.post("<?php echo U('simple/login/outLogin'); ?>",{},function(e){
						  layer.close(index);
						  layer.msg(e.msg, {
						  icon: 1,
						  time: 1000},function(){
						   location.href="/";
					   });
					  });
					return false;
				  });
				});

</script>
</html>