<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:87:"D:\wwwroot\127.0.0.1\public/../application/home\view\order\mobile\batch_card_index.html";i:1667656648;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/dropload/dropload.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/tab-list.css?v=20190604155231">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">单卡订单</a>
    <a class="w44" href="javascript:void(0);"></a>
</header>
<ul class="nav nav-pills fixed-top justify-content-around" id="sellcard-record-tab">
    <li class="nav-item">
        <a class="nav-link active" href="javascript:void(0);">全部</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">处理中</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">成功</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);">失败</a>
    </li>
</ul>

<!--中间内容部分-->
<main class="sellcard-record-wrap w-wrap">    

    <div class="tab-content">
        <ul class="data-list">

            <!--<div class="null_data">
                <i class="null_classify_ico"></i>
                <p>暂无相关卖卡记录哦~</p>
                <a href="http://m.renrenxiaoka.com/member/index.do" class="btn btn-blue">返回个人中心</a>
            </div>-->

            <!--<li class="list-item">
                <div class="list-item-head row" href="#">
                    <div class="col">订单号：GZ241054947950 </div>
                    <div class="col text-right color9">成功兑换：<span class="color-orange">7/20</span></div>
                </div>
                <a class="media sellcard-media" href='http://m.renrenxiaoka.com/member/sellCardRecord/getSellCardOrdersDetails.do?type="1"&id="1"'>
                    <div class="media-body">
                        <h5 class="media-con color-orange">￥287.00 </h5>
                    </div>
                </a>
                <p class="list-item-footer no-right-arrow">提交时间：2017-03-07 16:49:13</p>
            </li>-->
            			           
            <!-- <li class="list-item single-card-item">              
                <div class="list-item-head row">
                    <div class="col-9">中国移动快销</div>
                    <div class="col text-right">
                    	<span class="state-btn fail-state">销卡失败</span>                    	
                   	</div>
                </div>
                <div class="media sellcard-media">
                	<div class="media-body">                                                
                        <p class="color3">卡号：13548241054947950</p>
                        <p class="color3">
                        	<span>面值：￥10</span>
                        	<span class="line-bar"> |</span> 
                        	<span class="color-orange">结算：￥3</span>
                       	</p>                        
                    </div>
                </div>
                
                <div class="footer clearfix">
                	<span class="float-left">提交时间：2017-03-07 16:49:13</span>
                   	<a class="float-right text-right color9" href=''>查看详情&gt;</a>
                </div>              
            </li> -->
        </ul>
        <ul class="data-list d-none"></ul>
        <ul class="data-list d-none"></ul>
        <ul class="data-list d-none"></ul>
    </div>
  
</main>

<!--返回顶部按钮-->
<a href="javascript:void(0);" class="totop" id="totop"></a>

<script>
    var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
	var orurl="<?php echo U('home/order/msingleCardIndex'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/dropload/dropload.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/common.js"></script>
<script src="/static/home/js/mobile/sellCardRecord/singleRecord.js?v=21"></script>
</body>
</html>