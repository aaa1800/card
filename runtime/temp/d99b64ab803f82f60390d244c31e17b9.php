<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:84:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\login\mobile\retrieve.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/form.css?v=10">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">找回密码</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="findback-wrap w-wrap">
    <div class="form-pannel t44">
        <form method="post" id="findback-pannel">
            <div class="form-group">
                <i class="user"></i>
                <input type="text" name="phone" class="form-control" id="accountName" placeholder="手机号/邮箱" maxlength="50">
                <a class="right delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>

            <div class="form-group">
                <i class="shield"></i>
                <input type="text" name="code" class="form-control code" id="findCode" placeholder="验证码" maxlength="8">
                <a class="right89 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="resend-code color-green" href="javascript:void(0);" id="getCode">发送验证码</a>
            </div>

            <div class="form-group">
                <i class="lock"></i>
                <input type="password" name="pwd" class="form-control" id="newPass" placeholder="设置登录密码" maxlength="20">
                <a class="right37 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="right isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
            </div>

            <div class="form-group">
                <i class="lock"></i>
                <input type="password" class="form-control" id="newPassAgain" placeholder="再次确认密码" maxlength="20">
                <a class="right37 delete" href="javascript:void(0);"><b class="w30"></b></a>
                <a class="right isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
            </div>
        </form>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="findPassBtn">立即找回</a>

    </div>
</main>

<script type="text/javascript">
var ctxPath = {domain: "",domainapi: "",csrf:"",staticDomain:"",error:""};
	var sendm = "<?php echo U('home/api/findcode'); ?>",setpass="<?php echo U('home/api/setpassa'); ?>",shou="<?php echo U('home/login/index'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/findBack/findBack.js?v=172"></script>
</body>
</html>