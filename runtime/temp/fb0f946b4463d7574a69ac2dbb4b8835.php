<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:75:"D:\wwwroot\127.0.0.1\public/../application/home\view\gong\mobile\index.html";i:1667656648;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/notice/notice.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">平台公告</a>
    <a class="w44 home_logo" href="/">
        <img src="/static/home/images/common/return_home_icon.png">
    </a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="notice-wrap w-wrap mt-44">
            <!--暂无公告-->
            <!--<div class="null_data">
                <i class="null_notice_ico"></i>
                <p>暂无相关公告哦~</p>
                <a href="#" class="btn btn-blue">返回个人中心</a>
            </div>-->

            <!--公告列表-->
            <div class="notice-list-warp" id="notice-list-warp">
             
                 <!--<div class="item">
                    <p class="notice-time">1111</p>
                    <div class="card border-0">
                        <div class="card-header"><h5>1111</h5></div>
                        <div class="card-body">
                           ';;;;;;;;
                        </div>
                        <div class="card-footer bg-white">
                            <a href="javascript:void(0);" class="media">
                                <div class="media-body veiw-detail">查看详情</div>
                            </a>
                        </div>
                    </div>
                </div>-->
            </div>
        </main>
    </div>
</div>

<script>
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>

<!--自定义-->
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/notice/notice.js?v=10"></script>
</body>
</html>