<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\member\member_fl.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                    <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<script type="text/html" id="status">
		 <input type="checkbox" name="start" value="{{d.id}}" da-rate="{{d.urate}}" da-type="{{d.type}}" lay-skin="switch" lay-text="已启用|已停止" lay-filter="start" {{ d.start == 1 ? 'checked' : '' }}>
		</script>
        <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "<?php echo $url; ?>",
            method: 'post',
			page:true,
			totalRow:true,
            cols: [[
			    {type: 'checkbox',width:40,minWidth:30},
				{field:'type',title:"通道代码", align: 'center', sort: true},
			    {field:'name',title: '卡类',align: 'center'},
                {field:'payrate', title: '默认费率' ,align: 'center'} ,
				{field:'urate',align: 'center',title:'当前费率',edit:"text"},
				{field: 'start',align: 'center', title: '账户状态',toolbar: '#status',},
            ]],
			limit:20,
			limits:[20,50,60]
        });		
		table.on('edit(mlist)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = "<?php echo $uid; ?>",str = obj.value;
            $.post('<?php echo U("simple/member/memberFl"); ?>',{'id':id,'rate':str,'type':obj.data.type,'start':obj.data.start},function (res) {
                layer.close(loading);
				if(res.code==1){
				  tableIn.reload();
				}else{
					layer.msg(res.msg,{time:1000,icon:2});
					return false;
				}
            })		  
		});
		form.on('switch(start)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = "<?php echo $uid; ?>";
			var urate=$(this).attr('da-rate'),type=$(this).attr('da-type');
            var authopen = obj.elem.checked===true?1:0;
			var str="启用";
			 if(authopen==1){
			    str="停用";
			 }
				$.post('<?php echo U("simple/member/memberFl"); ?>',{'id':id,'rate':urate,'type':type,'start':authopen},function (res) {
					layer.close(loading);
					if (res.code==1) {
						tableIn.reload();
					}else{
						layer.msg(res.msg,{time:1000,icon:2});
						return false;
					}
				})
        });
		
		 //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });
        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });
    </script>
    </body>

</html>