<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:83:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\addpass.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
    <li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li   class="active"><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>


</ul>   
<div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
						
                            <form class="form-horizontal p-xl" id="updatePhoneInitForm">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">您当前绑定的手机号：</label>
                                    <div class="col-sm-3">
                                        <span class="form-control noBorder height-auto text-blue"><?php echo tfen($u['photo'],3,4); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">短信验证码：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="phoneCode">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-danger" id="getPhoneCode" href="javascript:void(0);">获取验证码</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <div class="errorTip color_f00"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="nextVerifyPhoneBtn">下一步</a>
                                    </div>
                                </div>
                            </form>
							 <form class="form-horizontal p-xl" id="phoneSettingIndexForm" style="display:none">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">新手机号码：</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" name="newPhone" value="" placeholder="请输入要绑定的手机号" id="phone">
                                    </div>
                                </div>
                                
                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">短信验证码：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="phoneCodea">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-danger" id="getPhoneCodea" href="javascript:void(0);">获取验证码</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <div class="errorTip color_f00"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitPhoneSetIndexFormBtn">立即提交</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = {domain: "",_csrf:"",staticDomain:""};
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/profileManage/phoneSetting/updatePhoneInit.js?v=21"></script>



</body>
</html>
