<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:86:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\admin\rule_edit.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
			<blockquote class="layui-elem-quote">
				1、《控/方》：意思是 控制器/方法; 例如 Sys/sysList<br/>
				2、图标名称为左侧导航栏目的图标样式，CLTPHP菜单小图标采用的是<a href="https://icomoon.io/#premium" target="_blank">premium图标</a>
			</blockquote>
			<form class="layui-form">
				<input type="hidden" name="id" value="<?php echo $rule['id']; ?>">
				<div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red">*</span>权限名称
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" name="title" value="<?php echo $rule['title']; ?>" lay-verify="required" placeholder="权限名称" class="layui-input">
                      </div>
                </div>
				<div class="layui-form-item">
					<label class="layui-form-label"><span class="x-red">*</span>控制器/方法</label>
					<div class="layui-input-inline">
						<input type="text" name="href" value="<?php echo $rule['href']; ?>" lay-verify="required" placeholder="控制器/方法" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">图标名称</label>
					<div class="layui-input-inline">
						<input type="text" name="icon" value="<?php echo $rule['icon']; ?>" placeholder="图标名称" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">菜单状态</label>
					<div class="layui-input-block">
						<input type="radio" name="menustatus" <?php if($rule['menustatus'] == 1): ?>checked<?php endif; ?> value="1" title="开启">
						<input type="radio" name="menustatus" <?php if($rule['menustatus'] == 0): ?>checked<?php endif; ?> value="0" title="关闭">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">排序</label>
					<div class="layui-input-inline">
						<input type="text" name="sort" value="<?php echo $rule['sort']; ?>" placeholder="<?php echo lang('pleaseEnter'); ?>排序编号" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button type="button" class="layui-btn" lay-submit="" lay-filter="auth">立即提交</button>
					</div>
				</div>
			</form>
		</div>
	</div>
                    </div>
                </div>
            </div>
		    </body>
<script>
    layui.use(['form', 'layer'], function () {
        var form = layui.form,layer = layui.layer,$ = layui.jquery;
        form.on('submit(auth)', function (data) {
            // 提交到方法 默认为本身
            $.post("<?php echo U('simple/admin/ruleEdit'); ?>",data.field,function(res){
                if(res.code > 0){
                    layer.msg(res.msg,{time:1800,icon:1},function(){
						window.parent.location.reload();
						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index);
                    });
                }else{
                    layer.msg(res.msg,{time:1800,icon:2});
                }
            });
        })
    })
</script>
</html>