<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:84:"C:\wwwroot\shouka.paimaivip.net\public/../application/simple\view\system\weixin.html";i:1616590070;s:73:"C:\wwwroot\shouka.paimaivip.net\application\Simple\view\public\heard.html";i:1616590069;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
	 <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">系统设置</a>
            <a>
              <cite>微信设置</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
              <div class="layui-card">
				<div class="layui-card-body ">
					<form class="layui-form" action="">
					  <div class="layui-form-item">
						<label class="layui-form-label">APPID：</label>
						<div class="layui-input-block">
						  <input type="text" name="wxappid" style="width:50%" value="<?php echo C('wxappid'); ?>" lay-verify="title" autocomplete="off" placeholder="请输入" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">secretID：</label>
						<div class="layui-input-block" style="width:50%">
						<input type="text" name="wxsecret" style="width:50%" value="<?php echo C('wxsecret'); ?>" lay-verify="title" autocomplete="off" placeholder="请输入" class="layui-input">
						  
						</div>
					  </div>
					   <div class="layui-form-item">
						<label class="layui-form-label">微信商户号：</label>
						<div class="layui-input-block">
						  <input type="text" name="wxmach" style="width:50%" value="<?php echo C('wxmach'); ?>" lay-verify="title" autocomplete="off" placeholder="请输入内容" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">微信KEY：</label>
						<div class="layui-input-block">
						  <input type="password" name="wxkey" value="<?php echo C('wxkey'); ?>" style="width:50%" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
						</div>
					  </div>
					   <div class="layui-form-item">
						<label class="layui-form-label">微信CERT证书：</label>
						<div class="layui-input-block">
						  <input type="text" name="wxcert" style="width:30%" value="<?php echo C('wxcert'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
						</div>
						<button type="button" class="layui-btn" id="cert">
							  <i class="layui-icon">&#xe67c;</i>上传文件
							</button>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">微信KEY证书：</label>
						<div class="layui-input-block">
						  <input type="text" name="wxfkey" style="width:30%" value="<?php echo C('wxfkey'); ?>" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
						</div>
						<button type="button" class="layui-btn" id="text">
							  <i class="layui-icon">&#xe67c;</i>上传文件
							</button>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">单笔最高限额：</label>
						<div class="layui-input-block">
						  <input type="number" name="wxxian" value="<?php echo C('wxxian'); ?>" style="width:50%" lay-verify="required" placeholder="请输入金额" autocomplete="off" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">自动汇款开关：</label>
						<div class="layui-input-block">
						  <input type="checkbox" <?php if(C('txopen') == 'on'): ?>checked=""<?php endif; ?> name="txopen" lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF">
						</div>
					  </div>
					  
					   <div class="layui-card-body ">
					
						  <fieldset class="layui-elem-field">
							  <legend>公众号验证</legend>
							  <div class="layui-field-box">
								 <div class="layui-form-item">
									<label class="layui-form-label" style="width:120px">token：</label>
									<div class="layui-input-inline">
									  <input type="text" name="stoken" class="layui-input" value="<?php echo C('stoken'); ?>" >
									</div>
								 </div>
								 <div class="layui-form-item">
									<label class="layui-form-label" style="width:120px">aeskey：</label>
									<div class="layui-input-inline">
									  <input type="text" name="saeskey" class="layui-input" value="<?php echo C('saeskey'); ?>" >
									</div>
								 </div>
								  <div class="layui-form-item">
									<label class="layui-form-label">是否验证：</label>
									<div class="layui-input-block">
									  <input type="checkbox" <?php if(C('wxyan') == 'on'): ?>checked=""<?php endif; ?> name="wxyan" lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF">
									</div>
									<div class="layui-form-mid" style="color:red">验证通过必须关闭此开关才能正常使用微信功能</div>
								  </div>
							  </div>
						  </fieldset>
					</div>
					  <div class="layui-form-item">
						<div class="layui-input-block">
						  <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
						  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
						</div>
					  </div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</body>
<script>
layui.use(['form', 'layedit', 'laydate','upload'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
  
  
  //监听提交
  form.on('submit(demo1)', function(data){
    var loading =layer.load(3, {shade: [0.1,'#fff']});
    $.post("<?php echo U('simple/system/weixin'); ?>",data.field,function(res){
	        layer.close(loading);
			if (res.code==1) {
				layer.msg(res.msg,{time:1000,icon:1});
			}else{
				layer.msg(res.msg,{time:1000,icon:2});
			}
	})
    return false;
  });
 
  var upload = layui.upload;
   
  //执行实例
  var uploadInst = upload.render({
     elem: '#cert' //绑定元素
    ,url: '<?php echo U("simple/system/uploadfile"); ?>' //上传接口
	,accept: 'file'
    ,done: function(res){
      if(res.code==1){
			$("input[name=wxcert]").val(res.url);
			layer.msg(res.info,{time:1000,icon:1});
		 }else{
		    layer.msg(res.info,{time:1000,icon:2});
		 }
    }
    ,error: function(){
      //请求异常回调
    }
  });
  var uploadInst = upload.render({
    elem: '#text' //绑定元素
    ,url: '<?php echo U("simple/system/uploadfile"); ?>' //上传接口
	,accept: 'file'
    ,done: function(res){
      if(res.code==1){
			$("input[name=wxfkey]").val(res.url);
			layer.msg(res.info,{time:1000,icon:1});
		 }else{
		    layer.msg(res.info,{time:1000,icon:2});
		 }
    }
    ,error: function(){
      //请求异常回调
    }
  });
  
  
});

</script>
</html>