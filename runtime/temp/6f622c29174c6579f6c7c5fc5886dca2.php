<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:83:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\system\email.html";i:1616590070;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
	  <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">管理中心</a>
                <a>
                    <cite>邮件配置</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label for="L_username" class="layui-form-label">
                            <span class="x-red">*</span>邮件服务器</label>
                        <div class="layui-input-inline ">
                            <input type="text" name="emailAps" class="layui-input" value="<?php echo C('emailAps'); ?>" autocomplete="true" ></div>
                        </div>
                    <div class="layui-form-item">
                        <label for="L_email" class="layui-form-label">
                            <span class="x-red">*</span>端口</label>
                        <div class="layui-input-inline">
                            <input type="number" id="L_email"  name="emailDuam" required="" value="<?php echo C('emailDuam'); ?>" autocomplete="off" class="layui-input"></div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_photo" class="layui-form-label">
                            <span class="x-red">*</span>邮件用户名</label>
                        <div class="layui-input-inline">
						 <input type="text" id="L_email"  name="emailName" required="" value="<?php echo C('emailName'); ?>" autocomplete="off" class="layui-input">
						</div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_pass" class="layui-form-label">
                            <span class="x-red">*</span>发送密钥</label>
                        <div class="layui-input-inline" >
                            <input type="password" id="L_pass" name="emailKey" value="<?php echo C('emailKey'); ?>"  autocomplete="off" class="layui-input">
							</div>
                       </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>修改邮件模版</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="regEmail" ><?php echo C('regEmail'); ?></textarea>
						</div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>后台通知模版</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="msgEmail" ><?php echo C('msgEmail'); ?></textarea>
						</div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>异地通知模版</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="bmsgEmail" ><?php echo C('bmsgEmail'); ?></textarea>
						</div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>异地通知模版</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="bmsgEmail" ><?php echo C('bmsgEmail'); ?></textarea>
						</div>
                    </div>
					<div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>邮件验证码模版</label>
                        <div class="layui-input-block" style="width:50%">
						  <textarea class="layui-textarea" name="bmtpl" ><?php echo C('bmtpl'); ?></textarea>
						</div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button class="layui-btn" type='button' lay-filter="aby" lay-submit="">提交</button></div>
				</form>
            </div>
        </div>
    </body>
    <script>
	layui.use(['form', 'layer'],
	    function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;
                //监听提交
                form.on('submit(aby)',function(data) {
						var loading =layer.load(3, {shade: [0.1,'#fff']});
						$.post("<?php echo U('simple/system/email'); ?>",data.field,function(res){
								layer.close(loading);
								if (res.code==1) {
									layer.alert(res.msg,{icon:1});
								}else{
									layer.alert(res.msg,{icon:2});
								}
						})
						return false;
                  
                })
            });</script>
</html>