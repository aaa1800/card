<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:85:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\editemail.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
    <li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
 <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  class="active"><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
    <!-- <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
    <li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
</ul>               
 <div class="tab-content">
 <?php if(!(empty($u['email']) || (($u['email'] instanceof \think\Collection || $u['email'] instanceof \think\Paginator ) && $u['email']->isEmpty()))): switch($cookies): case "12": ?>
	  <div class="tab-pane active">
                        <div class="panel-body">
                            <!--已经绑定邮箱，更换邮箱  验证身份页面-->
                            <form class="form-horizontal m-t" id="modifyMailVerifyCodeForm" action="" method="post">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">验证方式：</label>
                                    <div class="col-sm-3">
                                        <!--验证方式有，通过绑定手机或者绑定邮箱 请判断是否有绑定手机或邮箱，绑定才会显示-->
                                        <select class="form-control" name="type" id="getCodeWay" onchange="getCurVerifyWay(this.value)">
                                            <option value="phone" selected="selected">通过已绑定手机验证</option>                                            <option value="email" >通过已绑定邮箱验证</option>                                        </select>
                                    </div>
                                </div>

                                <!--通过已绑定手机验证-->
                                <div id="viaPhoneVerify">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">已验证手机：</label>
                                        <div class="col-sm-3">
                                            <span class="form-control noBorder"><?php echo tfen($u['photo'],3,4); ?></span>
                                        </div>
                                    </div>
                                   
                                </div>

                                <!--通过已绑定邮箱验证-->
                                <div id="viaMailVerify">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">已验证邮箱：</label>
                                        <div class="col-sm-3">
                                            <span class="form-control noBorder"><?php echo tfen($u['email'],3,4); ?></span>
                                        </div>
                                    </div>

                                   
                                </div>
								<div >
                                   
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">验证码：</label>
                                        <div class="col-sm-2">
                                            <input class="form-control" type="text" name="" value="" placeholder="请输入验证码" id="mailCode">
                                        </div>
                                        <div class="col-sm-1">
                                            <a class="btn btn-danger" id="getMailCode" href="javascript:void(0);">获取验证码</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00" id="error"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitModifyMailVerifyCodeBtn">下一步</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

	<?php break; case "10": ?>
	  <div class="tab-pane active">
                        <div class="panel-body">
                            <!--已经绑定邮箱，更换邮箱  输入新邮箱页面-->
                            <form class="form-horizontal m-t" id="bindNewMailForm" action="" method="post">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">原邮箱账号：</label>
                                    <div class="col-sm-3">
                                        <span class="form-control noBorder"><?php echo tfen($u['email'],3,4); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">新邮箱账号：</label>
                                    <div class="col-sm-3">
                                        <input type="text" value="" class="form-control" placeholder="请输入新邮箱账号" id="newMail" name="newMail">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">验证码：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="newMailCode">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-danger" id="getNewMailCode" href="javascript:void(0);">获取验证码</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00" id="doerr"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitBindNewMailFormBtn">保存</a>
                                    </div>
                                </div>
                                <?php echo token(); ?>
                            </form>
                        </div>
                    </div>


	<?php break; default: ?>
       <div class="tab-pane active">
                        <div class="panel-body pt20">
                            <!--②、已经绑定邮箱-->
                            <div class="status-box">
                                <div class="status-box-icon pull-left"><i class="ico140 green-envelope"></i></div>
                                <div class="status-box-text">
                                    <h3 class="text-green">当前状态：邮箱已绑定</h3>
                                    <p class="color999">当前绑定邮箱：<?php echo tfen($u['email'],3,4); ?></p>
                                    <p class="color999">绑定邮箱可用于安全验证、找回密码等重要操作</p>
                                    <a class="btn btn-blue phone" href="<?php echo U('home/member/editemail',['cookies'=>12]); ?>"><i class="mail-ico"></i>更换邮箱</a>
                                </div>
                            </div>
                            <div class="wram-tips">
                                <h5>温馨提示：</h5>
                                <ul>
                                    <li>1、您的邮箱绑定之后，即可享受邮箱登录、找回密码等服务，让您的网上购物体验更安全更便捷；</li>
                                    <li>2、若您的邮箱可正常使用但无法接收到验证码，请检查您的垃圾邮件或广告邮件，将发件人设置为白名单，如果5分钟内仍未收到邮<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱验证码，您可以重新发送；
                                    </li>
                                    <li>3、如果您的邮箱已经无法正常使用，请提供用户名，手机号，身份证号，点击联系平台
                                        <a class="text-blue text-underline" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo C('kefu'); ?>&Site=%E6%B7%BB%E5%8A%A0%E5%AE%A2%E6%9C%8DQQ%E8%B4%AD%E4%B9%B0%E7%BB%AD%E8%B4%B9%E4%B8%8D%E8%BF%B7%E8%B7%AF&Menu=yes" target="_blank">在线客服</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
         <?php endswitch; else: ?>
                    <div class="tab-pane active">
                        <div class="panel-body">
                            <!--未绑定邮箱  绑定邮箱页面-->
                            <form class="form-horizontal m-t" id="bindMailForm" action="" method="post">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">邮箱账号：</label>
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="mail" class="form-control" placeholder="请输入邮箱账号" id="nMail">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">邮箱验证码：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="mCode">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-danger" id="getOldMailCode" href="javascript:void(0);">获取验证码</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00" id="nerror"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitBindMailFormBtnsss">保存</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = "<?php echo U('home/api/jiaoyimima'); ?>",editTran="<?php echo U('home/member/editTran'); ?>",precode="<?php echo U('home/api/preCode'); ?>",curll="<?php echo U('home/member/editemail',['cookies'=>10]); ?>",sendEmail="<?php echo U('home/api/sendEmail'); ?>",addemail="<?php echo U('home/member/addemail'); ?>";
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/profileManage/mailSetting/updateMailInit.js?v=11"></script>



</body>
</html>
