<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:92:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\order\single_card_index.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 首页内容(账户总览)</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
 <link href="/static/home/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <!--查询部分 start-->
            <div class="search-toolbar">
                <form role="form" class="form-inline" id="queryForm">
                    <input type="hidden" name="_csrf" value="e73ba083-c73f-4c4c-b2f2-9f51170855b7" id="csrf"/>
                    <input type="hidden" name="keywords" value=""><!--关键字-->
                    <input type="hidden" name="orderType" value="" id="orderType"><!--排序方式-->

                    <input type="hidden" name="region" value="TODAY" id="commitRegion"><!--提交时间按钮类型 默认今天-->
                    <input type="hidden" name="startDate" value="" id="commitStartDate"><!--提交时间开始-->
                    <input type="hidden" name="endDate" value="" id="commitEndDate"><!--提交时间结束-->

                    <input type="hidden" name="pageSize" value="10" id="pageSize"><!--每页显示多少条记录 默认显示10条-->
                    <input type="hidden" name="pageNumber" value="1" id="pageNumber"><!--查询页码-->

                    <div class="form-group">
                        <label>提交时间:</label>
                        <input id="timeStart" class="form-control laydate-icon" placeholder="YYYY-MM-DD hh:mm:ss">
                        -
                        <input id="timeEnd" class="form-control laydate-icon" placeholder="YYYY-MM-DD hh:mm:ss">
                    </div>
                    <div class="form-group button-group" id="dateBtns">
                        <button region="LASTDAY" type="button" class="btn btn-default btn-xs">昨天</button>
                        <button region="TODAY" type="button" class="btn btn-default btn-xs">今天</button>
                        <button region="WEEK" type="button" class="btn btn-default btn-xs">近7天</button>
                        <button region="1" type="button" class="btn btn-default btn-xs">近一月</button>
                        <!--<button region="3" type="button" class="btn btn-default btn-xs">近三月</button>-->
                        <button region="NONE" type="button" class="btn btn-default btn-xs">忽略</button>
                    </div>
                    <br>

                    <div class="form-group">
                        <label>订单查询:</label>
                        <input id="textKeywordId" onchange="doTextKeyWordId();" name="orderNumber" value="" type="text" class="form-control" placeholder="请输入关键词搜索">
                        <select onchange="selectDdlType(this.value, 'textKeywordId')" class="form-control">
                            <option value="orderNumber" selected="selected">订单号</option>
                            <option value="cardNumber">卡号</option>
                            <option value="cardPassword">卡密</option>
                        </select>
                        <select name="collectionStatus" class="form-control">
                            <option value="" selected="selected">状态</option>
                            <option value="1">正在处理</option>
                            <option value="2">处理完成</option>
                            <!-- <option value="3">回收失败</option> -->
                        </select>
                    </div>
                    
                    <div class="form-group">
                    	<label>类型查询:</label>
                        <select class="form-control" id="productClassifyId" name="productClassifyId" onchange="doproductClassifyId()">
                            <option value="-1" selected="selected">卡类(全部)</option>
                                                        	<option value="0">话费卡</option>
                                                        <option value="1">游戏卡</option>
                                                        <option value="3">加油卡</option>
                                                        <option value="4">商超卡</option>
                                                        <option value="5">视频卡</option>
                                                        <option value="6">电商卡</option>
                                                    </select>
                        <select name="operatorId" class="form-control" id="operatorId">
                            <option value="" selected="selected">卡种(全部)</option>
                                                        	<?php if(is_array($channel) || $channel instanceof \think\Collection || $channel instanceof \think\Paginator): if( count($channel)==0 ) : echo "" ;else: foreach($channel as $key=>$p): ?>
                                                        <option value="<?php echo $p['type']; ?>"><?php echo $p['name']; ?></option>
														<?php endforeach; endif; else: echo "" ;endif; ?>
                                                    </select>
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-w-m72 btn-blue m-r-14" type="button" id="search">确定查询</button>
                        <!-- <button onclick="doHistory();" class="btn btn-w-m72 btn-blue m-r-14" type="button" id="searchHistory">切换到一月前的订单查询</button> -->
                    </div>
                </form>
            </div>
            <!--查询部分 end-->

            <table id="dataTable" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>日期</th>
                        <th>订单号</th>
                        <th>卡号</th>
                        <th>卡种</th>
                        <th>提交总面值</th>
                        <th>实际总面值</th>
                        <th>实际可得</th>
                        <th>总共提交</th>
                        <th>成功销卡</th>
                        <th>销卡失败</th>
                        <th>处理状态</th>
                        <th>类型</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody class="dataWraper">

                </tbody>
                <tfoot>
                    <tr>
                        <th>本页合计</th>
                        <th colspan="5"></th>
                        <th id="actualAmountCurPage">?</th>
                        <th id="totalCountCurPage">?</th>
                        <th id="successCountCurPage">?</th>
                        <th id="failedCountCurPage">?</th>
                        <th colspan="3"></th>
                    </tr>
                    <tr>
                        <th>查询总计</th>
                        <th colspan="5"></th>
                        <th id="actualAmountTotal">?</th>
                        <th id="totalCountTotal">?</th>
                        <th id="successCountTotal">?</th>
                        <th id="failedCountTotal">?</th>
                        <th colspan="3"></th>
                    </tr>
                </tfoot>
            </table>

            <!--分页-->
            <div class="row myPager" id="queryPagination"></div>

        </div>
    </div>
</div>


<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script src="/static/home/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="/static/home/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script>
    var ctxPath ="<?php echo U('home/order/singleCardIndex'); ?>";
</script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/common-util.js"></script>
<script src="/static/home/js/common/page-data.js"></script>
<script src="/static/home/js/sellCardRecord/singleRecord.js?v=14"></script>




</body>
</html>
