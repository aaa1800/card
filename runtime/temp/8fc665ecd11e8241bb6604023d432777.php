<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\admin\admin_edit.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red">*</span>登录名
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="user" name="user" value="<?php echo $u['user']; ?>" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">*</span>将会成为您唯一的登入名
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="phone" class="layui-form-label">
                          <span class="x-red">*</span>手机
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="tel" name="tel" value="<?php echo $u['tel']; ?>" required="" lay-verify="phone"
                          autocomplete="off" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">*</span>确认身份
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="L_email" class="layui-form-label">
                          <span class="x-red">*</span>邮箱
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="email" name="email" value="<?php echo $u['email']; ?>" required="" lay-verify="email"
                          autocomplete="off" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">*</span>接收登陆验证码
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label class="layui-form-label"><span class="x-red">*</span>角色</label>
                      <div class="layui-input-block">
                        <?php echo Adz($u['diction']); ?>
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="L_pass" class="layui-form-label">
                          <span class="x-red">*</span>密码
                      </label>
                      <div class="layui-input-inline">
                          <input type="password" id="L_pass" name="pass"   lay-verify="pass" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          不修改留空
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                          <span class="x-red">*</span>确认密码
                      </label>
                      <div class="layui-input-inline">
                          <input type="password" id="L_repass" name="repassb"  lay-verify="repass" class="layui-input">
                      </div>
					   <div class="layui-form-mid layui-word-aux">
                          不修改留空
                      </div>
                  </div>
				  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                          可操作会员
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="L_repass" name="eid" value="<?php echo $u['eid']; ?>"  class="layui-input">
                      </div>
					   <div class="layui-form-mid layui-word-aux">
                          不设定留空，例如：102,2,5
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                      </label>
					  <input type="hidden" name="id" value="<?php echo $u['id']; ?>">
                      <button type="button" class="layui-btn" lay-filter="add" lay-submit="">
                          保存
                      </button>
                  </div>
              </form>
            </div>
        </div>
    <script>
	

		layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;
                layer.ready(function(){
				   var ok="<?php echo $ok; ?>";
				   if(!ok){
						layer.msg("参数错误",{time:1800,icon:2},function(){
								var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
								parent.layer.close(index);
							});
					}
				  });
                //自定义验证规则
                form.verify({
                    user: function(value) {
                        if (value.length < 5) {
                            return '昵称至少得5个字符啊';
                        }
                    },
                   // pass: [/(.+){6,12}$/, '密码必须6到12位'],
                    repass: function(value) {
                        if ($('#L_pass').val() != $('#L_repass').val()) {
                            return '两次密码不一致';
                        }
                    }
                });

                //监听提交
                form.on('submit(add)',
					  function(data) {
						console.log(data);
						 $.post("<?php echo U('simple/admin/adminEdit'); ?>",data.field,function(res){
						      if(res.code==1){
									layer.alert("更新成功", {icon: 6},function() {
										xadmin.close();
										xadmin.father_reload();
									});
								}else{
								 layer.alert("更新失败", {icon: 5});
								}
						})
					  });

            });
			</script>
    </body>

</html>
