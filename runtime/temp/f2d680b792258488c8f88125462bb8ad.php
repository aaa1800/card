<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:87:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\index\mobile\news_detail.html";i:1638015884;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css">
</head>
<body class="bg-gray article-detail-page">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);"></a>
    <a class="w44" href="<?php echo U('home/member/index'); ?>">
        <img src="/static/home/images/common/return_me_icon.png">
    </a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="w-wrap">
            <div class="modules-item">
                <a class="media" href="javascript:void(0);">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt" id="articleTitle"></span>
                        </p>
                    </div>
                </a>
                <div class="media-content" id="articleCon"></div>
            </div>

			<input type="hidden" value="<?php echo $id; ?>" id="articleId"/>
        </main>
    </div>
</div>

<script>
    var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/minirefresh/minirefresh.min.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/article/article-detail.js?v=12"></script>
</body>
</html>