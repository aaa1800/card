<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:77:"C:\wwwroot\8.210.135.237\public/../application/home\view\member\recharge.html";i:1605265943;s:65:"C:\wwwroot\8.210.135.237\application\Home\view\layout\member.html";i:1605265943;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 提现记录</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
    <link href="/static/home/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <!--查询部分 start-->
            <div class="search-toolbar">
                <form role="form" class="form-inline" id="queryForm">
                    <input type="hidden" name="_csrf" value="e73ba083-c73f-4c4c-b2f2-9f51170855b7" id="csrf"/>
                    <input type="hidden" name="keywords" value=""><!--关键字-->
                    <input type="hidden" name="orderType" value="" id="orderType"><!--排序方式-->

                    <input type="hidden" name="region" value="TODAY" id="withdrawRegion"><!--提现时间按钮类型 默认今天-->
                    <input type="hidden" name="startDate" value="" id="withdrawStartDate"><!--提现时间开始-->
                    <input type="hidden" name="endDate" value="" id="withdrawEndDate"><!--提现时间结束-->

                    <input type="hidden" name="pageSize" value="10" id="pageSize"><!--每页显示多少条记录 默认显示10条-->
                    <input type="hidden" name="pageNumber" value="1" id="pageNumber"><!--查询页码-->

                    <div class="form-group">
                        <label>提现时间:</label>
                        <input id="timeStart" class="form-control laydate-icon" placeholder="YYYY-MM-DD hh:mm:ss">
                        -
                        <input id="timeEnd" class="form-control laydate-icon" placeholder="YYYY-MM-DD hh:mm:ss">
                    </div>
                    <div class="form-group button-group" id="dateBtns">
                        <button region="LASTDAY" type="button" class="btn btn-default btn-xs">昨天</button>
                        <button region="TODAY" type="button" class="btn btn-default btn-xs">今天</button>
                        <button region="WEEK" type="button" class="btn btn-default btn-xs">近7天</button>
                        <button region="1" type="button" class="btn btn-default btn-xs">近一月</button>
                        <!--<button region="3" type="button" class="btn btn-default btn-xs">近三月</button>-->
                        <button region="NONE" type="button" class="btn btn-default btn-xs">忽略</button>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-w-m72 btn-primary m-r-14" type="button" id="search">确定查询</button>
                    </div>
                </form>
            </div>
            <!--查询部分 end-->

            <table id="dataTable" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>提现单号</th>
                        <th>提现方式</th>
                        <th>姓名</th>
                        <th>帐号</th>
                        <th>提现金额</th>
                        <th>手续费</th>
                        <th>实际到账金额</th>
                        <th>申请时间</th>
                        <th>处理时间</th>
                        <th>状态</th>
                        <th>管理</th>
                    </tr>
                </thead>
                <tbody class="dataWraper">

                </tbody>
            </table>

            <!--分页-->
            <div class="row myPager" id="queryPagination"></div>

        </div>
    </div>
</div>


<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
var ctxPath="<?php echo U('home/moneylog/tixian'); ?>",detail="<?php echo U('home/member/getDetail'); ?>";
</script>
<script src="/static/home/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="/static/home/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/common-util.js"></script>
<script src="/static/home/js/common/page-data.js?v=10"></script>
<script src="/static/home/js/withdrawCashRecord/withdrawCashRecord.js?v=11"></script>

</body>
</html>
