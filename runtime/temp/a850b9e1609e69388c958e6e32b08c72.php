<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:71:"D:\wwwroot\127.0.0.1\public/../application/home\view\member\addpwd.html";i:1667656648;s:61:"D:\wwwroot\127.0.0.1\application\Home\view\layout\member.html";i:1667656648;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
    <li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
   <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li class="active" ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
    <!-- <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
    <li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
</ul>               <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
                            <!--已经绑定手机或邮箱，也设置了登录密码  修改登录密码页面-->
                            <form class="form-horizontal m-t" id="modifyLoginPasswordForm" action="http://api.renrenxiaoka.com/m/member/passwordManage/securityPassword/updateMemberSetPassword.do">
                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                                                                <ul class="form-btn-group clearfix">
        <li class="pull-left selected">
        <a class="btn" href="<?php echo U('home/member/addpwd'); ?>">
            <i class="ico24 blue-lock"></i>登录密码管理<b></b>
        </a>
    </li>
    <li class="pull-right">
        <a class="btn" href="<?php echo U('home/member/editPass'); ?>">
            <i class="ico24 blue-shield"></i>交易密码管理<b></b>
        </a>
    </li>
    </ul>                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">原登录密码：</label>
                                    <div class="col-sm-3">
                                        <input type="password" name="" value="" class="form-control" placeholder="请输入原登录密码" id="oldLoginPwd">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="javascript:forgetLoginPwd();" class="red-tip">忘记原密码？</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">新登录密码：</label>
                                    <div class="col-sm-3">
                                        <input type="password" name="" value="" class="form-control" placeholder="请输入新登录密码" id="newLoginPwd">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">确认新密码：</label>
                                    <div class="col-sm-3">
                                        <input type="password" name="" value="" class="form-control" placeholder="请重新输入一次新密码" id="newLoginPwdAgain">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--修改登录密码只需要验证原登录密码，不需要用手机或者邮箱接收验证码-->
                                    <label class="col-sm-4 control-label">验证码：</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="" value="" class="form-control" placeholder="请输入验证码" id="code">
                                    </div>
                                    <div class="col-sm-1 p-l-r-10">
                                        <img onclick="this.src='<?php echo captcha_src(); ?>'" src="<?php echo captcha_src(); ?>" alt="验证码" class="codeImg" title="点击图片刷新验证码">
                                    </div>
                                </div>
								<?php echo token(); ?>
                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitModifyLoginPwdFormBtn">保存</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = "<?php echo U('home/login/retrieve'); ?>",editPass="<?php echo U('home/member/editPass'); ?>";
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/profileManage/passwordManage/modifyLoginPwd.js"></script>



</body>
</html>
