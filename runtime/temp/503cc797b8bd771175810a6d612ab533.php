<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:83:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\index\mobile\recycle.html";i:1638015884;s:73:"C:\wwwroot\hs.qulaida.top\application\Home\view\layout\mobile\footer.html";i:1638015884;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/productClassify/classify.css?v=22">
	
</head>
<style>
 li img:first-child{
 background-color: #FFF;
 }
</style>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a href="javascript:void(0);"></a>
    <a class="title" href="javascript:void(0);">全部回收类型</a>
    <a class="w44" href="<?php echo U('home/index/myMessage'); ?>">
        <img src="/static/home/images/index/system_news_icon.png">
    </a>
</header>
<input type="hidden" id="productClassifyId" value="5" />
<!--中间内容部分-->
<main class="allCard-classify-wrap w-wrap">
    <ul class="nav nav-pills fixed-top justify-content-around" id="allCard-classify-tab"></ul>
    <div id="tab-content" class="tab-content"></div>
</main>

<!--固定底部-->
<!--固定底部-->
<footer class="navbar fixed-bottom bg-white w-wrap">
    <div class="row">
        <div class="col">
            <a class="d-block  <?php if(($acti == 'index') and ($cont == 'Index')): ?>active<?php endif; ?>" href="/">
                <div class="card border-0 rounded-0">
                    <i class="home"></i>
                    <div class="card-body">
                        <p class="card-text text-center">首页</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col">
            <a class="d-block <?php if($acti == 'recycle'): ?>active<?php endif; ?>" href="<?php echo U('home/index/recycle'); ?>" >
                <div class="card border-0 rounded-0">
                    <i class="category"></i>
                    <div class="card-body">
                        <p class="card-text text-center">分类</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col">
            <a class="d-block" href="<?php echo U('home/member/sellCard'); ?>">
                <div class="card border-0 rounded-0">
                    <i class="sell"></i>
                </div>
            </a>
        </div>
        <div class="col">
            <a class="d-block <?php if($acti == 'retrieve'): ?>active<?php endif; ?>" href="<?php echo U('home/index/retrieve'); ?>">
                <div class="card border-0 rounded-0">
                    <i class="enterprise"></i>
                    <div class="card-body">
                        <p class="card-text text-center">企业</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col">
        	<!-- <a class="d-block" href="http://m.renrenxiaoka.com/passport/login/index.html"> -->
            <a class="d-block <?php if($cont == 'Member'): ?>active<?php endif; ?>" href="<?php echo U('home/member/index'); ?>">
                <div class="card border-0 rounded-0">
                    <i class="mine"></i>
                    <div class="card-body">
                        <p class="card-text text-center">我的</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</footer>
<!--返回顶部按钮-->
<a href="javascript:void(0);" class="totop" id="totop"></a>
<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/bootstrap.bundle.min.js"></script><!--包含popper-->
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/productClassify/productClassify.js?v=25"></script>

<!--自定义-->
<script src="/static/home/js/mobile/common/common.js"></script>
</body>
</html>