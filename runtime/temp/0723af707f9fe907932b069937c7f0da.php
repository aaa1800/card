<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:84:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\moneylog.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 账户流水</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
    <link href="/static/home/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <!--查询部分 start-->
            <div class="search-toolbar">
                <form role="form" class="form-inline" id="queryForm">
                    <input type="hidden" name="_csrf" value="e73ba083-c73f-4c4c-b2f2-9f51170855b7" id="csrf"/>
                    <input type="hidden" name="keywords" value=""><!--关键字-->
                    <input type="hidden" name="orderType" value="" id="orderType"><!--排序方式-->

                    <input type="hidden" name="region" value="TODAY" id="timeRegion"><!--时间查询按钮类型 默认今天-->
                    <input type="hidden" name="startDate" value="" id="startDate"><!--时间查询开始-->
                    <input type="hidden" name="endDate" value="" id="endDate"><!--时间查询结束-->

                    <input type="hidden" name="pageSize" value="10" id="pageSize"><!--每页显示多少条记录 默认显示10条-->
                    <input type="hidden" name="pageNumber" value="1" id="pageNumber"><!--查询页码-->

                    <div class="form-group">
                        <label>订单查询:</label>
                        <input name="orderNumber" value="" type="text" class="form-control" placeholder="请输入订单号查询">
                    </div>
                    <div class="form-group">
                        <label>类型查询:</label>
                        <select class="form-control" name="type">
                            <option value="" selected="selected">全部</option>
                            <option value="1">单卡兑换</option>
                            <option value="2">批量兑换</option>
                            <option value="3">提现</option>
                            <option value="4">加/扣款</option>
                            <option value="5">佣金</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>状态查询:</label>
                        <select class="form-control" name="status">
                            <option value="" selected="selected">全部</option>
                            <option value="1">增加余额</option>
                            <option value="2">减少余额</option>
                        </select>
                    </div>
                    <br>

                    <div class="form-group">
                        <label>时间查询:</label>
                        <input id="timeStart" class="form-control laydate-icon" placeholder="YYYY-MM-DD hh:mm:ss">
                        -
                        <input id="timeEnd" class="form-control laydate-icon" placeholder="YYYY-MM-DD hh:mm:ss">
                    </div>
                    <div class="form-group button-group" id="dateBtns">
                        <button region="LASTDAY" type="button" class="btn btn-default btn-xs">昨天</button>
                        <button region="TODAY" type="button" class="btn btn-default btn-xs">今天</button>
                        <button region="WEEK" type="button" class="btn btn-default btn-xs">近7天</button>
                        <button region="1" type="button" class="btn btn-default btn-xs">近一月</button>
                        <button region="NONE" type="button" class="btn btn-default btn-xs">忽略</button>
                    </div>
                    <br>

                    <div class="form-group">
                        <label></label>
                        <button class="btn btn-w-m72 btn-blue m-r-14" type="button" id="search">确定查询</button>
                        <!--<button onclick="doHistory();" class="btn btn-w-m72 btn-blue m-r-14" type="button" id="searchHistory">切换到一月前的账单查询</button>-->
                    </div>
                </form>
            </div>
            <!--查询部分 end-->

            <table id="dataTable" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>时间</th>
                        <th>订单号</th>
                        <th>批量订单号</th>
                        <th>类型</th>
                        <th>变动金额</th>
                        <th>变动后</th>
                        <th>备注</th>
                    </tr>
                </thead>
                <tbody class="dataWraper">
                    
                </tbody>
            </table>

            <!--分页-->
            <div class="row myPager" id="queryPagination"></div>

        </div>
    </div>
</div>

<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<!-- Data Tables -->
<script src="/static/home/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="/static/home/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/common-util.js"></script>
<script src="/static/home/js/common/page-data.js?v=13"></script>
<script src="/static/home/js/accountStatement/accountStatement.js?v=10"></script>

</body>
</html>
