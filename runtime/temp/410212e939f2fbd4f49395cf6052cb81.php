<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:88:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\addqq.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/form.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">联系QQ</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="contactQQ-wrap w-wrap">
    <div class="form-easy">
        <form action="" method="post" id="nickname-pannel">
            <div class="form-group">
                <input type="text" name="" class="form-control" id="qqNumber" placeholder="请输入您的QQ号码" maxlength="12">
                <!--如果是修改，则提示“请输入新的QQ号码”-->
                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
            </div>

            <p class="form-tip">请填写您的QQ号，以便处理卡密问题时能方便联系到您</p>

        </form>


        <a href="javascript:void(0);" class="btn btn-blue full-btn mb-0" id="submitContactQQFormBtn">保存</a>

    </div>
</main>

<script>
    var ctxPath = {domain:"",domainapi:"",cookieDomain:"",_csrf:"",staticDomain:""};
	var qqurl="<?php echo U('home/member/addqq'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<!--自定义-->
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/contactQQ/contactQQ.js?v=19"></script>
</body>
</html>