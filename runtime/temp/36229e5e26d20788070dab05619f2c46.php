<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:88:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\withdrawCash.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 我要提现</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css?v=20181024001" rel="stylesheet">


	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading text-center">我要提现</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 center-box">
                            <div class="alert alert-warning">提现金额扣除手续费后要大于等于1元</div>
                        </div>
                    </div>

                    <form class="form-horizontal m-t-md" id="withdrawCashForm" method="post" action="">
                        <input type="hidden" name="_csrf" value="e73ba083-c73f-4c4c-b2f2-9f51170855b7" id="csrf"/>
                         <input type="hidden" name ='platform' value="1" id='platform' ><!-- 平台 : 1.PC端    2.手机浏览器端    3.微信端-->
                        <div class="form-group">
                            <label class="col-sm-5 control-label">可提现余额：</label>
                            <div class="col-sm-3">
                                <span class="form-control noBorder text-orange">￥<?php echo $u['money']; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">收款人：<?php echo $u['username']; ?></label>
                            <div class="col-sm-3">
                                <span class="form-control noBorder">
								<?php if($u['real'] == '0'): ?>
                                    <!--未实名-->
                                    <span class="text-red m-r-md">未知</span><a class="text-blue text-underline" href="javascript:gotoRealName();">马上去实名认证</a>
                                                                    </span>
																	<?php else: ?>
																	<span class="m-r-md"><?php echo $u['username']; ?></span><span class="btn btn-green btn-xs">已认证</span>
                                                                    </span>

																	<?php endif; ?>
                            </div>
                        </div>
                        <!-- 提现方式 -->
                        
                        <div class="form-group">
                            <label class="col-sm-5 control-label">提现方式：</label>
                            <div class="col-sm-2">
                            
                               <select class="form-control" name="type" onchange="selectWithDrawType(this.value)" id="withDrawType">
                                 
                              	</select>
                            </div>
                            <div class="col-sm-3">
                                <!--<span class="right_red_tip">备注：单笔提款限额<i id="limitMoney">?</i>万元</span>-->
                                <span class="right_red_tip" id="limitMoneyTip"></span>
                            </div>
                        </div>

                        <div class="form-group" id="accountSelectId" style="display: none;">
                        
                            <label class="col-sm-5 control-label">帐号选择：</label>
                            <div class="col-sm-3">
                          		<!-- 银行卡 -->
                                <select class="form-control" name="bankCardId" onchange="selectBankCard(this.value)"  id="bankCardSelect">

                                </select>
                                <!-- 支付宝 -->
                                <select class="form-control" name="bankCardId" onchange="selectAlipayAccount(this.value)"  id="alipayAccountSelect" >

                                </select>
                                <!-- 微信 -->
                                <select class="form-control" name="bankCardId" onchange="selectWeChatAccount(this.value)" id="weChatAccountSelect">
                                 
                                </select>
                                 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-5 control-label">提现金额：</label>
                            <div class="col-sm-2">
                                <input name="money" type="text" value="" class="form-control" id="curWithdrawMoney">
                            </div>
                            <div class="col-sm-3">
                                <input type="hidden" id="useableMoney" value="<?php echo $u['money']; ?>"><!--可提现余额-->
                                <button type="button" class="btn btn-primary" id="withdrawAllMoney">全部提现</button>
                            </div>
                             
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">交易密码：</label>
                            <div class="col-sm-3">
                                <input name="tradePassword" type="password" value="" class="form-control" placeholder="请输入交易密码" id="tradePassword">
                            </div>
							<?php if(empty($u['jiaoyimima']) || (($u['jiaoyimima'] instanceof \think\Collection || $u['jiaoyimima'] instanceof \think\Paginator ) && $u['jiaoyimima']->isEmpty())): ?>
                            <!--没有设置交易密码-->
                              <div class="col-sm-3">
                                <span class="form-control noBorder">
                                    <a class="text-blue text-underline" href="javascript:gotoSetTradePassword();">去开启交易密码</a>
                                </span>
                            </div><?php endif; ?>
                                                    </div>

                        <div class="form-group">
                            <label class="col-sm-5 control-label"></label>
                            <div class="col-sm-4">
                                <div id="errorTip" class="color_f00"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-3">
                                <button class="btn btn-w-m btn-blue" type="button" id="submitWithdrawFormBtn">立即提现</button>
                            </div>
                        </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" value="<?php if($u['real'] > '0'): ?>true<?php else: ?>false<?php endif; ?>" id="isRealNameAuthen"><!--是否实名认证-->
<!--<input type="hidden" value="" id="isBindWx">微信绑定状态（ 1：绑定，2：解绑,3：未绑定）-->
<!-- <input type="hidden" value="3" id="isFollowWx">是否关注微信公众号（1：已关注2：未关注，） -->

<input type="hidden" value="<?php if(empty($u['jiaoyimima']) || (($u['jiaoyimima'] instanceof \think\Collection || $u['jiaoyimima'] instanceof \think\Paginator ) && $u['jiaoyimima']->isEmpty())): ?>false<?php else: ?>true<?php endif; ?>" id="isOpenTradePaaword"><!--是否开启交易密码-->



<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
   var banklist="<?php echo U('home/moneylog/banklist'); ?>",txjl="<?php echo U('home/member/recharge'); ?>";
   var alixian=<?php echo C('alixian'); ?>,wxxian=<?php echo C('wxxian'); ?>,bankxian=<?php echo C('bankxian'); ?>;
</script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/gotoAnotherPage.js"></script>
<script src="/static/home/js/withdrawCash/withdrawCash.js?v=25"></script>


</body>
</html>
