<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:105:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\resetTradePasswordInit.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/loginBind.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">忘记交易密码</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="w-wrap">
    <div class="phoneSetting-warp">
        <p class="top-tip tip-ico-parent">您当前进行的是重置交易密码的操作，请先验证身份</p>
        <div class="setting-modules mt-30">
            <form action="" method="post">
                <div class="media cursor_pointer" id="verifyWayMedia">
                    <img class="align-self-center" src="/static/home/images/common/form/login_nber_icon.png">

                    <!--绑定了手机，不管有没有绑定邮箱，值为phone;只绑定了邮箱,值为mail -->
                                        <input type="hidden" value="phone" id="selectedVerifyWay">
                   	                    
                    <div class="media-body">
                    	                        <!--绑定了手机，不管有没有绑定邮箱，都默认手机验证 -->
                        <p class="d-flex clearfix">
                            <span class="left-txt">通过已绑定手机认证</span>
                            <span class="right-txt"><?php echo tfen($u['photo'],3,4); ?></span>
                        </p>
						                    </div>
                </div>
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/common/form/login_veri_icon.png">
                    <div class="media-body no-bg">
                        <!--
					                        绑定了手机，不管有没有绑定邮箱，都默认手机验证，隐藏邮箱验证码
					                        只绑定了邮箱，才使用邮箱验证，隐藏手机验证码
                        -->
                        <div class="d-flex clearfix d-none" id="phoneCodeBox">
                            <span class="form-group">
                                <input type="text" value="" class="form-control" placeholder="请输入手机验证码" id="phoneCode">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                            <a class="resend-code color-green" href="javascript:void(0);" id="getPhoneCode">发送验证码</a>
                        </div>
                        <div class="d-flex clearfix d-none" id="mailCodeBox">
                            <span class="form-group">
                                <input type="text" value="" class="form-control" placeholder="请输入邮箱验证码" id="mailCode">
                                <a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
                            </span>
                            <a class="resend-code color-green" href="javascript:void(0);" id="getMailCode">发送验证码</a>
                        </div>
                    </div>
                </div>
				<div id="neirong" style="display:none">
					<div class="media">
						<img class="align-self-center" src="/static/home/images/common/form/login_veri_icon.png">
						<div class="media-body no-bg">
							<div class="d-flex clearfix" id="content">
								<span class="form-group">
									<input type="text" value="" class="form-control" placeholder="请输入密码" id="newLoginPwd">
									<a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
								</span>
							</div>
						</div>
					</div>
					<div class="media">
						<img class="align-self-center" src="/static/home/images/common/form/login_veri_icon.png">
						<div class="media-body no-bg">
							<div class="d-flex clearfix" id="content">
								<span class="form-group">
									<input type="text" value="" class="form-control" placeholder="请输入密码" id="newLoginPwdAgain">
									<a class="delete" href="javascript:void(0);"><b class="w30"></b></a>
								</span>
							</div>
						</div>
					</div>
				</div>
            </form>
        </div>

        <a href="javascript:void(0);" class="btn btn-blue full-btn" id="submitResetTradePwdInitFormBtn">下一步</a>
    </div>

</main>

<!-- <input type="hidden" value="" id="username"> -->
<input type="hidden" value="<?php echo tfen($u['photo'],3,4); ?>" id="phone"><!--用户绑定的手机号-->
<input type="hidden" value="<?php echo tfen($u['email'],3,4); ?>" id="mail"><!--用户绑定的邮箱号-->

<script type="text/javascript">
	var ctxPath = {domain: "",domainapi: "",csrf:"660c07bb-2f57-4539-a87d-7808ca39a90e",staticDomain:""};
	var  jiaoyi="<?php echo U('home/api/jiaoyimima'); ?>",editurl="<?php echo U('home/member/editTran'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/passwordManage/forgotTradePassword/resetTradePwdInit.js?v=2767758"></script>
</body>
</html>