<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:72:"D:\wwwroot\127.0.0.1\public/../application/home\view\member\add_tel.html";i:1667656648;s:61:"D:\wwwroot\127.0.0.1\application\Home\view\layout\member.html";i:1667656648;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
        <li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li  class="active" ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>
    <!-- <li ><a class="J_menuItem" curhref="<?php echo U('home/member/qqbind'); ?>">QQ绑定</a></li>
    <li ><a class="J_menuItem" curhref="<?php echo U('home/member/wxbind'); ?>">微信绑定</a></li> -->
</ul>                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
						<?php if(empty($u['photo']) || (($u['photo'] instanceof \think\Collection || $u['photo'] instanceof \think\Paginator ) && $u['photo']->isEmpty())): ?>
                            
							<!-- 没绑定手机-->
							 <!--①、手机未绑定，并且没有绑定邮箱-->
                            <form class="form-horizontal p-xl" id="phoneSettingIndexForm">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">手机号码：</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" name="validNewPhone"  value="" placeholder="请输入要绑定的手机号" id="phone">
                                    </div>
                                </div>
                                
                                                                <div class="form-group">
                                    <label class="col-sm-3 control-label">登录密码：</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="password" name="password" value="" placeholder="请输入6-20位包含数字字母的登录密码" id="password">
                                    </div>
                                   	 设置后可以使用手机和登录密码进行帐号登录
                                </div>
                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">短信验证码：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="phoneCode">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-danger" id="getPhoneCodea" href="javascript:void(0);">获取验证码</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <div class="errorTip color_f00"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitPhoneSetIndexFormBtn">立即提交</a>
                                    </div>
                                </div>
                                
                            </form>
							<?php else: ?>
							 <!--③、手机已绑定-->
                            <div class="status-box">
                                <div class="status-box-icon pull-left"><i class="ico140"></i></div>
                                <div class="status-box-text">
                                    <h3 class="text-green">当前状态：已绑定手机（<?php echo tfen($u['photo'],3,4); ?>）</h3>
                                    <p class="color999">绑定手机可以用于安全身份验证、找回密码等重要操作</p>
                                    <p class="color999">如有问题，请联系客服咨询</p>
                                    <a class="btn btn-blue phone" href="<?php echo U('home/member/addpass'); ?>"><i class="phone-ico"></i>更换手机</a>
                                </div>
                            </div>
                            <div class="wram-tips">
                                <h5>温馨提示：</h5>
                                <ul>
                                    <li>1、您的手机绑定之后，即可享受手机登录、找回密码等服务，让您的网上购物体验更安全更便捷</li>
                                    <li>2、若您的手机可正常使用但无法接收到验证码短信，可能是由于通信网络异常造成的，请您稍后重新尝试操作。</li>
                                    <li>3、如果您的手机已经无法正常使用，请提供用户名，手机号，身份证号，点击联系平台
                                        <a class="text-blue text-underline" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo C('kefu'); ?>&Site=%E6%B7%BB%E5%8A%A0%E5%AE%A2%E6%9C%8DQQ%E8%B4%AD%E4%B9%B0%E7%BB%AD%E8%B4%B9%E4%B8%8D%E8%BF%B7%E8%B7%AF&Menu=yes" target="_blank">在线客服</a>
                                    </li>
                                </ul>
                            </div>
							<?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = {domain: "http://www.renrenxiaoka.com",_csrf:"e73ba083-c73f-4c4c-b2f2-9f51170855b7",staticDomain:"https://renrenxiaoka-resource.oss-cn-shenzhen.aliyuncs.com"};
</script>
<script src="/static/home/js/common/profileManage/common_profile.js"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/common/profileManage/baseProfile/baseProfile.js"></script>


</body>
</html>
