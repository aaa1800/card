<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:91:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\member\member_mx_log.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
								<div class="layui-inline layui-show-xs-block">
										<button class="layui-btn layui-btn-danger" id="delAll"><i class="layui-icon"></i>批量删除</button>
                                </div>
                        </div>
						 
                        <div class="layui-card-body ">
                            <table class="layui-table" id="mlist" lay-filter="mlist">
                                <thead>
                                    <tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
	layui.use(['table','form','laydate'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery,laydate = layui.laydate;
        tableIn = table.render({
            elem: '#mlist',
            url: "<?php echo U('simple/member/memberMxLog',['id'=>$id]); ?>",
            method: 'post',
			page:true,
			totalRow:true,
            cols: [[
			    {type: 'checkbox',width:60,minWidth:30},
				{field:'id',title:"ID", align: 'center', sort: true,},
			    {field:'type',title: '资金类型',align: 'center'},
				{field:'price',title: '变动金额',align: 'center'},
				{field:'money',title: '账户余额',align: 'center'},
				{field:'data',title: '备注信息',align: 'center'},
				{field:'addtime', align: 'center',title:'时间'}
            ]],
			limit:15,
			limits:[10,20,50,60]
        });		
		
		
		$("#delAll").click(function(){
            layer.confirm('确认要删除选中信息吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('mlist'); //test即为参数id设定的值
				console.log(checkStatus,table);
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo U('simple/member/mxDel'); ?>", {ids: ids}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        });
		
      });
    </script>
</html>