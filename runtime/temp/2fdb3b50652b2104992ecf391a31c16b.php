<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:81:"C:\wwwroot\hs.qulaida.top\public/../application/home\view\member\mobile\info.html";i:1638015884;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>
    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/minirefresh/minirefresh.min.css">
    <!--<link rel="stylesheet" href="https://renrenxiaoka-mobile-resource.oss-cn-shenzhen.aliyuncs.com/template/common/layer_mobile-v2.0/need/layer.css">-->
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css">
</head>
<body class="bg-gray setting-index-page">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="<?php echo U('home/member/index'); ?>">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">个人信息</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<div id="minirefresh" class="minirefresh-wrap">
    <div class="minirefresh-scroll bg-transparent">
        <main class="person-info-wrap w-wrap">
            <div class="setting-modules">
                <a class="media" href="javascript:void(0);">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_name_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">用户名</span>
                            <span class="float-right right-txt" id="nickName"></span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/AuthenInit'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_real_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">实名认证</span>
                            <span class="float-right right-txt" id="realNameStatus"></span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/tixianbank'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_acco_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">提现账号</span>
                            <span class="float-right right-txt" id="withdrawAccountStaus"></span>
                        </p>
                    </div>
                </a>
            </div>

            <div class="setting-modules">
                <a class="media" href="<?php echo U('home/member/addpass'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_phone_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">手机号</span>
                            <span class="float-right right-txt" id="phone"></span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/editemail'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_email_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">邮箱</span>
                            <span class="float-right right-txt" id="mail"></span>
                        </p>
                    </div>
                </a>
                <!--<a class="media" href="http://m.renrenxiaoka.com/member/profileManage/LoginBind/index.do">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_login_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">登录绑定</span>
                             <span class="float-right right-txt">去绑定 / 更换绑定</span> 
                        </p>
                    </div>
                </a>-->
                <!-- <a class="media" href="http://m.renrenxiaoka.com/member/profileManage/subscribeOfficialWeixinAccount/index.do">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_weixin_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">关注公众号</span>
                            <span class="float-right right-txt" id="followWXStatus"></span>
                        </p>
                    </div>
                </a> -->
            </div>

            <div class="setting-modules">
                <a class="media" href="<?php echo U('home/member/addpwd'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_pass_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">密码管理</span>
                            <span class="float-right right-txt">修改</span>
                        </p>
                    </div>
                </a>
                <a class="media" href="<?php echo U('home/member/addqq'); ?>">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_qq_icon.png">
                    <div class="media-body">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">联系QQ</span>
                            <span class="float-right right-txt" id="contactQq"><?php echo $u['qq']; ?></span>
                        </p>
                    </div>
                </a>
            </div>

            <div class="setting-modules">
                <a class="media" href="javascript:void(0);">
                    <img class="w40 align-self-center" src="/static/home/images/profileManage/personInfo/person_time_icon.png">
                    <div class="media-body no-bg">
                        <p class="clearfix">
                            <span class="float-left left-txt color3">注册时间</span>
                            <span class="float-right right-txt" id="regTime"></span>
                        </p>
                    </div>
                </a>
            </div>
        </main>
    </div>
</div>

<script>
	var ctxPath = {domain: "",domainapi: "",csrf:"a57418b9-d00e-474d-bba0-d0b0ac3b5b13",staticDomain:""};	
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/profileManage/index.js"></script>
</body>
</html>