<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:94:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\index\mobile\card_article.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="keywords" content="<?php echo C('keywords'); ?>">
    <meta http-equiv="description" content="<?php echo C('description'); ?>">
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/dropload/dropload.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/setting/setting.css?v=20181115170116">
	
</head>
<body class="bg-gray setting-faq-page">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);">常见问题</a>
    <a class="w44" href="/">
        <img src="/static/home/images/common/return_me_icon.png">
    </a>
</header>

<!--中间内容部分-->
<main class="w-wrap faq-list-wrap mt-44">
    <div class="modules-box">
    	<div class="list">
	     	<!-- <div class="modules-item">
	             <a class="media" href="javascript:void(0);">
	                 <div class="media-body">
	                     <p class="clearfix">
	                         <span class="float-left left-txt">1、支持回收的二手卡有哪些？</span>
	                     </p>
	                 </div>
	             </a>
	             <div class="media-content">
	                 人人销卡网暂时只针对网站公布回收的二手卡进行回收处理，目前支持回收的卡包括电商卡、加油卡、游戏卡、话费卡等50余种，如果没有您想要提交的卡，您可以联系客服，后期我们会会逐步的发布更多的卡类回收，详情请参考网站公示。
	             </div>
	         </div> -->   
    	</div>                  
    </div>
</main>

<script>
    var ctxPath ="<?php echo U('home/Gong/getFap'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>

<script src="/static/home/js/mobile/common/dropload/dropload.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>
<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/setting/faq/faq.js?v=20181116113235"></script>
</body>
</html>