<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:85:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\edit_pass.html";i:1616590069;s:72:"C:\wwwroot\shouka.paimaivip.net\application\Home\view\layout\member.html";i:1616590069;}*/ ?>
<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo C('title'); ?> - 信息管理</title>
    <meta name="keywords" content="<?php echo C('keywords'); ?>">
    <meta name="description" content="<?php echo C('description'); ?>">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
	

    <link href="/static/home/css/animate.css" rel="stylesheet">
    <link href="/static/home/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/static/home/css/common/customer.css" rel="stylesheet">
    <link href="/static/home/css/profileManage/profileManage.css" rel="stylesheet">

	
	<script>
	  var phoneSetting="<?php echo U('home/member/addTel'); ?>",realNameAuthenInit="<?php echo U('home/member/AuthenInit'); ?>",bankAccountIndex="<?php echo U('home/member/bankAccount'); ?>",passwordManageInit="<?php echo U('home/member/addpwd'); ?>",tradePasswordManage="<?php echo U('home/member/addpwd'); ?>",mailSettingInit="<?php echo U('home/member/editemail'); ?>",withdrawCashRecord="<?php echo U('home/member/recharge'); ?>",withdrawCash="<?php echo U('home/member/tixian'); ?>",accountStatement="<?php echo U('home/member/moneylog'); ?>",member="<?php echo U('home/member/maina'); ?>";
	  var regurl="<?php echo U('home/api/sends'); ?>";
	  var addTel="<?php echo U('home/member/addpass'); ?>";
	  var edittel="<?php echo U('home/member/addTel'); ?>";
	  var fa="<?php echo U('home/api/facode'); ?>";
	  var bindtel="<?php echo U('home/api/bindtel'); ?>";
	  var bindemail="<?php echo U('home/member/editemail'); ?>";
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                                <ul class="nav nav-tabs">
    <li    ><a class="J_menuItem" curhref="<?php echo U('home/member/userinfo'); ?>">基本资料</a></li>
    <li   ><a class="J_menuItem" curhref="<?php echo U('home/member/addTel'); ?>">手机设置</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/AuthenInit'); ?>">实名认证</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/bankAccount'); ?>">银行账号</a></li>-->
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/aliPay'); ?>">支付宝账号</a></li>
    <!--<li  ><a class="J_menuItem" curhref="<?php echo U('home/member/weixin'); ?>">微信帐号</a></li>-->
    <li class="active" ><a class="J_menuItem" curhref="<?php echo U('home/member/addpwd'); ?>">密码管理</a></li>
    <li  ><a class="J_menuItem" curhref="<?php echo U('home/member/editemail'); ?>">邮箱设置</a></li>

</ul>               <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
                            <!--已经绑定手机或邮箱，也设置了登录密码  修改登录密码页面-->
                            <form class="form-horizontal m-t" id="modifyLoginPasswordForm" action="">
                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                                                                <ul class="form-btn-group clearfix">
        <li class="pull-left ">
        <a class="btn" href="<?php echo U('home/member/addpwd'); ?>">
            <i class="ico24 blue-lock"></i>登录密码管理<b></b>
        </a>
    </li>
    <li class="pull-right selected">
        <a class="btn" href="<?php echo U('home/member/editPass'); ?>">
            <i class="ico24 blue-shield"></i>交易密码管理<b></b>
        </a>
    </li>
    </ul>                                    </div>
                                </div>
								<?php if(!(empty($u['jiaoyimima']) || (($u['jiaoyimima'] instanceof \think\Collection || $u['jiaoyimima'] instanceof \think\Paginator ) && $u['jiaoyimima']->isEmpty()))): if($cz == '10'): ?>
								  <div class="form-group m-t-lg">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="alert alert-warning yellow-tip-box">
                                            <i class="yellow-tip-ico"></i>您当前进行的是重置交易密码的操作，请先验证身份
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">验证方式：</label>
                                    <div class="col-sm-3">
                                        <!--验证方式有：手机（已经绑定的情况下）、邮箱（已经绑定的情况下）、原交易密码-->
                                        <!--请判断是否有绑定手机或邮箱（至少有一个绑定，否则不会显示该页面），绑定的情况下才显示该验证方式，否则不显示-->
                                        <select class="form-control" name="type" id="verifyWay" onchange="getCurVerifyWay(this.value)">
                                            <?php if(!(empty($u['photo']) || (($u['photo'] instanceof \think\Collection || $u['photo'] instanceof \think\Paginator ) && $u['photo']->isEmpty()))): ?><option value="phone" selected="selected">通过已绑定手机验证</option><?php endif; if(!(empty($u['email']) || (($u['email'] instanceof \think\Collection || $u['email'] instanceof \think\Paginator ) && $u['email']->isEmpty()))): ?><option value="email" >通过已绑定邮箱验证</option><?php endif; ?></select>
                                    </div>
                                </div>

                                <!--通过已绑定手机验证-->
                                <div id="viaPhoneVerify">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">已验证手机：</label>
                                        <div class="col-sm-3">
                                            <span class="form-control noBorder"><?php echo tfen($u['photo'],3,4); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <!--通过已绑定邮箱验证-->
                                <div id="viaMailVerify">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">已验证邮箱：</label>
                                        <div class="col-sm-3">
                                            <span class="form-control noBorder"><?php echo tfen($u['email'],3,4); ?></span>
                                        </div>
                                    </div>
                                </div>
								<div >
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">验证码：</label>
                                        <div class="col-sm-2">
                                            <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="mailCode">
                                        </div>
                                        <div class="col-sm-1">
                                            <a class="btn btn-danger" id="getMailCode" href="javascript:void(0);">获取验证码</a>
                                        </div>
                                    </div>
                                </div>

                               
								<div id="tran" style="display:none">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">新交易密码：</label>
                                        <div class="col-sm-3">
                                            <input type="password" name="pass" value="" class="form-control" placeholder="请输入新交易密码" id="oldTran">
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- <span class="text-red">
                                                若忘记原交易密码，请通过绑定的手机或邮箱接收验证码验证或<br>
                                                <a class="text-blue" href="http://wpa.qq.com/msgrd?v=3&amp;uin=4008889259&amp;site=qq&amp;menu=yes" target="_blank">联系客服</a>
                                            </span> -->
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-sm-4 control-label">确认密码：</label>
                                        <div class="col-sm-3">
                                            <input type="password" name="cvfff" value="" class="form-control" placeholder="请再次输入新交易密码" id="newTran">
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- <span class="text-red">
                                                若忘记原交易密码，请通过绑定的手机或邮箱接收验证码验证或<br>
                                                <a class="text-blue" href="http://wpa.qq.com/msgrd?v=3&amp;uin=4008889259&amp;site=qq&amp;menu=yes" target="_blank">联系客服</a>
                                            </span> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00" id="jiaoyi"></div>
                                    </div>
                                </div>
                                <div class="form-group" id="subid" style="display:none">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitResetTradePwdOneFormBtn">保存</a>
                                    </div>
                                </div>
								<?php else: ?>
								  <div class="status-box m-t-b-30">
                                <div class="status-box-icon pull-left"><i class="ico140 green-shield"></i></div>
                                <div class="status-box-text">
                                    <h3 class="text-green">当前状态：您的交易密码已开启</h3>
                                    <p class="color999">在使用余额等操作均需通过交易密码进行身份验证</p>
                                    <p class="color999">如有问题，请联系客服咨询</p>
                                    <a class="btn btn-blue phone" href="<?php echo U('home/member/editPass',['cz'=>10]); ?>"><i class="lock-ico"></i>重置密码</a>
                                </div>
                            </div><?php endif; else: ?>
								
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">交易密码：</label>
                                    <div class="col-sm-3">
                                        <input type="password" name="tradePassword" value="" class="form-control" placeholder="请设置交易密码" id="tradePwd">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">手机/邮箱：</label>
                                    <div class="col-sm-3">
                                        <!--如果用户同时绑定了手机或者邮箱，那可以下拉选择使用手机或者邮箱进行接验证码验证,默认是使用手机-->
                                        <select class="form-control" name="type" id="verifyWay">
										    
                                            <?php if(!(empty($u['photo']) || (($u['photo'] instanceof \think\Collection || $u['photo'] instanceof \think\Paginator ) && $u['photo']->isEmpty()))): ?><option value="phone" selected="selected"><?php echo tfen($u['photo'],3,4); ?></option><?php endif; if(!(empty($u['email']) || (($u['email'] instanceof \think\Collection || $u['email'] instanceof \think\Paginator ) && $u['email']->isEmpty()))): ?><option value="email" ><?php echo tfen($u['email'],3,6); ?></option><?php endif; ?> </select>
                                        <!-- <input type="hidden" value="" name="username" id="username"> -->
                                        <input type="hidden" name="_csrf" value="c5a75546-1af4-4d96-9d98-140158b19c85">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">短信/邮箱验证码：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" name="code" value="" placeholder="请输入验证码" id="code">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-danger" id="getCode" href="javascript:void(0);">获取验证码</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-4">
                                        <div class="errorTip color_f00"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-4">
                                        <a class="btn btn-w-m btn-blue" href="javascript:void(0);" id="submitSetTradePwdFormBtn">保存</a>
                                    </div>
                                </div>
								<?php endif; ?>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/plugins/layer/laydate/laydate.js"></script>

<script>
    var ctxPath = "<?php echo U('home/api/jiaoyimima'); ?>",editTran="<?php echo U('home/member/editTran'); ?>";
</script>
<script src="/static/home/js/common/profileManage/common_profile.js?v=123"></script>
<script src="/static/home/js/common/conttab_child.js"></script>
<script src="/static/home/js/profileManage/passwordManage/setTradePwd.js?v=2017018"></script>
<script src="/static/home/js/profileManage/passwordManage/resetTradePwd.js?v=72"></script>




</body>
</html>
