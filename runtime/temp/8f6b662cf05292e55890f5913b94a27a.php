<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:91:"C:\wwwroot\shouka.paimaivip.net\public/../application/home\view\member\mobile\editPass.html";i:1616590069;}*/ ?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
     <meta name="keyword" content="<?php echo C('keywords'); ?>" />
	<meta name="description" content="<?php echo C('description'); ?>" />
    <title><?php echo C('title'); ?></title>

    <link rel="stylesheet" href="/static/home/css/mobile/bootstrap.min.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/layer_mobile-v2.0/need/layer.css">
    <link rel="stylesheet" href="/static/home/css/mobile/common/base.css">
    <link rel="stylesheet" href="/static/home/css/mobile/profileManage/loginBind.css">
</head>
<body class="bg-gray">
<!--固定头部-->
<header class="navbar fixed-top bg-blue w-wrap">
    <a class="return" href="javascript:history.go(-1);">
        <img src="/static/home/images/common/white_return_btn.png">
    </a>
    <a class="title" href="javascript:void(0);"><?php echo $str; ?>密码</a>
    <a href="javascript:void(0);"></a>
</header>

<!--中间内容部分-->
<main class="w-wrap">
    <div class="phoneSetting-warp">
        <div class="setting-modules">
            <form action="" method="post">
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/common/form/login_pass_icon.png">
                    <div class="media-body no-bg">
                        <div class="form-group mr-0">
                            <input type="password" value="" class="form-control pr-64" id="oldLoginPwd" placeholder="请输入原<?php echo $str; ?>密码">
                            <a class="delete right75" href="javascript:void(0);"><b class="w30"></b></a>
                            <a class="isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/profileManage/passwordManage/new_pass_icon.png">
                    <div class="media-body no-bg">
                        <div class="form-group mr-0">
                            <input type="password" value="" class="form-control pr-64" id="newLoginPwd" placeholder="请输入新<?php echo $str; ?>密码">
                            <a class="delete right75" href="javascript:void(0);"><b class="w30"></b></a>
                            <a class="isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="align-self-center" src="/static/home/images/profileManage/passwordManage/new_pass_icon.png">
                    <div class="media-body no-bg">
                        <div class="form-group mr-0">
                            <input type="password" value="" class="form-control pr-64" id="newLoginPwdAgain" placeholder="再次确认新密码">
                            <a class="delete right75" href="javascript:void(0);"><b class="w30"></b></a>
                            <a class="isShowPass hidePassword" href="javascript:void(0);"><b class="w44"></b></a>
                        </div>
                    </div>
                </div>
				 <div class="media">
                    <img class="align-self-center" src="/static/home/images/profileManage/passwordManage/new_pass_icon.png">
                    <div class="media-body no-bg">
                        <div class="form-group mr-0">
                            <input type="text" value="" class="form-control pr-64" id="newcode" placeholder="输入验证码" style="width: 50%;float: left;">
                                        <img onclick="this.src='<?php echo captcha_src(); ?>'" src="<?php echo captcha_src(); ?>" alt="验证码" class="codeImg" style="width: 50%;height: 32px;" title="点击图片刷新验证码">
                        </div>
                    </div>
                </div>
				<input name="cz" value="<?php echo $cz; ?>" type="hidden">
            </form>
        </div>

        <div class="clearfix">
            <a class="float-right forgot-password" href="<?php if($cz == '1'): ?><?php echo U('home/login/retrieve'); else: ?><?php echo U('home/member/resetTradePasswordInit'); endif; ?>">忘记原<?php echo $str; ?>密码</a>
        </div>
        <a href="javascript:void(0);" class="btn btn-blue full-btn mt-0" id="submitModifyLoginPwdFormBtn">保存</a>
    </div>
</main>
<script>
	var ctxPath = {domain: "",domainapi: "",csrf:"",staticDomain:""};	
	  var editu="<?php echo U('home/member/editPass'); ?>";
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/static/home/js/mobile/common/jquery-1.12.3.min.js"></script>
<script src="/static/home/js/mobile/bootstrap.min.js"></script>
<script src="/static/home/js/mobile/common/layer_mobile-v2.0/layer.js"></script>

<script src="/static/home/js/mobile/common/ajaxUtils.js"></script>
<script src="/static/home/js/mobile/common/formVerify.js"></script>
<script src="/static/home/js/mobile/profileManage/passwordManage/loginPassword/alsoSetPasswordInit.js?v=115"></script>
</body>
</html>