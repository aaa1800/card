<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\system\edit_news.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
		<form class="layui-form" action="">
		  <div class="layui-form-item">
			<label class="layui-form-label">标题：</label>
			<div class="layui-input-block">
			  <input type="text" name="title" style="width:50%" lay-verify="title" autocomplete="off" value="<?php echo $u['title']; ?>" placeholder="请输入标题" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">文章关键字：</label>
			<div class="layui-input-block">
			  <textarea  name="keyword" style="width:670px;height:100px" ><?php echo $u['keyword']; ?></textarea>
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">类型：</label>
			<div class="layui-input-block" style="width:20%">
			 <?php echo Nstate($u['type']); ?>
			</div>
		  </div>
		   <div class="layui-form-item layui-form-text">
			<label class="layui-form-label">编辑器</label>
			<div class="layui-input-block">
			  <textarea class="layui-textarea layui-hide" name="contents" lay-verify="content" id="LAY_demo_editor"><?php echo $u['contents']; ?></textarea>
			</div>
		  </div>
		  <div class="layui-form-item">
			<div class="layui-input-block">
			<input type="hidden" name="id" value="<?php echo $u['id']; ?>">
			  <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
			  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		  </div>
		</form>
	</div>
</div>
	</div>
</div>
	</div>

    </body>
<script>

layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form,layer = layui.layer,layedit = layui.layedit
  ,laydate = layui.laydate;
   layedit.set({
		  uploadImage: {
			url: "<?php echo U('simple/system/tdieupload'); ?>" //接口url
		  }
	});
  //创建一个编辑器
  var editIndex = layedit.build('LAY_demo_editor');
 

  //监听提交
  form.on('submit(demo1)', function(data){
    data.field.contents=layedit.getContent(editIndex);
  $.post('<?php echo U("simple/system/editNews"); ?>',data.field,function(e){
          if(e.code==1){
					layer.alert("更新成功", {icon: 6},function() {
						xadmin.close();
						xadmin.father_reload();
					});
				}else{
				 layer.alert("更新失败", {icon: 5});
				}
  })
    return false;
  });

});
</script>
</html>