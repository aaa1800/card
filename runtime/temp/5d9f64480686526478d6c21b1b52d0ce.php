<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\wwwroot\yhs.haochiyidian.top\public/../application/simple\view\admin\admin_rule.html";i:1616590069;s:73:"C:\wwwroot\yhs.haochiyidian.top\application\Simple\view\public\heard.html";i:1623503162;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="/">首页</a>
            <a href="">系统管理</a>
            <a>
              <cite>权限管理</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  onclick="xadmin.open('新增权限','<?php echo U('simple/Admin/ruleAdd'); ?>')" ><i class="layui-icon"></i>新增权限</button>  <button class="layui-btn layui-btn-danger" id="delAll"><i class="layui-icon"></i>批量删除</button>
                                </div>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form" id="list" lay-filter="list" >
                             
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
<script type="text/html" id="auth">
    <input type="checkbox" name="authopen" value="{{d.id}}" lay-skin="switch" lay-text="是|否" lay-filter="authopen" {{ d.authopen == 0 ? 'checked' : '' }}>
</script>
<script type="text/html" id="status">
    <input type="checkbox" name="menustatus" value="{{d.id}}" lay-skin="switch" lay-text="显示|隐藏" lay-filter="menustatus" {{ d.menustatus == 1 ? 'checked' : '' }}>
</script>
<script type="text/html" id="order">
    <input name="{{d.id}}" data-id="{{d.id}}" class="list_order layui-input" value=" {{d.sort}}" size="10"/>
</script>
<script type="text/html" id="icon">
    <span class="icon iconfont"><i class="layui-icon {{d.icon}}" ></i></span>
</script>
<script type="text/html" id="action">
    <a onclick="xadmin.open('编辑权限','<?php echo U('simple/Admin/ruleEdit'); ?>?id={{d.id}}')" class="layui-btn layui-btn-xs">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
     layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery;
        tableIn = table.render({
            elem: '#list',
            url: "<?php echo U('simple/admin/adminRule'); ?>",
            method: 'post',
            cols: [[
			    {checkbox:true,align: 'center',width:80},
                {field: 'id', title: 'ID',width:80},
                {field: 'icon', align: 'center',title: '图标', templet: '#icon',width:100},
                {field: 'ltitle', title: '权限名称',width:200},
                {field: 'href', title: '控制器/方法',width:300  },
                {field: 'authopen',align: 'center', title: '是否验证权限',toolbar: '#auth',width:200},
                {field: 'menustatus',align: 'center',title: '菜单状态', toolbar: '#status',width:200},
                {field: 'sort',align: 'center', title: '排序',  templet: '#order',width:100},
                {align: 'center', toolbar: '#action',width:353}
            ]]
        });
        form.on('switch(authopen)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = this.value;
            var authopen = obj.elem.checked===true?0:1;
            $.post('<?php echo U("simple/admin/ruleTz"); ?>',{'id':id,'authopen':authopen},function (res) {
                layer.close(loading);
                if (res.status==1) {
                    tableIn.reload();
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                    return false;
                }
            })
        });
        form.on('switch(menustatus)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = this.value;
            var menustatus = obj.elem.checked===true?1:0;
            $.post('<?php echo U("simple/admin/ruleState"); ?>',{'id':id,'menustatus':menustatus},function (res) {
                layer.close(loading);
                if (res.status==1) {
                    tableIn.reload();
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                    return false;
                }
            })
        });
        table.on('tool(list)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                layer.confirm('您确定要删除该记录吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo U('simple/admin/ruleDel'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
            }
        });
        $('body').on('blur','.list_order',function() {
           var id = $(this).attr('data-id');
           var sort = $(this).val();
           $.post('<?php echo U("simple/admin/ruleOrder"); ?>',{id:id,sort:sort},function(res){
                if(res.code==1){
                    layer.msg(res.msg,{time:1000,icon:1},function(){
                        location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                }
           })
        })
		$("#delAll").click(function(){
            layer.confirm('确认要删除选中信息吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('list'); //test即为参数id设定的值
				console.log(checkStatus,table);
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo U('simple/admin/ruleDel'); ?>", {ids: ids}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        })
    })
	
</script>
</html>