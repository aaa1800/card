<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:73:"D:\wwwroot\127.0.0.1\public/../application/simple\view\system\alipay.html";i:1667656648;s:62:"D:\wwwroot\127.0.0.1\application\Simple\view\public\heard.html";i:1667656648;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>后台登录-X-admin2.2</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="/static/simple/css/font.css">
        <link rel="stylesheet" href="/static/simple/css/xadmin.css">
        <script src="/static/simple/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/simple/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      
    </head>
    <body>
	 <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">系统设置</a>
            <a>
              <cite>支付宝设置</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
              <div class="layui-card">
				<div class="layui-card-body ">
					<form class="layui-form" action="">
					  <div class="layui-form-item">
						<label class="layui-form-label">支付宝PID：</label>
						<div class="layui-input-block">
						  <input type="text" name="alipid" style="width:50%" value="<?php echo C('alipid'); ?>" lay-verify="title" autocomplete="off" placeholder="请PID" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">开发者私钥：</label>
						<div class="layui-input-block" style="width:50%">
						<textarea class="layui-textarea" name="alimach"><?php echo C('alimach'); ?></textarea>
						  
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">支付宝公钥：</label>
						<div class="layui-input-block" style="width:50%">
						<textarea class="layui-textarea" name="alikey"><?php echo C('alikey'); ?></textarea>
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">单笔最高限额：</label>
						<div class="layui-input-block">
						  <input type="number" name="alixian" value="<?php echo C('alixian'); ?>" style="width:50%" lay-verify="required" placeholder="请输入金额" autocomplete="off" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">银行单笔最高限额：</label>
						<div class="layui-input-block">
						  <input type="number" name="bankxian" value="<?php echo C('bankxian'); ?>" style="width:50%" lay-verify="required" placeholder="请输入金额" autocomplete="off" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">自动汇款开关：</label>
						<div class="layui-input-block">
						  <input type="checkbox" <?php if(C('alitxopen') == 'on'): ?>checked=""<?php endif; ?> name="alitxopen" lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF">
						</div>
					  </div>
					  
					  
					  <div class="layui-form-item">
						<div class="layui-input-block">
						  <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
						  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
						</div>
					  </div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</body>
<script>
layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
  
  
  //监听提交
  form.on('submit(demo1)', function(data){
    var loading =layer.load(3, {shade: [0.1,'#fff']});
    $.post("<?php echo U('simple/system/alipay'); ?>",data.field,function(res){
	        layer.close(loading);
			if (res.code==1) {
				layer.msg(res.msg,{time:1000,icon:1});
			}else{
				layer.msg(res.msg,{time:1000,icon:2});
			}
	})
    return false;
  });
 

  
  
});
</script>
</html>