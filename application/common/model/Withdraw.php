<?php
namespace app\common\Model;
use think\Model;

class Withdraw extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'shtime'  =>  'timestamp',
		'addtime'  =>  'timestamp'
    ];
	protected $insert = ['addtime'];
	protected function setAddtimeAttr()
    {
        return time();
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(!empty($data['st'])){
			$map['addtime']=['between time',[$data['st'],empty($data['et'])?date("Y-m-d H:i:s"):$data['et']]];
		}
		if(!empty($data['name']) && !empty($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}else{
			$map['uid']=['gt',0];
		}
		$ok=$this::with('profile')->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		$map['status']=1;
		$ok['money']=$this->where($map)->sum("money");
		return $ok;
	}
	/*
	前台receivable
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$des=$data['order'][0]['dir'];
		$ordd=$data['columns'][$data['order'][0]['column']]['data'];
		$str=$ordd." ".$des;
		$res=[];
		$search=$data['search']['value'];
		if(!empty($search)){
			$map['zh|order|money']=['like',"%".$search."%"];
		}
		$ok=$this::with('profile')->where($map)->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		return $ok;
	}
	
	public function profile()
    {
        return $this->hasOne('User','id','uid')->bind('user,username');
    }
	protected function getTypeAttr($val)
    {
        return paybank($val);
    }
	public function flist($da){
		$psize=$da['limit'];
		$page=$da['page'];
	    $user=new User;
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['uid']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['uid']=$user->where(['name'=>$da['user']])->value("id");
		}
		$list=$this->field("zhname,count(id) as cid,sum(money) as money,sum(price) as su")->where($map)->group("zhname")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		return (new Order)->redata($list);
 }
}