<?php
namespace app\common\model;

use think\Model;

class MoneyOrder extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	protected function getTypeAttr($val)
    {
		$r=["wxpay"=>"微信","swxpay"=>"微信商户",'alipay'=>"支付宝"];
        return $r[$val];
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		$map['uid']=$data['id'];
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		return $ok;
	}
	public function getSelect(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(isset($data['name']) && isset($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}else{
			$map['uid']=['gt',0];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		$map['status']=1;
		$ok['money']=$this->where($map)->sum("ysk_order");
		return $ok;
	}
	/*
	前台
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$des=$data['order'][0]['dir'];
		$str=$data['columns'][$data['order'][0]['column']]['data']." ".$des;
		$res=[];
		$ok=$this->where($map)->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();	
		return $ok;
	}
	
	public function recharge($order,$money,$type){
		$this->where(['addtime'=>['lt',time()-300],'status'=>0])->update(['status'=>2]);
		switch($type){
				case "swxpay":
					$map['uid']=session("user_id");
					$map['out_trade_no']=$order;
					$map['type']=$type;
					$map['money']=$money;
					$map['ysk_order']= "";
					$map['receivable']=0;
					$orjiao=$money;
					$url="";
				break;
				case "wxpay":
				    $jin=$this->generateJin($money,$type);
					$orjiao= $type=='swxpay' ? $money : $jin ? $jin : 0;
					if($orjiao==0){return ['code'=>-1,'msg'=>'系统繁忙,请稍后再试'];break;}
					$ewmurl=(new Userpay)->generate($orjiao,$type);
					if($ewmurl===false){return ['code'=>-1,'msg'=>'收款账号资源不足，请稍后再试'];break;}
					$map['uid']=session("user_id");
					$map['out_trade_no']=$order;
					$map['type']=$type;
					$map['money']=$money;
					$map['ysk_order']= $orjiao;
					$map['receivable']=$ewmurl['id'];
					$url=$ewmurl['url'];
				break;
			}
			self::create($map);
			return ['code'=>1,'img'=>$url,'money'=>$orjiao];
	}
	
	//生成金额
	private function generateJin($money,$type){
		$money=sprintf("%.2f",$money);
		$li=$this->where(['status'=>0,'ysk_order'=>$money,'type'=>$type])->find();
		if($li){
			$res=$this->where(['ysk_order'=>['between',[$money,$money+1]],'status'=>0,'type'=>$type])->select();
			$ar=[];
			if($res){
				foreach($res as $k=>$v){
					$ar[]=$v['ysk_order'];
				}
				for($i=0;$i<98;$i++){
					$arr[]=sprintf("%.2f",$money+0.01*$i);
				}
				$rt=array_diff($arr,$ar);
				if(count($rt)==0){
					return false;
				}else{
				   return min($rt);
				}
			}else{
				return $money;
			}
		}else{
			return $money;
		}
	}
	
}