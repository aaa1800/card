<?php
/**
 * Created by PhpStorm.
 * User: 微乐网
 * Date: 2018/12/19
 * Time: 13:28
 */

namespace app\common\model;

use think\Request;
use think\Model;

class Api extends Model{
	Protected $autoCheckFields = false;
	private $ukey;
	private $pkey;
	
	public function initt($id,$uid){
		$ul=(new Operator)->where(['id'=>$id])->value("des");
		$upi=unserialize($ul);
		$this->ukey=$upi['pay_key'];
		$this->pkey=(new User)->where(['id'=>$uid])->value("key");
	}
  //软件返回
    public function sendAuto($or,$da){
      $this->initt($or['type'],$or['uid']);
      $sign=md5("order={$da['order']}&money={$da['money']}&settle={$da['settle']}&cardno={$da['cardno']}&key=".$this->ukey);
      $da['status']=str_to_utf8( $da['status']);
	  $order=new Order;
		if($da['status']=="ok"){
			if($sign==$da['sign']){
                  $order->isokorder($or['id'],$da['settle']);//加款并更新订单
                  $msg=['code'=>1,'msg'=>"处理成功",'ext_param'=>$or['custom'],'orderno'=>$or['orderno'],'money'=>$da['settle'],'amt'=>$or['amount']];
				 }else{
                  echo "SIGNERR";
                }
		 }else{
			 $order->save(['state'=>2,'remarks'=>$da['status']],['id'=>$or['id']]);
			 $msg=['code'=>102,'msg'=>$da['status']];
		 }
		 if(!empty($or['notify'])){
			 $msg['sign']=md5Verify($msg,"",$this->pkey);
			 vpost($or['notify'],$msg);
		 }
		 echo "OK";
		 exit;
    }
  //end软件返回
	public function sendsht($or,$da){
		$this->initt($or['type'],$or['uid']);
		//logt("this");
		//logt($da);
		$sign=md5("ret_code=".$da['ret_code']."&agent_id=".$da['agent_id']."&bill_id=".$da['bill_id']."&huisu_bill_id=".$da['huisu_bill_id']."&bill_status=".$da['bill_status']."&card_real_price=".$da['card_real_price']."&card_settle_amt=".$da['card_settle_amt']."|||".$this->ukey);
		$order=new Order;
		if($da['ret_code']==0){
			if($sign==$da['sign']){
				 switch($da['bill_status']){
					 case 0:
					  $msg=['code'=>0,'msg'=>"待处理"];
					 break;
					 case 1:
					  $msg=['code'=>2,'msg'=>"处理中"];
					 break;
					 case 2:
					  $order->isokorder($or['id'],$da['card_real_price']);//加款并更新订单
					  $msg=['code'=>1,'msg'=>"处理成功",'ext_param'=>$or['custom'],'orderno'=>$or['orderno'],'money'=>$da['card_real_price'],'amt'=>$or['amount']];
					 break;
					 default:
					  $order->save(['state'=>2,'remarks'=>shcerr($da['ret_msg'])],['id'=>$or['id']]);
					  $msg=['code'=>3,'msg'=>shcerr($da['ret_msg'])];
				 }
			}else{
				echo "ERROR";
			}
		 }else{
			 $order->save(['state'=>2,'remarks'=>"创单失败"],['id'=>$or['id']]);
			 $msg=['code'=>102,'msg'=>"创单失败"];
		 }
		 if(!empty($or['notify'])){
			 $msg['sign']=md5Verify($msg,"",$this->pkey);
			 vpost($or['notify'],$msg);
		 }
		 echo "OK";
		 exit;
	}
	public function sendbl($or,$da){
		$this->initt($or['type'],$or['uid']);
		$siok=md5Verify($da,$da['Sign'],$this->ukey);
		$order=new Order;
		if($siok){
			 switch($da['ReturnCode']){
				 case 1:
				 case 2:
				  $order->isokorder($or['id'],$da['RealAmount'],isset($da['ReturnMsg'])?$da['ReturnMsg']:"",$da['SettlementAmt']);//加款并更新订单
				  $jin=$order->where(['id'=>$or['id']])->value("amount");
				  $msg=['code'=>1,'msg'=>"处理成功",'ext_param'=>$or['custom'],'orderno'=>$or['orderno'],'money'=>$da['RealAmount'],'amt'=>$jin];
				 break;
				 default:
				  $order->save(['state'=>2,'remarks'=>$da['ReturnMsg'],'update_time'=>date("Y-m-d H:i:s")],['id'=>$or['id']]);
				  $msg=['code'=>3,'msg'=>$da['ReturnMsg']];
			 }
			 if(!empty($or['notify'])){
				 $msg['sign']=md5Verify($msg,"",$this->pkey);
				 vpost($or['notify'],$msg);
			 }
		}
			echo "OK";
			exit;
	}
	/////速销卡
	public function supay($or,$da){
		logt($da);
		$this->initt($or['type'],$or['uid']);
		$order=new Order;
		$stre=$da['customerId']."&".$da['orderId']."&".$da['systemOrderId']."&".$da['productCode']."&".$da['status']."&".$da['amount']."&".$or['card_no']."&".$or['card_key']."&".$da['successAmount']."&".$da['actualAmount']."&".$da['code']."&".$this->ukey;
		$sign=md5($stre);
		if($sign==$da['sign']){
			 switch($da['status']){
				 case 2:
				  $order->isokorder($or['id'],$da['realPrice'],isset($da['memo'])?$da['memo']:"",$da['actualAmount']);//加款并更新订单
				  $jin=$order->where(['id'=>$or['id']])->value("amount");
				  $msg=['code'=>1,'msg'=>"处理成功",'ext_param'=>$or['custom'],'orderno'=>$or['orderno'],'money'=>$da['realPrice'],'amt'=>$jin];
				 break;
				 default:
				  $order->save(['state'=>2,'remarks'=>fanhui($da['code']),'update_time'=>date("Y-m-d H:i:s")],['id'=>$or['id']]);
				  $msg=['code'=>3,'msg'=>fanhui($da['code'])];
			 }
			 if(!empty($or['notify'])){
				 $msg['sign']=md5Verify($msg,"",$this->pkey);
				 vpost($or['notify'],$msg);
			 }
		}
			echo "Y";
			exit;
	}
	
	/////一家
	public function yipay($or,$da){
		$da['Msg']=unicodeDecode($da['Msg']);
		$this->initt($or['type'],$or['uid']);
		$order=new Order;
		$stre="Code=".(int)$da['Code']."&Msg={$da['Msg']}&RealValue=".sprintf("%.4f",$da['RealValue'])."&UserIncome=".sprintf("%.4f",$da['UserIncome'])."&UserOrderId={$da['UserOrderId']}&key={$this->ukey}";
		$sign=strtoupper(md5($stre));
		if($sign==$da['Sing']){
			 switch($da['Code']){
				 case 1:
				  $order->isokorder($or['id'],$da['RealValue'],$da['Msg'],$da['UserIncome']);//加款并更新订单
				  $jin=$order->where(['id'=>$or['id']])->value("amount");
				  $msg=['code'=>1,'msg'=>"处理成功",'ext_param'=>$or['custom'],'orderno'=>$or['orderno'],'money'=>$da['RealValue'],'amt'=>$jin];
				 break;
				 default:
				  $order->save(['state'=>2,'remarks'=>$da['Msg'],'update_time'=>date("Y-m-d H:i:s")],['id'=>$or['id']]);
				  $msg=['code'=>3,'msg'=>$da['Msg']];
			 }
			 if(!empty($or['notify'])){
				 $msg['sign']=md5Verify($msg,"",$this->pkey);
				 vpost($or['notify'],$msg);
			 }
		}
			echo "OK";
			exit;
	}
	
	
	
}