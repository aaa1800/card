<?php
namespace app\common\Model;
use think\Model;


class User extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
		'old_login_time'=>'timestamp',
		'realtime'=>'timestamp'
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	protected static function init()
    {
		self::afterInsert(function ($u){
//			self::addrate($u->id);
	    });
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(isset($data['name'])){
			if(empty($data['name'])){
				$map['id']=['gt',0];
			}else{
			  $map['user|id|photo|email']=$data['name'];
			}
		}else{
			$map['id']=['gt',0];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['tuig']=$this->where(['assets'=>$v['id']])->count();
		}
		//$ok['total']=(new User)->sum("money");
		return $ok;
	}
	
	public function hasMan(){
		return $this->hasMany("Userbank",'uid');
	}
	public function xiaji(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$des=$data['order'][0]['dir'];
		$str=$data['columns'][$data['order'][0]['column']]['data']." ".$des;
		$ok= $this::with('done')->where(['assets'=>session("user_id")])->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['photo']=phyin($v['photo']);
		}
		return $ok;
	}
	//写入费率
   public static function addrate($uid){
	   $res=(new Channel)->select();
	   $arr=array();
	   foreach($res as $k=>$v){
		   $arr[$v['fiel']]=$v['payrate']."|".$v['start'];
	   }
	   $arr['uid']=$uid;
	   $ok=(new MemberRate)->where(['uid'=>$uid])->find();
	   if(!$ok){
	     (new MemberRate)->save($arr);
	   }

   }
}