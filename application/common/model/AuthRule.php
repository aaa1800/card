<?php
namespace app\common\model;

use think\Model;

class AuthRule extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
        'addtime'  =>  'timestamp',
		
    ];
	protected $insert = ['addtime'];  
	
	protected function setAddtimeAttr()
    {
        return time();
    }
}