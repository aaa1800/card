<?php
namespace app\common\Model;
use think\Model;

class Domain extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
		'stoptime'  =>  'timestamp',
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(!empty($data['st'])){
			$map['addtime']=['between time',[$data['st'],empty($data['et'])?date("Y-m-d H:i:s"):$data['et']]];
		}
		if(!empty($data['name']) && !empty($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}else{
			$map['uid']=['gt',0];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->each(function($item, $key){
			$st=strtotime($item->stoptime);
			$c=$st-time();
			$h=$c/(60*60);
			$item->timeed=$c>0?floor($h/24):0;
			$item->hour=$c>0?floor($h%24):0;
			$item->uurl="http://".$_SERVER['SERVER_NAME'].U('api/weixin/index',['id'=>$item->id]);
		})->toArray();
		$ok['money']=$this->count();
		$ok['succ']=$this->where(['stoptime'=>['>= time',time()]])->count();
		return $ok;
	}
	/*
	前台receivable
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$des=$data['order'][0]['dir'];
		$ordd=$data['columns'][$data['order'][0]['column']]['data'];
		$str=$ordd." ".$des;
		$search=$data['search']['value'];
		if(!empty($search)){
			$map['name|id']=['like',"%".$search."%"];
		}
		$ok=$this->where($map)->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->each(function($item, $key){
			$st=strtotime($item->stoptime);
			$c=$st-time();
			$h=$c/(60*60);
			$item->timeed=$c>0?floor($h/24):0;
			$item->hour=$c>0?floor($h%24):0;
			$item->uurl=$item->id;
		})->toArray();
		return $ok;
	}
	
	
}