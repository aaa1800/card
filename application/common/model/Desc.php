<?php
namespace app\common\model;

use think\Model;


class Desc extends Model
{    
    protected static $key;
    protected static $iv;
    
    public function __construct(){
			  parent::__construct();
			  self::$key = C('secretKey');
			  self::$iv  = C('secretmk');
	}
	
	public static function getcon($type,$data=0){
		    new self();
			$arr=['type'=>$type,'data'=>$data];
			$str=self::encrypt($arr);
			$result=fenpost(C('tourl')."fenzhan.html",['da'=>$str]);
			if($result['code']==1){
				if(is_array($result['msg']['data'])){
					$data=$result['msg']['data'];
				}else{
				  $data=self::decrypt($result['msg']['data']);
				}
				return $data;
			}else{
				return false;
			}
		}
    /**
     * 3des加密
     * 
     * @param    array       $input            明文
     * @return string
     */
    private static function encrypt($str)
    {
		$str=createLinkstring($str);
        $input = self::padding($str);
        $key = base64_decode(self::$key);
        $td = mcrypt_module_open( MCRYPT_3DES, '', MCRYPT_MODE_CBC, '');
        //使用MCRYPT_3DES算法,cbc模式
        mcrypt_generic_init($td, $key, self::$iv);
        //初始处理
        $data = mcrypt_generic($td, $input);
        //加密
        mcrypt_generic_deinit($td);
        //结束
        mcrypt_module_close($td);
        $data = self::removeBR(base64_encode($data));
        return $data;
    }

    /**
     * 3des解密
     * 
     * @param    string        $encrypted            密文
     * @return array
     */
   private static function decrypt($encrypted=1)
    {
        $encrypted = base64_decode($encrypted);
        $key = base64_decode(self::$key);
        $td = mcrypt_module_open( MCRYPT_3DES,'',MCRYPT_MODE_CBC,'');
        //使用MCRYPT_3DES算法,cbc模式
        mcrypt_generic_init($td, $key, self::$iv);
        //初始处理
        $decrypted = mdecrypt_generic($td, $encrypted);
        //解密
        mcrypt_generic_deinit($td);
        //结束
        mcrypt_module_close($td);
        $decrypted = self::removePadding($decrypted);
		$arr=[];
		$res=explode("&",$decrypted);
		foreach($res as $v){
			$ab=explode("=",$v);
			$arr[$ab[0]]=$ab[1];
		}
        return $arr;
    }

    /**
     * 填充密码，填充至8的倍数
     * 
     * @param    string        $str            原始字串
     * @return string
     */
    private static function padding($str){
        $len = 8 - strlen($str) % 8;
        for($i = 0; $i < $len; $i++){
            $str .= chr(0);
        }
        return $str ;
    }

    /**
     * 删除填充符
     * 
     * @param    string        $str            原始字串
     * @return string
     */
    private static function removePadding( $str )
    {
        $len = strlen( $str );
        $newstr = "";
        $str = str_split($str);
        for ($i = 0; $i < $len; $i++ )
        {
            if ($str[$i] != chr( 0 ))
            {
                $newstr .= $str[$i];
            }
        }
        return $newstr;
    }
    
    
    /**
     * 删除回车和换行
     * 
     * @param    string        $str            原始字串
     * @return string
     */
    private static function removeBR($str) {
        $len = strlen( $str );
        $newstr = "";
        $str = str_split($str);
        for ($i = 0; $i < $len; $i++) {
            if ($str[$i] != '\n' && $str[$i] != '\r') {
                $newstr .= $str[$i];
            }
        }
        return $newstr;
    }
}
?>