<?php
namespace app\common\Model;
use think\Model;
use think\Request;

class Admin extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
        'login_time'  =>  'timestamp',
		'old_time'  =>  'timestamp',
		'regtime'  =>  'timestamp',
    ];
	protected $insert = ['regtime'];  
	
	public function profile()
    {
        return $this->hasOne('AuthGroup','group_id','diction')->bind('title');
    }
	protected function setRegtimeAttr()
    {
        return time();
    }
}