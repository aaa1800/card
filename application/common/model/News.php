<?php
namespace app\common\Model;
use think\Model;


class News extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
    ];
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(isset($data['type'])){
			$map['type']=$data['type']>0?$data['type']:['gt',0];
		}else{
			$map['type']=['gt',0];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['type']=newstype($v['type']);
		}
		return $ok;
	}
	public function news($type){
		$ok=$this->where(['status'=>1,'type'=>$type])->order("id desc")->paginate(15,false,['query'=>request()->param() ]);
		return $ok;
	}
	
   
}