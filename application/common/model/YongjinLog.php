<?php
namespace app\common\model;

use think\Model;

class YongjinLog extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(isset($data['name']) && isset($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}else{
			$map['uid']=['gt',0];
		}
		$ok=$this::with('touid')->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		return $ok;
	}
	/*
	前台
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$des=$data['order'][0]['dir'];
		$str=$data['columns'][$data['order'][0]['column']]['data']." ".$des;
		$ok=$this::with('touid')->where($map)->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();	
		return $ok;
	}
	protected function getTypeAttr($val)
    {
		$r=["","充值","下级佣金","短信","域名","开通"];
        return $r[$val];
    }
	public function touid(){
		return $this->hasOne("user","id","formID")->bind("user");
	}
	
	public function addYong($money,$uid,$order=null){
		$num=C('dlnum');
		$fl=C('dlbl');
		$map=[];
		for($i=0;$i<$num;$i++){
			$toid=(new User)->where(['id'=>$uid])->value('assets');
			$U=new User;
			$user=$U->where(['id'=>$toid])->find();
			if($user){
				$yongjin=sprintf("%.2f",$money*$fl/100);
				$str=$i+1 . "级佣金" .$yongjin."元";
				$map[$i]=['uid'=>$toid,'data'=>$str,'price'=>$money,'money'=>$yongjin,'type'=>2,'formID'=>$uid];
				$U->where(['id'=>$toid])->setInc("money",$yongjin);
				$um=$U->where(['id'=>$toid])->value("money");
				(new MoneyLog)->upda(0,$toid,$yongjin,5,$order);
				$uid=$toid;
				$money=$money-$yongjin;
			}else{
				break;
			}
		}
		$this->saveAll($map);
		
	}
	
}