<?php
namespace app\common\Model;
use think\Model;

class Userbank extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'create_time'  =>  'timestamp',
    ];
	protected $insert = ['create_time'];
	
	protected function setCreateTimeAttr()
    {
        return time();
    }
	
	public function addBank($accounts,$user,$hread,$uid){
		$map['uid']=$uid;
		$map['bankname']="weixin";
		$map['accounts']=$accounts;
		$map['user']=$user;
		$map['hread']=$hread;
		$map['create_time']=time();
		$this->save($map);
	}		
	
	public function profile()
    {
        return $this->hasOne('User','uid','id')->bind(['count','sid'=>'id']);
    }
	
	
}