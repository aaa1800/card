<?php
namespace app\common\Model;
use think\Model;


class Webjs extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(!empty($data['st'])){
			$map['addtime']=['between time',[$data['st'],empty($data['et'])?date("Y-m-d H:i:s"):$data['et']]];
		}else{
			$map['id']=['gt',0];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		$ok['money']=$this->sum('lirun');
		return $ok;
	}
	
	public function addjs(){
		$res=$this->order("id desc")->find();
		$st= !$res?strtotime("2010-01-01 10:10:10"):$res['addtime'];
		$css=$this->scsxf($st);
		$acc=$this->sctx($st);
		$map['sum_recaharge']=$css['zpay'];
		$map['rech_fl']=$css['zsxf'];
		$map['sum_tx']=$acc['zpay'];
		$map['tx_fl']=$acc['zsxf'];
		$map['kt_fl']=(new Viporder)->where(['logtime'=>['gt',$st]])->sum("money");
		$map['domain_fl']=(new DomainOrder)->where(['addtime'=>['gt',$st]])->sum("money");
		$map['dxpay']=(new Dxorder)->sum("money");
		$map['payfl']=0;
		$map['feng']=0;
		$shj=array_sum($map);
		$map['yongjin']=(new YongjinLog)->where(['addtime'=>['gt',$st]])->sum('money');
		$map['lirun']=$shj-$map['yongjin'];
		$this->save($map);
		return json(['code'=>1,'msg'=>"结算成功"]);
	}
	private function scsxf($st){
		$pay=new PayOrder;
		$arr['zpay']=$pay->where(['status'=>1,'addtime'=>['gt',$st]])->sum("money");
		$arr['zsxf']=$pay->where(['status'=>1,'addtime'=>['gt',$st]])->sum("fl");
		return $arr;
	}
	private function sctx($st){
		$pay=new TxOrder;
		$arr['zpay']=$pay->where(['status'=>1,'addtime'=>['gt',$st]])->sum("money");
		$arr['zsxf']=$pay->where(['status'=>1,'addtime'=>['gt',$st]])->sum("fl");
		return $arr;
	}
	
	public function shanchu($str){
		$time=date("Y-m-d",strtotime("-".$str." month"));
		//DomainOrder::destroy(['addtime'=>['lt',$time]]);
		//Dxorder::destroy(['addtime'=>['lt',$time]]);
		//Dxsend::destroy(['addtime'=>['lt',$time]]);
		LoginLog::destroy(['addtime'=>['< time',$time]]);
		MoneyLog::destroy(['addtime'=>['< time',$time]]);
		Order::destroy(['create_time'=>['< time',$time]]);
		//PayOrder::destroy(['addtime'=>['lt',$time]]);
		//TxOrder::destroy(['addtime'=>['lt',$time]]);
		YongjinLog::destroy(['addtime'=>['< time',$time]]);
	}
	   
}