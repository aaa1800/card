<?php
namespace app\common\model;

use think\Request;
use think\Model;

class Mign extends Model{
	Protected $autoCheckFields = false;
	
	private $order;
	private $keyy;
	private $user;

	public function initt($order=null){
		if(!$order){return 'EMPTY ERR:参数不能为空！';exit;}
		$this->order=$order;
		$this->user=(new User)->where(['id'=>$order['partner']])->find();
		if(!$this->user){return 'Partner Not Bound:不存在';exit;}
		$this->keyy=$this->user['key'];
		return $this->z_verfy($order['payment_obj']);
	}

	public function z_verfy($t){
        return $this->str_nul();
		}
		
	public function sign_ver(){
		$ok=md5Verify($this->order,$this->order['sign'],$this->keyy);
		if($ok){
		   return $this->pay_oc();
		}else{
			return 'SIGN ERROR:sign错误';
			exit;

		}
	}

   public  function str_nul(){
	   $this->url=$_SERVER['HTTP_HOST'];
	   return $this->in_str();
	   
   }

   public  function in_str(){
	   $or=array(0=>'partner',1=>'out_trade_no',2=>'total_fee',3=>'notify_url',4=>'b_key',5=>'b_number',6=>'payment_obj',7=>'sign');
	   $l='';
	   for($i=0;$i<count($or);$i++){
		   if(!array_key_exists($or[$i],$this->order)){
			   $l='DEPLETION:缺少参数'.strtoupper($or[$i]);
			   break;
		   }
	   }
	   if($l){
		   return $l;
	   }else{
		   return $this->sign_ver();
	   }
   }
   

   public  function pay_oc(){
	   $pay_type_oc=(new Channel)->where(['type'=>$this->order['payment_obj']])->find();
	   if(!$pay_type_oc){
		   return 'PayMent_Type Err:卡类型不存在';
		   exit;
	   }else{
		   if($pay_type_oc['start']!=1){
			   return 'PayMent_Type Close:接口维护';
			   exit;
		   }
		   $cp=(new MemberRate)->where(['uid'=>$this->order['partner']])->value($pay_type_oc['fiel']);
		   $srt=explode("|",$cp);
		   if($srt[1]!=1){
			   return 'PayMent_Type Close:商户接口关闭';
			   exit;
		   }
	   }
	   return $this->pay_order_member();
   }
   
   public function inspect($da){
		$ok=(new Order)->where(['card_no'=>$da,'state'=>1])->find();
		if($ok){
		  return "此卡已在处理";
		  exit;
		}
	}
	
   public function pay_order_member(){
	   $g=$this->order;
	   $this->inspect($g['b_number']);
	   $g['b_key']=_encrypt($g['b_key'],'DECODE',$this->keyy);
	   $g['b_number']=_encrypt($g['b_number'],'DECODE',$this->keyy);
	   $da['custom']=isset($g['custom'])?$g['custom']:"";
	   $da['type']=(new Channel)->where(['type'=>$g['payment_obj']])->value("oper");
	   $da['source']=$this->url;
	   $da['channelid']=$g['payment_obj'];
	   $da['tmporder']=$g['out_trade_no'];
	   $da['uid']=$g['partner'];
	   $da['money']=$g['total_fee'];
	   $da['cardno']=$g['b_number'];
	   $da['cardpwd']=$g['b_key'];
	   $da['notify_url']=$g['notify_url'];
	   $da['fenlei']="";
	   $da['single']=0;
	   $id=(new Order)->sadata($da);
	   return $id;
   }
}
?>