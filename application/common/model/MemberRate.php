<?php
/**
 * Created by PhpStorm.
 * User: 微乐网  2306974
 * Date: 2018/12/19
 * Time: 13:28
 */

namespace app\common\model;


use think\Model;

class MemberRate extends Model
{
	//个人费率
	public function getrate($uid){
		$list=$this->where(['uid'=>$uid])->find();
		if($list){
			$list=$list->toArray();
			$arr=array();
			if($list){
				foreach($list as $k=>$v){
					if($k!="uid" && $k!="id"){
						$parm=explode("|",$v);
						$tr=(new Channel)->where(['fiel'=>$k])->find();
					    $arr[]=array("id"=>$tr['type'],'xt'=>$tr['sysrate'],'zd'=>$k,"fl"=>$parm[0],"state"=>$parm[1],'start'=>$tr['start'],'type'=>$tr['name']);
					}
				}

			}
			return $arr;
		}
	}
	public function setat($uid,$da){
		$res=$this->where(['uid'=>$uid])->value($da['id']);
		if($res){
			$parm=explode("|",$res);
			if($da['ty']==1){
				if($parm[1]==2){
					$msg=['code'=>103,"msg"=>"通道禁用中，无法开启"];
				}elseif($parm[1]==1){
					$msg=['code'=>104,"msg"=>"通道已经开启"];
				}else{
					$str=$parm[0]."|1";
					$this->where(['uid'=>$uid])->setField($da['id'],$str);
					$msg=['code'=>1,"msg"=>"开启成功"];
				}
			}else{
				$str=$parm[0]."|0";
				$this->where(['uid'=>$uid])->setField($da['id'],$str);
				$msg=['code'=>1,"msg"=>"关闭成功"];
			}
		}else{
			$msg=['code'=>102,"msg"=>"参数错误"];
		}
		return json($msg);
	}
	public function upstate($da){
		$res=$this->where(['uid'=>$da['uid']])->value($da['id']);
		if($res){
			$parm=explode("|",$res);
			$strr=$parm[0]."|".$da['s'];
			$this->where(['uid'=>$da['uid']])->setField($da['id'],$strr);	
			$msg=['code'=>1,"msg"=>"修改成功"];
		}else{
			$msg=['code'=>102,"msg"=>"参数错误"];
		}
		return json($msg);
	}
	
	public function opall($id){
		$f=(new Channel)->where(['type'=>$id])->value("fiel");
		$list=$this->select();
		foreach($list as $k=>$v){
			$this->upkai($v['uid'],$v[$f],$f);
		}
		return true;
	}
	public function upkai($uid,$u,$f){
		$re=explode("|",$u);
		$str=$re[0]."|1";
		$this->where(['uid'=>$uid])->setField($f,$str);
	}
	
	public function editRate($uid,$id,$rate){
		$fiel=(new Channel)->where(['type'=>$id])->value("fiel");
		$sya=(new Channel)->where(['type'=>$id])->value("sysrate");
		$msg=['code'=>2,"系统错误"];
		if($fiel){
			$rec=$this->where(['uid'=>$uid])->value($fiel);
			if($rec){
				$res=explode("|",$rec);
				$str=$rate."|".$res[1];
				if($rate>$sya){return json(['code'=>102,'msg'=>'会员费率不能大于系统费率']);exit;}
				$ok=$this->where(['uid'=>$uid])->setField($fiel,$str);
				if($ok){
					$msg=['code'=>1,'msg'=>"设置成功"];
				}else{
					$msg=['code'=>100,'msg'=>"费率没有变动"];
				}
			}
		}else{
			$msg=['code'=>102,'msg'=>"参数错误"];
		}
		return json($msg);
	}
	public function setall($da){
		$list=(new Member)->select();
		foreach($list as $v){
			$this->editRate($v['id'],$da['id'],$da['ra']);
		}
	}

}