<?php
namespace app\common\model;

use think\Model;

class Order extends Model{
	public function getCreateTimeAttr($time)
	{
		return $time;//返回create_time原始数据，不进行时间戳转换。
	}
	
	public function getUpdateTimeAttr($time){
		return $time;
	}
	public function sadata($da,$money){
		$arr['type']=$da['type'];
		$arr['class']=$da['channelid'];
		$arr['orderno']=pay_order("C");
		$arr['single']=$da['single'];
		$arr['tmporder']=isset($da['tmporder'])?$da['tmporder']:null;
		$arr['uid']=$da['uid'];
		$arr['source']=$da['source'];
		if(isset($da['custom'])){
			$arr['custom']=$da['custom'];
		}
		if(isset($da['notify_url'])){
			$arr['notify']=$da['notify_url'];
		}
		$arr['money']=$money;
		$arr['ip']=getip();
		$arr['create_time']=date("Y-m-d H:i:s");
		$arr['card_no']=$da['cardno'];
		$arr['card_key']=$da['cardpwd'];
		$arr['area']=huoip($arr['ip']);
		$arr['fenlei']=$da['fenlei'];
		$arr['day']=date("Y-m-d");
		$id=$this->insertGetId($arr);
		return $id;
	}
	
	public function getAll(){
		$ad=(new Admin)->where(['id'=>session('is_admin')])->value('id');
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		$map['id']=['gt',0];
		
		if(!empty($data['name']) && !empty($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}
		if(!empty($data['money'])){
			$map['money']=$data['money'];
		}
		if($ad!=1){
			$map['qiang']=$ad;
		}
		if(!empty($data['state'])){
		   $map['state']=$data['state']==3?0:$data['state'];
		}
		if(!empty($data['st'])){
			$map['create_time']=['between time',[$data['st'],empty($data['et'])?date("Y-m-d H:i:s"):$data['et']]];
		}
		if(!empty($data['classa'])){
			$map['class']=$data['classa'];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['uname']=(new User)->where(['id'=>$v['uid']])->value('user');
			$ok['data'][$k]['xit']=$v['amount']+$v['profit'];
			$ok['data'][$k]['orderno']=mb_substr($v['orderno'],10,8,'UTF-8');
			$ok['data'][$k]['class']=(new Channel)->where(['type'=>$v['class']])->value('name');
			$ok['data'][$k]['oper']=(new Operator)->where(['id'=>$v['type']])->value('name');
			$ok['data'][$k]['haos']=$v['update_time']?strtotime($v['update_time'])-strtotime($v['create_time'])."秒":"--";
			$ok['data'][$k]['qian']=($v['qiang']>0)?($v['qiang']==session('is_admin'))?'<button class="layui-btn layui-btn-danger" >自己</button>':'<button class="layui-btn layui-btn-disabled" lay-event="">'.$v['qiang'].'</button>':'<button class="layui-btn" lay-event="qiang" >抢单</button>';
		}
		$ok['money']=$this->where(['state'=>1])->sum('money');
		return $ok;
	}
	
	public function getprint(){
		$data=input();

		$map['id']=['gt',0];
		if(!empty($data['name']) && !empty($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}
		if(!empty($data['state'])){
		   $map['state']=$data['state'];
		}
		if(!empty($data['classa'])){
			$map['class']=$data['classa'];
		}
		if(!empty($data['st'])){
			$map['create_time']=['between time',[$data['st'],empty($data['et'])?date("Y-m-d H:i:s"):$data['et']]];
		}
		$ok=$this->field('id,type,class,qiang,orderno,uid,card_no,card_key,money,ip,settle_amt,amount,state,remarks,area')->where($map)->order("id desc")->select();
		$list=[];
		foreach($ok as $k=>$v){
			$list[$k]['id']=$v['id'];
			$list[$k]['type']=(new Operator)->where(['id'=>$v['type']])->value('name');
			$list[$k]['qiang']=($v['qiang']>0)?(new Admin)->where(['id'=>$v['qiang']])->value('user'):'无人抢单';
			$list[$k]['class']=(new Channel)->where(['type'=>$v['class']])->value('name');
			$list[$k]['orderno']=$v['orderno'];
			$list[$k]['uid']=$v['uid'];
			$list[$k]['card_no']='&nbsp;'.$v['card_no'];
			$list[$k]['card_key']="&nbsp;".$v['card_key'];
			$list[$k]['money']=$v['money'];
			$list[$k]['settle_amt']=$v['settle_amt'];
			$list[$k]['amount']=$v['amount'];
			$list[$k]['state']=($v['state']==0)?"处理中":($v['state']==1)?"处理完成":"处理失败";
			$list[$k]['remarks']=$v['remarks'];
			$list[$k]['haos']=$v['update_time']?strtotime($v['update_time'])-strtotime($v['create_time'])."秒":"--";
			$list[$k]['ip']=$v['ip'];
			$list[$k]['area']=$v['area'];
		}
		return $list;
	}
	public function okorder($da){
        $or=$this->where(['id'=>$da['id']])->find();
		$res=(new Channel)->where(['type'=>$or['class']])->find();
//		$rate=userrate($res['id'],$or['uid'],ceil($da['money']));
		$rate=$res['sysrate'];
		//$us=(new MemberRate)->where(['uid'=>$or['uid']])->value($res['fiel']);
		//$us2=explode("|",$us);
		//$rate=$us2[0];
		if(isset($da['type']) && $da['type']){
			$arr['type']=$da['type'];
		}
		if(isset($da['remarks'])){
			$arr['remarks']=$da['remarks'];
		}
		$arr['state']=1;
		$arr['update_time']=date("Y-m-d H:i:s");
		$arr['settle_amt']=$da['money'];
		$arr['amount']=$da['money']*$rate/100;
		$arr['profit']=$da['money']*$res['sysrate']/100-$da['money']*$rate/100;
		$arr['consuming']=strtotime($arr['update_time'])-strtotime($or['create_time']);
		$this->save($arr,['id'=>$da['id']]);
		$qmoney=(new User)->where(['id'=>$or['uid']])->value("money");
		(new User)->where(['id'=>$or['uid']])->setInc("money",$arr['amount']);
		(new MoneyLog)->upda($qmoney,$or['uid'],$arr['amount'],2,$or['orderno']);
		(new YongjinLog)->addYong($arr['amount'],$or['uid'],$or['orderno']);
	}
	public function isokorder($da,$money,$re="",$jin=""){
		$or=$this->where(['id'=>$da,'state'=>0])->find();
        if($or){
            $res=(new Channel)->where(['type'=>$or['class']])->find();
			$rate=userrate($res['id'],$or['uid'],ceil($money));
            $arr['amount']=$money*$rate/100;
            if(!empty($re)){
                $arr['remarks']=$re;
            }else{
              $arr['remarks']="";
            }
            if(!empty($jin)){
                $arr['profit']=$jin-$arr['amount'];
            }else{
                $arr['profit']=$money*$res['sysrate']/100-$money*$rate/100;
            }
            $arr['state']=1;
            $arr['update_time']=date("Y-m-d H:i:s");
            $arr['settle_amt']=$money;
           // $arr['cosok']=1;
            $arr['consuming']=strtotime($arr['update_time'])-strtotime($or['create_time']);
            $this->save($arr,['id'=>$da]);
            $qmoney=(new User)->where(['id'=>$or['uid']])->value("money");
            (new User)->where(['id'=>$or['uid']])->setInc("money",$arr['amount']);
            (new MoneyLog)->upda($qmoney,$or['uid'],$arr['amount'],2,$or['orderno']);
			(new YongjinLog)->addYong($arr['amount'],$or['uid'],$or['orderno']);
      }
	}
	/**

     * @param array $data 要导出的数据

     * @param array $title excel表格的表头

     * @param string $filename 文件名

     */

    public function daochu_excel($data=array(),$title=array(),$filename='报表'){//导出excel表格
        //处理中文文件名
        ob_end_clean();
        Header('content-Type:application/vnd.ms-excel;charset=utf-8');
        header("Content-Disposition:attachment;filename=export_data.xls");
        //处理中文文件名
        $ua = $_SERVER["HTTP_USER_AGENT"];
        $encoded_filename = urlencode($filename);
        $encoded_filename = str_replace("+", "%20", $encoded_filename);
        if (preg_match("/MSIE/", $ua) || preg_match("/LCTE/", $ua) || $ua == 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko') {
            header('Content-Disposition: attachment; filename="' . $encoded_filename . '.xls"');
        }else {
            header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        }
        header ( "Content-type:application/vnd.ms-excel" );
        $html = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
		$html.="<html xmlns='http://www.w3.org/1999/xhtml'>";
		$html.="<meta http-equiv='Content-type' content='text/html;charset=UTF-8' /><head><title>".$filename."</title>";
		$html.="<style>td{text-align:center;font-size:12px;font-family:Arial, Helvetica, sans-serif;border:#1C7A80 1px solid;color:#152122;";
		$html.="width:auto;}table,tr{border-style:none;}.title{background:#7DDCF0;color:#FFFFFF;font-weight:bold;}</style>";
		$html.="</head><body><table width='100%' border='1'><tr>";
        foreach($title as $k=>$v){
            $html .= " <td class='title' style='text-align:center;'>".$v."</td>";
        }   
        $html .= "</tr>";    
        foreach ($data as $key =>$value) {
            $html .= "<tr>";
            foreach($value as $aa){
                $html .= "<td>".$aa."</td>";
            }
			$html .= "</tr>";
			}
        $html .= "</table></body></html>";
        echo $html;
        exit;
    }
	
	public function bcard($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['uid']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['uid']=(new User)->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("class")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['class']=(new Channel)->where(['type'=>$v['class']])->value("name");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function ccard($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$user=new User;
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['uid']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['uid']=(new User)->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("uid,class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("uid")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['name']=$user->where(['id'=>$v['uid']])->value("user");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function dcard($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$user=new User;
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['uid']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['uid']=(new User)->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("day,uid,class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("day")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['name']=$user->where(['id'=>$v['uid']])->value("user");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function ecard($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['uid']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['uid']=(new User)->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("type,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("type")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['name']=(new Operator)->where(['id'=>$v['type']])->value("name");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function bcarda($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['uid']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['uid']=(new User)->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("class")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['class']=(new Channel)->where(['type'=>$v['class']])->value("name");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function ccarda($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$user=new Admin;
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['qiang']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['qiang']=$user->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("uid,class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("uid")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['name']=$user->where(['id'=>$v['uid']])->value("user");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function dcarda($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$user=new Admin;
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['qiang']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['qiang']=$user->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("day,uid,class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("day")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['name']=$user->where(['id'=>$v['uid']])->value("user");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function ecarda($da){
		$psize=$da['limit'];
		$page=$da['page'];
		$map['id']=['gt',0];
		if(!empty($da['STime']) && !empty($da['ETime'])){
		  $map['create_time']=array('between',[$da['STime'],$da['ETime']]);
		}
		if(!empty($da['Uid'])){
			$map['qiang']=$da['Uid'];
		}
		if(!empty($da['Name'])){
			$map['qiang']=(new Admin)->where(['user'=>$da['Name']])->value("id");
		}
		$list=$this->field("type,class,count(*) as cid,sum(money) as money,sum(amount) as su,sum(profit) as spr,sum(settle_amt) as amt")->where($map)->group("type,money")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($list['data'] as $k=>$v){
			$list['data'][$k]['name']=(new Channel)->where(['type'=>$v['class']])->value("name");
			$list['data'][$k]['supr']=$v['su']+$v['spr'];
		}
		return $this->redata($list);
	}
	public function redata($list){
		$arr=[];
		$arr['code']=0;
		$arr['count']=$list['total'];
		$arr['data']=$list['data'];
		$arr['msg']="获取成功!";
		$arr['rel']=1;
		return $arr;
	}
}