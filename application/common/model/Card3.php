<?php
/**
 * Created by PhpStorm.
 * User: 微乐网
 * Date: 2018/12/19
 * Time: 13:28
 */

namespace app\common\model;
use think\Session;
use think\Request;
use think\Model;

class Card3 extends Model{
	Protected $autoCheckFields = false;
	
	private $K;
	private $MK;
	private $iv;
	
	public function kk($str,$key,$iv){
		$this->K=$key;
		$this->iv=$iv;
		$ss=$this->desc($str);
		return $ss;
	}

	public function tt($par){
		return $this->crea($par);
	}

	private  function desc($str){
		$string = $this->PaddingPKCS7($str);
		$key =$this->K; //24位
		$td = mcrypt_module_open( MCRYPT_3DES, '', MCRYPT_MODE_CFB, '' );
		//$iv = mcrypt_create_iv( mcrypt_enc_get_iv_size( $td ), MCRYPT_RAND );
		mcrypt_generic_init( $td, $key, $this->iv );
		$result = mcrypt_generic( $td, $string );
		return base64_encode($result);
	}
		
	private function PaddingPKCS7($input) {
		$srcdata = $input;
		$block_size = mcrypt_get_block_size('tripledes', 'cfb');
		$padding_char = $block_size - (strlen($input) % $block_size);
		$srcdata .= str_repeat(chr($padding_char),$padding_char);
		return $srcdata;
		}
	private function crea($para) {
		$arg  = "";
		while (list ($key, $val) = each ($para)){
			if($key=="NotifyUrl"){
			  $arg.=$key."=".$val."&";
			}else{
				$arg.=$key."=".URLEncode($val)."&";
			}
			}
			$arg = substr($arg,0,count($arg)-2);
			if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
			return $arg;
	}


}