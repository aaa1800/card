<?php
namespace app\common\model;

use think\Model;

class Qiye extends Model{
	
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
        'addtime'  =>  'timestamp'
		
    ];
	protected $insert = ['addtime'];  
		
	protected function setAddtimeAttr()
    {
        return time();
    }
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(!empty($data['st'])){
			$map['addtime']=['between time',[$data['st'],empty($data['et'])?date("Y-m-d H:i:s"):$data['et']]];
		}
		if(!empty($data['name'])){
		   $map['uid|name']=$data['name'];
		}else{
			$map['uid']=['gt',0];
		}
		if(isset($data['kk'])){
			$map['state']=$data['kk']==3?0:$data['kk'];
		}
		$ok=$this::with('profile')->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['idCardStartDate']=date('Y/m/d',strtotime($v['idCardStartDate']));
			$ok['data'][$k]['idCardEndDate']=date('Y/m/d',strtotime($v['idCardEndDate']));
			
		}
		return $ok;
	}
	public function profile()
    {
        return $this->hasOne('User','id','uid')->bind('user,username,idcard');
    }
}