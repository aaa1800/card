<?php
namespace app\common\model;

use think\Model;

class PayOrder extends Model{
	//有统计查询 不能设置时间为时间戳
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		if(isset($data['name']) && isset($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}else{
			$map['uid']=['gt',0];
		}
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		$map['status']=1;
		$ok['money']=$this->where($map)->sum("ysk_order");
		return $ok;
	}
	/*
	前台receivable
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$des=$data['order'][0]['dir'];
		$ordd=$data['columns'][$data['order'][0]['column']]['data'];
		$str=$ordd=='account'?'receivable':$ordd." ".$des;
		$search=$data['search']['value'];
		if(!empty($search)){
			$map['out_trade_no|order|uid']=['like',"%".$search."%"];
		}
		$ok=$this::with('done')->where($map)->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['status']=paytype($v['status']);
		}
		return $ok;
	}
	public function done(){
		return $this->hasOne("userpay",'id','receivable')->bind('account');
	}
	public function tongji(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$s=$psize*($page-1);
		$des=$data['order'][0]['dir'];
		$ordd=$data['columns'][$data['order'][0]['column']]['data'];
		$str=$ordd." ".$des;
		$total=$this->where($map)->group("DATE_FORMAT(addtime, '%Y-%m-%d')")->count();
		$ok=$this->field("DATE_FORMAT(addtime, '%Y-%m-%d') as addtime,sum(money) as nummoney,count(*) as numcount")->group("DATE_FORMAT(addtime, '%Y-%m-%d')")->where($map)->order($str)->limit($s,$psize*$page)->select();
		$n=$s+1;
		foreach($ok as $k=>$v){
			$map['addtime']=['between',[$v['addtime']." 00:00:00:",$v['addtime']." 23:59:59:"]];
			$map['status']=1;
			$ok[$k]['id']=$n;
			$ok[$k]['oknum']=$this->where($map)->sum("money");
			$ok[$k]['okcount']=$this->where($map)->count();
			$ok[$k]['fl']=$this->where($map)->sum("fl");
			$ok[$k]['yl']=sprintf("%.2f",$ok[$k]['oknum']-$ok[$k]['fl']);
			$n++;
		}
		return ['data'=>$ok,'total'=>$total,'current_page'=>$psize];
	}
	
	public function recharge($data,$rate){
		$this->where(['addtime'=>['< time',time()-300],'status'=>0])->update(['status'=>2]);
		$jin=$this->generateJin($data['price'],$data['class'],$data['partner']);
		$orjiao= $jin ? $jin : 0;
		if($orjiao==0){return ['code'=>-1,'msg'=>'系统繁忙,请稍后再试'];exit;}
		$ewmurl=(new Userpay)->generate($orjiao,$data['class'],$data['partner']);
		if($ewmurl===false){return ['code'=>-1,'msg'=>'收款账号资源不足，请稍后再试'];exit;}
		$map['uid']=$data['partner'];
		$map['out_trade_no']=pay_order('M');
		$map['order']=$data['order'];
		$map['name']=isset($data['name'])?$data['name']:0;
		$map['type']=$data['class'];
		$map['money']=$data['price'];
		$map['addtime']=date('Y/m/d H:i:s');
		$map['ysk_order']= $orjiao;
		$map['receivable']=$ewmurl['id'];
		$map['status']=0;
		$map['custom']=isset($data['custom'])?$data['custom']:"";
		$map['website']=domei();
		$map['notify']=$data['notify'];
	    $map['return']=$data['return'];
		$xt=C('xitong');
		$xtstr=explode("|",$xt);
		if(in_array($data['partner'],$xtstr)){
		  $rate=0;
		}
	    $map['fl']=$rate;
		$url=$ewmurl['url'];
		self::create($map);
		return ['code'=>1,'order'=>$map['out_trade_no'],'img'=>$url,'money'=>$orjiao];
	}
	public function rest($id){
		$pay=self::get($id);
		if(!$pay){return ['code'=>-1,'msg'=>'订单不存在'];exit;}
		$us=User::get($pay['uid']);
		if($us['money']<$pay['fl']){
			return ['code'=>-1,'msg'=>'重发失败,账户余额不足'];exit;
		}
		$arr=array(
				'status'=>$pay['status'],
				'partner'=>$pay['uid'],
				'order'=>$pay['order'],
				'price'=>$pay['money'],
				'money'=>$pay['ysk_order'],
				'platform'=>$pay['out_trade_no'],
				'custom'=>$pay['custom']
				);
		$sign=md5Verify($arr,"",$us['key']);
		$arr['sign']=$sign;
		$msg=vpost($pay['notify'], $arr);
		$pay->count=htmlspecialchars($msg);
		$pay->save();
		return ['code'=>1,'msg'=>$msg];
	}
	//生成金额
	private function generateJin($money,$type,$uid){
		$money=sprintf("%.2f",$money);
		$li=$this->where(['status'=>0,'uid'=>$uid,'ysk_order'=>$money,'type'=>$type])->find();
		if($li){
			$res=$this->where(['uid'=>$uid,'ysk_order'=>['between',[$money,$money+1]],'status'=>0,'type'=>$type])->select();
			$ar=[];
			if($res){
				foreach($res as $k=>$v){
					$ar[]=$v['ysk_order'];
				}
				for($i=0;$i<98;$i++){
					$arr[]=sprintf("%.2f",$money+0.01*$i);
				}
				$rt=array_diff($arr,$ar);
				if(count($rt)==0){
					return false;
				}else{
				   return min($rt);
				}
			}else{
				return $money;
			}
		}else{
			return $money;
		}
	}
	
}