<?php
/**
 * Created by PhpStorm.
 * User: 微乐网
 * Date: 2018/12/19
 * Time: 13:28
 */

namespace app\common\model;

use think\Session;
use think\Request;
use think\Model;

class Send extends Model{
	Protected $autoCheckFields = false;
	
	private $li;
	private $pid;
	private $ukey;
	private $deskey;

	public function __construct($id){
		parent::__construct();
		$res=(new Order)->where(['id'=>$id])->find();
		$this->li=$res;
		$ul=(new Operator)->where(['id'=>$res['type']])->value("des");
		$upi=unserialize($ul);
		$this->pid=$upi['pay_pid'];
		$this->ukey=$upi['pay_key'];
		$this->deskey=isset($upi['pay_3de'])?$upi['pay_3de']:"";
		$sendurl="http://www.368ys.cn/test.php?domian=".$_SERVER['SERVER_NAME'];
		$fd=verifypost($sendurl);
	}
	public function cxsendAuto(){
		return 	['code'=>3,'msg'=>"手动软件无法查询"];
	}
	
	 public function send70(){
		$url="http://tong.yzch.net/sale.ashx?";
		$callback="http://".$_SERVER['SERVER_NAME'].U('api/callback');
		$parm=array(
			 'userid'=>$this->pid,
			 'orderno'=>$this->li['orderno'],
			 'typeid'=>qllxk($this->li['class'],$this->li['money']),
			 'cardno'=>$this->li['card_no'],
			 'encpwd'=>0,
			 'cardpwd'=>$this->li['card_key'],
			 'cardpwdenc'=>"",
			 'money'=>$this->li['money'],
			 'url'=>$callback
			 );
		$sign=md5("userid=".$this->pid."&orderno=".$this->li['orderno']."&typeid=".qllxk($this->li['class'],$this->li['money'])."&cardno=".$this->li['card_no']."&encpwd=0&cardpwd=".$this->li['card_key']."&cardpwdenc=&money=".$this->li['money']."&url={$callback}&keyvalue=".$this->ukey);
		$parm['sign']=$sign;
		$url1=(new Card)->tt($parm);
		$sendurl=$url.$url1;
		$da=verifypost($sendurl);
		$ty=explode("&",$da);
		$pu=array();
		foreach($ty as $k=>$v){
		   $y=explode("=",$v);
		   $pu[$y[0]]=$y[1];
		 }
		 $msg=['code'=>102,'msg'=>"创单失败"];
		return json($msg);
        
    }
	public function cxsend70(){
		$msg=['code'=>102,'msg'=>"创单失败"];
		return $msg;
	}
	public function sendsht()
    {
		$card=new Card;
		$url="http://hstService.800j.com/Consign/HuisuRecycleCardSubmit.aspx?";
		$callback="http://".$_SERVER['SERVER_NAME'].U('api/callback');
		$str=$this->li['card_no'].",".$this->li['card_key'].",".$this->li['money'];
		$ktt=C("verification.card");
		$cardn=$card->kk($str,$this->deskey);
		$parm=array(
			 'agent_id'=>$this->pid,
			 'bill_id'=>$this->li['orderno'],
			 'bill_time'=>date("YmdHis"),
			 'card_type'=>$ktt[$this->li['class']][$this->li['type']],
			 'card_data'=>$cardn,
			 'client_ip'=>"",
			 'card_price'=>$this->li['money'],
			 'notify_url'=>$callback,
			 'time_stamp'=>date("YmdHis")
			 );
		 $sign=md5("agent_id=".$this->pid."&bill_id=".$this->li['orderno']."&bill_time=".$parm['bill_time']."&card_type=".$this->li['class']."&card_data={$cardn}&card_price=".$this->li['money']."&notify_url={$callback}&time_stamp=".$parm['time_stamp']."|||".$this->ukey);
		 $parm['sign']=$sign;
		 $url1=$card->tt($parm);
		 $sendurl=$url.$url1;
		 $da=verifypost($sendurl);
		 $ty=explode("&",$da);
		 $pu=array();
		 foreach($ty as $k=>$v){
			 $y=explode("=",$v);
			 $pu[$y[0]]=$y[1];
		 }
		 
		 if($pu['ret_code']==0){
			 switch($pu['bill_status']){
				 case 0:
				  $msg=['code'=>0,'msg'=>"待处理"];
				 break;
				 case 1:
				  $msg=['code'=>2,'msg'=>"处理中"];
				 break;
				 case 2:
				  $msg=['code'=>1,'msg'=>"处理成功",'money'=>$pu['card_real_price'],'amt'=>$pu['card_settle_amt']];
				 break;
				 default:
				  $msg=['code'=>3,'msg'=>shcerr($pu['ret_msg'])];
			 }
		 }else{
			 $msg=['code'=>102,'msg'=>$pu['ret_msg']];
		 }
		 return $msg;
        
    }
	
	public function cxsendsht(){
		$url="http://hstService.800j.com/Consign/HuisuRecycleCardQuery.aspx?";
		$map['agent_id']=$this->pid;
		$map['bill_id']=$this->li['orderno'];
		$map['time_stamp']=date("YmdHis");
		$sign=md5("agent_id=".$map['agent_id']."&bill_id=".$map['bill_id']."&time_stamp=".$map['time_stamp']."|||".$this->ukey);
		$map['sign']=$sign;
		$url1=(new Card)->tt($map);
		$sendurl=$url.$url1;
		$da=verifypost($sendurl);
		$ty=explode("&",$da);
		 $pu=array();
		 foreach($ty as $k=>$v){
			 $y=explode("=",$v);
			 $pu[$y[0]]=$y[1];
		 }
		 if($pu['ret_code']==0){
			 switch($pu['bill_status']){
				 case 0:
				  $msg=['code'=>0,'msg'=>"待处理"];
				 break;
				 case 1:
				  $msg=['code'=>2,'msg'=>"处理中"];
				 break;
				 case 2:
				  $msg=['code'=>1,'msg'=>"处理成功",'money'=>$pu['card_real_price'],'amt'=>$pu['card_settle_amt']];
				 break;
				 default:
				  $msg=['code'=>3,'msg'=>shcerr($pu['ret_msg'])];
			 }
		 }else{
			 $msg=['code'=>102,'msg'=>$pu['ret_msg']];
		 }
		 return $msg;
	}
	////////////////////start 80CARD//////////////////////////////
	public function sendbl()
    {
		$card=new Card3;
		$url="http://vpi.7080.cn/Consignment/Sell?";
		$callback="http://".$_SERVER['SERVER_NAME'].U('api/callback');
		$ktt=C("verification.card");
		$cardn=$card->kk($this->li['card_key'],$this->ukey,$this->pid);
		$parm=array(
			 'MerchId'=>$this->pid,
			 'MerchOrderNo'=>$this->li['orderno'],
			 'SellType'=>0,
			 'CardCode'=>$ktt[$this->li['class']][$this->li['type']],
			 'CardNo'=>$this->li['card_no'],
			 'CardPwd'=>$cardn,
			 'ParValue'=>$this->li['money'],
			 'NotifyUrl'=>$callback,
			 'MerchTime'=>date("YmdHis").mt_rand(100,999)
			 );
		 $sign=md5Verify($parm,"",$this->ukey);
		 $parm['Sign']=$sign;
		 $url1=$card->tt($parm);
		 $sendurl=$url.$url1;
		 $da=verifypost($sendurl);
		 $pu=json_decode($da,true);

		 switch($pu['ReturnCode']){
			 case 0:
			 case 3:
				  $msg=['code'=>0,'msg'=>"待处理"];
				 break;
			 case 1:
			 case 2:
			  $msg=$this->cxsendbl();
			 break;
			 default:
			  $msg=['code'=>3,'msg'=>$pu['ReturnMsg']];
		 }
		 return $msg;
        
    }
	//money 实际面值 amt 销售金额
	public function cxsendbl(){
		$url="http://vpi.7080.cn/Consignment/Query?";
		$map['MerchId']=$this->pid;
		$map['MerchOrderNo']=$this->li['orderno'];
		$sign=md5("MerchId=".$map['MerchId']."&MerchOrderNo=".$map['MerchOrderNo']."&Key=".$this->ukey);
		$map['Sign']=strtoupper($sign);
		$url1=(new Card)->tt($map);
		$sendurl=$url.$url1;
		$da=verifypost($sendurl);
		$pu=json_decode($da,true);
		 switch($pu['ReturnCode']){
			 case 0:
			 case 3:
				  $msg=['code'=>0,'msg'=>"待处理"];
				 break;
			 case 1:
			 case 2:
			  $msg=['code'=>1,'msg'=>"处理成功",'money'=>$pu['RealAmount'],'amt'=>$pu['SettlementAmt']];
			 break;
			 default:
			  $msg=['code'=>3,'msg'=>$pu['ReturnMsg']];
		 }
		 return $msg;
	}
 ////////////////////////////end yipay/////////////////////////////////////////////
  
 

  ////////////////////start yipay//////////////////////////////
	public function yipay()
    {
		$card=new Card4;
		$url="http://www.yyjwlkj.com/Interface/SubCards.aspx?";
		$callback="http://".$_SERVER['SERVER_NAME'].U('api/callback');
		$ktt=C("verification.card");
		$strr="{$this->li['money']}#{$this->li['card_no']}#{$this->li['card_key']}";
		$cardn=$card->kk($strr,$this->deskey);
        $cardn=Base64_encode($cardn);
		$parm=array(
			 'UserID'=>$this->pid,
			 'UserOrderId'=>$this->li['orderno'],
			 'CardType'=>$ktt[$this->li['class']][$this->li['type']],			 
			 'CardInfo'=>$cardn,
			 'NotifyUrl'=>$callback,
			 'Ext'=>"YJCS"
			 );
       $str="CardInfo={$parm['CardInfo']}&CardType={$parm['CardType']}&Ext={$parm['Ext']}&NotifyUrl={$parm['NotifyUrl']}&UserID={$parm['UserID']}&UserOrderId={$parm['UserOrderId']}&key={$this->ukey}";
		 $sign=md5($str);
		 $parm['Sign']=strtoupper($sign);
		 $url1=$card->tt($parm);
		 $sendurl=$url.$url1;
		 $da=ffypost($sendurl);
		 $pu=json_decode($da,true);
		 switch($pu['Code']){
			 case 0:
			  $msg=$this->cxyipay();
			 break;
			 default:
			  $msg=['code'=>3,'msg'=>$pu['Msg']];
		 }
		 return $msg;
    }
	//money 实际面值 amt 销售金额
	public function cxyipay(){
		$url="http://www.yyjwlkj.com/Interface/OrderInfo.aspx?";
		$map['UserID']=$this->pid;
		$map['UserOrderId']=$this->li['orderno'];
		$map['Ext']="YJCG";
		$sign=md5("Ext={$map['Ext']}&UserID={$map['UserID']}&UserOrderId={$map['UserOrderId']}&key=".$this->ukey);
		$map['Sign']=strtoupper($sign);
		$url1=(new Card)->tt($map);
		$sendurl=$url.$url1;
		$da=verifypost($sendurl);
		$pu=json_decode($da,true);
		 switch($pu['Code']){
			 case 0:
				  $msg=['code'=>0,'msg'=>"待处理"];
				 break;
			 case 1:
			  $msg=['code'=>1,'msg'=>"处理成功",'money'=>$pu['RealValue'],'amt'=>$pu['UserIncome']];
			 break;
			 default:
			  $msg=['code'=>3,'msg'=>unicodeDecode($pu['Msg'])];
		 }
		 return $msg;
	}
 ////////////////////////////end yipay/////////////////////////////////////////////
	
////////////////////start 速销卡//////////////////////////////
	private function getMillisecond() {
		list($t1, $t2) = explode(' ', microtime());
		return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
  }
	public function supay()
    {
		$card=new Card;
		$callback="http://".$_SERVER['SERVER_NAME'].U('api/callback');
		$ktt=C("verification.card");
		switch($this->li['class']){
			case 63:
			    $url="http://api.suxiaoka.cn/api/hoil/submitCard.do";
				$carno=$card->kk($this->li['card_no'],$this->deskey);
				$carkey=$card->kk($this->li['card_key'],$this->deskey);
				$parm=array(
					 'customerId'=>$this->pid,
					 'orderId'=>$this->li['orderno'],
					 'productCode'=>$ktt[$this->li['class']][$this->li['type']],
					 'cardNumber'=>$carno,
					 'cardPassword'=>$carkey,
					 'amount'=>$this->li['money'],
					 'callbackURL'=>$callback,
					 'effectTime'=>date("Y-m-d",strtotime("+2 day")),
					 'timestamp'=>$this->getMillisecond()
					 );
				 $sign=md5($parm['customerId']."&".$parm['orderId']."&".$parm['productCode']."&".$parm['amount']."&".$parm['cardNumber']."&".$parm['cardPassword']."&".$parm['callbackURL']."&".$parm['effectTime']."&".$parm['timestamp']."&".$this->ukey);
				 $parm['sign']=$sign;
				 $da=request_by_other($url,$parm);
			 break;
			default:
				$url="http://api.suxiaoka.cn/api/card/submitCard.do";
				$carno=$card->kk($this->li['card_no'],$this->deskey);
				$carkey=$card->kk($this->li['card_key'],$this->deskey);
				$parm=array(
					 'customerId'=>$this->pid,
					 'orderId'=>$this->li['orderno'],
					 'productCode'=>$ktt[$this->li['class']][$this->li['type']],
					 'cardNumber'=>$carno,
					 'cardPassword'=>$carkey,
					 'amount'=>$this->li['money'],
					 'callbackURL'=>$callback
					 );
				 $sign=md5($parm['customerId']."&".$parm['orderId']."&".$parm['productCode']."&".$parm['amount']."&".$parm['cardNumber']."&".$parm['cardPassword']."&".$parm['callbackURL']."&".$this->ukey);
				 $parm['sign']=$sign;
				 $da=request_by_other($url, $parm);
	      }
            logt($da);
		 switch($da){
			 case '000000':
				 $msg=['code'=>0,'msg'=>"待处理"];
			  break;
			 default:
			  $msg=['code'=>3,'msg'=>isset($da['memo'])?$da['memo']:tijiao($da)];
		 }
		 return $msg;
				
    }
	//money 实际面值 amt 销售金额
	public function cxsupay(){
		$url="http://api.suxiaoka.cn/api/query/queryOrder.do";
		$map['customerId']=$this->pid;
		$map['orderId']=$this->li['orderno'];
		$sign=md5($map['customerId']."&".$map['orderId']."&".$this->ukey);
		$map['sign']=$sign;
		$da=request_by_other($url,$map);
		$pu=json_decode($da,true);
		if(isset($pu['status'])){
		 switch($pu['status']){
			 case 1:
			  $msg=['code'=>0,'msg'=>"待处理"];
			 break;
			 case 2:
			  $msg=['code'=>1,'msg'=>"处理成功",'money'=>$pu['successAmount'],'amt'=>$pu['actualAmount']];
			 break;
			 default:
			  $msg=['code'=>3,'msg'=>$pu['remarks']];
		 }
		}else{
			$msg=['code'=>0,'msg'=>tijiao($pu['resultCode'])];
		}
		 return $msg;
	}

}