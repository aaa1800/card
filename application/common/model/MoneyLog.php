<?php
namespace app\common\model;

use think\Model;

class MoneyLog extends Model{
	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		$map['uid']=$data['id'];
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		return $ok;
	}
	/*
	前台
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$des=$data['order'][0]['dir'];
		$str=$data['columns'][$data['order'][0]['column']]['data']." ".$des;
		$res=[];
		$ok=$this->where($map)->order($str)->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();	
		return $ok;
	}
	protected function getTypeAttr($val)
    {
		$r=["","单卡对换","批量对换","提现","加减金额",'佣金'];
        return $r[$val];
    }
	
	public function addLog($money,$type,$data,$rt=0,$uid=null,$order=null){
		if($uid){
			$usid=$uid;
			}else{
				$usid=session("user_id");
		}
		if((int)$type==2 )$money= -$money;
		$price=(new User)->where(['id'=>$usid])->value($rt==1?"yongjin":"money");
		$this->save(['orderno'=>$order,'uid'=>$usid,'data'=>$data,'type'=>$type,'money'=>$price,'price'=>$money]);
	}
	 public function upda($qmoney,$uid,$money,$type,$order=null){
		  $us=(new User)->where(['id'=>$uid])->value("money");
		  $arr['uid']=$uid;
		  $arr['orderno']=$order;
		  $arr['money']=$us;
		  $arr['type']=$type;
		  
		 switch($type){
			 case 1://提现
			  $arr['data']="[商户结算],{$money}元";
			  $arr['price']=-$money;
			 break;
			 case 2://增加
			  $arr['data']="[商户售卡],{$money}元";
			  $arr['price']=$money;
			  
			 break;
			 case 3://增加
			  $arr['data']="[提现退票],{$money}元";
			  $arr['price']=$money;
			 break;
			 case 4://减少
			  $arr['data']="[订单重设],{$money}元";
			  $arr['price']=-$money;
			 break;
			 case 5://佣金加
			  $arr['data']="[佣金],{$money}元";
			  $arr['price']=$money;
			 break;
		 }
		 $this->save($arr);
 }
	
}