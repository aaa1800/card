<?php
/**
 * Created by PhpStorm.
 * User: 微乐网
 * Date: 2018/12/19
 * Time: 13:28
 */

namespace app\common\model;
use think\Session;
use think\Model;

class Cashwith extends Model{
	Protected $autoCheckFields = false;
	
	
	public function cash($orderid,$userid='',$money='',$remarks=''){
		if(C('alixian')<$money){
			return ['code'=>0,'msg'=>'单笔提现大于'.$money];exit;
		}
		if(C('alitxopen')=="on"){
			return $this->withdr($orderid,$userid,$money,$remarks);
		}else{
			return ['code'=>1,'msg'=>'请手动转账，支付宝自动转账关闭'];
		}
	}
	public function withdr($orderid,$userid='',$money='',$remarks=''){
              
			$aop = new AopClient ();
			$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do'; //请求url
			$aop->appId = C('alipid');        //商家支付宝APPID
			$aop->rsaPrivateKey = C('alimach');    //私钥
			$aop->alipayrsaPublicKey=C('alikey');    //支付宝公钥
			$aop->apiVersion = '1.0';    //版本号
			$aop->signType = 'RSA2';    //加密方式
			$aop->postCharset='utf-8';    
			$aop->format='xml';  //支付宝返回方式
			$out_biz_no = $orderid;    //订单号
			$request = new AlipayFundTransToaccountTransferRequest();
			$request->setBizContent("{" .
			"\"out_biz_no\":\"".$out_biz_no."\"," .
			"\"payee_type\":\"ALIPAY_LOGONID\"," .
			"\"payee_account\":\"".$userid."\"," .
			"\"amount\":\"".$money."\"," .
			"\"payer_show_name\":\"公司名\"," .
			"\"remark\":\"".$remarks."\"" .
			"}");
			$result = $aop->execute ( $request); 
			$responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
			$resultCode = $result->$responseNode->code;
			$result = json_decode(json_encode($result),true);
			if($result['code']== 10000 && $result['msg']=='Success'){
				//转账记录入库
				$data['code'] = 1;
				$data['msg'] = '企业转账';
				$data['out_biz_no'] = $out_biz_no;
				return  $data;
			} else {
				$data['code'] = 0;
				$data['msg'] = $result['sub_msg'];
				return $data;
			}
    }

}