<?php
namespace app\common\model;

use think\Model;

class Channel extends Model{
	protected $type = [
		'amt'  =>  'array'
    ];
	
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		$map['id']=['gt',0];
		if(!empty($data['name']) && !empty($data['kk'])){
		   $map[$data['kk']]=$data['name'];
		}
		$ok=$this->where($map)->order("id asc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$ok['data'][$k]['class']=typed($v['class']);
			$ok['data'][$k]['oper']=(new Operator)->where(['id'=>$v['oper']])->value('name');
			$ok['data'][$k]['amt']=cardkey($v['amt']);
		}
		return $ok;
	}
	
	public function getU(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		$uid=isset($data['uid'])?$data['uid']:0;
		$ok=$this->field('type,name,fiel,payrate')->where(['id'=>['gt',0]])->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		foreach($ok['data'] as $k=>$v){
			$us=(new MemberRate)->where(['uid'=>$uid])->value($v['fiel']);
			if(!$us){User::addrate($uid);$us=(new MemberRate)->where(['uid'=>$uid])->value($v['fiel']);}
			$us2=explode("|",$us);
			$ok['data'][$k]['urate']=isset($us2[0])?$us2[0]:$v['fiel'];
			$ok['data'][$k]['start']=isset($us2[1])?$us2[1]:1;
			$ok['data'][$k]['field']=$v['fiel'];
		}
		//$ok['total']=(new User)->sum("money");

		return $ok;
	}
}