<?php
namespace app\common\Model;
use think\Model;
use think\Request;

class LoginLog extends Model{

	protected $dateFormat = 'Y/m/d H:i:s';
	protected $type = [
		'addtime'  =>  'timestamp',
    ];
	protected $insert = ['addtime'];
	
	protected function setAddtimeAttr()
    {
        return time();
    }
	public static function addLog($uid,$str,$type){
		$ip=getip();
		$address=huoip($ip);
		$att="{$ip}【{$address}】";
		self::create(['uid'=>$uid,'type'=>$type,'data'=>$str,'ip'=>$att,'address'=>$address]);
	}
	public function getAll(){
		$data=input();
		$psize=$data['limit'];
		$page=$data['page'];
		$map['uid']=$data['id'];
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
		return $ok;
	}
	
	public function getTypeAttr($value)
    {
        $status = [1=>'登陆',2=>'退出',3=>'其他'];
        return $status[$value];
    }
	
	/*
	前台
	*/
	public function getList(){
		$data=input();
		$psize=$data['length'];
		$page=$data['start']/$data['length']+1;
		$map['uid']=session('user_id');
		$res=[];
		$ok=$this->where($map)->order("id desc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();		
		return $ok;
	}
	
}