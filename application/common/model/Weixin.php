<?php
/**
 * Created by PhpStorm.
 * User: 微乐网
 * Date: 2018/12/19
 * Time: 13:28
 */

namespace app\common\model;
use think\Request;
use think\Model;

class Weixin extends Model{
	Protected $autoCheckFields = false;
	public $appid="";
	public $secret="";
	protected $url = "";
    protected $access_tokens = "";
	
	
	public function __construct(){

        //获取$access_token
		$this->appid=C("wxappid");
		$this->secret=C("wxsecret");
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $this->appid . "&secret=" . $this->secret . "";
        $result = vpost($url);
        $access_tokens = json_decode($result, true);
        $this->access_tokens = isset($access_tokens['access_token'])?$access_tokens['access_token']:"";
    }
	
	public function getda(){
		return $this->access_tokens;
	}

    public function Follow(){

        //非必传项
        $rs = $this->getTemporaryQrcode($this->access_tokens);
        $ticket = isset($rs['ticket'])?$rs['ticket']:"";
        $qrcode = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" . $ticket . "";
        ///打印二维码显示

        return $qrcode;

    }
	
 

//生成二维码

    public function getTemporaryQrcode($access_tokens){
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" .$access_tokens ;

          //生成二维码需要的参数
		$id=session("userid");
        $qrcode ="{\"action_name\":\"QR_LIMIT_SCENE\",\"action_info\":{\"scene\":{\"scene_id\":{$id},\"extinfo\":{$id}}}}";
		$re=json_decode($qrcode,true);
        $result = vpost($url,$qrcode);
        $rs = json_decode($result, true);

        return $rs;

    }

}