<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use app\common\model\AuthGroup;
use app\common\model\User;
use app\common\model\Channel;
use app\common\model\MemberRate;

function userrate($type,$uid="",$money=0){
	       $res=(new Channel)->where(['id'=>$type])->find();
		   if($uid && $money>0){
			   $ress=explode("|",$res['rule']);
			   $arr=[];
			   $us=(new MemberRate)->where(['uid'=>$uid])->value($res['fiel']);
			   $us2=explode("|",$us);
			   $rate=$us2[0];
			   foreach($ress as $k=>$v){
					$rt=explode(",",$v);
					$arr[$rt[0]]=isset($rt[1])?$rt[1]:1;
			   }
			   $new_rate=isset($arr[$money])?$arr[$money]:1;
			   $rate=sprintf("%.3f",$new_rate*$rate);
		   }else{
				if(session("?user_id")){
					$us=(new MemberRate)->where(['uid'=>session('user_id')])->value($res['fiel']);
					$us2=explode("|",$us);
					$rate=$us2[0];
				}else{
					$rate=$res['payrate'];
				}
		   }
		return $rate;
}
/**
 * @todo 敏感词过滤，返回结果
 * @param array $list  定义敏感词一维数组
 * @param string $string 要过滤的内容
 * @return string $log 处理结果
 */
function sensitive($string){
  $list=explode("|",C('string'));
  $count = 0; //违规词的个数
  $sensitiveWord = '';  //违规词
  $stringAfter = $string;  //替换后的内容
  $pattern = "/".implode("|",$list)."/i"; //定义正则表达式
  if(preg_match_all($pattern, $string, $matches)){ //匹配到了结果
    $patternList = $matches[0];  //匹配到的数组
    $count = count($patternList);
    $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
    $replaceArray = array_combine($patternList,array_fill(0,count($patternList),'*')); //把匹配到的数组进行合并，替换使用
    $stringAfter = strtr($string, $replaceArray); //结果替换
  }
  if($count==0){
    return true;
  }else{
    return false;
  }
}

/*加密解密 ENCODE 加密   DECODE 解密*/
function _encrypt($string, $operation = 'ENCODE', $key = '', $expiry = 0){
	if($operation == 'DECODE') {
		$string =  str_replace('_', '/', $string);
	}
	$key_length = 4;
	$key = md5($key != '' ? $key : C('xtpass'));
	$fixedkey = md5($key);
	$egiskeys = md5(substr($fixedkey, 16, 16));
	$runtokey = $key_length ? ($operation == 'ENCODE' ? substr(md5(microtime(true)), -$key_length) : substr($string, 0, $key_length)) : '';
	$keys = md5(substr($runtokey, 0, 16) . substr($fixedkey, 0, 16) . substr($runtokey, 16) . substr($fixedkey, 16));
	$string = $operation == 'ENCODE' ? sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$egiskeys), 0, 16) . $string : base64_decode(substr($string, $key_length));
	$i = 0; $result = '';
	$string_length = strlen($string);
	for ($i = 0; $i < $string_length; $i++){
		$result .= chr(ord($string{$i}) ^ ord($keys{$i % 32}));
	}
	if($operation == 'ENCODE') {
		$retstrs =  str_replace('=', '', base64_encode($result));
		$retstrs =  str_replace('/', '_', $retstrs);
		return $runtokey.$retstrs;
	} else {	
		if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$egiskeys), 0, 16)) {
			return substr($result, 26);
		} else {
			return '';
		}
	}
}

function FtpSend($data){
	error_reporting(E_ALL);  
	$service_port = 19735;
	$address = '127.0.0.1';
	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);  
	if ($socket < 0) {  
		 return ['code'=>-1,'msg'=>"socket创建失败原因: " . socket_strerror($socket)]; die(); 
	}
	try{
	  $result = socket_connect($socket, $address, $service_port); 
	} catch (Exception $e) {
       return ['code'=>-1,'msg'=>"软件没有开启"];
       die();
     }	
	if ($result < 0) {  
	    return ['code'=>-1,'msg'=>"SOCKET连接失败原因: ($result) " . socket_strerror($result)]; exit;  
	} 
	$in = json_encode($data);
	socket_write($socket, $in, strlen($in));  
	$buf_read_data = socket_read($socket, 255, PHP_BINARY_READ);
	$buf_read_data=str_to_utf8($buf_read_data);
	socket_close($socket);
	return json_decode($buf_read_data,true);
}

function trimall($str){
    $qian=array(" ","　","\t","\n","\r");
    return str_replace($qian, '', $str);   
}
function str_to_utf8 ($str = '') {
    $current_encode = mb_detect_encoding($str, array("ASCII","GB2312","GBK",'BIG5','UTF-8')); 
    $encoded_str = mb_convert_encoding($str, 'UTF-8', $current_encode);
    return $encoded_str;
}
function request_by_other($remote_server, $post_string)
{
    $context = array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($post_string))
        );
    $stream_context = stream_context_create($context);
    $data = file_get_contents($remote_server, false, $stream_context);

    return $data;
} 
function verifypost($url){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = mb_convert_encoding($output, 'utf-8', 'GBK,UTF-8,ASCII');
		return $output;
	}

function SwichCadType($numm,$pwdd) {
        $num = strlen(trim($numm));
        $pwd = strlen(trim($pwdd));
        if ($num == 9 && $pwd == 12 && is_numeric($pwdd)) {
            return 57; //腾讯
        }
        else if ($num == 15 && $pwd == 8) {
            return 41; //盛大卡
        }else if ($num == 18 && $pwd == 16) {
            return 31; //唯品会
        }
        else if ($num == 15 && $pwd == 9) {
            return 41; //盛大卡
        }
        else if ((($num == 20 && $pwd == 19) || ($num == 16 && $pwd == 19)) && (strpos($numm,"JDE") != -1 || strpos($numm,"JDV") != -1)) {
            return 22; //京东礼品卡
        }
        else if (($num == 10 && is_numeric($numm) && $pwd == 15 && is_numeric($pwdd)) || ($num == 9 && $pwd == 6 && is_numeric($numm) && is_numeric($pwdd))) {
            return 44; //完美一卡通
        }
        else if ($num == 20 && $pwd == 12) {
            return 11; //畅游一卡通
        }
        else if ($num == 16 && $pwd == 8) {
            if (strpos($numm,"80133") != "-1" || strpos($numm,"yd") != "-1" || strpos($numm,"yc") != "-1" || strpos($numm,"sh") != "-1") {
                return 61; //盛付通一卡
            }else {
                return 12; //巨人一卡通
            }
        }
        else if ($num == 12 && $pwd == 6) {
            return 47; //久游一卡通
        }
        else if ($num == 13 && $pwd == 10) {
            return 47; //久游一卡通
        }
        else if ($num == 13 && $pwd == 9 ) {
			 if(is_numeric($pwd)){
                return 42; //网易一卡通
			 }else{
				 return 19;//金山一卡通
			 }
        }
        else if ($num == 20 && $pwd == 8) {
            return 58; //光宇一卡通
        }
        else if ($num == 17 && $pwd == 18) {
            return 13; //神州行充值卡
        }
        else if ($num == 15 && $pwd == 19) {
            return 14; //联通充值卡
        }
        else if ($num == 10 && $pwd == 12) {
            return 60; //天宏一卡通
        }
        else if ($num == 12 && $pwd == 15) {
            return 60; //天宏一卡通
        }
        else if ($num == 15 && $pwd == 8) {
            return 59; //天下一卡通
        }
        else if ($num == 19 && $pwd == 18) {
            return 15; //电信
        }
        else if ($num == 16 && $pwd == 20) {
			$e=substr( $num,0,1);
			if($e!="E"){
               return 63; //中石化加油卡
			}else{
				return 28; //同程旅游卡
			}
        }
        else if ($num == 16 && $pwd == 16 && strpos($numm,"GCA")===false) {
			$e=substr( $num,0,1);
			if($e==9 || $e==8){
				return 20;//骏网话费通
			}else{
              return 10; //骏网一卡通
		    }
        }
		else if ($num == 15 && $pwd == 15) {
            return 56; //纵游一卡通
        }
		else if ($num == 15 && $pwd == 10) {
            return 16; //32一卡通
        }else if ($num == 16 && $pwd == 16 && strpos($numm,"GCA")!==false) {
			return 17; //苹果卡
		}
		else if ($num == 16 && $pwd == 19) {
			return 21; //骏网全业务卡商通自游卡
		}
		else if ($num == 19 && $pwd == 6 && strpos($numm,"2326")===false && strpos($numm,"2336")===false && strpos($numm,"2396")===false) {
			return 23; //商通自游卡
		}else if($num == 12 && $pwd == 15 && strpos($numm,"CY")!==false && strpos($numm,"2326")===false){
			return 32; //天宏畅付卡
		}else if($num == 19 && $pwd == 6 && strpos($numm,"2326")!==false){
          return 66;//袄尔玛
        }else if($num == 19 && $pwd == 6 && strpos($numm,"2336")!==false){
          return 67;//家乐福
        }else if($num == 19 && $pwd == 6 && strpos($numm,"2396")!==false){
          return 69;//500寺库
        }else if($num == 16 && $pwd == 6  ){
          return 68;//苏宁
        }
		
        return 0;
    }
function sheng($id,$user){
	$url="http://op.juhe.cn/idcard/query";
	$arr=['idcard'=>$id,
	      'realname'=>$user,
		  'key'=>'298eaeb0848c562019047e28ee2567aa'];
    $res=vpost($url,$arr);
	return json_decode($res,true);
}
 function send($phone,$str){
	 header("Content-Type:text/html;charset=utf-8");
	$smsapi = "http://api.smsbao.com/";
	$user = "17637209788"; //短信平台帐号
	$pass = md5("456477"); //短信平台密码
	$content=$str;//要发送的短信内容
	// file_put_contents("C:/wwwroot/yhs.haochiyidian.top/send.txt",$content);
	$sendurl = $smsapi."sms?u=".$user."&p=".$pass."&m=".$phone."&c=".urlencode($content);
	$result =file_get_contents($sendurl);
	if($result==0){
		return array('code'=>0,'msg'=>'发送成功');
	}else{
		return array('code'=>1,'msg'=>'发送失败');
	}

 }
 
 
 function send_bak($phone,$str){
	 header("Content-Type:text/html;charset=utf-8");
		$apikey = C('dxkey'); //修改为您的apikey(https://www.yunpian.com)登录官网后获取
		$mobile = $phone; //请用自己的手机号代替
		$text=$str;
		$ch = curl_init();
		/* 设置验证方式 */
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept:text/plain;charset=utf-8',
			'Content-Type:application/x-www-form-urlencoded', 'charset=utf-8'));
		/* 设置返回结果为流 */
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		/* 设置超时时间*/
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		/* 设置通信方式 */
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		
		$data=array('text'=>$text,'apikey'=>$apikey,'mobile'=>$mobile);
		$json_data = psend($ch,$data);

		$array = json_decode($json_data,true);
		return $array;

 }
 
 function paybank($n=""){
	$arr=['','微信钱包',"银行卡提现",'银行卡快速提现','支付宝提现'];
	return isset($arr[$n])?$arr[$n]:"账号类型错误";
}
  function psend($ch,$data){
    curl_setopt ($ch, CURLOPT_URL, 'https://sms.yunpian.com/v2/sms/single_send.json');
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    $result = curl_exec($ch);
    $error = curl_error($ch);
    return $result;
}
function sendMail($to, $subject = '', $body = '') {
    Vendor('MailApi.PHPMailerAutoload');
    $mail = new \PHPMailer(); //实例化
    $mail->IsSMTP(); // 启用SMTP
    $mail->Host = C('emailAps'); //SMTP服务器 以126邮箱为例子
    $mail->Port = C('emailDuam');  //邮件发送端口
    $mail->SMTPAuth     =    'TRUE';                    //启用smtp认证
    $mail->SMTPSecure = "ssl";   // 设置安全验证方式为ssl,开启这个需要服务器php里面开启 ssl扩展
    $mail->CharSet = "UTF-8"; //字符集
    $mail->Encoding = "base64"; //编码方式
    $mail->Username = C('emailName');  //你的邮箱
    $mail->Password = C('emailKey');  //你的邮箱第三方密码，需要在邮箱设置里面获取
    $mail->From = C('emailName');  //发件人地址（也就是你的邮箱）
    $mail->FromName = C('title');  //发件人姓名
	$mail->AddAddress($to, "尊敬的客户"); //添加收件人（地址，昵称）
    $mail->IsHTML('TRUE');                              //是否HTML格式邮件
	$mail->Subject = $subject;
	$mail->Body = $body; //邮件主体内容
 return $mail->Send();
}


function huoip($ip){
	$ak = 'CW8hGeIza1cS3ZOl0oeaUEpRf8rhSF8p';
	$sk = '09GZMeH08GaNzcU5cSDRGUelVb6ob9K5';
	$url="http://api.map.baidu.com/location/ip?ip=%s&ak=%s&sn=%s";
	$uri="/location/ip";
	$querystring_arrays = array (
	'ip' => $ip,
	'ak' => $ak
    );
	$sn = caculateAKSN($sk,$uri,$querystring_arrays);
	$target = sprintf($url, $ip, $ak, $sn);
	$querystring_arrays['sn']=$sn;
    $html=vpost("http://api.map.baidu.com/location/ip",$querystring_arrays);
	$arr=json_decode($html,true);
    if((int)$arr['status']>0){
		logt("获取IP地址错误：{$arr['status']}");
		return "未知地址";
	}else{
		return  $arr['content']['address'];
	}
}

function caculateAKSN($sk,$url,$querystring_arrays, $method = 'POST')
{  
    if ($method === 'POST'){  
        ksort($querystring_arrays);  
    }  
    $querystring = http_build_query($querystring_arrays);  
    return md5(urlencode($url.'?'.$querystring.$sk));  
}
function strfen($str,$n=26){
	$num=strlen($str);
	if($num>$n){
		$str=mb_substr($str,0,26,'UTF-8');
		$str.=".....";
	}
	return $str;
}
function phyin($tel){
	$pattern = "/(\d{3})\d\d(\d{2})/";
    $replacement = "\$1****\$3";
    return preg_replace($pattern, $replacement, $tel);
}
function isDateTime($dateTime){
    $ret = strtotime($dateTime);
    return $ret !== FALSE && $ret != -1;
}
function filterEmoji($str)
{
  $str = preg_replace_callback(
    '/./u',
    function (array $match) {
      return strlen($match[0]) >= 4 ? '' : $match[0];
    },
    $str);

  return $str;
}

function emoji_filter($nickname) {
    $nickname = preg_replace('/[\x{1F600}-\x{1F64F}]/u', '', $nickname);
 
    $nickname = preg_replace('/[\x{1F300}-\x{1F5FF}]/u', '', $nickname);
 
    $nickname = preg_replace('/[\x{1F680}-\x{1F6FF}]/u', '', $nickname);
 
    $nickname = preg_replace('/[\x{2600}-\x{26FF}]/u', '', $nickname);
 
    $nickname = preg_replace('/[\x{2700}-\x{27BF}]/u', '', $nickname);
 
    $nickname = str_replace(array('"','\''), '', $nickname);
 
    return addslashes(trim($nickname));
}
function vpost($url, $data = array(),$ssl=false) {// 模拟提交数据函数
    $curl = curl_init();
    // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url);
    // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    // 自动设置Referer
    curl_setopt($curl, CURLOPT_POST, 1);
    // 发送一个常规的Post请求
    @curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 8);
	if($ssl){
		curl_setopt($curl, CURLOPT_SSLCERTTYPE, 'PEM');//证书类型
        curl_setopt($curl, CURLOPT_SSLCERT, ROOT_PATH . C('wxcert'));//证书位置 cert
        curl_setopt($curl, CURLOPT_SSLKEYTYPE, 'PEM');//CURLOPT_SSLKEY中规定的私钥的加密类型
        curl_setopt($curl, CURLOPT_SSLKEY, ROOT_PATH . C('wxfkey'));//证书位置key
	}
    // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0);
    // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl);

    // 执行操作
    if (curl_errno($curl)) {
        return 'Errno' . curl_error($curl);
        //捕抓异常
    }else{
    curl_close($curl);
    // 关闭CURL会话
    return $tmpInfo;
	}
    // 返回数据
}
function fenpost($url, $data = array()) {// 模拟提交数据函数
    $headers = ['fenzhan:'.C('zid')];
    $curl = curl_init();
    // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url);
    // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    // 自动设置Referer
    curl_setopt($curl, CURLOPT_POST, 1);
    // 发送一个常规的Post请求
    @curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 8);
	
    // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl);
    // 执行操作
    if (curl_errno($curl)) {
        return ['code'=>-1,'msg'=>'Errno' . curl_error($curl)];
        //捕抓异常
    }else{
    curl_close($curl);
    // 关闭CURL会话
    return ['code'=>1,'msg'=>json_decode($tmpInfo,true)];
	}
    // 返回数据
}
 function arraytoxml($data){
		$str='<xml>';
		foreach($data as $k=>$v) {
			$str.='<'.$k.'>'.$v.'</'.$k.'>';
		}
		$str.='</xml>';
		return $str;
	}
 function xmltoarray($xml) { 
		//禁止引用外部xml实体 
		libxml_disable_entity_loader(true); 
		$xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA); 
		$val = json_decode(json_encode($xmlstring),true); 
		return $val;
	}

function logt($word='') {
	$fp = fopen("log.txt","a");
	flock($fp, LOCK_EX) ;
	if(is_array($word)){
		foreach($word as $k=>$v){
			if(!is_array($k) && !is_array($v)){
			fwrite($fp,$k.">>>>".$v."\n");
			}
		}
	}else{
	fwrite($fp,"执行日期：".strftime("%Y%m%d%H%M%S",time())."\n".$word."\n");
	}
	flock($fp, LOCK_UN);
	fclose($fp);
}
function isWeixin() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    } else {
        return false;
    }
  }
  function tfen($str,$s,$e){
	 if($str){
		 $st="";
		 for($i=0;$i<$e;$i++){
			 $st.="*";
		 }
		$str=substr_replace($str,$st,$s,$e);
		return $str;
	 }else{
		 return "";
	 }
}
 function wx_gettoken($Appid, $Appkey,$openid) {
    $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$Appid}&secret={$Appkey}";
    $html = vpost($url);
    $json = json_decode($html, 1);
    $access_token = isset($json['access_token'])?$json['access_token']:"";
	$res=[];
	if($access_token){
		$res=wx_getinfo($access_token, $openid);
	}
    return $res;
}    
function wx_getinfo($token, $openid) {
	
    $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$token}&openid={$openid}&lang=zh_CN";
    $html = vpost($url);
    $json = json_decode($html, 1);
    $nickname = $json['nickname'];
    $headhtml = $json['headimgurl'];
    return array("nickname" => $nickname, 'headimg' => $headhtml);
}

function dxb(){
	$smsapi = "https://www.smsbao.com/query";
    $user = C("mobile.dx1_pid"); //短信平台帐号
    $pass = md5(C("mobile.dx1_pass")); //短信平台密码
    $sendurl = $smsapi."?u=".$user."&p=".$pass;
    $result =file_get_contents($sendurl);
	$code=explode(",",$result);
    return isset($code[1])?$code[1]:0;
}

function sendapim($photo,$str){
	
    $smsapi = "http://api.jisuapi.com/sms/send";
    $pass = C("mobile.dx1_pass"); //key
    $sendurl = $smsapi."?mobile=".$phone."&appkey=".$pass."&content=".$str;
    $result =file_get_contents($sendurl);
	$result=json_decode($result,true);
    return ['msg'=>$result['msg'],'code'=>$result['status']];
}

function duanType($photo,$code,$type){
	return sendmobile($photo,$code,$type);
}

function sendmobile($photo,$code,$type){
	switch($type){
		case 'reg':
		 $str="欢迎你注册收款宝，你的验证码是{$code}【收款宝】 ";
		break;
		case 'yue':
		$str="尊敬的用户，你的平台余额不足10元，请尽快充值，以免影响数据返回【".C('title')."】";
		break;
		case 'xgmm':
		 $str="你正在进行密码修改，请不要随意透露你的验证码，验证码是{$code}【".C('title')."】";
		break;
		case 'zhaohui':
		 $str="本次找回密码操作的验证码是{$code}【".C('title')."】";
		break;
		default:
		$str="您的手机验证码为{$code}，5分钟内有效。请不要把此验证码泄露给任何人。【收款宝】";
	}
    $smsapi = "http://api.jisuapi.com/sms/send";
    $pass = C("dxkey"); //短信平台密码
    $content=$str;//要发送的短信内容
    $phone = $photo;//要发送短信的手机号码
    $sendurl = $smsapi."?mobile=".$phone."&appkey=".$pass."&content=".$content;
    $result =file_get_contents($sendurl);
	$result=json_decode($result,true);
    return ['msg'=>$result['msg'],'code'=>$result['status']];
}

function sendaldy($photo,$code,$type){
	switch($type){
		case "reg":
		  $content="【易收款】欢迎你注册收款宝，你的验证码是{$code}";
		break;
		default:
		  $content="【易收款】本次操作的验证码是{$code}";
	}
	$statusStr = array("0" => "短信发送成功","-1" => "参数不全","-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！","30" => "密码错误","40" => "账号不存在","41" => "余额不足","42" => "帐户已过期","43" => "IP地址限制","50" => "内容含有敏感词");
    $smsapi = "http://api.smsbao.com/";
    $user = C("dxpid"); //短信平台帐号
    $pass = md5(C("dxkey")); //短信平台密码
    $phone = $photo;//要发送短信的手机号码
    $sendurl = $smsapi."sms?u=".$user."&p=".$pass."&m=".$phone."&c=".urlencode($content);
    $result =file_get_contents($sendurl);
    return ['msg'=>$statusStr[$result],'code'=>$result];
}

function smsApi($phone,$content){
	$smsapi = "http://api.jisuapi.com/sms/send";
    $pass = C("dxkey"); //短信平台密码
    $sendurl = $smsapi."?mobile=".$phone."&appkey=".$pass."&content=".$content;
    $result =file_get_contents($sendurl);
	$result=json_decode($result,true);
    return ['msg'=>$result['msg'],'code'=>$result['status']];
}

function paytype($t){
	$arr=array("<p class='text-danger'>未支付</p>","<p class='text-success'>支付成功</p>","<p class='text-warning'>订单超时</p>");
	return isset($arr[$t])?$arr[$t]:"";
}
function txtype($t){
	$arr=array("<p class='text-warning'>未支付</p>","<p class='text-success'>支付成功</p>","<p class='text-danger'>风控提醒</p>");
	return isset($arr[$t])?$arr[$t]:"";
}
function dxRes($res){
	$arr=explode("|",$res);
	$nm=[];
	foreach($arr as $k=>$v){
		$str=explode(",",$v);
		$nm[$str[0]]=$str[1];
	}
	return $nm;
}

function generate_password( $length = 8 ) { 
	 $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	 $password = "";  
	 for ( $i = 0; $i < $length; $i++ ){ 
		 $password .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
		 $password .= $chars[ mt_rand(0, strlen($chars)-1) ];  
		 }  
		 return $password;
	}
		 
function GetDisk(){ //获取硬盘情况
        $t =round(@disk_total_space(".")/(1024*1024*1024),3);
        $f =round(@disk_free_space(".")/(1024*1024*1024),3);
        return sprintf("%.2f",(1-($f/$t))*100);
    }
//获取用户真实IP 
function getIp() { 
     if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) 
         $ip = getenv("HTTP_CLIENT_IP"); 
     else 
         if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) 
             $ip = getenv("HTTP_X_FORWARDED_FOR"); 
         else 
             if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) 
                 $ip = getenv("REMOTE_ADDR"); 
             else 
                 if (isset ($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) 
                     $ip = $_SERVER['REMOTE_ADDR']; 
                 else 
                     $ip = "unknown"; 
     return ($ip); 
 }
 //获取来路域名
 function domei(){
	$url=isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:"";
	$str = str_replace("http://","",$url); //去掉http://
    $strdomain = explode("/",$str); // 以“/”分开成数组
	if($strdomain[0]=="localhost"){
		return "127.0.0.1";
	}else{
       return $strdomain[0];
	}
 }
 
 //是否手机端
 function isMobile(){
//		$_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
//
//		$mobile_browser = '0';
//
//		if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
//				$mobile_browser++;
//		}
//		if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false)) {
//				$mobile_browser++;
//		}
//		if(isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
//				$mobile_browser++;
//		}
//		if(isset($_SERVER['HTTP_PROFILE'])) {
//				$mobile_browser++;
//		}
//		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
//		$mobile_agents = array(
//												'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
//												'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
//												'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
//												'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
//												'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
//												'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
//												'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
//												'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
//												'wapr','webc','winw','winw','xda','xda-'
//												);
//
//		if(in_array($mobile_ua, $mobile_agents)) {
//				$mobile_browser++;
//		}
//		if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false) {
//				$mobile_browser++;
//		}
//		// Pre-final check to reset everything if the user is on Windows
//		if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false) {
//				$mobile_browser=0;
//		}
//		// But WP7 is also Windows, with a slightly different characteristic
//		if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false) {
//				$mobile_browser++;
//		}
//		if($mobile_browser>0) {
//				return true;
//		} else {
//				return false;
//		}
     return true;

 }
function createLinkstring($para) {
		$arg  = "";
	while (list ($key, $val) = each ($para)) {
		$arg.=$key."=".$val."&";
	}
	//去掉最后一个&字符
	$arg = substr($arg,0,count($arg)-2);
	
	//如果存在转义字符，那么去掉转义
	if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
	
	return $arg;
	}
    /*去掉字符空值*/
function paraFilter($para){
		$para_filter = array();
		while (list ($key, $val) = each ($para)){
			if($key == "sign")continue;
			else
				$para_filter[$key] = $para[$key];
			}
			return $para_filter;
			}
   /*数组排序*/
function argSort($para){
	   ksort($para);
	   reset($para);
	   return $para;
	   }
function md5Verify($prestr,$sign=null,$key) {
	   $para=paraFilter($prestr);
	   $parm=argSort($para);
	   $prestr=createLinkstring($parm);
	   $prestr = $prestr.$key;
	   $mysgin = md5($prestr);

	   if($sign){
	   if($mysgin == $sign) {
		      return true;
		   }else{
			  return false;
			   }
	   }else{
		   return $mysgin;
	   }
	}

function pay_order($dingdanzhui=''){
	return $dingdanzhui.time().substr(microtime(),2,6).rand(0,3);
	}
//管理员组
function Adz($s=0){
	$list=AuthGroup::all(['status'=>1]);
	$arr='';
	foreach($list as $k=>$v){
		if($s==$v['group_id']){
			$arr.='<input type="radio" name="diction" lay-skin="primary" title="'.$v['title'].'" value="'.$v['group_id'].'" checked="checked">';
		}else{
		   $arr.='<input type="radio" name="diction" lay-skin="primary" title="'.$v['title'].'" value="'.$v['group_id'].'">';
		}
	}
	return $arr;
}
function makeToken()
    {
        $str = md5(uniqid(md5(microtime(true)), true)); //生成一个不会重复的字符串
        $str = sha1($str); //加密
        return $str;
    }
function Nstate($s=0){
	$list=newstype(1,false);
	$arr='';
	$arr.="<select name='type'>";
	$arr.='<option value="'.$s.'">无分类</option>';
	foreach($list as $k=>$v){
		if($s==$k){
			$arr.='<option value="'.$k.'" selected="">'.$v.'</option>';
		}else{
		   $arr.='<option value="'.$k.'" >'.$v.'</option>';
		}
	}
	$arr.="</select>";
	return $arr;
}
function payT(){
	$list=['alipay'=>'支付宝','wxpay'=>'微信'];
	$arr='';
	$arr.="<select name='type'>";
	foreach($list as $k=>$v){
		   $arr.='<option value="'.$k.'" >'.$v.'</option>';
	}
	$arr.="</select>";
	return $arr;
}
function stateS($a){
	$arr=['<span class="text-dark-blue">正在处理</span>','<span class="text-dark-green">回收成功</span>','<span class="text-red">回收失败</span>'];
	return $arr[$a];
}
function level($uid){
	$s=1;
	$u=(new User)->where(['id'=>$uid])->find();
	if($u){
		if($u['photo'])$s=$s+2;
		if($u['jiaoyimima'])$s=$s+2;
		if($u['email'])$s=$s+2;
	}
	return $s;
}
function tijiao($n){
	 $arr['100001']="系统维护";
	 $arr['100002']="必要参数为空";
	 $arr['100003']='卡号、卡密超过失败次数限制';
	 $arr['100004']='商户不存在';
	 $arr['100005']='商户被关闭';
	 $arr['100006']='时间戳不合法（要10分钟之内）';
	 $arr['100055']='未设置密钥';
	 $arr['100007']='产品不存在';
	 $arr['100008']='产品暂停销售';
	 $arr['100009']='商户没有开通该产品';
	 $arr['100010']='不支持的面值';
	 $arr['100011']='签名错误';
	 $arr['100012']='卡密已成功（已有成功记录）';
	 $arr['100013']='卡密正在处理中';
	 $arr['100014']='订单号已存在';
	 $arr['100015']='该产品的运营商费率不存在或未开通';
	 $arr['100016']='卡号、卡密不符合规则';
	 $arr['100017']='回调地址格式、订单号长度、扩展信息长度不合法（回调地址格式为URL格式，长度不能超过255个字符、订单号32个数字字符以内、扩展信息不能超过50个字符）';
	 $arr['100077']='卡种为空';
	 $arr['100018']='输入的折扣区间不合法';
	 $arr['100019']='不支持的电信地方卡';
	 $arr['100099']='商户编号不能为空';
	 $arr['100020']='销卡有效期不合法(时间要为今天起3天之内的日期，格式为yyyy-MM-dd)';
	 $arr['100021']='系统错误，可重新提交或联系客服';
	 $arr['100022']='该卡号已被锁定，请联系客服';
	 $arr['100023']='请求IP不在白名单';
	 $arr['100024']='系统繁忙';
	 $arr['100025']='无法判断充值卡发行地区';
	 $arr['100026']='充值卡预判面值与提交面值不符';
	 $arr['100027']='卡号密码错误';
	 $arr['100028']='卡号密码加解密失败';
	 $arr['100029']='该订单支付已失败，不能重复提交';
	 $arr['100030']='提交失败，可以重新提交';
	 $arr['100031']='金额小于0或金额格式不正确';
	 $arr['100032']='校验码验证失败';
	 $arr['100033']='订单相关错误';
	 $arr['100034']='收卡失败';
	 $arr['100035']='合作方账号类型或者状态错误';
	 $arr['100036']='合作方资金账户错误';
	 $arr['100037']='合作方余额不足等错误';
	 $arr['100038']='合作方支付错误';
	 $arr['100039']='合作方没有该业务权限';
	 $arr['100040']='商品状态相关错误';
	 $arr['100041']='充值卡无效';
	 $arr['100042']='处理失败，卡密没有消耗';
	 $arr['100043']='系统异常，需要核实';
	 $arr['100055']='时间戳不合法';
	 $arr['100099']='商户编号不能为空';
	 $arr['100044']='该卡号属于被禁止提交的类型，详情请联系客服咨询';
	 $arr['100045']='卡号或密码不正确或已经使用完了';
	 $arr['100046']='卡中余额不足';
	 return isset($arr[$n])?$arr[$n]:"";
}
function fanhui($n){
	$arr['000000']='兑换成功';
	$arr['000001']='兑换成功，充值卡实际大于订单金额';
    $arr['000002']='兑换成功，充值卡实际小于订单金额';
    $arr['000003']='错选订单，充值卡实际小于订单金额，兑换失败，充值卡内金额丢失';
    $arr['000004']='金额格式异常';
    $arr['000005']='订单支付已成功，请勿重复提交';
    $arr['000006']='运营商系统维护，支付通道暂时关闭';
    $arr['000007']='运营商系统维护，该面值暂时关闭';
    $arr['000008']='商户没有开通此支付通道，请联系客服';
    $arr['000009']='商户不支持余额卡支付，请联系客服';
    $arr['000010']='订单支付已失败，不能重复提交';
    $arr['000011']='支付失败，该卡已失效，已被锁卡';
    $arr['000012']='失败次数过多，已经锁卡，如需重试请联系客服解锁';
    $arr['000013']='充值卡正在处理中,不能重复提交';
	$arr['000014']='系统处理超时，可以重新提交';
	$arr['000015']='当前商户不支持多卡支付';
	$arr['000016']='面额选择错误';
	$arr['000017']='订单正在处理中，请勿重复提交';
	$arr['000018']='输入参数有误';
	$arr['000019']='运营商系统临时维护，该省充值卡暂时无法支付，可继续提交，且不影响其它省充值卡支付';
	$arr['000020']='卡号密码失效';
	$arr['000021']='运营商系统处理失败，该卡可再次提交';
	$arr['000022']='充值卡金额不足，无法支付订单';
	$arr['000023']='无效的充值卡号或密码';
	$arr['000024']='暂不支持该充值卡支付';
	$arr['000025']='充值卡号密码加解密失败';
	$arr['000026']='订单不存在';
	$arr['000027']='充值卡超时未使用';
	$arr['000028']='充值卡已使用';
	$arr['000029']='第三方运营商官方维护';
	$arr['000030']='充值卡可疑，可联系客服处理';
	$arr['000031']='余额不足';
	$arr['000032']='未使用';
	$arr['000033']="充值卡号、卡密正在处理中";
	$arr['000034']='该充值卡已有成功记录';
	$arr['000035']='该充值卡类型不存在';
	$arr['000036']="第三方销卡系统处理失败，请联系客服了解具体失败信息";
	return isset($arr[$n])?$arr[$n]:"";

}
function typed($v,$r=true){
	$list = db('category')->where('id','=',$v)->find();
	return $list['name'];
	/*
	foreach($list as $v1){
		$arr[] = $v1['name'];
	}
	//$arr=['影视会员','音乐读书','','美食出行','联通卡密','其他卡类','电商卡'];
	if($r){
	  return $arr[$v];
	}else{
		return $arr;
	}
	*/
}
 function cardstate($num){
	   $ty=array('处理中','已成功','已失败',"未处理","已退票");
	   if(isset($ty[$num])){
		   return $ty[$num];
	   }else{
		   return '未知状态';
	   }
 }
function cardkey($ar){
	$str="";
	if(is_array($ar)){
		foreach($ar as $v){
			$str.="[";
			if($v['cardNumberLength'])$str.="卡号:".$v['cardNumberLength']."位";
			if($v['cardPasswordLength'])$str.="|卡密:".$v['cardPasswordLength']."位";
			$str.="] ";
		}
	}
	return $str;
}
function newsType($s,$r=true){
	$arr=[1=>'卡卷介绍',2=>'卡卷购买',3=>'卡卷使用',4=>'卡卷查询',5=>'卡卷回收',6=>'行业资讯',7=>'常见问题',8=>'公告'];
	if($r){
	   return isset($arr[$s])?$arr[$s]:'无此分类';
	}else{
		return $arr;
	}
}
//生成密码和验证

function md6($str,$code=null){
	if($code==null){
		return password_hash($str, PASSWORD_DEFAULT);
	}else{
		return password_verify($str, $code);
	}
}
