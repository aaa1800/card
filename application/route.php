<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [

   "/"=>'home/index/index',
   
   //index
   'whelp'=>'home/index/help',
   'recycle'=>'home/index/recycle',
   'rtweixin'=>'home/index/weixin',
   'retrievet'=>'home/index/retrieve',
   'inviteFriends'=>'home/index/inviteFriends',
   'faqww'=>'home/index/faq',
   'gonggao'=>'home/index/gongg',
   'cardArticle'=>'home/index/cardArticle',
   'newsDetail'=>'home/index/newsDetail',
   'getMaxBatchSubmitCount'=>'home/index/getMaxBatchSubmitCount',
   'getRecycleOperatorsByProductClassifyId'=>'home/index/getRecycleOperatorsByProductClassifyId',
   'getRecentConsumeDuration'=>'home/index/getRecentConsumeDuration',
   'getCardRules'=>'home/index/getCardRules',
   'getRecyclePricesByOperatorId'=>'home/index/getRecyclePricesByOperatorId',
   'enterprise'=>'home/index/enterprise',
   'myMessage'=>'home/index/myMessage',
   'sellFlow'=>'home/index/sellFlow',
   'getIndexSlides'=>'home/index/getIndexSlides',
   'getProductClassifiesList'=>'home/index/getProductClassifiesList',
   'getOperatorsList'=>'home/index/getOperatorsList',
   'getBaseInfo'=>'home/index/getBaseInfo',
   'productClassify'=>'home/index/productClassify',
   'mgetCardRules'=>'home/index/mgetCardRules',
   'getRateRangeInfo'=>'home/index/getRateRangeInfo',
   'autoSend'=>'home/index/autoSend',
   'newsw'=>'home/index/newsw',
   
	'isParticipateNewMemberActivity'=>'home/index/isParticipateNewMemberActivity',
   //login
   'loginIndex'=>'home/login/index',
   'registered'=>'home/login/registered',
   'sendCode'=>'home/login/sendCode',
   'huiCode'=>'home/login/huiCode',
   'checkCode'=>'home/login/regCode',
   'userreg'=>'home/login/regAdd',
   'logino'=>'home/login/loginout',
   'retrieve'=>'home/login/retrieve',
   'findSetp'=>'home/login/findSetp',
   'findEdit'=>'home/login/findEdit',
   'verifyInviteNumber'=>'home/login/verifyInviteNumber',
  
   //api
   'sendsms'=>'home/api/sendsms',
   'sendf'=>'home/api/sends',
   'fassd'=>"home/api/facode",
   'bindtel'=>'home/api/bindtel',
   'jiaoyimima'=>'home/api/jiaoyimima',
   'preCode'=>'home/api/preCode',
   'asssendEmail'=>'home/api/sendEmail',
   'autosen125'=>'home/api/batchCard',
   'retijiao'=>'home/api/retijiao',
   'findcode'=>'home/api/findcode',
   'premCode'=>'home/api/premCode',
   'setpass'=>'home/api/setpass',
   'setpassa'=>'home/api/setpassa',
   'mach'=>'home/api/mach',
   'apity'=>'home/api/apiti',
  'callback'=>'home/api/callback',
  'getList'=>'home/api/getList',
  'tocard'=>"home/api/tosend",
  'toweixin'=>'home/api/weixin',
   
   
   //order
     'batch'=>'home/order/batchCardIndex',
	 'single'=>'home/order/singleCardIndex',
	 'ClassifyId'=>'home/order/ClassifyId',
	 'getSellCardOrdersDetails'=>'home/order/getSellCardOrdersDetails',
	 'mbatchCardIndex'=>'home/order/mbatchCardIndex',
	 'msingleCardIndex'=>'home/order/msingleCardIndex',
	 'getSellCardOrdersDetailsa'=>'home/order/getSellCardOrdersDetailsa',
   
   //member
   'member'=>'home/member/index',
   'userinfo'=>'home/member/userinfo',
   'addpwd'=>'home/member/addpwd',
   'recharge'=>"home/member/chonglog",
   'settle'=>'home/member/settle',
   'moneylog'=>'home/member/moneylog',
   'getDart'=>'home/member/getDa',
   'sendT'=>'home/member/sendT',
   'addTel'=>'home/member/addTel',
   'setKey'=>'home/member/setKey',
   'kaiTx'=>'home/member/kaiTx',
   'setApi'=>'home/member/setApi',
   'editPass'=>'home/member/editPass',
   'mdtixian'=>'home/member/tixian',
   'paygo'=>'home/member/Recharge',
   'sendemail'=>'home/member/sendemail',
   'editeamil'=>'home/member/editemail',
   'main'=>'home/member/maina',
   'sell'=>'home/member/sellCard',
   'batchCard'=>'home/member/batchCardIndex',
   'singleCard'=>'home/member/singleCardIndex',
   'AuthenInit'=>'home/member/AuthenInit',
   'bankAccount'=>'home/member/bankAccount',
   'aliPay'=>'home/member/aliPay',
   'weixinx'=>'home/member/weixin',
   'qq'=>'home/member/qqbind',
   'wxin'=>'home/member/wxbind',
   'getDetail'=>'home/member/getDetail',
   'addbank'=>'home/member/addbank',
   'addali'=>'home/member/addali',
   'deleteBankCard'=>'home/member/deleteBankCard',
   'addpass'=>"home/member/addpass",
    'editTran'=>'home/member/editTran',
	'getMemberLimitAmount'=>'home/member/getMemberLimitAmount',
	'smain'=>'home/member/smain',
	'infoo'=>'home/member/info',
	'profileManage'=>'home/member/profileManage',
	'tixianbank'=>'home/member/tixianbank',
	'getBank'=>'home/member/getBank',
	'passwordManageInit'=>'home/member/passwordManageInit',
	'resetTradePasswordInit'=>'home/member/resetTradePasswordInit',
	'addqq'=>'home/member/addqq',
	'setting'=>'home/member/setting',
	'getJuniorAuthenStatus'=>'home/member/getJuniorAuthenStatus',
	'AuthenInitQiye'=>'home/member/AuthenInitQiye',
	'Qiyeup'=>'home/member/Qiyeup',
	'realhelp'=>'home/member/realhelp',
	'autoSendme'=>'home/member/autoSend',
	'zhipai'=>'simple/member/zhipai',
	'addemail'=>'home/member/addemail',
	'gaoji'=>'home/member/AuthenInitGaoji',
	'Gaojiup'=>'home/member/Gaojiup',
	'addweixin'=>'home/member/addweixin',
   
   //moneylog
   'getAccountStatementRecords'=>'home/moneylog/getDa',
   'banklisted'=>'home/moneylog/banklist',
   'tixianlog'=>'home/moneylog/tixian',
   'asset'=>'home/moneylog/asset',
   'mgetDa'=>'home/moneylog/mgetDa',
   'mtixian'=>'home/moneylog/mtixian',
   'masset'=>'home/moneylog/masset',
   'bbjklop'=>'home/moneylog/mbanklist',
   
   //mobile
  'mmobile'=>'home/Mobile/mindex',
  'mlog'=>'home/Mobile/mlog',
  'paylog'=>'home/Mobile/paylog',
  'kaiDx'=>'home/Mobile/kaiDx',
  'gou'=>'home/Mobile/gou',
  'add'=>'home/Mobile/add',
  'cha'=>'home/Mobile/cha',
  'tplDel'=>'home/Mobile/tplDel',
  'restDx'=>'home/Mobile/restDx',
  
  //Gong
  'getHeadNotice'=>'home/Gong/getHeadNotice',
  'getArticlePageData'=>'home/Gong/getArticlePageData',
  'getFaq'=>'home/Gong/getFap',
  'getHeadNotices'=>'home/gong/getHeadNotices',
  'indexhj'=>'home/gong/index',
  'getHelpList'=>'home/gong/getHelpList',
  
  //daili
  'daili'=>'home/Daili/daili',
  'subordinate'=>'home/Daili/subordinate',
  'qrcode'=>'home/daili/qrcode',
  'tixian'=>'home/daili/tixian',
  
  //Substation
  'suindex'=>'home/Substation/suindex',
  'sumy'=>'home/Substation/sumy',
    'editmyMoney'=>'simple/member/editmyMoney',
  
  
  /*simple */
  //index
  
  'simple'=>"simple/Index/login",
  'sendEmail'=>'simple/index/sendEmail',
  //orders
  'iorder'=>'simple/orders/index',
  //admin
  'index'=>'simple/admin/index',
  'welcome'=>'simple/admin/welcome',
  'adminList'=>'simple/admin/adminList',
  'adminAdd'=>'simple/admin/adminAdd',
  'adminEdit'=>'simple/admin/adminEdit',
  'adminRole'=>'simple/admin/adminRole',
  'adminRule'=>'simple/admin/adminRule',
  'roleAdd'=>'simple/admin/roleAdd',
  'ruleTz'=>'simple/admin/ruleTz',
  'ruleState'=>'simple/admin/ruleState',
  'ruleDel'=>'simple/admin/ruleDel',
  'ruleEdit'=>'simple/admin/ruleEdit',
  'ruleAdd'=>'simple/admin/ruleAdd',
  'ruleOrder'=>'simple/admin/ruleOrder',
  'roleKg'=>'simple/admin/roleKg',
  'groupDel'=>'simple/admin/groupDel',
  'roleCreate'=>'simple/admin/roleCreate',
  'editS'=>'simple/admin/editS',
  'delS'=>'simple/admin/delS',
  'getDa'=>'simple/admin/getDa',
  //system
  'sysindex'=>'simple/system/index',
  'sysweixin'=>'simple/system/weixin',
  'sysfenzhan'=>'simple/system/fenzhan',
  'news'=>'simple/system/news',
  'editNews'=>'simple/system/editNews',
  'delNews'=>'simple/system/delNews',
  'uppic'=>'simple/system/upload',
  'uploadfile'=>'simple/system/uploadfile',
  'sysemail'=>'simple/system/email',
  'sysbanner'=>'simple/system/banner',
  'tdieupload'=>'simple/system/tdieupload',
  'Software'=>'simple/system/Software',
  'alipay'=>'simple/system/alipay',
  

  
  //member
  'mlist'=>'simple/member/memberlist',
  'medit'=>'simple/member/memberEdit',
  'madd'=>'simple/member/memberAdd',
  'userDel'=>'simple/member/userDel',
  'memberFl'=>'simple/member/memberFl',
  'memberLoginLog'=>'simple/member/memberLoginLog',
  'loginDel'=>'simple/member/loginDel',
  'memberMoneyLog'=>'simple/member/memberMoneyLog',
  'moneyDel'=>'simple/member/moneyDel',
  'memberMoneyOrder'=>'simple/member/memberMoneyOrder',
  'orderDel'=>'simple/member/orderDel',
  'orderqiang'=>'simple/member/orderqiang',
  'ordersucc'=>'simple/card/ordersucc',
  'ordererr'=>'simple/card/ordererr',
  'memberMxLog'=>'simple/member/memberMxLog',
  'mxDel'=>'simple/member/mxDel',
  'orderRest'=>'simple/member/orderRest',
  'memberTxOrder'=>'simple/member/memberTxOrder',
  'txDel'=>'simple/member/txDel',
  'memberVipOrder'=>'simple/member/memberVipOrder',
  'memberWithOrder'=>'simple/member/memberWithOrder',
  'withEdit'=>'simple/member/withEdit',
  'memberYongjin'=>'simple/member/memberYongjin',
  'txrestt'=>'simple/member/txRest',
  'editMoney'=>'simple/member/editMoney',
  'memberPayOrder'=>'simple/member/memberPayOrder',
  'memberfal'=>'simple/member/memberfal',
  'qiang'=>'simple/member/qiang',
  'memberPeifu'=>'simple/member/memberPeifu',
  'printt'=>'simple/member/printt',
  'ssedistill'=>'simple/member/ssedistill',
  'guanbi'=>'simple/member/guanbi',
  'guanbiti'=>'simple/member/guanbiti',
  'adminorder'=>'simple/Orders/adminorder',
  'upall'=>'simple/member/upall',

  
  //Qiyeu
  'qiyeindex'=>'simple/qiye/index',
  'doEditAA'=>'simple/qiye/doEdit',
  'qiyeedit'=>'simple/qiye/edit',
  'picc'=>'simple/qiye/pic',
  
  //Gaoji
  'gaojiindex'=>'simple/Gaoji/index',
  'doEditgaoji'=>'simple/Gaoji/doEdit',
  'gaojiedit'=>'simple/Gaoji/edit',
  'piccgaoji'=>'simple/Gaoji/pic',
  //card
  
  'mVerify'=>'simple/card/mVerify',
  'msgEdit'=>'simple/card/msgEdit',
  'msgOrder'=>'simple/card/opaer',
  'msDel'=>'simple/card/msDel',
  'msgSend'=>'simple/card/cardAdd',
  'opearEdit'=>'simple/card/opearEdit',
  'dasucc'=>'simple/card/dasucc',
  'daerr'=>'simple/card/daerr',
  'afind'=>'simple/card/find',
  'findlist'=>'simple/card/findlist',
  'opencard'=>'simple/card/opencard',
  
    'copaer'=>'simple/category/opaer',
    'cdel'=>'simple/category/msdel',
    'cedit'=>'simple/category/opearedit',

  //domain
  'dolist'=>'simple/domain/doList',
  'doSet'=>'simple/domain/doSet',
  'dodel'=>'simple/domain/doDel',
  'doEdit'=>'simple/domain/doEdit',
  'xfList'=>'simple/domain/xfList',
  //ewm
  'pay'=>'simple/ewm/pay',
  'eSet'=>'simple/ewm/eSet',
  'ewmEdit'=>'simple/ewm/ewmEdit',
  'eDel'=>'simple/ewm/eDel',
  'ewmList'=>'simple/ewm/ewmList',
  'upfiel'=>'simple/ewm/upfiel',
  'toDel'=>'simple/ewm/toDel',
  //webjs
  
  'webjs'=>'simple/webjs/index',
  'alldel'=>'simple/webjs/delet',
  'sfdel'=>'simple/webjs/dataDel',
  'juiesuan'=>'simple/webjs/jiesuan',
  //login
  'loginout'=>'simple/login/outLogin',
  
  
  ///Api
  
  'getOrder'=>'api/api/getOrder',
  'getding'=>'api/api/getpayOrder',
  'payapi'=>'api/api/pay',
  'withdrawal'=>'api/api/tixian',
  'tosend'=>'api/api/tosend',
  'notifypay'=>'api/api/notify',
  
  
  //weixin
  'wxindex'=>'api/weixin/index',
  'wxlogin'=>'api/weixin/wxlogin',
  '__miss__'=>'home/index/miss',

];
