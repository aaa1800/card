<?php
return [
   'paginate'               => [
        'type'      => 'page\Page',
        'var_page'  => 'page',
        'list_rows' => 15,
    ],
	'view_replace_str'  => [
	    '__LIB__'    => '/static/simple/lib',
        '__CSS__'    => '/static/simple/css',
        '__JS__'     => '/static/simple/js',
		'__IMG__'     => '/static/simple/images'
     ]
];