<?php
namespace app\Simple\Controller;
use think\Controller;
use clt\Leftnav;
use app\common\model\AuthRule;
use app\common\model\AuthGroup;
use app\common\model\Admin as Ad;
use app\common\model\User;
use app\common\model\Order;
use app\common\model\MoneyOrder;
use app\common\model\YongjinLog as YJ;
use app\common\model\Webjs;
use app\common\model\Desc;

class AdminController extends CheckloginController{

	public function index(){
		
		$this->assign('ad',session('adname'));
		return $this->fetch();
	}
	public function welcome(){
		$time=(new Webjs)->order("id desc")->value("addtime");
		$atime=$time?$time:"2017/02/02 00:00:00";
		$num['user']=User::count();
		$num['d_user']=User::whereTime('addtime','d')->count();
		
		$num['card_count']=Order::count();
		$num['card_num']=Order::sum('money');
		
		$num['d_card_count']=Order::whereTime('create_time','d')->count();
		$num['d_card_num']=Order::whereTime('create_time','d')->sum('money');
		
		$num['secc_count']=Order::where(['state'=>1])->count();
		$num['secc_num']=Order::where(['state'=>1])->sum('money');
		$num['d_secc_count']=Order::where(['state'=>1])->whereTime('create_time','d')->count();
		$num['d_secc_num']=Order::where(['state'=>1])->whereTime('create_time','d')->sum('money');
		
		$num['err_count']=Order::where(['state'=>2])->count();
		$num['err_num']=Order::where(['state'=>2])->sum('money');
		$num['d_err_count']=Order::where(['state'=>2])->whereTime('create_time','d')->count();
		$num['d_err_num']=Order::where(['state'=>2])->whereTime('create_time','d')->sum('money');
		
		
		
		$num['profit']=Order::where(['state'=>1])->sum('profit');
		$num['d_profit']=Order::where(['state'=>1])->whereTime('create_time','d')->sum('profit');
		
		$this->assign("f",$num);
		return view();
	}
	public function adminList(){
		if(request()->isAjax()){
			$data= Ad::with('profile')->select();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$data,'rel'=>1];
		}
		return $this->fetch();
	}
	
	public function getDa(){
		$arr=[];
		for($i=31;$i>=0;$i--){
			$s=date('Y-m-d 00:00:00',strtotime('-'.$i.' day'));
			$e=date('Y-m-d 23:59:59',strtotime('-'.$i.' day'));
			$arr['t'][]=date('m/d',strtotime('-'.$i.' day'));
			$arr['user'][]=(new User)->where(['addtime'=>['between time',[$s,$e]]])->count();
			$arr['rech'][]=(new Order)->where(['create_time'=>['between time',[$s,$e]]])->sum('money');
			$arr['hui'][]=(new Order)->where(['create_time'=>['between time',[$s,$e]],'state'=>1])->sum('money');
			$arr['pay'][]=(new Order)->where(['create_time'=>['between time',[$s,$e]],'state'=>1])->sum('profit');
		}
		return json(['code'=>1,'data'=>$arr]);
	}
	
	//停用Admin
	public function editS(){
		if(request()->isAjax()){
			$da=input("post.");
			if((new Ad)->save(['state'=>$da['authopen']],['id'=>$da['id']])!==false){
					return json(['code' => 1, 'msg' => '设置成功!']);
				} else {
					return json(['code' => 0, 'msg' =>'设置失败！']);
				}	
		}
	}
	//删除Admin
	public function delS(){
		if(request()->isAjax()){
			$id=input("post.id");
			Ad::destroy(['id'=>$id]);
            return ['code'=>1,'msg'=>'删除成功!'];	
		}
	}
	
	public function adminAdd(){
		return $this->fetch();
	}
	public function adminEdit(){
		if(request()->isAjax()){
			$da=input("post.");
			if(!empty($da['pass'])){
				if($da['pass']!=$da['repassb']){
					return json(['code' => 0, 'msg' => '两次密码不一样!']);
					exit;
				}
				$da['password']=md6($da['pass']);
			}
			if(isset($da['id'])){
			  $ok=(new Ad)->allowField(true)->save($da,['id'=>$da['id']]);
			}else{
				$da['ip']=getIp();
				$AD=new Ad($da);
				$ok=$AD->allowField(true)->save();
			}
			if($ok){
                return json(['code' => 1, 'msg' => '添加成功!']);
            } else {
                return json(['code' => 0, 'msg' =>'保存失败！']);
            }	
		}
		$id=input("get.id");
		$act=Ad::get($id);
		$this->assign("u",$act);
		$this->assign("ok",$act?true:false);
		return $this->fetch();
	}

	public function adminRole(){
		$list=(new AuthGroup)->paginate(15);
		$this->assign('list',$list);
		return view();
	}
	public function roleCreate(){
		if(request()->isAjax()){
			$da=input();
			if(!isset($da['authids'])){
				return ['code'=>10,'msg'=>'请选择权限!'];
			}
			if(!isset($da['name'])){
				return ['code'=>10,'msg'=>'请填写名称!'];
			}
			$name=$da['name'];
			$id=$da['idd'];
			$ids=$da['authids'];
			if(empty($id)){
				$ok=AuthGroup::create(['title'=>$name,'rules'=>$ids,'addtime'=>time()]);
			}else{
				$ok=(new AuthGroup)->save(['title'=>$name,'rules'=>$ids],['group_id'=>$id]);
			}
			if($ok){
                return json(['code' => 1, 'msg' => '添加成功!']);
            } else {
                return json(['code' => 0, 'msg' =>'保存失败！']);
            }		
		}
	}
	public function groupDel(){
		$id=input('post.id');
        AuthGroup::destroy(['group_id'=>$id]);
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	public function roleKg(){
		if(request()->isAjax()){
			$da=input('post.');
			if((new AuthGroup)->where(['group_id'=>$da['id']])->update(['status'=>$da['state']])!==false){
			   return json(['code' => 1]);
            } else {
                return json(['code' => 0]);
            }
		}
	}
	public function adminRule(){
		if(request()->isAjax()){
            $nav = new Leftnav();
            $arr = cache('authRuleList');
            if(!$arr){
                $authRule = AuthRule::all(function($query){
                    $query->order('sort', 'asc');
                });
                $arr = $nav->menu($authRule);
                cache('authRuleList', $arr, 3600);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$arr,'rel'=>1];
        }
		return view();
	}
	public function roleAdd(){
		$id=input('id');
		if(request()->isAjax()){
			$nav = new Leftnav();
			$admin_rule=(new AuthRule)->field('id,pid,title,menustatus')->order('sort asc')->select();
			$rules = AuthGroup::getByGroupId($id);
			$arr=[];
			$title="";
			if($rules){
				$arr=$rules->rules;
				$title=$rules->title;
			}
			$arr = $nav->auth($admin_rule,$pid=0,$arr);
			return ["code"=> 0,"msg"=>"获取成功",'id'=>$id,'name'=>$title,"data"=>['trees'=>$arr]];
		}else{
		  $this->assign('url',U('simple/admin/roleAdd',['id'=>$id]));
		  return $this->fetch();
		}
	}
	
	
	public function ruleAdd(){
		if(request()->isPost()) {
            $datas = input('post.');
            if(AuthRule::create($datas)) {
                cache('authRule', NULL);
                cache('authRuleList', NULL);
                return json(['code' => 1, 'msg' => '添加成功!','url' => U('simple/admin/adminRule')]);
            } else {
                return json(['code' => 0, 'msg' =>'保存失败！']);
            }
        }else{
			$nav = new Leftnav();
            $arr = cache('authRuleList');
            if(!$arr){
                $authRule = AuthRule::all(function($query){
                    $query->order('sort', 'asc');
                });
                $arr = $nav->menu($authRule);
                cache('authRuleList', $arr, 3600);
            }
            $this->assign('admin_rule',$arr);//权限列表
		  return $this->fetch();
		}
	}
	public function ruleTz(){
		$id=input('post.id');
        $authopen=input('post.authopen');
        if((new AuthRule)->where(['id'=>$id])->update(['authopen'=>$authopen])!==false){
            cache('authRule', NULL);
            cache('authRuleList', NULL);
            return ['status'=>1,'msg'=>'设置成功!'];
        }else{
            return ['status'=>0,'msg'=>'设置失败!'];
        }
	}
	public function ruleState(){
        $id=input('post.id');
        $menustatus=input('post.menustatus');
        if((new AuthRule)->where(['id'=>$id])->update(['menustatus'=>$menustatus])!==false){
            cache('authRule', NULL);
            cache('authRuleList', NULL);
            return ['status'=>1,'msg'=>'设置成功!'];
        }else{
            return ['status'=>0,'msg'=>'设置失败!'];
        }
    }
	public function ruleDel(){
		$data=input('post.');
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
        AuthRule::destroy($map);
        cache('authRule', NULL);
        cache('authRuleList', NULL);
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	

    public function ruleEdit(){
        if(request()->isPost()) {
            $datas = input('post.');
			$ok=AuthRule::update($datas);
            if($ok) {
                cache('authRule', NULL);
                cache('authRuleList', NULL);
                return json(['code' => 1, 'msg' => '保存成功!', 'url' => U('simple/admin/adminRule')]);
            } else {
                return json(['code' => 0, 'msg' =>'保存失败！']);
            }
        }else{
            $admin_rule = AuthRule::get(function($query){
                $query->where(['id'=>input('id')])->field('id,href,title,icon,sort,menustatus');
            });
            $this->assign('rule',$admin_rule);
            return view();
        }
    }
	public function ruleOrder(){
        $data = input('post.');
        if(AuthRule::update($data)!==false){
            cache('authRuleList', NULL);
            cache('authRule', NULL);
            return $result = ['code'=>1,'msg'=>'排序更新成功!','url'=>U('simple/admin/adminRule')];
        }else{
            return $result = ['code'=>0,'msg'=>'排序更新失败!'];
        }
    }
	
}
?>