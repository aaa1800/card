<?php
namespace app\Simple\Controller;
use think\Controller;

use app\common\Model\Order;
use app\common\Model\User;
use app\common\Model\Withdraw;

class OrdersController extends CheckloginController{	

    public function index(){
		if(Request()->isAjax()){
			$da=input('post.');
			$list=array();
			$da['STime']=isset($da['STime'])?$da['STime']:"";
			$da['ETime']=isset($da['ETime'])?$da['ETime']:"";
			$da['Uid']=isset($da['Uid'])?$da['Uid']:"";
			$da['Name']=isset($da['Name'])?$da['Name']:"";
			$order=new Order;
			switch($da['Search']){
				case 1:
				  $list=$order->bcard($da);
			    break;
				case 2:
				  $list=$order->ccard($da);
				break;
			    case 3:
				  $list=$order->dcard($da);
			    break;
				case 4:
				  $list=(new Withdraw)->flist($da);
				break;
				default:
				  $list=$order->ecard($da);
			}
			return $list;
		}
        return view();
    }
	public function adminorder(){
		if(Request()->isAjax()){
			$da=input('post.');
			$list=array();
			$da['STime']=isset($da['STime'])?$da['STime']:"";
			$da['ETime']=isset($da['ETime'])?$da['ETime']:"";
			$da['Uid']=isset($da['Uid'])?$da['Uid']:"";
			$da['Name']=isset($da['Name'])?$da['Name']:"";
			$order=new Order;
			switch($da['Search']){
				case 1:
				  $list=$order->bcarda($da);
			    break;
				case 2:
				  $list=$order->ccarda($da);
				break;
			    case 3:
				  $list=$order->dcarda($da);
			    break;
				default:
				  $list=$order->ecarda($da);
			}
			return $list;
		}
        return view();
	}
	
	
}
