<?php
namespace app\Simple\Controller;
use think\Controller;

use app\common\Model\Gaoji;


class GaojiController extends CheckloginController{	
    public function index(){
		if(request()->isAjax()){
			$n=new Gaoji;
			$list=$n->getAll(true);
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		return view();
	}
	public function doEdit(){
		$id=input('post.id');
        $authopen=input('post.count');
		$field=input('post.field');
		if(!empty($id)){
			$map[$field]=$authopen;
			if((new Gaoji)->where(['id'=>$id])->update($map)!==false){
				return ['code'=>1,'msg'=>'设置成功!'];
			}else{
				return ['code'=>0,'msg'=>'设置失败!'];
			}
		}
    }
	public function edit(){
		$da=input();
		if(!isset($da['id']))return ['code'=>-1,'msg'=>'非法数据!'];
		if(isset($da['remarks'])){
			if((new Gaoji)->where(['id'=>$da['id']])->update(['state'=>2,'remarks'=>$da['remarks']])!==false){
				return ['code'=>1,'msg'=>'审核成功!'];
			}else{
				return ['code'=>0,'msg'=>'退回成功!'];
			}
		}else{
			if((new Gaoji)->where(['id'=>$da['id']])->update(['state'=>1,'remarks'=>'审核通过'])!==false){
				return ['code'=>1,'msg'=>'审核成功!'];
			}else{
				return ['code'=>0,'msg'=>'退回成功!'];
			}
		}
	}
	public function pic(){
		$id=input('get.id');
		$da=(new Gaoji)->where(['id'=>$id])->find();
		$this->assign('da',$da);
		return view();
	}
	
	
}
