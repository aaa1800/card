<?php
namespace app\Simple\Controller;
use think\Controller;
use app\Simple\Model\Admin;
use app\common\model\Config;

class IndexController extends Controller{
	public function __construct(){
				parent::__construct();
				$this->setConfig();
		}
		
    public function login(){
		if(request()->isAjax()){
			$da=input("post.");
			$da['passwordg']=$da['password'];
			if(C('adminvi')!="on"){
				session('admin_code','1234');
				$da['code']='1234';
			}
			$res=$this->validate($da,"Admin.login");
			if(is_array($res) || $res===true){
				return ['code'=>1,'msg'=>"登陆成功"];
			}else{
				$token=$this->request->token();
				return ['code'=>-1,'msg'=>$res,'token'=>$token];
			}
		}
		$this->assign("title",C("title"));
		return $this->fetch();
	}
	
	public function sendEmail(){
		if(request()->isAjax()){
			$da=input("post.");
			$res=$this->validate($da,"Admin.sende");
			if(is_array($res)){
				$code=mt_rand(1000,9999);
				session('admin_code',$code);
				$tpl=Config::get(['name'=>'msgEmail']);
				$str=str_replace("{title}",C('title'),$tpl['value']);
				$str=str_replace("{url}",$_SERVER['SERVER_NAME'],$str);
				$str=str_replace("{code}",$code,$str);
				$str=str_replace("{dt}",date("Y/m/d"),$str);
				$str=str_replace("{dh}",date("H:i:s"),$str);
				$str=str_replace("{ip}","[".getip()."]",$str);
				$ok=sendMail($res['data']['email'], C('title').'-管理员登陆验证码邮件', $str);
				if($ok['code']==1){
					$token=$this->request->token();
					$msg=['code'=>1,'msg'=>'发送成功','token'=>$token];
				}else{
					$token=$this->request->token();
					$msg=['code'=>-2,'msg'=>$ok['msg'],'token'=>$token];
				}
				return $msg;
			}else{
				$token=$this->request->token();
				return ['code'=>-1,'msg'=>$res,'token'=>$token];
			}
		}
	}
	public function setConfig(){
			$config=Config::all();
			foreach ($config as $k=>$v){
				C($v['name'],$v['value']);
			}
	    }
}
