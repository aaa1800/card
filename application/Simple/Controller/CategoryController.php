<?php
namespace app\Simple\Controller;
use think\Controller;
use app\common\model\Channel;
use app\common\model\Operator;
use app\common\model\Order;
use app\common\model\User;
use app\common\model\MoneyLog;
use app\common\model\Send;
use app\common\model\Qiye;
use app\common\model\Withdraw;
use app\common\model\Gaoji;
use think\Db;

class CategoryController extends CheckloginController{	
    public function mVerify(){
		if(request()->isAjax()){
			$n=new Channel;
			$list=$n->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		return view();
	}
	public function msgEdit(){
		if(request()->isAjax()){
			$da=input("post.");
			$AD=new Channel;
            if(empty($da['id'])){
				$ok=$AD->allowField(true)->save($da);
			}else{	
			    $ok=$AD->isUpdate(true)->allowField(true)->save($da);
			}
			if($ok){
				return json(['code' => 1, 'msg' => '修改成功!']);
			} else {
				return json(['code' => 0, 'msg' =>"修改失败"]);
			}	
		}
		$ef=(new Operator)->where(['start'=>1])->select();
		$id=input("get.id");
		$act=Channel::get($id);
		$this->assign("arre",$ef);
		$this->assign("u",$act);
		$this->assign("ok",$act?true:false);
		return view();
	}
	
	public function opencard(){
		$da=input();
		$li=(new Channel)->where(['id'=>$da['id']])->find();
		if($li){
			$ok=(new Channel)->where(['id'=>$da['id']])->setField('start',$da['start']);
			if($ok){
				return ['code'=>1,'msg'=>'设置成功'];
			}else{
				return ['code'=>-1,'msg'=>'系统错误'];
			}
		}else{
			return ['code'=>-1,'msg'=>'参数错误'];
		}
	}
	
	public function opaer(){
		if(request()->isAjax()){
			$list=(new Operator)->getAll();
			
			$data=input();
			$psize=$data['limit'];
			$page=$data['page'];
			$map['id']=['gt',0];
		    $list=db('category')->where($map)->order("id asc")->paginate(array('list_rows'=>$psize,'page'=>$page))->toArray();
			
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		return view();
	}
	
	public function opearEdit(){
		if(request()->isAjax()){
			$da=input();
			$AD=new Operator;
			unset($da['file']);
			if(empty($da['id'])){
				$da['addtime'] = date("Y-m-d H:i:s");
				$da['iconUrl'] = $da['iconUrl'];
				$ok=db('category')->insert($da);
			}else{	
			    $ok=db('category')->update($da);
			}
			if($ok){
				return json(['code' => 1, 'msg' => '修改成功!']);
			} else {
				return json(['code' => 0, 'msg' =>"修改失败"]);
			}
		}
			$id=input("id");
			$id=$id?$id:0;
			$da=db('category')->where(['id'=>$id])->find();
			$this->assign("da",$da?$da:[]);
			return view();

	}
	
	public function msDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		if(!isset($data['tab'])){
			return ['code'=>0,'msg'=>'未选择表!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
	
		db('category')->delete($map);
		
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	
	public function cardAdd(){
		if(request()->isAjax()){
			   $da=input('post.');
			   if(empty($da['name'])){
				   return json(['code'=>12,'msg'=>'通道名称必填']);
				   exit;
			   }
			   if(empty($da['type'])){
				   return json(['code'=>12,'msg'=>'通道代码必填']);
				   exit;
			   }
			   if(empty($da['payrate'])){
				   return json(['code'=>12,'msg'=>'通道费率必填']);
				   exit;
			   }
			   if(empty($da['sysrate'])){
				   return json(['code'=>12,'msg'=>'系统费率必填']);
				   exit;
			   }
			   if(empty($da['rule'])){
				   return json(['code'=>12,'msg'=>'点卡面值必填']);
				   exit;
			   }
			   $ch=new Channel;
			   $ok=$ch->where(['type'=>$da['type']])->find();
			   if($ok){
				   return json(['code'=>12,'msg'=>'通道代码已经存在，请修改']);
				   exit;
			   }
			   $ch->allowField(true)->save($da);
			   $id=$ch->id;
			   if($id){
				   $fel=generate_password(2).$id;
				   $ch->save(['fiel'=>$fel],['id'=>$id]);
				   Db::execute("ALTER TABLE `mq_member_rate` ADD  `{$fel}` varchar(50) NOT NULL DEFAULT  '".$da['payrate']."|1'");	
				   $msg=['code'=>1,'msg'=>'执行成功'];
			   }else{
				   $msg=['code'=>12,'msg'=>'系统错误'];
			   }
			   return json($msg);
		}
		$ef=(new Operator)->where(['start'=>1])->select();
		$this->assign("arre",$ef);
		return view();
	}
	
	public function dasucc(){
		if(request()->isAjax()){
			$da=input("post.");
			$or=new Order;
			$uid=(new Order)->where(['id'=>$da['id']])->value('qiang');
			if($uid==0){
				$msg=['code'=>-1,'msg'=>"请先抢单再操作"];
			}elseif($uid!=session('is_admin')){
				$msg=['code'=>-1,'msg'=>"该订单已被其他管理员抢单，请不要操作"];
			}else{
			    $or->okorder(['id'=>$da['id'],'money'=>$da['amt'],'type'=>$da['op'],"remarks"=>"成功"]);
				$msg=['code'=>1,'msg'=>"操作成功"];				 
			}
			return json($msg);
		}
	}
	
	public function daerr(){
		if(request()->isAjax()){
			$da=input("post.");
			$or=new Order;
			$li=$or->where(['id'=>$da['id']])->find();
			$msg=['code'=>-1,'msg'=>'系统错误'];
			if($li['qiang']==0){
				$msg=['code'=>-1,'msg'=>"请先抢单再操作"];
			}elseif($li['qiang']!=session('is_admin')){
				$msg=['code'=>-1,'msg'=>"该订单已被其他管理员抢单，请不要操作"];
			}elseif($li){
				if($li['state']==1){
					$qmoney=(new User)->where(['id'=>$li['uid']])->value("money");
					(new User)->where(['id'=>$li['uid']])->setDec("money",$li['amount']);
					(new MoneyLog)->upda($qmoney,$li['uid'],$li['amount'],4,$li['orderno']);
				}
				$arr['type']=$da['op'];
				$arr['state']=2;
				$arr['amount']=0;
				$arr['settle_amt']=0;
				$arr['profit']=0;
				$arr['remarks']=isset($da['str'])?$da['str']:'';
				$arr['update_time']=date("Y-m-d H:i:s");
				$arr['consuming']=strtotime($arr['update_time'])-strtotime($or['create_time']);
			$ok=$or->save($arr,['id'=>$da['id']]); 
			if($ok){
					 $msg=['code'=>1,'msg'=>"操作成功"];
				 }else{
					 $msg=['code'=>-1,'msg'=>"系统错误"];
				 }	
		  }
		  return json($msg);
		}
	}
	
	
	
	
		public function ordersucc(){
		$data=input('post.');
		if(!isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map= $data['ids'];
		foreach ($map as $k =>$v) {
			$or=new Order;
				$li=$or->where(['id'=>$v])->find();
				
			
			$uid=(new Order)->where(['id'=>$v])->value('qiang');
			if($uid==0){
				$msg=['code'=>-1,'msg'=>"请先抢单再操作"];
			}elseif($uid!=session('is_admin')){
				$msg=['code'=>-1,'msg'=>"该订单已被其他管理员抢单，请不要操作"];
			}else{
			    $or->okorder(['id'=>$v,'money'=>$li['money'],'type'=>$li['type'],"remarks"=>"成功"]);
				$msg=['code'=>1,'msg'=>"操作成功"];				 
			
		
		}
		}
			return json($msg);
    }
    
    
    	public function ordererr(){
		$data=input('post.');
		if(!isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map= $data['ids'];
	//	var_dump($map);
		foreach ($map as $k =>$v) {
		$or=new Order;
			$li=$or->where(['id'=>$v])->find();
			$msg=['code'=>-1,'msg'=>'系统错误'];
			if($li['qiang']==0){
				$msg=['code'=>-1,'msg'=>"请先抢单再操作"];
			}elseif($li['qiang']!=session('is_admin')){
				$msg=['code'=>-1,'msg'=>"该订单已被其他管理员抢单，请不要操作"];
			}elseif($li){
				if($li['state']==1){
					$qmoney=(new User)->where(['id'=>$li['uid']])->value("money");
					(new User)->where(['id'=>$li['uid']])->setDec("money",$li['amount']);
					(new MoneyLog)->upda($qmoney,$li['uid'],$li['amount'],4,$li['orderno']);
				}
				$arr['type']=$li['type'];
				$arr['state']=2;
				$arr['amount']=0;
				$arr['settle_amt']=0;
				$arr['profit']=0;
				$arr['remarks']="失败";
				$arr['update_time']=date("Y-m-d H:i:s");
				$arr['consuming']=strtotime($arr['update_time'])-strtotime($or['create_time']);
			$ok=$or->save($arr,['id'=>$v]); 
			if($ok){
					 $msg=['code'=>1,'msg'=>"操作成功"];
				 }else{
					 $msg=['code'=>-1,'msg'=>"系统错误"];
				 }	
		  }
		 
		}
		 return json($msg);
   
    	}
    	
    	
    	
	
	public function find(){
		if(request()->isAjax()){
			$da=input();
			$msg=$this->tosel($da);
			return json($msg);
		}
	}
	
	private function tosel($da){
		    $res=(new Operator)->where(['id'=>$da['type']])->value("class");
			$send=new Send($da['id']);
			$tostr="cx".$res;
			$res=$send->$tostr();
			$or=new Order;
			switch($res['code']){
				case 1:
					$li=$or->where(['id'=>$da['id'],'state'=>['neq',1]])->find();
					if($li){
						$or->okorder(['id'=>$da['id'],'money'=>$res['money'],'remarks'=>"成功"]);
						$or->save(['remarks'=>"成功"],['id'=>$da['id']]);
					}
					$msg=['result'=>1,'msg'=>"销卡成功"];
				break;
				case 0:
				case 2:
				  $or->save(["state"=>0,'remarks'=>$res['msg'],'update_time'=>date("Y-m-d H:i:s")],['id'=>$da['id']]);
				  $msg=['result'=>102,'msg'=>$res['msg']];
				break;
				default:
					$or->save(["state"=>2,'remarks'=>$res['msg'],'update_time'=>date("Y-m-d H:i:s")],['id'=>$da['id']]);
					$msg=['result'=>102,'msg'=>$res['msg']];
			}
			return $msg;
	}
	
	public function findlist(){
		if(request()->isAjax()){
			$da=input("");
			$num=0;
			$err=0;
			if(isset($da['ids'])){
				foreach($da['ids'] as $v){
					if($v){
						$type=(new Order)->where(['id'=>$v])->value('type');
						if($type){
							$da['id']=$v;
							$da['type']=$type;
							$ms=$this->tosel($da);
							if($ms['result']==1){
								$num++;
							}else{
								$err++;
							}
						}
					}
				}
				return json(['result'=>1,'snum'=>$num+$err,'ok'=>$num,'err'=>$err]);
			}else{
				return json(['result'=>-1,'err'=>"未选择数据"]);
			}
		}
	}
	
	
	
}
