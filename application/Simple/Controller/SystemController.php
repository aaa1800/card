<?php
namespace app\Simple\Controller;
use think\Controller;
use think\Request;
use app\common\model\News;
use app\common\model\Config;

class SystemController extends CheckloginController {

	public function index(){
		if(request()->isAjax()){
			$da=input("post.");
			unset($da['file']);
			$da['kfreg']=isset($da['kfreg'])?'on':'off';
			$da['webopen']=isset($da['webopen'])?'on':'off';
			$da['kali']=isset($da['kali'])?'on':'off';
			$da['kweixin']=isset($da['kweixin'])?'on':'off';
			$da['kbank']=isset($da['kbank'])?'on':'off';
			foreach($da as $k=>$v){
				if(Config::where(['name'=>$k])->find()){
				     Config::where(['name'=>$k])->update(['value' => $v]);
				}else{
					(new Config)->allowField(true)->save(['name'=>$k,'value'=>$v]);
				}
			}
			return json(['code' => 1, 'msg' => '更新成功!']);
		}
		return view();
	}
	public function weixin(){
		if(request()->isAjax()){
			$da=input("post.");
			unset($da['file']);
			$da['txopen']=isset($da['txopen'])?'on':'off';
			$da['wxyan']=isset($da['wxyan'])?'on':'off';
			foreach($da as $k=>$v){
				if(Config::where(['name'=>$k])->find()){
				  Config::where(['name'=>$k])->update(['value' => $v]);
				}else{
					(new Config)->allowField(true)->save(['name'=>$k,'value'=>$v]);
				}
			}
			return json(['code' => 1, 'msg' => '更新成功!']);
		}
		return view();
	}
	public function alipay(){
		if(request()->isAjax()){
			$da=input("post.");
			unset($da['file']);
			$da['alitxopen']=isset($da['alitxopen'])?'on':'off';
			foreach($da as $k=>$v){
				if(Config::where(['name'=>$k])->find()){
				     Config::where(['name'=>$k])->update(['value' => $v]);
				}else{
					(new Config)->allowField(true)->save(['name'=>$k,'value'=>$v]);
				}
			}
			return json(['code' => 1, 'msg' => '更新成功!']);
		}
		return view();
	}
	
	public function email(){
		if(request()->isAjax()){
			$da=Request::instance()->param(false);
			unset($da['file']);
			foreach($da as $k=>$v){
				if(Config::where(['name'=>$k])->find()){
				     Config::where(['name'=>$k])->update(['value' => $v]);
				}else{
					(new Config)->allowField(true)->save(['name'=>$k,'value'=>$v]);
				}
			}
			return json(['code' => 1, 'msg' => '更新成功!']);
		}
		return view();
	}
	public function banner(){
		if(request()->isAjax()){
			$da=Request::instance()->param(false);
			unset($da['file']);
			foreach($da as $k=>$v){
				if(Config::where(['name'=>$k])->find()){
				     Config::where(['name'=>$k])->update(['value' => $v]);
				}else{
					(new Config)->allowField(true)->save(['name'=>$k,'value'=>$v]);
				}
			}
			return json(['code' => 1, 'msg' => '更新成功!']);
		}
		return view();
	}
	public function Software(){
		if(request()->isAjax()){
			$da=input("post.");
			unset($da['file']);
			switch(isset($da['type'])?$da['type']:0){
				case "setting":
				 $msg=FtpSend(['type'=>'setting','phone'=>$da['sphone'],'password'=>$da['spassword'],'neixing'=>$da['sneixing']]);
				 if($msg['code']!=1){return $msg;exit;}
				break;
				case "setsn":
				 $msg=FtpSend(['type'=>'setsn','phone'=>$da['snphone'],'password'=>$da['snpassword'],'neixing'=>$da['snneixing']]);
				 if($msg['code']!=1){return $msg;exit;}
				break;
				case "open":
				  switch($da['name']){
					  case "sopen":
					     $msg=FtpSend(['type'=>'open','open'=>$da['sopen']]);
						 if($msg['code']!=1){return $msg;exit;}
						 $da['sopen']=$msg['open'];
					  break;
					  case "snopen":
					      $msg=FtpSend(['type'=>'opensn','open'=>$da['sopen']]);
						 if($msg['code']!=1){return $msg;exit;}
						 $da['snopen']=$msg['open'];
					  break;
				  }
				break;
				case "userkey":
				 $msg=FtpSend(['type'=>'userkey','key'=>$da['skey']]);
				 if($msg['code']!=1){return $msg;exit;}
				break;
				case "peizhi":
				$msg=FtpSend(['type'=>'peizhi','denglu'=>$da['sdenglu'],'domain'=>$da['sdomain'],'minnum'=>$da['sminnum'],'maxnum'=>$da['smaxnum']]);
				if($msg['code']!=1){return $msg;exit;}
				break;
				default:
				  $msg=FtpSend(['type'=>'tongbu','phone'=>$da['sphone'],'password'=>$da['spassword'],'key'=>$da['skey'],'denglu'=>$da['sdenglu'],'domain'=>$da['sdomain'],'minnum'=>$da['sminnum'],'maxnum'=>$da['smaxnum'],'snname'=>$da['snphone'],'snpass'=>$da['snpassword'],'sntype'=>$da['sneixing'],'smtype'=>$da['snneixing']]);
				  if($msg['code']!=1){return $msg;exit;}
			}
			if($msg['code']==1){
				foreach($da as $k=>$v){
					if((new Config)->where(['name'=>$k])->find()){
				      Config::where(['name'=>$k])->update(['value' => $v]);
					}else{
					  (new Config)->save(['name'=>$k,'value' => $v]);
					}
			    }
			}
			return $msg;
		}
		return view();
	}
	

	public function news(){
		if(request()->isAjax()){
			$n=new News;
			$list=$n->Getall();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		$this->assign("type",input('type'));
		$this->assign("arr",newstype(1,false));
		return view();
	}
	public function editNews(){
		if(request()->isAjax()){
				$da=Request::instance()->param(false);;
				if(!empty($da['id'])){
				  $ok=(new News)->isUpdate(true)->allowField(true)->save($da);
				}else{
					$da['addtime']=time();
					$AD=new News($da);
					$ok=$AD->allowField(true)->save();
				}
				if($ok){
					return json(['code' => 1, 'msg' => '添加成功!']);
				} else {
					return json(['code' => 0, 'msg' =>'保存失败！']);
				}	
		}
		$id=input("get.id");
		$act=News::get($id);
		$this->assign("u",$act);
		$this->assign("ok",$act?true:false);
		return view();
	}
	public function delNews(){
		if(request()->isAjax()){
			$id=input("post.id");
			News::destroy(['id'=>$id]);
            return ['code'=>1,'msg'=>'删除成功!'];	
		}
	}
	public function tdieupload(){
		$fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        $info = $file->validate(['size'=>100524278,'ext'=>'jpg,png,gif','hex'=>true])->move(ROOT_PATH . 'public/static' . DS . 'uploads');
        if($info){
            $result['code'] = 0;
            $result['info'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['data']['src']=  '/static/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] = -1;
            $result['info'] = $file->getError();
            $result['data']['src'] = '';
            return $result;
        }

	}
	public function upload(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
                 
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // file_put_contents(__DIR__.'/xx.txt',json_encode($file)."\n",8);
        $info = $file->validate(['size'=>524278,'ext'=>'jpg,png,gif','hex'=>true])->move(ROOT_PATH . 'public/static' . DS . 'uploads');
        if($info){
            $result['code'] = 1;
            $result['info'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/static/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['info'] = '图片上传失败!';
            $result['url'] = '';
            return $result;
        }
    }
    
    	public function uploads(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
         file_put_contents(__DIR__.'/xx.txt',json_encode($fileKey)."\n",8);
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        $info = $file->validate(['size'=>524278,'ext'=>'jpg,png,gif','hex'=>true])->move(ROOT_PATH . 'public/static' . DS . 'uploads');
        //  file_put_contents(__DIR__.'/xx.txt',json_encode($info)."\n",8);
        if($info){
            $result['code'] = 1;
            $result['info'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/static/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['info'] = '图片上传失败!';
            $result['url'] = '';
            return $result;
        }
    }
	public function uploadfile(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        $info = $file->validate(['size'=>524278,'hex'=>true])->move(ROOT_PATH . 'ecrt');
        if($info){
            $result['code'] = 1;
            $result['info'] = '文件上传成功!'.ROOT_PATH . 'ecrt';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/ecrt/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['info'] = '文件上传失败!';
            $result['url'] = '';
            return $result;
        }
    }
	
}

?>