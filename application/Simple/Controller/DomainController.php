<?php
namespace app\Simple\Controller;
use think\Controller;
use app\common\Model\Domain;
use app\common\Model\DomainOrder;

class DomainController extends CheckloginController{	
    public function doList(){
		if(request()->isAjax()){
			$n=new Domain;
			$list=$n->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'money'=>$list['money'],'ok'=>$list['succ'],'rel'=>1];
		}
		return view();
	}
	public function xfList(){
		if(request()->isAjax()){
			$n=new DomainOrder;
			$list=$n->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'money'=>$list['money'],'rel'=>1];
		}
		return view();
	}
	public function doSet(){
		$id=input('post.id');
        $authopen=input('post.state');
		if(!empty($id)){
			$map['state']=$authopen;
			if((new Domain)->where(['id'=>$id])->update($map)!==false){
				return ['code'=>1,'msg'=>'设置成功!'];
			}else{
				return ['code'=>0,'msg'=>'设置失败!'];
			}
		}
    }
	public function doEdit(){
		$id=input('post.id');
        $authopen=input('post.count');
		$field=input('post.field');
		if(!empty($id) && !empty($field)){
			if($field=="stoptime"){
				 $ret = isDateTime($authopen);
                 if(!$ret){
					 return ['code'=>0,'msg'=>'请输入有效时间!'];
					 exit;
				 }
			}
			$map[$field]=$authopen;
			if((new Domain)->where(['id'=>$id])->update($map)!==false){
				return ['code'=>1,'msg'=>'设置成功!'];
			}else{
				return ['code'=>0,'msg'=>'设置失败!'];
			}
		
		}
    }
	public function doDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		if(!isset($data['tab'])){
			return ['code'=>0,'msg'=>'未选择表!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
		switch($data['tab']){
			case "do":
             Domain::destroy($map);
			 break;
			case "dr":
			 Domainorder::destroy($map);
			 break;
		}
        return ['code'=>1,'msg'=>'删除成功!'];
    }
}
