<?php
namespace app\Simple\Controller;
use think\Controller;

class LoginController extends Controller{
	
	public function outLogin(){
		if(request()->isAjax()){
			Session('is_admin',null);
			return ['code'=>1,'msg'=>'退出成功'];
		}
	}
}
?>