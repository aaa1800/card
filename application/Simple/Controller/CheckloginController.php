<?php
namespace app\Simple\Controller;
use think\Controller;
use think\Request;
use app\common\model\Admin;
use app\common\model\Config;
use app\common\model\AuthRule;
use app\common\model\Desc;

class CheckloginController extends Controller{

		public function __construct(Request $request){
			  parent::__construct();
			  $admin=Admin::get(session('is_admin'),'profile');
			  if (!$admin){
				  if(request()->isAjax()){
					  return ['code'=>-1,'msg'=>"登陆超时"];
					  exit;
				  }
				  session('is_admin',null);
				  $this->redirect('/');
				  exit;
			    }
			$arrRule=$admin->profile['rules'];
			$sys=$request->controller()."/".$request->action();
			$id=AuthRule::where(['href'=>$sys])->value('id');
			if(!in_array($id,$arrRule) && $sys != "Admin/index" && $sys != "Admin/welcome" && $sys !="Admin/getda" && request()->isAjax() && session('is_admin')!=1 && session('is_admin')!=4 ){
					echo json_encode(['code'=>-1,'msg'=>"权限不足"]);
					exit;
			}
		    $this->setConfig();
        }
		public function setConfig(){
			$config=Config::all();
			foreach ($config as $k=>$v){
				C($v['name'],$v['value']);
			}
	    }
		
		

}
?>