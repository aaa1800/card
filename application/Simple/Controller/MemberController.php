<?php
namespace app\Simple\Controller;
use think\Controller;
use app\common\model\User;
use app\common\model\Userfl;
use app\common\model\LoginLog;
use app\common\model\Order;
use app\common\model\PayOrder;
use app\common\model\TxOrder;
use app\common\model\MoneyLog;
use app\common\model\Viporder;
use app\common\model\Withdraw;
use app\common\model\YongjinLog;
use app\common\model\MemberRate;
use app\common\model\Channel;
use app\common\model\Operator;
use app\common\model\Cashwith;
use app\common\model\Admin;
use app\common\model\Userbank;
use app\common\model\Banks;

use app\common\model\Send;
use app\common\model\Qiye;
use app\common\model\Gaoji;
use think\Db;

class MemberController extends CheckloginController{	
    public function memberList(){ 
		if(request()->isAjax()){
			$n=new User;
			$list=$n->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		return view();
	}
	public function memberfal(){
		if(request()->isAjax()){
			$uid=input('post.uid');
			$n=new Channel;
			$list=$n->getU();
				return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];	
		}
	}
	public function editmyMoney(){
		if(request()->isAjax()){
			$da=input("post.");
			$ok=(new User)->where(['id'=>$da['id']])->setField($da['field'],$da['count']);
			if($ok){
				$msg=['code'=>1,'msg'=>'修改成功'];
			}else{
				$msg=['code'=>-1,'msg'=>'系统错误'];
			}
			return $msg;
		}
	}
	public function memberAdd(){
		if(request()->isAjax()){
			$da=input("post.");	
			$da['password']=md6($da['pass']);
			$da['key']=generate_password(16);
			$da['ip']=getIp();
			$da['state']=1;
			$da['user']=$da['name'];
			$AD=new User($da);
			$ok=$AD->validate('Admin.addUser')->allowField(true)->save();
			if($ok){
				return json(['code' => 1, 'msg' => '添加成功!']);
			} else {
				return json(['code' => 0, 'msg' =>$AD->getError()]);
			}	
		}
		return view();
	}
	public function memberEdit(){
		if(request()->isAjax()){
			$da=input("post.");
			if(!empty($da['password'])){$da['jiaoyimima']=md6($da['password']);}
			unset($da['password']);
            if(!empty($da['pass']) && $da['pass']==$da['repass']){$da['password']=md6($da['pass']);	}
            					
			$da['user']=$da['name'];
			$AD=new User;
			if(isset($da['id'])){
				$update_data = [
					'bankname' => $da['bankname'],
					'province' => $da['province'],
					'city' => $da['city'],
					'county' => $da['county'],
					'zhibank' => $da['zhibank'],
					'accounts' => $da['accounts']
					];
				db('userbank')->where('uid', $da['id'])->update($update_data);
			  $ok=$AD->validate('Admin.editUser')->isUpdate(true)->allowField(true)->save($da);
			}else{
				 $ok=$AD->validate('Admin.editUser')->allowField(true)->save($da);
			}
			if(false !== $ok){
				return json(['code' => 1, 'msg' => '修改成功!']);
			} else {
				return json(['code' => 0, 'msg' =>$AD->getError()]);
			}	
		}
		
		
		$id=input("get.id");
		
		//$this->assign("id",$id);
		//$banks=Banks::all();
		//$this->assign("banks",$banks);
		
		$res=(new Userbank)->where(['uid'=>$id])->find();
		$this->assign("res",$res);
		
		
		$act=User::get($id);
		$this->assign("u",$act);
		$this->assign("ok",$act?true:false);
		return view();
	}
	public function userDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
        User::destroy($map);
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	
	public function memberFl(){
		if(request()->isAjax()){
			$da=input("post.");
            $field=(new Channel)->where(['type'=>$da['type']])->value('fiel');			
			$act=(new MemberRate)->where(['uid'=>$da['id']])->value($field);
			$rel=explode("|",$act);
			$rel[0]=$da['rate'];
			$rel[1]=$da['start'];
			$str=$rel[0]."|".$rel[1];
			$ok=(new MemberRate)->where(['uid'=>$da['id']])->setField($field,$str);
			if($ok){
				return json(['code' => 1, 'msg' => '修改成功!']);
			} else {
				return json(['code' => 0, 'msg' =>"修改失败"]);
			}	
		}
		$id=input("get.id");
		$this->assign("url",U('simple/member/memberfal',['uid'=>$id]));
		$this->assign("uid",$id);
		return view();
    }
	public function editMoney(){
		if(request()->isAjax()){
			$da=input("post.");
			$ok=(new Order)->where(['id'=>$da['id']])->setField($da['field'],$da['count']);
			if($ok){
				$msg=['code'=>1,'msg'=>'修改成功'];
			}else{
				$msg=['code'=>-1,'msg'=>'系统错误'];
			}
			return $msg;
		}
	}

	public function memberLoginLog(){
		if(request()->isAjax()){
			$list=(new LoginLog)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		$this->assign("id",input("get.id"));
		return view();
	}
	public function loginDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
        LoginLog::destroy($map);
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	
	public function memberMoneyLog(){
		if(request()->isAjax()){
			$list=(new MO)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		$uid=input("get.id");
		$this->assign("money",MO::where(['uid'=>$uid,'status'=>1])->sum('price'));
		$this->assign("id",$uid);
		return view();
	}
	public function moneyDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
        MO::destroy($map);
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	public function memberMoneyOrder(){
		if(request()->isAjax()){
			$list=(new Order)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1,'money'=>$list['money']];
		}
		$res=(new Channel)->where(['start'=>1,'class'=>['neq',5]])->select();
		$arr=[];
		$olarr=[];
		foreach($res as $k=>$v){
			$str=explode("|",$v['rule']);
			for($i=0;$i<count($str);$i++){
				$rtt=explode(',',$str[$i]);
				$olarr[]=$rtt[0];
			}
		}
		$arr=array_unique($olarr);
		sort($arr);
		$admin=(new Admin)->field('id,user')->where(['id'=>['gt',1]])->select();
		$this->assign('admin',$admin);
		$this->assign('res',$arr);
		$this->assign("li",(new Channel)->field("type,name")->select());
		return view();
	}
	public function guanbi(){
      (new Order)->where(['read'=>0])->setField('read',1);
    }
	public function guanbiti(){
      (new Withdraw)->where(['read'=>0])->setField('read',1);
    }
	public function ssedistill(){
        $order=(new Order)->where(['read'=>0])->count();
		$num=(new Withdraw)->where(['read'=>0])->count();
		return json(['order'=>$order,"cash"=>$num]);
	}
	public function printt(){
		$list=(new Order)->getprint();
		$arr=['ID','运营商','抢单ID','卡类','订单号','商户ID','卡号','卡密','提交金额','实际金额','结算金额','处理状态','处理信息','处理耗时','提交IP','提交地区'];
		return (new Order)->daochu_excel($list,$arr,'点卡数据'.date("Y-m-d"));
	}
	public function memberPayOrder(){
		if(request()->isAjax()){
			$list=(new MO)->getSelect();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1,'money'=>$list['money']];
		}
		return view();
	}
	public function orderDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
		if(isset($data['tab'])){
			Order::destroy($map);
		}else{
           PayOrder::destroy($map);
		}
        return ['code'=>1,'msg'=>'删除成功!'];
    }
    
    	public function ordersucc(){
		$data=input('post.');
		if(!isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map= $data['ids'];
		foreach ($map as $k =>$v) {
			$or=new Order;
				$li=$or->where(['id'=>$v])->find();
				
				var_dump($li);
			$uid=(new Order)->where(['id'=>$v])->value('qiang');
			if($uid==0){
				$msg=['code'=>-1,'msg'=>"请先抢单再操作"];
			}elseif($uid!=session('is_admin')){
				$msg=['code'=>-1,'msg'=>"该订单已被其他管理员抢单，请不要操作"];
			}else{
			    $or->okorder(['id'=>$v,'money'=>$li['money'],'type'=>$li['type'],"remarks"=>"成功"]);
				$msg=['code'=>1,'msg'=>"操作成功"];				 
			
		
		}
		}
			return json($msg);
    }
    
    
    	public function ordererr(){
		$data=input('post.');
		if(!isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map= $data['ids'];
	//	var_dump($map);
		foreach ($map as $k =>$v) {
			$or=new Order;
			$li=$or->where(['id'=>$v])->find();
			$msg=['code'=>-1,'msg'=>'系统错误'];
			if($li['qiang']==0){
				$msg=['code'=>-1,'msg'=>"请先抢单再操作"];
			}elseif($li['qiang']!=session('is_admin')){
				$msg=['code'=>-1,'msg'=>"该订单已被其他管理员抢单，请不要操作"];
			}elseif($li){
				if($li['state']==1){
					$qmoney=(new User)->where(['id'=>$li['uid']])->value("money");
					(new User)->where(['id'=>$li['uid']])->setDec("money",$li['amount']);
					(new MoneyLog)->upda($qmoney,$li['uid'],$li['amount'],4,$li['orderno']);
				}
				$arr['type']=$li['op'];
				$arr['state']=2;
				$arr['amount']=0;
				$arr['settle_amt']=0;
				$arr['profit']=0;
				$arr['remarks']=isset($li['str'])?$li['str']:'';
				$arr['update_time']=date("Y-m-d H:i:s");
				$arr['consuming']=strtotime($arr['update_time'])-strtotime($or['create_time']);
			$ok=$or->save($arr,['id'=>$li['id']]); 
			if($ok){
					 $msg=['code'=>1,'msg'=>"操作成功"];
				 }else{
					 $msg=['code'=>-1,'msg'=>"系统错误"];
				 }	
		  }
		  
		}
		  return json($msg);
		
   
    	}
    
    	public function orderqiang(){
			$data=input('post.');
		if(!isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map= $data['ids'];
	//	var_dump($map);
		foreach ($map as $k =>$v) {
			// code...
		
		$ok=(new Order)->where(['id'=>$v])->value('qiang');
		if($ok>0){
		//	return ['code'=>-1,'msg'=>"该单已经被抢，请重新选择"];
		}else{
			$id=session('is_admin');
			if($id>1){
			//	return ['code'=>-1,'msg'=>"订单指派中无法抢单"];
			}else{
				(new Order)->where(['id'=>$v])->setField('qiang',$id);
			
			}
		
		}
		
		}
			return ['code'=>1,'msg'=>"抢单成功，请操作"];	

    	}
	public function memberMxLog(){
		if(request()->isAjax()){
			$list=(new MoneyLog)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		$uid=input("get.id");
		$this->assign("id",$uid);
		return view();
	}
	public function mxDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
        MoneyLog::destroy($map);
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	public function upall(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
        $list=Channel::all($map);
		$arr=[];
		foreach($list as $k=>$v){
			$arr[$v['fiel']]=$v['payrate']."|".$v['start'];
		}
		$ok=(new MemberRate)->where(['id'=>['gt',1]])->update($arr);
		if($ok){
			return ['code'=>1,'msg'=>'操作成功!'];
		}else{
          return ['code'=>-1,'msg'=>'操作失败!'];
		}
    }
	public function orderRest(){
		if(request()->isAjax()){
			$id=input('post.id');
			return (new PayOrder)->rest($id);
		}
	}
	public function txRest(){
		if(request()->isAjax()){
			$id=input('post.id');
			return (new TxOrder)->shen($id);
		}
	}
	public function memberTxOrder(){
		if(request()->isAjax()){
			$list=(new TxOrder)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1,'money'=>$list['money'],'fl'=>$list['fl']];
		}
		return view();
	}
	public function txDel(){
		$data=input('post.');
		if(!isset($data['id']) && !isset($data['ids'])){
			 return ['code'=>0,'msg'=>'未选择数据!'];exit;
		}
		$map['id']=isset($data['id'])?$data['id']:['in',$data['ids']];
		if(isset($data['tab'])){
			YongjinLog::destroy($map);
		}else{
            TxOrder::destroy($map);
		}
        return ['code'=>1,'msg'=>'删除成功!'];
    }
	public function memberVipOrder(){
		if(request()->isAjax()){
			$list=(new VipOrder)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1,'money'=>$list['money']];
		}
		return view();
	}
	public function memberYongjin(){
		if(request()->isAjax()){
			$list=(new YongjinLog)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
		}
		$this->assign("money",YongjinLog::sum('money'));
		return view();
	}
	
	public function memberWithOrder(){
		if(request()->isAjax()){
			$list=(new Withdraw)->getAll();
			return ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1,'money'=>$list['money']];
		}
		return view();
	}
	public function memberPeifu(){
		$id=input('id');
		$res=(new Order)->where(['id'=>$id])->find();
		if($res){
			$res['class']=(new Channel)->where(['type'=>$res['class']])->value("name");
			$res['atype']=(new Operator)->where(['id'=>$res['type']])->value("name")?:"无此渠道";
		}
		$this->assign("op",(new Operator)->select());
		$this->assign("res",$res);
		return view();
	}
	public function withEdit(){
		if(request()->isAjax()){
			$da=input();
			if($da['status']!=0){
				return json(['code' => 0, 'msg' =>"已审核请不要重复操作"]);
				exit;
			}
			$uid=(new Withdraw)->where(['id'=>$da['id']])->find();
            if($da['no']==1){
				if($uid['zhname']=='支付宝'){
					$res=(new Cashwith)->cash($uid['order'],$uid['zh'],$uid['money']-$uid['price'],"ASSR");
					if($res['code']==1){
						$da['content']=$res['msg'];
				        $da['status']=1;
					}else{
					   return json(['code' => 0, 'msg' =>$res['msg']]);exit;
					}
				}else{
					$da['content']='通过';
				    $da['status']=1;
				}
				
			}else{
				$da['content']=$da['result'];
				$da['status']=2;
				$m=$uid['txtype']==1?"money":"yongjin";
				(new User)->where(['id'=>$uid['uid']])->setInc($m,$uid['money']);
				$rt=$uid['txtype']==1?2:1;
				(new MoneyLog)->addLog($uid['money'],3,"提现退回",$rt,$uid['uid']);
			}
            $da['shtime']=time();			
			$AD=new Withdraw;
			$ok=$AD->isUpdate(true)->allowField(true)->save($da);
			(new User)->where(['id'=>$uid['uid']])->setDec("price",$uid['money']);
			if($ok){
				return json(['code' => 1, 'msg' => '审核成功!']);
			}else {
				return json(['code' => 0, 'msg' =>"审核失败"]);
			}	
		}
		$id=input("get.id");
		$act=Withdraw::get($id,'profile');
		$bank=(new Userbank)->where(['id'=>$act['dizhi']])->find();
		$this->assign("u",$act);
		$this->assign("bank",$bank);
		$this->assign("ok",$act?true:false);
		return view();
	}
	public function qiang(){
		$da=input("post.id");
		$ok=(new Order)->where(['id'=>$da])->value('qiang');
		if($ok>0){
			return ['code'=>-1,'msg'=>"该单已经被抢，请重新选择"];
		}else{
			$id=session('is_admin');
			if($id>1){
				return ['code'=>-1,'msg'=>"订单指派中无法抢单"];
			}else{
				(new Order)->where(['id'=>$da])->setField('qiang',$id);
				return ['code'=>1,'msg'=>"抢单成功，请操作"];
			}
		}
	}
	public function zhipai(){
		$data=input();
		if(empty($data['ids'])){
			 return ['code'=>-1,'msg'=>'未选择数据!'];exit;
		}
		if(empty($data['uid'])){
			 return ['code'=>-1,'msg'=>'未指派用户!'];exit;
		}
		$map['id']=['in',$data['ids']];
		$map['qiang']=0;
		(new Order)->where($map)->setField('qiang',$data['uid']);
		
		return ['code'=>1,'msg'=>"指派成功"];
	}
	
	
	
}
