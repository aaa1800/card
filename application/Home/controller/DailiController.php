<?php
namespace app\Home\controller;
use think\Controller;
use think\Request;
use app\common\model\YongjinLog;
use app\common\model\User;
use app\common\model\Withdraw;
use app\common\model\MoneyLog;
use app\common\model\Order;

class DailiController extends ConfigController {
    public function daili(){
		if(request()->isAjax()){
		    $list=(new YongjinLog)->Getlist();
          return ['data'=>$list['data'],'draw'=>(int)input('get.draw'),'recordsFiltered'=>$list['total'],'recordsTotal'=>$list['current_page']];		
		}
		$url=$_SERVER['SERVER_NAME'];
		$url="http://".$url.U('home/login/registered',['qr'=>$this->user['id']]);
		$cnum=(new User)->where(['assets'=>$this->user['id']])->count();
		$fop=(new User)->where(['assets'=>$this->user['id']])->select();
		$r=0;
		foreach($fop as $v){
			if((new Order)->where(['uid'=>$v['id']])->sum('money')>300){
				$r++;
			}
		}
		$this->assign("num",$cnum);
		$this->assign("okn",$r);
		$this->assign("url",$url);
		if(isMobile()){
				return view("daili/mobile/daili");
			}else{
			   return view();
			}
    }
	public function subordinate(){
		if(request()->isAjax()){
		  $list=(new User)->xiaji();
           return ['data'=>$list['data'],'draw'=>(int)input('get.draw'),'recordsFiltered'=>$list['total'],'recordsTotal'=>$list['current_page']];
		}
		return $this->fetch();
    }
	public function tixian(){
		if(request()->isAjax()){
			$da=input("post.");
			if(!isset($da['type']))return ['code'=>0,'msg'=>'非法操作，冻结账号！'];
			if($da['type']<0 || $da['type']>1)return ['code'=>0,'msg'=>'你想干啥！！'];
			if(empty($da['zh']))return ['code'=>0,'msg'=>'账号不能为空'];
			if(empty($da['zhname']))return ['code'=>0,'msg'=>'真实姓名不能为空'];
			if(empty($da['money']) || $da['money']<1)return ['code'=>0,'msg'=>'金额不能为空或小于1'];
			$u=User::get($this->user['id']);
			if($u->yongjin < $da['yongjin']){
				return ['code'=>0,'msg'=>'余额不足'];
				exit;
			}
			$u->yongjin=$this->user['yongjin']-$da['money'];
			$u->price=$this->user['price']+$da['money'];
			$ok=$u->save();
			if(!$ok)return ['code'=>0,'msg'=>'操作失败，余额不足'];
			(new MoneyLog)->addLog($da['money'],2,"佣金提现",1);
			$da['order']=pay_order("W");
			$da['uid']=$this->user['id'];
			$da['price']=0;
			$da['txtype']=2;
			$da['umoney']=$u->yongjin;
			(new Withdraw)->save($da);
			return ['code'=>1,'msg'=>'提现成功，等待审核'];
		}
	}
	
	public function qrcode(){
		Vendor('phpqrcode.phpqrcode'); 
		$QRcode = new \QRcode();
		$url="http://";
		if(isset($_SERVER['HTTPS']))$url="https://";
		$value = $url."{$_SERVER['HTTP_HOST']}/registered.html?qr={$this->user['id']}"; //二维码内容 
		ob_start();
		$QRcode->png($value,false,'H',8,2);
		$imageString = base64_encode(ob_get_contents());
		ob_end_clean();
		return "data:image/png;base64,".$imageString;
	}
	
}