<?php
namespace app\Home\controller;
use think\Controller;
use app\common\Model\News;
use app\common\Model\Channel;
use app\common\Model\Order;
use think\Request;
use think\Db;
class IndexController extends WebController {
	public function __construct(Request $request){
        parent::__construct();

		$this->assign("cont",$request->controller());
        $this->assign("acti",$request->action());
        
        $cate_list = db('category')->order('id asc')->select();
		$this->assign('cate_list',$cate_list);
        
    }
    public function index(){
    	
		$data=(new News)->where(['type'=>1])->order('id desc')->limit(5)->select();
		$this->assign("new",$data);
		$sql = 'SELECT class FROM mq_order GROUP BY class ORDER BY COUNT(class) DESC LIMIT 3';
        $res =Db::query($sql);
		$arr=[];
		foreach($res as $v){
			$arr[]=$v['class'];
		}
		$newy=Channel::all(['qian'=>1]);
		$list=Channel::all();
		$this->assign('newy',$newy);
		$this->assign('list',$list);
		
		//$cate_list = db('category')->order('id asc')->select();
		//$this->assign('cate_list',$cate_list);
		
		if(isMobile()){
			return view("index/mobile/index");
		}else{
           return view();
		}
    }
	public function recycle(){
		$type=input("type");
		$type=$type?$type:0;
		$this->assign("type",$type);
		$list=Channel::all();
		$this->assign('list',$list);
		//$cate_list = db('category')->order('id asc')->select();
		//$this->assign('cate_list',$cate_list);
		if(isMobile()){
			return view("index/mobile/recycle");
		}else{
		   return $this->fetch();
		}
	}
	public function sellFlow(){
		return view('index/mobile/sell_flow');
	}
	public function myMessage(){
		return view('index/mobile/my_message');
	}
	public function weixin(){
		return view();
	}
	public function retrieve(){
		
		if(isMobile()){
			return view("index/mobile/retrieve");
		}else{
		   return $this->fetch();
		}
	}
	public function inviteFriends(){
		if(isMobile()){
			return view("index/mobile/invite_friends");
		}else{
		   return $this->fetch();
		}
	}
	
	public function help(){
		$type=input("type");
		$type=!$type?2:$type;
		$list=(new News)->where(['type'=>$type])->find();
		$this->assign("list",$list);
		$this->assign("type",$type);
		if(isMobile()){
			return view("index/mobile/help");
		}else{
		   return $this->fetch();
		}
	}
	
	public function autoSend(){
		$list=Channel::all();
		$this->assign('list',$list);
		$this->assign("type",0);
		return view();
	}
	public function faq(){
		$this->assign("type",0);
		return view();
	}
	public function newsDetail(){
		$id=input('id');
		if(empty($id))$this->redirect(U('home/index/miss'));
		$data=(new News)->where(['id'=>$id])->find();
		if(!$data)$this->redirect(U('home/index/miss'));
		$this->assign('h',$data);
		$this->assign('id',$id);
		if(request()->isAjax()){
			return json($data);
		}else{
			if(isMobile()){
				return view("index/mobile/news_detail");
			}else{
			   return $this->fetch();
			}
		}
	}
	public function cardArticle(){
		$data=input();
		$type=isset($data['type'])?$data['type']:1;
		$map['type']=$type;
		$class=isset($data['class'])?$data['class']:0;
		if($class>0)$map['class']=$class;
		$list=(new News)->where($map)->paginate(10,false,['query' => request()->param()]);
		$this->assign("type",$type);
		$this->assign("class",$class);
		$this->assign("li",$list);
		if(isMobile()){
				return view("index/mobile/card_article");
			}else{
			   return view();
			}
		
	}
	public function gongg(){
		$list=(new News)->where(['type'=>8,'status'=>1])->paginate(10,false,['query' => request()->param()]);
		$this->assign('li',$list);
		$this->assign("type",0);
		return view();
	}
	
	public function miss(){
		if(request()->isAjax()){
			return ['code'=>-1,'msg'=>'迷路了！'];
		}else{
			return view();
		}
	}
	public function getRecycleOperatorsByProductClassifyId(){
		$id=input("post.productClassifyId");
		$sl=(new Channel)->where(['class'=>$id])->select();
		$arr=[];
		foreach($sl as $k=>$v){
			$arr[$k]['enable']= $v['start']?true:false;
            $arr[$k]['iconUrl']= $v['iconUrl'];
            $arr[$k]['id']= $v['id'];
            $arr[$k]['auto']= $v['auto'];
			$arr[$k]['maintenanceState']= $v['start']?0:1;
			$arr[$k]['name']= $v['name'];
			$arr[$k]['phoneRecycleIcon']= $v['phoneRecycleIcon'];
			$arr[$k]['productCode']= $v['type'];
		}
		return json($arr);
	}
	public function getMaxBatchSubmitCount(){
		return json(['code'=>"000000",'object'=> 2000]);
	}
	public function newsw(){
		$id=input("id");
		$res=(new News)->where(['id'=>$id])->find();
		$this->assign("res",$res);
		return view("index/mobile/news");
	}
	public function isParticipateNewMemberActivity(){
		if(!session("?token")){
			$arr=['code'=>"000004",'message'=> "未登录，显示登录获取新人活动资格"];
		}else{
			if((new Order)->where(['uid'=>session('user_id')])->find()){
			  $arr=['code'=>"000005",'message'=>'可以参与活动'];
			}else{
				$arr=['code'=>'000003','message'=>'参加过了'];
			}
		}
		return json($arr);
	}
	public function enterprise(){
		return view();
	}
	//获取最近消卡 消耗时长
	public function getRecentConsumeDuration(){
		$arr=['consumptionTime'=> 4682,'consumptionTimeNumber'=> 1000,'discount'=> 0,'durationMilliSeconds'=> 4993000,'operatorName'=> "沃尔玛",'price'=> 100];
		return json($arr);
	}
	public function getCardRules(){
		$id=input('post.operatorId');
		$da=(new Channel)->where(['id'=>$id])->find();
		return json(['code'=> 1,'msg'=> $da['amt']]);
	}
	public function mgetCardRules(){
		$id=input('post.operatorId');
		$da=(new Channel)->where(['id'=>$id])->find();
		return json($da['amt']);
	}
	public function getRateRangeInfo(){
		return json(null);
	}
	public function getRecyclePricesByOperatorId(){
		    $da=input("post.operatorId");
			$type=input("post.stype");
			if($type!=3){
				$res=(new Channel)->where(['id'=>$da])->value("rule");
				if(!$res){return json([]);}
				$res=explode("|",$res);
				$arr=[];
//				$raul=userrate($da);
				$raul=(new Channel)->where(['id'=>$da])->value("sysrate");
				foreach($res as $k=>$v){
					$rt=explode(",",$v);
					$rt1=isset($rt[1])?$rt[1]:1;
					$fl=sprintf("%.3f",$raul/100);
					$arr[$k]['maintenanceState']=0;
					$arr[$k]['price']=$rt[0];
                  if(isset($rt[1])){
                    $arr[$k]['recyclePrice']=sprintf("%.3f",$fl*$rt1);
                  }else{
					$arr[$k]['recyclePrice']=sprintf("%.3f",$rt[0]*$fl);
                  }
					$arr[$k]['type']=(new Channel)->where(['id'=>$da])->value("class");
				}
			}else{
				$res=(new Channel)->where(['start'=>1,'class'=>['neq',5]])->select();
				$arr=[];
				$olarr=[];
				foreach($res as $k=>$v){
					$str=explode("|",$v['rule']);
					for($i=0;$i<count($str);$i++){
						$rr=explode(",",$str[$i]);
						$olarr[]=$rr[0];
					}
				}
				$newarr=array_unique($olarr);
				sort($newarr);
				for($n=0;$n<count($newarr);$n++){
					$arr[$n]['maintenanceState']=0;
					$arr[$n]['price']=$newarr[$n];
					$arr[$n]['recyclePrice']=0;
					$arr[$n]['type']=2;
				}
			}
			return json($arr);

	}
	//首页滚动图
	public function getIndexSlides(){
		$arr=[['link'=>"/retrieveActivity/retrieve.html", 'name'=>"新人活动",'photoUrl'=>"http://faceimg.renrencou.com/temp_upload/2018/11/23/20181123113800567.png"]];
		return json($arr);
	}
	
	public function getProductClassifiesList(){
		
		$cate_list = db('category')->order('id asc')->select();
		$arr = array();
		foreach ($cate_list as $v){
			$arr[] = ["iconUrl"=>$v['iconUrl'],"id"=>$v['id'],'class'=>$v['id'],"name"=>$v['name']];
		}
		/*
		$arr=[
			["iconUrl"=>"/static/home/images/index/e6c8f0ecb5707b01c40d87ce7044056a.png","id"=>"1",'class'=>0,"name"=>"影视会员"],
			["iconUrl"=>"/static/home/images/index/94780646c86955872655c39ed026fccf.png","id"=>"2",'class'=>1,"name"=>"音乐读书"],
			["iconUrl"=>"/static/home/images/index/e27aed856dc262b8ff3bc01875649d90.png","id"=>"3",'class'=>3,"name"=>"美食出行"],
			["iconUrl"=>"/static/home/images/index/1a1f2fd3e24b47a101e80dc8abe77f8c.png","id"=>"4",'class'=>4,"name"=>"联通卡密"],
			["iconUrl"=>"/static/home/images/index/16e399523daa607a765aece163bbb942.png","id"=>"5",'class'=>5,"name"=>"其他卡类"]
			];
		*/
		return json($arr);
	}
	
	public function getOperatorsList(){
		$list=(new Channel)->where(['start'=>1])->select();
		$arr=[];
		foreach($list as $k=>$v){
			$arr[$k]['iconUrl']=$v['phoneRecycleIcon'];
			$arr[$k]['name']=$v['name'];
			$arr[$k]['id']=$v['id'];
			$arr[$k]['pcId']=$v['class'];
			$arr[$k]['type']=$v['type'];
		}
		return json($arr);
	}
	
	public function getBaseInfo(){
		$arr=["copyright"=>C('coltd'),"csPhone"=>C('phone'),"csQQ"=>C('kefu'),"csTime"=>"周一至周日 8:00-24:00","descr"=>C('description'),"domain"=>$_SERVER['HTTP_HOST'],"email"=>"99905590@qq.com","icpNumber"=>C('beian'),"keywords"=>C('keywords'),"logo"=>C('logo'),"name"=>C('title'),"staticDomain"=>''];
		return json($arr);
	}
	public function productClassify(){
		$arr=[];
		for($i=0;$i<count(typed(1,false));$i++){
			if($i!=2){
				$arr[$i]['iconUrl']='';
				$arr[$i]['id']=$i+1;
				$arr[$i]['atype']=$i;
				$arr[$i]['name']=typed($i);
				$arr[$i]['operators']=(new Channel)->where(['class'=>$i])->select();
			}
		}
		sort($arr);
		return json($arr);
	}
	
	
}