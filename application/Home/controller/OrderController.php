<?php
namespace app\Home\controller;
use think\Controller;
use think\Request;
use app\common\model\Order;
use app\common\model\Userfl;
use app\common\model\Userpay;
use app\common\model\Channel;

class OrderController extends ConfigController {
	
    public function batchCardIndex(){
		if(request()->isAjax()){
			$da=input();
			$uid=$this->user['id'];
			$map['uid']=$uid;
			$map['single']=1;
			if(empty($da['keywords'])){
				
				if(!empty($da['collectionStatus'])){
					if($da['collectionStatus']==1){
					  $map['state']=0;
					}
				}
				if($da['productClassifyId']!= -1){
					$map['kanei']=$da['productClassifyId'];
				}
				if(!empty($da['operatorId'])){
					$map['class']=$da['operatorId'];
				}
				if(!empty($da['startDate'])){
					$map['create_time']=['between',[$da['startDate'],$da['endDate']]];
				}
			}else{
				$map['orderno|card_no|card_key|fenlei']=$da['keywords'];
			} 
			
			$ok=(new Order)->field("count(*) as cid,class,sum(money) as sm,sum(amount) as money,create_time,fenlei,type,sum(settle_amt) as price,state")->where($map)->group("fenlei")->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
			foreach($ok['data'] as $k=>$v){
				$ok['data'][$k]['oknum']=(new Order)->where(['fenlei'=>$v['fenlei'],'state'=>1])->count();
				$ok['data'][$k]['nonum']=(new Order)->where(['fenlei'=>$v['fenlei'],'state'=>2])->count();
				$ok['data'][$k]['type']=2;
				$ok['data'][$k]['operatorName']=(new Channel)->where(['type'=>$v['class']])->value('name');
			}
			$ok['totalCountTotal']=(new Order)->where($map)->count();
			$ok['actualAmountTotal']=(new Order)->where($map)->sum('amount');
			$map['state']=1;
			$ok['successCountTotal']=(new Order)->where($map)->count();
			$map['state']=2;
			$ok['failedCountTotal']=(new Order)->where($map)->count();
			return $ok;
		}else{
			  if(isMobile()){
				return view("order/mobile/batch_card_index");
			}else{
			   return view();
			}
		}
    }
	public function mbatchCardIndex(){
			$da=input();
			$uid=$this->user['id'];
			$map['uid']=$uid;
			$map['single']=1;
			if($da['collectionStatus']>=0){
				switch($da['collectionStatus']){
					case 1:
					  $map['state']=0;
					 break;
					case 2:
					  $map['state']=['gt',0];
					break;
				}
                			
			}
			$ok=(new Order)->field("count(*) as cid,class,sum(money) as sm,sum(amount) as money,create_time,fenlei,type,sum(settle_amt) as price,state")->where($map)->group("fenlei")->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
			foreach($ok['data'] as $k=>$v){
				$ok['data'][$k]['oknum']=(new Order)->where(['fenlei'=>$v['fenlei'],'state'=>1])->count();
				$ok['data'][$k]['nonum']=(new Order)->where(['fenlei'=>$v['fenlei'],'state'=>2])->count();
				$ok['data'][$k]['totalCount']=(new Order)->where(['fenlei'=>$v['fenlei']])->count();
				$ok['data'][$k]['type']=2;
				$ok['data'][$k]['operateStatus']=$v['state'];
				$ok['data'][$k]['operatorName']=(new Channel)->where(['type'=>$v['class']])->value('name');
			}
			return json($ok);
    }
	
	public function ClassifyId(){
		$type=input("productClassifyId");
		if($type == -1){
			$map['id']=['gt',0];
		}else{
			$map['class']=$type;
		}
		$list=(new Channel)->where($map)->select();
		return json(['code'=>1,'data'=>$list]);
	}
	public function singleCardIndex(){
		if(request()->isAjax()){
			$da=input();
			$uid=$this->user['id'];
			$map['uid']=$uid;
			$map['single']=0;
			if(empty($da['keywords'])){
				if(!empty($da['collectionStatus'])){
					if($da['collectionStatus']==1){
					  $map['state']=0;
					}
				}
				if($da['productClassifyId']!= -1){
					$map['kanei']=$da['productClassifyId'];
				}
				if(!empty($da['operatorId'])){
					$map['class']=$da['operatorId'];
				}
				if(!empty($da['startDate'])){
					$map['create_time']=['between',[$da['startDate'],$da['endDate']]];
				}
			}else{
				$map['orderno|card_no|card_key|fenlei']=$da['keywords'];
			} 
			$ok=(new Order)->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
			foreach($ok['data'] as $k=>$v){
				$ok['data'][$k]['totalCount']=1;
				if($v['state']==1){
				 $ok['data'][$k]['successCount']=1;
				}
				if($v['state']==2){
					$ok['data'][$k]['failedCount']=1;
				}
				$ok['data'][$k]['type']=1;
				$ok['data'][$k]['operatorName']=(new Channel)->where(['type'=>$v['class']])->value('name');
			}
			$ok['totalCountTotal']=(new Order)->where($map)->count();
			$ok['actualAmountTotal']=(new Order)->where($map)->sum('amount');
			$map['state']=1;
			$ok['successCountTotal']=(new Order)->where($map)->count();
			$map['state']=2;
			$ok['failedCountTotal']=(new Order)->where($map)->count();
			return $ok;
		}else{
		  if(isMobile()){
				return view("order/mobile/single_card_index");
			}else{
			   return view();
			}
		}
	}
	public function getSellCardOrdersDetailsa(){
		 $da=input();
		if(request()->isAjax()){
			$arr=[];
			$rt=[];
			if($da['type']==1){
				$ls=(new Order)->where(['fenlei'=>$da['id']])->order('id desc')->find();
				$rt=[];
				$rt['arrivalStatus']= "2";
				$rt['id']= $ls['id'];
				$rt['operateStatus']=$ls['state'];
				$rt['orderNumber']=$ls['fenlei'];
				$rt['orderTime']= $ls['create_time'];
				$rt['operatorName']=(new Channel)->where(['type'=>$ls['class']])->value('name');
				$rt['arrivalMoney']=0;
				$rt['failedCount']=0;
				$rt['successCount']=0;
				$rt['totalCount']=0;
				$rt['commitFaceVal']=0;
				$list=(new Order)->where(['fenlei'=>$da['id']])->select();
				foreach($list as $k=>$v){	
					if($v['state']==2){
					   $rt['failedCount']++;
					}
					if($v['state']==1){
						$rt['successCount']++;
						$rt['arrivalMoney']+=$v['amount'];
					}
					$rt['totalCount']++;
					$rt['commitFaceVal']+=$v['money'];
				}
				$arr['batchOrdersDetails']=$rt;
				$arr['ordersList']=$list;
			}else{
				$list=(new Order)->where(['id'=>$da['id']])->find();
				$arr['batchOrdersDetails']=['arrivalMoney'=>$list['amount'],
				                            'arrivalStatus'=> "2",
											'failedCount'=> $list['state']==1?0:1,
											'id'=> $list['id'],
											'operateStatus'=>$list['state'],
											'orderNumber'=>$list['orderno'],
											'orderTime'=> $list['create_time'],
											'successCount'=> $list['state']==1?1:0,
											'totalCount'=> 1,
											'operatorName'=>(new Channel)->where(['type'=>$list['class']])->value('name'),'commitFaceVal'=>$list['money']];
				$arr['ordersList']=[$list];
			}
			$arr['canBeChangeSubmitType']=true;
			$arr['canBeReSubmited']=true;
			$arr['orderType']=$da['type'];
			
			return json($arr);
		}else{
			$this->assign("da",$da);
		    return view('order/mobile/getSellCardOrdersDetails');
		}
	}
	public function msingleCardIndex(){
			$da=input();
			$uid=$this->user['id'];
			$map['uid']=$uid;
			$map['single']=0;
			if($da['collectionStatus']>=0){
                $map['state']=	$da['collectionStatus'];			
			} 
			$ok=(new Order)->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
			foreach($ok['data'] as $k=>$v){
				$ok['data'][$k]['operatorName']=(new Channel)->where(['type'=>$v['class']])->value('name');
			}
			return json($ok);
    }
	public function getSellCardOrdersDetails(){
		$id=input("id");
		$type=input("type");
		if($type){
			$map['fenlei']=$id;
		}else{
			$map['id']=$id;
		}
		$res=(new Order)->where($map)->select();
		$data['count']=0;
		$data['ok']=0;
		$data['no']=0;
		$data['money']=0;
		foreach($res as $k=>$v){
			$data['count']++;
			if($v['state']==1)$data['ok']++;
			if($v['state']==2)$data['no']++;
			$data['money']+=$v['amount'];
			$res[$k]['operatorName']=(new Channel)->where(['type'=>$v['class']])->value('name');
			$res[$k]['sta']=stateS($v['state']);
		}
		$this->assign("d",$data);
		$this->assign("order",$id);
		$this->assign('time',(new Order)->where($map)->value('create_time'));
		$this->assign("res",$res);
		return view();
	}
	
}