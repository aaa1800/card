<?php
namespace app\Home\Controller;

use think\Controller;
use app\common\model\Config;
use app\common\model\News;
use app\common\Model\User;

class WebController extends Controller{
	
	public function __construct(){
        parent::__construct();
		
		$this->setsConfig();
		if(C('webopen')!="on"){
			$this->close(C('wclose'));
		}
		$list=(new News)->news(8);
		$user=User::get(['token'=>session("token"),'timeout'=>['gt',time()]]);
		$this->rea=$user['real'];
		$this->assign("u",$user);
		$this->assign("login",session('?token')?1:0);
		$this->assign("newss",$list);
		
        
    }
	public function setsConfig(){
			$config=Config::all();
			foreach ($config as $k=>$v){
				C($v['name'],$v['value']);
			}
			
	    }
}