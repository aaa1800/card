<?php
namespace app\Home\controller;
use think\Controller;
use think\Request;
use app\common\model\MoneyLog;
use app\common\Model\User;
use app\common\model\Userbank;
use app\common\model\Withdraw;
use app\common\model\Order;


class MoneylogController extends ConfigController {
    
	
	public function getDa(){
		$da=input();
		if(empty($da['keywords'])){
			if(!empty($da['startDate'])){
				$map['addtime']=['between',[strtotime($da['startDate']),strtotime($da['endDate'])]];
			}
			if(!empty($da['type'])){
				$map['type']=$da['type'];
			}
			if(!empty($da['status'])){
				$map['price']=$da['status']==1?['gt',0]:['lt',0];
			}
		}else{
			$map['pil|danka']=$da['keywords'];
		} 
		$uid=$this->user['id'];

		$map['uid']=$uid;
		$ok=(new MoneyLog)->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
		return $ok;
	}
	public function mgetDa(){
		$da=input();
		if($da['type']){
			$map['type']=$da['type']<3?['lt',3]:$da['type'];
		}
		$uid=$this->user['id'];
		$map['uid']=$uid;
		$ok=(new MoneyLog)->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
		return json($ok);
		
	}
	public function asset(){
		$da=input();
		if(empty($da['keywords'])){
			if(!empty($da['startDate'])){
				$map['addtime']=['between',[strtotime($da['startDate']),strtotime($da['endDate'])]];
			}
		}else{
			$map['id']=$da['keywords'];
		} 
		$uid=$this->user['id'];
		$map['assets']=$uid;
		$ok=(new user)->field('id,addtime')->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
		$arr=[];
		$ok['totalPrice']=0;
		$ok['totalCommision']=0;
		foreach($ok['data'] as $k=>$v){
			switch($da['type']){
				case 2:
				   $m=(new Order)->where(['uid'=>$v['id']])->sum('money');
				   if($m<300){
					   $arr[$k]['type']=0;
					   $arr[$k]['regTime']=$v['addtime'];
					   $arr[$k]['subordinateMemberNumber']=$v['id'];
					  $arr[$k]['totalPrice']=(new Order)->where(['uid'=>$v['id']])->sum('money')?:0;
					  $arr[$k]['totalCommision']=sprintf("%.2f",$arr[$k]['totalPrice']*0.03);
					  $ok['totalPrice']+=$arr[$k]['totalPrice'];
					  $ok['totalCommision']+=$arr[$k]['totalCommision'];
				   }
				break;
				case 1:
				  $m=(new Order)->where(['uid'=>$v['id']])->sum('money');
				   if($m>300){
					   $arr[$k]['type']=1;
					   $arr[$k]['regTime']=$v['addtime'];
					   $arr[$k]['subordinateMemberNumber']=$v['id'];
					  $arr[$k]['totalPrice']=(new Order)->where(['uid'=>$v['id']])->sum('money')?:0;
					  $arr[$k]['totalCommision']=sprintf("%.2f",$arr[$k]['totalPrice']*0.03);
					  $ok['totalPrice']+=$arr[$k]['totalPrice'];
					  $ok['totalCommision']+=$arr[$k]['totalCommision'];
				   }
				break;
				default:
				  $arr[$k]['regTime']=$v['addtime'];
				  $arr[$k]['subordinateMemberNumber']=$v['id'];
				  $arr[$k]['totalPrice']=(new Order)->where(['uid'=>$v['id']])->sum('money')?:0;
				  $arr[$k]['type']=($arr[$k]['totalPrice']>300)?1:0;
				  $arr[$k]['totalCommision']=sprintf("%.2f",$arr[$k]['totalPrice']*0.03);
				  $ok['totalPrice']+=$arr[$k]['totalPrice'];
				  $ok['totalCommision']+=$arr[$k]['totalCommision'];
			}
		}
		$ok['data']=$arr;
		return json($ok);
	}
	public function masset(){
		$da=input();
		$uid=$this->user['id'];
		$map['assets']=$uid;
		$ok=(new user)->field('id,addtime')->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
		$arr=[];
		foreach($ok['data'] as $k=>$v){
			switch($da['type']){
				case 2:
				   $m=(new Order)->where(['uid'=>$v['id']])->sum('money');
				   if($m<300){
					   $arr[$k]['type']=0;
					   $arr[$k]['regTime']=$v['addtime'];
					   $arr[$k]['subordinateMemberNumber']=$v['id'];
					   $arr[$k]['totalPrice']=(new Order)->where(['uid'=>$v['id']])->sum('money')?:0;
					   $arr[$k]['totalCommision']=sprintf("%.2f",$arr[$k]['totalPrice']*0.03);
				   }
				break;
				case 1:
				  $m=(new Order)->where(['uid'=>$v['id']])->sum('money');
				   if($m>300){
					   $arr[$k]['type']=1;
					   $arr[$k]['regTime']=$v['addtime'];
					   $arr[$k]['subordinateMemberNumber']=$v['id'];
					  $arr[$k]['totalPrice']=(new Order)->where(['uid'=>$v['id']])->sum('money')?:0;
					  $arr[$k]['totalCommision']=sprintf("%.2f",$arr[$k]['totalPrice']*0.03);
				   }
				break;
				default:
				  $arr[$k]['regTime']=$v['addtime'];
				  $arr[$k]['subordinateMemberNumber']=$v['id'];
				  $arr[$k]['totalPrice']=(new Order)->where(['uid'=>$v['id']])->sum('money')?:0;
				  $arr[$k]['type']=($arr[$k]['totalPrice']>300)?1:0;
				  $arr[$k]['totalCommision']=sprintf("%.2f",$arr[$k]['totalPrice']*0.03);
			}
		}
		$ok['data']=$arr;
		return json($ok);
	}
	public function banklist(){
		$type=input('type');
		$bank=Userbank::all(['uid'=>session('user_id'),'isdel'=>1]);
		$arr=[['addTime'=> 1504608300000,'enable'=> C('kali'),'id'=> "1",'logo'=>"http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_weixin.png",'
name'=>"支付宝",'orders'=>2,'platform'=>"1,2,3",'poundage'=>0,'type'=>"4",'updateTime'=>1575345490000]];
//               ['addTime'=> 1516763280000,'enable'=> C('kbank'),'id'=> "3",'logo'=> "http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_unpay.png",'name'=> "银行卡快速提现",'orders'=> 3,'platform'=> "1,2,3",'poundage'=> 0,'type'=> "3",
// 'updateTime'=> 1575345498000],
//               ['addTime'=>1504608300000,'enable'=> C('kweixin'),'id'=> "1",'logo'=> "http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_weixin.png",'name'=> "微信",'orders'=> 2,'platform'=> "1,2,3",'poundage'=> 0,'type'=> "1",'updateTime'=> 1590984676000]];
		$weixin=[];
		$list=[];
		$alipay=[];
		foreach($bank as $k=>$v){
			switch($v['bankname']){
				case '支付宝':
				    if(C('kali')=="on"){
						$alipay[$k]['account']=tfen($v['accounts'],2,4);
						$alipay[$k]['bankCodeName']=$v['bankname'];
						$alipay[$k]['id']=$v['id'];
						$alipay[$k]['sourceAccount']= $v['accounts'];
					}
					break;
				case 'weixin':
				  if(C('kweixin')=="on"){
				    $weixin[$k]['account']=$v['accounts'];
					$weixin[$k]['bankCodeName']=$v['user'];
					$weixin[$k]['id']=$v['id'];
					$weixin[$k]['sourceAccount']= $v['accounts'];
				  }
				break;
				default:
				  if(C('kbank')=="on"){
				    $list[$k]['account']=tfen($v['accounts'],8,4);
					$list[$k]['bankCodeName']=$v['bankname'];
					$list[$k]['id']=$v['id'];
					$list[$k]['sourceAccount']= $v['accounts']; 
				  }
			}
		}
		$arr=["bankCardList"=>array_merge($list),"drawTypeSettings"=>$arr,'aliPayAccountList'=>array_merge($alipay),"weixinDrawAccountList"=>$weixin,"m"=>$this->user];
		return json($arr);
	}
	public function mbanklist(){
		$type=input('type');
		$bank=Userbank::all(['uid'=>session('user_id'),'isdel'=>1]);
		$arr=[
			['addTime'=> 1504608300000,'enable'=> true,'id'=> "1",'logo'=>"http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_weixin.png",'name'=>"支付宝",'orders'=>2,'platform'=>"1,2,3",'poundage'=>0,'type'=>"4",'updateTime'=>1575345490000],
			['addTime'=> 1516763280000,'enable'=> true,'id'=> "3",'logo'=> "http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_unpay.png",'name'=> "银行卡快速提现",'orders'=> 3,'platform'=> "1,2,3",'poundage'=> 0,'type'=> "3",
'updateTime'=> 1575345498000]
			];
			
			
// 		['addTime'=> 1516763280000,'enable'=> true,'id'=> "3",'logo'=> "http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_unpay.png",'name'=> "银行卡快速提现",'orders'=> 3,'platform'=> "1,2,3",'poundage'=> 0,'type'=> "3",
// 'updateTime'=> 1575345498000],['addTime'=>1504608300000,'enable'=> true,'id'=> "1",'logo'=> "http://kaxiaoshou.oss-cn-shenzhen.aliyuncs.com/template/images/comm/icon_manag_weixin.png",'name'=> "微信",'orders'=> 2,'platform'=> "1,2,3",'poundage'=> 0,'type'=> "1",'updateTime'=> 1590984676000]];
		$weixin=[];
		$list=[];
		$alipay=[];
		foreach($bank as $k=>$v){
			switch($v['bankname']){
				case '支付宝':
					$alipay[$k]['account']=tfen($v['accounts'],2,4);
					$alipay[$k]['bankCodeName']=$v['bankname'];
					$alipay[$k]['id']=$v['id'];
					$alipay[$k]['sourceAccount']= $v['accounts'];
					break;
				case 'weixin':
				    $weixin[$k]['account']=$v['accounts'];
					$weixin[$k]['bankCodeName']=$v['user'];
					$weixin[$k]['id']=$v['id'];
					$weixin[$k]['sourceAccount']= $v['accounts'];
				break;
				default:
				    $list[$k]['account']=tfen($v['accounts'],8,4);
					$list[$k]['bankCodeName']=$v['bankname'];
					$list[$k]['id']=$v['id'];
					$list[$k]['sourceAccount']= $v['accounts']; 
			}
		}
		$arr=["bankCardList"=>array_merge($list),"drawTypeSettings"=>$arr,'aliPayAccountList'=>array_merge($alipay),"weixinDrawAccountList"=>$weixin,"m"=>$this->user];
		return json($arr);
	}
	public function tixian(){
		$da=input();
		if(!empty($da['startDate'])){
			$map['addtime']=['between',[strtotime($da['startDate']),strtotime($da['endDate'])]];
		}
		$uid=$this->user['id'];
		$map['uid']=$uid;
		$ok=(new Withdraw)->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
		foreach($ok['data'] as $k=>$v){
			$kl=(new User)->where(['id'=>$v['uid']])->value('username');
			$ok['data'][$k]['memberRealName']=$kl;
			$ok['data'][$k]['poundage']=$v['price'];
			$ok['data'][$k]['shiji']=$v['money']-$v['price'];
			$ok['data'][$k]['shtime']=$v['shtime']?$v['shtime']:'--';

		}
		return $ok;
	}
	public function mtixian(){
		$da=input();
		if(!empty($da['status'])){
			$map['status']=$da['status'];
		}
		$uid=$this->user['id'];
		$map['uid']=$uid;
		$ok=(new Withdraw)->where($map)->order("id desc")->paginate(array('list_rows'=>$da['pageSize'],'page'=>$da['pageNumber']))->toArray();
		foreach($ok['data'] as $k=>$v){
			$kl=(new User)->where(['id'=>$v['uid']])->value('username');
			$ok['data'][$k]['memberRealName']=$kl;
			$ok['data'][$k]['poundage']=$v['price'];
			$ok['data'][$k]['shiji']=$v['money']-$v['price'];
			$ok['data'][$k]['shtime']=$v['shtime']?$v['shtime']:'--';

		}
		return json($ok);
	}
		
	
}