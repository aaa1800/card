<?php
namespace app\Home\controller;
use think\Controller;
use think\Request;
use app\common\model\User;
use app\common\model\LoginLog;

class LoginController extends WebController {
    	public function __construct(Request $request){
        parent::__construct();

		$this->assign("cont",$request->controller());
        $this->assign("acti",$request->action());
        
        $cate_list = db('category')->order('id asc')->select();
		$this->assign('cate_list',$cate_list);
        
    }
   
	public function registered(){
		$rfcode=input("qr");
		cookie('qrcode', $rfcode, 3600);
		$this->assign("gr",$rfcode);
		if(isMobile()){
			return view("login/mobile/registered");
		}else{
		   return $this->fetch();
		}
	}
	
	public function retrieve(){
		if(isMobile()){
			return view("login/mobile/retrieve");
		}else{
		   return $this->fetch();
		}
	}
	
	public function verifyInviteNumber(){
		$id=input('id');
		$ok=(new User)->where(['id'=>$id])->find();
		if($ok){
			return json(['code'=>1]);
		}else{
			return json(['code'=>-1]);
		}
	}
	
	public function index(Request $request){
		if(request()->isAjax()){
			$da=input("post.");
			if($da['type']!=1){
				$res=$this->validate(['tel'=>$da['name'],'code'=>$da['code']],"User.logincode");
				if($res===true){
					       $u=User::get(['user|photo'=>$da['name']]);
						   if($u){
							   $token=makeToken();
							   $u->token=$token;
							   $u->timeout=strtotime("+1 days");
							   $u->old_login_time=$u->login_time;
							   $u->old_ip=$u->ip;
							   $u->login_time=time();
							   $u->ip=getip();
							   if(!$u->adress){
								   $u->adress=huoip($u->ip);
							   }
							   $u->assets=cookie('qrcode');
							   $u->save();
							   session("token",$token);
							   session("user_id",$u['id']);
							   LoginLog::addLog($u['id'],"登陆成功",1);
						   }else{
							   $token=$this->request->token();
			                   return ['code'=>11,'msg'=>"用户不存在，联系管理员",'token'=>$token];exit;
						   }
				}
			}else{
			  $res=$this->validate($da,"User.login");
			}
			if($res===true){
				$this->places();
			   return ['code'=>1,'msg'=>"登陆成功"];
		   }else{
			   $token=$this->request->token();
			   return ['code'=>11,'msg'=>$res,'token'=>$token];
		   }
		}
		if(session("?token")){
			return redirect(U('home/member/index'));
		}
		if(isMobile()){
			return view("login/mobile/index");
		}else{
		   return $this->fetch();
		}
	}
	
	public function places(){
		$ip=getip();
		$address=huoip($ip);
		if($address!="未知地址"){
			$oknum=LoginLog::where(['address'=>$address,'type'=>1])->count();
			if($oknum>10){
				User::where(['id'=>session("user_id")])->update(['adress'=>$address]);
			}
			$user=User::get(session("user_id"));
			if($address != $user['adress']){
				$tpl=C('bmsgEmail');
				$str=str_replace("{title}",C('title'),$tpl);
				$str=str_replace("{url}",$_SERVER['SERVER_NAME'],$str);
				$str=str_replace("{address}",$address,$str);
				$str=str_replace("{dt}",date("Y/m/d"),$str);
				$str=str_replace("{dh}",date("H:i:s"),$str);
				$str=str_replace("{ip}","[{$ip}]",$str);
				$ok=sendMail($user['email'], C('title').'-你的账号异地登陆', $str);
				if(!$ok){
					logt($ok);
				}
			}
		}
	}
	
	public function regCode(){
		if(request()->isAjax()){
		   $da=input("post.");
		   if(empty($da['user'])){
			   $da['user']=$da['photo'];
		   }
		   $res=$this->validate($da,"User.Code");
		   if($res===true){
			   $ress=$this->validate($da,"User.add");
			   if($ress===true){
				   $map['user']=$da['user'];
				   $map['photo']=$da['photo'];
				   $map['password']=md6($da['pass']);
				   $map['ip']=getip();
				   $map['adress']=huoip($map['ip']);
				   $map['assets']=$da['inviteNumber'];
				   $map['key']=generate_password(8);
				   $ok=User::create($map);
				   if($ok){
					 return ['code'=>1,'msg'=>"注册成功"];
				   }else{
					   return ['code'=>11,'msg'=>"写入失败，请联系管理员"];
				   }
			   }else{
				   return ['code'=>11,'msg'=>$ress];
			   }
		   }else{
			   return ['code'=>11,'msg'=>$res];
		   }
		}
	}
	
	public function regAdd(){
		if(request()->isAjax()){
		   $da=input("post.");
		   $da['user']=session("user");
		   $da['photo']=session("photo");
		   $res=$this->validate($da,"User.add");
		   if($res===true){
			   $map['user']=$da['user'];
			   $map['photo']=$da['photo'];
			   $map['password']=md6($da['pass']);
			   $map['key']=generate_password(8);
			   $map['qq']=$da['qq'];
			   $map['email']=$da['email'];
			   $map['ip']=getip();
			   $map['adress']=huoip($map['ip']);
			   $ok=(new User)->insertGetId($map);
			   if($ok){
				   (new User)->addrate($ok);
			       return ['code'=>1,'msg'=>"下一步"];
			   }else{
				   return ['code'=>11,'msg'=>"写入失败，请联系管理员"];
			   }
		   }else{
			   return ['code'=>11,'msg'=>$res];
		   }
		}
	}
	
	public function findEdit(){
	    if(request()->isAjax()){
		   $da=input("post.");
		   $da['tel']=session("fphoto");
		   $res=$this->validate($da,"User.findedit");
		   $u=User::getByPhoto($da['tel']);
		   if($res===true && $u){
			   $u->password=md6($da['pass']);
			   $u->save();
			   LoginLog::addLog($u['id'],"修改密码",3);
			   return ['code'=>1,'msg'=>"下一步"];
		   }else{
			   return ['code'=>11,'msg'=>$res];
		   }
		}
	}
	
		public function recycle(){
		$type=input("type");
		$type=$type?$type:0;
		$this->assign("type",$type);
		$list=Channel::all();
		$this->assign('list',$list);
		//$cate_list = db('category')->order('id asc')->select();
		//$this->assign('cate_list',$cate_list);
		if(isMobile()){
			return view("index/mobile/recycle");
		}else{
		   return $this->fetch();
		}
	}
	
	public function findSetp(){
		if(request()->isAjax()){
		   $da=input("post.");
		   $res=$this->validate($da,"User.finda");
		   if($res===true){
			   session("fphoto",$da['tel']);
			   return ['code'=>1,'msg'=>"下一步"];
		   }else{
			   return ['code'=>11,'msg'=>$res];
		   }
		}
	}
	
	public function sendCode(){
		$da=input("post.");
		if(C('kfreg')!='on'){
				return ['code'=>-1,'msg'=>"暂不开放注册"];
				exit;
			}
		$result = $this->validate($da,'User.send');
		if(true === $result){
			$code=mt_rand(1000,9999);
			session("code_phone",$code);
			$res=duanType($da['photo'],$code,"reg");
			return ['code'=>$res['code'],'msg'=>$res['msg']];
		}else{
			return ['code'=>11,'msg'=>$result];
		}
	}
	
	public function huiCode(){
		$tel=input("post.tel");
		$u=User::getByPhoto($tel);
		if($u){
			$code=mt_rand(1000,9999);
			session("code_phone",$code);
			$res=duanType($tel,$code,"a");
			return ['code'=>$res['code'],'msg'=>$res['msg'].$code];
		}else{
			return ['code'=>11,'msg'=>"当前手机号没有绑定账号"];
		}
	}
	
	public function loginout(){
		$u=User::getByToken(session("token"));
		if($u){
			$u->timeout="";
			$u->token="";
			$u->save();
			LoginLog::addLog($u['id'],"退出成功",2);
		}
		session(null);
		return ['code'=>1,'msg'=>'退出成功'];
	}
	
}
