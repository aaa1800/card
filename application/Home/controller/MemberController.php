<?php
namespace app\Home\controller;
use think\Controller;
use think\Request;
use app\common\model\PayOrder;

use app\common\model\MoneyLog;
use app\common\model\LoginLog;
use app\common\Model\User;
use app\common\Model\MoneyOrder;
use app\common\Model\Userbank;
use app\common\model\Withdraw;
use app\common\model\YongjinLog;
use app\common\model\BankName;
use app\common\model\Qiye;
use app\common\model\Gaoji;
use app\common\model\Weixin;
use app\common\model\Cashwith;
use app\common\model\Banks;




class MemberController extends ConfigController {
    public function index(){
		$ye=input("main");
		$type=$ye?$ye:"maina";
		$this->assign("url",U('home/member/'.$type));
		
       if(isMobile()){
			return view("member/mobile/index");
		}else{
		   return view();
		}
    }
	public function info(){
		if(isMobile()){
			return view("member/mobile/info");
		}else{
		   return view();
		}
	}
	public function getJuniorAuthenStatus(){
		if($this->user['real']>0){
			$arr=['code'=>1,'msg'=>'','object'=>true];
		}else{
			$arr=['code'=>1,'msg'=>'请先实名认证','object'=>false];
		}
		return json($arr);
	}
	public function autoSend(){
		$this->assign("type",0);
		return view();
	}
	public function profileManage(){
		$arr=['qq'=>$this->user['qq'],'attentionTime'=> $this->user['addtime'],'id'=> $this->user['id'],'isCashAccount'=> "0",'isRealNameAuthen'=> $this->user['real']?true:false,'mail'=> tfen($this->user['email'],3,4),'nickName'=> $this->user['user'],'number'=> 90532,'phone'=> tfen($this->user['photo'],3,4),'qqStatus'=> "2",'realName'=> $this->user['username'],'status'=>$this->user['state']];
		return json($arr);
	}
	public function Qiyeup(Request $request){
		    $data=input();
			$files = request()->file('image');
			$msg=['code'=>0,'msg'=>"没有文件"];
			$arr=['idjust','idback','license'];
			$i=0;
			if(empty($data['name'])){
				return json(['code'=>-1,'msg'=>'请填写企业名称']);exit;
			}
			if(empty($data['number'])){
				return json(['code'=>-1,'msg'=>'请填写营业执照证件号']);exit;
			}
			if(empty($data['idCardStartDate'])){
				return json(['code'=>-1,'msg'=>'请填写营业执照颁发日期']);exit;
			}
			if(empty($data['idCardEndDate'])){
				return json(['code'=>-1,'msg'=>'请填写营业执照有效期']);exit;
			}
			foreach($files as $file){
				$info = $file->validate(['size'=>524278,'ext'=>'jpg,png,gif','hex'=>true])->move(ROOT_PATH . 'public/static' . DS . 'uploads'. DS .'real');
				if($info){
					$path=str_replace('\\','/',$info->getSaveName());
				    $reurl = '/static/uploads/real/'. $path;
					$data[$arr[$i]]=$reurl;
					$i++;
				}else{
					$msg=['code'=>0,'msg'=>$file->getError()];
					return json($msg);
					break;
				}
			}
			if(!isset($data['idjust'])){
				return json(['code'=>-1,'msg'=>'请上传法人身份证正面']);exit;
			}
			if(!isset($data['idback'])){
				return json(['code'=>-1,'msg'=>'请上传法人身份证背面']);exit;
			}
			if(!isset($data['license'])){
				return json(['code'=>-1,'msg'=>'请上传营业执照']);exit;
			}
			$data['uid']=$this->user['id'];
			$id=(new Qiye)->where(['uid'=>$this->user['id']])->value('id');
			$data['state']=0;
			if($id){
			    $ok=(new Qiye)->allowField(true)->save($data,['id'=>$id]);
			}else{
			    $ok=(new Qiye)->allowField(true)->save($data);
			}
			if($ok){
				return json(['code'=>1,'msg'=>'提交成功等待审核']);
			}else{
				return json(['code'=>-1,'msg'=>'系统错误，联系管理员']);
			}
			
	}
	public function Gaojiup(Request $request){
		    $data=input();
			$files = request()->file('image');
			$msg=['code'=>0,'msg'=>"没有文件"];
			$arr=['idjust','idback','license'];
			$i=0;
			foreach($files as $file){
				$info = $file->validate(['size'=>524278,'ext'=>'jpg,png,gif','hex'=>true])->move(ROOT_PATH . 'public/static' . DS . 'uploads'. DS .'real');
				if($info){
					$path=str_replace('\\','/',$info->getSaveName());
				    $reurl = '/static/uploads/real/'. $path;
					$data[$arr[$i]]=$reurl;
					$i++;
				}else{
					$msg=['code'=>0,'msg'=>$file->getError()];
					return json($msg);
					break;
				}
			}
			if(!isset($data['idjust'])){
				return json(['code'=>-1,'msg'=>'请上传身份证正面']);exit;
			}
			if(!isset($data['idback'])){
				return json(['code'=>-1,'msg'=>'请上传身份证背面']);exit;
			}
			if(!isset($data['license'])){
				return json(['code'=>-1,'msg'=>'请上传手持身份证照']);exit;
			}
			$data['uid']=$this->user['id'];
			$id=(new Gaoji)->where(['uid'=>$this->user['id']])->value('id');
			$data['state']=0;
			if($id){
			    $ok=(new Gaoji)->allowField(true)->save($data,['id'=>$id]);
			}else{
			    $ok=(new Gaoji)->allowField(true)->save($data);
			}
			if($ok){
				return json(['code'=>1,'msg'=>'提交成功等待审核']);
			}else{
				return json(['code'=>-1,'msg'=>'系统错误，联系管理员']);
			}
	}
	
	public function AuthenInitQiye(){
		$da=(new Qiye)->where(['uid'=>$this->user['id']])->find();
		$this->assign("da",$da);
		$this->assign("ok",$da?$da['state']:-1);
		if(isMobile()){
			return view("member/mobile/authen_init_qiye");
		}else{
		   return view();
		}
	}
	
	public function AuthenInitGaoji(){
		$da=(new Gaoji)->where(['uid'=>$this->user['id']])->find();
		$this->assign("da",$da);
		$this->assign("ok",$da?$da['state']:-1);
		if(isMobile()){
			return view("member/mobile/authen_init_gaoji");
		}else{
		   return view();
		}
	}
	public function realhelp(){
		return view('member/mobile/realhelp');
	}
	public function maina(){
		return view('domember');
	}
	public function deleteBankCard(){
		if(request()->isAjax()){
			$id=input('post.id');
			$pass=input("post.tradePassword");
			if(md6($pass,$this->user['jiaoyimima'])===true){
				Userbank::destroy($id);
				return ['code'=>1,'msg'=>'删除成功'];
			}else{
					return ['code'=>-1,'msg'=>'交易密码错误'];
				}
		}
	}
	public function addbank(){
		if(request()->isAjax()){
			$da=input("post.");
			$pass=$da['tradePassword'];
			$bankno=$da['cardNumber'];
			$zhibank=$da['zhibank'];
			$bankname=$da['bankname'];
			if(!$bankno){
				return ['code'=>-1,'msg'=>'请输入银行账号'];exit;
			}
			$id=isset($da['id'])?$da['id']:"";
//			if(empty($da['bankname'])){
//				return ['code'=>-1,'msg'=>'开户行不能为空'];exit;
//			}
			if(md6($pass,$this->user['jiaoyimima'])===true){
			    $da['uid']=$this->user['id'];
				$da['bankname']=$bankname;
				$da['accounts']=$bankno;
				$da['zhibank']=$zhibank;
				$da['user']=$this->user['username'];
				if($id){
					$ok=(new Userbank)->allowField(true)->save($da,['id'=>$id]);
				}else{
					$ok=(new Userbank)->allowField(true)->save($da);
				}
				if($ok){
					return ['code'=>1,'msg'=>'添加成功'];
				}else{
					return ['code'=>-1,'msg'=>'未做变更'];
				}
			}else{
				return ['code'=>-1,'msg'=>'交易密码错误'];
			}
		}else{
			$id=input("get.id");
			$this->assign("id",$id);
			$res=(new Userbank)->where(['id'=>$id])->find();
			$banks=Banks::all();
			$this->assign("banks",$banks);
			$this->assign("res",$res);
			return view();
		}
	}
	public function addali(){
		if(request()->isAjax()){
			$da=input("post.");
			$pass=$da['tradePassword'];
			$bankno=$da['account'];
			if(!$bankno){
				return ['code'=>-1,'msg'=>'请输入账号'];
			}
			$id=isset($da['id'])?$da['id']:"";
			if(md6($pass,$this->user['jiaoyimima'])===true){
			    $map['uid']=$this->user['id'];
				$map['bankname']="支付宝";
				$map['accounts']=$bankno;
				$map['user']=$this->user['username'];
				if($id){
					$ok=(new Userbank)->save($map,['id'=>$id]);
				}else{
					$ok=(new Userbank)->save($map);
				}
				if($ok){
					return ['code'=>1,'msg'=>'添加成功'];
				}else{
					return ['code'=>-1,'msg'=>'系统错误'];
				}
			}else{
				return ['code'=>-1,'msg'=>'交易密码错误'];
			}
		}else{
			$id=input("get.id");
			$ty=input("ty");
			$res=(new Userbank)->where(['id'=>$id])->find();
			$this->assign("id",$id);
			$this->assign("res",$res);
			$banks=Banks::all();
			$this->assign("banks",$banks);
			$this->assign("type",($res['bankname']=="支付宝" || $ty==1)?1:0);
			if(isMobile()){
				 return view('member/mobile/edit_bank');
			}else{
			  return view();
			}
		}
	}
	public function addqq(){
		if(request()->isAjax()){
			$qq=input('qq');
			if($qq){
				(new User)->where(['id'=>session('user_id')])->setField('qq',$qq);
				return json(['code'=>1,'msg'=>'操作成功']);
			}else{
				return json(['code'=>-1,'msg'=>'更新失败']);
			}
		}
		return view('member/mobile/addqq');
	}
	public function getDa(){
		$uid=$this->user['id'];
		$a=[];
		$b=[];
		$c=[];
		$s=date("Y-m-d 00:00:00");
		$e=date("Y-m-d 23:59:59");
		for($i=0;$i<40;$i++){
			$s1=PayOrder::where(['uid'=>$uid,'status'=>1,'addtime'=>['between time',[$s,$e]]])->sum("money");
			$s2=TxOrder::where(['uid'=>$uid,'status'=>1,'addtime'=>['between time',[$s,$e]]])->sum("money");
			$s3=MoneyLog::where(['uid'=>$uid,'type'=>4,'addtime'=>['between time',[$s,$e]]])->sum("money");
			$rd=(int)date("md",strtotime($s));
			$a[$i]['x']=$rd;
			$a[$i]['y']=$s1;
			$b[$i]['x']=$rd;
			$b[$i]['y']=$s2;
			$c[$i]['x']=$rd;
			$c[$i]['y']=$s3;
			$s=date("Y-m-d 00:00:00", strtotime("-1 day", strtotime($s)));
			$e=date("Y-m-d 23:59:59", strtotime("-1 day", strtotime($e)));
		}
		sort($a);
		sort($b);
		sort($c);
		return ['code'=>1,'data'=>[['key'=>'收款','values'=>$a],['key'=>'汇款','values'=>$b],['key'=>'佣金','values'=>$c]]];
        		
	}
		public function tixian(){
		if(request()->isAjax()){
			$da=input("post.");
			$pid=$da['bankCardId'];
			$mima=$da['tradePassword'];
			if(md6($mima,$this->user['jiaoyimima'])===true){
				if(empty($da['money']) || $da['money']<1)return ['code'=>0,'msg'=>'金额不能为空或小于1'];
				$u=User::get($this->user['id']);
				if($u->money <$da['money']){
					return ['code'=>0,'msg'=>'余额不足'];
					exit;
				}
					$uk=(new Userbank)->where(['id'=>$pid])->find();
				if(!$uk){
					return ['code'=>-1,'msg'=>'参数错误'];
					exit;
				}
				
				if($uk['bankname']=='weixin' && $da['money']>C('wxxian')){
					return ['code'=>-1,'msg'=>'微信单次最高提现'.C('wxxian')];
					exit;
				}elseif($uk['bankname']=='支付宝' && $da['money']>C('alixian')){
					return ['code'=>-1,'msg'=>'支付宝单次最高提现'.C('alixian')];
					exit;
				}elseif($da['money']>C('bankxian')){
						return ['code'=>-1,'msg'=>'银行卡单次最高提现'.C('bankxian')];
					exit;
				}
				$u->money=$this->user['money']-$da['money'];
				$u->price=$this->user['price']+$da['money'];
				$ok=$u->save();
				if(!$ok){
					return ['code'=>0,'msg'=>'操作失败，余额不足'];
					exit;
				}
				$da['order']=pay_order("W");
				(new MoneyLog)->addLog($da['money'],3,"余额提现",$da['order']);
			
				$da['zhname']=$uk['bankname'];
				$da['zh']=$uk['accounts'];
				$da['uid']=$this->user['id'];
				$da['price']=0;//手续费
				$da['txtype']=1;
				$da['umoney']=$u->money;
				if($da['type']==3){
					$da['dizhi']=$da['bankCardId'];
				}
				$rus=(new Withdraw)->allowField(true)->save($da);
				if($da['zhname']=='支付宝' && C('alitxopen')=="jjon"){
					$res=(new Cashwith)->cash($da['order'],$da['zh'],$da['money']-$da['price'],"ASSR");
					$dad['shtime']=time();
					$AD=new Withdraw;
					if($res['code']==1){
						$dad['content']=$res['msg'];
				        $dad['status']=1;
						$ok=$AD->where(['order'=>$da['order']])->update($dad);
						(new User)->where(['id'=>$da['uid']])->setDec("price",$da['money']);
						return ['code'=>1,'msg'=>'提现成功'];exit;
					}else{
						$dad['content']=$res['msg'];
						$ok=$AD->where(['order'=>$da['order']])->update($dad);
					   return json(['code' => 0, 'msg' =>$res['msg']]);exit;
					}
				}else{
				  return ['code'=>1,'msg'=>'提现成功，等待审核'];
				}
			}else{
				return ['code'=>-1,'msg'=>'交易密码错误'];
			}
		}else{
			if(isMobile()){
			return view("member/mobile/tixian");
		}else{
		   return view('withdrawCash');
		}
			
		}
	}
	public function getDetail(){
		$id=input('id');
		$data=(new Withdraw)->where(['id'=>$id])->find();
		$data['username']=(new User)->where(['id'=>$data['uid']])->value('username');
		$this->assign("da",$data);
		return view();
	}	
	public function userinfo(){
		if(request()->isAjax()){
			$list=(new LoginLog())->getList();
			return ['data'=>$list['data'],'draw'=>$list['last_page'],'recordsFiltered'=>$list['current_page'],'recordsTotal'=>$list['total']];
		}
		return $this->fetch();
	}
	public function sendT(){
		if(request()->isAjax()){
			$da=$this->user['photo'];
			$code=rand(1000,9999);
			session("code",$code);
		   return duanType($da,$code,'a');
		}
	}
	
	public function editemail(){
		if(request()->isAjax()){
			$da=input('post.');
			if(empty($da['yzm']))return ['code'=>-1,'msg'=>'验证码不能为空'];
			if(empty($da['email']))return ['code'=>-1,'msg'=>'邮箱不能为空'];
			if(session("codeE")!=$da['yzm'])return ['code'=>-1,'msg'=>'验证码错误'];
		//	if(session('emailAuth')!="ok")return ['code'=>-1,'msg'=>'非法操作'];
			$map['email']=$da['email'];
			$ok=User::where(['id'=>$this->user['id']])->update($map);
			session('emailAuth',null);
		    return ['code'=>1,'msg'=>'修改成功'];
		}else{
			$cz=input("cookies");
			if($cz==10 && session('emailAuth')!="ok"){
				$cz=12;
			}
			$this->assign("cookies",$cz);
			if(isMobile()){
				return view('member/mobile/editemail');
			}else{
			  return view();
			}
		}
	}
	public function addemail(){
		if(request()->isAjax()){
			$da=input('post.');
			if(empty($da['yzm']))return ['code'=>-1,'msg'=>'验证码不能为空'];
			if(empty($da['email']))return ['code'=>-1,'msg'=>'邮箱不能为空'];
			if(session("codeE")!=$da['yzm'])return ['code'=>-1,'msg'=>'验证码错误'];
			$map['email']=$da['email'];
			$ok=User::where(['id'=>$this->user['id']])->update($map);
		    return ['code'=>1,'msg'=>'修改成功'];
		}
	}
	public function addTel(){
		if(request()->isAjax()){
			$da=input();
			if(empty($da['yzm']))return ['code'=>102,'msg'=>'验证码不能为空'];
			if(empty($da['photo']))return ['code'=>103,'msg'=>'手机号不能为空'];
			if(session("codephone")!=$da['yzm'])return ['code'=>104,'msg'=>'验证码错误'];
			if(session("okedit")!=1)return ['code'=>105,'msg'=>'非法提交'];
			$ok=User::where(['id'=>$this->user['id']])->update(['photo' => $da['photo']]);
			session("codephone",null);session("okedit",null);
		    return ['code'=>1,'msg'=>'修改成功'];
		}else{
			if(isMobile()){
				return view('member/mobile/addtel');
			}else{
			return view();
			}
		}
	}
	public function addpass(){
		if(request()->isAjax()){
			 $code=input('code');
			 if(session('codephone')!=$code){
			     return json(['code'=>102,'msg'=>"验证码错误"]);
			 }else{
				 session("codephone",null);
				 session("okedit",1);
				 return json(['code'=>0,'msg'=>"成功"]);
			 }
		}else{
			if(isMobile()){
				return view('member/mobile/addpass');
			}else{
		      return view();
			}
		}
	}
	public function editPass(){
		if(request()->isAjax()){
			$da=input();
			$da['cz']=isset($da['cz'])?$da['cz']:0;
			$res=$this->validate($da,"User.edit");
			if($res===true){
				$typef=$da['cz']==2?"jiaoyimima":'password';
			   $ok=User::where(['id'=>$this->user['id']])->update([$typef => md6($da['ps1'])]);
		       return ['code'=>1,'msg'=>'修改成功'];
			}else{
				$token=$this->request->token();
				return ['code'=>-1,'msg'=>$res,'token'=>$token];
			}
		}else{
			$cz=input("cz");
			$this->assign("cz",$cz);
			if(isMobile()){
					$str=$cz==2?"交易":"登陆";
					$this->assign('str',$str);
				return view('member/mobile/editPass');
			}else{
			  return view();
			}
		}
	}
	public function editTran(){
		    $da=input();
			$res=$this->validate($da,"User.editTran");
			if($res===true){
			   $ok=User::where(['id'=>$this->user['id']])->update(['jiaoyimima' => md6($da['tradePwd'])]);
		       return json(['code'=>1,'msg'=>'修改成功']);
			}else{
				$token=$this->request->token();
				return json(['code'=>-1,'msg'=>$res,'token'=>$token]);
			}
		
	}
	public function setKey(){
		if(request()->isAjax()){
			$key=generate_password(16);
			$ok=User::where(['id'=>$this->user['id']])->update(['key' => $key]);
		    return ['code'=>1,'msg'=>'修改成功','k'=>$key];
		}
	}
	
	public function passwordManageInit(){
		$arr=["id"=>$this->user['id'],"mail"=>$this->user['email'],"password"=>$this->user['password'],"phone"=>$this->user['photo'],"tradePassword"=>$this->user['jiaoyimima'],"username"=>$this->user['user']];
		return json($arr);
	}
	public function resetTradePasswordInit(){
		return view('member/mobile/resetTradePasswordInit');
	}
	
	public function addpwd(){
		if(isMobile()){
			return view('member/mobile/mima');
		}else{
	     return $this->fetch();
		}
	}
	public function Recharge(){
		if(request()->isAjax()){
			$money=sprintf("%.2f",input("post.jin"));
			$type=input("post.type");
			$order=pay_order("C");
			$res=(new MoneyOrder)->recharge($order,$money,$type);
			if($res['code']==1){
				$res['order']=$order;
			}
			return $res;
		}
		if(isMobile()){
			return view("member/mobile/recharge");
		}else{
		   return view();
		}
	}
	
	public function chonglog(){
		if(request()->isAjax()){
			$list=(new Withdraw)->getList();
			return ['data'=>$list['data'],'draw'=>(int)input('get.draw'),'recordsFiltered'=>$list['total'],'recordsTotal'=>$list['current_page']];
		}
		return $this->fetch();
	}

	public function settle(){
		if(request()->isAjax()){
			$list=(new MoneyOrder())->getList();
			return ['data'=>$list['data'],'draw'=>(int)input('get.draw'),'recordsFiltered'=>$list['total'],'recordsTotal'=>$list['current_page']];
		}
		return view();
	}
	public function moneylog(Request $reques){
		if(request()->isAjax()){
		  $list=(new MoneyLog())->getList();
		 return ['data'=>$list['data'],'draw'=>(int)input('get.draw'),'recordsFiltered'=>$list['total'],'recordsTotal'=>$list['current_page']];	
		}else{
			if(isMobile()){
				return view("member/mobile/moneylog");
			}else{
			   return view();
			}
		}
	}
	public function smain(){
		$arr=["copyright"=>C('coltd'),"csPhone"=>C('phone'),"csQQ"=>C('kefu'),"csTime"=>"周一至周日 8:00-24:00","descr"=>C('description'),"domain"=>$_SERVER['HTTP_HOST'],"email"=>"99905590@qq.com","icpNumber"=>C('beian'),"keywords"=>C('keywords'),"logo"=>C('logo'),"name"=>C('title'),"staticDomain"=>''];
		$this->user['generalizeGradeName']="普通推广员";
		$this->user['isRealNameAuthen']=$this->user['real'];
		$this->user['isSetTradePass']=$this->user['jiaoyimima']?makeToken():"";
		return json(['base'=>$arr,'m'=>$this->user]);
	}
	//卖卡
	public function sellCard(){
		$type=input("type");
		$type=$type?$type:0;
		$this->assign("type",$type);
		if(isMobile()){
				return view("member/mobile/sell_card");
			}else{
			   return view();
			}
	}

	public function restkey(){
		$user=User::get(session("user_id"));
		if($user){
			$user->key=generate_password(16);
			$user->save();
			$arr=["code"=>1,'msg'=>"重置密钥成功"];
		}else{
			$arr=["code"=>-1,'msg'=>"重置密钥失败"];
		}
		return $arr;
	}
	//实名
	public function AuthenInit(){
		if(request()->isAjax()){
			$da=input();
			$res=sheng($da['idcard'],$da['username']);
			if($res['error_code']!="0"){
				return json(['code'=>-1,'msg'=>$res['reason']]);
				exit;
			}
			$da['real']=1;
			$da['realtime']=time();
			$ok=(new User)->allowField(true)->save($da,['id'=>session("user_id")]);
			if($ok){
				return json(['code'=>1,'msg'=>"实名认证成功"]);
			}else{
				return json(['code'=>-1,'msg'=>"写入错误，请联系管理员"]);
			}
	      }else{
		   if(isMobile()){
				return view("member/mobile/authen_init");
			}else{
			   return view();
			}
		  }
	}
	//银行
	public function bankAccount(){
		$li=(new Userbank)->where(['uid'=>$this->user['id'],'bankname'=>['neq',"支付宝"]])->select();
		$this->assign("li",$li);
		$this->assign("num",count($li));
		if(isMobile()){
			return view('member/mobile/bank_account');
		}else{
		  return view();
		}

	}
	public function getBank(){
		$type=input("id");
		if($type==1){
			$map['bankname']='支付宝';
		}else{
			$map['bankname']=['neq','支付宝'];
		}
		$map['uid']=session('user_id');
		$li=(new Userbank)->where($map)->select();
		foreach($li as $k=>$v){
			$li[$k]['accounts']=tfen($v['accounts'],6,6);
		}
		$arr['bankCardList']=$li;
		$arr['isSureAddBankCard']=count($li)<5?true:false;
		$arr['m']=['id'=>session('user_id'),'idCard'=>tfen($this->user['idcard'],4,4),'isRealNameAuthen'=>$this->user['real']?true:false,'realName'=>$this->user['username'],'tradePassword'=>$this->user['jiaoyimima']];
		return json($arr);
	}
	public function aliPay(){
		$li=(new Userbank)->where(['uid'=>$this->user['id'],'bankname'=>"支付宝"])->select();
		$this->assign("li",$li);
		$this->assign("num",count($li));
		if(isMobile()){
			return view('member/mobile/alipay');
		}else{
		  return view();
		}

	}
	public function tixianbank(){
		$li=(new Userbank)->where(['uid'=>$this->user['id'],'bankname'=>['eq',"weixin"]])->count();
		$this->assign('num',$li);
		return view('member/mobile/withdraw_account');
	}
	public function weixin(){
		$li=(new Userbank)->where(['uid'=>$this->user['id'],'bankname'=>['eq',"weixin"]])->select();
		$this->assign("li",$li);
		$this->assign("num",count($li));
		if(isMobile()){
			return view('member/mobile/weixin');
		}else{
		  return view();
		}
	}
	public function addweixin(){
		$li=(new Userbank)->where(['uid'=>$this->user['id'],'bankname'=>['eq',"weixin"]])->select();
		$this->assign("li",$li);
		if(request()->isAjax()){
			return ['code'=>1,'message'=>'获取成功','url'=>(new Weixin)->Follow()];
		}
		if(isMobile()){
			return view('member/mobile/addweixin');
		}else{
		  return view();
		}
	}
	public function qqbind(){
		return view();
	}
	public function wxbind(){
		return view();
	}
	public function getMemberLimitAmount(){
		return json(['code'=>"000000",'object'=> -1]);
	}
	public function setting(){
		return view('member/mobile/setting');
	}
}