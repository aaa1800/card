<?php
namespace app\Home\Controller;

use think\Controller;
use app\common\Model\User;
use app\common\model\Config;
use app\common\model\News;
use app\common\model\Channel;
use think\Request;

class ConfigController extends WebController{
	public $user;
	
	public function __construct(Request $request){
        parent::__construct();
		$this->setConfig();
		if(C('webopen')!="on"){
			$this->close(C('wclose'));
		}
		if(!session("?token"))$this->redirect(U('home/login/index'));
		$user=User::get(['token'=>session("token"),'timeout'=>['gt',time()]]);
		if($user['state']==2){$this->error("该账号被冻结了,请联系管理员");}
		if(!$user){session(null);$this->error("你的账户在其他地方登陆了",U('home/login/index'));}
		session("user_id",$user['id']);
		$this->user=$user;
		$this->assign("news",(new News)->where(['type'=>8,'status'=>1])->order('id desc')->find());
		$this->assign("cont",$request->controller());
        $this->assign("acti",$request->action());
		$this->assign('u',$user);
    }
	public function setConfig(){
			$config=Config::all();
			foreach ($config as $k=>$v){
				C($v['name'],$v['value']);
			}
			$list=(new Channel)->field('type,name')->where(['start'=>1])->select();
		    $this->assign('channel',$list);
	    }
}