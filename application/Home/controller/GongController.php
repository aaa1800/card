<?php
namespace app\Home\controller;
use think\Controller;
use think\Request;
use app\common\model\News;
use app\common\model\User;


class GongController extends Controller {
	public function index(){
		return view('gong/mobile/index');
	}
	public function getHeadNotice(){
		$rt=(new News)->where(['type'=>1])->order('id desc')->find();
		return $rt;
		
	}
	public function getHeadNotices(){
		$id=input('id');
		$all=input('all');
		if($all){
			$rt=(new News)->where(['type'=>8])->select();
		}else{
			if($id){
				$rt=(new News)->where(['id'=>$id])->find();
			}else{
		      $rt=(new News)->where(['type'=>8])->order('id desc')->limit(5)->select();
			}
		}
		return json($rt);
		
	}
	public function getHelpList(){
		$rt=(new News)->where(['type'=>['gt',50]])->order('id desc')->select();
		return json($rt);
	}
	public function getArticlePageData(){
		$data=input('post.pageNumber');
		$id=input('post.articleClassifyId');
		$data=$data?$data:1;
		$s=($data-1)*10;
		$rt=(new News)->field('id,title,url')->where(['type'=>$id])->order('id desc')->limit('{$s},10')->select();
		return json(['content'=>$rt]);
	}
	public function getFap(){
		$data=input('pageNumber');
		$page=input('pageSize');
		$s=($data-1)*$page;
		//$s=$s<1?1:$s;
		$rt=(new News)->field('id,title,url')->where(['class'=>1])->order('id desc')->limit($s,$page)->select();
		$co=(new News)->where(['class'=>1])->count();
		if($co<=$page){
			$totalPages=1;
		}else{
			if($co%$page==0){
				$totalPages=$co/$page;
			}else{
			    $totalPages=ceil($co/$page);
			}
		}
		return json(['content'=>$rt,'number'=>(int)$data-1,'totalPages'=>$totalPages]);
	}
    
	
}