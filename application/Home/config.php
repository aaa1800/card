<?php
return [
	//分页配置
    'paginate'               => [
        'type'      => 'page\Page',
        'var_page'  => 'page',
        'list_rows' => 15,
    ],
	'view_replace_str'  => [
        '__CSS__'    => '/static/home/css',
        '__JS__'     => '/static/home/js',
		'__CSSM__'    => '/static/home/css/mobile',
        '__JSM__'     => '/static/home/js/mobile',
		'__IMG__'     => '/static/home/images'
     ]
];