<?php
return [

  'bin'=>"【收卡网】您的验证码是{code}。如非本人操作，请忽略本短信",
  'reg'=>'【收卡网】您正在注册，验证码：{code}。如非本人操作，请忽略本短信（15分钟内有效）',
   'locktime'=>2,
   'locknum'=>5,
   'sm'=>["settlecash","settlebind"],
   'emtpl'=>'<div class="wrapper" style="margin: 20px auto 0; width: 500px; padding-top:16px; padding-bottom:10px;"><br style="clear:both; height:0"><div class="content" style="background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E9E9E9; margin: 2px 0 0; padding: 30px;"><p>您好: </p><p>感谢您注册 <a href="{url}">{title}</a></p><p style="border-top: 1px solid #DDDDDD;margin: 15px 0 25px;padding: 15px;">请点击以下链接激活并设置您的账号: <a href="{urla}" target="_blank">{urla}</a></p><p style="border-top: 1px solid #DDDDDD; padding-top:6px; margin-top:25px; color:#838383;"><p>请勿回复本邮件, 此邮箱未受监控, 您不会得到任何回复。</p><p>如果点击上面的链接无效，请尝试将链接复制到浏览器地址栏访问。</p></p></div></div>',
   'bmtpl'=>'<div class="wrapper" style="margin: 20px auto 0; width: 500px; padding-top:16px; padding-bottom:10px;"><br style="clear:both; height:0"><div class="content" style="background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E9E9E9; margin: 2px 0 0; padding: 30px;"><p>您好: </p><p style="border-top: 1px solid #DDDDDD;margin: 15px 0 25px;padding: 15px;">你的验证码为: {code}</p><p style="border-top: 1px solid #DDDDDD; padding-top:6px; margin-top:25px; color:#838383;"><p>请勿回复本邮件, 此邮箱未受监控, 您不会得到任何回复。</p><p>请不要将验证码告诉他人。</p></p></div></div>',
     'txtpl'=>'<div class="wrapper" style="margin: 20px auto 0; width: 500px; padding-top:16px; padding-bottom:10px;"><br style="clear:both; height:0"><div class="content" style="background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E9E9E9; margin: 2px 0 0; padding: 30px;"><p>管理员您好: </p><p style="border-top: 1px solid #DDDDDD;margin: 15px 0 25px;padding: 15px;">用户ID-{id}, 申请提现{$money}元，请尽快处理</p><p style="border-top: 1px solid #DDDDDD; padding-top:6px; margin-top:25px; color:#838383;"><p>请勿回复本邮件, 此邮箱未受监控, 您不会得到任何回复。</p><p>此邮件为提醒邮件。</p></p></div></div>',

  'card'=>array(
   10=>[9=>10,10=>'JW',11=>"JW",12=>6],//骏网一卡通
   
   11=>[9=>14,10=>'LT',11=>'UNICOM',12=>10],//联通充值卡
   12=>[9=>0,10=>'ZT',11=>0,12=>4],//巨人一卡通
   13=>[9=>13,10=>'SZX',11=>'MOBILE',12=>8],//神州行充值卡
   14=>[9=>0,10=>0,11=>0,12=>35],//畅游一卡通
   15=>[9=>15,10=>'DX',11=>'TELECOM',12=>9],//电信充值卡
   16=>[9=>0,10=>'SE',11=>0,12=>20],//32一卡通
   17=>[9=>65,10=>'PG',11=>0,12=>22],//苹果充值卡
   18=>[9=>0,10=>0,11=>0,12=>0],//4399一卡通
   19=>[9=>0,10=>0,11=>'JS',12=>11],//金山一卡通
   20=>[9=>62,10=>0,11=>'JW_ALL',12=>30],//骏网话费通
   21=>[9=>0,10=>0,11=>'JW_ALL',12=>30],//骏网全业务卡
   22=>[9=>0,10=>'JDEC',11=>0,12=>29],//京东E卡
   23=>[9=>23,10=>0,11=>'ZYK',12=>31],//商通自游卡
   24=>[9=>0,10=>'YCZY',11=>0,12=>0],//易充纵游卡
   25=>[9=>0,10=>'YCTHC',11=>0,12=>0],//易充天宏卡
   26=>[9=>0,10=>'YC32',11=>0,12=>0],//易充32卡
   27=>[9=>0,10=>'YCJBC',11=>0,12=>0],//易充玖佰卡
   28=>[9=>68,10=>0,11=>0,12=>28],//同程旅游卡
   29=>[9=>66,10=>'XCRWX',11=>0,12=>27],//携程卡
   31=>[9=>0,10=>'',11=>0,12=>41],//唯品会
   42=>[9=>42,10=>'WY',11=>'WY',12=>5],//网易一卡通
   44=>[9=>44,10=>'WM',11=>'WM',12=>2],//完美一卡通
   47=>[9=>47,10=>'JY',11=>'JY',12=>7],//久游一卡通
   56=>[9=>55,10=>'ZY',11=>'ZY',12=>25],//纵游一卡通
   57=>[9=>57,10=>'QQ',11=>'QB',12=>14],//腾讯一卡通
   58=>[9=>58,10=>0,11=>0,12=>0],//光宇一卡通
   60=>[9=>60,10=>'TH',11=>'TH',12=>26],//天宏一卡通
   61=>[9=>61,10=>'SFT',11=>'SFT',12=>12],//盛付通卡
   63=>[9=>63,10=>'JYC',11=>'HOIL',12=>23],//中石化加油卡
   32=>[9=>0,10=>0,11=>0,12=>35],//天宏畅付卡
   66=>[9=>0,10=>0,11=>0,12=>45],//沃尔玛卡
   67=>[9=>0,10=>0,11=>0,12=>52],//家乐福
   68=>[9=>0,10=>0,11=>0,12=>42],//苏宁
   69=>[9=>0,10=>0,11=>0,12=>0])//500寺库
   ];