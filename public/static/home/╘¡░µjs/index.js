$(function(){
    //隐藏加载动画
    $("iframe").css("z-index",1);

    //头部用户账号、帮助鼠标移入移出
    $(".navbar-customer .navbar-item").hover(function() {
        $(this).addClass("open");
    }, function() {
        $(this).removeClass("open");
    });

    //全部回收类型
    $("#allRecycleType").hover(function() {
        $(this).find(".all-type-sort").show();
    }, function() {
        $(this).find(".all-type-sort").hide();
    });

    // iframe结构的网站按F5刷新子页面的实现方式
    $("body").bind("keydown",function(event){
        if (event.keyCode == 116) {
            //console.log("我按了F5");
            event.preventDefault(); //阻止默认刷新
            $("#main_frame").attr("src", ctxPath.domain+"/member/main.do");
        }
    })
});