/*!
  * author：lxf
  * date: date
  * description: 
  */
$(function(){
    //动态计算官网动态部分导航的宽度
    calcCrumbsNavWidth();

    //根据文章类型ID获取“官网动态”数据
    changePage("/getArticlePageData.html", "faqArticleModule", "faqArticlePagination", "refreshArticleListData", "faqArticlePageNumber", 1);
    changePage("/getArticlePageData.html", "industryArticleModule", "industryArticlePagination", "refreshArticleListData", "industryArticlePageNumber", 1);
});

function calcCrumbsNavWidth(){
    var $li = $(".crumbs-nav-box li").not(".list-division");
    var li_len = $li.length;
    var all_len = 1320 - (li_len - 1);
    $li.css("width", parseInt(all_len/li_len));
}

function refreshArticleListData(data, moduleId, pageWrapperId){
    if(data != null && data != "" && data != undefined) {
        var d = data.content;
        if(d.length > 0){
            var html = '';
            for(var i = 0; i < d.length; i++){
                var oLink = '';
                if(d[i].link != null && d[i].link != "" && d[i].link != undefined){
                    oLink = '<a href="'+d[i].link+'">'+d[i].title+'</a>';
                }else{
                    
                    oLink = '<a href="/newsDetail/id/'+d[i].id+'.html">'+d[i].title+'</a>';
                }

                html+='<li><i class="tag '+( (i==0 || i==1 || i==2 )? 'red-tag':'' )+'">'+ (i+1) +'</i>'+oLink+'</li>';
            }
            $("#"+moduleId).find("ul.list").html(html);

            if(data.totalPages>1){
                $("#"+pageWrapperId).show();//显示分页
            }

            calcArticleListMinHeight();
        }else{
            // articlesDataEmpty();
        }
    } else {
        // articlesDataEmpty();
    }
}

function calcArticleListMinHeight(){
    var faqLiLen = $("#faqArticleModule").find(".list li").length;
    var industryLiLen = $("#industryArticleModule").find(".list li").length;
    var maxLen = Math.max(faqLiLen, industryLiLen);
    var liH = $("#faqArticleModule").find(".list li").height();

    $(".web-trends .module ul.list").css("min-height", maxLen*liH);
}