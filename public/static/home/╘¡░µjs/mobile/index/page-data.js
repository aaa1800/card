/**
 * 切换页码
 * @param url
 * @param moduleId
 * @param pageWrapperId：整个分页容器ID
 * @param refreshFun
 * @param pageNumberId：去第几页输入框ID
 */
function changePage(url, moduleId, pageWrapperId, refreshFun, pageNumberId, pageNumber)
{
    $("#"+moduleId).find(".pageNumberHid").val(pageNumber);//当前是第几页

    var headers = {};
    headers['X-CSRF-TOKEN'] = ctxPath.csrf;//这里需要传入sessionid的真实值
    $.ajax({
        url: url,
        data:$("#" + moduleId).find("form").serialize(),
        dataType: "json",
        async: true,
        type: "POST",
        headers: headers,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {
            $("#"+pageWrapperId).hide();
        },
        success: function(data) {
            $("#" + moduleId).find("ul.list").html("");
            var func = eval(refreshFun);
            func(data, moduleId, pageWrapperId);
            refreshPage(data, url, moduleId, pageWrapperId, pageNumberId, refreshFun);
        },
        complete: function() {},
        error: function(e) {
            // layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        }
    });
}

/**
 * 翻页后更新页码条
 * @param data
 * @param moduleId
 * @param pageWrapperId：整个分页容器ID
 * @param pageNumberId：去第几页输入框ID
 * @param refreshFun
 */
function refreshPage(data, url, moduleId, pageWrapperId, pageNumberId, refreshFun)
{
    $("#" + pageWrapperId).html("");
    var currentPage = parseInt(data.number) + 1;
    var totalPages = parseInt(data.totalPages);//总共多少页
    var totalElements = parseInt(data.totalElements);//总共多少条数据
    var lastPage;
    var nextPage;

    //上一页事件处理
    var pageHtml = "";
    if(data.number <= 0){
        pageHtml += '<span class="f-noClick"><a title="上一页" href="javascript:void(0);"><i class="f-tran f-tran-prev">&lt; 上一页</i></a></span>';
    }else{
        lastPage = data.number;
        pageHtml += '<span><a title="上一页" href="javascript:changePage(\'' + url + '\', \'' + moduleId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + lastPage + '\')"><i class="f-tran f-tran-prev">&lt; 上一页</i></a></span>';
    }

    //中间分页按钮
    if(totalPages == 0){//一页数据都没有
        pageHtml += '<span class="current02"><a href="javascript:void(0);" title="第1页">1</a></span>';
    }else if(totalPages <= 10){//小于10页
        for(var i = 1; i <= totalPages; i++){
            pageHtml += '<span' + ((i == currentPage) ?  ' class="current02"':'') + '><a href="' + ((i != currentPage) ? 'javascript:changePage(\'' + url + '\', \'' + moduleId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + i +  '\')' : 'javascript:void(0)') + '" title="第'+i+'页">' + i + '</a></span>';
        }
    }else{//大于10页
        var back = totalPages - currentPage;
        var front = totalPages - back - 1;
        var swi = 0;
        var ewi = 0;

        if(currentPage <= 6){
            swi = 1;
            ewi = 10;
        }else{
            if(back <= 4){
                swi = currentPage - (10 - (totalPages - currentPage)) + 1;
                ewi = currentPage + back;
            }else{
                swi = currentPage - 5;
                ewi = currentPage + 4;
            }
        }

        for(var i = swi; i <= ewi; i++){
            pageHtml += '<span' + ((i == currentPage) ?  ' class="current02"':'') + '><a href="' + ((i != currentPage) ? 'javascript:changePage(\'' + url + '\', \'' + moduleId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + i +  '\')' : 'javascript:void(0)') + '" title="第'+i+'页">' + i + '</a></span>';
        }
    }

    //下一页事件处理
    if(data.totalPages <= 1 || currentPage >= data.totalPages){
        pageHtml += '<span class="f-noClick"><a title="下一页" href="javascript:void(0);"><i class="f-tran f-tran-next">下一页 &gt;</i></a></span>';
    }else{
        nextPage = currentPage + 1;
        pageHtml += '<span><a title="下一页" href="javascript:changePage(\'' + url + '\', \'' + moduleId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + nextPage + '\')"><i class="f-tran f-tran-next">下一页 &gt;</i></a></span>';
    }

    pageHtml += '<span>共<em>'+ totalPages +'</em>页，去第</span><span><input type="text" value="1" id="'+pageNumberId+'">页</span>' +
        '<span class="f-mar-left"><a href="javascript:goPage(\'' + pageNumberId + '\',\'' + url + '\',\'' + moduleId + '\', \'' + pageWrapperId + '\', \'' + refreshFun + '\',\'' + totalPages + '\');" class="page-sure" title="确定">确定</a></span>';


    var pageHtmlWrap = '<div class="pageing-cont"><div class="g-pagination">'+ pageHtml +'</div></div>';

    $("#" + pageWrapperId).html(pageHtmlWrap);
}

/**
 * 页码跳转--去第几页确定按钮
 * @param pageNumberId：去第几页输入框ID
 * @param moduleId
 * @param pageWrapperId：整个分页容器ID
 * @param refreshFun
 * @param totalPage
 * @returns {Boolean}
 */
function goPage(pageNumberId, url, moduleId, pageWrapperId, refreshFun, totalPage)
{
    var pageNumber = $("#" + pageNumberId).val();
    pageNumber = pageNumber.replace(/ /g,"");

    if(pageNumber.length == 0)
    {
        parent.layer.msg("请输入页码！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(isNaN(pageNumber))
    {
        parent.layer.msg("页码请输入数字！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(parseInt(pageNumber) > parseInt(totalPage))
    {
        parent.layer.msg("输入页码数不能大于总页码数！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(parseInt(pageNumber) < 1)
    {
        parent.layer.msg("请输入大于1的页码数！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(pageNumber.length > 1)
    {
        var index0 = pageNumber.indexOf("0");
        while(index0 == 0)
        {
            pageNumber = pageNumber.substring(1, pageNumber.length);
            index0 = pageNumber.indexOf("0");
        }
    }
    changePage(url, moduleId, pageWrapperId, refreshFun, pageNumberId, pageNumber);
}
