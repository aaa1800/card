var BatchArrayError = [];
$(function(){
    //卡类选择
    $(".recycle-wrap").on("click", "#cardTypeMedia", function(){
        var currentItem = $(this).find(".card-type-item.active").index();
        showCardTypeMeun(currentItem);
    });

    //卡种选择
    $(".recycle-wrap").on("click", "#cardSortMedia", function(){
        //根据产品类型ID获取它下面的所有运营商信息
        getRecycleOperatorsByProductClassifyId($("#productClassifyId").val(), true);
    });

    //面值选择
    $(".recycle-wrap").on("click", "#cardFaceValueMedia", function(){
        getRecyclePricesByOperatorId($("#operatorId").val(), true);
    });

    //方式选择
    $(".recycle-wrap").on("click", "#cardSubmitWayMedia", function(){
        var type = $.trim($("#type").val());
        showCardSubmitWayMeun(type);
    });

    //查看卡密示例
    $(".recycle-wrap").on("click", "#viewCardSample", function(){
    	var sampleHtml = '';
    	var isOnlyCardPassResult = isOnlyCardPass();
    	if(isOnlyCardPassResult){
    		var operatorId = $.trim($("#operatorId").val());
    		if(operatorId=="24"){
    			sampleHtml = '<p class="text-left">此卡种<span class="color-red">无需卡号</span>，只需填写卡密，每张卡密占用一行用 “<span class="color-red">换行</span>”隔开，例如：</p>' +
                '<p class="text-left">16CD-05F2-DE0B-53C6</p><p class="text-left">16CD-05F2-DE0B-5W6T</p>';
    		}else{
    			sampleHtml = '<p class="text-left">此卡种<span class="color-red">无需卡号</span>，只需填写卡密，每张卡密占用一行用 “<span class="color-red">换行</span>”隔开，例如：</p>' +
                '<p class="text-left">0111001401658359783</p><p class="text-left">0111001708358350006</p>';
    		}
    		
    	}else{
    		sampleHtml = '<p class="text-left">卡号与卡密间请用<span class="color-red">空格</span>隔开，每张卡占用一行用 “<span class="color-red">换行</span>”隔开，例如：</p>' +
            '<p class="text-left">011100170206 100304016583597830</p><p class="text-left">011100170206 105754083583500026</p>';
    	}
        
        layer.open({
            content: sampleHtml
            ,style: 'width: 15rem;'
            ,btn: '我知道了'
        });
    });

    //折扣提示
    $(".recycle-wrap").on("click", ".discount-tip-ico", function(){
        var discountTipHtml = $("#cardDiscount").attr("placeholder");
        discountTipHtml = discountTipHtml.replace('是','值：');
        layer.open({
            content: discountTipHtml+'，数值高回收慢'
            ,btn: '我知道了'
        });
    });

    //输入框删除功能
    $(document).on('input propertychange', '.inpt', function (e) {
        if (e.type === "input" || e.orignalEvent.propertyName === "value") {
            if(this.value.length > 0){
                $(this).parents(".media").find(".delete-ico").show();
            }else{
                $(this).parents(".media").find(".delete-ico").hide();
            }
        }
    });
    $(document).on('click', '.media .delete-ico', function (e) {
        $(this).hide();
        var oInput = $(this).parents(".media").find(".inpt");
        oInput.val("");
        if(oInput.attr('id') === 'cardPass'){
            $("#cardPassHid").val('');
        }
    });

    //是否同意协议
    $(document).on('click', 'label.isAgree', function (e) {
        var status = $(this).find("em").attr("state");
        if(status === "off") {
            $(this).find("em").addClass("active");
            $(this).find("em").attr("state", "on");
            $("#isAgree1").val("1");
            $("#isAgree2").val("1");

            //启用按钮
            $("#sellSubmitBtn").removeClass("disabled");
        } else {
            $(this).find("em").removeClass("active");
            $(this).find("em").attr("state", "off");
            $("#isAgree1").val("0");
            $("#isAgree2").val("0");

            //禁用按钮
            $("#sellSubmitBtn").addClass("disabled");
        }
    });

    //关闭弹层
    $(document).on("click", ".actionsheet-close-btn", function(){
        $(this).parents(".layui-m-layer").remove();
    });

    //监听批量提交输入框
    $(document).on("keyup", '#batchCardlist', function (e){
        var key = e.keyCode;
       // tidyCardFun($(this).val(), key);
    });
    $(document).on("keyup", '#singleMode .inpt', function (e){
		var tyyy=$("#type").val();
		if(tyyy==1){return;}
        var productTypeId = $("#productClassifyId").val();
        var cardOpeId = $.trim($("#operatorId").val()); //卡种
        var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
        var thisVal = "";
        if(productTypeId == "1" || productTypeId == "3"){
            thisVal = $(this).val().replace(/\D/g, '');//过滤数字之外的其他字
        }else if(productTypeId == "2"){
        	var operatorId = $("#operatorId").val();            
            
            if(productCode == "SD" || productCode === 'SFT' || productCode === 'ZT' || productCode === 'QB' || productCode === 'WM' || productCode === 'WY' || productCode === 'JY' || productCode === 'SH' || productCode === 'ZY' || productCode === 'JW' || productCode === 'APPLE' || productCode === 'ZYK' || productCode === 'JS'){
            	thisVal = $(this).val().replace(/\W/g, '');//过滤数字和字母之外的其他字
            }else{
            	if(operatorId=="21") {//骏网全业务卡，卡密19位纯数字
                	thisVal = $(this).val().replace(/\D/g, '');//过滤数字之外的其他字
                }else{
                	thisVal = $(this).val();
                }           	
            }           
        }else if(productTypeId == "4"){// 商超卡
        	if(productCode === "JD_E"){
        		if($(this).attr('id') === 'cardPass'){
        			thisVal = $(this).val().replace(/\W/g, '');//过滤数字和字母之外的其他字,字母不区分大小写
                    var len = thisVal.length;
                    if(len > 4 && len < 8){
                    	thisVal = thisVal .substr(0, 4) + '-' + thisVal.substr(4);
                    } else if (len >= 8 && len < 12) {
                    	thisVal = thisVal.substr(0, 4) + '-' + thisVal.substr(4, 4) + '-' + thisVal.substr(8, 4);
                    } else if (len >= 12) {
                    	thisVal = thisVal.substr(0, 4) + '-' + thisVal.substr(4, 4) + '-' + thisVal.substr(8, 4) + '-' + thisVal.substr(12, 4);
                    }
        		}else{
        			thisVal = $(this).val();
        		}	
        	}else{
        		thisVal = $(this).val();
        	}     
        	
        	if(productCode === "JD_E"){ 
        		
        	}else{
        		thisVal = $(this).val();
        	}        	        	
        }else if(productTypeId == "5" || productTypeId == "6"){
        	thisVal = $(this).val();
        }
        
        $(this).val(thisVal);
        if($(this).attr('id') === 'cardPass'){
            $("#cardPassHid").val(thisVal);
            if(cardOpeId == "24"){ // 提交到后台不能带横杠
                $("#cardPassHid").val(thisVal.replace(/\W/g, ''));
            }
        }
    });
    
    //整理卡号
    $(document).on("click", "#cardlist-tidy-btn", function(){
    	if($(this).hasClass('no-allowed')){    		
    		layerInfoTip("该卡暂不支持自动整理，请手动整理！");
    	}else{
    		tidyCardFun($("#batchCardlist").val(), '');
    	}    	
    });

    //确认提交卖卡
    $(document).on("click", "#sellSubmitBtn", function(){
		var type=$("#type").val();
		if($(this).hasClass("disabled")){
			layerInfoTip("请勾选同意相关服务协议！");
			return ;
		}
		if(type==3){
			     var price=$("#price").val();
			   if(price=="" || price==null || price<0){
				 layerInfoTip("请选择面值");return;
			   }

			   $.post(tjurl,{cards:$("#dbatchCardlist").val(),price:price},function(p){
					 layerInfoTip(p.msg);
			 });
			}else{
			if($(this).hasClass('no-allowed')){
				layerInfoTip("抱歉，当前卡种回收通道维护中~");
				return false;
			}else{
					tidyCardFun($("#batchCardlist").val(), '');
					//验证表单
					verifyRecycleForm();
			}
		}
        
    });

    //下拉刷新
    pullToRefreshSellCardPage();   
});

function pullToRefreshSellCardPage(){
    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            callback: function () {
                window.location.reload();//页面刷新
                miniRefresh.endDownLoading(true);// 结束下拉刷新
            }
        },
        up: {
            isAuto: false,
            callback: function () {
                miniRefresh.endUpLoading();// 结束上拉加载
            }
        }
    });
}

/**
 * @description: 显示“请选择卡类”底部菜单
 */
function showCardTypeMeun(curItemIndex){
    var cardTypeItemHtml = '';
    $(".card-type-item").each(function(){
        cardTypeItemHtml += '<a class="list-item" href="'+ $(this).attr("curLink") +'">' + $(this).html() + '</a>';
    });

    var cardTypeListHtml = '<div class="actionsheet-menu">' +
        '<div class="actionsheet-header"><h4>请选择卡类</h4><span class="actionsheet-close-btn"><i class="icon-close"></i></span></div>' +
        '<div class="cardtype-listing" id="cardTypeMeun">'+ cardTypeItemHtml +'</div>' +
        '</div>';

    openActionSheet(cardTypeListHtml);

    //设置选中项
    $("#cardTypeMeun").find(".list-item").removeClass("active");
    $("#cardTypeMeun").find(".list-item").eq(curItemIndex).addClass("active");
}

/**
 * @description: 显示“请选择方式”底部菜单
 * @param type：提交方式
 */
function showCardSubmitWayMeun(type){
    var cardSubmitWayItemHtml = '<a class="list-item color3" href="javascript:void(0);" submitWay="2" onclick="cardSubmitWaySelect(this);">批量提交</a>' +
        '<a class="list-item color3" href="javascript:void(0);" submitWay="1" onclick="cardSubmitWaySelect(this);">单卡提交</a>'+
		'<a class="list-item color3" href="javascript:void(0);" submitWay="3" onclick="cardSubmitWaySelect(this);">智能提交</a>';

    var cardSubmitWayListHtml = '<div class="actionsheet-menu">' +
        '<div class="actionsheet-header"><h4>请选择方式</h4><span class="actionsheet-close-btn"><i class="icon-close"></i></span></div>' +
        '<div class="cardSubmitWay-listing" id="cardSubmitWayMeun">'+ cardSubmitWayItemHtml +'</div>' +
        '</div>';

    openActionSheet(cardSubmitWayListHtml);

    //设置选中项
    if(type != ""){
        $("#cardSubmitWayMeun .list-item").each(function(){
            if($(this).attr("submitWay") == type){
                $(this).addClass("active");
            }else{
                $(this).removeClass("active");
            }
        });
    }
}

/**
 * @description: 卡种菜单项点击事件
 */
function cardSortSelect(obj){
	var s_maintain = $(obj).attr("maintenanceState"); // 维护状态[0：正常   1：正在维护]	如果为空，那就默认是正常的;只有为1的时候，才是正在维护中
	if(s_maintain === '1'){
		$("#sellSubmitBtn").addClass('no-allowed');
	}else{
		$("#sellSubmitBtn").removeClass('no-allowed');
	}
	
    $("#operatorId").val($(obj).attr("sortId"));
    var selectedCardSortItem = '<div operatorId="'+ $(obj).attr("sortId") +'" productCode="'+ $(obj).attr("productCode") +'" opeMaintance="'+s_maintain+'">'+ $(obj).html() +'</div>';
    $("#cardSortMedia .card-sort-selected").html(selectedCardSortItem);
    $("#facevalue-selected").html("请选择面值");
    $("#price").val("0");
    $(obj).parents(".layui-m-layer").remove();

    getCardRulesByOperatorId($(obj).attr("sortId"));
    getRateRangeInfo($(obj).attr("sortId"), $("#price").val());
    getRecentConsumeDuration($("#operatorId").val(), $("#price").val());
    //onlyCardPassFn($("#type").val());
}

/**
 * @description: 面值菜单项点击事件
 */
function cardFaceValueSelect(obj){
	var face_maintain = $(obj).attr("parMainTain"); // 维护状态[0：正常   1：正在维护]	如果为空，那就默认是正常的;只有为1的时候，才是正在维护中
	if(face_maintain === '1'){
		layerInfoTip("抱歉，该面值正在维护中");
	}else{
		$("#price").val($(obj).attr("priceVal"));

	    var selectedFaceValueItem = '<p priceVal="'+ $(obj).attr("priceVal") +'">'+ $(obj).html() +'</p>';
	    $("#facevalue-selected").html(selectedFaceValueItem);

	    $(obj).parents(".layui-m-layer").remove();

	    if($(obj).attr("rateType")== "2"){
	        getRateRangeInfo($("#operatorId").val(), $("#price").val());
	    }

	    //获取消耗时长
	    getRecentConsumeDuration($("#operatorId").val(), $("#price").val());
	}
	
}

/**
 * @description: 方式菜单项点击事件
 */
function cardSubmitWaySelect(obj){
    var way = $(obj).attr("submitWay");
    $("#type").val(way);
    var selectedFaceValueItem = '<p submitWay="'+ way +'">'+ $(obj).html() +'</p>';
    $("#submitWay-selected").html(selectedFaceValueItem);
    $(obj).parents(".layui-m-layer").remove();
	$("#cardTypeMedia").show();
	$("#cardSortMedia").show();
	$("#batchCardlist").show();
	$("#abatchMode").hide();
	$("#cardNumber").val("");
	$("#cardPass").val("");
    if(way == "1"){
        $("#batchMode").hide();
        $("#singleMode").show();
        onlyCardPassFn(1);
    }else if(way == "2"){
        $("#singleMode").hide();
        $("#batchMode").show();
        onlyCardPassFn(2);
    }else{
		$("#cardTypeMedia").hide();
		$("#cardSortMedia").hide();
		$("#batchCardlist").hide();
		$("#abatchMode").show();
		$("#singleMode").hide();
        $("#batchMode").hide();
       $("#sellSubmitBtn").removeClass("no-allowed");
	}
}

/**
 * 表单验证
 * 1、是否实名认证：未实名认证不能提交卡密
 * 2、是否输入完成：卡类、卡种、面值、提交方式、折扣、卡号卡密
 * 3、如果是话费卡慢销类的产品，会有提示语弹窗，要用户再次确认下
 */
function verifyRecycleForm(){
    var isRealNameAuthen = $.trim($("#isRealNameAuthen").val()); //是否实名
    var cardType = $.trim($("#productClassifyId").val()); //卡类
    var cardSort = $.trim($("#operatorId").val()); //卡种
    var cardFaceValue = $.trim($("#price").val()); //面值
    var cardSubmitWay = $.trim($("#type").val()); //提交方式

    var cardDiscount = $.trim($("#cardDiscount").val()); //折扣

    if(isRealNameAuthen == "false"){
        noRealNameAuthen("未实名认证不能提交卡密，请先实名认证");
    }else{
        if(cardType == ""){
            layerInfoTip("请选择卡类");
            return false;
        }
        if(cardSort == ""){
            layerInfoTip("请选择商品");
            return false;
        }
        if(cardFaceValue == "" || cardFaceValue == null || cardFaceValue == undefined || cardFaceValue == "0"){
            layerInfoTip("请选择面值");
            return false;
        }
        if(cardSubmitWay == ""){
            layerInfoTip("请选择提交方式");
            return false;
        }

        if($("#cardDiscountMedia").css("display") == 'none'){
            checkCardNumberAndPass(cardSubmitWay, cardFaceValue);
        }else{
            if(cardDiscount == ""){
                layerInfoTip("请输入折扣");                
            }else{
            	var discountFrom = $.trim($("#discountHidFromId").val());
        		var discountEnd = $.trim($("#discountHidEndId").val());
        		if(parseFloat(cardDiscount) >= discountFrom && parseFloat(cardDiscount) <= discountEnd){
        			checkCardNumberAndPass(cardSubmitWay, cardFaceValue);
        		}else{
        			layerInfoTip('供货折扣输入不正确！');        			
        		}
            }         
        }

    }
}

function checkCardNumberAndPass(cardSubmitWay, cardFaceValue){
    var batchCardlist = $("#batchCardlist").val(); //批量提交的卡号卡密
    var singleCardNumber = $("#cardNumber").val(); //单卡卡号
    var singleCardPass = $("#cardPass").val(); //单卡卡密

    var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
    // var productCode = "TH";
    if(cardSubmitWay == "1"){//单卡提交
    	var isOnlyCardPassResult = isOnlyCardPass();
        if(isOnlyCardPassResult) {//只需要卡密
        	cardPassVerify();
        }else{
        	if($.trim(singleCardNumber) == ""){
                layerInfoTip("请输入要提交的卡号");
            }else if(!checkSingleCard(productCode, $.trim(singleCardNumber).length)){
                layerInfoTip("卡号格式不正确");
            }else{
            	if(productCode === 'ZJYDSK'){
            		if($.trim(singleCardNumber).substr(5,2) !== '11'){
            			layerInfoTip('卡号第六位、第七位必须是"1"');
        	        }else{
        	        	cardPassVerify();
        	        }
            	}else{
            		cardPassVerify();
            	}          
            }
        }
        
        function cardPassVerify(){
        	if($.trim(singleCardPass) == ""){
                layerInfoTip("请输入要提交的卡密");
            }else if(!checkSingleCardPass(productCode, $.trim(singleCardPass).length)){
                layerInfoTip("卡密格式不正确");
            }else if(productCode === 'MOBILE_SLOW' || productCode === 'UNICOM_SLOW' || productCode === 'TELECOM_SLOW'){
                layer.open({
                    content: '您选择的面值为'+ cardFaceValue +'（面值选错如造成损失后果自负），话费卡慢销面值要确认正确，否则会造成金额损失，确认提交？'
                    ,btn: ['确定', '取消']
                    ,yes: function(index){
                        ajaxSubmitCardForm();
                        layer.close(index);
                    }
                });
            }else{
                ajaxSubmitCardForm();
            }
        }
        
    }else if(cardSubmitWay == "2"){//批量提交
    	
    	if($.trim(batchCardlist) == ""){
    		var isOnlyCardPassResult = isOnlyCardPass();
            if(isOnlyCardPassResult) {//只需要卡密
            	layerInfoTip("请输入要提交的卡密");
            }else{
            	layerInfoTip("请输入要提交的卡号、卡密");
            }
    		            
        }else{
            //console.log("批量卡号："+ typeof batchCardlist + ";值："+ batchCardlist);
        	// 判断规则是否明确
        	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
            var cardPassLenHidVal = $.trim($("#cardPassRule").val());
            
            if(productCode === 'ZJYDSK'){
            	cardRuleExact(batchCardlist, function(name, pass, conduct){
            		if(parseInt(name.length) === parseInt(cardNumLenHidVal)){
                        // 卡号验证通过，验证卡密
                        if(parseInt(pass.length) === parseInt(cardPassLenHidVal)){                	
                        	if(name.substr(5,2) === '11'){

                	        }else{
                    	        BatchArrayError.push(conduct);
                	        }
                        }else{
                            BatchArrayError.push(conduct);
                        }
                    }else{
                        BatchArrayError.push(conduct);
                    }
            	});               
            }else{
            	if(cardNumLenHidVal != '-1' && cardPassLenHidVal != '-1'){
                	//用英文逗号代替空格传到后台,格式：卡号,卡密,卡号,卡密
                    batchCardlist = batchCardlist.replace(/[ \r\n]/g,",").replace(new RegExp(',+',"gm"),',');
                    var realVal = "";
                    if(batchCardlist.indexOf(",", batchCardlist.length - 1) > 0){
                        realVal = batchCardlist.substring(0,batchCardlist.length-1);
                    }else{
                        realVal = batchCardlist;
                    }
                    $("#batchCards").val(realVal);
                    console.log("批量卡号隐藏域的值："+ $("#batchCards").val());
                    clearRepeatCards($("#batchCards").val());
                }else{
                	// 规则不明确，根据用户输入的提交
                	noRuleFn(batchCardlist);
                }
            }
                      
        }       
        
    }
}

/**
 * 批量提交-特殊格式检查
 * @param cardList
 */
function cardRuleExact(cardList, callbackFn){
	var cardsData = $.trim(cardList.replace(/\n/g, ';'));
	var batchData = []; //存卡密数组
	batchData = cardsData.split(";"); // 筛选出每一行
    var noNullBatch = [];  
    for (var i = 0; i < batchData.length; i++) {
        if (batchData[i] != "") {
        	noNullBatch.push(batchData[i]);
        }
    }
    
    noNullBatch = noNullBatch.map(function (item) {
    	item = item.replace(/\s{3}/g,",").replace(/[\，]/g,',');   	
    	var item0 = '';
    	var item1 = '';
    	if(item.indexOf(",") != -1 ){
    		item0 = item.split(",")[0];
            item1 = item.split(",")[1];
    	}else{
    		item0 = '';
            item1 = item;
    	}
        return [item0, item1];
    }, this);
    
    var bathHide = [];
    for (var j = 0; j < noNullBatch.length; j++) {
    	callbackFn(noNullBatch[j][0], noNullBatch[j][1], j+1); 	
    	bathHide.push(noNullBatch[j][0]);
        bathHide.push(noNullBatch[j][1]);
    }
    
    if(BatchArrayError == ""){
        $("#batchCards").val(bathHide);
        clearRepeatCards($("#batchCards").val());
    }else{
        $("#batchCards").val("");
        cardBatchError(BatchArrayError);
    }
}

/**
* @description: 无明确规则的批量卡号卡密处理
*/
function noRuleFn(cardList){
	var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
	if(productCode === "JD_E"){ // 提交到后台不能带横杠
		var cardsData = $.trim(cardList.replace(/\n/g, ';').replace(/-/g, '')); //卡号卡密  \s：匹配任何空白字符，包括空格、制表符、换页符等等   \n：匹配一个换行符
    }else{
    	var cardsData = $.trim(cardList.replace(/\n/g, ';')); //卡号卡密  \s：匹配任何空白字符，包括空格、制表符、换页符等等   \n：匹配一个换行符
    }
	
    var batchData = []; //存卡密数组
    batchData = cardsData.split(";"); // 筛选出每一行
    var noNullBatch = [];  
    for (var i = 0; i < batchData.length; i++) {
        if (batchData[i] != "") {
        	noNullBatch.push(batchData[i]);
        }
    }
    // 计算输入了几张卡
    var _length = noNullBatch.length;
    $('#cardnum').html(_length);
    
    //var cardNumLenHidVal = $.trim($("#cardNumRule").val());
    var _count = 0;
    var isOnlyCardPassResult = isOnlyCardPass();
    if(isOnlyCardPassResult){
    	noNullBatch = noNullBatch.map(function (item) {
        	item = item.replace(/\s/g,",").replace(/[\，]/g,',');   	        	
        	var item1 = '';
        	if(item.indexOf(",") != -1 ){
        		if(item.split(",")[0].length > 0){
        			_count++;
        		}else{
        			item1 = item.split(",")[1];
        		}
        	}else{        		
                item1 = item;
        	}
            return [item1];
        }, this);
    	
    	var bathHide = [];
        for (var j = 0; j < noNullBatch.length; j++) {    	
        	if (noNullBatch[j] != "") { 
        		checkMaxLength("", noNullBatch[j][0], j+1);
                bathHide.push(noNullBatch[j]);
            }        	
        }
    }else{
    	noNullBatch = noNullBatch.map(function (item) {
        	item = item.replace(/\s/g,",").replace(/[\，]/g,',');   	
        	var item0 = '';
        	var item1 = '';
        	if(item.indexOf(",") != -1 ){
        		item0 = item.split(",")[0];
                item1 = item.split(",")[1];
        	}else{
        		item0 = '';
                item1 = item;
        	}
            return [item0, item1];
        }, this);
    	
    	var bathHide = [];
        for (var j = 0; j < noNullBatch.length; j++) {
        	checkMaxLength(noNullBatch[j][0], noNullBatch[j][1], j+1); 	
        	bathHide.push(noNullBatch[j][0]);
            bathHide.push(noNullBatch[j][1]);
        }
    }

    if(_count>0){
    	layerInfoTip('该卡不需要填写卡号，请检查后再提交');
    }else{
    	if(BatchArrayError == ""){
            $("#batchCards").val(bathHide);
            clearRepeatCards($("#batchCards").val());
        }else{
            $("#batchCards").val("");
            cardBatchError(BatchArrayError);
        }
    }
}

/**
* @description: 验证卡号、卡密的最大长度
* 卡号、卡密长度分别不能大于30
*/
function checkMaxLength(name, pass, conduct){
	var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
	if(productCode === "JD_E"){ // 提交到后台不能带横杠
		name = name.replace(/-/g, '');
		pass = pass.replace(/-/g, '');
    }
	
	var maxLen = 30;
	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
    var cardPassLenHidVal = $.trim($("#cardPassRule").val());
	if(parseInt(name.length) > maxLen){		
        BatchArrayError.push(conduct);
	}else{
		if(cardNumLenHidVal != '-1'){ // 0或具体数字
			if(parseInt(name.length) === parseInt(cardNumLenHidVal)){
                // 卡号验证通过，验证卡密
                if(parseInt(pass.length) > maxLen){                	
                    BatchArrayError.push(conduct);
                }else{
                    if(cardPassLenHidVal != '-1'){
                        if(parseInt(pass.length) === parseInt(cardPassLenHidVal)){
                            // 卡密验证通过
                        }else{
                            BatchArrayError.push(conduct);
                        }
                    }else{

                    }
                }
            }else{
                BatchArrayError.push(conduct);
            }
		}else{
			if(parseInt(name.length) === 0){
                BatchArrayError.push(conduct);
			}else{
				// 验证卡密
	            if(parseInt(pass.length) > maxLen){
	                BatchArrayError.push(conduct);
	            }else{
	                if(cardPassLenHidVal != '-1'){
	                    if(parseInt(pass.length) === parseInt(cardPassLenHidVal)){
	                        // 卡密验证通过
	                    }else{
	                        BatchArrayError.push(conduct);
	                    }
	                }else{
	                	
	                }
	            }
			}
		}
		
	}
}

/**
* @description: 批量提交显示报错
* @param lineNumArr: 错误行号数组
*/
function cardBatchError(lineNumArr){
	var errorTxt = '';
	var isOnlyCardPassResult = isOnlyCardPass();
	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
	var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
	
	if(productCode==='ZJYDSK'){
		errorTxt = '卡号17位，卡密18位 并且 卡号第六位、第七位必须是"1"。<br />';
	}else{
		if(cardNumLenHidVal != '0'){
			errorTxt = '卡号或卡密最多30个字符，卡号不能为空。<br />';
		}else{
			errorTxt = '卡密最多30个字符。<br />';
		}
	}
	
    if(isOnlyCardPassResult){
        for (var i =0; i< lineNumArr.length; i++) {
        	errorTxt += '行'+lineNumArr[i]+'：卡密不正确;<br />';
        }
	}else{
        for (var i =0; i< lineNumArr.length; i++) {
        	errorTxt += '行'+lineNumArr[i]+'：卡号或卡密不正确;<br />';
        }
    }
    layerInfoTip(errorTxt, 5);
    BatchArrayError.splice(0,BatchArrayError.length); 
}

/**
 * 批量提交去掉重复的卡号
 */
function clearRepeatCards(cards){
    var cardsArr = cards.split(',');
    var len = cardsArr.length;   
    var cardOperator = $.trim($("#operatorId").val()); //卡种
    
    var isOnlyCardPassResult = isOnlyCardPass();
    if(isOnlyCardPassResult){//去掉重复的卡密
    	$("#cardnum").html(len);
    	
        var new_arr=[];
        for(var i=0; i<len; i++){
            var items = cardsArr[i];
            if($.inArray(items,new_arr)==-1) {
                new_arr.push(items);
            }
        }
        console.log("只有卡密的批量提交，去重后的新数组："+new_arr);
        var new_arrLen = new_arr.length;
		$("#batchCards").val(new_arr);
		if(cardOperator == '24'){
            $("#batchCards").val($("#batchCards").val().replace(/\-/g, ''));
		}
    }else{//去掉重复的卡号+卡密
    	//计算输入的总张数  
        if(len%2==0){
            var _length = len / 2;
            $("#cardnum").html(_length);
        }else{
            var _length = (len -1)/2;
            $("#cardnum").html(_length);
        }
        
        var newArry = [];
        for(var i=0; i<len; i++){
            if(i%2==0){
            	if(cardsArr[i+1]!="" && cardsArr[i+1]!=null && cardsArr[i+1]!=undefined){
                    var item = cardsArr[i]+'&'+cardsArr[i+1];	//卡号&卡密
                    newArry.push(item);
                }            	             
            }
        }
        var newArryLen = newArry.length;
        var new_arr=[];
        for(var j=0;j<newArryLen;j++) {
            var items = newArry[j];
            if($.inArray(items,new_arr)==-1) {
                new_arr.push(items);
            }
        }

        var new_arrLen = new_arr.length;
        var m_arr = [];
        for(var p=0;p<new_arrLen;p++){
            var p_arr = new_arr[p].split('&');
            m_arr.push(p_arr[0], p_arr[1]);

        }
		console.log("去重后的新数组："+m_arr);
        $("#batchCards").val(m_arr);
    }

    var sCardType = $.trim($("#productClassifyId").val());//卡类
    var cardNum = $('#cardnum').html();
    $("#actualCardNum").val(new_arrLen);
    if(sCardType=="1") {//话费卡提交确认
        var con = '<p>您提交了'+cardNum+'张卡密，去掉重复的卡密'+ (parseInt(cardNum)-parseInt(new_arrLen)) +'张，实际准备提交 <span class="color-red">'+new_arrLen+'张</span></p>' +
            '<p>注意：切记面值不符，<span class="color-red" style="font-size: 20px;">余额不退！</span></p>';
        sureSubmitCardsPrompt(con);
    }else{
        var con = '<p>您提交了'+cardNum+'张卡密，去掉重复的卡密'+ (parseInt(cardNum)-parseInt(new_arrLen)) +'张，实际准备提交 <span class="color-red">'+new_arrLen+'张</span></p>';
        sureSubmitCardsPrompt(con);
	}
}

function sureSubmitCardsPrompt(con){
	var productName = $(".card-sort-selected span").text();
	var price = $("#price").val();
	var sureSubmitCon = '<div class="card-submit-tipcon">尊敬的用户：' +
		'<p>您将要提交卡密为：<span class="color-red">'+productName+'</span></p>' +
		'<p>面值：<span class="color-red">'+price+'元</span></p>'+con+
		'</div>';
	layer.open({
		content: sureSubmitCon,
		className: 'sure-submit-layer',
		btn: ['确定', '取消']
		,yes: function(indexo, layero){
			//按钮【确认提交】的回调
			ajaxSubmitCardForm();
			layer.close(indexo);
		}
	});
}
/**
 * @description: 卡类型相对应的值 电信卡快销：TELECOM，电信卡慢销：TELECOM_SLOW , 移动卡快销：MOBILE，移动卡慢销：MOBILE_SLOW，电信卡快销：TELECOM ， 电信卡慢销：TELECOM_SLOW
 * 盛大：SD，Q币：QB，完美：WM，天宏：TH，网易：WY，征途：ZT，久游：JY，搜狐：SH，纵游： ZY，石油：HOIL，石油快销：HOIL_SLOW，中石油：ZSY，骏网：JW，盛付通：SFT
 */
function tidyCardFun(textareaVal, keyCode){
    var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
  $.post("/mach.html",{card:textareaVal},function(e){
       $("#cardnumm").text(e.num);
       $("#batchCards").val(e.res);
       $("#batchCardlist").val(e.msg);
  });
   
}

/**
 * 处理批量处理的卡号卡密:话费卡、加油卡只能输入数字；游戏卡可以输入字母和数字
 * @param productCode
 * @param textareaVal
 * @param cardNumberLen
 * @param cardPassLen
 */
function cardNeaten(productCode, textareaVal, cardNumberLen, cardPassLen){
    var productTypeId = $("#productClassifyId").val();
   console.log(productTypeId);

    var thisVal = "";
    if(productTypeId == "0" || productTypeId == "3"){
        thisVal = textareaVal.replace(/\D/g, '');
    }else if(productTypeId == "1"){
        thisVal = textareaVal.replace(/\W/g, '');//过滤数字和字母之外的其他字
    }else if(productTypeId == "5" || productTypeId == "6"){
    	thisVal = textareaVal;
    }

    var batchData = [];
    var totalLen = cardNumberLen + cardPassLen;
    for (var i = 0; i < thisVal.length; i += totalLen) {//24 = 卡号长度+卡密长度
        batchData.push(thisVal.substr(i, totalLen));
    }
    batchData = batchData.map(function (item) {
        var item0 = item.substr(0, cardNumberLen).length === cardNumberLen ? item.substr(0, cardNumberLen) + '   ': item.substr(0, cardNumberLen);
        var item1 = item.substr(cardNumberLen, cardPassLen).length === cardPassLen ? item.substr(cardNumberLen, cardPassLen) + '\n' : item.substr(cardNumberLen, cardPassLen);
        return [item0, item1];
    }, this);
    batchData = batchData.slice(0, 1000);//slice() 方法可从已有的数组中返回选定的元素;  join() 方法用于把数组中的所有元素放入一个字符串，元素是通过指定的分隔符进行分隔的，如果省略，则使用逗号作为分隔符。

    var _val = batchData.join('').replace(/,/g, '');
    $("#batchCardlist").val(_val);
    try {
        //计算输入了几张卡
        var _length = batchData[batchData.length - 1][1].length === (cardPassLen + 1) ? batchData.length : batchData.length - 1;
        $('#cardnum').html(_length);
    } catch (err) {}
}

function cardThNeaten(productCode, textareaVal){
    console.log("天宏支持两种规则，不知道怎么处理");
    var thisVal = textareaVal.replace(/[\u4E00-\u9FA5]/g, '');//过滤中文
    $("#batchCardlist").val(thisVal);
    if(thisVal.length > 20){
        //$('#cardnum').html(thisVal.length % 20);
    }else{
        $('#cardnum').html("0");
    }
}

function onlyCardPassComNeaten(textareaVal, cardPassLen, isLimt){
	var batchData = [];
    for (var i = 0; i < textareaVal.length; i += cardPassLen) {
        batchData.push(textareaVal.substr(i, cardPassLen));
    }
    batchData = batchData.map(function (item) {
        var newItem = item.substr(0, cardPassLen).length === cardPassLen ? item.substr(0, cardPassLen) + '\n': item.substr(0, cardPassLen);
        return newItem;
    }, this);
    batchData = batchData.slice(0, 1000);
    var _val = '';
    if(isLimt){ // 没有限制只能输入数字和字母
    	_val = batchData.join('');
    }else{
    	_val = batchData.join('').replace(/,/g, '');
    }
    
    $("#batchCardlist").val(_val);
    try {
        //计算输入了几张卡
        var _length = batchData[batchData.length - 1].length === (cardPassLen + 1) ? batchData.length : batchData.length - 1;
        $('#cardnum').html(_length);
    } catch (err) {}
}


/**
 * 批量提交卡号卡密处理--无卡号，只有卡密的情况
 * @param textareaVal
 * @param cardPassLen
 */
function onlyCardPassNeaten(textareaVal, cardPassLen){
	var thisVal = textareaVal.replace(/\D/g, '');
	onlyCardPassComNeaten(thisVal, cardPassLen, false);    
}


/**
 * 批量提交卡号卡密处理--无卡号，只有卡密的情况，每4位用横杠分割
 * @param productCode
 * @param textareaVal
 * @param cardNumberLen
 * @param cardPassLen
 */
function onlyCardPassNeatenByBar(productCode, textareaVal, cardNumberLen, cardPassLen){
	var thisVal = textareaVal.replace(/\W/g, '');
    var batchData = [];
    for (var i = 0; i < thisVal.length; i += cardPassLen) {
        batchData.push(thisVal.substr(i, cardPassLen));
    }
    console.log('京东E卡：'+batchData);
    batchData = batchData.map(function (item) {
        var itemLen = item.length;
        if(itemLen > 4 && itemLen < 9){
            item = item .substr(0, 4) + '-' + item.substr(4);
        } else if (itemLen >= 9 && itemLen < 14) {
            item = item.substr(0, 4) + '-' + item.substr(4, 4) + '-' + item.substr(8, 4);
        } else if (itemLen >= 14) {
            item = item.substr(0, 4) + '-' + item.substr(4, 4) + '-' + item.substr(8, 4) + '-' + item.substr(12, 4);
        }
        var newItem = item.substr(0, cardPassLen+3).length === cardPassLen+3 ? item.substr(0, cardPassLen+3) + '\n': item.substr(0, cardPassLen+3);
        return newItem;
    }, this);
    console.log('京东E卡111：'+batchData);
    batchData = batchData.slice(0, 1000);
    var _val = batchData.join('').replace(/,/g, '');
    console.log('京东E卡222：'+_val);
    $("#batchCardlist").val(_val);
    try {
        //计算输入了几张卡
        var _length = batchData[batchData.length - 1].length === (cardPassLen+3 + 1) ? batchData.length : batchData.length - 1;
        $('#cardnum').html(_length);
    } catch (err) {}
}


function isNoRuleFn(){
	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
    var cardPassLenHidVal = $.trim($("#cardPassRule").val());
    if(cardNumLenHidVal != '-1' && cardPassLenHidVal != '-1'){
    	// 两种情况(0， 具体数字； 具体数字， 具体数字)
    	return false;
    }else{
    	// 四种无法整理卡号的情况（-1， -1； -1， 具体数字； 0， -1； 具体数字， -1）    	
    	return true;
    }
}


function noLimitRule(productCode, batchCardlist){
	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
    var cardPassLenHidVal = $.trim($("#cardPassRule").val());
	var noruleResult = isNoRuleFn();
	if(!noruleResult){
		var filteredData = batchCardlist.replace(/\s/g, '').replace(/\n/g, '').replace(/\r/g, '');
    	if(parseInt(cardNumLenHidVal) == 0){
    		// 不需要卡号
    		onlyCardPassComNeaten(filteredData, parseInt(cardPassLenHidVal), true);
    	}else{
    		cardNeaten(productCode, filteredData, parseInt(cardNumLenHidVal), parseInt(cardPassLenHidVal));
    	}
	}else{
		
	}	      
}


/**
 * 单卡提交验证卡号
 * @param productCode:产品编码
 * @param inputLen：卡号输入框内容的长度
 * @returns {boolean}
 */
function checkSingleCard(productCode, inputLen){
    if(productCode === 'TH') {
        if(inputLen == 12 || inputLen == 10){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'QB'){
        if(inputLen == 9){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'WM'){
        if(inputLen == 10){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'WY' || productCode === 'JY' || productCode === 'JS'){
        if(inputLen == 13){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'UNICOM' || productCode === 'UNICOM_SLOW' || productCode === 'UNICOM_DISCOUNT' || productCode === 'ZY'){
        if(inputLen == 15){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "SD" || productCode === 'ZT' || productCode === 'HOIL' || productCode === 'HOIL_SLOW' || productCode === 'JW' || productCode === 'SFT' || productCode === 'APPLE'){
        if(inputLen == 16){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'MOBILE' || productCode === 'MOBILE_SLOW' || productCode === 'MOBILE_DISCOUNT'){
        if(inputLen == 17){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'TELECOM' || productCode === 'TELECOM_SLOW' || productCode === 'TELECOM_DISCOUNT' || productCode === 'ZYK'){
        if(inputLen == 19){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'SH'){
        if(inputLen == 20){
            return true;
        }else{
            return false;
        }
    }else{
    	// 判断规则是否明确
    	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
        var cardPassLenHidVal = $.trim($("#cardPassRule").val());       
        if(cardNumLenHidVal != '-1' && cardPassLenHidVal != '-1'){
        	if(parseInt(inputLen) === parseInt(cardNumLenHidVal)){
        		return true;
    	    }else{
    	    	return false;
    	    }
        }else{
        	// 规则不明确，只限制卡号长度不能超过30
        	var maxLen = 30;
        	if(parseInt(inputLen) > maxLen){		
        		return false;
        	}else{
        		return true;
        	}
        }         
    }
}


/**
 * 单卡提交验证卡密
 * @param productCode:产品编码
 * @param passInptLen：卡密输入框内容的长度
 * @returns {boolean}
 */
function checkSingleCardPass(productCode, passInptLen){
    var cardNumber = $.trim($("#cardNumber").val());
    if(productCode === 'TH') {
        if(cardNumber.length == 10){
            if(passInptLen == 10){
                return true;
            }else{
                return false;
            }
        }else if(cardNumber.length == 12){
            if(passInptLen == 15){
                return true;
            }else{
                return false;
            }
        }

    }else if(productCode === 'ZYK'){
    	if(passInptLen == 6){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "SD" || productCode === 'ZT' || productCode === 'SFT'){
        if(passInptLen == 8){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "WY" || productCode == "JS"){
        if(passInptLen == 9){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "JY"){
        if(passInptLen == 10){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "QB" || productCode === 'SH'){
        if(passInptLen == 12){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "WM" || productCode === 'ZY'){
        if(passInptLen == 15){
            return true;
        }else{
            return false;
        }
    }else if(productCode == "JW" || productCode === 'APPLE'){
        if(passInptLen == 16){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'MOBILE' || productCode === 'MOBILE_SLOW' || productCode === 'MOBILE_DISCOUNT' || productCode === 'TELECOM' || productCode === 'TELECOM_SLOW' || productCode === 'TELECOM_DISCOUNT'){
        if(passInptLen == 18){
            return true;
        }else{
            return false;
        }
    }else if(productCode == 'UNICOM' || productCode === 'UNICOM_SLOW' || productCode === 'UNICOM_DISCOUNT' || productCode === 'JW_ALL'){
        if(passInptLen == 19){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'HOIL' || productCode === 'HOIL_SLOW'){
        if(passInptLen == 20){
            return true;
        }else{
            return false;
        }
    }else if(productCode === 'JD_E'){
    	if(passInptLen == 19){
            return true;
        }else{
            return false;
        }
    }else{
    	// 判断规则是否明确
    	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
        var cardPassLenHidVal = $.trim($("#cardPassRule").val());       
        if(cardNumLenHidVal != '-1' && cardPassLenHidVal != '-1'){
        	if(parseInt(passInptLen) === parseInt(cardPassLenHidVal)){
        		return true;
    	    }else{
    	    	return false;
    	    }
        }else{
        	// 规则不明确，只限制卡密长度不能超过30
        	var maxLen = 30;
        	if(parseInt(passInptLen) > maxLen){		
        		return false;
        	}else{
        		return true;
        	}                  	
        }         
        
    	
    }
}

/**
 * 时间格式化
 * @param msel：毫秒数
 */
function meslDurationFormat(msel){
    var num = 1000 * 60;
    var days = parseInt(msel / (1000 * 60 * 60 * 24));
    var hours = parseInt((msel % (num * 60 * 24)) / (num * 60));
    var minutes = parseInt((msel % (num * 60)) / num);
    var seconds = (msel % num) / 1000;

    daysStr = (days > 0) ? days + " 天 " : "";
    hoursStr = (hours > 0) ? hours + " 小时 " : "";
    minutesStr = (minutes > 0) ? minutes + " 分钟 " : "";
    secondsStr = (seconds > 0) ? seconds + " 秒 " : "";

    return daysStr + hoursStr + minutesStr + secondsStr;
}

function noRealNameAuthen(msg){
    layer.open({
        content: '<p class="ico ico_warn color-red">'+msg+'</p>'
        ,btn: '立即去实名'
        ,yes: function(index){
        	window.location.href= ctxPath.domain+'/member/profileManage/realNameAuthen/realNameAuthenInit.do';
            layer.close(index);
        }
    });
}

/**
 * 判断是否是只需要卡密
 */
function isOnlyCardPass(){
	var operatorId = $.trim($("#operatorId").val());
	var cardNumLenHidVal = $.trim($("#cardNumRule").val());
	if(operatorId=="21" || parseInt(cardNumLenHidVal) === 0){
		return true;
	}else{
		return false;
	}
}

/**
 * 只需卡密相关功能切换
 * @param submitWay:提交方式
 */
function onlyCardPassFn(submitWay){    
    if(submitWay=="1"){//单卡提交
    	var isOnlyCardPassResult = isOnlyCardPass();
        if(isOnlyCardPassResult) {//只需要卡密
            $("#singleMode .media").eq(0).hide();      
            $("#cardNumber").val("");
        }else{
            $("#singleMode .media").eq(0).show();           
        }
    }else{//批量提交
    	var isOnlyCardPassResult = isOnlyCardPass();
        if(isOnlyCardPassResult) {//只需要卡密
            $("#batchCardlist").attr("placeholder", '此卡种无需卡号，只需填写卡密，每张一行用"回车键"隔开！');
        }else{
        	$("#batchCardlist").attr("placeholder", '请粘贴充值卡卡号卡密，卡号与卡密间请用空格隔开');
        }
    }
}

/**
 * 设置产品规则提示信息
 */
function setProductRuleTip(operatorId){
	var productCode = $("#cardSortMedia .card-sort-selected").children("div").attr("productCode");
	var pRuleTipArr = [{
    	code: 'YMS',
    	tip: '如果没有卡号，在卡号那里也填卡密；面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'WLT',
    	tip: '卡号和卡密必填！上传之前请确认卡是否已经开通激活，未激活的卡请联系发卡方激活。'
    },{
    	code: 'YHD',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'YQB',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'GMMTK',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'LHMSP',
    	tip: '请务必确认领货码的使用范围为E型（食品+生鲜+酒水）G型（美妆个护+家居+母婴+食品+生鲜），实体卡背面面值后面有字母为E/G的可以上传,品类提交错误，系统无法退还损失自负！'
    },{
    	code: 'JD_GB',
    	tip: '面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'LHMSJ',
    	tip: '请务必确认领货码的使用范围为手机或数码3C，品类提交错误，系统无法退还损失自负！实体卡背面面值后面有字母为A/B/C的可以上传'
    },{
    	code: 'DDLP',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'FFT',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'BLT',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'WRM',
    	tip: '只收2326开头的沃尔玛电子券和12和20位数字的兑换码，2326开头的沃尔玛必须有6位数的在线支付密码才能回收'
    },{
    	code: 'WST',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'ZHXST',
    	tip: '只收全国通用商通卡(7320***和 7360***号段)，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'HXT',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'HGT',
    	tip: '【卡密规则】卡号19位+密码6位，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'NSDK',
    	tip: '卡号和卡密必填！卡密必填，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'JD_E',
    	tip: '仅回收可购买大部分京东自营商品的京东E卡，如有京东其他类型礼品卡，造成损失后果自负，处理时间：9:00-24:00'
    },{
    	code: 'YDSHK',
    	tip: '无法全额回收！以实际消耗金额结算（无论多少面额的卡都会剩余45元）'
    },{
    	code: 'JJYKT',
    	tip: '卡号卡密必填'
    },{
    	code: 'QCS',
    	tip: ''
    },{
    	code: 'YHCS',
    	tip: ''
    },{
    	code: 'BGY',
    	tip: ''
    },{
    	code: 'TMCSXTK',
    	tip: '卡号和卡密必填！不能有误（http://jf.10086.cn 移动积分可兑换天猫超市卡）'
    },{
    	code: 'MTMDK',
    	tip: '卡号16位,卡密 6位'
    },{
    	code: 'WMKQPL',
    	tip: ''
    },{
    	code: 'SNYGLPK',
    	tip: '注意：苏宁电器卡请选择正确类型，苏宁超市卡无法回收请勿提交，提交错类型损失自负！提交前请确认卡种！卡号和卡密必填！并确保卡密及面值正确。'
    },{
    	code: 'XCRWX',
    	tip: '请确认卡密输入无误，处理时间9:00-22:00'
    },{
    	code: 'XCRWY',
    	tip: ''
    },{
    	code: 'TNSLK',
    	tip: '请确认卡密输入无误，处理时间9：00-22：00 【卡号 9位 卡密 6位】'
    },{
    	code: 'WYYXK',
    	tip: '请确认卡密输入无误，处理时间9:00-22:00 卡号16位，卡密16位。'
    },{
    	code: 'WPHLPK',
    	tip: '【卡密规则】卡号18位,卡密16位'
    },{
    	code: 'LMMLPK',
    	tip: ''
    },{
    	code: 'QNELPK',
    	tip: '卡号卡密必填'
    },{
    	code: 'HMXS',
    	tip: '注：卡密是12位的，正常处理时间1-2个工作日，请勿催单。请勿大量提交无效卡，谢谢。'
    },{
    	code: 'JLFCSDJQ',
    	tip: '只收未绑定的2336开头的（卡号19位、卡密6位）的家乐福礼品卡，面值一定要选择正确，如造成损失后果自负！'
    },{
    	code: 'MGTV',
    	tip: '该卡密需人工处理，预计3-5个工作日回款，请保证卡密剩余7日有效期。请勿催单。卡密长度17位，请勿输错（严禁提交已被使用的激活码）。'
    }];
    console.log('当前产品编码：'+productCode+';运营商ID：'+operatorId);
    for(var i=0; i<pRuleTipArr.length; i++){
    	if(productCode===pRuleTipArr[i].code){
    		$("#recycleRulesWrap").html(pRuleTipArr[i].tip);  		
    	}
    }
}

