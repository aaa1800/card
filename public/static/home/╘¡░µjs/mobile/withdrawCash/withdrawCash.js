$(function() {
    //提现方式选择
    $(".withdraw-cash-wrap").on("click", "#drawWayMedia", function () {
        getWithdrawWay(true);
    });

    //提现账号选择
    $(".withdraw-cash-wrap").on("click", "#drawAccountMedia", function () {
        showDrawAccountMenu($(this));
    });

    //全部提现
    $(".withdraw-cash-wrap").on("click", "#withdrawAllMoney", function () {
        var useableMoney = $.trim($("#useableMoney").val());
        if( parseFloat(useableMoney) > 0 ) {
            $("#curWithdrawMoney").val(useableMoney);
        } else {
            layerInfoTip("您当前无可提现余额，无法申请提现");
        }
    });

    //表单验证
    $(".withdraw-cash-wrap").on("click", "#submitWithdrawFormBtn", function(){
        verifyWithdrawCashForm();
    });

    //交易密码输入框
    $(document).on('input propertychange', '#tradePassword', function (e) {
        if (e.type === "input" || e.orignalEvent.propertyName === "value") {
            if(this.value.length > 0){
                $(this).parents(".form-group").find(".delete").show();                
            }else{
                $(this).parents(".form-group").find(".delete").hide();
            }
        }
    });
    $(document).on('click', '.form-group .delete', function (e) {
        $(this).hide();
        $(this).parents(".form-group").find("#tradePassword").val("");
    });
    $(document).on('click', '.form-group .isShowPass', function (e) {
        var $inpt = $(this).parents(".form-group").find(".form-control");
        if($(this).hasClass("showPassword")){
            $(this).removeClass("showPassword").addClass("hidePassword");
            $inpt.attr("type", "password");
        }else if($(this).hasClass("hidePassword")){
            $(this).removeClass("hidePassword").addClass("showPassword");
            $inpt.attr("type", "text");
        }
    });

    //关闭弹层
    $(document).on("click", ".actionsheet-close-btn", function(){
        $(this).parents(".layui-m-layer").remove();
    });
});


function drawWaySelect(obj){
    var drawWay = $(obj).attr("drawWay");
    var poundage = $(obj).attr("poundage");
    var limit = $(obj).attr("limit");
    $("#withDrawWay").val(drawWay);
    $("#drawWayMedia").html($(obj).html()).attr("poundage", poundage).attr("limit", limit);
    $(obj).parents(".layui-m-layer").remove();
    getWithdrawAccountsByWithdrawWay( drawWay );
    getWithdrawLimitTip(poundage, limit);
}

function drawAccountSelect(obj){
    var withDrawWay = $.trim($("#withDrawWay").val());
    var drawAccount = $(obj).attr("drawAccount");
    var drawAccountId = $(obj).attr("drawAccountId");
    if(drawAccount != ""){
        $("#drawAccountMedia .media").each(function(){
            if($(this).attr("drawAccount") == drawAccount){
                $(this).addClass("active");
                if(withDrawWay == "1"){
                	$("#bankCardSelect").val("");
    	            $("#alipayAccountSelect").val("");
    	            $("#wxAccountSelect").val(drawAccountId);
                }else if(withDrawWay == "2" || withDrawWay == "3"){
                	$("#bankCardSelect").val(drawAccountId);
    	            $("#alipayAccountSelect").val("");
    	            $("#wxAccountSelect").val("");
                }else if(withDrawWay == "4"){
                	$("#bankCardSelect").val("");
    	            $("#alipayAccountSelect").val(drawAccountId);
    	            $("#wxAccountSelect").val("");
                }
                
            }else{
                $(this).removeClass("active");
            }
        });
    }
    $(obj).parents(".layui-m-layer").remove();
}

/**
 * @description: 显示选择提现账号底部菜单
 */
function showDrawAccountMenu(obj){
    var withDrawWay = $.trim($("#withDrawWay").val());
    var $media = $(obj).find(".media");
    var drawAccountItemHtml = '';
    
    var otitle = '';
    if(withDrawWay == "1"){
    	otitle = '请选择微信提现账号';
    }else if(withDrawWay == "2" || withDrawWay == "3"){
    	otitle = '请选择银行卡提现账号';
    }else if(withDrawWay == "4"){
    	otitle = '请选择支付宝提现账号';
    }
    if($media.length > 0){
        for(var i = 0, mediaLen = $media.length; i < mediaLen; i++){
            drawAccountItemHtml += '<div class="media" drawAccount="'+ $media.eq(i).attr("drawAccount") +'" drawAccountId="'+ $media.eq(i).attr("drawAccountId") +'" onclick="drawAccountSelect(this);">' +
                '<img class="w74 align-self-center" src="'+ $media.eq(i).find("img").attr("src") +'">' +
                '<div class="media-body">' + $media.eq(i).find(".media-body").html() + '</div>' +
                '</div>';
        }

        var drawAccountListHtml = '<div class="actionsheet-menu">' +
            '<div class="actionsheet-header"><h4>'+ otitle +'</h4><span class="actionsheet-close-btn"><i class="icon-close"></i></span></div>' +
            '<div class="actionsheet-listing drawAccount-listing" id="drawAccountMeun">'+ drawAccountItemHtml +'</div>' +
            '</div>';

        openActionSheet(drawAccountListHtml);

        //设置选中项
        var selectedWithDrawAccount = $(obj).find(".media.active").attr("drawAccount");
        if(selectedWithDrawAccount != ""){
            $("#drawAccountMeun .media").each(function(){
                if($(this).attr("drawAccount") == selectedWithDrawAccount){
                    $(this).addClass("active");
                }else{
                    $(this).removeClass("active");
                }
            });
        }
    }

}

/**
 * 表单验证
 * 1、是否有余额
 * 2、提现账号验证：如果是微信必须同时绑定微信和关注公众号；如果是银行卡必须有银行卡才行;现在改成了扫码成功自动添加账号
 * 3、提现金额验证：最多允许输入小数点后二位；不可大于可提现余额；提现金额扣除手续费后要大于等于1元；不能大于等于提现的最大限额
 * 4、交易密码验证：是否输入
 */
function verifyWithdrawCashForm(){
    var useableMoney = $.trim($("#useableMoney").val());
    if( parseFloat(useableMoney) > 0 ) {
        var withDrawWay = $("#withDrawWay").val();
        if(withDrawWay == "1"){
            /*var isBindWx = $("#isBindWx").val();// 1：绑定，2：解绑,3：未绑定
            var isFollowWx = $("#isFollowWx").val();//1：已关注 2：未关注

            if(isBindWx !="1" && isFollowWx != "1"){//没有绑定微信也没有关注公众号
                layerInfoTip("只有绑定微信和关注公众号，才可以微信提现");
            }else if(isBindWx !="1" && isFollowWx == "1"){//没有绑定微信,关注了公众号
                layerInfoTip("您当前未绑定微信，绑定后才可以微信提现");
            }else if(isBindWx =="1" && isFollowWx != "1"){//绑定了微信，没有关注公众号
                layerInfoTip("您当前未关注公众号，关注后才可以微信提现");
            }else{
                verifyRestElement();
            }*/
        	
        	verifyRestElement();

        }else if(withDrawWay == "2" || withDrawWay == "3"){
            //判断是否有银行卡，待判断
            verifyRestElement();
        }else if(withDrawWay == "4"){
            //判断是否有支付宝帐号，待判断
            verifyRestElement();
        }else{
            layerInfoTip("提现方式未知，请联系网站管理员");
        }
    }else{
        layerInfoTip("您当前无可提现余额，无法申请提现");
    }
}

function verifyRestElement(){
    var useableMoney = parseFloat($.trim($("#useableMoney").val()));//可提现余额
    var curWithdrawMoney = $.trim($("#curWithdrawMoney").val());
    var moneyReg = /^[1-9]\d*(\.\d{1,2})?$|^0(\.\d{1,2})?$/;
    if(curWithdrawMoney == ""){
        layerInfoTip("请输入提现金额");
    }else{
        if(curWithdrawMoney > useableMoney){
            layerInfoTip("可提现余额不足");
        }else{
            
			if(moneyReg.test(curWithdrawMoney)){
                //可提现余额足够，提现金额扣除手续费后要大于等于1元且不能大于等于提现的最大限额
                var poundage = $("#drawWayMedia").attr("poundage");
                var maxLimitTxt = $("#drawWayMedia").attr("limit");
                var maxLimit = parseFloat(maxLimitTxt)*10000;
                //console.log("手续费："+poundage+";最大限额："+maxLimit+";类型："+ typeof maxLimit);
                if( parseFloat(curWithdrawMoney) - parseFloat(poundage) < 1 ){
                    layerInfoTip("提现金额扣除手续费后不能小于1元~");
                    return false;
                }
                if(parseFloat(curWithdrawMoney) >= maxLimit){
                    layerInfoTip("提现金额不能大于等于提现的最大限额");
                    return false;
                }

                var tradePassword = $.trim($("#tradePassword").val());
                if(tradePassword == ""){
                    layerInfoTip("请输入交易密码");
                }else{
                    //ajax提交表单
                	ajaxWithdrawCash();
                }
            }else{
                layerInfoTip("提现金额请输入数字，最多保留两位小数");
            }
        }
    }
}

//判断是否是微信浏览器
function isWeiXin()
{
    var ua = window.navigator.userAgent.toLowerCase();
    //通过正则表达式匹配ua中是否含有MicroMessenger字符串
    return (ua.match(/MicroMessenger/i) == 'micromessenger');
}



