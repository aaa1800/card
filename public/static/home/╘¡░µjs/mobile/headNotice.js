/**
 * 头部公告
 */
$(function(){
	getHeadNoticeData();
});

function getHeadNoticeData(){
	var headers = {};
    headers['X-CSRF-TOKEN'] = ctxPath.csrf ? ctxPath.csrf : ctxPath._csrf;
	$.ajax({
        url:"/getHeadNotice.html",
        data:{_csrf:$("input[name='_csrf']").val()},
        dataType: "json",
        async: true,
        type: "POST",
        headers: headers,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
        	if($.cookie('head_notice_cookie') == null || $.cookie('head_notice_cookie') == '' || $.cookie('head_notice_cookie') == undefined){ // 第一次
    			if(data.state==2 || data.state==4){
    				var importantNotice = '<div class="head-top top-prompt">'+
		    				    '<div class="w1320 clearfix">'+
		    		        '<div class="fl left-part">'+
		    		            '<span class="fl">公告：'+data.title+'</span>'+
		    		            '<a href="/getHeadNoticeDetail.html" class="fl txt-blue view-detail-btn">点击查看详情>></a>'+
		    		        '</div>'+
		    		        '<a href="javascript:void(0);" class="fr close-btn" title="关闭" onclick="closeHeadNotice(this);"></a>'+
		    		    '</div>'+
		    		'</div>';
    				$(".head-top").before(importantNotice);
    				var el = $(".fixed-nav #wrapper");
    				if(el.length > 0){
    					el.css("padding-top", "194px");
    				}
    			}
    		}
        },
        complete: function() {},
        error: function(e) {}
    });
}

function closeHeadNotice(obj){
	$(obj).parents('.top-prompt').remove();
	$.cookie("head_notice_cookie", '0', {path:'/'}); // 刷新不显示，关闭退出浏览器再进入才显示
	var el = $(".fixed-nav #wrapper");
	if(el.length > 0){
		el.css("padding-top", "160px");
	}
}
