$(function(){
	//加载数据
	memberCenterPageInit();
	
    //下拉刷新
    pullToRefreshMemberCenter();  

});

function memberCenterPageInit(){
	ajax_doSubmit("/smain.html","data:",function(data){    
    	if(data != null && data != "" && data != undefined){
    		if(data.m.qq == "" || data.m.qq == null || data.m.qq == undefined){
    			$("#memberInfo .user-photo").attr("src", "/static/home/images/heard.png");   			
    		}else{
    			$("#memberInfo .user-photo").attr("src", "http://q4.qlogo.cn/headimg_dl?dst_uin="+data.m.qq+"&spec=100");
    		}
    		
    		if(data.m.nickName == "" || data.m.nickName == null || data.m.nickName == undefined){
    			$("#memberInfo .user-nickname").html('<span class="no-nickname">设置用户昵称</span>');
    		}else{
    			$("#memberInfo .user-nickname").html(data.m.nickName);
    		}
    		
    		$("#memberInfo .user-number").html(data.m.number);
    		$("#memberInfo .user-level").html(data.m.generalizeGradeName);
    		$("#balance").html("￥"+data.m.money);
    		 
    		$("#isRealNameAuthen").val(data.m.isRealNameAuthen);
    		if(data.m.isRealNameAuthen){
    			$("#realNameAuth").addClass("btn-green");
    			$("#realNameAuth").html("已实名");    			
    		}else{
    			$("#realNameAuth").addClass("btn-red");
    			$("#realNameAuth").html("去实名认证");
    		}
    	
    		$("#isSetTradePass").val(data.m.isSetTradePass);
    		$("#customerPhone").html(data.base.csPhone);
    		$("#clientqq").html("renrenxk");
    		$("#copy_qq_btn").attr("data-clipboard-text", "renrenxk");
		}
    });
	
	//复制客服QQ
    copyClientQq();
}

function pullToRefreshMemberCenter(){
    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            successAnim: {
                // 下拉刷新结束后是否有成功动画，默认为false，如果想要有成功刷新xxx条数据这种操作，请设为true，并实现对应hook函数
                isEnable: true
            },
            callback: function () {
                window.location.reload();//页面刷新
                miniRefresh.endDownLoading(true, '恭喜您，刷新页面成功');// 结束下拉刷新
            }
        },
        up: {
            isAuto: false,
            callback: function () {
                miniRefresh.endUpLoading();// 结束上拉加载
            }
        }
    });
}

function copyClientQq(){
    var clipboard = new ClipboardJS('#copy_qq_btn');
    clipboard.on('success', function(e) {
        layer.open({
            content: '<p class="ico ico_right" style="text-align: left;">微信公众号已复制，可粘贴到微信公众号里搜索联系客服，请认准企业认证，谨防被骗！</p>'
            ,btn: '我知道了'
        });
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
        layer.open({
            content: '<p class="ico ico_warn">复制出错，请长按文本拷贝</p>'
            ,btn: '我知道了'
        });
    });
}

/**
 * 我要提现
 * 一、用户未实名认证：提示先实名认证
 * 二、帐号已实名认证，但未设置交易密码：提现先设置交易密码
 * 三、帐号已实名，并且已设置交易密码：直接进入提现页面
 */
function gotoWithdrawCash(){
    var isRealNameAuthen = $.trim($("#isRealNameAuthen").val()); //是否实名
    var isSetTradePass = $.trim($("#isSetTradePass").val()); //是否设置了交易密码

    if(isRealNameAuthen == "false"){
        layer.open({
            content: '<p class="ico ico_warn color-red">未实名认证不能提现，请先进行实名认证</p>'
            ,btn: '立即去实名'
            ,yes: function(index){
            	window.location.href = ctxPath.domain + "/member/profileManage/realNameAuthen/realNameAuthenInit.do";
                layer.close(index);
            }
        });
    }else{
        if(isSetTradePass == ""){
            layer.open({
                content: '<p class="ico ico_warn color-red">未设置交易密码不能提现，请先设置</p>'
                ,btn: '立即去设置'
                ,yes: function(index){                    
                    window.location.href = ctxPath.domain + "/member/profileManage/passwordManage/tradePassword/tradePasswordManageIndex.do";
                    layer.close(index);
                }
            });
        }else{
            window.location.href = tx;
        }
    }
}