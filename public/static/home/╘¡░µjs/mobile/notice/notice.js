$(function(){	
    //查看详情
    $("#notice-list-warp").on("click", ".card-footer .media", function(){
        var $parent = $(this).parents(".item");

        if( $parent.find(".card-body").hasClass("show") ){
            $parent.find(".card-body").removeClass("show");
            $parent.find(".card-footer .media").removeClass("show");
            $parent.find(".card-footer .veiw-detail").html("查看详情");
        }else{
            $parent.find(".card-body").addClass("show");
            $parent.find(".card-footer .media").addClass("show");
            $parent.find(".card-footer .veiw-detail").html("收起");

            $parent.siblings(".item").find(".card-body").removeClass("show");
            $parent.siblings(".item").find(".card-footer .media").removeClass("show");
            $parent.siblings(".item").find(".card-footer .veiw-detail").html("查看详情");
        }
    });

    //下拉刷新与上拉加载
    pullNoticeToRefresh();
});

function pullNoticeToRefresh(){
	// var page = 0; // 初始化页码

    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            successAnim: {
                // 下拉刷新结束后是否有成功动画，默认为false，如果想要有成功刷新xxx条数据这种操作，请设为true，并实现对应hook函数
                isEnable: true
            },
            onPull: function() {
                $("#minirefresh .minirefresh-downwrap").css("top", "2.2rem");
            },
            callback: function () {
                ajaxLoadNoticeData('down');
            }
        },
        up: {
            isAuto: true,
            loadFull: {
                isEnable: true // 开启配置后，只要没满屏幕，就会自动加载
            },
            contentdown: '上拉显示更多',
            contentrefresh: '加载中...',
            contentnomore: '没有更多数据了',
            callback: function () {
                ajaxLoadNoticeData('up');
            }
        }
    });

    var ajaxLoadNoticeData = function (downOrUp){
        /*if (downOrUp == 'down') {
            page = 1;// 下拉刷新页码设置
        } else {
            page++;// 上拉加载递增页码
        }*/
        //console.log("ajax加载数据，当前page："+page);
        
        ajax_doSubmit("/getHeadNotices.html","all:1",function(data){
        	//console.log(JSON.stringify(data));
        	if(data!=null && data!="" && data!=undefined){
                var html = '';
                for(var i=0;i<data.length;i++){
                    html += '<div class="item">'+
	                    '<p class="notice-time">'+data[i].addtime+'</p>'+
	                    '<div class="card border-0">'+
	                        '<div class="card-header"><h5>'+data[i].title+'</h5></div>'+
	                        '<div class="card-body">'+(data[i].contents == "" ? "暂无详情":data[i].contents)+'</div>'+
	                        '<div class="card-footer bg-white">'+
	                            '<a href="javascript:void(0);" class="media">'+
	                                '<div class="media-body veiw-detail">查看详情</div>'+
	                            '</a>'+
	                        '</div>'+
	                    '</div>'+
	                '</div>';
                }

                if (downOrUp == 'down') {
                    $('#notice-list-warp').html('');
                    $('#notice-list-warp').append(html);// DOM 插入数据
                    miniRefresh.endDownLoading(true, '恭喜您，刷新页面成功');// 结束下拉刷新
                    $("#minirefresh .minirefresh-downwrap").css("top", "0");
                } else {
                	$('#notice-list-warp').html('');
                    $('#notice-list-warp').append(html);
                    miniRefresh.endUpLoading(true);// 结束上拉加载
                    /*if (data.totalPages == page) {// 是否已取完数据页
                        miniRefresh.endUpLoading(true);// 结束上拉加载
                    } else {
                        miniRefresh.endUpLoading(false);
                    }*/
                }

            }else{
                //没有数据
                nullNoticeDataSet(downOrUp);
            }
        });
        
    };

    var nullNoticeDataSet = function (downOrUp){
        if (downOrUp == 'down') {
            $('#notice-list-warp').html("");
            miniRefresh.endDownLoading(true);
            $("#minirefresh .minirefresh-downwrap").css("top", "0");
        } else {
            miniRefresh.endUpLoading(true);
        }

        $(".minirefresh-scroll").css("height", "calc(100% - 3.2rem)");
        var nullHtml = '<div class="null_data">' +
            '<i class="null_notice_ico"></i>' +
            '<p>暂无相关公告哦~</p>' +
            '<a href="#" class="btn btn-blue">返回个人中心</a>' +
            '</div>';
        $("main.notice-wrap").html(nullHtml);

        $("#minirefresh .minirefresh-upwrap").hide();
    };
}

