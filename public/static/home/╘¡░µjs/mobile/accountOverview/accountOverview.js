$(function(){
    //绑定手机等方块移入移出效果
    $(".security-group .security-item").on("mouseenter",function(e){
        var This=$(this);
        This.addClass("animated bounceIn");
        This.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass("animated bounceIn");
        });
    });
});

//查看提现记录
function gotoWithdrawCashRecord() {
    addFrame( withdrawCashRecord, '13358', '提现记录');
}

//我要提现
function gotoWithdrawCash() {
    addFrame( withdrawCash, '12358', '我要提现');
}

