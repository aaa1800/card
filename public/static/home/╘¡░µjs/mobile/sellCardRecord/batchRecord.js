/**
 * 批量订单记录
 */
$(function(){
	getBatchRecordData();
});

function getBatchRecordData(){
	var itemIndex = 0;
    var tab1LoadEnd = false;
    var tab2LoadEnd = false;
    var tab3LoadEnd = false;
    
    //tab 切换
    var tabLi = $("#sellcard-record-tab .nav-item");
    	
    tabLi.click(function(){
    	 var $this = $(this);
         itemIndex = $this.index();
				
		$("#sellcard-record-tab").find('.nav-link.active').removeClass('active');
        $this.find(".nav-link").addClass('active');
        $('.data-list').eq(itemIndex).removeClass("d-none").siblings(".data-list").addClass("d-none");

        var noDataArea = $('.data-list').eq(itemIndex).find(".null_data").length;		
		if(noDataArea > 0){
			$(".dropload-down").css("display", "none");
		}else{
			$(".dropload-down").css("display", "block");
		}
		
		// 如果选中菜单一
        if(itemIndex == '0'){
            // 如果数据没有加载完
            if(!tab1LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        // 如果选中菜单二
        }else if(itemIndex == '1'){
            if(!tab2LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        else if(itemIndex == '2'){
            if(!tab3LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        
        // 重置
        dropload.resetload();
	});
    
    var pageNumber1 = 0;
    var pageNumber2 = 0;
    var pageNumber3 = 0;
	
    /**
     * @param collectionStatus:回收状态(空:为全部 1:处理中、2:已完成 )
     * @param type：1、单卡提交 2、批量提交
     * @param pageSize：每页数量，默认10
     * @param pageNumber
     */   
    var loadData = function(collectionStatus, pageNumber, $refresh, refreshType, tabIndex){
    	ajax_doSubmit(orurl,"data:collectionStatus="+collectionStatus+"&type=2&pageSize=10&pageNumber="+pageNumber,function(data){
    		if(data == null || data == "" || data.totalElements == 0){
    			// 没有数据
    			var nullHtml = '<div class="null_data">' +
	                '<i class="null_classify_ico"></i>' +
	                '<p>暂无相关卖卡记录哦~</p>' +
	                '<a href="'+ ctxPath.domain +'/member/index.do" class="btn btn-blue">返回个人中心</a>' +
	                '</div>';
                $('.data-list').eq(itemIndex).html(nullHtml);   
                
                // 数据加载完                
            	if(tabIndex == 0){
                	tab1LoadEnd = true;
        		}else if(tabIndex == 1){
            		tab2LoadEnd = true;
        		}else if(tabIndex == 2){
            		tab3LoadEnd = true;
        		}
                
                // 无数据
            	$refresh.noData();
                $(".dropload-down").css("display", "none");
    		}else{
    			var d = data.data;
    			var html = '';
    			
    			for(var i = 0; i < d.length; i++){
//    				html += '<li class="list-item">' +
//	                    '<div class="list-item-head row">' +
//	                    '    <div class="col-8 text-left pl-0 pr-0">订单号：'+d[i].orderNumber+' </div>' +
//	                    '    <div class="col text-right color9">成功兑换：<span class="color-orange">'+d[i].successCount+'/'+d[i].totalCount+'</span></div>' +
//	                    '</div>' +
//	                    '<a class="media sellcard-media" href="'+ctxPath.domain+'/member/sellCardRecord/getSellCardOrdersDetails.do?type='+d[i].type+'&id='+d[i].id+'">' +
//	                    '    <div class="media-body">' +
//	                    '        <h5 class="media-con color-orange">￥'+d[i].actualAmount+'</h5>' +
//	                    '    </div>' +
//	                    '</a>' +
//	                    '<p class="list-item-footer no-right-arrow">提交时间：'+format(d[i].commitTime, 'yyyy-MM-dd HH:mm:ss')+'</p>' +
//	                    '</li>';
    				var stateTxt = ''; // 处理状态
    				
    				var processingCount=d[i].totalCount-d[i].oknum-d[i].nonum;
    				
    				//var totalparVal=d[i].parVal * d[i].totalCount;
    				
        			if(d[i].operateStatus==2){
						stateTxt = '<span class="state-btn succ-state">处理完成</span>';
        			}else if(d[i].operateStatus==1){
						stateTxt = '<span class="state-btn succ-state">处理完成</span>';
					}else if(d[i].operateStatus==0){
						stateTxt = '<span class="state-btn being-state">处理中</span>';
    				}
    				html += '<li class="list-item single-card-item">' +
	                '<div class="list-item-head row">' +
	                    '<div class="col-9">'+d[i].operatorName+'</div>' +
	                    '<div class="col text-right">'+stateTxt+'</div>' +
	                '</div>' +
	                '<div class="media sellcard-media">' +
	                	'<div class="media-body">' +                                      
	                        '<p class="color3">批量订单号：'+d[i].fenlei+'</p>' +
	                        '<p class="color3">' +
	                        	'<span>提交总面值：￥'+d[i].sm+'</span>' +
	                        	'<span class="line-bar"> |</span>' +
	                        	'<span class="color-orange">已结算：￥'+d[i].money+'</span>' +
	                       	'</p>' +
	                       	'<p class="color3">' +
                        	'<span>提交总数量：'+d[i].totalCount+'</span>' +
                        	'<span class="line-bar"> |</span>' +
                        	'<span class="color-brown">处理中：'+processingCount+'张</span>' +
                        	'<span class="color-blue padd-left">成功：'+d[i].oknum+'张</span>' +
                        	'<span class="color-crimson padd-left">失败：'+d[i].nonum+'张</span>' +
                       	'</p>' +
	                    '</div>' +
	                '</div>' +		                
	                '<div class="footer clearfix">' +
	                	'<span class="float-left">提交时间：'+format(d[i].create_time, 'yyyy-MM-dd HH:mm:ss')+'</span>' +
	                   	'<a class="float-right text-right color9" href="/getSellCardOrdersDetailsa.html?type=1&id='+d[i].fenlei+'">查看详情&gt;</a>' +
	                '</div>' +
	            '</li>';
    			}
    			
    			if(refreshType == "UPDATE"){
                	$('.data-list').eq(itemIndex).html(html);
                	if(tabIndex == 0){
                		pageNumber1 = 1;
            		}else if(tabIndex == 1){
                		pageNumber2 = 1;
            		}else if(tabIndex == 2){
                		pageNumber3 = 1;
            		}
                	
                	// 数据加载完
                	if(tabIndex == 0){
                    	tab1LoadEnd = false;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = false;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = false;
            		}

                    // 无数据
                    $refresh.noData(false);
            	}else{
                	$('.data-list').eq(itemIndex).append(html);
            	}
         		
         		if(data.last_page == data.current_page){
                	// 数据加载完
                    
                    if(tabIndex == 0){
                    	tab1LoadEnd = true;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = true;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = true;
            		}

                    // 无数据
                   $refresh.noData();
            	}
    		}
    		
    		// 每次数据加载完，必须重置
    		$refresh.resetload();
    	});
	};
	
	// dropload
    var dropload = $('.tab-content').dropload({
        scrollArea : window,
        // 下拉刷新模块显示内容
        domUp : {
            domClass   : 'dropload-up',
            // 下拉过程显示内容
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            // 下拉到一定程度显示提示内容
            domUpdate  : '<div class="dropload-update">↑释放更新</div>',
            // 释放后显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown : {
            domClass   : 'dropload-down',
            // 滑动到底部显示内容
            domRefresh : '<div class="dropload-refresh">↑上拉加载更多</div>',
            // 内容加载过程中显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            // 没有更多内容-显示提示
            domNoData  : '<div class="dropload-noData">已没有更多数据~</div>'
        },
        loadUpFn : function(me){
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", 1, me, "UPDATE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", 1, me, "UPDATE", itemIndex);
            }
            else if(itemIndex == '2'){
            	loadData("2", 1, me, "UPDATE", itemIndex);
            }
        }
        ,
        loadDownFn : function(me){
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", ++pageNumber1, me, "MORE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", ++pageNumber2, me, "MORE", itemIndex);
            }
            else if(itemIndex == '2'){
            	loadData("2", ++pageNumber3, me, "MORE", itemIndex);
            }
        },
        distance:50
    });
}