
/**
 * 好友邀请
 */

// 浮动对话框
$(document).ready(function() {
    var domThis = $('#dialog')[0];
    var wh = $(window).height() / 2;
    $("body").css({
        "background-image": "url(about:blank)",
        "background-attachment": "fixed"
    });
});

//显示灰色 jQuery 遮罩层
function showBg() {
    var bh = $("body").height();
    var bw = $("body").width();
    $("#fullbg").css({
        height:bh,
        width:bw,
        display:"block"
    });
    $("#dialog").show();
}
function copyLog() {
    var bh = $("body").height();
    var bw = $("body").width();
    $("#fullbg").css({
        height:bh,
        width:bw,
        display:"block"
    });
    $("#copy_log").show();
    $("#main").hide();

}
//关闭灰色 jQuery 遮罩
function closeBg() {
    $("#fullbg,#dialog").hide();
    $("#fullbg,#copy_log").hide();
    $("#main").show();
}
//关闭灰色 jQuery 遮罩
function paste() {
  $("#fullbg,#dialog").hide();
  $("#fullbg,#copy_log").hide();
  $("#main").show();
  var ele = document.getElementById("copy_txt");
  ele.select();
  document.execCommand("Copy");
  layerInfoTip("已复制");
}



// 点击隐藏
$(document).ready(function(){
    $("#hide_st").click(function(){
        $("#div3").fadeToggle(0);
        if($("#div3").hasClass('wz_p')) {
            $("#div3").addClass('wz_p')
        }
        if($("#hide_st").hasClass('explain')) {
            $("#hide_st").addClass("explain_unfold")
            $("#hide_st").removeClass("explain")
        }
        else{
        	$("#hide_st").addClass("explain")
            $("#hide_st").removeClass("explain_unfold")
        }
        
    });
});
//邀请功能请求
$(function(){
	ajax_doSubmit("/m/inviteFriends/index.do","data:",function(data){
		var inviteWay = '';
		var way1_box = '';
		var way2_box = '';
		var redBtn = '';
		
		if(data.m && data.m.isRealNameAuthen){
			inviteWay = '<p class="text-center way_p">以下邀请方式，任意一种邀请成功都可佣金返现</p>';
			way1_box = '<p class="way1_box_p">'+data.m.number+'</p>';
			way2_box = '<textarea class="copy_txt" id = "copy_txt" name="" cols="30" rows="10">http://m.renrenxiaoka.com/?invitenumber='+data.m.number+'</textarea>';
			redBtn = '<button class="act-btn or_btn" onclick="copyLog();"></button>'; 
			$("#inviteWay").html(inviteWay);
			$("#way1_box").html(way1_box);
			$("#way2_box").html(way2_box);
			$("#redBtn").html(redBtn);
			
		}else if(data.m && data.m.isRealNameAuthen == false){
			inviteWay = '<p class="text-center way_p">只有通过实名认证，才能有邀请好友的资格，才会有专属邀请链接</p>';
			way1_box = '<textarea class="copy_txt" name="" id="4" cols="30" rows="10">先实名才能发展邀请好友</textarea>';
			way2_box = '<textarea class="copy_txt" name="" id="2" cols="30" rows="10">暂无邀请链接，尽快实名认证哦</textarea>';
			redBtn = '<a  class="real-btn or_btn" href="'+ctxPath.domain+'/member/index.do"></a>';
			$("#inviteWay").html(inviteWay);
			$("#way1_box").html(way1_box);
			$("#way2_box").html(way2_box);
			$("#redBtn").html(redBtn);
		}
		else{
			inviteWay = ' <p class="text-center way_p">以下邀请方式，任意一种邀请成功都可佣金返现</p>';
			way1_box = '<p class="way1_box_p">？？？</p>';
			way2_box = ' <textarea class="copy_txt" name="" id="3" cols="30" rows="10">暂无邀请链接，请登录后获取链接</textarea>';
			redBtn = '<a class="login-btn or_btn" href="'+ctxPath.domain+'/passport/login/index.html"></a>';
			$("#inviteWay").html(inviteWay);
			$("#way1_box").html(way1_box);
			$("#way2_box").html(way2_box);
			$("#redBtn").html(redBtn);
		}		
	});
});





