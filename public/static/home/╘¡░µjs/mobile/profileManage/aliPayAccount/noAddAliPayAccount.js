$(function(){
    //删除
    $(".card-list-item .delete-ico").on("click", function(){
        var id=$(this).parent().find("input[name='id']").val();
        deleteAliPayAccount(id);
    });

    //编辑
    $(".card-list-item .update-ico").on("click", function(){
        var id=$(this).parent().find("input[name='id']").val();
        updateAliPayAccount(id);
    });
});


//添加支付宝账号 或 支付宝账号列表页面继续添加支付宝账号
function addAliPayAccount(){
    parent.layer.alert('', {
        type: 2,
        title: "添加支付宝账号",
        area: ['480px', '442px'],
        scrollbar: false, //是否允许浏览器出现滚动条
        content: addbank,
        success: function(layero, index){

        }
    });
}



//编辑支付宝账号
function updateAliPayAccount(id){
    parent.layer.alert('', {
        type: 2,
        title: "编辑支付宝账号",
        area: ['480px', '442px'],
        scrollbar: false, //是否允许浏览器出现滚动条
        content: addbank+"?id="+id,
        success: function(layero, index){

        }
    });
}

//支付宝账号列表页面删除账号
function deleteAliPayAccount(id){
    $('#sureTradePwdModal').modal('show');
    $("#delId").val(id);//给弹出层的delId赋值

}


function sureDelSeltedAliPayAccount(){
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    var tradePassword = $.trim($("#tradePassword").val());

    var icon = "<i class='fa fa-times-circle'></i>";
    var $errorObj = $("#sureTradePwdModal .errorTip");

    if(isOpenTradePaaword == ""){
        $errorObj.html(icon+" 您未开启交易密码，删除支付宝账号需要验证交易密码");
    }else{
        if(tradePassword == ""){
            $errorObj.html(icon+" 请输入交易密码");
        }else{
            var id=$("#delId").val();
            $errorObj.html("");
            // 删除用户绑定的支付宝账号
            deleteMemberAliPayAccount(icon,$errorObj,id,tradePassword);

        }
    }
}

/**
 * 删除用户绑定的支付宝账号
 * @param icon
 * @param $errorObj
 * @param id
 * @param tradePassword
 */
function deleteMemberAliPayAccount(icon,$errorObj,id,tradePassword){
    $.ajax({
        url:del,
        data:{id:id,tradePassword:tradePassword},
        dataType: "json",
        async: true,
        type: "POST",
        beforeSend: function() {},
        success: function(data) {
            if(data.code == 1)
            {
                //删除成功，刷新页面
                window.location.reload();
            }
            else{
                $errorObj.html(icon+data.msg);
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

//模态框--忘记/设置交易密码
function gotoTradePwdPageFromModel(){
    addFrame( jiyi,"0110040","交易密码");
    $('#sureTradePwdModal').modal('hide');
}