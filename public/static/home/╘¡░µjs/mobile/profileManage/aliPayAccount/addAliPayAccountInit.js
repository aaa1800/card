$(function(){
    //立即添加
    $("#submitAliPayAccountFormBtn").on("click", function(){
        verifyAliPayAccountForm();
    });
});

/**
 * 支付宝账号：1、是否输入  2、格式是否正确（手机或邮箱）
 * 交易密码：1、是否开启  2、是否输入
 */
function verifyAliPayAccountForm(){
    var aliPayAccount = $.trim($("#aliPayAccount").val());
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    var tradePassword = $.trim($("#tradePassword").val());

    var icon = "<i class='fa fa-times-circle'></i>";
    var $errorObj = $("#addAliPayAccountForm .errorTip");

    if(aliPayAccount == ""){
        $errorObj.html(icon+" 请输入支付宝账号");
    }else{
        // var phoneReg = /^1[345879]\d{9}$/;
        var phoneReg = /^1\d{10}$/;
        var mailReg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

        if(phoneReg.test(aliPayAccount) || mailReg.test(aliPayAccount)){
            if(isOpenTradePaaword == ""){
                $errorObj.html(icon+" 您未开启交易密码，开启交易密码才可添加支付宝账号");
            }else{
                if(tradePassword == ""){
                    $errorObj.html(icon+" 请输入交易密码");
                }else{
                    $errorObj.html("");
                    saveAliPayAccount(icon,$errorObj);
                }
            }
        }else{
            $errorObj.html(icon+" 支付宝账号不正确");
        }
    }
}



/**
 * 保存支付宝账号
 */
function saveAliPayAccount(icon,$errorObj){
    $.ajax({
        url:$("#addAliPayAccountForm").attr("action"),
        data:$("#addAliPayAccountForm").serialize(),
        dataType: "json",
        async: true,
        type: "POST",
        beforeSend: function() {},
        success: function(data) {
            if(data.code == 1)
            {

                //添加成功，跳转到支付宝账号首页
                addFrame( alilist,"011009","支付宝账号");

                var winindex = parent.layer.getFrameIndex(window.name); //获取窗口索引
                parent.layer.close(winindex);

            }
            else{
                $errorObj.html(icon+data.msg);
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

//设置交易密码
function gotoSetTradePwdFn(){

    addFrame( jiyi,"0110040","交易密码");

    var winindex = parent.layer.getFrameIndex(window.name); //获取窗口索引
    parent.layer.close(winindex);

}

//忘记交易密码
function forgetTradePwdFn(){
    addFrame( jiyi,"0110040","交易密码");

    var winindex = parent.layer.getFrameIndex(window.name); //获取窗口索引
    parent.layer.close(winindex);
}