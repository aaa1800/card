/**
 * 添加微信账号
 */
$(function(){
	getQrCodeImg();
	
    var qrcodeSrc = $.trim($("#addWxQRImg").attr("src"));
    $("#addWxQRImg").on("click", function(){
        if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
            window.location.href = qrcodeSrc;
        } else if (/(Android)/i.test(navigator.userAgent)) {
            console.log('这是Android');
        } else {
            console.log('这是PC');
        }
    });
    
});

//获取二维码图片
function getQrCodeImg(){
	$.post(ctxPath,function(data){
		 if(data.code==1){
			$("#addWxQRImg").attr("src", data.url);
			$("#loadImg").hide();
			$("#addWxQRImg").show();
		}else{
			layerInfoTip(data.msg);
		}
	})
	
}