/*!
  * author：lxf
  * date: 2018-4-10 11:02:34
  * description: 支付宝账号列表
  */
$(function(){
	aliAccountDataInit();
	
    //删除
	$(document).on("click", "#aliAccountList .delete-ico", function(){
        var id = $(this).parents("li").find("input[name='id']").val();
        deleteAliPayAccount(id);
    });
});

function aliAccountDataInit(){
	ajax_doSubmit("/getBank.html","data:id=1",function(data){
		if(data != null && data != "" && data != undefined){
			var realName = data.m.realName;
			var d = data.bankCardList;
			if(d.length > 0){
				var liItem = "";
				for(var i=0;i<d.length;i++){
					liItem += '<li>'+
		                    '<input type="hidden" value="'+d[i].id+'" name="id">'+
		                    '<a class="media" href="javascript:void(0);">'+
		                        '<img class="w74 align-self-center" src="/static/home/images/profileManage/withdrawAccount/card_zfb_icon.png">'+
		                        '<div class="media-body">'+realName+'</div>'+
		                    '</a>'+
		                    '<p class="desc text-truncate">'+
		                        '<span>'+d[i].accounts+'</span>'+
		                        '<a href="'+editl+'?id='+d[i].id+'" class="edit-ico"></a>'+
		                        '<i class="delete-ico"></i>'+
		                    '</p>'+
		                '</li>';
				}
				$("#aliAccountList").html(liItem);
			}
			
			if(data.isSureAddBankCard){
				$("#addAccountBtn").removeClass('d-none');
			}
		}
	});
}

//支付宝账号列表页面删除账号
function deleteAliPayAccount(id){
    $('#sureTradePwdModal').modal('show');
    $("#delId").val(id);//给弹出层的delId赋值
}


function sureDelSeltedAliPayAccount(){
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    var tradePassword = $.trim($("#tradePassword").val());

    var $errorObj = $("#sureTradePwdModal .errorTip");

    if(isOpenTradePaaword == ""){
        $errorObj.addClass("wrong").html("您未开启交易密码，开启后才可执行删除操作");
    }else{
        if(tradePassword == ""){
            $errorObj.addClass("wrong").html("请输入交易密码");
        }else{
            var id = $("#delId").val();
            $errorObj.removeClass("wrong").html("");
            // 删除用户绑定的支付宝账号
            deleteMemberAliPayAccount($errorObj,id,tradePassword);

        }
    }
}

/**
 * 删除用户绑定的支付宝账号
 * @param $errorObj
 * @param id
 * @param tradePassword
 */
function deleteMemberAliPayAccount($errorObj,id,tradePassword){
	$.post(delurl,{id:id,tradePassword:tradePassword},function(data){
		if(data.code == 1){
            //删除成功，刷新页面
            window.location.reload();
        }else{
            $errorObj.addClass("wrong").html(data.msg);
        }
	})
}

