/*!
  * author：lxf
  * date: 2018-4-3 11:29:18
  * description: 联系QQ设置
  */

$(function(){
    $(document).on("click", "#submitContactQQFormBtn", function(){
        updateContactQQ();
    })
});

/**
 * 保存QQ号码
 */
function updateContactQQ() {
    var qqNumber = $.trim($("#qqNumber").val());
    if(qqNumber == ""){
        layerInfoTip("请输入QQ号码");
    }else{
        var qqreg = /^[0-9]{5,12}$/;
        if(qqreg.test(qqNumber)){
            // 注意，这里不能给返回个人信息的按钮，否则点击个人信息的返回按钮的时候是退回到设置QQ号码页面
            // window.location.href = webPath.domain+"/member/profileManage/index.do";
          /*  layer.open({
                content: '<p class="ico ico_right">QQ号码设置成功,如有疑问我们会及时联系您</p>'
                ,btn: ['我知道了']
                ,yes: function(index){
                    window.location.reload();
                    layer.close(index);
                }
            });*/
			$.post(qqurl,{qq:qqNumber,_csrf:ctxPath._csrf},function(data){
				 if(data.code==1){
                        //成功样式
//                        updateNickNameOrQQSuccess("更新QQ号码成功！");
                        layerInfoTip("更新QQ号码成功！");
                    }else{
                       // layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
						//报错信息
                        $errorObj.html(icon + data.msg);
                    }
			})
           /*  $.ajax({
                url: qqurl,
                data:{qq:qqNumber,_csrf:ctxPath._csrf},
                xhrFields: {
   	             withCredentials: true
                },
                dataType: "json",
                crossDomain: true,
                async: true,
                type: "POST",
                beforeSend: function() {

                },
                success: function(data) {
                    if(data.code=="000000"){
                        //成功样式
//                        updateNickNameOrQQSuccess("更新QQ号码成功！");
                        layerInfoTip("更新QQ号码成功！");
                    }else{
                        //报错信息
                        $errorObj.html(icon + data.message);
                    }
                },
                complete: function() {
                },
                error: function(e) {
                    layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
                }
            }); */

        }else{
            layerInfoTip("QQ号码格式不正确");
        }
    }
}