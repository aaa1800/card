/**
 * date：2018-1-19 09:32:44
 * author：lxf
 * description：重置交易密码接收验证码或输入原交易密码页面脚本
 */
$(function(){
    //验证方式的显示或隐藏
    var verifyWay = $("#verifyWay").val();
    getCurVerifyWay(verifyWay);

    //获取邮箱验证码
    $("#getMailCode").on("click", function(){
        var $getCodeBtn = $("#getMailCode");
        sendVertifyCode($getCodeBtn);
    });

    /**
     * 下一步
     */
    $("#submitResetTradePwdOneFormBtn").on("click", function(e){
        var mailCode = $.trim($("#mailCode").val());//验证码
        var oldTran = $.trim($("#oldTran").val());//新交易密码
        var newTran = $.trim($("#newTran").val());//新交易密码
        var icon = "<i class='fa fa-times-circle'></i>";
        var $errorObj = $("#jiaoyi");
		
            var verifyOldTradePwdResult = verifyCode(mailCode, $errorObj);
			if(!verifyOldTradePwdResult)return;
            var fggh=verifyPassword(oldTran, $errorObj, '交易密码');
			if(oldTran!=newTran){
				var icon = "<i class='fa fa-times-circle'></i>";
				$errorObj.html(icon+" 两次密码不一致");
				return;
			}
            if(verifyOldTradePwdResult && fggh && oldTran==newTran){
                // 重置交易密码-通过原交易密码验证交易密码
                $.ajax({
                    url: editTran,
                    data:{tradePwd:oldTran,codeTran:mailCode},
                    dataType: "json",
                    async: true,
                    type: "POST",
                    xhrFields: {
                    	withCredentials: true
	       	        },
	       	        crossDomain: true,
                    beforeSend: function() {},
                    success: function(data) {
                        if(data.code == 1)
                        {
                            //成功，刷新页面
					layer.alert('<span style="color:#27c5ab;">交易密码设置成功,若忘记自行修改或联系客服！</span>', {
						title: "提示",
						icon: 1,
						skin: 'layer-ext-moon',
						btn: '返回账户总览',
						yes: function(index, layero){
							layer.close(index);
							gotoAccountOverviewPage();
						},
						cancel: function(index, layero){//右上角关闭按钮回调
							window.location.href = member;
						}
					});
                        }
                        else{
                            $errorObj.html(icon + data.msg);
                        }
                    },
                    complete: function() {
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                    }
                });
            }else{
                e.preventDefault();
            }

    });
});

function getCurVerifyWay(verifyWay){
    var $phoneWayObj = $("#viaPhoneVerify");
    var $mailWayObj = $("#viaMailVerify");
    var $tradePwdWayObj = $("#viaTradePwdVerify");

    //为了视觉效果，先全部隐藏
    $phoneWayObj.hide();
    $mailWayObj.hide();
    $tradePwdWayObj.hide();

    if(verifyWay == "phone"){
        $phoneWayObj.show();
        $mailWayObj.hide();
        $tradePwdWayObj.hide();
    }else if(verifyWay == "email"){
        $phoneWayObj.hide();
        $mailWayObj.show();
        $tradePwdWayObj.hide();
    }else if(verifyWay == "tradePassword"){
        $phoneWayObj.hide();
        $mailWayObj.hide();
        $tradePwdWayObj.show();
    }
}

//ajax发送验证码
function ajaxSendVertifyCode($getCodeBtn, time){
    var verifyWay = $("#verifyWay").val();
    // 重置交易密码-获取验证码初始化
    resetTradePasswordVertifyInit(verifyWay,$getCodeBtn,time);

}

    //



    /**
     * 重置交易密码-获取验证码初始化
     * @param type
     * @param $getCodeBtn
     * @param time
     */
    function resetTradePasswordVertifyInit(type,$getCodeBtn,time) {
        $.ajax({
            url: ctxPath,
            data:{type:type,_csrf:ctxPath._csrf},
            xhrFields: {
	             withCredentials: true
	        },
	        crossDomain: true,
            dataType: "json",
            async: true,
            type: "POST",
            beforeSend: function() {},
            success: function(data) {
                if(data.code == "0")
                {
                    // $("#username").val(data.object);
                    // 重置交易密码-发送重置交易密码验证码
                    noticeCodeSendSuccess("验证码发送成功", $getCodeBtn, time);
					$("#tran").show();
					$("#subid").show();
                }
                else{
                    parent.layer.msg(data.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
                }
            },
            complete: function() {
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }


   


/**
 * 重置交易密码-验证交易密码验证码
 * @param type
 */
function vertifyResetTradePasswordCode(type, code,$errorObj,icon,getCode) {
    $.ajax({
        url: ctxPath.domainapi+"/m/member/passwordManage/tradePassword/vertifyResetTradePasswordCode.do",
        data:{type:type,code:code,_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "000000")
            {     //成功，刷新页面

                window.location.href = ctxPath.domain+"/member/profileManage/passwordManage/tradePassword/setNewTradePasswordInit.do";
            }
            else{
                if(data.code=="000002"){
                    if ($("#"+getCode).hasClass("btn-danger")) {
                        noticeCodeSendSuccess(data.message,$("#"+getCode), 60);
                    }else{
                        parent.layer.msg(data.message, {icon: 2,shade: [0.4, '#000'],time : 1000});
                    }
                }else{
                    $errorObj.html(icon + data.message);
                }
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}