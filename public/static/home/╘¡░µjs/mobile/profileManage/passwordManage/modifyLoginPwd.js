/**
 * date：2018-1-18 16:24:08
 * author：lxf
 * description：已经绑定手机或邮箱，也已经设置了登录密码，修改登录密码页面脚本
 */
$(function(){
    
    /**
     * 表单验证
     * 1、原登录密码：是否为空，密码由6-20位字母、数字或者符号两种或以上组成
     * 2、新登录密码：同上
     * 3、确认新密码：同上，并且需要与新登录密码一样
     * 4、图片验证码：5位
     */
    $("#submitModifyLoginPwdFormBtn").on('click', function(e) {
        var oldLoginPwd = $.trim($("#oldLoginPwd").val());//原登录密码
        var newLoginPwd = $.trim($("#newLoginPwd").val());//新登录密码
        var newLoginPwdAgain = $.trim($("#newLoginPwdAgain").val());//确认新密码
		var token = $("input[name=__token__]").val();
        var code = $.trim($("#code").val());//验证码

        var icon = "<i class='fa fa-times-circle'></i>";
        var $errorObj = $("#modifyLoginPasswordForm .errorTip");
        var verifyOldLoginPwdResult = verifyPassword(oldLoginPwd, $errorObj, "原登录密码");
        if(verifyOldLoginPwdResult){
            var verifyNewLoginPwdResult = verifyPassword(newLoginPwd, $errorObj, "新登录密码");
            if(verifyNewLoginPwdResult){
                var newLoginPwdAgainResult = verifyPassword(newLoginPwdAgain, $errorObj, "'确认新密码'");
                if(newLoginPwdAgainResult){

                    if(newLoginPwd == newLoginPwdAgain){
                        if(code == ""){
                            $errorObj.html(icon+" 请输入验证码");
                        }else{

                            $.post(editPass,{passa:oldLoginPwd,ps1:newLoginPwdAgain,ps2:newLoginPwd,mcode:code,__token__:token},function(data) {
                                    if(data.code == 1)
                                    {
                                        //成功，刷新页面
                                        layer.alert('<span style="color:#27c5ab;">登录密码修改成功</span>', {
                                            title: "提示",
                                            icon: 1,
                                            skin: 'layer-ext-moon',
                                            btn: '返回账户总览',
                                            yes: function(index, layero){
                                                layer.close(index);
                                                gotoAccountOverviewPage();
                                            },
                                            cancel: function(index, layero){//右上角关闭按钮回调
                                                window.location.href = member;
                                            }
                                        });

                                    }
                                    else{
										$("input[name=__token__]").val(data.token);
                                        $errorObj.html(icon + data.msg);
                                    }
                                }
                            );

                        }


                    }else{
                        $errorObj.html(icon+" 两次新密码输入不一致");
                    }

                }else{
                    e.preventDefault();
                }
            }else{
                e.preventDefault();
            }
        }else{
            e.preventDefault();
        }
    });
});

//忘记原登录密码
function forgetLoginPwd() {
    top.location.href = ctxPath;
}


function changeImg() {
    var $imgObj = $("#imgObj");
    $imgObj.attr("src", chgUrl());
}

//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
function chgUrl() {
    var timestamp = (new Date()).valueOf();
    return ctxPath.domain+"/code" + "?timestamp=" + timestamp;
}



