/*!
  * author：lxf
  * date: 2018-6-6 11:36:01
  * description: 密码管理首页
  */
$(function(){
	loadPageInit();
});

function loadPageInit(){
	ajax_doSubmit("/passwordManageInit.html","data:",function(data){ 		
		if(data.password=="" || data.password==null || data.password==undefined){
           $("#loginPwdStatus").html('未设置');
        }else{
        	$("#loginPwdStatus").html('修改');
        }
		
		if(data.tradePassword=="" || data.tradePassword==null || data.tradePassword==undefined){
           $("#tradePwdStatus").html('未设置');
           $("#forgetTradePwdStatus").html('未设置');
        }else{
        	$("#tradePwdStatus").html('修改');
        	$("#forgetTradePwdStatus").html('找回');
        }
    });
}