/*!
  * author：lxf
  * date: date
  * description: 
  */
$(function () {
    //证件颁发日期
    var issuanceCalendar = new lCalendar({
    	finishHandle: function(v){
    		console.log("证件颁发日期:"+v);
    		var expireDate = $.trim($("#expireDate").val());
    		isShowUploadImgWrap(expireDate);
    	},
    	closeHandle: function(){
    		console.log("取消");
    	}
    });
    issuanceCalendar.init({
        'trigger': '#issuanceDate',
        'type': 'date'
    });

    //证件到期日期
    var expireCalendar = new lCalendar({
    	finishHandle: function(v){
    		console.log("证件到期日期:"+v);
    		var issuanceDate = $.trim($("#issuanceDate").val());
    		isShowUploadImgWrap(issuanceDate);
    	},
    	closeHandle: function(){
    		console.log("取消");
    	}
    });
    expireCalendar.init({
        'trigger': '#expireDate',
        'type': 'date'
    });
    
    //显示隐藏照片部分
    $(document).on("click", "#uploadPicMedia", function(){
    	if($(this).hasClass("show-pic-wrap")){
    		$(this).removeClass("show-pic-wrap");
    		$(".upload-wrap").addClass("d-none");
    	}else{
    		$(this).addClass("show-pic-wrap");
    		$(".upload-wrap").removeClass("d-none");
    	}
    });

    //提交表单
    $(document).on("click", "#submitMediumAuthenInitFormBtn", function(){
		var name = $.trim($("#name").val());
		var number = $.trim($("#number").val());
		var issuanceDate = $.trim($("#issuanceDate").val());
        var issuanceDate = $.trim($("#issuanceDate").val());
        var expireDate = $.trim($("#expireDate").val());
        var faceImgFile = $.trim($("#upload_frontpic_btn").val());
        var backImgFile = $.trim($("#upload_backpic_btn").val());
        var handImgFile = $.trim($("#upload_handpic_btn").val());       

        if(name=="" || name==null || name==undefined){
            layerInfoTip("请填写企业名称");
        }else if(number=="" || number==null || number==undefined){
            layerInfoTip("请填写营业执照号");
        }else if(issuanceDate=="" || issuanceDate==null || issuanceDate==undefined){
            layerInfoTip("请选择证件颁发日期");
        }else if(expireDate=="" || expireDate==null || expireDate==undefined){
            layerInfoTip("请选择证件到期日期");
        }else if(faceImgFile=="" || faceImgFile==null || faceImgFile==undefined){
        	layerInfoTip("您当前未上传正面照，请先上传");
        }else if(backImgFile=="" || backImgFile==null || backImgFile==undefined){
        	layerInfoTip("您当前未上传反面照，请先上传");
        }else if(handImgFile=="" || handImgFile==null || handImgFile==undefined){
        	layerInfoTip("您当前未上传手持身份证照，请先上传");
        }else{
        	//var headers = {};
        	//headers['X-CSRF-TOKEN']=ctxPath.csrf;
        	
        	var formData = new FormData($('#mediumAuthenInitForm')[0]);
            
            $.ajax({
                url: ctxPath,
                type: "POST",
                data: formData,
                xhrFields: {
                    withCredentials: true
               },
        　　　		// 允许跨域
               crossDomain: true,
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function () {
                	layerInfoTip("正在处理中，请稍后...");
    			},
                success: function (data) {
                	if(data.code==1){
        				window.location.reload();
        	        }else{           
        	        	layerInfoTip(data.msg);
        	        }
                },
                error: function (data) {
                	layerInfoTip(data.message);
                }
            });
        }
    });
});

function isShowUploadImgWrap(date){
	if(date!="" && date!=null && date!=undefined){
		$("#uploadPicMedia").addClass("show-pic-wrap");
		$(".upload-wrap").removeClass("d-none");
	}else{
		//alert("null");
	}
}

function getImgVal(obj){
    var $parents = $(obj).parents(".upload-item");
    $parents.find(".upload-tips").hide();
    $parents.find(".upload-sucimg-wrap").show();
    
    var files = obj.files;
    if(files && files.length){
        if(files[0].size/1024 > 1024*2){//大于2M，进行压缩上传
            // console.log("大于2M，进行压缩上传");
            photoCompress(files[0], {
                quality: 0.2
            }, function(base64Codes){
                // console.log("压缩后：" + base64Codes.length / 1024 +"KB");
                $parents.find(".upload-sucimg-wrap img").attr("src", base64Codes);
            });
        }else{//小于等于2M 原图上传
            // console.log("小于等于2M 原图上传");

            //JS file 图片 即选即得 显示
            //创建一个FileReader对象
            var reader = new FileReader();
            //读取File对象的数据
            reader.onload = function(evt){
                //data:img base64 编码数据显示
                $parents.find(".upload-sucimg-wrap img").attr("src", evt.target.result);
            };
            reader.readAsDataURL(files[0]);
        }

    }           
    
}

