/**
 * 实名认证表单提交
 */
$(function(){
    //立即提交
    $("#submitRealNameFormBtn").on("click", function(){
        verifyRealNameForm();
    });
});

/**
 * 真实姓名：2-15个汉字，或者含·
 * 身份证号：15位或者18位
 */
function verifyRealNameForm(){
    var name = $.trim($("#name").val());
    var idCard = $.trim($("#idCard").val());
//    var chineseNameReg = /^[\u4e00-\u9fa5]{2,15}$/;
    var chineseNameReg = /^[\u4e00-\u9fa5]([\u4e00-\u9fa5]|\·){1,14}$/;

    if(name == ""){
        layerInfoTip("请输入真实姓名");
    }else{
        if(chineseNameReg.test(name)){
            if(idCard == ""){
                layerInfoTip(" 请输入身份证号");
            }else{
                if(isIdCardNo(idCard)){
                    //ajax提交表单
                    memberRealNameAuthen();
                }else{
                    layerInfoTip("身份证号格式不正确");
                }
            }
        }else{
            layerInfoTip("姓名不规范，请输入正确的真实姓名");
        }
    }
}

/**
 * 实名认证
 */
function memberRealNameAuthen(){  
    $.post(ren,{'username':$("input[name=name]").val(),'idcard':$("input[name=idCard]").val()},function(data){
		if(data.code == 1){
        	layer.open({
                content: '<p class="ico ico_right">实名认证通过</p>'
                ,shadeClose: false
                ,btn: ['我知道了']
                ,yes: function(index){
                    window.location.reload();
                    layer.close(index);
                }
            });
		}else{
			layerInfoTip(data.msg);
		}
	})
}