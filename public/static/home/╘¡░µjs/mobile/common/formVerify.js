$(function(){
    //实时监听输入框变化
    $(document).on('input propertychange', '.form-control', function (e) {
        if (e.type === "input" || e.orignalEvent.propertyName === "value") {
            if(this.value.length > 0){
                $(this).parents(".form-group").find("a.delete").show();
            }else{
                $(this).parents(".form-group").find("a.delete").hide();
            }
        }
    });

    //清除输入框的值
    $(document).on('click', '.form-group .delete', function (e) {
        $(this).hide().siblings(".form-control").val("");
        /*var isShowPass = $(this).parents(".form-group").find(".isShowPass");
        if(isShowPass.length > 0){
            isShowPass.hide();
        }*/
    });

    //显示密码
    $(document).on('click', '.form-group .isShowPass', function (e) {
        var $inpt = $(this).parents(".form-group").find(".form-control");
        if($(this).hasClass("showPassword")){
            $(this).removeClass("showPassword").addClass("hidePassword");
            $inpt.attr("type", "password");
        }else if($(this).hasClass("hidePassword")){
            $(this).removeClass("hidePassword").addClass("showPassword");
            $inpt.attr("type", "text");
        }
    });
});

//手机
function checkPhone(phone){
    var phoneReg = /^1\d{10}$/;
    return phoneReg.test(phone);
}

/**
 * 手机号验证：是否输入、格式是否正确
 * @param phone
 */
function verifyPhone(phone){
    if(phone == ""){
        layerInfoTip("请输入手机号码");
        return false;
    }else{
        if(!checkPhone(phone)){
            layerInfoTip("手机号格式不正确");
            return false;
        }else{
            return true;
        }
    }
}

//邮箱
function checkMail(mail) {
    var mailReg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    return mailReg.test(mail);
}

/**
 * 邮箱验证：是否输入、格式是否正确
 * @param mail
 * @returns {boolean}
 */
function verifyMail(mail){
    if(mail == ""){
        layerInfoTip("请输入邮箱账号");
        return false;
    }else{
        if(!checkMail(mail)){
            layerInfoTip("邮箱账号格式不正确");
            return false;
        }else{
            return true;
        }
    }
}

//密码
function checkPassword(password) {
    var passReg = /^(?![A-z]+$)(?!\d+$)(?![\W_]+$)^.{6,20}$/;
    return passReg.test(password);
}

//验证码
function checkCode(code){
    var codeReg = /^\d{4}$/;
    return codeReg.test(code);
}
/**
 * 验证码验证：是否输入、格式是否正确
 * @param code
 */
function verifyCode(code){
    var codeLength = 4;
    if(code == ""){
        layerInfoTip("请输入验证码");
        return false;
    }else{
        return true;
    }
}

/**
 *
 * @param $getCodeBtn: 获取验证码按钮对象
 * @param timeId
 * @param txt
 */
function codeTimer($getCodeBtn, timeId, txt){
    $getCodeBtn.removeClass("color-green").html('重新发送<span id="time">(60)</span>');

    var t = 59;//设定跳转的时间
    var iCount = setInterval(refer,1000); //启动1秒定时
    function refer(){

        if(t==0){
            $getCodeBtn.addClass("color-green").html(txt);
            clearInterval(iCount);
        }

        $getCodeBtn.find("#"+timeId).html("(" + t + ")");

        t--; // 计数器递减
    }
}

//身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
function isIdCardNo(card) {
    var idCard = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return idCard.test(card);
}