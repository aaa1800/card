//手机设置
function gotoBindPhone() {
    addFrame( phoneSetting,"011001","手机设置");
}

//实名认证
function gotoRealName() {
    addFrame(realNameAuthenInit,"011002","实名认证");
}

//银行账号
function gotoBankAccount() {
    addFrame( bankAccountIndex,"011003","银行账号");
}

//登录密码
function gotoSetLoginPassword() {
    addFrame( passwordManageInit,"011004","登录密码");
}

//交易密码
function gotoSetTradePassword() {
    addFrame( tradePasswordManage,"0110040","交易密码");
}

//邮箱设置
function gotoBindMail() {
    addFrame( mailSettingInit,"011005","邮箱设置");
}

//QQ绑定
function gotoBindQQ() {
    addFrame( ctxPath.domain+'/member/profileManage/qqBind/index.do',"011006","QQ绑定");
}

//微信绑定
function gotoBindWeiXin() {
    addFrame( ctxPath.domain+'/member/profileManage/bindWeChat/index.do',"011007","微信绑定");
}

//支付宝账号
function gotoAlipayAccount() {
    addFrame( ctxPath.domain+'/member/profileManage/aliPayAccount/aliPayAccount/index.do',"011009","支付宝账号");
}

//微信账号
function gotoWeixinAccount() {
  addFrame( ctxPath.domain+'/member/profileManage/weixinAccount/weixinAccount/index.do',"011012","微信账号");
}

//微信添加账号
function gotoFollowWeiXinAccount() {
  addFrame( ctxPath.domain+'/member/profileManage/weixinAccount/weixinAccount/addOrUpdateWeChatAccountInit.do',"011011","微信添加账号");
}

//账户流水
function gotoAccountFinance() {
    addFrame( accountStatement,"010002","账户流水");
}