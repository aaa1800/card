/**
 * date： 2018-1-15 11:16:07
 * author： lxf
 * description: 资料管理-基本资料
 */
/*$(function(){

});*/

/**
 * 打开设置或修改用户昵称模态框
 * @param title：标题
 */
function openNickNameModal(title){
    $('#setOrModifyNickNameModal').modal('show');
    $("#setOrModifyNickNameModal .modal-title").html(title);
}

/**
 * 昵称为2-20个字符，汉字、字母、数字、字符组成
 */
function updateNickName() {
    var icon = "<i class='fa fa-times-circle'></i>";
    var $errorObj = $("#setOrModifyNickNameModal .errorTip");
    var name = $.trim($("#nickName").val());
    if(name == ""){
        $errorObj.html(icon+" 请输入昵称");
    }else{
        var reg = /^[\u4e00-\u9fa5\w-]{2,20}$/;
        if(reg.test(name)){
            $.ajax({
                url: ctxPath.domain+"/member/profileManage/baseProfile/updateMemberNickName.do",
                data:{nickName:name,_csrf:ctxPath._csrf},
                dataType: "json",
                async: true,
                type: "POST",
                beforeSend: function() {

                },
                success: function(data) {
                  if(data.code=="000000"){
                      //成功样式
                      $errorObj.html("");
                      updateNickNameOrQQSuccess("更新昵称成功！");
                  }else{
                      //报错信息
                      $errorObj.html(icon + data.message);
                  }
                },
                complete: function() {
                },
                error: function(e) {
                    layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
                }
            });

        }else{
            $errorObj.html(icon+" 昵称为2-20个字符，汉字、字母、数字、字符组成");
        }
    }
}

/**
 * 打开设置或修改QQ号码模态框
 * @param title：标题
 */
function openQQnumberModal(title){
    $('#setOrModifyQQModal').modal('show');
    $("#setOrModifyQQModal .modal-title").html(title);
}

/**
 * 保存QQ号码按钮
 */
function updateQqNumber() {
    var icon = "<i class='fa fa-times-circle'></i>";
    var $errorObj = $("#setOrModifyQQModal .errorTip");
    var qqNumber = $.trim($("#qqNumber").val());
    if(qqNumber == ""){
        $errorObj.html(icon+" 请输入QQ号码");
    }else{
        var qqreg = /^[0-9]{5,12}$/;
        if(qqreg.test(qqNumber)){
            $.ajax({
                url: ctxPath.domain+"/member/profileManage/baseProfile/updateMemberQq.do",
                data:{qq:qqNumber,_csrf:ctxPath._csrf},
                dataType: "json",
                async: true,
                type: "POST",
                beforeSend: function() {

                },
                success: function(data) {
                    if(data.code=="000000"){
                        //成功样式
                        $errorObj.html("");
                        updateNickNameOrQQSuccess("更新QQ号码成功！");
                    }else{
                        //报错信息
                        $errorObj.html(icon + data.message);
                    }
                },
                complete: function() {
                },
                error: function(e) {
                    layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
                }
            });

        }else{
            $errorObj.html(icon+" QQ号码格式不正确");
        }
    }
}

function updateNickNameOrQQSuccess(message) {
    parent.layer.alert('<span style="color:#27c5ab;">'+ message +'</span>', {
        title: "提示",
        icon: 1,
        skin: 'layer-ext-moon',
        btn: '返回资料管理',
        yes: function(index, layero){
            //按钮的回调
            window.location.reload();
            parent.layer.close(index);
            // addFrame( ctxPath.domain+'/member/withdrawCashRecord/index.do',"1133","提现记录");
        },
        cancel: function(index, layero){//右上角关闭按钮回调
            window.location.reload();
        }
    });
}

