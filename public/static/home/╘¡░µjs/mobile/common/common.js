$(function(){
    //返回顶部
    $(window).scroll(function () {
        $(window).scrollTop() > 400 ? $("#totop").show() : $("#totop").hide();
    });
    $("#totop").click(function () {
        $("body,html").scrollTop(0);
    });
});

/**
 * 数字千位符格式化
 * @param num
 * @returns {string}
 */
function numberFormatter(num) {
    return (parseFloat(num).toFixed(2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}

