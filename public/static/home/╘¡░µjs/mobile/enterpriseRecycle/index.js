$(function(){
	ajax_doSubmit("/getBaseInfo.html","data:",function(data){		
		$(".cooperation-btn").attr("href", 'tel:'+data.csPhone);
	});
	
    pullToRefresh();
});

function pullToRefresh(){
    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            callback: function () {
                window.location.reload();//页面刷新
                miniRefresh.endDownLoading(true);// 结束下拉刷新
            }
        },
        up: {
            isAuto: false,
            callback: function () {
                miniRefresh.endUpLoading();// 结束上拉加载
            }
        }
    });
}