/**
 * 资料管理--手机设置--手机和邮箱都没有绑定
 */
$(function(){
    //获取验证码
    $("#getPhoneCode").click(function(){
       var phone = $.trim($("#phone").val());
       var $errorObj = $("#phoneSettingIndexForm .errorTip");
       var verifyPhoneResult = verifyPhone(phone, $errorObj);
       if(verifyPhoneResult){
           var $getCodeBtn = $("#getPhoneCode");
           sendVertifyCode($getCodeBtn);
       }

    });

    //立即提交
    $("#submitPhoneSetIndexFormBtn").on('click', function(e) {
        var phone = $.trim($("#phone").val());//手机号
        var phoneCode = $.trim($("#phoneCode").val());//手机验证码

        var icon = "<i class='fa fa-times-circle'></i>";
        var $errorObj = $("#phoneSettingIndexForm .errorTip");
        var verifyPhoneResult = verifyPhone(phone, $errorObj);

        if(verifyPhoneResult){
            var verifyPhoneCodeResult = verifyCode(phoneCode, $errorObj);
            if(verifyPhoneCodeResult){
                // 未绑定手机和邮箱- 验证绑定手机号验证码
                vertifyBindPhoneCode(icon,$errorObj,phoneCode,phone);
            }else{
                e.preventDefault();
            }
        }else{
            e.preventDefault();
        }

    });
});

//ajax发送验证码
function ajaxSendVertifyCode($getCodeBtn, time){
    // 未绑定手机和邮箱-手机绑定验证初始化
    bindPhoneVertifyInit($getCodeBtn, time);

}


/**
 * 未绑定手机和邮箱-手机绑定验证初始化
 */
function bindPhoneVertifyInit($getCodeBtn, time){
    $.ajax({
        url: ctxPath.domainapi + "/m/member/profileManage/phoneSetting/bindPhoneVertifyInit.do",
        data:{_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "000000")
            {
                //初始化成功，未绑定手机和邮箱-发送绑定手机号验证码
                sendBindPhoneVertifyCode($getCodeBtn, time);
            }
            else
            {
                parent.layer.msg(data.message, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

/**
 * 未绑定手机和邮箱-发送绑定手机号验证码
 * @param $getCodeBtn
 * @param time
 * @param phone
 */
function sendBindPhoneVertifyCode($getCodeBtn, time){
    var phone = $.trim($("#phone").val());//手机号
    $.ajax({
        url: ctxPath.domainapi + "/m/member/profileManage/phoneSetting/sendBindPhoneVertifyCode.do",
        data:{phone:phone,_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "000000")
            {
                noticeCodeSendSuccess(data.message, $getCodeBtn, time);
            }
            else
            {
                parent.layer.msg(data.message, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

/**
 * 未绑定手机和邮箱- 验证绑定手机号验证码
 * @param icon
 * @param $errorObj
 * @param mailCode
 * @param phone
 */
function vertifyBindPhoneCode(icon,$errorObj,phoneCode,phone){
    $.ajax({
        url: ctxPath.domainapi + "/m/member/profileManage/phoneSetting/vertifyBindPhoneCode.do",
        //data:{code:phoneCode,validNewPhone:phone,_csrf:ctxPath._csrf},
        data:$("#phoneSettingIndexForm").serialize(),
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "000000")
            {
                //成功提示
                parent.layer.alert('<span style="color:#27c5ab;">手机设置成功！</span>', {
                    title: "提示",
                    icon: 1,
                    skin: 'layer-ext-moon',
                    btn: '查看手机设置',
                    yes: function(index, layero){
                        window.location.reload();
                        parent.layer.close(index);
                    },
                    cancel: function(index, layero){//右上角关闭按钮回调
                        window.location.reload();
                    }
                });
            }
            else
            {

                if(data.code=="000002"){
                    if ($("#getMailCode").hasClass("btn-danger")) {
                        noticeCodeSendSuccess(data.message,$("#getMailCode"), 60);
                    }else{
                        parent.layer.msg(data.message, {icon: 2,shade: [0.4, '#000'],time : 1000});
                    }
                }else{
                    $errorObj.html(icon + data.message);
                }
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

