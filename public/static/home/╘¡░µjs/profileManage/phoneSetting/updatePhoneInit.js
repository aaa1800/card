/**
 * 资料管理--手机设置--更换手机
 */
$(function(){
    //获取验证码
    $("#getPhoneCode").click(function(){
        var $getCodeBtn = $(this);
        sendVertifyCode($getCodeBtn);
    });
	
	$("#getPhoneCodea").click(function(){
        var photo=$("#phone").val();
		var $errorObj = $("#phoneSettingIndexForm .errorTip");
		if(verifyPhone(photo,$errorObj)){
			$.post(fa,{phone:photo},function(e){
				 if(e.code=="0"){
					 noticeCodeSendSuccess(e.msg, $(this), time);
				 }else{
					 parent.layer.msg(e.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
				 }
			});
		}
    });
	$("#submitPhoneSetIndexFormBtn").click(function(){

		$.post(edittel,{photo:$("#phone").val(),yzm:$("#phoneCodea").val()},function(e){
			 if(e.code==1){
				 parent.layer.msg(e.msg, {icon : 1,shade : [ 0.4, '#000' ],time : 2000});
			 }else{
				 parent.layer.msg(e.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
			 }
		});
	});

    //下一步
    $("#nextVerifyPhoneBtn").on('click', function(e) {
        var phoneCode = $.trim($("#phoneCode").val());//邮箱验证码

        var icon = "<i class='fa fa-times-circle'></i>";
        var $errorObj = $("#updatePhoneInitForm .errorTip");

        var verifyPhoneCodeResult = verifyCode(phoneCode, $errorObj);
        if(verifyPhoneCodeResult){
            // 已绑定手机-验证修改当前绑定手机号验证码
            vertifyModifyCurrentBindPhoneCode(icon,$errorObj,phoneCode);
        }else{
            e.preventDefault();
        }


    });
});

//ajax发送验证码
function ajaxSendVertifyCode($getCodeBtn, time){
	sendModifyCurrentBindPhoneVertifyCode($getCodeBtn, time);
    //modifyBindPhoneVertifyInit( $getCodeBtn, time);

}

/**
 * 已绑定手机-手机修改绑定验证初始化
 */
function modifyBindPhoneVertifyInit($getCodeBtn, time){
    $.ajax({
        url: regurl,
        data:{_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "000000")
            {
                //初始化成功，发送修改当前绑定手机号验证码
                sendModifyCurrentBindPhoneVertifyCode($getCodeBtn, time);
            }
            else
            {
                parent.layer.msg(data.message, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

/**
 * 已绑定手机-发送修改当前绑定手机号验证码
 */
function sendModifyCurrentBindPhoneVertifyCode($getCodeBtn, time){
    $.ajax({
        url: regurl,
        data:{_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "0")
            {
                noticeCodeSendSuccess(data.msg, $getCodeBtn, time);
            }
            else
            {
                parent.layer.msg(data.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}


/**
 * 已绑定手机-验证修改当前绑定手机号验证码
 */
function vertifyModifyCurrentBindPhoneCode(icon,$errorObj,phoneCode){
	$.post(addTel,{code:phoneCode},function(e){
		if(e.code=="0"){
			$("#updatePhoneInitForm").hide();
				$("#phoneSettingIndexForm").show();
		}else{
			$errorObj.html(icon + e.msg);
		}
	});
    
}

