/**
 * date： 2018-10-16 11:30:07
 * author： lxf
 * description: 卖卡记录-批量订单
 */
var commitTimeStart = {
    elem: "#timeStart",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        commitTimeEnd.min = datas;
        commitTimeEnd.start = datas;
        $("#commitStartDate").val(datas);
        $("#commitRegion").val("CUSTOM");
    }

};
var commitTimeEnd = {
    elem: "#timeEnd",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        commitTimeStart.max = datas;
        $("#commitEndDate").val(datas);
        $("#commitRegion").val("CUSTOM");
    }
};


function doTextKeyWordId()
{
	var textKeywordId=$("#textKeywordId").val();
    var re =  /^[0-9a-zA-Z_]*$/g;
    if (!re.test(textKeywordId))
    {
    	parent.layer.msg("关键字只能输入数字、字母或下划线", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
    }
}

$(document).ready(function () {
    laydate(commitTimeStart);
    laydate(commitTimeEnd);

    //时间按钮点击事件
    $("#queryForm .button-group button").click(function(e) {
        var parentId = $(this).parent().attr("id");
        $(this).removeClass("btn-default").addClass("btn-primary").siblings().removeClass("btn-primary").addClass("btn-default");

        if(parentId == "dateBtns"){
            $("#commitRegion").val($(this).attr("region"));
        }

        getOrderQueryDate(parentId, $(this).attr("region"));

    });

    //查询
    $("#search").click(function(){
    	var textKeywordId=$("#textKeywordId").val();
        var re =  /^[0-9a-zA-Z_]*$/g;
        if (!re.test(textKeywordId)) {
            parent.layer.msg("关键字只能输入数字、字母或下划线", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        }else{
            changePage(ctxPath, "queryForm", "queryPagination", "refreshSellCardRecordData", "sellCardRecordPageNumber", 1);
        }
    });

    //给enter绑定查询事件
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $('#search').click();
        }
    });

    //分页--每页显示多少条数据
    $("#queryPagination").on("click", ".dropdown-menu a", function(){
        var pageSize = $.trim($(this).html());
        $("#pageSize").val(pageSize);
        $("#queryPagination").find(".page-size").val(pageSize);
        changePage(ctxPath, "queryForm", "queryPagination", "refreshSellCardRecordData", "sellCardRecordPageNumber", 1);
    });

    //提交时间默认近7天改成默认今天
    $('#dateBtns button[region="TODAY"]').click();


    //获取表格数据
    changePage(ctxPath, "queryForm", "queryPagination", "refreshSellCardRecordData", "sellCardRecordPageNumber", 1);

    //导出记录
    /*$("#exportRecord").click(function(e) {
        exportRecord();
    });*/

    //只导出成功记录
    /*$("#exportSuccessRecord").click(function(e) {
        exportSuccessRecord();
    });*/
});

//显示表格数据
function refreshSellCardRecordData(data){
    // console.log("我是卖卡记录："+JSON.stringify(data));
    if(data != null) {
        var d = data.data;
        if(d.length > 0){
            var html = '';
            var actualAmountCurPage=0;
            var totalCountCurPage=0;
            var successCountCurPage=0;
            var failedCountCurPage=0;
            
            for(var i = 0; i < d.length; i++){
                var typeStr='';
                // 类型：1：单个订单 2：批量订单
                if(d[i].type=='1'){
                    typeStr='单个订单';
                }else{
                    typeStr='批量订单';
                }
                var state=parseInt(d[i].state);
                
                var successCount=0,failedCount=0;
                
                // 回收状态(1:处理中、2:回收成功、3:回收失败)
                var statusHtml='';
                
                if(d[i].type=='1')
                {
	                switch (state)
	                {
	                    case 0:
	                    	{
	                    		successCount=0;
	                    		failedCount=0;
	                    		statusHtml='<span class="text-dark-blue">正在处理</span>';
		                        break;
	                    	}
	                    case 1:
	                    	{	
	                    		successCount=1;
	                    		statusHtml='<span class="text-green">回收成功</span>';
	                    		break;
	                    	}
	                        
	                    case 2:
	                    	{
	                    		failedCount=1;
	                    		statusHtml='<span class="text-red">回收失败</span>';
	                    		break;
	                    	}
	                        
	                }
                }
                else
            	{
                	successCount=d[i].oknum;
                	failedCount=d[i].nonum;
                	switch (state)
	                {
	                    case 1:
	                    case 2:
	                        statusHtml='<span class="'+changeStatusClass(parseInt(failedCount))+'">处理完成</span>';
	                        break;
						default:
	                        statusHtml='<span class="text-dark-blue">正在处理</span>';
	                }
            	}

                var id="'"+d[i].id+"'";
                var type="'"+d[i].type+"'";
                html+='<tr>' +
                    '<td>'+d[i].create_time+'</td>' +
                    '<td>'+d[i].fenlei+'</td>' +
                    '<td>'+ d[i].operatorName+'</td>' +
                    '<td><span class="text-orange">￥'+numberFormatter(d[i].sm)+'</span></td>' +
                    '<td><span class="text-orange">￥'+numberFormatter(d[i].price)+'</span></td>' +
                    '<td><span class="text-dark-green">￥'+numberFormatter(d[i].money)+'</span></td>' +
                    '<td>'+d[i].cid+'张</td>' +
                    '<td><span class="text-dark-green">'+d[i].oknum+'张</span></td>' +
                    '<td><span class="text-dark-green">'+d[i].nonum+'张</span></td>' +
                    '<td>'+statusHtml+'</td>' +
                    '<td>'+typeStr+'</td>' +
                    '<td><a class="text-blue text-underline" href="javascript:viewDetail(\''+d[i].fenlei+'\');">查看详情</a></td>' +
                    '</tr>';

                actualAmountCurPage+=parseInt(d[i].money);
                totalCountCurPage+=d[i].cid;
                successCountCurPage+=successCount;
                failedCountCurPage+=failedCount;
            }
            $("#dataTable tbody.dataWraper").html(html);
            $("#queryPagination").show();//显示分页

            $("#dataTable").find("tfoot").show();//显示页脚
            $("#actualAmountCurPage").html("￥"+numberFormatter(actualAmountCurPage));
            $("#totalCountCurPage").html(totalCountCurPage+"张");
            $("#successCountCurPage").html(successCountCurPage+"张");
            $("#failedCountCurPage").html(failedCountCurPage+"张");
			
            $("#actualAmountTotal").html("￥"+numberFormatter(data.actualAmountTotal));
            $("#totalCountTotal").html(data.totalCountTotal+"张");
            $("#successCountTotal").html(data.successCountTotal+"张");
            $("#failedCountTotal").html(data.failedCountTotal+"张");
           
            
        } else {
            dataEmpty($("#dataTable"), "queryPagination");
        }
    } else {
        dataEmpty($("#dataTable"), "queryPagination");
    }
}



function doproductClassifyId()
{
	var productClassifyId=$("#productClassifyId").val();
	$.ajax({
        url: "/ClassifyId.html?productClassifyId="+productClassifyId,
        dataType: "json",
        type: "get",
        success: function(d) {
        	$("#operatorId option").remove();
        	$("#operatorId").append("<option value='-1' selected='selected'>卡种(全部)</option>");
        	var data=d.data;
        	for(i=0;i<data.length;i++)
        	{
        		$("#operatorId").append("<option value='"+data[i].type+"'>"+data[i].name+"</option>");
        	}
        },
        error: function(e) {
            layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        }
    });
}


function getOrderQueryDate(parentId, region) {
    var AddDayCount = '';
    if(region == "LASTDAY"){
        AddDayCount = -1;
    }else if(region == "TODAY"){
        AddDayCount = 0;
    }else if(region == "WEEK"){
        AddDayCount = -7;
    }else if(region == "1"){
        AddDayCount = -31;
    }else if(region == "3"){
        AddDayCount = -92;
    }else if(region == "NONE"){
        AddDayCount = '';
    }

    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    var year = dd.getFullYear(); //获取当前年，四位数
    var month = dd.getMonth()+1;//获取当前月
    var day = dd.getDate();  //获取当前日
    var dateStartPart = year + "-";
    var dateEndPart = '';

    if(region == "LASTDAY"){
        dateEndPart = year + "-";
        if(month < 10)
            dateEndPart += "0";
        dateEndPart += month + "-";
        if(day < 10)
            dateEndPart += "0";
        dateEndPart += day;

    }else{
        var mydate = new Date();
        var curyear = mydate.getFullYear();
        var curmonth = mydate.getMonth()+1;//获取当前月
        var curday = mydate.getDate();//获取当前月
        dateEndPart = curyear + "-";
        if(curmonth < 10)
            dateEndPart += "0";
        dateEndPart += curmonth + "-";
        if(curday < 10)
            dateEndPart += "0";
        dateEndPart += curday;
    }

    if(month < 10)
        dateStartPart += "0";
    dateStartPart += month + "-";
    if(day < 10)
        dateStartPart += "0";
    dateStartPart += day;
    var fullStartDate = dateStartPart + " " + "00" + ":" + "00" + ":" + "00";
    var fullEndDate = dateEndPart + " " + "23" + ":" + "59" + ":" + "59";

    if(region == "NONE"){
        if(parentId == "dateBtns"){
            $("#timeStart").val("");
            $("#timeEnd").val("");

            $("#commitStartDate").val("");
            $("#commitEndDate").val("");
        }
    }else{
        if(parentId == "dateBtns"){
            $("#timeStart").val(fullStartDate);
            $("#timeEnd").val(fullEndDate);

            $("#commitStartDate").val(fullStartDate);
            $("#commitEndDate").val(fullEndDate);
        }
    }

}


/**
 * 说明：类型为1的时候ID代表普通订单, 类型为2的时候ID代表批量订单ID
 * @param type
 * @param id
 */
function viewDetail(id){
    addFrame( '/getSellCardOrdersDetails.html?type=1&id='+id, '010000', '卖卡记录详情');
}

/**
 * 关键字查询
 * @param val
 * @param idWrap: 关键字输入框的ID
 */
function selectDdlType(val, idWarp){
    var item = val;
    $("#"+idWarp).attr("name",item);
}


/*function noExportDataTip(tipTxt) {
    parent.layer.alert('<span style="color:#f39c13;">'+ tipTxt +'</span>', {
        title: "提示",
        icon: 0,
        skin: 'layer-ext-moon',
        btn: '我知道了',
        yes: function(index, layero){
            //按钮的回调
            parent.layer.close(index);
        }
    });
}*/

/*function exportRecord(){
    var thLength = $(".dataWraper").find("tr:not(.empty):not(.loading)").length;
    if(thLength > 0){
        alert("导出记录");
    }else{
        noExportDataTip("暂无相关记录可导出");
    }
}*/

/*function exportSuccessRecord(){
    var thLength = $(".dataWraper").find("tr:not(.empty):not(.loading)").length;
    if(thLength > 0){
        alert("只导出成功记录");
    }else{
        noExportDataTip("暂无相关记录可导出");
    }
}*/

//批量订单——处理状态_处理成功状态颜色区分
function changeStatusClass(failedCount){
    var str;
    if( failedCount > 0){
        str="text-red";
    }else{
        str="text-green";
    }
    return str;
}

function doHistory() {
    window.location.href=ctxPath.domain+"/member/sellCardRecordHistory/batchCardIndex.do";
}