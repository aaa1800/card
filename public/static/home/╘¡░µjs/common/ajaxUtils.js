/****
 * ajax请求提交,返回提交状态
 * Author 邹树春
 */
function ajax_doSubmit(ajax_url,ajax_data_or_type,basebackFun,beforeFn)
{
	//需要提交到服务器的数据待转点
	var ajax_data="_csrf="+ctxPath.csrf;
	
	//获取发送到服务器的数据的取值方式
	var ajax_D_type = ajax_data_or_type.substring(0,ajax_data_or_type.indexOf(":"));
	//获取数据或者表单的Id
	var ajax_D_data_or_id = ajax_data_or_type.substring(ajax_data_or_type.indexOf(":")+1,ajax_data_or_type.length);
	
	//判断是通过表单id序列号数据，还是直接传入数据
	if(ajax_D_type=="form")
	{
		ajax_data =ajax_data+"&"+$("#"+ajax_D_data_or_id).serialize();		//序列化需要提交数据的表单
	}
	if(ajax_D_type=="data")	
	{
		if(ajax_D_data_or_id!="")
		{
			ajax_data=ajax_data+"&"+ajax_D_data_or_id;
		}
	}
	
	$.ajax(
		{
			type:"post",
			url:ctxPath.domainapi+ajax_url,
			xhrFields: {
	             withCredentials: true
	        },
	        crossDomain: true,
			dataType:"json",
			data:ajax_data,
			beforeSend:function (XMLHttpRequest)
			{		
            	if(typeof(beforeFn)=="function")
            		beforeFn();
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) { 
				var ostatus = XMLHttpRequest.status;
				var readyState = XMLHttpRequest.readyState;
				console.log(ostatus, textStatus, errorThrown);
				if(ostatus==403){
					window.location.href = ctxPath.domain+"/passport/login/index.html";
				}else if(ostatus==404){
					layerInfoTip("网络异常，请检查网络或者重新刷新页面！" + " textStatus:" + textStatus + " errorThrown:" + errorThrown + " readyState:" + readyState);
				}else{
					//请求失败要做的事情
					layerInfoTip("请求失败,请稍后再试！");
				}
				
            },
            complete:function (XMLHttpRequest,textStatus)
            {
            	//请求完成要做的事情
            },
			success: function (data,textStatus) {
				//请求成功要做的事情
            	if(typeof(basebackFun)=="function")
            		basebackFun(data);
            }
		}
	);
}

function ajax_doSubmitGet(ajax_url,ajax_data_or_type,basebackFun)
{
	//需要提交到服务器的数据待转点
	var ajax_data="_csrf="+ctxPath.csrf;
	
	//获取发送到服务器的数据的取值方式
	var ajax_D_type = ajax_data_or_type.substring(0,ajax_data_or_type.indexOf(":"));
	//获取数据或者表单的Id
	var ajax_D_data_or_id = ajax_data_or_type.substring(ajax_data_or_type.indexOf(":")+1,ajax_data_or_type.length);
	
	//判断是通过表单id序列号数据，还是直接传入数据
	if(ajax_D_type=="form")
	{
		ajax_data =ajax_data+"&"+$("#"+ajax_D_data_or_id).serialize();		//序列化需要提交数据的表单
	}
	if(ajax_D_type=="data")	
	{
		if(ajax_D_data_or_id!="")
		{
			ajax_data=ajax_data+"&"+ajax_D_data_or_id;
		}
	}
	$.ajax(
		{
			type:"get",
			url:ctxPath.domainapi+ajax_url,
			xhrFields: {
	             withCredentials: true
	        },
	　　　　// 允许跨域
	        crossDomain: true,
			dataType:"json",
			data:ajax_data,
			beforeSend:function (XMLHttpRequest)
			{
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) { 
				//请求失败要做的事情

            },
            complete:function (XMLHttpRequest,textStatus)
            {
            	//请求完成要做的事情
            },
			success: function (data,textStatus) {
				//请求成功要做的事情
            	if(typeof(basebackFun)=="function")
            		basebackFun(data);
            }
		}
	);
}

function doSubstr(str)
{
	return str.substr(0,10);
}

//日期格式化
function format(time, format){
    var t = new Date(time);
    var tf = function(i){return (i < 10 ? '0' : '') + i};
    return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a){
        switch(a){
            case 'yyyy':
                return tf(t.getFullYear());
                break;
            case 'MM':
                return tf(t.getMonth() + 1);
                break;
            case 'mm':
                return tf(t.getMinutes());
                break;
            case 'dd':
                return tf(t.getDate());
                break;
            case 'HH':
                return tf(t.getHours());
                break;
            case 'ss':
                return tf(t.getSeconds());
                break;
        }
    })
};