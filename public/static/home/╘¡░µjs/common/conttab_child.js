/**
 * Created by lxf on 2016/11/2.
 */
$(function(){
    $("body").bind("keydown",function(event){
        if (event.keyCode == 116) {
            event.preventDefault(); //阻止默认刷新
            //location.reload();
            //采用location.reload()在火狐下可能会有问题，火狐会保留上一次链接
            location=location;
        }
    })
});

function addFrame(str,index,title){
    var o =str, m = index, l = title, k = true;
    if (o == undefined || $.trim(o).length == 0) {
        return false
    }

    // frame页面已存在，显示并刷新
    $('.J_mainContent .J_iframe',parent.document).each(function () {
        if ($(this).data('id') == o) {
            $(this).attr("data-id", o);
            $(this).attr("src", o);
            $(this).show().siblings('.J_iframe').hide();

            k = false;
            return false;
        }
    });
    //给左侧导航添加样式
    $('#side-menu .J_menuItem',parent.document).each(function () {
        if ($(this).attr('href') == o) {
            $(this).parent("li").addClass("active").siblings().removeClass("active");
        }
    });

    // frame页面不存在
    if (k) {
        // 添加对应的iframe
        var n = '<iframe class="J_iframe" name="iframe' + m + '" width="100%" height="100%" src="' + o + '" frameborder="0" data-id="' + o + '" seamless></iframe>';
        $(".J_mainContent",parent.document).find("iframe.J_iframe").hide().parents(".J_mainContent").append(n);

        //给左侧导航添加样式
        $('#side-menu .J_menuItem',parent.document).each(function () {
            if ($(this).attr('href') == o) {
                $(this).parent("li").addClass("active").siblings().removeClass("active");
            }
        });
    }
    return false
}
function g(n) {
    var o = f($(n).prevAll()), q = f($(n).nextAll());
    var l = f($(".content-tabs",parent.document).children().not(".J_menuTabs",parent.document));
    var k = $(".content-tabs",parent.document).outerWidth(true) - l;
    var p = 0;
    if ($(".page-tabs-content",parent.document).outerWidth() < k) {
        p = 0
    } else {
        if (q <= (k - $(n).outerWidth(true) - $(n).next().outerWidth(true))) {
            if ((k - $(n).next().outerWidth(true)) > q) {
                p = o;
                var m = n;
                while ((p - $(m).outerWidth()) > ($(".page-tabs-content",parent.document).outerWidth() - k)) {
                    p -= $(m).prev().outerWidth();
                    m = $(m).prev()
                }
            }
        } else {
            if (o > (k - $(n).outerWidth(true) - $(n).prev().outerWidth(true))) {
                p = o - $(n).prev().outerWidth(true)
            }
        }
    }
    $(".page-tabs-content",parent.document).animate({marginLeft: 0 - p + "px"}, "fast")
}

function f(l) {
    var k = 0;
    $(l).each(function () {
        k += $(this).outerWidth(true)
    });
    return k
}

//关闭frame
function closeFrame(str){
    var o = str;
    if (o == undefined || $.trim(o).length == 0) {
        return false
    }

    // 移除tab对应的内容区
    $('.J_mainContent .J_iframe',parent.document).each(function () {
        if ($(this).data('id') == o) {
            $(this).remove();
            return false;
        }
    });

}

//显示frame
function showFrame(str){
    var o = str;
    if (o == undefined || $.trim(o).length == 0) {
        return false
    }

    $('.J_mainContent .J_iframe',parent.document).each(function () {
        if ($(this).data('id') == o) {
            $(this).show();
        }
    });
}