/**
 * 公共的js
 */


/**
 * 日期格式化
 * @param time
 * @param format
 * @returns {*}
 */
function formatDate(time, format) {
    if(time!=null){
        var t = new Date(time);
        var tf = function(i){return (i < 10 ? '0' : '') + i};
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a){
            switch(a){
                case 'yyyy':
                    return tf(t.getFullYear());
                    break;
                case 'MM':
                    return tf(t.getMonth() + 1);
                    break;
                case 'mm':
                    return tf(t.getMinutes());
                    break;
                case 'dd':
                    return tf(t.getDate());
                    break;
                case 'HH':
                    return tf(t.getHours());
                    break;
                case 'ss':
                    return tf(t.getSeconds());
                    break;
            }
        })
    }else{
        return '';
    }
}


/**
 * 数字千位符格式化
 * @param num
 * @returns {string}
 */
function numberFormatter(num) {
    return (parseFloat(num).toFixed(2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}

/**
 *
 * @param str
 * @returns {string}
 */
function isEqUndefined(str) {
    return str==undefined?"":str;
}

function compareTime(date1, date2){
    if (date1 > date2) {
        return false;
    }else{
        return true;
    }
}