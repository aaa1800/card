/*!
  * author：lxf
  * date: 2018-5-9 16:32:59
  * description: 邀请管理
  */

var searchTimeStart = {
    elem: "#timeStart",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        searchTimeEnd.min = datas;
        searchTimeEnd.start = datas;
        $("#startDate").val(datas);
        $("#timeRegion").val("CUSTOM");
    }

};
var searchTimeEnd = {
    elem: "#timeEnd",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        searchTimeStart.max = datas;
        $("#endDate").val(datas);
        $("#timeRegion").val("CUSTOM");
    }
};
$(function(){
    laydate(searchTimeStart);
    laydate(searchTimeEnd);

    //时间按钮点击事件
    $("#dateBtns button").click(function(e) {
        var parentId = $(this).parent().attr("id");
        $(this).removeClass("btn-default").addClass("btn-primary").siblings().removeClass("btn-primary").addClass("btn-default");

        $("#timeRegion").val($(this).attr("region"));

        getOrderQueryDate(parentId, $(this).attr("region"));

    });

    //查询
    $("#search").click(function(){
        changePage(ctxPath, "queryForm", "queryPagination", "refreshSuborinateRecordsData", "suborinateRecordsPageNumber", 1);
    });

    //给enter绑定查询事件
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $('#search').click();
        }
    });

    //分页--每页显示多少条数据
    $("#queryPagination").on("click", ".dropdown-menu a", function(){
        var pageSize = $.trim($(this).html());
        $("#pageSize").val(pageSize);
        $("#queryPagination").find(".page-size").val(pageSize);
        changePage(ctxPath, "queryForm", "queryPagination", "refreshSuborinateRecordsData", "suborinateRecordsPageNumber", 1);
    });

    //时间查询默认今天
    $('#dateBtns button[region="NONE"]').click();

    //排序功能
    $("#dataTable .m_sort").click(function(){
        var cursort = $(this).attr("cursort");//当前排序状态
        var hidenId = $(this).attr("sorthideid");//排序隐藏域id

        $("#queryForm .sortHideInpt").val("");
        $("#dataTable .m_sort").attr("class", "m_sort sorting").attr("cursort", "sorting");

        if(cursort == "sorting" || cursort == "sorting_desc"){
            $("#"+hidenId).val("1");
            $(this).attr("cursort", "sorting_asc");
            $(this).removeClass("sorting sorting_desc").addClass("sorting_asc");
        }else if(cursort == "sorting_asc"){
            $("#"+hidenId).val("2");
            $(this).attr("cursort", "sorting_desc");
            $(this).removeClass("sorting sorting_asc").addClass("sorting_desc");
        }

        changePage(ctxPath, "queryForm", "queryPagination", "refreshSuborinateRecordsData", "suborinateRecordsPageNumber", 1);
    });

    //获取表格数据
    changePage(ctxPath, "queryForm", "queryPagination", "refreshSuborinateRecordsData", "suborinateRecordsPageNumber", 1);

    //复制分享
    $("#btnCopy").click(function(){
        var InviteLink = $("#InviteLink").val();
        try {
            window.clipboardData.setData('Text', InviteLink);
            layer.msg("复制分享信息成功！", {icon : 1,shade : [ 0.4, '#000' ],time : 2000});
        } catch(e) {
            layer.msg("复制失败可能浏览器内核不支持，请手动复制", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        }
    });

});

//显示表格数据
function refreshSuborinateRecordsData(data){
    console.log("下级用户列表："+JSON.stringify(data));
    if(data != null) {
        var d = data.data;
        if(d.length > 0){
            var html = '';
            var totalPriceCurPage=0;
            var commisionCurPage=0;
            for(var i = 0; i < d.length; i++){
                var typeStr = "";
                if(d[i].type == "0"){
                    typeStr = "未生效下级";
                }else if(d[i].type == "1"){
                    typeStr = "有效下级";
                }
                html+='<tr>' +
                    '<td>'+d[i].regTime+'</td>' +
                    '<td>'+d[i].subordinateMemberNumber+'</td>' +
                    '<td>'+typeStr+'</td>' +
                    '<td class="text-orange">￥'+numberFormatter(d[i].totalPrice)+'</td>' +
                    '<td class="text-green">￥'+numberFormatter(d[i].totalCommision)+'</td>' +
                    '</tr>';

                totalPriceCurPage+=d[i].totalPrice;
                commisionCurPage+=d[i].totalCommision;
            }
            $("#dataTable tbody.dataWraper").html(html);
            $("#queryPagination").show();//显示分页

            $("#dataTable").find("tfoot").show();//显示页脚
            $("#totalPriceCurPage").html("￥"+numberFormatter(totalPriceCurPage));
            $("#commisionCurPage").html("￥"+numberFormatter(commisionCurPage));

            $("#totalPriceTotal").html("￥"+numberFormatter(data.totalPrice));
            $("#commisionTotal").html("￥"+numberFormatter(data.totalCommision));

        } else {
            dataEmpty($("#dataTable"), "queryPagination");
        }
    } else {
        dataEmpty($("#dataTable"), "queryPagination");
    }
}

function getOrderQueryDate(parentId, region) {
    var AddDayCount = '';
    if(region == "LASTDAY"){
        AddDayCount = -1;
    }else if(region == "TODAY"){
        AddDayCount = 0;
    }else if(region == "WEEK"){
        AddDayCount = -7;
    }else if(region == "1"){
        AddDayCount = -31;
    }else if(region == "3"){
        AddDayCount = -92;
    }else if(region == "NONE"){
        AddDayCount = '';
    }

    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    var year = dd.getFullYear(); //获取当前年，四位数
    var month = dd.getMonth()+1;//获取当前月
    var day = dd.getDate();  //获取当前日
    var dateStartPart = year + "-";
    var dateEndPart = '';

    if(region == "LASTDAY"){
        dateEndPart = year + "-";
        if(month < 10)
            dateEndPart += "0";
        dateEndPart += month + "-";
        if(day < 10)
            dateEndPart += "0";
        dateEndPart += day;

    }else{
        var mydate = new Date();
        var curyear = mydate.getFullYear();
        var curmonth = mydate.getMonth()+1;//获取当前月
        var curday = mydate.getDate();//获取当前月
        dateEndPart = curyear + "-";
        if(curmonth < 10)
            dateEndPart += "0";
        dateEndPart += curmonth + "-";
        if(curday < 10)
            dateEndPart += "0";
        dateEndPart += curday;
    }

    if(month < 10)
        dateStartPart += "0";
    dateStartPart += month + "-";
    if(day < 10)
        dateStartPart += "0";
    dateStartPart += day;
    var fullStartDate = dateStartPart + " " + "00" + ":" + "00" + ":" + "00";
    var fullEndDate = dateEndPart + " " + "23" + ":" + "59" + ":" + "59";

    if(region == "NONE"){
        if(parentId == "dateBtns"){
            $("#timeStart").val("");
            $("#timeEnd").val("");

            $("#startDate").val("");
            $("#endDate").val("");
        }
    }else{
        if(parentId == "dateBtns"){
            $("#timeStart").val(fullStartDate);
            $("#timeEnd").val(fullEndDate);

            $("#startDate").val(fullStartDate);
            $("#endDate").val(fullEndDate);
        }
    }

}