//微信账号初始化
$(function(){
	$.ajax({
        url: ctxPath,
        data:{_csrf:ctxPath._csrf},
        dataType: "json",
        type:'post',
        xhrFields: {
            withCredentials: true
       },
　　　	// 允许跨域
       	crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
//        	 console.log("微信账号初始化",data)
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
	
});



//添加微信账号页面
function gotoFollowWeiXinAccount(){
	parent.layer.alert('', {
		type: 2,
		title: "添加提现使用的微信帐号",
		area: ['550px', '442px'],
		scrollbar: false, //是否允许浏览器出现滚动条
		content: ctxPath,
		success: function(){}
	});
}
function deleteid(obj,id){
        deleteAliPayAccount(id);
};

//微信账号列表页面删除账号
function deleteAliPayAccount(id){
  $('#sureTradePwdModal').modal('show');
  $("#delId").val(id);//给弹出层的delId赋值
  console.log(id);
}


function sureDelSeltedAliPayAccount(){
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    
    var tradePassword = $.trim($("#tradePassword").val());
   
    var icon = "<i class='fa fa-times-circle'></i>";
    var $errorObj = $("#sureTradePwdModal .errorTip");
//    console.log(isOpenTradePaaword,tradePassword);
    if(isOpenTradePaaword == ""){
        $errorObj.html(icon+" 您未开启交易密码，删除微信账号需要验证交易密码");
    }else{
        if(tradePassword == ""){
            $errorObj.html(icon+" 请输入交易密码");
        }else{
            var id=$("#delId").val();
            $errorObj.html("");
            // 删除用户绑定的支付宝账号
            deleteMemberWeixinAccount(icon,$errorObj,id,tradePassword);

        }
    }
}

/**
 * 删除用户绑定的微信账号
 * @param icon
 * @param $errorObj
 * @param id
 * @param tradePassword
 */
function deleteMemberWeixinAccount(icon,$errorObj,id,tradePassword){
	$.post(shanc,{id:id,tradePassword:tradePassword},function(data){
		  if(data.code == 1)
            {
                //删除成功，刷新页面
                window.location.reload();
            }
            else{
                $errorObj.html(icon+data.msg);
            }
	})
    
}

//模态框--忘记/设置交易密码
function gotoTradePwdPageFromModel(){
  addFrame( jiyi,"0110040","交易密码");
  $('#sureTradePwdModal').modal('hide');
}


