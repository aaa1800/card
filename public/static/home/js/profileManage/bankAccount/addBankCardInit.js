$(function(){
	
    //立即添加
    $("#submitAddBankCardFormBtn").on("click", function(){
        verifyAddBankCardForm();
    });
});
function getQueryString(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r!=null) return r[2]; return '';
}


/**
 * 银行卡号：1、是否输入  2、格式是否正确
 * 交易密码：1、是否开启  2、是否输入
 */
function verifyAddBankCardForm(){
    var cardNumber = $.trim($("#cardNumber").val());
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    var tradePassword = $.trim($("#tradePassword").val());
    var bankCardReg = /^[0-9]{12,19}$/;
    //var bankCardReg = /^([1-9]{1})(\\d{11}|\\d{12}|\\d{13}|\\d{14}|\\d{15}|\\d{16}|\\d{17}|\\d{18})$/;
    var icon = "<i class='fa fa-times-circle'></i>";
    var $errorObj = $("#addBankCardForm .errorTip");
    if(cardNumber == ""){
        $errorObj.html(icon+" 请输入银行卡号");
    }else{
        if(bankCardReg.test(cardNumber)){
            if(isOpenTradePaaword == ""){
                $errorObj.html(icon+" 您未开启交易密码，开启交易密码才可添加银行卡");
            }else{
                if(tradePassword == ""){
                    $errorObj.html(icon+" 请输入交易密码");
                }else{
                    $errorObj.html("");
                    saveBankCard(icon,$errorObj);
                }
            }
        }else{
            $errorObj.html(icon+" 银行卡号格式不正确");
        }
    }
}



/**
 * 保存银行卡
 */
function saveBankCard(icon,$errorObj){
    $.ajax({
        url:$("#addBankCardForm").attr("action"),
        data:$("#addBankCardForm").serialize(),
        dataType: "json",
        async: true,
        type: "POST",
        beforeSend: function() {},
        success: function(data) {
            if(data.code == 1)
            {
                //添加成功，跳转到银行账号首页
                addFrame( banklist,"011003","银行账号");

                var winindex = parent.layer.getFrameIndex(window.name); //获取窗口索引
                parent.layer.close(winindex);

            }
            else{
                $errorObj.html(icon+data.msg);
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

//设置交易密码
function gotoSetTradePwdFn(){

    addFrame( jiyi,"0110040","交易密码");

    var winindex = parent.layer.getFrameIndex(window.name); //获取窗口索引
    parent.layer.close(winindex);

}

//忘记交易密码
function forgetTradePwdFn(){
    addFrame( jiyi,"0110040","交易密码");

    var winindex = parent.layer.getFrameIndex(window.name); //获取窗口索引
    parent.layer.close(winindex);
}