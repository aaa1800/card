/**
 * 实名认证表单提交
 */

//时间引用
function time_icon1(){
	!function(){
		laydate({elem: '#some_class_1'});//绑定元素
	}();
	
}
function time_icon2(){
	!function(){
		laydate({elem: '#some_class_2'});//绑定元素
	}();
}

//正面
function zhengmian(){
	$('#img_z').click();
}

function getzImg(imgFile){
	var file = imgFile.files[0];
	var reader = new FileReader();
//	var png = file.type === 'image/png';
//	var jpg = file.type === 'image/jpg';
//	var jpeg = file.type === 'image/jpeg';
//	
//	if(!png && !jpg && jpeg){
//		alert("只能上传png,jpg,jpeg的格式");
//	}
//	else{
		if(file.size/1024 > 1024*2){//大于2M，进行压缩上传
//          console.log("大于2M，进行压缩上传");
         photoCompress(file, {
             quality: 0.2
         }, function(base64Codes){
        	 	console.log("压缩后：" + base64Codes.length / 1024 +"KB");
	            $("#zmz").attr("src",base64Codes);
	            $(".one .right-icon").css("display","block");
	      		$(".one").css("background-color","#f5f5f5");
	      		$(".one a").css("display","block");
	         });
		}
		else{
			//小于等于2M 原图上传
//			console.log("小于等于2M 原图上传");
			reader.readAsDataURL(file);
			reader.onload = function(e){
	      		$("#zmz").attr("src",e.target.result);
	      		$(".one .right-icon").css("display","block");
	      		$(".one").css("background-color","#f5f5f5");
	      		$(".one a").css("display","block");
			}
		}
//	}
}  
		
//反面
function fanmian(){
	$('#img_f').click();
}

function getfImg(imgFile){
	var file = imgFile.files[0];
	var reader = new FileReader();
//	var png = file.type === 'image/png';
//	var jpg = file.type === 'image/jpg';
//	var jpeg = file.type === 'image/jpeg';
	
//	if(!png && !jpg && jpeg){
//		alert("只能上传png,jpg,jpeg的格式");
//	}
//	else{
		if(file.size/1024 > 1024*2){//大于2M，进行压缩上传
//          console.log("大于2M，进行压缩上传");
         photoCompress(file, {
             quality: 0.2
         }, function(base64Codes){
//        	 	console.log("压缩后：" + base64Codes.length / 1024 +"KB");
	            $("#fmz").attr("src",base64Codes);
	            $(".two .right-icon").css("display","block");
			  	$(".two").css("background-color","#f5f5f5");
			  	$(".two a").css("display","block");
	         });
		}
		else{//小于等于2M 原图上传
//			console.log("小于等于2M 原图上传");
			reader.readAsDataURL(file);
			reader.onload = function(e){
			  	$("#fmz").attr("src",e.target.result);
			  	$(".two .right-icon").css("display","block");
			  	$(".two").css("background-color","#f5f5f5");
			  	$(".two a").css("display","block");
			}
		}
//	}
}
		//手持照
function shoucmian(){
	$('#img_s').click();
	}
function getsImg(imgFile){

	var file = imgFile.files[0];
	var reader = new FileReader();
//	var png = file.type === 'image/png';
//	var jpg = file.type === 'image/jpg';
//	var jpeg = file.type === 'image/jpeg';
	
//	if(!png && !jpg && jpeg){
//		alert("只能上传png,jpg,jpeg的格式");
//	}
//	else{
		if(file.size/1024 > 1024*2){//大于2M，进行压缩上传
//          console.log("大于2M，进行压缩上传");
         photoCompress(file, {
             quality: 0.2
         }, function(base64Codes){
//        	 	console.log("压缩后：" + base64Codes.length / 1024 +"KB");
	            $("#scz").attr("src",base64Codes);
	            $(".three .right-icon").css("display","block");
				$(".three").css("background-color","#f5f5f5");
				$(".three a").css("display","block");
	         });
		}
		else{//小于等于2M 原图上传
//			console.log("小于等于2M 原图上传");
			reader.readAsDataURL(file);
			reader.onload = function(e){
				$("#scz").attr("src",e.target.result);
				$(".three .right-icon").css("display","block");
				$(".three").css("background-color","#f5f5f5");
				$(".three a").css("display","block");
			}
		}
//	}
	
} 

         
    


//提交按钮
function checkForm(){
	if($('#idname').val() == null || $('#idname').val() == ''){
		$(".timea-error").css('display','block');
		return false;
	}else{
		$(".timea-error").css('display','none');
	}
	//	请选择颁发日期！
	if($('#idnumber').val() == null || $('#idnumber').val() == ''){
		$(".timeb-error").css('display','block');
		return false;
	}else{
		$(".timeb-error").css('display','none');
	}
	//	请选择到期日期！
	if($('#some_class_2').val() == null || $('#some_class_2').val() == ''){
		$(".timeE-error").css('display','block');
		return false;
	}else{
		$(".timeE-error").css('display','none');
	}
	//	上传身份证正面
	var faceImgFile=$('#img_z').val();
	if(faceImgFile==null||faceImgFile==''){
		$(".zmz-error").css('display','block');
		return false;
	}else{
		$(".zmz-error").css('display','none');
	}
	var backImgFile=$('#img_f').val();
	//	上传身份证反面
	if(backImgFile==null||backImgFile==''){
		$(".fmz-error").css('display','block');
		return false;
	}else{
		$(".fmz-error").css('display','none');
	}
	var handImgFile=$('#img_s').val();
	//	手持身份证照
	if(handImgFile==null||handImgFile==''){
		$(".sczm-error").css('display','block');
		return false;
	}else{
		$(".sczm-error").css('display','none');
	}
	intermediateAuthen();
}

function intermediateAuthen(){
	var formData = new FormData($('#intermediateAuthenForm')[0]);   
	/*var imgArr = $(".xs img");
	var newImgArr =  ['faceImgFile', 'backImgFile', 'handImgFile'];
	
	for(var i=0; i<imgArr.length; i++){
		var base64Url = imgArr[i].src;
		var bl = convertBase64UrlToBlob(base64Url);
		formData.append(newImgArr[i], bl, "file_"+Date.parse(new Date())+".jpg"); // 文件对象
	} */
	$.ajax({
		url:ctxPath,
        data:formData,
        dataType: "json",
        xhrFields: {
            withCredentials: true
		},
		crossDomain: true,
        async: true,
        type: "POST",
        enctype:"multipart/form-data",
        processData: false ,
        contentType : false,
        beforeSend: function() {},
        success: function(data) {
        	if(data.code == 1)
    		{
        		window.location.reload();
    		}
        	else
    		{
        		layer.msg(data.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 3000});
    		}
	    },
	    complete: function() {},
	    error: function(XMLHttpRequest, textStatus, errorThrown) {
	    	layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 4000});
	    }
    });
		
}