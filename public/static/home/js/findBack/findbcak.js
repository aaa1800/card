
$(function(){
	var oFindBackError = $('#FindBackError');
	var oFindBackName = $("#FindBackName");
	var oFindBackCode = $("#FindBackCode");
	var oFindBackPass = $("#FindBackPass");
	var oFindBackReinput = $("#FindBackReinput");
	var oFindBackBtn = $("#FindBackBtn");
	var oFindCodeBtn = $("#FindCodeBtn");
	var oFindBackGoBtn = $("#FindBackGoBtn");
	var validCode = true;
	var oIphone = function(B) {
        var A = /^1\d{10}$/;
        if (!A.exec(B)) {
            return false
        } else {
            return true
        }
    };
    var oMailbox = function(A) {
        var B = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (B.test(A)) {
            return true
        }
        return false
    };
    var oPassWork = function(B) {
        var A = /^(?![A-z]+$)(?!\d+$)(?![\W_]+$)^.{6,20}$/;
        if (!A.exec(B)) {
            return false
        }
        return true
    };
	/**
	* @description: 点击验证
	*/
	oFindBackBtn.click(function(event) {
		var oFindBackNameVal = $.trim(oFindBackName.val());
		var oFindBackCodeVal = $.trim(oFindBackCode.val());
		var rego = /^.{6}$/;
		//var codeNum = 12345;
		if(oFindBackNameVal === ''){
			oFindBackName.focus();
			fnLoginError('请输入手机号或邮箱！');
			return false;
		}else if(!oIphone(oFindBackNameVal) && !oMailbox(oFindBackNameVal)){
			oFindBackName.focus();
			fnLoginError('手机号或者邮箱不正确！');
			return false;
		}else if(oFindBackCodeVal === ''){
			oFindBackCode.focus();
			fnLoginError('请输入验证码！');
			return false;
		}else if(!rego.test(oFindBackCodeVal)){
			oFindBackCode.focus();
			fnLoginError('验证码为6位！');
			return false;
		}else{
			$.post(premcode,$("#findbackForm").serialize(),function(data){
				if(data.code==1){
					$("#findbackForm").hide();
					$("#editForm").show();
				}else{
					layer.msg(data.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 1000});
				}
			})
			
		}
	});

	//点击去掉文本框的内容
	$(".login-mainly .inputIcon-close").click(function(event) {
		var oCloseVal = $(this).parent().find('input');
		if(oCloseVal.val() === ''){
			$(this).hide();
		}else{
			$(this).hide();
			oCloseVal.val('');
		}
	});
	$(".login-mainly input").click(function(event) {
    	$(this).parent().addClass('border-blue');
    }).focus(function(event) {
    	$(this).parent().addClass('border-blue');
    }).keyup(function(event) {
    	if($(this).val() === ''){
			$(this).parent().find('.inputIcon-close').hide();
		}else{
			$(this).parent().find('.inputIcon-close').show();
		}
    }).blur(function(event) {
    	$(this).parent().removeClass('border-blue');
    });
    /**
	* @description: 验证密码
	*/
	oFindBackGoBtn.click(function(event) {
		var oFindBackPassVal  = $.trim(oFindBackPass.val());
		var oFindBackReinputVal  = $.trim(oFindBackReinput.val());

		if(oFindBackPassVal === ''){
			oFindBackPass.focus();
			fnLoginError('请输入密码！');
			return false;
		}else if(!oPassWork(oFindBackPassVal)){
			oFindBackPass.focus();
			fnLoginError('密码由6-20位字母、数字或者符号两种或以上组成!');
			return false;
		}else if(oFindBackReinputVal === ''){
			oFindBackReinput.focus();
			fnLoginError('请重新输入一次密码！');
			return false;
		}else if(!oPassWork(oFindBackReinputVal)){
			oFindBackReinput.focus();
			fnLoginError('密码由6-20位字母、数字或者符号两种或以上组成!');
			return false;
		}else if(!(oFindBackPassVal === oFindBackReinputVal)){
			oFindBackReinput.focus();
			fnLoginError('两次密码输入不一致，请重新输入!');
			return false;
		}else{	
			$.post(setpass,$("#editForm").serialize(),function(data){
				if(data.code==1){
					layer.msg(data.msg, {icon: 1,shade: [0.4, '#000'],time : 1000},function(){
						location.href=shou;
					});
				}else{
					layer.msg(data.msg, {icon: 2,shade: [0.4, '#000'],time : 1000});
				}
			});
			
		}

	});
	/**
	* @description: 输入去掉提示
	*/
	oFindBackPass.keyup(function(event) {
		if($(this).val() === ''){
    		$(".register-cue-box").show();
    	}else{
    		$(".register-cue-box").hide();
    	}
	});
	/**
	* @description: 发送验证码
	*/
	oFindCodeBtn.click(function(event) {
		var oFindBackNameVal = $.trim(oFindBackName.val());
		if(oFindBackNameVal === ''){
			layer.msg('请填写手机或者邮箱！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else if(!oIphone(oFindBackNameVal) && !oMailbox(oFindBackNameVal)){
			layer.msg('手机号或者邮箱不正确！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else{
			
			$.post(sendm,{phone:$("input[name='account']").val()},function(e){
				if(e.code =='0'){
					layer.msg(e.msg, {icon : 1,shade : [ 0.4, '#000' ],time : 1000},function(){
						 CountDownCode();
					});
				}else{
					layer.msg(e.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 1000});
				}
			});
		}
		var countdown = 60;
		var  CountDownCode = function(){
			if(validCode){
				validCode  = false;
				//layer.msg('验证码发送成功！', {icon: 1,shade: [0.4, '#000'],time : 1000});
				var time = setInterval(function(){
					countdown --;
					oFindCodeBtn.addClass('login-code-gray');
					oFindCodeBtn.html('重新发送('+countdown+')');
					if(countdown == 0){
						clearInterval(time);
						oFindCodeBtn.removeClass('login-code-gray');
						oFindCodeBtn.html('获取验证码');
						validCode  = true;
						countdown = 60;
					}

				},1000);
			}else{
				layer.msg('请不要一直点击！', {icon: 2,shade: [0.4, '#000'],time : 1000});
				return false;
			}

		}

		
	});
	
	/**
	* @description: 显示密码
	*/
	$(".login-icon-on").click(function(event) {

		if($(this).hasClass('login-icon-off')){
			oFindBackPass.attr('type','password');
			oFindBackReinput.attr('type','password');
			$(this).removeClass('login-icon-off');
		}else{
			oFindBackPass.attr('type','text');
			oFindBackReinput.attr('type','text');
			$(this).addClass('login-icon-off');
		}

	});
	/**
	* @description: 提示报错
	*/
	function fnLoginError (str){
		var oHtml = '<span class="error-info"><i class="comm-icon login-error-icon"> </i>'+str+'</span>'
		oFindBackError.html(oHtml);
		infoNullTimer();
	}
	/**
	* @description: 提示成功
	*/
	function fnLoginSuccess(str){
		var oHtml = '<span class="error-info success-info txt-green"> <i class="comm-icon login-success-icon"> </i>'+str+'</span>';
		oFindBackError.html(oHtml);
		infoNullTimer();
	}
	/**
	* @description: 去掉提示
	*/
	function infoNullTimer()  
	{  
	    if(oFindBackError.html() != ''){
			setTimeout(function () { 
				oFindBackError.html('');
			},3000);
		}
		
	}  

});