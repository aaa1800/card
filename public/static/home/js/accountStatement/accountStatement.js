/*!
  * author：lxf
  * date: 2018-5-9 14:19:00
  * description: 账户流水
  */
var searchTimeStart = {
    elem: "#timeStart",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        searchTimeEnd.min = datas;
        searchTimeEnd.start = datas;
        $("#startDate").val(datas);
        $("#timeRegion").val("CUSTOM");
    }

};
var searchTimeEnd = {
    elem: "#timeEnd",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        searchTimeStart.max = datas;
        $("#endDate").val(datas);
        $("#timeRegion").val("CUSTOM");
    }
};

$(document).ready(function () {
    laydate(searchTimeStart);
    laydate(searchTimeEnd);

    //时间按钮点击事件
    $("#dateBtns button").click(function(e) {
        var parentId = $(this).parent().attr("id");
        $(this).removeClass("btn-default").addClass("btn-primary").siblings().removeClass("btn-primary").addClass("btn-default");

        $("#timeRegion").val($(this).attr("region"));

        getOrderQueryDate(parentId, $(this).attr("region"));

    });

    //查询
    $("#search").click(function(){
        changePage("/getAccountStatementRecords.html", "queryForm", "queryPagination", "refreshAccountFinanceData", "accountFinancePageNumber", 1);
    });

    //给enter绑定查询事件
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $('#search').click();
        }
    });

    //分页--每页显示多少条数据
    $("#queryPagination").on("click", ".dropdown-menu a", function(){
        var pageSize = $.trim($(this).html());
        $("#pageSize").val(pageSize);
        $("#queryPagination").find(".page-size").val(pageSize);
        changePage("/getAccountStatementRecords.html", "queryForm", "queryPagination", "refreshAccountFinanceData", "accountFinancePageNumber", 1);
    });

    //时间查询默认今天
    $('#dateBtns button[region="TODAY"]').click();


    //获取表格数据
    changePage("/getAccountStatementRecords.html", "queryForm", "queryPagination", "refreshAccountFinanceData", "accountFinancePageNumber", 1);

});

//显示表格数据
function refreshAccountFinanceData(data){
    // console.log("账户记录："+JSON.stringify(data));
    if(data != null) {
        var d = data.data;
        if(d.length > 0){
            var html = '';
            for(var i = 0; i < d.length; i++){
                var orderNumberStr = "";
                if(d[i].type == 1){//加/扣款
                    orderNumberStr = d[i].orderno;
					pil="/";
                }else{
                    orderNumberStr = '/';
					pil=d[i].orderno;
                }

                var typeStr = d[i].type;
                

                html+='<tr>' +
                    '<td>'+d[i].addtime+'</td>' +
                    '<td>'+orderNumberStr+'</td>' +
                    '<td>'+pil+'</td>' +
                    '<td>'+typeStr+'</td>' +
                    '<td><span class="'+changeMoneyClass(parseInt(d[i].price))+'">￥'+numberFormatter(d[i].price)+'</span></td>' +
                    '<td class="text-orange">￥'+numberFormatter(d[i].money)+'</td>' +
                    '<td>'+d[i].data+'</td>' +
                    '</tr>';
            }
            $("#dataTable tbody.dataWraper").html(html);
            $("#queryPagination").show();//显示分页

        } else {
            dataEmpty($("#dataTable"), "queryPagination");
        }
    } else {
        dataEmpty($("#dataTable"), "queryPagination");
    }
}

function getOrderQueryDate(parentId, region) {
    var AddDayCount = '';
    if(region == "LASTDAY"){
        AddDayCount = -1;
    }else if(region == "TODAY"){
        AddDayCount = 0;
    }else if(region == "WEEK"){
        AddDayCount = -7;
    }else if(region == "1"){
        AddDayCount = -31;
    }else if(region == "3"){
        AddDayCount = -92;
    }else if(region == "NONE"){
        AddDayCount = '';
    }

    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    var year = dd.getFullYear(); //获取当前年，四位数
    var month = dd.getMonth()+1;//获取当前月
    var day = dd.getDate();  //获取当前日
    var dateStartPart = year + "-";
    var dateEndPart = '';

    if(region == "LASTDAY"){
        dateEndPart = year + "-";
        if(month < 10)
            dateEndPart += "0";
        dateEndPart += month + "-";
        if(day < 10)
            dateEndPart += "0";
        dateEndPart += day;

    }else{
        var mydate = new Date();
        var curyear = mydate.getFullYear();
        var curmonth = mydate.getMonth()+1;//获取当前月
        var curday = mydate.getDate();//获取当前月
        dateEndPart = curyear + "-";
        if(curmonth < 10)
            dateEndPart += "0";
        dateEndPart += curmonth + "-";
        if(curday < 10)
            dateEndPart += "0";
        dateEndPart += curday;
    }

    if(month < 10)
        dateStartPart += "0";
    dateStartPart += month + "-";
    if(day < 10)
        dateStartPart += "0";
    dateStartPart += day;
    var fullStartDate = dateStartPart + " " + "00" + ":" + "00" + ":" + "00";
    var fullEndDate = dateEndPart + " " + "23" + ":" + "59" + ":" + "59";

    if(region == "NONE"){
        if(parentId == "dateBtns"){
            $("#timeStart").val("");
            $("#timeEnd").val("");

            $("#startDate").val("");
            $("#endDate").val("");
        }
    }else{
        if(parentId == "dateBtns"){
            $("#timeStart").val(fullStartDate);
            $("#timeEnd").val(fullEndDate);

            $("#startDate").val(fullStartDate);
            $("#endDate").val(fullEndDate);
        }
    }

}

function changeMoneyClass(status) {
    // status（1：增加余额，2：减少余额）
    var str;
    if(status<0){
            str="text-green";
	}else{
            str="text-red";

    }
    return str;
}



function changeComment(type,id,remark,orderNumber){
    // type（1：单卡兑换，2：批量兑换，3：提现，4：加/扣款，5：佣金）
    var str;
    if(type=="1" || type=="2"){
        // 1、回收状态(1:处理中、2:回收成功、3:回收失败)  2、id
        str='<a class="text-blue text-underline" href="javascript:viewSellCardDetail(\''+type+'\', \''+id+'\', \''+orderNumber+'\');">查看详情</a>';
    }else if(type=="3"){
        // id
        str='<a class="text-blue text-underline" href="javascript:viewWithdrawCashDetail(\''+type+'\', \''+id+'\', \''+orderNumber+'\');">查看详情</a>';
    }else{
        str=remark;
    }
    return str;
}

/**
 * 说明：类型为1的时候ID代表单卡兑换, 类型为2的时候ID代表批量订单ID
 * @param type
 * @param id
 */
function viewSellCardDetail(type, id, orderNumber){
    getDetailId(type, id, orderNumber);
}

/**
 * 根据id查看提现详情
 * @param id
 */
function viewWithdrawCashDetail(type, id, orderNumber){
    getDetailId(type, id, orderNumber);
}

/**
 * 根据订单编号，类型获取对应的卖卡记录或提现记录ID
 * @param type
 * @param recordId
 * @param orderNumber
 */
function getDetailId(type, recordId, orderNumber){
    $.ajax({
        url: "/getDetailId.html",
        data:{type:type, orderNumber:orderNumber, _csrf:$("input[name='_csrf']").val()},
        dataType: "json",
        async: true,
        type: "POST",
        beforeSend: function() {},
        success: function(data) {
            if(data!=""){
                if(type=="1" || type=="2"){
                    addFrame( '/getSellCardOrdersDetails.html?type='+type+'&id='+data, '010000', '卖卡记录详情');
                }else if(type=="3"){
                    addFrame( '/getDetail.html?id='+data , '7777', '提现详情');
                }
            }else{
                layer.msg("详情ID不合法，请检查后重试", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }

        },
        complete: function() {},
        error: function(e) {}
    });
}