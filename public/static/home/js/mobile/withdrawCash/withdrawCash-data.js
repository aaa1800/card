$(function(){
	clearCash();
    dataInit();
});

function dataInit(){
	$("#withDrawWay").val("");
	getWithdrawWay(false);
}

function doClear(){
	var url = window.location.href;
	var ps = url.split("#");

	try{
		if(ps[1] != 1){
			url += "#1";
		}else{
			window.location = ps[0];
		}
	}catch(ex){
		url += "#1";
	}

	window.location.replace(url);
}

function clearCash(){
	//判断访问终端
	var browser={
	    versions:function(){
	        var u = navigator.userAgent, app = navigator.appVersion;
	        return {
	            trident: u.indexOf('Trident') > -1, //IE内核
	            presto: u.indexOf('Presto') > -1, //opera内核
	            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
	            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
	            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
	            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
	            android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
	            iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
	            iPad: u.indexOf('iPad') > -1, //是否iPad
	            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
	            weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
	            qq: u.match(/\sQQ/i) == " qq" //是否QQ
	        };
	    }(),
	    language:(navigator.browserLanguage || navigator.language).toLowerCase()
	};
	

	//谷歌浏览器会刷新两下，体验不好
	if(!browser.versions.webKit){ 
		doClear();
	}else if(browser.versions.mobile || browser.versions.android || browser.versions.iPhone){
		doClear();
	}
	
}

/**
 * @description: 获取所有提现方式
 * 1：微信 2：银行卡 3:银行卡快速提现 4：支付宝提现
 */
function getWithdrawWay(isShowActionSheet){	
    var drawWay = $("#withDrawWay").val();
    var cardTypeItemHtml = '';
    
    var pf = '';
	// 判断是微信还是浏览器
	if( !isWeiXin() ){
		pf = 2;
		$("#platformHidId").val(2);
    }else{
    	pf = 3;
    	$("#platformHidId").val(3);
    }
	
    ajax_doSubmit(jinl,"data:pf="+pf,function(data){ 	
    	if(data != null && data != "" && data != undefined){
  
    		var drawType = data.drawTypeSettings;
    		if(drawType.length > 0){
    			var maxLimt = "";
    			var logo = "";
    			for(var i = 0; i < drawType.length; i++){
    				if(drawType[i].type == "1"){
    					maxLimt = wxxian;
    					logo = '/static/home/images/withdrawCash/deal_weixin_icon.png';
    				}else if(drawType[i].type == "2"){
    					maxLimt = bank;
    					logo = '/static/home/images/withdrawCash/deal_bank_icon.png';
    				}else if(drawType[i].type == "3"){
    					maxLimt = bank;
    					logo = '/static/home/images/withdrawCash/deal_bank_icon.png';
    				}else if(drawType[i].type == "4"){
    					maxLimt = alixian;
    					logo = '/static/home/images/withdrawCash/deal_zfb_icon.png';
    				}   			
    				
    		        cardTypeItemHtml += '<div class="media" drawWay="'+ drawType[i].type +'" poundage="'+ drawType[i].poundage +'" limit="'+ maxLimt +'" onclick="drawWaySelect(this);">' +
    		            '<img class="w60 align-self-center" src="'+logo+'">' +
    		            '<div class="media-body"><p class="clearfix"><span class="float-left left-txt">'+ drawType[i].name +'</span></p></div>' +
    		            '</div>';

    		        if(i == 0 && drawWay == ""){//默认微信提现
    		            var curSelectedWay = '<img class="w60 align-self-center" src="/static/home/images/withdrawCash/deal_bank_icon.png">' +
    		                '<div class="media-body"><p class="clearfix"><span class="float-left left-txt">银行卡快速提现</span></p></div>';
    		                
    		            $("#withDrawWay").val("3");
    		            $("#drawWayMedia").attr("poundage", '0').attr("limit", '2');
    		            $("#bankCardSelect").val("3");
    		            $("#alipayAccountSelect").val("");    		            
    		        }
    		    }
    			$("#drawWayMedia").html(curSelectedWay);

    			if(isShowActionSheet){
    		        var cardTypeListHtml = '<div class="actionsheet-menu">' +
    		            '<div class="actionsheet-header"><h4>请选择提现方式</h4><span class="actionsheet-close-btn"><i class="icon-close"></i></span></div>' +
    		            '<div class="actionsheet-listing drawWay-listing" id="drawWayMeun">'+ cardTypeItemHtml +'</div>' +
    		            '</div>';

    		        openActionSheet(cardTypeListHtml);

    		        //设置选中项
    		        var curDrawWay = $("#withDrawWay").val();
    		        if(curDrawWay != ""){
    		            $("#drawWayMeun .media").each(function(){
    		                if($(this).attr("drawWay") == curDrawWay){
    		                    $(this).addClass("active");
    		                }else{
    		                    $(this).removeClass("active");
    		                }
    		            });
    		        }
    		    }
    			
    			getWithdrawAccountsByWithdrawWay( $.trim($("#withDrawWay").val()) );
		        getWithdrawLimitTip($("#drawWayMedia").attr("poundage"), $("#drawWayMedia").attr("limit"));
    		}else{
    			layerInfoTip("暂无可用的提现方式");
    		}
    	}else{
    		layerInfoTip("无数据");
    	}
    }); 
    
  
    
}

/**
 * @description: 根据提现方式获取提现账号
 * 银行卡最多添加5张，支付宝最多添加3个,微信没有限制
 * @param drawWay
 * @param isShowActionSheet
 */
function getWithdrawAccountsByWithdrawWay(drawWay){
	var pf = '';
	// 判断是微信还是浏览器
	if( !isWeiXin() ){
		pf = 2;
    }else{
    	pf = 3;
    }
	
	ajax_doSubmit(jinl,"data:pf="+pf,function(data){
		if(data != null && data != "" && data != undefined){
			
			var nullDrawAccount = "";
			var drawAccount = "";
			if(drawWay == "1"){
				var wxList = data.weixinDrawAccountList;				
				if(wxList.length>0){
					for(var i = 0; i < wxList.length; i++){
		                drawAccount += '<div class="media wx-account-media '+(i == 0 ? 'active' : '')+'" drawAccount="'+ wxList[i].nickName +'" drawAccountId="'+ wxList[i].id +'">' +
		                    '<img class="w74 align-self-center" src="'+ wxList[i].headImg +'">' +
		                    '<div class="media-body has-account">' +
		                    '	<p class="clearfix"><span class="float-left left-txt">微信账号</span></p>' +
		                    '	<p class="account wx-account">'+ wxList[i].nickName +'</p>' +
		                    '</div></div>';
		                
		                if(i==0){
		                	$("#bankCardSelect").val(wxList[i].id);
	    		            $("#alipayAccountSelect").val("");
	    		            $("#wxAccountSelect").val('');
		                }
		            }
					$("#drawAccountMedia").html(drawAccount).removeClass("media d-flex");
				}else{
					nullDrawAccount = '<img class="w74 align-self-center wx-img" src="/static/home/images/withdrawCash/brown_weixin.png">' +
		                '<div class="media-body no-account">' +
		                '    <p class="clearfix">' +
				                '<span class="float-left left-txt">请添加微信账号</span>' +
				                '<a class="float-right right-txt" href="'+ txbank +'">立即去添加</a>' +
			                '</p>' +
		                '</div>';
					$("#drawAccountMedia").html(nullDrawAccount).addClass("media d-flex");
					
					$("#bankCardSelect").val("");
		            $("#alipayAccountSelect").val("");
		            $("#wxAccountSelect").val("");
				}
	            
			}else if(drawWay == "2" || drawWay == "3"){
				var bankCardList = data.bankCardList;
				if(bankCardList.length>0){
					for(var i = 0; i < bankCardList.length; i++){
		                drawAccount += '<div class="media bank-account-media '+(i == 0 ? 'active' : '')+'" drawAccount="'+ bankCardList[i].account +'" drawAccountId="'+ bankCardList[i].id +'">' +
		                    '<img class="w74 align-self-start" src="/static/home/images/withdrawCash/logo_gfyh.png">' +
		                    '<div class="media-body has-account">' +
		                    '	<p class="clearfix"><span class="float-left left-txt">'+ bankCardList[i].bankCodeName +'</span></p>' +
		                    '	<p class="account bank-card">'+ bankCardList[i].account +'储蓄卡</p>' +
		                    '</div></div>';
		                
		                if(i==0){
		                	$("#bankCardSelect").val(bankCardList[i].id);
	    		            $("#alipayAccountSelect").val("");
	    		            $("#wxAccountSelect").val("");
		                }
		            }
					$("#drawAccountMedia").html(drawAccount).removeClass("media d-flex");
				}else{
					nullDrawAccount = '<img class="w74 align-self-center bank-img" src="/static/home/images/withdrawCash/brown_bank.png">' +
		                '<div class="media-body no-account">' +
		                '    <p class="clearfix">' +
				                '<span class="float-left left-txt">请添加银行卡账号</span>' +
				                '<a class="float-right right-txt" href="'+ txbank +'">立即去添加</a>' +
			                '</p>' +
		                '</div>';
					$("#drawAccountMedia").html(nullDrawAccount).addClass("media d-flex");
					
					$("#bankCardSelect").val("");
		            $("#alipayAccountSelect").val("");
		            $("#wxAccountSelect").val("");
				}
			}else if(drawWay == "4"){
				var aliPayAccountList = data.aliPayAccountList;
				if(aliPayAccountList.length>0){
					for(var i = 0; i < aliPayAccountList.length; i++){
						drawAccount += '<div class="media ali-account-media '+(i == 0 ? 'active' : '')+'" drawAccount="'+ aliPayAccountList[i].account +'" drawAccountId="'+ aliPayAccountList[i].id +'">' +
		                    '<img class="w74 align-self-start" src="/static/home/images/withdrawCash/logo_zfb.png">' +
		                    '<div class="media-body has-account">' +
		                    '	<p class="clearfix"><span class="float-left left-txt">支付宝账号</span></p>' +
		                    '	<p class="account ali-account">'+ aliPayAccountList[i].account +'</p>' +
		                    '</div></div>';
						
						if(i==0){
		                	$("#bankCardSelect").val(aliPayAccountList[i].id);
	    		            $("#alipayAccountSelect").val("");
	    		            $("#wxAccountSelect").val("");
		                }
		            }
					$("#drawAccountMedia").html(drawAccount).removeClass("media d-flex");
				}else{
					nullDrawAccount = '<img class="w74 align-self-center bank-img" src="/static/home/images/withdrawCash/brown_zfb.png">' +
		                '<div class="media-body no-account">' +
		                '    <p class="clearfix">' +
		                '<span class="float-left left-txt">请添加支付宝账号</span>' +
		                '<a class="float-right right-txt" href="'+ txbank +'">立即去添加</a>' +
		                '</p>' +
		                '</div>';
					$("#drawAccountMedia").html(nullDrawAccount).addClass("media d-flex");
					
					$("#bankCardSelect").val("");
		            $("#alipayAccountSelect").val("");
		            $("#wxAccountSelect").val("");
				}
			}
		}else{
			layerInfoTip("无数据");
		}
	}); 
	
}

function getWithdrawLimitTip(poundage, limit) {
    var poundageTip = "";
    if(parseFloat(poundage) > 0){
        poundageTip += '每笔收'+ poundage +'元手续费，';
    }else{
        poundageTip += '<span class="color-green">免手续费</span>，';
    }

    $("#limitMoneyTip").html(poundageTip+(parseFloat(poundage)+1)+"元&lt;可提现金额&lt;"+ limit +"元");
}

function ajaxWithdrawCash(){
	//console.log($("#withdrawCashForm").serialize());
	$.post($("#withdrawCashForm").attr('action'),$("#withdrawCashForm").serialize(),function(data){ 	
		if(data.code==1){
			layer.open({
                content: '<p class="ico ico_right">提交成功！后台审核将在2小时内完成付款，请注意查收！</p>'
                ,btn: ['确定', '取消']
                ,yes: function(index){
                	location.href=paylog;
                    layer.close(index);
                }
                ,no: function(){
                    location.reload();
                }
            });
        }else{           
        	layerInfoTip(data.msg);
        }
    	
    }); 
}


