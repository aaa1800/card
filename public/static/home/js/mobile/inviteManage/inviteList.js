/**
 * 邀请管理
 */
$(function(){
	inviteRecordPageInit();
});
function inviteRecordPageInit(){
	var itemIndex = 0;
    var tab1LoadEnd = false;
    var tab2LoadEnd = false;
    var tab3LoadEnd = false;
    
    //tab 切换
    var tabLi = $("#invite-record-tab .nav-item");
    	
    tabLi.click(function(){
    	 var $this = $(this);
         itemIndex = $this.index();
				
		$("#invite-record-tab").find('.nav-link.active').removeClass('active');
        $this.find(".nav-link").addClass('active');
        $('.data-list').eq(itemIndex).removeClass("d-none").siblings(".data-list").addClass("d-none");

        var noDataArea = $('.data-list').eq(itemIndex).find(".null_record_data").length;		
		if(noDataArea > 0){
			$(".dropload-down").css("display", "none");
		}else{
			$(".dropload-down").css("display", "block");
		}
		
		// 如果选中菜单一
        if(itemIndex == '0'){
            // 如果数据没有加载完
            if(!tab1LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        // 如果选中菜单二
        }else if(itemIndex == '1'){
            if(!tab2LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        else if(itemIndex == '2'){
            if(!tab3LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        
        // 重置
        dropload.resetload();
	});
    
    var pageNumber1 = 0;
    var pageNumber2 = 0;
    var pageNumber3 = 0;
	//type: ""为全部  0未生效下级  1有效下级
    var loadData = function(status, pageNumber, $refresh, refreshType, tabIndex){
    	ajax_doSubmit(acces,"data:type="+status+"&pageSize=10&pageNumber="+pageNumber,function(data){
    		if(data == null || data == "" || data.total == 0){
    			// 没有数据
                var nullHtml = '<div class="null_record_data">' +
                    '<i class="null_lower_ico"></i>' +
                    '<p>暂无相关邀请记录哦~</p>' +
                    '<a href="'+ ctxPath.domain +'/member/index.do" class="btn btn-blue">返回个人中心</a>' +
                    '</div>';
                $('.data-list').eq(itemIndex).html(nullHtml);   
                
                // 数据加载完                
            	if(tabIndex == 0){
                	tab1LoadEnd = true;
        		}else if(tabIndex == 1){
            		tab2LoadEnd = true;
        		}else if(tabIndex == 2){
            		tab3LoadEnd = true;
        		}
                
                // 无数据
            	$refresh.noData();
                $(".dropload-down").css("display", "none");
    		}else{
    			var d = data.data;
    			var html = '';
    			
    			for(var i = 0; i < d.length; i++){
    				var typeHtml = "";
    				var imgSrc = "";
    				if(d[i].type == "0"){// 未生效下级
    					
    					imgSrc += '/static/home/images/invite/disab_subor_icon.png';
    					typeHtml += '<span class="color-blue">未生效下级</span>';
    				
    				}else if(d[i].type == "1"){// 有效下级
    					
         				imgSrc += '/static/home/images/invite/valid_subor_icon.png';
         				typeHtml += '<span class="color-green">有效下级</span>';
     				}         			
         			    				    			    				
         			
    				html += '<li class="list-item">'+
			                '<a class="media" href="javascript:void(0);">'+
			                    '<img class="w74 align-self-center" src="'+imgSrc+'">'+
			                    '<div class="media-body">'+
			                        '<h5 class="tit">用户编号：'+d[i].subordinateMemberNumber+'</h5>'+
			                        '<p class="desc text-truncate">下级类型：'+typeHtml+'</p>'+
			                        '<p class="desc text-truncate">注册时间：'+d[i].regTime+'</p>'+
			                    '</div>'+
			                '</a>'+
			                '<a class="list-item-footer row no-right-arrow" href="javascript:void(0);">'+
			                    '<div class="col">销卡总面值：<span class="color-orange">￥'+numberFormatter(d[i].totalPrice)+'</span></div>'+
			                    '<div class="col text-right">佣金：<span class="color-green">￥'+numberFormatter(d[i].totalCommision)+'</span></div>'+
			                '</a>'+
			            '</li>';
    			}
    			
    			if(refreshType == "UPDATE"){
                	$('.data-list').eq(itemIndex).html(html);
                	if(tabIndex == 0){
                		pageNumber1 = 1;
            		}else if(tabIndex == 1){
                		pageNumber2 = 1;
            		}else if(tabIndex == 2){
                		pageNumber3 = 1;
            		}
                	
                	// 数据加载完
                	if(tabIndex == 0){
                    	tab1LoadEnd = false;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = false;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = false;
            		}

                    // 无数据
                    $refresh.noData(false);
            	}else{
                	$('.data-list').eq(itemIndex).append(html);
            	}
         		
         		if(data.last_page == data.current_page){
                	// 数据加载完                  
                    if(tabIndex == 0){
                    	tab1LoadEnd = true;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = true;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = true;
            		}

                    // 无数据
                   $refresh.noData();
            	}
    		}
    		
    		// 每次数据加载完，必须重置
    		$refresh.resetload();
    	});
	};
	
	// dropload
    var dropload = $('.tab-content').dropload({
        scrollArea : window,
        // 下拉刷新模块显示内容
        domUp : {
            domClass   : 'dropload-up',
            // 下拉过程显示内容
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            // 下拉到一定程度显示提示内容
            domUpdate  : '<div class="dropload-update">↑释放更新</div>',
            // 释放后显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown : {
            domClass   : 'dropload-down',
            // 滑动到底部显示内容
            domRefresh : '<div class="dropload-refresh">↑上拉加载更多</div>',
            // 内容加载过程中显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            // 没有更多内容-显示提示
            domNoData  : '<div class="dropload-noData">已没有更多数据~</div>'
        },
        loadUpFn : function(me){
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", 1, me, "UPDATE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", 1, me, "UPDATE", itemIndex);
            }
            else if(itemIndex == '2'){
            	loadData("0", 1, me, "UPDATE", itemIndex);
            }
        }
        ,
        loadDownFn : function(me){
        	//alert(itemIndex);
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", ++pageNumber1, me, "MORE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", ++pageNumber2, me, "MORE", itemIndex);
            }
            else if(itemIndex == '2'){
            	loadData("2", ++pageNumber3, me, "MORE", itemIndex);
            }
        },
        distance:50
    });

}