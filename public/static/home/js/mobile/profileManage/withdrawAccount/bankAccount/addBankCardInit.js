/*!
  * author：lxf
  * date: 2018-4-9 16:30:45
  * description: 
  */

$(function(){
    //表单验证
    $("#submitAddBankCardFormBtn").on("click", function(){
        verifyAddBankCardForm();
    });
});

/**
 * 银行卡号：1、是否输入  2、格式是否正确
 * 交易密码：1、是否输入
 */
function verifyAddBankCardForm(){
    var cardNumber = $.trim($("#cardNumber").val());
    var tradePassword = $.trim($("#tradePassword").val());
    var bankCardReg = /^[0-9]{12,19}$/;

    if(cardNumber == ""){
        layerInfoTip("请输入银行卡号");
    }else{
        if(bankCardReg.test(cardNumber)){
            if(tradePassword == ""){
                layerInfoTip("请输入交易密码");
            }else{
                saveBankCard();
            }
        }else{
            layerInfoTip("银行卡号格式不正确");
        }
    }
}

/**
 * 保存银行卡
 */
function saveBankCard(){   
    ajax_doSubmit("/m/member/bankAccount/saveBankCard.do","form:addBankCardForm",function(data){
    	if(data.code == "000000") {
            //添加成功，刷新页面
    		layer.open({
    	        content: '<p class="ico ico_right">银行卡添加成功</p>'
    	        ,shadeClose: false
    	        ,btn: ['我知道了']
    	        ,yes: function(index){
    	            window.location.reload();
    	            layer.close(index);
    	        }
    	    });
        } else {
            layerInfoTip(data.message);
        }
	}); 
}