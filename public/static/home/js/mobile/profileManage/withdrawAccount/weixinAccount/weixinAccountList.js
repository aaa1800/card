/**
 * 微信账号列表
 */
$(function(){
	//获取微信账号列表数据

	
    //删除
	$(document).on("click", "#wxAccountList .delete-ico", function(){
        var id = $(this).parents("li").find("input[name='id']").val();
        deleteWxAccount(id);
    });
});

function wxAccountDataInit(){
	ajax_doSubmit("/m/member/weixinAccount/getMyWeixinDrawAccounts.do","data:",function(data){
		if(data != null && data != "" && data != undefined){
			var len = data.length;
			if(len > 0){
				var liItem = "";
				for(var i=0;i<len;i++){
					liItem += '<li>'+
		                    '<input type="hidden" value="'+data[i].id+'" name="id">'+
		                    '<a class="media" href="javascript:void(0);">'+
		                        '<img class="w100" src="'+data[i].headImg+'">'+
		                        '<div class="media-body">'+
		                        	'<p class="font16">'+data[i].nickName+'</p>'+
		                        	'<p class="font14">'+data[i].openId+'</p>'+
		                        	'<p class="font14">'+format(data[i].addTime, 'yyyy-MM-dd HH:mm:ss')+'</p>'+
		                        '</div>'+
		                    '</a>'+
		                    '<i class="delete-ico"></i>'+
		                '</li>';
				}
				$("#wxAccountList").html(liItem);
			}
			
		}
	});
}

//删除微信账号
function deleteWxAccount(id){
    $('#sureTradePwdModal').modal('show');
    $("#delId").val(id);//给弹出层的delId赋值
}

function sureDelSeltedWxAccount(){
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    var tradePassword = $.trim($("#tradePassword").val());

    var $errorObj = $("#sureTradePwdModal .errorTip");

    if(isOpenTradePaaword === "false"){
        $errorObj.addClass("wrong").html("您未开启交易密码，开启后才可执行删除操作");
    }else{
        if(tradePassword == ""){
            $errorObj.addClass("wrong").html("请输入交易密码");
        }else{
            var id = $("#delId").val();
            $errorObj.removeClass("wrong").html("");
            // 删除用户绑定的微信账号
            deleteMemberWxAccount($errorObj,id,tradePassword);

        }
    }
}

/**
 * 删除用户绑定的微信账号
 * @param $errorObj
 * @param id
 * @param tradePassword
 */
function deleteMemberWxAccount($errorObj,id,tradePassword){
	$.post(shanc,{id:id,tradePassword:tradePassword},function(data){
		if(data.code == 1){
            //删除成功，刷新页面
            window.location.reload();
        }else{
            $errorObj.addClass("wrong").html(data.msg);
        }
	})
     
}

