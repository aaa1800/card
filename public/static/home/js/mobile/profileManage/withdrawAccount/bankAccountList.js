/*!
  * author：lxf
  * date: 2018-4-9 18:01:37
  * description: 银行卡账号列表
  */
$(function(){
	bankCardDataInit();
	
    //删除账号
    $(document).on("click", "#bankCardList .delete-ico", function(){
        var id = $(this).parents("li").find("input[name='id']").val();
        deleteBankCard(id);
    });
});

function bankCardDataInit(){
	ajax_doSubmit("/getBank.html","data:id=2",function(data){
		if(data != null && data != "" && data != undefined){
			var realName = data.m.realName;
			var d = data.bankCardList;
			if(d.length > 0){
				var liItem = "";
				for(var i=0;i<d.length;i++){
					liItem += '<li>'+
		                    '<input type="hidden" value="'+d[i].id+'" name="id">'+
		                    '<a class="media" href="javascript:void(0);">'+
		                        '<img class="w74 align-self-center" src="/static/home/images/profileManage/withdrawAccount/logo_zxyh.png">'+
		                        '<div class="media-body">'+
		                            '<h5 class="tit">'+realName+'</h5>'+
		                            '<span class="subhead">'+d[i].bankname+'</span>'+
		                        '</div>'+
		                    '</a>'+
		                    '<p class="desc text-truncate">'+
		                        '<span>'+d[i].accounts+'</span>'+
		                        '<a href="'+editl+'?id='+d[i].id+'" class="edit-ico"></a>'+
		                        '<i class="delete-ico"></i>'+
		                    '</p>'+
		                '</li>';
				}
				$("#bankCardList").html(liItem);
			}
			
			if(data.isSureAddBankCard){
				$("#addCardBtn").removeClass('d-none');
			}
		}
	});
}

//银行卡列表页面删除银行卡
function deleteBankCard(id){
    $('#sureTradePwdModal').modal('show');
    $("#delId").val(id);//给弹出层的delId赋值

}

function sureDelSeltedBankCard(){
    var isOpenTradePaaword = $.trim($("#isOpenTradePaaword").val());
    var tradePassword = $.trim($("#tradePassword").val());

    var $errorObj = $("#sureTradePwdModal .errorTip");

    if(isOpenTradePaaword == ""){
        $errorObj.addClass("wrong").html("您未开启交易密码，开启后才可删除银行卡");
    }else{
        if(tradePassword == ""){
            $errorObj.addClass("wrong").html("请输入交易密码");
        }else{
            var id = $("#delId").val();
            $errorObj.removeClass("wrong").html("");
            // 删除用户绑定的银行卡
            deleteMemberBankCard($errorObj,id,tradePassword);

        }
    }
}

/**
 * 删除用户绑定的银行卡
 * @param $errorObj
 * @param id
 * @param tradePassword
 */
function deleteMemberBankCard($errorObj,id,tradePassword){
    $.post(delurl,{id:id,tradePassword:tradePassword},function(data){
		if(data.code == 1){
            //删除成功，刷新页面
            window.location.reload();
        }else{
            $errorObj.addClass("wrong").html(data.msg);
        }
	})
 
}