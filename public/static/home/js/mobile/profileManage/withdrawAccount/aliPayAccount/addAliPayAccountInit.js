/*!
  * author：lxf
  * date: 2018-4-10 10:42:38
  * description: 支付宝账号表单
  */
$(function(){
    //表单验证
    $("#submitAliPayAccountFormBtn").on("click", function(){
		var stype=$.trim($("input[name=atype]").val());
		if(stype==1){
            verifyAliPayAccountForm();
		}else{
			verifyAddBankCardForm();
		}
    });
});

/**
 * 支付宝账号：1、是否输入  2、格式是否正确（手机或邮箱）
 * 交易密码：1、是否输入
 */
function verifyAliPayAccountForm(){
    var aliPayAccount = $.trim($("#aliPayAccount").val());
    var tradePassword = $.trim($("#tradePassword").val());
	

    if(aliPayAccount == ""){
        layerInfoTip("请输入支付宝账号");
    }else{

        if(!checkPhone(aliPayAccount) && !checkMail(aliPayAccount)){
            layerInfoTip("支付宝账号不正确");
        }else{
            if(tradePassword == ""){
                layerInfoTip("请输入交易密码");
            }else{
                saveAliPayAccount();
            }
        }
    }
}
/**
 * 银行卡号：1、是否输入  2、格式是否正确
 * 交易密码：1、是否输入
 */
function verifyAddBankCardForm(){
    var cardNumber = $.trim($("#aliPayAccount").val());
    var bankNameSel = $.trim($("#bankNameSel").val());
	var province = $.trim($("#province").val());
	var city = $.trim($("#city").val());
	var county = $.trim($("#county").val());
	var zhibank=$.trim($("#zhibank").val());
	var tradePassword = $.trim($("#tradePassword").val());
    var bankCardReg = /^[0-9]{12,19}$/;

    if(cardNumber == ""){
        layerInfoTip("请输入银行卡号");
    }else if(zhibank==""){
		layerInfoTip("请填写开户行地址");
	}else{
        if(bankCardReg.test(cardNumber)){
            if(tradePassword == ""){
                layerInfoTip("请输入交易密码");
            }else{
                saveBankCard();
            }
        }else{
            layerInfoTip("银行卡号格式不正确");
        }
    }
}
/**
 * 保存银行卡
 */
function saveBankCard(){   
    var aliPayAccount = $.trim($("#aliPayAccount").val());
    var tradePassword = $.trim($("#tradePassword").val());
	 var bankNameSel = $.trim($("#bankname").val());
	var province = $.trim($("#province").val());
	var city = $.trim($("#city").val());
	var county = $.trim($("#county").val());
	var zhibank=$.trim($("#zhibank").val());
	var id = $("input[name=id]").val();
    $.post(ctxurl,{cardNumber:aliPayAccount,tradePassword:tradePassword,id:id,bankname:bankNameSel,province:province,city:city,county:county,zhibank:zhibank},function(data){
		 if(data.code == 1) {
            //添加成功，刷新页面
    		layer.open({
    	        content: '<p class="ico ico_right">银行卡添加成功</p>'
    	        ,shadeClose: false
    	        ,btn: ['我知道了']
    	        ,yes: function(index){
    	            window.location.reload();
    	            layer.close(index);
    	        }
    	    });
        } else {
            layerInfoTip(data.msg);
        }
	})
}

/**
 * 保存支付宝账号
 */
function saveAliPayAccount(){
    var aliPayAccount = $.trim($("#aliPayAccount").val());
    var tradePassword = $.trim($("#tradePassword").val());
	var id = $("input[name=id]").val();
    $.post(ctxurl,{account:aliPayAccount,tradePassword:tradePassword,id:id},function(data){
		 if(data.code == 1) {
            //添加成功，刷新页面
    		layer.open({
    	        content: '<p class="ico ico_right">支付宝添加成功</p>'
    	        ,shadeClose: false
    	        ,btn: ['我知道了']
    	        ,yes: function(index){
    	            window.location.reload();
    	            layer.close(index);
    	        }
    	    });
        } else {
            layerInfoTip(data.msg);
        }
	})
}