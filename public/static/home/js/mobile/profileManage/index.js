$(function() {
    ajax_doSubmit("/profileManage.html", "data:", function (data) {
    	// console.log(data);
  	
    	// 修改昵称
    	if(data.nickName=="" || data.nickName==null || data.phone==undefined){
    		$('#nickName').html('<span>设置用户昵称</span>');
    	}else{
    		$('#nickName').html('<span class="color-blue">'+data.nickName+'</span>');
    	}   	

    	// 实名认证
    	if(data.isRealNameAuthen){
    		$("#realNameStatus").html('<span class="mr-2">'+data.realName+'</span><span class="badge badge-pill badge-success">已实名</span>');
    	}else{
    		$("#realNameStatus").html('<span class="badge badge-pill badge-danger">未实名</span>');
    	}
    	
    	// 提现账号
    	if(data.isCashAccount=="0"){
        	$("#withdrawAccountStaus").html('待添加');
    	}else if(data.isCashAccount=="1"){
        	$("#withdrawAccountStaus").html('修改');
    	}

    	// 绑定手机号    	
    	if(data.phone=="" || data.phone==null || data.phone==undefined){
    		$('#phone').html('去绑定');
    	}else{
    		$('#phone').html(data.phone);
    	}
    	
    	// 邮箱 	
    	if(data.mail=="" || data.mail==null || data.mail==undefined){
    		$('#mail').html('去绑定');
    	}else{
    		$('#mail').html(data.mail);
    	}
    	
    	// 关注公众号
    	if(data.status=="1"){
    		$('#followWXStatus').html('<span class="color-green">已关注</span>');
    	}else{
    		$('#followWXStatus').html('<span class="color-red">未关注</span>');
    	}
    	
    	// 联系QQ
    	if(data.qq=="" || data.qq==null || data.qq==undefined){
    		$('#contactQq').html('待补全');
    	}else{
    		$('#contactQq').html(data.qq);
    	}
    	
    	// 注册时间
    	$("#regTime").html(format(data.attentionTime, 'yyyy-MM-dd HH:mm'));
 
    });
});