/**
 * date：2018-1-22 10:22:55
 * author：lxf
 * description：更换邮箱初始化页面脚本
 */
$(function() {
    //验证方式的显示或隐藏
    var verifyWay = $("#getCodeWay").val();
    getCurVerifyWay(verifyWay);

    //获取手机验证码
    $("#getPhoneCode").on("click", function(){
        var $getCodeBtn = $("#getPhoneCode");
        sendVertifyCode($getCodeBtn);
    });

    //获取邮箱验证码
    $("#getMailCode").on("click", function(){
        var $getCodeBtn = $("#getMailCode");
		$(this).text("发送中..");
        ajaxSendVertifyCode($getCodeBtn);
    });
	$("#getNewMailCode").click(function(){
		 var email=$("#mail").val();
		 var $errorObj = $("#doerr");
		 var eok=verifyMail(email, $errorObj, "新邮箱");
		 if(eok){
			 $.post(sendEmail,{email:email},function(e){
				  if(e.code==1){
					   layerInfoTip("验证码发送成功，请注意查收");
					   var mailCode = $("#getNewMailCode");
					   codeTimer(mailCode, "time", "发送邮箱验证码");
				  }else{
					  layerInfoTip(e.msg);
				  }
			 });
		 }
	});
	$("#getOldMailCode").click(function(){
		 var email=$("#nMail").val();
		 var $errorObj = $("#nerror");
		 var eok=verifyMail(email, $errorObj, "邮箱");
		 if(eok){
			 $.post(sendEmail,{email:email},function(e){
				  if(e.code==1){
					   noticeCodeSendSuccess("验证码发送成功", $(this), 2000);
				  }else{
					  parent.layer.msg(e.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
				  }
			 });
		 }
	});
	
	$("#submitBindMailFormBtn").click(function(){
		var $errorObj = $("#doerr");
		var code=$("#mailCode").val(),email=$("#mail").val();
		var okcode=verifyCode(code, $errorObj);
		if(!okcode)return;
		var eok=verifyMail(email, $errorObj, "新邮箱");
		if(eok){
			 $.post(curll,{yzm:code,email:email},function(e){
				  if(e.code==1){
					   layer.alert('<span style="color:#27c5ab;">邮箱设置成功,若忘记自行修改或联系客服！</span>', {
						title: "提示",
						icon: 1,
						skin: 'layer-ext-moon',
						btn: '返回账户总览',
						yes: function(index, layero){
							layer.close(index);
							gotoAccountOverviewPage();
						},
						cancel: function(index, layero){//右上角关闭按钮回调
							window.location.href = member;
						}
					});
				  }else{
					  layerInfoTip(e.msg);
				  }
			 });
		 }else{
		 }
		
	})
	$("#submitBindMailFormBtnsss").click(function(){
		var $errorObj = $("#doerr");
		var code=$("#mCode").val(),email=$("#nMail").val();
		var okcode=verifyCode(code, $errorObj);
		if(!okcode)return;
		var eok=verifyMail(email, $errorObj, "新邮箱");
		if(eok){
			 $.post(curll,{yzm:code,email:email},function(e){
				  if(e.code==1){
					   layer.alert('<span style="color:#27c5ab;">邮箱设置成功,若忘记自行修改或联系客服！</span>', {
						title: "提示",
						icon: 1,
						skin: 'layer-ext-moon',
						btn: '返回账户总览',
						yes: function(index, layero){
							layer.close(index);
							gotoAccountOverviewPage();
						},
						cancel: function(index, layero){//右上角关闭按钮回调
							window.location.href = member;
						}
					});
				  }else{
					  parent.layer.msg(e.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
				  }
			 });
		 }
		
	})

    /**
     * 下一步
     */
    $("#submitModifyMailVerifyCodeBtn").on("click", function(e){
        
     
        var mailCode = $.trim($("#mailCode").val());//邮箱验证码
        
        var icon = "<i class='fa fa-times-circle'></i>";
        var $errorObj = $("#error");

            var verifyPhoneCodeResult = verifyCode(mailCode, $errorObj);
            if(verifyPhoneCodeResult){
                //验证成功进入输入新邮箱页面
                vertifyUserNameCode(mailCode,$errorObj,icon);
            }else{
                e.preventDefault();
            }

        

    });

});

function getCurVerifyWay(verifyWay){
    var $phoneWayObj = $("#viaPhoneVerify");
    var $mailWayObj = $("#viaMailVerify");

    //为了视觉效果，先全部隐藏
    $phoneWayObj.hide();
    $mailWayObj.hide();

    if(verifyWay == "phone"){
        $phoneWayObj.show();
    }else if(verifyWay == "email"){
        $mailWayObj.show();
    }
}

//ajax发送验证码
function ajaxSendVertifyCode($getCodeBtn, time){
    var verifyWay = $("#getCodeWay").val();
    // 更换邮箱-手机或邮箱验证默认初始化
	
    updateBindMailVertifyInit(verifyWay,$getCodeBtn,time);

}



/**
 * 更换邮箱-手机或邮箱验证默认初始化
 * @param type
 * @param $getCodeBtn
 * @param time
 */
function updateBindMailVertifyInit(type='email',$getCodeBtn,time) {
    $.ajax({
        url: ctxPath,
        data:{type:type,_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "0")
            {
               noticeCodeSendSuccess("验证码发送成功", $getCodeBtn, time);
               	$(this).text("验证码发送成功");
            }
            else{
				$getCodeBtn.text("获取验证码");
				 	$(this).text("获取验证码");
                parent.layer.msg(data.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}




/**
 * 更换邮箱-验证绑定手机或邮箱验证验证码
 */
function vertifyUserNameCode(code,$errorObj,icon) {
    $.ajax({
        url: precode,
        data:{code:code,_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == 1)
            {
                window.location.href = curll;
            }
            else{  
                    $errorObj.html(icon + data.msg);

            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}