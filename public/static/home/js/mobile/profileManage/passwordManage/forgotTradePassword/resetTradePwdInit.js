/*!
  * author：lxf
  * date: 2018-5-7 15:40:46
  * description: 忘记交易密码——重置交易密码验证身份表单
  */
$(function() {
	var verifyWay = $("#selectedVerifyWay").val();
    getCurVerifyWay(verifyWay);
    
    //切换验证方式
    $(document).on("click", "#verifyWayMedia", function () {
        changeResetTradePwdVerifyWay();
    });

    //关闭弹层
    $(document).on("click", ".actionsheet-close-btn", function(){
        $(this).parents(".layui-m-layer").remove();
    });

    //获取手机验证码
    $(document).on("click", "#getPhoneCode", function(){
        sendVertifyCode($(this));
    });

    //获取邮箱验证码
    $(document).on("click", "#getMailCode", function(){
        sendVertifyCode($(this));
    });

    //表单验证
    $("#submitResetTradePwdInitFormBtn").on("click", function(e){
        var verifyWay = $("#selectedVerifyWay").val();

        var phoneCode = $.trim($("#phoneCode").val());//手机验证码
        var mailCode = $.trim($("#mailCode").val());//邮箱验证码
        if(verifyWay == "phone"){

            if(phoneCode == ""){
            //     layerInfoTip('请输入手机验证码！');
            // }else{
                //ajax验证表单
                vertifyResetTradePwdCode(phoneCode, verifyWay, $("#getPhoneCode"));
            }

        }else if(verifyWay == "email"){

            if(mailCode == ""){
            //     layerInfoTip('请输入邮箱验证码！');
            // }else{
                //ajax验证表单
                vertifyResetTradePwdCode(mailCode, verifyWay, $("#getMailCode"));
            }

        }

    });
});

function getCurVerifyWay(verifyWay){
    var $phoneWayObj = $("#phoneCodeBox");
    var $mailWayObj = $("#mailCodeBox");
   
    $phoneWayObj.addClass("d-none");
    $mailWayObj.addClass("d-none");
    
    if(verifyWay == "phone"){
        $phoneWayObj.removeClass("d-none");
    }else if(verifyWay == "mail"){
        $mailWayObj.removeClass("d-none");
    }
}

/**
 * 验证方式切换
 */
function changeResetTradePwdVerifyWay(){
    var phone = $.trim($("#phone").val());
    var mail = $.trim($("#mail").val());
    if (phone !== "" && mail !== "") {
        var resetTradePwdVerifyWayHtml = '<div class="actionsheet-menu">' +
            '<div class="actionsheet-header"><h4>请选择验证方式</h4><span class="actionsheet-close-btn"><i class="icon-close"></i></span></div>' +
            '<div class="verifyWay-listing" id="resetTradePwdVerifyWayMeun">'+
            '   <a class="list-item" href="javascript:void(0);" verifyWay="phone" onclick="verifyWaySelect(this);">' +
            '       <img class="item-img" src="/static/home/images/common/sale_hfk_icon.png"><span>通过已绑定手机认证</span>' +
            '   </a>' +
            '   <a class="list-item" href="javascript:void(0);" verifyWay="email" onclick="verifyWaySelect(this);">' +
            '       <img class="item-img" src="/static/home/images/common/sale_email_icon.png"><span>通过已绑定邮箱认证</span>' +
            '   </a>' +
            '</div>' +
            '</div>';

        openActionSheet(resetTradePwdVerifyWayHtml);

        //设置选中项
        var curVerifyWay = $("#selectedVerifyWay").val();
        if(curVerifyWay != ""){
            $("#resetTradePwdVerifyWayMeun .list-item").each(function(){
                if($(this).attr("verifyWay") == curVerifyWay){
                    $(this).addClass("active");
                }else{
                    $(this).removeClass("active");
                }
            });
        }

    } else {
        layerInfoTip("暂无其他验证方式");
    }
}

/**
 * @description: 验证方式菜单项点击事件
 */
function verifyWaySelect(obj) {
    var verifyWay = $(obj).attr("verifyWay");
    $("#selectedVerifyWay").val(verifyWay);

    var selectedVerifyWayItem = "";
    if(verifyWay == "phone"){
        selectedVerifyWayItem = '<p class="d-flex clearfix"><span class="left-txt">通过已绑定手机认证</span><span class="right-txt">'+ $("#phone").val() +'</span></p>';
        $("#phoneCodeBox").removeClass("d-none");
        $("#mailCodeBox").addClass("d-none");
    }else if(verifyWay == "email"){
        selectedVerifyWayItem = '<p class="d-flex clearfix"><span class="left-txt">通过已绑定邮箱认证</span><span class="right-txt">'+ $("#mail").val() +'</span></p>';
        $("#phoneCodeBox").addClass("d-none");
        $("#mailCodeBox").removeClass("d-none");
    }
    $("#verifyWayMedia .media-body").html(selectedVerifyWayItem);

    $(obj).parents(".layui-m-layer").remove();
}

function sendVertifyCode($getCodeBtn) {
    if($getCodeBtn.hasClass("color-green")){
        //ajax发送重置交易密码验证身份验证码
        var verifyWay = $("#selectedVerifyWay").val();
        sendResetTradePwdVertifyCode(verifyWay, $getCodeBtn);
    }else{
        layerInfoTip("请不要一直点击！");
    }
}

/**
 * ajax发送重置交易密码验证身份验证码
 * @param type: phone、mail
 * @param $getCodeBtn
 */
function sendResetTradePwdVertifyCode(type, $getCodeBtn){
    $.post(jiaoyi,{type:type},function(data){
		if(data.code == 0) {
			$("#neirong").show();
            layerInfoTip("验证码发送成功，请注意查收");
            codeTimer($getCodeBtn, "time", "发送验证码");
        } else {
            layerInfoTip(data.msg);
        }    	
	})
}



/**
 * ajax验证表单
 * @param code：手机验证码、邮箱验证码
 * @param verifyWay：手机、邮箱
 * @param $getCodeBtn：发送验证码按钮
 */
function vertifyResetTradePwdCode(code, verifyWay, $getCodeBtn){
	var mia=$("#newLoginPwd").val(),mib=$("#newLoginPwdAgain").val();
	if(mia==null || mia=="" || mib==null || mib==""){
		layerInfoTip("密码不能为空");
		return ;
	}
	if(mia!=mib){
		layerInfoTip("两次密码不一样");
		return ;
	}
	$.post(editurl,{tradePwd:mib},function(data){
        	layerInfoTip(data.msg);
	})
    
}