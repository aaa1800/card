/**
 * date：2018-1-19 09:32:44
 * author：lxf
 * description：已经绑定手机或邮箱，但是没有设置交易密码，设置交易密码页面脚本
 */

$(function(){
    //获取验证码
    $("#getCode").on("click", function(){
        // var getCodeWay = $.trim($("#getCodeWay").val());
        // var $errorObj = $("#setLoginPasswordForm .errorTip");
        var $getCodeBtn = $("#getCode");
        sendVertifyCode($getCodeBtn);

    });

    /**
     * 表单验证
     * 1、交易密码：是否为空；必须是6-20个英文字母、数字或符号，区分大小写
     * 2、验证码：是否为空；4位数字
     */
    $("#submitSetTradePwdFormBtn").on('click', function(e) {
        var tradePwd = $("#tradePwd").val();//交易密码
        var code = $.trim($("#code").val());//验证码
        var icon = "<i class='fa fa-times-circle'></i>";
        var $errorObj = $("#modifyLoginPasswordForm .errorTip");
		if(verifyPassword(tradePwd, $errorObj, '交易密码')){
			$.post(editTran,{tradePwd:tradePwd,codeTran:code},function(e){
				if(e.code==1){
				//成功，刷新页面
					layer.alert('<span style="color:#27c5ab;">交易密码设置成功,若忘记自行修改或联系客服！</span>', {
						title: "提示",
						icon: 1,
						skin: 'layer-ext-moon',
						btn: '返回账户总览',
						yes: function(index, layero){
							layer.close(index);
							gotoAccountOverviewPage();
						},
						cancel: function(index, layero){//右上角关闭按钮回调
							window.location.href = member;
						}
					});
				}else{
					$errorObj.html(icon + e.msg);
				}
			});
		}
        
    });
});


//ajax发送验证码
function ajaxSendVertifyCode($getCodeBtn, time){
    var type =$('#getCodeWay option:selected').val();
    tradePasswordVertifyInit(type,$getCodeBtn,time);
}



/**
 * 设置交易密码-交易密码密码验证初始化
 * @param type
 * @param $getCodeBtn
 * @param time
 */
function tradePasswordVertifyInit(type,$getCodeBtn,time) {
    $.ajax({
        url: ctxPath,
        data:{type:type,_csrf:ctxPath._csrf},
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == '0')
            {
                noticeCodeSendSuccess("验证码发送成功", $getCodeBtn, time);
            }
            else{
                parent.layer.msg(data.msg, {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}





/**
 * 设置交易密码-验证交易密码验证码
 */
function vertifyResetTradePasswordCode($errorObj,icon) {
    $.ajax({
        url: $("#setTradePasswordForm").attr("action"),
        data: $("#setTradePasswordForm").serialize(),
        dataType: "json",
        async: true,
        type: "POST",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {},
        success: function(data) {
            if(data.code == "000000")
            {
                //成功，刷新页面
                layer.alert('<span style="color:#27c5ab;">交易密码设置成功,若忘记自行修改或联系客服！</span>', {
                    title: "提示",
                    icon: 1,
                    skin: 'layer-ext-moon',
                    btn: '返回账户总览',
                    yes: function(index, layero){
                        layer.close(index);
                        gotoAccountOverviewPage();
                    },
                    cancel: function(index, layero){//右上角关闭按钮回调
                        window.location.href = ctxPath.domain+"/member/profileManage/passwordManage/tradePassword/tradePasswordManageIndex.do";
                    }
                });
            }
            else{
                if(data.code=="000002"){
                    if ($("#getCode").hasClass("btn-danger")) {
                        noticeCodeSendSuccess(data.message,$("#getCode"), 60);
                    }else{
                        parent.layer.msg(data.message, {icon: 2,shade: [0.4, '#000'],time : 1000});
                    }
                }else{
                    $errorObj.html(icon + data.message);
                }
            }
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}
