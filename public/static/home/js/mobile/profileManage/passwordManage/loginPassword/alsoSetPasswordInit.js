/*!
  * author：lxf
  * date: 2018-5-7 16:46:47
  * description: 根据原登录密码修改登录密码
  */
$(function(){
    //表单验证
    $("#submitModifyLoginPwdFormBtn").on("click", function(e){

        var oldLoginPwd = $.trim($("#oldLoginPwd").val());
        var newLoginPwd = $.trim($("#newLoginPwd").val());
        var newLoginPwdAgain = $.trim($("#newLoginPwdAgain").val());

        if(oldLoginPwd == ""){
            layerInfoTip('请输入原登录密码！');
        }else if(newLoginPwd == ""){
            layerInfoTip('请输入新登录密码！');
        }else if(!checkPassword(newLoginPwd)){
            layerInfoTip('密码由6-20位字母、数字或者符号两种或以上组成！');
        }else if(newLoginPwdAgain == ""){
            layerInfoTip('请再次确认新密码！');
        }else if(!checkPassword(newLoginPwdAgain)){
            layerInfoTip('密码由6-20位字母、数字或者符号两种或以上组成！');
        }else if(newLoginPwd !== newLoginPwdAgain){
            layerInfoTip('两次新密码输入不一致！');
        }else{
            vertifyModifyLoginPasswordForm(oldLoginPwd, newLoginPwd);
        }

    });
});

/**
 * ajax验证根据原登录密码修改登录密码表单
 * @param oldLoginPwd：原登录密码
 * @param newLoginPwd：新登录密码
 */
function vertifyModifyLoginPasswordForm(oldLoginPwd, newLoginPwd) {   
    $.post(editu,{passa:oldLoginPwd,ps1:newLoginPwd,ps2:newLoginPwd,mcode:$('#newcode').val(),cz:$('input[name=cz]').val()},function(data){
		 if(data.code == 1){
    		layer.open({
    	        content: '<p class="ico ico_right">密码修改成功!</p>'
    	        ,shadeClose: false
    	        ,btn: ['返回个人中心', '返回个人信息']
    	        ,yes: function(index){
    	            window.location.href = "/infoo.html";
    	            layer.close(index);
    	        }
    	        ,no: function () {
    	            window.location.href = "/infoo.html";
    	            layer.close(index);
    	        }
    	    });
        }else{
        	layerInfoTip(data.msg);
        }
	})
}