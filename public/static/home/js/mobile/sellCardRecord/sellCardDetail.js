$(function(){
	$("#resubmit").click(function(event){
		
			resubmit($("#orderId").val(), "", $("#orderType").val());
	});
});

/**
 * 一键提交失败卡密
 */
function resubmit(id, type, orderType)
{
	var tip = null;
	$.ajax({
        url: ctxPath.domain + (orderType == "1" ? "/member/sellCardRecord/reSubmitFailedOrder.do" : "/member/sellCardRecord/reSubmitFailedOrders.do"),
        data:{id:id, type:type, _csrf:ctxPath._csrf},
        dataType:'json',
        type: 'POST',
        beforeSend: function() {
        	tip = layer.msg('提交处理中，请稍候...', {icon: 16, shade : [0.4, '#000'],time : 100000});
        },
        success: function(data) {
        	
        	if(data.code != "000000")
    		{
        		layer.msg(data.message, {icon : 2,shade : [ 0.4, '#000' ],time : 1500});
    		}
        	else
    		{
        		layer.alert(data.message, {
        			title:'提交成功',
        			icon : 1,
        			skin: 'layui-layer-lan', //样式类名
        			closeBtn: 1
    			}, function(index){
    				//window.location.reload();
    				//layer.close(tip);
    				layer.close(index);
    			});
    		}
         	
        },
        complete: function() {
        	layer.close(tip);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           //alert(XMLHttpRequest.status);
        }
    });
	
}