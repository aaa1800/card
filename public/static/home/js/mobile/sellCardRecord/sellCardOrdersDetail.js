/*!
  * author：lxf
  * date: 2018-3-30 14:42:57
  * description: 卖卡记录详情
  */
$(function(){
	var orderType = $("#orderType").val();
	var orderId = $("#orderId").val();
	getSellCardDetailData(orderType,orderId);
	
    //一键提交失败卡密
    $(document).on("click", "#submitAllFailCard", function(){
    	var canBeChangeSubmitType = $("#canBeChangeSubmitType").val();
    	if(canBeChangeSubmitType){
            resubmit($("#orderId").val(), "1", $("#orderType").val());
                    
    	}else{
    		resubmit($("#orderId").val(), "", $("#orderType").val());
    	}
        
    });

    //点击查看更多数据
    $(document).on("click", "#loadMoreData", function(){
        layerInfoTip("ajax加载更多数据");
    });
});

function getSellCardDetailData(type, id){
	$.post(faurl,{type:type,id:id},function(data){
		if(data != null && data != "" && data != undefined){

			var cardList='';
			var operatorName = data.batchOrdersDetails.operatorName;//产品类型
			var commitFaceVal = data.batchOrdersDetails.commitFaceVal;//提交面值
			
			var d=data.ordersList;
			if(d.length>0){
				$(".list-box").removeClass("d-none");
				for(var i=0;i<d.length;i++){
					var statusStr = "";
					var operatorTimeStr = '';
					var failReason = '';
					
					var collectionStatus = d[i].state;
					if(collectionStatus == 0){
						statusStr = '<b class="color-blue1">处理中</b>';
						operatorTimeStr = '';
						failReason = '';
					}else if(collectionStatus == "1"){
						statusStr = '<b class="color-green">成功</b>';
						operatorTimeStr = '<div class="line clearfix">'+
	                        '<div class="justify">处理时间</div><b class="mr-2">:</b><b>'+d[i].update_time+'</b></div>';
						failReason = '';
					}else if(collectionStatus == "2"){
						statusStr = '<b class="color-red">失败</b>';
						operatorTimeStr = '<div class="line clearfix">'+
	                    '<div class="justify">处理时间</div><b class="mr-2">:</b><b>'+d[i].update_time+'</b></div>';
						
						failReason = '<div class="line clearfix">'+
	                        '<div class="justify">失败原因</div><b class="mr-2">:</b><b class="color-red">'+d[i].remarks+'</b>'+
	                        '</div>';
					}
					cardList+='<li class="list-item">'+
	                    '<div class="line clearfix">'+
			            '    <div class="justify">卡号</div><b class="mr-2">:</b><b class="copy-btn" data-clipboard-text="'+d[i].card_no+'" onclick="copyWordsFn();">'+d[i].card_no+'</b>'+
			            '</div>'+
			            '<div class="line clearfix">'+
			            '    <div class="justify">卡密</div><b class="mr-2">:</b><b class="copy-btn" data-clipboard-text="'+d[i].card_key+'" onclick="copyWordsFn();">'+d[i].card_key+'</b>'+
			            '</div>'+
			            '<div class="line clearfix">'+
			            '    <div class="justify">真实面值</div><b class="mr-2">:</b><b class="color-orange">￥'+numberFormatter(d[i].settle_amt)+'</b>'+
			            '</div>'+operatorTimeStr+		           
			            '<div class="line clearfix">'+
			            '    <div class="justify">金额</div><b class="mr-2">:</b><b class="color-orange">￥'+numberFormatter(d[i].amount)+'</b>'+
			            '</div>'+
			            '<div class="line clearfix">'+
			            '    <div class="justify">状态</div><b class="mr-2">:</b>'+statusStr+
			            '</div>'+failReason+
			        '</li>';
				}
			}else{				
				cardList = '';
			}
			
			$(".list-content ul").html(cardList);
			
			var detail = data.batchOrdersDetails;
			var cardDetailHtml = '';
			if(detail!= null && detail!= "" && detail!= undefined){
				var operateStatus = detail.operateStatus;
				var operateState = '';
				if(operateStatus == 0){
					operateState = '<span class="color-blue1">处理中</span>';
				}else if(operateStatus > 0){
					operateState = '<span class="color-green">已完成</span>';
				}
				cardDetailHtml='<div class="detail-item d-flex justify-content-between">'+
				   '	<span class="left-txt">订单号：</span>'+
				   '    <span>'+detail.orderNumber+'</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">产品类型：</span>'+
				   '     <span>'+operatorName+'</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">提交面值：</span>'+
				   '     <span class="color-blue">￥'+numberFormatter(commitFaceVal)+'</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '    <span class="left-txt">处理状态：</span>'+operateState+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">提交数量：</span>'+
				   '     <span class="color-orange">'+detail.totalCount+'张</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">成功数量：</span>'+
				   '     <span class="color-green">'+detail.successCount+'张</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">失败数量：</span>'+
				   '     <span class="color-red">'+detail.failedCount+'张</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">成功金额：</span>'+
				   '     <span class="color-orange">￥'+numberFormatter(detail.arrivalMoney)+'</span>'+
				   '</div>'+
				   '<div class="detail-item d-flex justify-content-between">'+
				   '     <span class="left-txt">提交时间：</span>'+
				   '     <span class="color6">'+detail.orderTime+'</span>'+
				   '</div>';				
			}else{
				cardDetailHtml = '';
			}
			
			$("#detailCon").html(cardDetailHtml);
					
			if(data.canBeReSubmited){
				$("#submitAllFailCard").removeClass("d-none");
				
				if(data.canBeChangeSubmitType){
					$("#canBeChangeSubmitType").val(true);
				}else{
					$("#canBeChangeSubmitType").val(false);
				}
			}					
						
		}else{
			//暂无详情
			
		}
	
	});
		
}

/**
 * 一键提交失败卡密
 */
function resubmit(id, type, orderType){

	$.post(oUrl,{id:id,type:type},function(data){
		if(data.code != 1){
			layerInfoTip("提交成功"+data.num+"张，"+data.str);
		}else{
			layer.open({
                content: '<p class="ico ico_right">提交成功</p>'
                ,btn: ['我知道了']
                ,yes: function(index){
                    layer.close(index);
                }
            });
		}
	});
}

/**
 * 数字千位符格式化
 * @param num
 * @returns {string}
 */
function numberFormatter(num) {
    return (parseFloat(num).toFixed(2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}

function copyWordsFn () {
	var clipboard = new ClipboardJS('.copy-btn');
    clipboard.on('success', function(e) {
        layer.open({
            content: '<p class="ico ico_right">复制成功，可粘贴使用</p>'
            ,btn: '我知道了'
        });
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
        layer.open({
            content: '<p class="ico ico_warn">复制出错，请长按文本拷贝</p>'
            ,btn: '我知道了'
        });
    });
}
