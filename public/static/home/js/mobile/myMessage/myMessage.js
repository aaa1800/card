$(function(){
    //下拉刷新
    pullMessagesToRefreshDown();
});

function pullMessagesToRefreshDown(){
    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        // 如果使用body的scroll，一个页面请只配置一个下拉刷新，否则会有冲突
        isUseBodyScroll: true,
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            successAnim: {
                // 下拉刷新结束后是否有成功动画，默认为false，如果想要有成功刷新xxx条数据这种操作，请设为true，并实现对应hook函数
                isEnable: true
            },
            onPull: function() {

            },
            callback: function () {
                window.location.reload();//页面刷新
                miniRefresh.endDownLoading(true, '恭喜您，刷新页面成功');// 结束下拉刷新
            }
        },
        up: {
            isAuto: false,
            callback: function () {
                miniRefresh.endUpLoading();// 结束上拉加载
            }
        }
    });
}