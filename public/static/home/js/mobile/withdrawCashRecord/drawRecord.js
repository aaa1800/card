$(function(){
    drawRecordPageInit();
});

function drawRecordPageInit(){
	var itemIndex = 0;
    var tab1LoadEnd = false;
    var tab2LoadEnd = false;
    var tab3LoadEnd = false;
    
    //tab 切换
    var tabLi = $("#withdraw-record-tab .nav-item");
    	
    tabLi.click(function(){
    	 var $this = $(this);
         itemIndex = $this.index();
				
		$("#withdraw-record-tab").find('.nav-link.active').removeClass('active');
        $this.find(".nav-link").addClass('active');
        $('.data-list').eq(itemIndex).removeClass("d-none").siblings(".data-list").addClass("d-none");

        var noDataArea = $('.data-list').eq(itemIndex).find(".null_data").length;		
		if(noDataArea > 0){
			$(".dropload-down").css("display", "none");
		}else{
			$(".dropload-down").css("display", "block");
		}
		
		// 如果选中菜单一
        if(itemIndex == '0'){
            // 如果数据没有加载完
            if(!tab1LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        // 如果选中菜单二
        }else if(itemIndex == '1'){
            if(!tab2LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        else if(itemIndex == '2'){
            if(!tab3LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        
        // 重置
        dropload.resetload();
	});
    
    var pageNumber1 = 0;
    var pageNumber2 = 0;
    var pageNumber3 = 0;
	
    var loadData = function(status, pageNumber, $refresh, refreshType, tabIndex){
    	ajax_doSubmit(tixurl,"data:status="+status+"&pageSize=10&pageNumber="+pageNumber,function(data){
    		if(data == null || data == "" || data.total == 0){
    			// 没有数据
                var nullHtml = '<div class="null_data">' +
                    '<i class="null_record_ico"></i>' +
                    '<p>暂无相关提现记录哦~</p>' +
                    '<a href="'+ ctxPath.domain +'/member/index.do" class="btn btn-blue">返回个人中心</a>' +
                    '</div>';
                $('.data-list').eq(itemIndex).html(nullHtml);   
                
                // 数据加载完                
            	if(tabIndex == 0){
                	tab1LoadEnd = true;
        		}else if(tabIndex == 1){
            		tab2LoadEnd = true;
        		}else if(tabIndex == 2){
            		tab3LoadEnd = true;
        		}
                
                // 无数据
            	$refresh.noData();
                $(".dropload-down").css("display", "none");
    		}else{
    			var d = data.data;
    			var html = '';
    			
    			for(var i = 0; i < d.length; i++){
    				var statusHtml = "";
    				if(d[i].status == 0)
    				{// 处理中
    					statusHtml += '<span class="color-blue">等待处理</span>';
    				}
    				else if(d[i].status == "1")	// 成功
    				{
         				statusHtml += '<span class="color-green">成功</span>';
     				}
         			else if(d[i].status == "2")	// 失败
     				{
         				statusHtml += '<span class="color-red">失败</span>';
     				}
         			else 	// 退款
     				{
         				statusHtml += '<span class="color-red">退款</span>';
     				}
    				
    				var imgSrc = "";
    				/* if(d[i].type == "1") // 微信
    				{
    					imgSrc += '/static/home/images/withdrawCashRecord/record_weixin_icon.png';
    				} */
    				         			
         			if(d[i].type == "支付宝提现") // 支付宝
     				{
         				imgSrc += '/static/home/images/withdrawCashRecord/record_zfb_icon.png';
     				}else{
    					imgSrc += '/static/home/images/withdrawCashRecord/record_bank_icon.png';
     				}
         			
    				html += '<li class="list-item">' +
	                    '<a class="media" href="javascript:void(0);">' +
	                    '    <img class="w74 align-self-center" src="'+imgSrc+'">' +
	                    '   <div class="media-body">' +
	                    '       <h5 class="tit">提现单号：'+ d[i].order +'</h5>' +
	                    '       <p class="desc text-truncate">提款金额：<span class="color-orange">￥'+ parseFloat(d[i].money).toFixed(2) +'</span></p>' +
	                    '       <p class="desc text-truncate">申请时间：'+d[i].addtime+'</p>' +
	                    '   </div>' +
	                    '</a>' +
	                    '<a class="list-item-footer row" href="' + ctxPath.domain + '/member/withdrawCashRecord/getDetail.do?id='+ d[i].id +'">' +
	                    '   <div class="col">实际到账：<span class="color-orange">￥'+ parseFloat(d[i].money).toFixed(2) +'</span></div>' +
	                    '   <div class="col">状态：'+statusHtml+'</div>' +
	                    '</a>' +
	                    '</li>';
    			}
    			
    			if(refreshType == "UPDATE"){
                	$('.data-list').eq(itemIndex).html(html);
                	if(tabIndex == 0){
                		pageNumber1 = 1;
            		}else if(tabIndex == 1){
                		pageNumber2 = 1;
            		}else if(tabIndex == 2){
                		pageNumber3 = 1;
            		}
                	
                	// 数据加载完
                	if(tabIndex == 0){
                    	tab1LoadEnd = false;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = false;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = false;
            		}

                    // 无数据
                    $refresh.noData(false);
            	}else{
                	$('.data-list').eq(itemIndex).append(html);
            	}
         		
         		if(data.current_page == data.last_page){
                	// 数据加载完
                    
                    if(tabIndex == 0){
                    	tab1LoadEnd = true;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = true;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = true;
            		}

                    // 无数据
                   $refresh.noData();
            	}
    		}
    		
    		// 每次数据加载完，必须重置
    		$refresh.resetload();
    	});
	};
	
	// dropload
    var dropload = $('.tab-content').dropload({
        scrollArea : window,
        // 下拉刷新模块显示内容
        domUp : {
            domClass   : 'dropload-up',
            // 下拉过程显示内容
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            // 下拉到一定程度显示提示内容
            domUpdate  : '<div class="dropload-update">↑释放更新</div>',
            // 释放后显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown : {
            domClass   : 'dropload-down',
            // 滑动到底部显示内容
            domRefresh : '<div class="dropload-refresh">↑上拉加载更多</div>',
            // 内容加载过程中显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            // 没有更多内容-显示提示
            domNoData  : '<div class="dropload-noData">已没有更多数据~</div>'
        },
        loadUpFn : function(me){
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", 1, me, "UPDATE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", 1, me, "UPDATE", itemIndex);
            }
            else if(itemIndex == '2'){
            	loadData("2", 1, me, "UPDATE", itemIndex);
            }
        }
        ,
        loadDownFn : function(me){
        	//alert(itemIndex);
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", ++pageNumber1, me, "MORE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", ++pageNumber2, me, "MORE", itemIndex);
            }
            else if(itemIndex == '2'){
            	loadData("2", ++pageNumber3, me, "MORE", itemIndex);
            }
        },
        distance:50
    });

}