/**
 * 文章详情
 */
$(function(){
	articleDetailPageInit($("#articleId").val());
});

function articleDetailPageInit(id){
	$.get("/newsDetail.html?id="+id,function(data){
		if(data!=null && data!="" && data!=undefined){
			$("header .title").html(data.title);
			$("#articleTitle").html(data.title);
			$("#articleCon").html(data.contents);
		}else{
			//无数据
		}
	});
	
}