$(function(){
    //是否同意协议
    $(document).on('click', '.isAgree', function (e) {
        var status = $(this).find("em").attr("state");
        if(status === "off") {
            $(this).find("em").addClass("active");
            $(this).find("em").attr("state", "on");

            //启用按钮
            $("#registerBtn").removeClass("disabled");
        } else {
            $(this).find("em").removeClass("active");
            $(this).find("em").attr("state", "off");

            //禁用按钮
            $("#registerBtn").addClass("disabled");
        }
    });

    //发送验证码
    $(document).on('click','#getCode',function (e) {
        if($(this).hasClass("color-green")){
            verifyRegisterAccount();
        }else{
            layerInfoTip("请不要一直点击！");
        }
    });

    //注册并登录
    $(document).on("click", '#registerBtn', function(e){
        checkRegisterForm();
    });
});

function verifyRegisterAccount(){
    var registerName = $.trim($("#registerName").val());
    if(registerName==""){
        layerInfoTip("请输入手机号或者邮箱号");
    }else if(!checkPhone(registerName) && !checkMail(registerName)){
        layerInfoTip("手机号或者邮箱号不正确");
    }else{
        //ajax判断注册账号是否正确，正确则发送验证码
        ajaxCheckRegAccount(registerName);
    }
}

function ajaxCheckRegAccount(account){
	$.post(regurl,{p:account,noreg:1,yreg:2},function(data){
		layerInfoTip(data.result);
    	if(data.code=="000000")
    	{
    		codeTimer($("#getCode"), "time", "发送验证码");
    	}
	})
   
}

function checkRegisterForm(){
    var registerAccountVal = $.trim($("#registerName").val());//账号
    var registerCodeVal = $.trim($("#registerCode").val());//验证码
    var registerPassVal = $.trim($("#registerPass").val());//登录密码

    var inviteNumberVal = $.trim($("#inviteNumber").val());//推广员用户编号
	var numberReg = /^\d{1,20}$/;
	
    if(registerAccountVal == ""){
        layerInfoTip('请输入手机号！');
    }else if(!checkPhone(registerAccountVal) && !checkMail(registerAccountVal)){
        layerInfoTip('手机号不正确！');
    }else if(registerPassVal == ""){
        layerInfoTip('请输入登录密码！');
    }else if(!checkPassword(registerPassVal)){
        layerInfoTip('登录密码由6-20位字母、数字或者符号两种或以上组成！');
    }else if(inviteNumberVal != ''){
    	if(!numberReg.test(inviteNumberVal)){
            layerInfoTip('推广员用户编号由1-20位数字组成！');           
        }else{
        	ajaxVerifyMemberNumber(inviteNumberVal);
        }
    }else{
        //ajax验证表单
        ajaxVerifyRegForm();
    }
}

/**
 * @description: ajax验证用户编号
 */
function ajaxVerifyMemberNumber(inviteNumberVal){
	$.post(regnum,{id:inviteNumberVal},function(data){
		if(data.code==1){
    		ajaxVerifyRegForm();
    	}else{
    		layerInfoTip('邀请码错误，没有此用户');
    	}
		
	})
}

function ajaxVerifyRegForm(){	
	$.post($("#registerForm").attr("action"),$("#registerForm").serialize(),function(data){
    	if(data.code== 1)
    	{
    		window.location.href="/loginIndex.html"; 
    	}else
    	{
    			layerInfoTip(data.msg);
    	}
    });  
}