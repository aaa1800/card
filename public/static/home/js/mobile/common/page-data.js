// 无刷新请求分页数据工具类
/**
 * 切换页码
 * @param url
 * @param formId
 * @param pageWrapperId：整个分页容器ID
 * @param refreshFun
 * @param pageNumberId：去第几页输入框ID
 */
function changePage(url, formId, pageWrapperId, refreshFun, pageNumberId, pageNumber)
{
    $("#pageNumber").val(pageNumber);//当前是第几页
    $.ajax({
        url: url,
        data:$("#" + formId).serialize(),
        dataType: "json",
        async: true,
        type: "POST",
        beforeSend: function() {
            var thLength = $(".dataWraper").parents("table").find("thead th").length;
            $(".dataWraper").html("<tr class='loading'><td colspan='"+ thLength +"' style='text-align:center;padding:30px 0;'>别着急，喝杯茶休息下~</td></tr>");
        },
        success: function(data) {
            $(".dataWraper").html("");
            var func = eval(refreshFun);
            func(data);
            refreshPage(data, url, formId, pageWrapperId, pageNumberId, refreshFun);
        },
        complete: function() {
        },
        error: function(e) {
            layer.msg("请求出错,请刷新后重试！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        }
    });
}

/**
 * 翻页后更新页码条
 * @param data
 * @param formId
 * @param pageWrapperId：整个分页容器ID
 * @param pageNumberId：去第几页输入框ID
 * @param refreshFun
 */
function refreshPage(data, url, formId, pageWrapperId, pageNumberId, refreshFun)
{
    $("#" + pageWrapperId).html("");
    var currentPage = parseInt(data.current_page);
    var totalPages = parseInt(data.last_page);//总共多少页
    var totalElements = parseInt(data.total);//总共多少条数据
    var lastPage;
    var nextPage;

    //上一页事件处理
    var pageHtml = "";
    if(data.number <= 0){
        pageHtml += '<li class="disabled"><a title="上一页" href="javascript:void(0);">«</a></li>';
    }else{
        lastPage = data.number;
        pageHtml += '<li><a title="上一页" href="javascript:changePage(\'' + url + '\', \'' + formId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + lastPage + '\')">«</a></li>';
    }

    //中间分页按钮
    if(totalPages == 0){//一页数据都没有
        pageHtml += '<li class="active"><a href="javascript:void(0);">1</a></li>';
    }else if(totalPages <= 10){//小于10页
        for(var i = 1; i <= totalPages; i++){
            pageHtml += '<li' + ((i == currentPage) ?  ' class="active"':'') + '><a href="' + ((i != currentPage) ? 'javascript:changePage(\'' + url + '\', \'' + formId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + i +  '\')' : 'javascript:void(0)') + '">' + i + '</a></li>';
        }
    }else{//大于10页
        var back = totalPages - currentPage;
        var front = totalPages - back - 1;
        var swi = 0;
        var ewi = 0;

        if(currentPage <= 6){
            swi = 1;
            ewi = 10;
        }else{
            if(back <= 4){
                swi = currentPage - (10 - (totalPages - currentPage)) + 1;
                ewi = currentPage + back;
            }else{
                swi = currentPage - 5;
                ewi = currentPage + 4;
            }
        }

        for(var i = swi; i <= ewi; i++){
            pageHtml += '<li' + ((i == currentPage) ?  ' class="active"':'') + '><a href="' + ((i != currentPage) ? 'javascript:changePage(\'' + url + '\', \'' + formId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + i +  '\')' : 'javascript:void(0)') + '">' + i + '</a></li>';
        }
    }

    //下一页事件处理
    if(data.totalPages <= 1 || currentPage >= data.totalPages){
        pageHtml += '<li class="disabled"><a title="下一页" href="javascript:void(0);">»</a></li>';
    }else{
        nextPage = currentPage + 1;
        pageHtml += '<li><a title="下一页" href="javascript:changePage(\'' + url + '\', \'' + formId + '\', \'' + pageWrapperId + '\', \'' +  refreshFun + '\',\'' + pageNumberId + '\',\'' + nextPage + '\')">»</a></li>';
    }

    //输入框处理--默认第一页
    pageHtml += '<li>&nbsp;跳至 <input type="text" value="1" class="target-page" id="' + pageNumberId + '"> 页 '+
        '<a class="btn btn-sm btn-white" href="javascript:goPage(\'' + pageNumberId + '\',\'' + url + '\',\'' + formId + '\', \'' + pageWrapperId + '\', \'' + refreshFun + '\',\'' + totalPages + '\');" title="确定">确定</a>' +
        '</li>';

    //每页显示多少条记录
    var everyPageSize = '';
    var pageSize = $.trim($("#pageSize").val());
    everyPageSize += '<span class="btn-group dropup">' +
        '<button type="button" class="btn btn-default btn-outline dropdown-toggle" data-toggle="dropdown"><span class="page-size">'+ pageSize +'</span><span class="caret"></span></button>' +
        '<ul class="dropdown-menu" role="menu">' +
        '    <li' + ((pageSize == 10) ?  ' class="active"':'') + '><a href="javascript:void(0);">10</a></li>' +
        '    <li' + ((pageSize == 30) ?  ' class="active"':'') + '><a href="javascript:void(0);">30</a></li>' +
        '    <li' + ((pageSize == 50) ?  ' class="active"':'') + '><a href="javascript:void(0);">50</a></li>' +
        '    <li' + ((pageSize == 100) ?  ' class="active"':'') + '><a href="javascript:void(0);">100</a></li>' +
        '</ul>' +
        '</span>';

    //当前显示第几条到第几条数据
    var curPageFrom = '';
    var curPageEnd = '';

    if(data.number <= 0){
        curPageFrom += '1';

        if(totalElements >= pageSize){
            curPageEnd += pageSize;
        }else{
            curPageEnd += totalElements;
        }
    }else{
        // curPageFrom += (currentPage - 1) +'1';
        curPageFrom += (parseInt(currentPage)-1)*parseInt(pageSize)+1;

        if(totalElements >= parseInt(pageSize)*parseInt(currentPage)){
            // curPageEnd += (parseInt(curPageFrom) + parseInt(pageSize)) - 1;
            curPageEnd += parseInt(pageSize)*parseInt(currentPage);
        }else{
            curPageEnd += totalElements;
        }

    }
    var pageHtmlWrap = '<div class="col-sm-6">' +
        '<div class="left-pag">' +
        '    <span>总共 <em>'+ totalElements + '</em> 条数据，当前显示 <em>'+ curPageFrom + '</em> 到 <em>'+ curPageEnd + '</em> 条数据</span>' +
        '    <span> 每页显示'+ everyPageSize +'条记录</span>' +
        '</div></div>'+
        '<div class="col-sm-6"><div class="pull-right">' +
        '    <ul class="pagination">'+ pageHtml +'</ul>' +
        '</div></div>';


    $("#" + pageWrapperId).html(pageHtmlWrap);
}

/**
 * 页码跳转--去第几页确定按钮
 * @param pageNumberId：去第几页输入框ID
 * @param formId
 * @param pageWrapperId：整个分页容器ID
 * @param refreshFun
 * @param totalPage
 * @returns {Boolean}
 */
function goPage(pageNumberId, url, formId, pageWrapperId, refreshFun, totalPage)
{
    var pageNumber = $("#" + pageNumberId).val();
    pageNumber = pageNumber.replace(/ /g,"");

    if(pageNumber.length == 0)
    {
        parent.layer.msg("请输入页码！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(isNaN(pageNumber))
    {
        parent.layer.msg("页码请输入数字！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(parseInt(pageNumber) > parseInt(totalPage))
    {
        parent.layer.msg("输入页码数不能大于总页码数！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(parseInt(pageNumber) < 1)
    {
        parent.layer.msg("请输入大于1的页码数！", {icon : 2,shade : [ 0.4, '#000' ],time : 2000});
        return false;
    }

    if(pageNumber.length > 1)
    {
        var index0 = pageNumber.indexOf("0");
        while(index0 == 0)
        {
            pageNumber = pageNumber.substring(1, pageNumber.length);
            index0 = pageNumber.indexOf("0");
        }
    }
    changePage(url, formId, pageWrapperId, refreshFun, pageNumberId, pageNumber);
}

/**
 * 无数据样式
 * @param tableObj：表格对象
 * @param pageId：整个分页容器ID
 */
function dataEmpty(tableObj, pageId) {
    var columns = tableObj.find("th").length;
    var html = '<tr class="empty">' +
        '<td colspan="'+ columns +'" class="text-center">' +
        '<i class="ico"></i>' +
        '<span>暂无符合查询条件的记录哦 ~ !</span>' +
        '</td>' +
        '</tr>';
    tableObj.find("tbody.dataWraper").html(html);
    tableObj.removeClass("table-hover");
    $("#"+pageId).hide();
    tableObj.find("tfoot").hide();
}