$(function(){
	ajax_doSubmit("/getIndexSlides.html","data:",function(data){
		for(var i=0;i<data.length;i++)
		{
			var htmlLi='<div class="swiper-slide">'+
                '<a class="swiper-slide-link" href="'+ctxPath.domain+data[i].link+'">'+
            '<img src="'+data[i].photoUrl+'"></a></div>';
			$("#indexSlides").append(htmlLi);
		}
		
		//首页轮播图
	    indexBannerSwiper();
	});
	
	ajax_doSubmit("/getHeadNotices.html","data:",function(data){
		for(var i=0;i<data.length;i++)
		{
			var htmlLi='<div class="swiper-slide">'+
                '<a class="d-block" href="'+ctxPath.domain+'/indexhj.html">'+
            '<div class="media">'+
                '<div class="media-body text-truncate">'+data[i].title+'</div>'+
                '<span class="notice-time text-truncate align-self-center">'+data[i].addtime+'</span></div></a></div>';
			$("#noticesList").append(htmlLi);
		}
	    //首页公告轮播
	    indexNoticeSwiper();
	});
	
	ajax_doSubmit("/getProductClassifiesList.html","data:",function(data){
		for(var i=0;i<data.length;i++)
		{
			var htmlLi='<div class="col-4">'+
                '<a class="d-block" href="'+ctxPath.domain+'/sell.html?type='+data[i].class+'">'+
            '<div class="card border-0 rounded-0">'+
                '<img class="card-img-top w96" alt="" src="/static/home/images/index/nav_'+data[i].id+'_icon.png">'+
                '<div class="card-body">'+
                    '<p class="card-text text-center color3">'+data[i].name+'</p></div></div></a></div>';
			$("#productClassifiesList").append(htmlLi);
		}
	});
	
	ajax_doSubmit("/getOperatorsList.html","data:",function(data){
		for(var i=0;i<data.length;i++)
		{
			var htmlLi='<li>'+
                '<a class="d-block" href="'+ctxPath.domain+'/recycle/index.html?productType='+data[i].pcId+'&operatorId='+data[i].id+'">'+
            '<div class="card">'+
                '<img class="card-img-top" alt="" src="'+data[i].iconUrl+'">'+
                '<div class="card-body">'+
                    '<p class="card-text text-center text-truncate color9">'+data[i].name+'</p>'+
                '</div></div>'+
            '</a></li>';
			$("#operatorsList").append(htmlLi);
		}
	});
	
	ajax_doSubmit("/getBaseInfo.html","data:",function(data){
		$("#clientPhone span").html(data.csPhone);
		$("#clientPhone").attr("href", 'tel:'+data.csPhone);
		$("#clientqq").html("renrenxk"); 
	});
	
	ajax_doSubmit("/getFaq.html","data:pageSize=10&pageNumber=1",function(data){
		var d = data.content;
		if(d!=null && d!="" && d!=undefined){
			for(var i=0;i<d.length;i++)
			{
				var htmlLi='<div class="swiper-slide"><p class="subtit text-truncate">'+d[i].title+'</p></div>';
				$("#faqsList").append(htmlLi);
			}
		}else{
			
		}
		
	    //首页常见问题轮播
	    indexFaqSwiper();
	});
	

    //下拉刷新
    pullToRefreshDown();
    

    $(window).scroll(function () {
        if($(window).scrollTop() > 50){
            $("header.navbar").css("background", "#45b5f0");
        }else{
            $("header.navbar").css("background", "none");
        }
    });

    //复制客服QQ
    $("#copy_qq_btn").click(function(){
    	copyClientQq();
    });
    
    //跳转到pc
    $("#trans-pc").click(function(){
    	window.location.href="/";//http://www.renrenxiaoka.com http://192.168.1.116:8080/RenRenXiaoKa
    			
    });
    
});

function indexBannerSwiper(){
    var myIndexBannerSwiper = new Swiper ('#index-banner-swiper', {
        loop: true,//是否循环切换
        speed: 500,//切换速度，即单张图片滑动开始到结束的时间（单位ms）
        observer: true,//当改变swiper的样式（例如隐藏/显示）或者修改swiper的子元素时，自动初始化swiper
        observeParents: true,//当Swiper的父元素变化时，例如window.resize，Swiper更新
        autoplay: {//自动切换开启，默认停留三秒
            delay: 2000,
            stopOnLastSlide: false,//如果设置为true，当切换到最后一个slide时停止自动切换。（loop模式下无效）。
            disableOnInteraction: false//用户操作swiper之后，是否禁止autoplay。默认为true：停止。如果设置为false，用户操作swiper之后自动切换不会停止，每次都会重新启动autoplay。
        },

        // 如果需要分页器
        pagination: {
            el: '#banner-pagination',
            clickable: true,
            clickableClass : 'swiper-pagination'
        }
    });

    /* 如果只有一个slide就销毁swiper*/
    if(myIndexBannerSwiper.slides.length<=3){
        myIndexBannerSwiper.destroy();
    }
}

function indexNoticeSwiper(){
    var myIndexNoticeSwiper = new Swiper ('#index-notice-swiper', {
        direction : 'vertical',
        loop: true,
        observer: true,
        observeParents: true,
        autoplay: {
            delay: 2000,
            stopOnLastSlide: false,
            disableOnInteraction: false
        }

    });

    if(myIndexNoticeSwiper.slides.length<=3){
        myIndexNoticeSwiper.destroy();
    }
}

function indexFaqSwiper(){
	var myIndexFaqSwiper = new Swiper ('#index-faq-swiper', {
        direction : 'vertical',
        loop: true,
        observer: true,
        observeParents: true,
        autoplay: {
            delay: 2000,
            stopOnLastSlide: false,
            disableOnInteraction: false
        }

    });

    if(myIndexFaqSwiper.slides.length<=3){
    	myIndexFaqSwiper.destroy();
    }
}

function pullToRefreshDown(){
    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        // 如果使用body的scroll，一个页面请只配置一个下拉刷新，否则会有冲突
        isUseBodyScroll: true,
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            successAnim: {
                // 下拉刷新结束后是否有成功动画，默认为false，如果想要有成功刷新xxx条数据这种操作，请设为true，并实现对应hook函数
                isEnable: true
            },
            onPull: function() {
                $("header.navbar").css("background", "#45b5f0");
            },
            callback: function () {
                window.location.reload();//页面刷新
                miniRefresh.endDownLoading(true, '恭喜您，刷新页面成功');// 结束下拉刷新
            }
        },
        up: {
            isAuto: false,
            callback: function () {
                miniRefresh.endUpLoading();// 结束上拉加载
            }
        }
    });
}

function copyClientQq(){
    //复制
    /*var qq = $("#clientqq").html();
    var clipboard = new Clipboard('#copy_qq_btn', {
        text: function() {
            return qq;
        }
    });
    clipboard.on('success', function(e) {
        layer.open({
            content: '<p class="ico ico_right">客服QQ已复制，可粘贴到QQ里进行查询</p>'
            ,btn: '我知道了'
        });
    });
    clipboard.on('error', function(e) {
        layer.open({
            content: '<p class="ico ico_warn">复制出错，请长按文本拷贝</p>'
            ,btn: '我知道了'
        });
    });*/
	layer.open({
        content: '<p class="ico ico_warn">请手动复制</p>'
        ,btn: '我知道了'
    });
}
