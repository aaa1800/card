$(function(){
    //发送验证码
    $(document).on('click','#getCode',function (e) {
        if($(this).hasClass("color-green")){
            verifyFindBackAccount();
        }else{
            layerInfoTip("请不要一直点击！");
        }
    });

    //立即找回
    $(document).on("click", '#findPassBtn', function(e){
        checkFindBackForm();
    });
});

function verifyFindBackAccount(){
    var accountName = $.trim($("#accountName").val());
    if(accountName==""){
        layerInfoTip("请输入手机号或者邮箱号");
    }else if(!checkPhone(accountName) && !checkMail(accountName)){
        layerInfoTip("手机号或者邮箱号不正确");
    }else{
        //ajax判断账号是否正确，正确则发送验证码
        ajaxCheckFindAccount(accountName);
    }
}

function ajaxCheckFindAccount(account){
	$.post(sendm,{phone:account},function(data){
		layerInfoTip(data.msg);
    	if(data.code=="0")
    	{
    		codeTimer($("#getCode"), "time", "发送验证码");
    	}
	});
	
}

function checkFindBackForm(){
    var accountNameVal = $.trim($("#accountName").val());//账号
    var findCodeVal = $.trim($("#findCode").val());//验证码
    var newPassVal = $.trim($("#newPass").val());//登录密码
    var newPassAgainVal = $.trim($("#newPassAgain").val());

    if(accountNameVal == ""){
        layerInfoTip('请输入手机号或者邮箱号！');
    }else if(!checkPhone(accountNameVal) && !checkMail(accountNameVal)){
        layerInfoTip('手机号或者邮箱号不正确！');
    }else if(findCodeVal == ""){
        layerInfoTip('请输入验证码！');
    }else if(newPassVal == ""){
        layerInfoTip('请输入登录密码！');
    }else if(!checkPassword(newPassVal)){
        layerInfoTip('登录密码由6-20位字母、数字或者符号两种或以上组成！');
    }else if(newPassAgainVal == ""){
        layerInfoTip('请再次确认密码！');
    }else if(newPassVal != newPassAgainVal ){
        layerInfoTip('两次密码不一致！');
    }else{
        //ajax验证表单
        ajaxVerifyFindBackForm();
    }
}

function ajaxVerifyFindBackForm(){
	$.post(setpass,$("#findback-pannel").serialize(),function(data){
		layerInfoTip(data.msg);
		if(data.code=="000000"){
    		window.location.href =  shou;
    	}
	});
	  
}