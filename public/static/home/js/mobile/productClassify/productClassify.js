$(function(){
	var productClassifyId=$("#productClassifyId").val();
	ajax_doSubmit("/productClassify.html","data:",function(data){
		for(var i=0;i<data.length;i++)
		{
			if(productClassifyId==null || productClassifyId=="")
			{
				productClassifyId=data[i].id;
			}
			
			var htmlLi='<li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#pills-'+data[i].id+'-card">'+data[i].name+'</a></li>';
			
			var operatorshtml="";
			if(productClassifyId==data[i].id)
			{
				htmlLi='<li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#pills-'+data[i].id+'-card">'+data[i].name+'</a></li>';
				
				operatorshtml='<div class="tab-pane fade show active" id="pills-'+data[i].id+'-card"><ul class="list-unstyled">';
			}else
			{
				htmlLi='<li class="nav-item"><a class="nav-link" data-toggle="pill" href="#pills-'+data[i].id+'-card">'+data[i].name+'</a></li>';
				operatorshtml='<div class="tab-pane fade" id="pills-'+data[i].id+'-card"><ul class="list-unstyled">';
			}	
			$("#allCard-classify-tab").append(htmlLi);
			
			for(var j=0;j<data[i].operators.length;j++)
			{
				var ohtml='<li class="'+changePtypeBg(parseInt(data[i].id))+'">'+
                    '<a class="media" href="'+ ctxPath.domain +'/sell.html?type='+data[i].atype+'&operatorId='+data[i].operators[j].id+'">'+
                '<img class="w100 align-self-center rounded-circle" alt="产品图标" src="'+data[i].operators[j].phoneRecycleIcon+'">'+
                '<div class="media-body">'+
                    '<h5 class="tit">'+data[i].operators[j].name+'</h5>'+
                    '<p class="desc text-truncate">面值:'+data[i].operators[j].rule+'</p>'+
                '</div>'+
                '<img class="right-arrow align-self-center" alt="10x17" src="/static/home/images/common/recycle_det_btn.png"></a></li>';
				operatorshtml=operatorshtml+ohtml;
			}
			operatorshtml=operatorshtml+'</ul></div>';
			$("#tab-content").append(operatorshtml);
		}
	});
});

function changePtypeBg(classifyId){
	var str="";
    switch (classifyId){
        case 1:
            str="phoneCard-bg";
            break;
        case 2:
            str="gameCard-bg";
            break;
        case 3:
            str="gasCard-bg";
            break;
        case 4:
            str="ecoCard-bg";
            break;
        case 5:
            str="videoCard-bg";
            break;
        case 6:
            str="ebusinessCard-bg";
            break;
        default:
        	str="phoneCard-bg";
    }
    return str;
}