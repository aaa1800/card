$(function(){
    //登录方式切换
    $("#login-pannel-tab .nav-item").on("click", function(){
        var target = $(this).find(".nav-link").attr("otarget");
        $(this).find(".nav-link").addClass("active");
        $(this).siblings().find(".nav-link").removeClass("active");
        $(".tab-pane").removeClass("show active");
        $("#"+target).addClass("show active");
        
        $("#loginType").val($(this).index()+1);
    });

    //获取手机验证码
    $(document).on('click','#getPhoneCode',function (e) {
        if($(this).hasClass("color-green")){
            verifyCurPhone();
        }else{
            layerInfoTip("请不要一直点击！");
        }
    });

    //登录
    $("#login_btn").on("click", function(){       
        var loginType = $.trim($("#loginType").val());
        if(loginType == "1"){
            //验证账号密码
            checkAccountAndPass(loginType);
        }else if(loginType == "2" || loginType=="3"){
            //验证手机号及验证码
            checkIphoneAndCode(loginType);
        }
    });

    if(ctxPath.error!="")
    {
    	layerInfoTip(ctxPath.error);
    	ctxPath.error="";
    }
    
});

function verifyCurPhone(){
    var phone = $.trim($("#phone").val());
    if(phone==""){
        layerInfoTip("请输入手机号");
    }else if(!checkPhone(phone)){
        layerInfoTip("请填写正确的手机号");
    }else{
        //ajax判断手机号是否正确，正确则发送验证码
        ajaxCheckPhone(phone);
    }
}

/**
 * 验证账号密码
 */
function checkAccountAndPass(loginType){
    var accountVal = $.trim($("#userName").val());
    var passwordVal = $.trim($("#password").val());
    
    if(accountVal == ""){
        layerInfoTip('请输入手机号或者邮箱号！');
    }else if(!checkPhone(accountVal) && !checkMail(accountVal)){
        layerInfoTip('手机号或者邮箱号不正确！');
    }else if(passwordVal == ""){
        layerInfoTip('请输入密码！');
    }else if(!checkPassword(passwordVal)){
        layerInfoTip('登录密码由6-20位字母、数字或者符号两种或以上组成！');
    }else{
        //ajax验证表单
        accountLoginFormSubmit(accountVal,passwordVal,loginType);
    }
}

/**
 * 验证手机号及验证码
 */
function checkIphoneAndCode(loginType){
    var phone = $.trim($("#phone").val());
    var phoneCode = $.trim($("#code").val());

    if(phone == ""){
        layerInfoTip('请输入手机号！');
    }else if(!checkPhone(phone)){
        layerInfoTip('手机号不正确！');
    }else if(phoneCode == ""){
        layerInfoTip('请输入验证码！');
    }else if(!checkCode(phoneCode)){
        layerInfoTip('验证码为4位数字！');
    }else{
        //ajax验证表单
        phoneLoginFormSubmit(phone,phoneCode,loginType);
    }
}

/**
 * ajax判断手机号:1、账号是否正确   2、没有注册过也没有绑定过的手机号码，是否直接创建帐号
 * @param phone
 */
function ajaxCheckPhone(phone){   
   $.post(regurl,{p:phone,type:'reg'},function(data){
	   if(data.code=="0"){
    		layerInfoTip(data.result);
    		codeTimer($("#getPhoneCode"), "time", "发送手机验证码");
    		$("#loginType").val("2");
    		
    	}else if(data.code=="02"){
    		layer.open({
    	        content: '该手机号未注册过，是否直接创建帐号？'
    	        ,btn: ['好的', '否']
    	        ,yes: function(index){
    	            ajaxSendNoRegistCode(phone);   	            
    	            layer.close(index);
    	        }
    	    });
    	}else{
    		layerInfoTip(data.result);
    	}
   })
    
}

function ajaxSendNoRegistCode(phone){
	$("#loginType").val("3");
	$.post(regurl,{p:phone,type:'reg',noreg:1},function(data){
		if(data.code=="0"){
    		layerInfoTip(data.result);
            codeTimer($("#getPhoneCode"), "time", "发送手机验证码");
    	}else{
    		layerInfoTip(data.result);
    	}
	})
	
	
}

function accountLoginFormSubmit(accountVal,passwordVal,loginType){  
    /*ajax_doSubmit("/passport/login/loginCheck.do","data:username="+accountVal+"&password="+passwordVal+"&type="+loginType,function(data){       	
    	if(data.code != "000000") {
            layerInfoTip(data.message);
        } else {
            layerInfoTip("登录成功");
            window.location.href = ctxPath.domain+"/member/index.do";
        }
	});*/
	
	$("#loginPwdType").val(loginType);
	var token = $("input[name=__token__]").val();
	$.post("/loginIndex",{name:accountVal,password:passwordVal,__token__:token,type:$("#loginPwdType").val()},function(e){
				if(e.code==1){
					layerInfoTip("登录成功");
                    window.location.href = member;
				}else{
					$("input[name=__token__]").val(e.token);
					layerInfoTip(e.msg);
					return false;
				}
			})
}

function phoneLoginFormSubmit(phone,phoneCode,loginType){
    /*ajax_doSubmit("/passport/login/loginCheck.do","data:phone="+phone+"&code="+phoneCode+"&type="+loginType,function(data){   
    	console.log(JSON.stringify(data));
    	if(data.code != "000000") {
            layerInfoTip(data.message);
        } else {
            layerInfoTip("登录成功");
            window.location.href = ctxPath.domain+"/member/index.do";
        }
	});*/ 
	$("#loginPhoType").val(loginType);
	var token = $("input[name=__token__]").val();
	$.post("/loginIndex",{name:phone,code:phoneCode,__token__:token,type:2},function(e){
				if(e.code==1){
					layerInfoTip("登录成功");
                    window.location.href = member;
				}else{
					$("input[name=__token__]").val(e.token);
					layerInfoTip(e.msg);
					return false;
				}
			})
}

function dealJump(){
	ajax_doSubmit("/isParticipateNewMemberActivity.html","data:",function(data){ 		
    	if(data!=null && data!="" && data!=undefined){
            if(data.code == "000005" || data.code == "000003"){            	            	
    			window.location.href= member;
            }           
        }
    	
    });
}