/**
 * 账户流水记录
 */
$(function(){
    accountFinanceRecordPageInit();
    
    $(document).on("click", ".data-list .list-item-footer", function(e){
    	if($(this).hasClass('no-right-arrow')){
    		e.preventDefault();  
    	}else{
    		var type = $(this).attr("type");
    		var orderNumber = $(this).attr("orderNumber");
    		viewDetail(type, orderNumber);
    	}
    });
});

function accountFinanceRecordPageInit(){
	var itemIndex = 0;
    var tab1LoadEnd = false;
    var tab2LoadEnd = false;
    var tab3LoadEnd = false;
    var tab4LoadEnd = false;
    var tab5LoadEnd = false;
    
    //tab 切换
    var tabLi = $("#accountFinance-record-tab .nav-item");
    	
    tabLi.click(function(){
    	 var $this = $(this);
         itemIndex = $this.index();
				
		$("#accountFinance-record-tab").find('.nav-link.active').removeClass('active');
        $this.find(".nav-link").addClass('active');
        $('.data-list').eq(itemIndex).removeClass("d-none").siblings(".data-list").addClass("d-none");

        var noDataArea = $('.data-list').eq(itemIndex).find(".null_data").length;		
		if(noDataArea > 0){
			$(".dropload-down").css("display", "none");
		}else{
			$(".dropload-down").css("display", "block");
		}
				
        if(itemIndex == '0'){// 如果选中菜单一
            // 如果数据没有加载完
            if(!tab1LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }        
        }else if(itemIndex == '1'){// 如果选中菜单二
            if(!tab2LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }else if(itemIndex == '2'){// 如果选中菜单三
            if(!tab3LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }else if(itemIndex == '3'){// 如果选中菜单四
            if(!tab4LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }else if(itemIndex == '4'){// 如果选中菜单五
            if(!tab5LoadEnd){
                // 解锁
                dropload.unlock();
                dropload.noData(false);
            }else{
                // 锁定
                dropload.lock('down');
                dropload.noData();
            }
        }
        
        // 重置
        dropload.resetload();
	});
    
    var pageNumber1 = 0;
    var pageNumber2 = 0;
    var pageNumber3 = 0;
    var pageNumber4 = 0;
    var pageNumber5 = 0;
	
    /**
     * @param type: 查询类型
     * @param pageNumber: 查询页码
     * @param $refresh: 下拉刷新对象
     * @param $refreshType: 刷新类型（下拉刷新UPDATE、上拉加载MORE）
     * @param $tabIndex: 查询类型面板索引
     */
    var loadData = function(type, pageNumber, $refresh, refreshType, tabIndex){
    	ajax_doSubmit(moneylog,"data:type="+type+"&pageSize=10&pageNumber="+pageNumber,function(data){
    		if(data == null || data == "" || data.total == 0){
    			// 没有数据
                var nullHtml = '<div class="null_data">' +
                    '<i class="null_finance_ico"></i>' +
                    '<p>暂无相关账户流水哦~</p>' +
                    '<a href="/member.html" class="btn btn-blue">返回个人中心</a>' +
                    '</div>';
                $('.data-list').eq(itemIndex).html(nullHtml);   
                
                // 数据加载完                
            	if(tabIndex == 0){
                	tab1LoadEnd = true;
        		}else if(tabIndex == 1){
            		tab2LoadEnd = true;
        		}else if(tabIndex == 2){
            		tab3LoadEnd = true;
        		}else if(tabIndex == 3){
            		tab4LoadEnd = true;
        		}else if(tabIndex == 4){
            		tab5LoadEnd = true;
        		}
                
                // 无数据
            	$refresh.noData();
                $(".dropload-down").css("display", "none");
    		}else{
    			var d = data.data;
    			var html = '';
				
    			for(var i = 0; i < d.length; i++){
    				
    				var imgSrc = "";
    				var typeHtml = "";
    				var itemFooterClass = "";
    				var changeMoneyClass = "";
    				
    				if(d[i].type == "单卡对换" || d[i].type == "批量对换") // 销卡
    				{
    					imgSrc += '/static/home/images/finance/deta_xiaoka_icon.png';
    					typeHtml += '销卡';
    					itemFooterClass += "list-item-footer row";
    					changeMoneyClass += "color-green";
    				}
    				else if(d[i].type == "提现") // 提现
    				{
    					imgSrc += '/static/home/images/finance/deta_draw_icon.png';
    					typeHtml += '提现';
    					itemFooterClass += "list-item-footer row";
    					changeMoneyClass += "color-pink";
     				}         			
         			else if(d[i].type == "加减金额") // 加款
     				{
						if(parseInt(d[i].price)>0){
							imgSrc += '/static/home/images/finance/deta_add_icon.png';
							typeHtml += '加款';
							itemFooterClass += "list-item-footer row no-right-arrow";
							changeMoneyClass += "color-green";
						}else{
							imgSrc += '/static/home/images/finance/deta_cut_icon.png';
							typeHtml += '扣款';
							itemFooterClass += "list-item-footer row no-right-arrow";
							changeMoneyClass += "color-pink";
						}
     				}
         			else if(d[i].type == "佣金") // 佣金
     				{
         				imgSrc += '/static/home/images/finance/deta_commi_icon.png';
         				typeHtml += '佣金';
         				itemFooterClass += "list-item-footer row no-right-arrow";
         				changeMoneyClass += "color-green";
     				}
         			
    				
    				var remarkHtml= "";
    				if(d[i].data!=null && d[i].data!="" && d[i].data!=undefined){
    					remarkHtml += '<p class="remark">备注：<span class="color9">'+d[i].data+'</span></p>'; 
    				}
   
    				html += '<li class="list-item">' +
	                    '<a class="media" href="javascript:void(0);">' +
	                    '    <img class="w74 align-self-center" src="'+imgSrc+'">' +
	                    '   <div class="media-body">' +
	                    '       <h5 class="tit">单号：'+ d[i].orderno +'</h5>' +
	                    '       <p class="desc text-truncate">类型：<span class="color-blue">'+ typeHtml +'</span></p>' +
	                    '       <p class="desc text-truncate">时间：'+d[i].addtime+'</p>' +
	                    '   </div>' +
	                    '</a>' +
	                    '<div class="'+itemFooterClass+'" type="'+d[i].type+'" orderNumber="'+d[i].orderno+'">' +
	                    '   <div class="col">变动金额：<span class="'+changeMoneyClass+'">￥'+ parseFloat(d[i].price).toFixed(2) +'</span></div>' +
	                    '   <div class="col">变动后：<span class="color-orange">￥'+ parseFloat(d[i].money).toFixed(2) +'</span></div>' +
	                    '</div>'+remarkHtml+'</li>';
    			}
    			
    			if(refreshType == "UPDATE"){
                	$('.data-list').eq(itemIndex).html(html);
                	if(tabIndex == 0){
                		pageNumber1 = 1;
            		}else if(tabIndex == 1){
                		pageNumber2 = 1;
            		}else if(tabIndex == 2){
                		pageNumber3 = 1;
            		}else if(tabIndex == 3){
            			pageNumber4 = 1;
            		}else if(tabIndex == 4){
                		pageNumber5 = 1;
            		}
                	
                	// 数据加载完
                	if(tabIndex == 0){
                    	tab1LoadEnd = false;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = false;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = false;
            		}else if(tabIndex == 3){
                		tab4LoadEnd = false;
            		}else if(tabIndex == 4){
                		tab5LoadEnd = false;
            		}

                    // 无数据
                    $refresh.noData(false);
            	}else{
                	$('.data-list').eq(itemIndex).append(html);
            	}
         		
         		if(data.current_page == data.last_page){
                	// 数据加载完
                    
                    if(tabIndex == 0){
                    	tab1LoadEnd = true;
            		}else if(tabIndex == 1){
                		tab2LoadEnd = true;
            		}else if(tabIndex == 2){
                		tab3LoadEnd = true;
            		}else if(tabIndex == 3){
                		tab4LoadEnd = true;
            		}else if(tabIndex == 4){
                		tab5LoadEnd = true;
            		}

                    // 无数据
                   $refresh.noData();
            	}
    		}
    		
    		// 每次数据加载完，必须重置
    		$refresh.resetload();
    	});
	};
	
	// dropload
    var dropload = $('.tab-content').dropload({
        scrollArea : window,
        // 下拉刷新模块显示内容
        domUp : {
            domClass   : 'dropload-up',
            // 下拉过程显示内容
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            // 下拉到一定程度显示提示内容
            domUpdate  : '<div class="dropload-update">↑释放更新</div>',
            // 释放后显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown : {
            domClass   : 'dropload-down',
            // 滑动到底部显示内容
            domRefresh : '<div class="dropload-refresh">↑上拉加载更多</div>',
            // 内容加载过程中显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            // 没有更多内容-显示提示
            domNoData  : '<div class="dropload-noData">已没有更多数据~</div>'
        },
        loadUpFn : function(me){
            if(itemIndex == '0'){//全部
            	loadData("", 1, me, "UPDATE", itemIndex);            
            }else if(itemIndex == '1'){//销卡
            	loadData("1", 1, me, "UPDATE", itemIndex);
            }else if(itemIndex == '2'){//提现
            	loadData("3", 1, me, "UPDATE", itemIndex);
            }else if(itemIndex == '3'){//佣金
            	loadData("5", 1, me, "UPDATE", itemIndex);
            }else if(itemIndex == '4'){//加/扣款
            	loadData("4", 1, me, "UPDATE", itemIndex);
            }
        }
        ,
        loadDownFn : function(me){
            // 加载菜单一的数据
            if(itemIndex == '0'){
            	loadData("", ++pageNumber1, me, "MORE", itemIndex);
            // 加载菜单二的数据
            }else if(itemIndex == '1'){
            	loadData("1", ++pageNumber2, me, "MORE", itemIndex);
            }else if(itemIndex == '2'){
            	loadData("3", ++pageNumber3, me, "MORE", itemIndex);
            }else if(itemIndex == '3'){
            	loadData("5", ++pageNumber4, me, "MORE", itemIndex);
            }else if(itemIndex == '4'){
            	loadData("4", ++pageNumber5, me, "MORE", itemIndex);
            }
        },
        distance:50
    });

}

/**
 * 根据订单编号，类型获取对应的卖卡记录或提现记录ID
 * @param type
 * @param orderNumber
 */
function viewDetail(type, orderNumber){
	ajax_doSubmit("/m/member/memberFinanceRecord/memberFinanceRecordInit.do","data:type="+type+"&orderNumber="+orderNumber,function(data){   

		if(data!=null && data!="" && data!=undefined){
	        if(type=="1" || type=="2"){//卖卡记录详情           
                window.location.href = ctxPath.domain+'/member/sellCardRecord/getSellCardOrdersDetails.do?type='+type+'&id='+data;
                
            }else if(type=="3"){//提现详情
                window.location.href = ctxPath.domain+'/member/withdrawCashRecord/getDetail.do?id='+data;
            }else{
            	
            }
	    } else {
	        layerInfoTip("详情ID不合法，请检查后重试");
	    }
	});
}