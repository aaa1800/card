/**
 * 帮助中心
 */
$(function(){
	helpPageInit();
});

function helpPageInit(){
	ajax_doSubmit("/getHelpList.html","data:",function(data){    
		//console.log("帮助中心："+JSON.stringify(data));
		if(data!=null && data!="" && data!=undefined){
			var module1 = '';
			var module2 = '';
			var module3 = '';
			
			var curlink = "";
			for(var i=0;i<data.length;i++){
				if(data[i].id=="12" || data[i].id=="9" || data[i].id=="57" || data[i].id=="70"){
					
					module1+='<a class="media" href="/newsDetail.html?id='+data[i].id+'">'+
		                    '<div class="media-body"><p class="clearfix"><span class="float-left left-txt">'+data[i].title+'</span></p>'+
		                '</div>';
					
				}else if(data[i].id=="7" || data[i].id=="11" || data[i].id=="58" || data[i].id=="59"){
					if(data[i].id=="52"){//网站公告
						curlink = "/newsDetail.html";
					}else if(data[i].id=="56"){//常见问题
						curlink = ctxPath.domain+"/setting/faq/index.html";
					}else{
						curlink = '/newsDetail.html?id='+data[i].id;
					}
					module2+='<a class="media" href="'+curlink+'">'+
	                    '<div class="media-body"><p class="clearfix"><span class="float-left left-txt">'+data[i].title+'</span></p>'+
	                '</div>';
					
				}else if(data[i].id=="13" || data[i].id=="14" || data[i].id=="55" || data[i].id=="60"){
					var articleUrl = "";
					if(data[i].id=="53"){//行业资讯
						articleUrl =ctxPath.domain+'/setting/industry/index.html';
					}else if(data[i].id=="60"){
						articleUrl =ctxPath.domain+'/enterpriseRecycle/index.html';
					}else{
						articleUrl = '/newsDetail.html?id='+data[i].id;
					}
					module3+='<a class="media" href="'+articleUrl+'">'+
	                    '<div class="media-body"><p class="clearfix"><span class="float-left left-txt">'+data[i].title+'</span></p>'+
	                '</div>';
				}else{
					//其他内容
				}
			}
			
			$("#module1").append(module1);
			$("#module2").append(module2);
			$("#module3").append(module3);
		}else{
			//无数据
		}
	});
}