/**
 * @description: 设置首页脚本
 * @author：lxf
 * @date: 2018-3-27 09:37:55
 */
$(function(){
    //下拉刷新
    pullToRefreshSettingIndexPage();

});

function pullToRefreshSettingIndexPage(){
    // miniRefresh 对象
    var miniRefresh = new MiniRefresh({
        container: '#minirefresh',
        down: {
            //isLock: true,//是否禁用下拉刷新
            contentdown: '下拉刷新',
            contentover: '释放刷新',
            contentrefresh: '加载中...',
            successAnim: {
                // 下拉刷新结束后是否有成功动画，默认为false，如果想要有成功刷新xxx条数据这种操作，请设为true，并实现对应hook函数
                isEnable: true
            },
            callback: function () {
                window.location.reload();//页面刷新
                miniRefresh.endDownLoading(true, '恭喜您，刷新页面成功');// 结束下拉刷新
            }
        },
        up: {
            isAuto: false,
            callback: function () {
                miniRefresh.endUpLoading();// 结束上拉加载
            }
        }
    });
}

function clearCache(){
    layer.open({
        content: '确定清理应用缓存？'
        ,btn: ['立即清理', '否']
        ,yes: function(index){
            //清理功能待实现
        	window.location.reload(true);       	
            layerInfoTip('已清理完毕本应用的缓存');
            layer.close(index);
        }
    });
}

function loginOut(){
	$.post(loginout,{},function(data){ 	
		if(data.code==1){
			window.location.href=index;
		}
    	
    });	
}