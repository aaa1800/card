$(function(){
	faqPageInit();
});

function faqPageInit(){
	var tab1LoadEnd = false;     
    var pageNumber1 = 0;
    
    /**
     * @param pageSize：每页数量
     * @param pageNumber
     */   
    var loadData = function(pageNumber, $refresh, refreshType){
		$.post(ctxPath,{pageSize:15,pageNumber:pageNumber},function(data){
			  if(data == null || data == "" || data.totalElements == 0){
    			// 没有数据
    			var nullHtml = '<div class="null_data">' +
	                '<i class="null_classify_ico"></i>' +
	                '<p>暂无相关问题哦~</p>' +
	                '<a href="/" class="btn btn-blue">返回个人中心</a>' +
	                '</div>';
                $('.modules-box .list').html(nullHtml);   
                
                // 数据加载完                
                tab1LoadEnd = true;
                
                // 无数据
            	$refresh.noData();
                $(".dropload-down").css("display", "none");
    		}else{
    			var d = data.content;
    			var _number = data.number;
    			var html = '';
    			
    			for(var i = 0; i < d.length; i++){
    				var _No = _number*10+(i+1);
    				html += '<div class="modules-item">'+
					            '<a class="media" href="/newsw.html?id='+d[i].id+'">'+
					                '<div class="media-body">'+
					                    '<p class="clearfix"><span class="float-left left-txt">'+_No+'、'+d[i].title+'</span></p>'+
					                '</div>'+
					            '</a>'+				            
				            '</div>';
    			}
    			
    			if(refreshType == "UPDATE"){
                	$('.modules-box .list').html(html);              	
            		pageNumber1 = 1;
            		tab1LoadEnd = false;
            	
                    // 有数据
                    $refresh.noData(false);
                    
            	}else{
                	$('.modules-box .list').append(html);
            	}
         		
         		if(data.number >= data.totalPages - 1){
                	// 数据加载完                   
         			tab1LoadEnd = true;

                    // 无数据
                   $refresh.noData();
            	}
    		}
    		
    		// 每次数据加载完，必须重置
    		$refresh.resetload();
		})
    	
	};
	
	var dropload = $('.modules-box').dropload({
        scrollArea : window,
        // 下拉刷新模块显示内容
        domUp : {
            domClass   : 'dropload-up',
            // 下拉过程显示内容
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            // 下拉到一定程度显示提示内容
            domUpdate  : '<div class="dropload-update">↑释放更新</div>',
            // 释放后显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown : {
            domClass   : 'dropload-down',
            // 滑动到底部显示内容
            domRefresh : '<div class="dropload-refresh">↑上拉加载更多</div>',
            // 内容加载过程中显示内容
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            // 没有更多内容-显示提示
            domNoData  : '<div class="dropload-noData">已没有更多数据~</div>'
        },
        autoLoad: true,
        loadUpFn : function(me){
            // 加载菜单一的数据
        	loadData(1, me, "UPDATE");
        }
        ,
        loadDownFn : function(me){
            // 加载菜单一的数据
        	loadData(++pageNumber1, me, "MORE");  
        },
        distance:50
    });
	
}