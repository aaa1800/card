/*!
  * author：lxf
  * date: 2018-4-16 16:31:04
  * description: 记录用户上次提交成功的卡密所选择的卡类、卡种、面值
  */
/**
 * 充值卡对象
 * @param cardType：卡类
 * @param cardOperator：卡种
 * @param cardFaceVal：面值
 */
function Card(cardType, cardOperator, cardFaceVal) {
    this.cardType = cardType;
    this.cardOperator = cardOperator;
    this.cardFaceVal = cardFaceVal;
}

function addUserSelectedOptionsCookies(card) {
    var cookieSet = {expires: 7,path:'/',domain:ctxPath.cookieDomain};
    $.cookie("cardType", card.cardType, cookieSet);
    $.cookie("cardOperator", card.cardOperator, cookieSet);
    $.cookie("cardFaceVal", card.cardFaceVal, cookieSet);
}