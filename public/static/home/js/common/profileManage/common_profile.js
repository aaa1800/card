/**
 * date： 2018-1-16 10:03:41
 * author： lxf
 * description: 资料管理-公共脚本
 */
$(function(){
    //tab点击
    $('.tabs-container .J_menuItem').on('click', function(){
        var curHref = $(this).attr("curhref");
        var index = $(this).parent("li").index();
        addFrame(curHref,"01100"+index,$(this).text());
    });
	var icon = "<i class='fa fa-times-circle'></i>";
	$("#submitRealNameFormBtn").click(function(){

		var name=$("#name").val(),idcard=$("#idCard").val(),$errorObj=$("#realNameForm .errorTip");
		if(name=="" || !name){
			$errorObj.html(icon+" 请输入真实姓名");
			return;
		}
		if(!isIdCardNo(idcard)){
			$errorObj.html(icon+" 身份证号码错误");
			return;
		}
		$.post(realNameAuthenInit,{username:name,idcard:idcard},function(e){
			$errorObj.html("");
			if(e.code==1){
			   parent.layer.msg(e.msg, {icon: 1,shade: [0.4, '#000'],time : 1000},function(){
				   location.reload();
			   });
			}else{
				parent.layer.msg(e.msg, {icon: 2,shade: [0.4, '#000'],time : 1000});
			}
		});
		
	});
	$("#submitPhoneSetIndexFormBtn").click(function(){
		var tel=$("#phone").val(),code=$("#phoneCode").val(),$errorObj=$("#phoneSettingIndexForm .errorTip");
		
	});
	$("#getPhoneCodea").click(function(){
		var pwd=$("#password").val(),tel=$("#phone").val(),$errorObj=$("#phoneSettingIndexForm .errorTip");
		if(verifyPhone(tel, $errorObj)){
			if(pwd==null || pwd==""){$errorObj.html(icon+"请输入密码");return;}
			$("#phone").attr("disabled",'disabled');console.log(pwd);
			$.post(bindtel,{photo:tel,pwd:pwd},function(e){
				console.log(2);
				if(e.code==1){
					parent.layer.msg(e.msg, {icon: 1,shade: [0.4, '#000'],time : 1000});
				}else{
					$("#phone").removeAttr("disabled");
					$errorObj.html(icon+e.msg);
				}
			});
		}
	});

});

//账户总览
function gotoAccountOverviewPage() {
    addFrame( member,"0","账户总览");
}

//马上去实名认证
function gotoRealnamePage() {
    addFrame( realNameAuthenInit,"011002","实名认证");
}

//进入手机设置页面
function gotoPhoneSet() {
    addFrame( phoneSetting,"011001","手机设置");
}

//进入邮箱设置页面
function gotoMailSet() {
    addFrame( bindemail,"011005","邮箱设置");
}

//进入QQ绑定页面
function gotoQQbindPage() {
    addFrame( ctxPath.domain+'/member/profileManage/qqBind/index.do',"011006","QQ绑定");
}

//进入微信绑定页面
function gotoWXbindPage() {
    addFrame( ctxPath.domain+'/member/profileManage/bindWeChat/index.do',"011007","微信绑定");
}

//进入关注公众号页面
function gotoFollowWxPage() {
    addFrame( ctxPath.domain+'/member/profileManage/subscribeOfficialWeixinAccount/index.do',"011008","关注公众号");
}

//验证手机
function verifyPhone(phone, $errorObj){
    var icon = "<i class='fa fa-times-circle'></i>";
    // var phoneReg = /^1[34587]\d{9}$/;
    var phoneReg = /^1\d{10}$/;
    if(phone == ""){
        $errorObj.html(icon+" 请输入手机号码");
        return false;
    }else{
        if(phoneReg.test(phone)){
            $errorObj.html("");
            return true;
        }else{
            $errorObj.html(icon+" 手机号格式不正确");
            return false;
        }
    }
}

//验证邮箱
function verifyMail(mail, $errorObj, errorTxt){
    var icon = "<i class='fa fa-times-circle'></i>";
    var mailReg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if(mail == ""){
        $errorObj.html(icon+" 请输入"+errorTxt);
        return false;
    }else{
        if(mailReg.test(mail)){
            $errorObj.html("");
            return true;
        }else{
			console.log(55);
            $errorObj.html(icon + errorTxt +" 格式不正确");
            return false;
        }
    }
}

//验证密码 (密码由6-20位字母、数字或者符号两种或以上组成)
function verifyPassword(pwd, $errorObj, errorTxt){
    var icon = "<i class='fa fa-times-circle'></i>";
    var pwdReg = /^(?![A-z]+$)(?!\d+$)(?![\W_]+$)^.{6,20}$/;

    if(pwd == ""){
        $errorObj.html(icon+" 请输入"+errorTxt);
        return false;
    }else{
    	
    	if(pwd.indexOf(" ")!=-1){
    		$errorObj.html(icon+"交易密码不能有空格");
            return false;
    	}
    	
        if(pwdReg.test(pwd)){
            $errorObj.html("");
            return true;
        }else{
            $errorObj.html(icon + errorTxt +"由6-20位字母、数字或者符号两种或以上组成");
            return false;
        }
    }
}

//验证验证码
function verifyCode(code, $errorObj){
    var icon = "<i class='fa fa-times-circle'></i>";
    var codeLength = 6;
    if(code == ""){
        $errorObj.html(icon+" 请输入验证码");
        return false;
    }else{
		return true;
	}
}

function sendVertifyCode($getCodeBtn) {
    var time = 60;
    if ($getCodeBtn.hasClass("btn-danger")) {
        //ajax发送验证码

        ajaxSendVertifyCode($getCodeBtn, time);
    }else{
        parent.layer.msg('请不要一直点击', {icon: 2,shade: [0.4, '#000'],time : 1000});
    }
}

//验证码发送成功提示
function noticeCodeSendSuccess(message, $getCodeBtn, time){
    parent.layer.msg(message, {icon : 1,shade : [ 0.4, '#000' ],time : 1000}, function(){
        $getCodeBtn.removeClass("btn-danger");
        $getCodeBtn.addClass("btn-default disabled");
        var t = setInterval(function(){
            time--;
            $getCodeBtn.html("重新发送（" + time + "）");

            if (time==0) {
                clearInterval(t);
                $getCodeBtn.html("获取验证码");
                $getCodeBtn.removeClass("btn-default disabled");
                $getCodeBtn.addClass("btn-danger");
            }
        },1000);
    });
}

//身份证号码验证
function isIdCardNo(num) {
	var idCard = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return idCard.test(num);
}
