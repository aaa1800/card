/*
	页面共同调用的方法
 */
$(function(){
    /**
     * @description: 移动出现导航栏
     */
    $(".head-view-con").hover(function() {
        $(this).find(".head-view-sort").show();
    }, function() {
        $(this).find(".head-view-sort").hide();
    });

    //置顶图标显示
	$(window).scroll(function(){
		 if($(this).scrollTop() > 100){
			$(".back-top").fadeIn();
		 }
		 else{
			$(".back-top").fadeOut();
		 }
	  });
	//置顶事件
	$("#btnTop").click(function(event) {
		$('body,html').animate({scrollTop:0},300);
	});

	//客服鼠标移动出现提示
	$(".service-float").hover(function() {
		$(this).addClass('service-float-block');
	}, function() {
		$(this).removeClass('service-float-block');
	});
	/**
	* @description: 帮助中心的效果
	*/
	$('.list-comm-right .list-right-main ul.list-faq-cont li').click(function(event) {
		$(this).addClass('active').siblings().removeClass('active');
	});
	/**
	* @description: 卡劵详情的效果
	*/
	$(".list-ifold-menu").click(function(event) {
		if($(this).parent('.list-ifold').hasClass('iunfold')){
			$(this).parent('.list-ifold').removeClass('iunfold');
		}else{
			$(this).parent('.list-ifold').addClass('iunfold').siblings().removeClass('iunfold');
		}
	});
	$(".aside-menu li").click(function(event) {
		$(this).addClass('active').siblings().removeClass('active');
	});



	

});

function autoScrollToTarget(distance) {
    $("body,html").animate({
        scrollTop: distance
    }, 400);
}

function meslDurationFormat(msel){
	var num = 1000 * 60;
    var days = parseInt(msel / (1000 * 60 * 60 * 24));
    var hours = parseInt((msel % (num * 60 * 24)) / (num * 60));
    var minutes = parseInt((msel % (num * 60)) / num);
    var seconds = (msel % num) / 1000;

    daysStr = (days > 0) ? days + " 天 " : "";
    hoursStr = (hours > 0) ? hours + " 小时 " : "";
    minutesStr = (minutes > 0) ? minutes + " 分钟 " : "";
    secondsStr = (seconds > 0) ? seconds + " 秒 " : "";

    return daysStr + hoursStr + minutesStr + secondsStr;
}