var withdrawTimeStart = {//提现时间起
    elem: "#timeStart",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        withdrawTimeEnd.min = datas;
        withdrawTimeEnd.start = datas;
        $("#withdrawStartDate").val(datas);
        $("#withdrawRegion").val("CUSTOM");
    }

};
var withdrawTimeEnd = {//提现时间止
    elem: "#timeEnd",
    format: "YYYY-MM-DD hh:mm:ss",
    istime: true,
    istoday: true,
    max:laydate.now(),
    choose: function (datas) {
        $("#dateBtns button").removeClass("btn-primary").addClass("btn-default");
        withdrawTimeStart.max = datas;
        $("#withdrawEndDate").val(datas);
        $("#withdrawRegion").val("CUSTOM");
    }
};


$(document).ready(function () {
    laydate(withdrawTimeStart);
    laydate(withdrawTimeEnd);

    //时间按钮点击事件
    $("#queryForm .button-group button").click(function(e) {
        var parentId = $(this).parent().attr("id");
        $(this).removeClass("btn-default").addClass("btn-primary").siblings().removeClass("btn-primary").addClass("btn-default");

        if(parentId == "dateBtns"){
            $("#withdrawRegion").val($(this).attr("region"));
        }

        getOrderQueryDate(parentId, $(this).attr("region"));

    });

    //查询
    $("#search").click(function(){
        changePage(ctxPath, "queryForm", "queryPagination", "refreshWithdrawRecordData", "withdrawRecordPageNumber", 1);
    });

    //给enter绑定查询事件
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $('#search').click();
        }
    });

    //分页--每页显示多少条数据
    $("#queryPagination").on("click", ".dropdown-menu a", function(){
        var pageSize = $.trim($(this).html());
        $("#pageSize").val(pageSize);
        $("#queryPagination").find(".page-size").val(pageSize);
        changePage(ctxPath, "queryForm", "queryPagination", "refreshWithdrawRecordData", "withdrawRecordPageNumber", 1);
    });

    //提现时间默认今天
    $('#dateBtns button[region="TODAY"]').click();


    //获取表格数据
    changePage(ctxPath, "queryForm", "queryPagination", "refreshWithdrawRecordData", "withdrawRecordPageNumber", 1);
});

//显示表格数据
function refreshWithdrawRecordData(data){
    // console.log("我是订单记录："+JSON.stringify(data));
    if(data != null) {
        var d = data.data;
        if(d.length > 0){
            var html = '';
            for(var i = 0; i < d.length; i++){
                var typeStr=d[i].type;
                var accountStr=isEqUndefined(d[i].zh);
                
                var status=parseInt(d[i].status);
                // 状态 1：处理中 2：成功 3：失败 4:未知
                var statusHtml='';
                switch (status){

                    case 1:
                        statusHtml='<span class="text-green">处理成功</span>';
                        break;
                    case 3:
                        statusHtml='<span class="text-red">处理失败</span>';
                        break;
                    case 4:
                        statusHtml='<span class="text-dark-blue">正在核实</span>';
                        break;
                    case 2:
                        statusHtml='<span class="text-red">退款</span>';
                        break;
                    case 6:
                        statusHtml='<span class="text-dark-blue">正在处理</span>';
                        break;
					default:
					  statusHtml='<span class="text-dark-blue">正在处理</span>';
                }
                var id="'"+d[i].id+"'";
                html+='<tr>' +
                    '<td>'+d[i].order+'</td>' +
                    '<td>'+typeStr+'</td>' +
                    '<td>'+isEqUndefined(d[i].memberRealName)+'</td>' +
                    '<td>'+accountStr+'</td>' +
                    '<td>￥'+numberFormatter(d[i].money)+'</td>' +
                    '<td>￥'+numberFormatter(d[i].poundage)+'</td>' +
                    '<td>￥'+numberFormatter(d[i].shiji)+'</td>' +
                    '<td>'+d[i].addtime+'</td>' +
                    '<td>'+d[i].shtime+'</td>' +
                    '<td>'+statusHtml +'</td>' +
                    '<td><a class="text-blue text-underline" href="javascript:viewDetail('+id+');">查看详情</a></td>' +
                    '</tr>';
            }
            $("#dataTable tbody.dataWraper").html(html);
            $("#queryPagination").show();//显示分页

        } else {
            dataEmpty($("#dataTable"), "queryPagination");
        }
    } else {
        dataEmpty($("#dataTable"), "queryPagination");
    }
}



function getOrderQueryDate(parentId, region) {
    var AddDayCount = '';
    if(region == "LASTDAY"){
        AddDayCount = -1;
    }else if(region == "TODAY"){
        AddDayCount = 0;
    }else if(region == "WEEK"){
        AddDayCount = -7;
    }else if(region == "1"){
        AddDayCount = -31;
    }else if(region == "3"){
        AddDayCount = -92;
    }else if(region == "NONE"){
        AddDayCount = '';
    }

    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    var year = dd.getFullYear(); //获取当前年，四位数
    var month = dd.getMonth()+1;//获取当前月
    var day = dd.getDate();  //获取当前日
    var dateStartPart = year + "-";
    var dateEndPart = '';

    if(region == "LASTDAY"){
        dateEndPart = year + "-";
        if(month < 10)
            dateEndPart += "0";
        dateEndPart += month + "-";
        if(day < 10)
            dateEndPart += "0";
        dateEndPart += day;

    }else{
        var mydate = new Date();
        var curyear = mydate.getFullYear();
        var curmonth = mydate.getMonth()+1;//获取当前月
        var curday = mydate.getDate();//获取当前月
        dateEndPart = curyear + "-";
        if(curmonth < 10)
            dateEndPart += "0";
        dateEndPart += curmonth + "-";
        if(curday < 10)
            dateEndPart += "0";
        dateEndPart += curday;
    }

    if(month < 10)
        dateStartPart += "0";
    dateStartPart += month + "-";
    if(day < 10)
        dateStartPart += "0";
    dateStartPart += day;
    var fullStartDate = dateStartPart + " " + "00" + ":" + "00" + ":" + "00";
    var fullEndDate = dateEndPart + " " + "23" + ":" + "59" + ":" + "59";

    if(region == "NONE"){
        if(parentId == "dateBtns"){
            $("#timeStart").val("");
            $("#timeEnd").val("");

            $("#withdrawStartDate").val("");
            $("#withdrawEndDate").val("");
        }
    }else{
        if(parentId == "dateBtns"){
            $("#timeStart").val(fullStartDate);
            $("#timeEnd").val(fullEndDate);

            $("#withdrawStartDate").val(fullStartDate);
            $("#withdrawEndDate").val(fullEndDate);
        }
    }

}


/**
 * 根据id查看提现详情
 * @param id
 */
function viewDetail(id){
    addFrame( detail+'?id='+id , '7777', '提现详情');
}