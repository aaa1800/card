
$(function(){
	
	var oLoginError = $('.login-from-prompt');
	
	init();
	
	var validCode=true;
	var oIphone = function(B) {
        var A = /^1\d{10}$/;
        if (!A.exec(B)) {
            return false
        } else {
            return true
        }
    };
    var oMailbox = function(A) {
        var B = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (B.test(A)) {
            return true
        }
        return false
    };
    var oPassWork = function(B) {
        var A = /^(?![A-z]+$)(?!\d+$)(?![\W_]+$)^.{6,20}$/;
        if (!A.exec(B)) {
            return false
        }
        return true
    };


	/**
	* @description: 登陆框切换
	*/
	$('.login-tit span').click(function(event) {
		$(this).addClass('active').siblings().removeClass('active');
		$('.login-content').eq($(this).index()).addClass('active').siblings().removeClass('active');
		if($(this).index() === 0){
			$("#AccountLogin").attr("logintype","accountLogin");
			$("#type").val("1");
		}else{
			$("#AccountLogin").attr("logintype","iphoneLogin");
			$("#type").val("2");
		}
	});
		
	/**
	* @description: 记住密码
	*/
	$(".login-remember .login-icon-on").click(function(event) {
		
		if($(this).hasClass('login-icon-off')){
             $(this).removeClass('login-icon-off');   
             $("input[name='remember-me']").prop("checked",false);
		}else{
			$(this).addClass('login-icon-off');
			$("input[name='remember-me']").prop("checked",true);
		}
	});

	/**
	* @description: 点击登陆
	*/
	$("#AccountLogin").click(function(event) {

		var oType = $(this).attr('logintype');
		
		if($('.com-btn').hasClass('btn-gray')){
			layer.msg('请不要一直点击！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else{
			if(oType === 'accountLogin'){
				//验证账号密码
				checkAccount();
			}else if(oType === 'iphoneLogin'){
				//验证手机号
				checkIphone();
			}
			
		}
	});



	/**
	* @description: 验证账号跟密码
	*/
	function checkAccount(){
		var oName = $('#AccountName').val().trim();
		var oPass = $('#AccountPass').val().trim();
		if(oName === ""){
			$('#AccountName').focus();
			fnLoginError('请输入手机号或者邮箱号！');
			return false;
		}else if(!oIphone(oName) && !oMailbox(oName)){
			fnLoginError('手机号或者邮箱号不正确！');
			return false;
		}else if(oPass === ""){
			$('#AccountPass').focus();
			fnLoginError('请输入密码！');
			return false;
		}else if(!oPassWork(oPass)){
			$('#AccountPass').focus();
			fnLoginError('登录密码由6-20位字母、数字或者符号两种或以上组成');
			return false;
		}else{
			//alert($("#type").val());
			fnLoginSuccess('验证成功！正在登陆...');
			//return true;*/
			//$("#loginForm").submit();
		}
		var token = $("input[name=__token__]").val();
		$.post("/loginIndex",{name:oName,password:oPass,__token__:token,type:1},function(e){
				if(e.code==1){
					fnLoginSuccess (e.msg,true);
				}else{
					$("input[name=__token__]").val(e.token);
					fnLoginError(e.msg);
					$("#AccountLogin").removeClass('btn-gray');
		            $("#AccountLogin").text('登录');
					return false;
				}
			})

	}

	$('.login-mainly input').click(function(event) {
		$(this).parent().addClass('border-blue');
	}).blur(function(event) {
		$(this).parent().removeClass('border-blue');
	}).focus(function(event) {
		$(this).parent().addClass('border-blue');
	}).keyup(function(event) {
		if($(this).val() === ''){
			$(this).parent().find('.inputIcon-close').hide();
		}else{
			$(this).parent().find('.inputIcon-close').show();
		}
	});
	

	/**
	* @description: 验证手机跟验证码
	*/
	function checkIphone(){
		var oName = $("#IphoneName").val().trim();
		var oCode = $("#code").val().trim();
		var rego = /^.{4}$/;
		//codeNum = 12345;
		if(oName === ""){
			$('#IphoneName').focus();
			fnLoginError('请输入手机号！');
			return false;
		}else if(!oIphone(oName)){
			$('#IphoneName').focus();
			fnLoginError('输入的手机号不正确！');
			return false;
		}else if(oCode === ""){
			$("#code").focus();
			fnLoginError('验证码不能为空！');
			return false;
		}else if(!rego.test(oCode)){
			$("#code").focus();
			fnLoginError('验证码为4位数字！');
			return false;
		}/*else if($("#code").val() != codeNum){
			$("#code").focus();
			fnLoginError('验证码不正确！');
			return false;
		}*/else{
			/*fnLoginSuccess('验证成功！正在登陆...');
			return true;*/
			//alert($("#type").val());
			$.post("/loginIndex",{name:oName,code:oCode,type:2},function(e){
				if(e.code==1){
					fnLoginSuccess (e.msg,true);
				}else{
					$("input[name=__token__]").val(e.token);
					fnLoginError(e.msg);
					$("#AccountLogin").removeClass('btn-gray');
		            $("#AccountLogin").text('登录');
					return false;
				}
			})
		}

	}
	//去掉文本框的内容
	$(".login-mainly .inputIcon-close").click(function(event) {
		var oCloseVal = $(this).parent().find('input');
		if(oCloseVal.val() === ''){
			$(this).hide();
		}else{
			$(this).hide();
			oCloseVal.val('');
		}
	});

	/**
	* @description: 点击获取验证码
	*/
	$('.login-code-btn').click(function(event) {
		var oIphoneName = $("#IphoneName").val().trim();
		$("#type").val("2");
		if(oIphoneName === ""){
			layer.msg('请填写手机号！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else if(!oIphone(oIphoneName)){
			layer.msg('请填写正确的手机号！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else{
			
			$.ajax({
		        url:regurl,
		        data:{p:$("input[name='phone']").val(), type:"reg"},
		        dataType: "json",
		        async: true,
		        type: "POST",
		        beforeSend: function() {
		        },
		        success: function(data) {
		           if(data.code=="0")
		           {
		        	   CountDownCode();
		        	   layer.msg(data.result, {icon : 1,shade : [ 0.4, '#000' ],time : 1000},function(){
		                   //window.location.reload();
		               });
		           }
		           else
		    	   {
		        	   // 账号不存在，询问用户是否要注册
		        	   if(data.code == "02")
		        	   {
		        		   layer.alert('<span style="color:#f4a322;">该手机还未注册，是否直接注册？</span>', {
		        		        title: "验证码登录提示",
		        		        icon: 0,
		        		        skin: 'layer-ext-moon',
		        		        btn: ['直接注册'],
		        		        yes: function(index, layero){
		        		            //alert("进行注册");
		        		            $("#type").val("3");
		        		            sendRegisterCode();
		        		        },
		        		        cancel: function(index, layero){//右上角关闭按钮回调

		        		        }
		        		    });
		        	   }
		        	   else
	        		   {
		        		   layer.msg(data.result, {icon : 2,shade : [ 0.4, '#000' ],time : 1000});
	        		   }
		    	   }
		        },
		        complete: function() {
		        },
		        error: function(e) {
		        }
		    });
		}
	});
	
	/**
	* @description: 发送验证码直接注册登录
	*/
	var sendRegisterCode = function()
	{
		var oIphoneName = $("#IphoneName").val().trim();
		if(oIphoneName === ""){
			layer.msg('请填写手机号！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else if(!oIphone(oIphoneName)){
			layer.msg('请填写正确的手机号！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}else{
			$.ajax({
		        url:regurl,
		        data:{p:$("input[name='phone']").val(), type:'reg','noreg':1},
		        dataType: "json",
		        async: true,
		        type: "POST",
		        beforeSend: function() {
		        },
		        success: function(data) {
		           if(data.code=="000000")
		           {
		        	   CountDownCode();
		        	   layer.msg(data.result, {icon : 1,shade : [ 0.4, '#000' ],time : 1000},function(){
		                   //window.location.reload();
		               });
		           }
		           else
		    	   {
		        	   layer.msg(data.result, {icon : 2,shade : [ 0.4, '#000' ],time : 1000});
		    	   }
		        },
		        complete: function() {
		        },
		        error: function(e) {
		        }
		    });
		}
		
	}
	
	var countdown=60; 
	var CountDownCode  = function(){
		if(validCode){
			validCode = false;
			//layer.msg('验证码发送成功！', {icon: 1,shade: [0.4, '#000'],time : 1000});
			var time = setInterval( function(){
				countdown--;
				$('.login-code-btn').addClass('login-code-gray');
				$('.login-code-btn').html('重新发送('+countdown+')');
				if(countdown === 0){
					clearInterval(time);
					$('.login-code-btn').removeClass('login-code-gray');
					$('.login-code-btn').html('获取验证码');
					validCode=true;
					countdown=60; 
				}
			},1000)
		}else{
			layer.msg('请不要一直点击！', {icon: 2,shade: [0.4, '#000'],time : 1000});
		}
	}

	/**
	* @description: 登陆提示报错
	*/
	function fnLoginError (str){
		var oHtml = '<span class="error-info"><i class="comm-icon login-error-icon"> </i>'+str+'</span>'
		oLoginError.html(oHtml);
		infoNullTimer();
	}
	/**
	* @description: 登陆提示成功
	*/
	function fnLoginSuccess (str,rf){
		var rf=rf||false;
		var oHtml = '<span class="error-info success-info txt-green"> <i class="comm-icon login-success-icon"> </i>'+str+'</span>';
		oLoginError.html(oHtml);
		$("#AccountLogin").addClass('btn-gray');
		$("#AccountLogin").text('登录中...');
		infoNullTimer();
		if(rf){
			  window.location.href="/member.html";
		}
	}
	/**
	* @description: 去掉提示
	*/
	function infoNullTimer()  
	{  
	    if(oLoginError.html() != ''){
			setTimeout(function () { 
				oLoginError.html('');
			},3000);
		}
		
	}  
	
	/**
	* @description: 页面初始化
	*/
	function init()
	{
		// 如果登陆失败，则显示登陆失败的错误信息
		if($("#error").val() != "")
		{
			fnLoginError($("#error").val());
		}
		
		// 如果登陆失败则切换至上次选择的登陆方式
		var loginType = $("#loginType").val();
		if(loginType != null && loginType != "")
		{
			if(loginType == "2")
			{
				$('.login-tit span').eq(1).addClass('active').siblings().removeClass('active');
				$('.login-content').eq(1).addClass('active').siblings().removeClass('active');
				$("#AccountLogin").attr("logintype","iphoneLogin");
				$("#type").val("2");
			}
			else if(loginType == "1")
			{
				$('.login-tit span').eq(0).addClass('active').siblings().removeClass('active');
				$('.login-content').eq(0).addClass('active').siblings().removeClass('active');
				$("#AccountLogin").attr("logintype","accountLogin");
				$("#type").val("1");
			}
			else
			{
				$("#type").val("3");
			}
		}
	}
});