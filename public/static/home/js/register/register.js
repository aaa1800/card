
$(function(){
    var oRegisterError = $("#RegisterError");
    var oRegisterName = $("#RegisterName");
    var oRegisterPass = $("#RegisterPass");
    var oReinput =$("#Reinput");
    var oInviteNumber = $("#inviteNumber");
    var oRegisterCode = $("#RegisterCode");
    var oRegCodeBtn = $("#RegCodeBtn");
    var validCode = true;
    var oIphone = function(B) {
        var A = /^1\d{10}$/;
        if (!A.exec(B)) {
            return false
        } else {
            return true
        }
    };
    var oMailbox = function(A) {
        var B = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (B.test(A)) {
            return true
        }
        return false
    };
    var oPassWork = function(B) {
        var A = /^(?![A-z]+$)(?!\d+$)(?![\W_]+$)^.{6,20}$/;
        if (!A.exec(B)) {
            return false
        }
        return true
    };

    oRegisterPass.keyup(function(event) {
        if($(this).val() === ''){
            $(".register-cue-box").show();
        }else{
            $(".register-cue-box").hide();
        }
    });
    $(".login-mainly input").click(function(event) {
        $(this).parent().addClass('border-blue');
    }).focus(function(event) {
        $(this).parent().addClass('border-blue');
    }).keyup(function(event) {
        if($(this).val() === ''){
            $(this).parent().find('.inputIcon-close').hide();
        }else{
            $(this).parent().find('.inputIcon-close').show();
        }
    }).blur(function(event) {
        $(this).parent().removeClass('border-blue');
    });

    /**
     * @description: 注册按钮
     */
    $("#registerBtn").click(function(event) {
        var oRegisterNameVal = oRegisterName.val().trim();
        var oRegisterPassVal = oRegisterPass.val().trim();
        var oReinputVal = oReinput.val().trim();
        var oInviteNumberVal = oInviteNumber.val().trim();
        var numberReg = /^\d{1,20}$/;

        if(oRegisterNameVal === ''){
            oRegisterName.focus();
            fnLoginError('请输入手机号或邮箱号！');
            return false;
        }else if(!oIphone(oRegisterNameVal) && !oMailbox(oRegisterNameVal)){
            oRegisterName.focus();
            fnLoginError('手机号或者邮箱号不正确！');
            return false;
        }else if(oRegisterPassVal === ''){
            oRegisterPass.focus();
            fnLoginError('请输入登陆密码！');
            return false;
        }else if(!oPassWork(oRegisterPassVal)){
            oRegisterPass.focus();
            fnLoginError('注册密码由6-20位字母、数字或者符号两种或以上组成!');
            return false;
        }else if(oReinputVal === ''){
            oReinput.focus();
            fnLoginError('请再次确认密码！');
            return false;
        }else if(oReinputVal != oRegisterPassVal ){
            oReinput.focus();
            fnLoginError('两次密码不一致！');
            return false;
        }else if(oInviteNumberVal!= ''){
            if(!numberReg.test(oInviteNumberVal)){
                oInviteNumber.focus();
                fnLoginError('推广员用户编号由1-20位数字组成！');
                return false;
            }else{
            	ajaxVerifyMemberNumber(oInviteNumberVal);
            }
        }else{
            verifyRestFormEle();
		}

    });
    //去掉文本框的内容
    $(".login-mainly .inputIcon-close").click(function(event) {
        var oCloseVal = $(this).parent().find('input');
        if(oCloseVal.val() === ''){
            $(this).hide();
        }else{
            $(this).hide();
            oCloseVal.val('');
        }
    });
    /**
     * @description: 发送验证码
     */
    $("#RegCodeBtn").click(function(event) {
        var oRegisterNameVal = oRegisterName.val().trim();
        if(oRegisterNameVal === ''){
            layer.msg('请填写手机！', {icon: 2,shade: [0.4, '#000'],time : 1000});
        }else if(!oIphone(oRegisterNameVal) && !oMailbox(oRegisterNameVal)){
            layer.msg('手机号不正确！', {icon: 2,shade: [0.4, '#000'],time : 1000});
        }else{
            CountDownCode();
            $.ajax({
                url:regurl,
                data:{p:oRegisterNameVal,'noreg':11,'yreg':2},
                dataType: "json",
                async: true,
                type: "POST",
                beforeSend: function() {
                },
                success: function(data) {
                    if(data.code=="0")
                    {
                        layer.msg(data.result, {icon : 1,shade : [ 0.4, '#000' ],time : 1000},function(){
                            //window.location.reload();
                        });
                    }
                    else
                    {
                        layer.msg(data.result, {icon : 2,shade : [ 0.4, '#000' ],time : 1000});
                    }
                },
                complete: function() {
                },
                error: function(e) {
                }
            });
        }


    });
    var countdown = 60;
    var  CountDownCode = function(){

        if(validCode){
            validCode  = false;
            //layer.msg('验证码发送成功！', {icon: 1,shade: [0.4, '#000'],time : 1000});
            var time = setInterval(function(){
                countdown --;
                oRegCodeBtn.addClass('login-code-gray');
                oRegCodeBtn.html('重新发送('+countdown+')');
                if(countdown == 0){
                    clearInterval(time);
                    oRegCodeBtn.removeClass('login-code-gray');
                    oRegCodeBtn.html('获取验证码');
                    validCode  = true;
                    countdown = 60;
                }

            },1000);
        }else{
            layer.msg('请不要一直点击！', {icon: 2,shade: [0.4, '#000'],time : 1000});
            return false;
        }

    }
    /**
     * @description: 显示密码
     */
    $(".login-icon-on").click(function(event) {

        if($(this).hasClass('login-icon-off')){
            oReinput.attr('type','password');
            oRegisterPass.attr('type','password');
            $(this).removeClass('login-icon-off');
        }else{
            oReinput.attr('type','text');
            oRegisterPass.attr('type','text');
            $(this).addClass('login-icon-off');
        }

    });

    /**
     * @description: 提示报错
     */
    function fnLoginError (str){
        var oHtml = '<span class="error-info"><i class="comm-icon login-error-icon"> </i>'+str+'</span>'
        oRegisterError.html(oHtml);
        infoNullTimer();
    }
    /**
     * @description: 提示成功
     */
    function fnLoginSuccess (str){
        var oHtml = '<span class="error-info success-info txt-green"> <i class="comm-icon login-success-icon"> </i>'+str+'</span>';
        oRegisterError.html(oHtml);
        infoNullTimer();
    }
    /**
     * @description: 去掉提示
     */
    function infoNullTimer()
    {
        if(oRegisterError.html() != ''){
            setTimeout(function () {
                oRegisterError.html('');
            },3000);
        }

    }

    /**
	 * @description: ajax验证用户编号
     */
    function ajaxVerifyMemberNumber(oInviteNumberVal){
        $.ajax({
            url: regnum,
            data: {id:oInviteNumberVal, _csrf:$("input[name='_csrf']").val()},
            dataType: "json",
            async: true,
            type: "POST",
            beforeSend: function() {},
            success: function(data) {
                if (data.code==1) {
					verifyRestFormEle();
                }  else {
                    fnLoginError("邀请码错误，没有此用户");
				}
            },
            complete: function() {},
            error: function(e) {}
        });
	}

	function verifyRestFormEle(){
        $.ajax({
                url:$("#registerForm").attr("action"),
                data:$("#registerForm").serialize(),
                dataType: "json",
                async: true,
                type: "POST",
                beforeSend: function() {},
                success: function(data) {
                    if (data.code==1) {
                        document.location.href = "/loginIndex.html";
                    } else {
                         fnLoginError(data.msg);
                       
                    }
                },
                complete: function() {},
                error: function(e) {}
            });
		}

});