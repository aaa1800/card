$(function(){
    /**
     * @description: 鼠标移动改变icon
     */
    $(".main-choice-list li").hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });
	/**
	* @description: 鼠标移动顶部帮助出现浮动层 
	*/
	$(".site-nav-help ").hover(function() {
		$(this).addClass('site-nav-block');
	}, function() {
		$(this).removeClass('site-nav-block');
	});

	/**
	* @description: 顶部头像鼠标移动出现浮动层
	*/
	$(".head-top li.txt-blue").hover(function() {
		$(this).addClass('nav-user-block');
	}, function() {
		$(this).removeClass('nav-user-block');
	});
	/**
	* @description: 所有卡的点击切换
	*/
	$(".switch-top li").click(function(event) {
		var oNum = $(this).index();//下标第一种写法
		$(this).addClass('active').siblings().removeClass('active');
		$('.switch-comm-box .switch-comm').eq(oNum).show().siblings().hide();
	});
	/**
	* @description: 移动div出现边框
	*/
	$(".switch-comm .switch-box-list").hover(function() {
		$(this).addClass('switch-box-hover');
	}, function() {
		$(this).removeClass('switch-box-hover');
	});

    /**
     * @description: 公告
     */
    function proclamationFn () {
        $(".notice-ul").css("margin-top","-60px");
        $('.notice-ul li:last').insertBefore(".notice-ul li:first"); //把数据传进第一个li里
        $(".notice-ul").animate({marginTop:'-0px'});
    }
    var time = setInterval(function(){
        proclamationFn ();
    }, 4000);
    function stopCount()
    {
        clearTimeout(time)
    }

    $("#FontScroll ul.notice-ul").hover(function(e) {
        stopCount();
    },function(){
        time = setInterval(function(){
            proclamationFn ();
        }, 4000);
    });

    /**
     * @description: 导航栏效果
     */

    $(".head-nav .head-view-list li").hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });
    /**
     * @description: 鼠标移动改变icon
     */
    $(".main-cont-list ul li").hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });

    /**
	* @description: 用户需知的内容
	*/
	$('.know-right-main ul li').click(function(event) {
		$(this).addClass('active').siblings().removeClass('active');
	});
		/**
	* @description: 首页轮播图
	* @param oli 为幻灯片 ，slideBox 点击按钮切换
	*/
	SlideFn('.banner-warp .banner-img li','.main-center')
 	function SlideFn(oli,slideBox){
 		var oslide = $(oli),
 			oslideBox = $(slideBox);
 		var oNext = oslideBox.find('.main-btn-next');
 		var oPrev = oslideBox.find('.main-btn-prev');
 		var oLi = oslideBox.find('.carousel-inner ul');
 		var oPicn = oslide.length;
 		var index= 0;
 		var isExpert=true;
 		if(oPicn>1){
 			console.log(oPicn)
 			for (var i= 1; i <= oPicn; i++) {
 				oLiDiv = '<li></li>';
 				oLi.append(oLiDiv)
 			}
 			oLi.find('li').eq(0).addClass("selected");
 			oslide.eq(0).css({
	 			position: 'absolute',
	 			left: '0px',
	 			top: '0px',
	 			display: 'block',
	 		}).siblings(oslide).css({
	 			position: 'absolute',
	 			left: '0px',
	 			top: '0px',
	 			display: 'none',
	 		});
 		}
 		var timer = setInterval(function(){
		    index++;
		    if(index==oPicn){
		    	index=0;
		    }
		    show(index);
		},3000);

 		function show(index){
		    oLi.find('li').removeClass("selected").eq(index).addClass("selected");
		    oslide.eq(index).stop().fadeIn(500).siblings(oslide).stop().fadeOut(500);
            $(".main-center-click li").eq(index).addClass('active').siblings().removeClass('active');
		    isExpert = true;
		    console.log(isExpert);
		}
 		oslideBox.hover(function() {
 			oNext.show();
 			oPrev.show();
 		}, function() {
 			oNext.hide();
 			oPrev.hide();
 		});
 		oNext.click(function(event) {
 			window.clearInterval(timer);
 			console.log(isExpert);
 			if(isExpert){
 				index++;
	 			console.log(index);
	 			if(index==oPicn){
			    	index=0;
			    	isExpert = false;
			    }
	 			show(index);
	 			
 			}
 			
 		});

 		oPrev.click(function(event) {
 			window.clearInterval(timer);
 			index--;
 			if(index<0){
 				index = oPicn-1;
 			}
 			show(index);
 		});

 	}

    /**
	 * @description: 设置首页产品分类宽度
     */
    var ProductTypeLen = $(".switch-top li").length;
    $(".switch-top li").css('width', 100/ProductTypeLen+'%');
});