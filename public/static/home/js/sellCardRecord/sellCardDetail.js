$(function(){
	$("#resubmit").click(function(event){
		// 可以选择修改提交方式
		// 可以提交为“慢销”或者“快销”
		if($("#canBeChangeSubmitType").val() == "true")
		{
					resubmit($("#orderId").val(), "1", $("#orderType").val());
		}
		else
		{
			resubmit($("#orderId").val(), "", $("#orderType").val());
		}
		
		//event.stopPropagation();
	});
});

/**
 * 一键提交失败卡密
 */
function resubmit(id, type, orderType)
{
	var tip = null;
	$.ajax({
        url:tiji,
        data:{id:id, type:type},
        dataType:'json',
        type: 'POST',
        beforeSend: function() {
        	tip = layer.msg('提交处理中，请稍候...', {icon: 16, shade : [0.4, '#000'],time : 100000});
        },
        success: function(data) {
        	
        	if(data.code != 1)
    		{
        		layer.msg("提交成功"+data.num+"张，"+data.str, {icon : 2,shade : [ 0.4, '#000' ],time : 1500});
    		}
        	else
    		{
        		layer.alert("提交成功", {
        			title:'提交成功',
        			icon : 1,
        			skin: 'layui-layer-lan', //样式类名
        			closeBtn: 1
    			}, function(index){
    				//window.location.reload();
    				//layer.close(tip);
    				layer.close(index);
    			});
    		}
         	
        },
        complete: function() {
        	layer.close(tip);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           //alert(XMLHttpRequest.status);
        }
    });
	
}