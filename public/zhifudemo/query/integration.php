<?php
class Integration{
	public $keyy;
	public $order;
	public $url;
	public $cardlistt;

	public function init($config=null,$url=null){
		$this->keyy=$config;
		$this->url=$url;
	}
	public function Send($parm){
		unset($parm['key']);
		$parm['b_number']=$this->encrypt($parm['b_number'],'ENCODE',$this->keyy);
		$parm['b_key']=$this->encrypt($parm['b_key'],'ENCODE',$this->keyy);
		$this->order=$parm;
		$parm['sign']=$this->sign_ver();
		$str = $this->verifypost($this->url,$parm);
		return json_decode($str,true);
	}
	public function verifypost($url,$data = array()){
		$curl = curl_init();
		// 启动一个CURL会话
		curl_setopt($curl, CURLOPT_URL, $url);
		// 要访问的地址
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		// 对认证证书来源的检查
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		// 从证书中检查SSL加密算法是否存在
		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		// 模拟用户使用的浏览器
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		// 使用自动跳转
		curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
		// 自动设置Referer
		curl_setopt($curl, CURLOPT_POST, 1);
		// 发送一个常规的Post请求
		@curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		// Post提交的数据包
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		// 设置超时限制防止死循环
		curl_setopt($curl, CURLOPT_HEADER, 0);
		// 显示返回的Header区域内容
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		// 获取的信息以文件流的形式返回
		$tmpInfo = curl_exec($curl);
		// 执行操作
		if (curl_errno($curl)) {
			echo 'Errno' . curl_error($curl);
			//捕抓异常
		}
		curl_close($curl);
		// 关闭CURL会话
		return $tmpInfo;
		// 返回数据

	}

	/*生成字串符*/
	public function createLinkstring($para) {
		$arg  = "";
		while (list ($key, $val) = each ($para)){
			$arg.=$key."=".$val."&";
			}
			$arg = substr($arg,0,count($arg)-2);
			if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
			return $arg;
	}

    /*去掉字符空值*/
	public function paraFilter($para){
		$para_filter = array();
		while (list ($key, $val) = each ($para)){
			if($key == "sign")continue;
			else
				$para_filter[$key] = $para[$key];
			}
			return $para_filter;
	}

   /*数组排序*/
   public function argSort($para){
	   ksort($para);
	   reset($para);
	   return $para;
	   }

   
    /*签名*/
	public  function sign_ver(){
		$parm=$this->paraFilter($this->order);
		$parm_sort=$this->argSort($parm);
		$parm_str=$this->createLinkstring($parm_sort);
		$ok=md5($parm_str.$this->keyy);
		return $ok;
	}
	/*卡类请求字符串生成*/
    public  function card_ver(){
		$parm=$this->paraFilter($this->order);
		$parm_sort=$this->argSort($parm);
		$parm_str=$this->createLinkstring($parm_sort);
		$ok=md5($parm_str.$this->keyy);
		$parm_str.="&sign=".$ok;
		return $parm_str;
	}

	/*自动提交表单*/
	public function blsend($para, $method, $button_name,$url) {
			$sHtml = "<form id='blpaysubmit' name='blpaysubmit' action='".$url."' method='".$method."'>";
			while (list ($key, $val) = each ($para)) {
				$sHtml.= "<input type='text' name='".$key."' value='".$val."'/>";
			}
			$sHtml = $sHtml."<input type='submit' style='border:none;width:200px;height:35px;line-height:35px;background:none;font-size:18px;' value='".$button_name."'></form>";
			//$sHtml = $sHtml."<script>document.forms['blpaysubmit'].submit();</script>";
			return $sHtml;
	}
	/*加密解密 ENCODE 加密   DECODE 解密*/
   public function encrypt($string, $operation = 'ENCODE', $key = '', $expiry = 0){
			if($operation == 'DECODE') {
				$string =  str_replace('_', '/', $string);
			}
			$key_length = 4;
			$key = md5($key ? $key : 10);
			$fixedkey = md5($key);
			$egiskeys = md5(substr($fixedkey, 16, 16));
			$runtokey = $key_length ? ($operation == 'ENCODE' ? substr(md5(microtime(true)), -$key_length) : substr($string, 0, $key_length)) : '';
			$keys = md5(substr($runtokey, 0, 16) . substr($fixedkey, 0, 16) . substr($runtokey, 16) . substr($fixedkey, 16));
			$string = $operation == 'ENCODE' ? sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$egiskeys), 0, 16) . $string : base64_decode(substr($string, $key_length));

			$i = 0; $result = '';
			$string_length = strlen($string);
			for ($i = 0; $i < $string_length; $i++){
				$result .= chr(ord($string{$i}) ^ ord($keys{$i % 32}));
			}
			if($operation == 'ENCODE') {
				$retstrs =  str_replace('=', '', base64_encode($result));
				$retstrs =  str_replace('/', '_', $retstrs);
				return $runtokey.$retstrs;
			} else {	
				if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$egiskeys), 0, 16)) {
					return substr($result, 26);
				} else {
					return '';
				}
			}
		}
    /*XML转数组*/
	public function xmlToArray($url){
		header("Content-Type: text/html; charset=utf-8");
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($url, 'SimpleXMLElement', LIBXML_NOCDATA)), true);        
        return $values;
    }

	public function logt($word='') {
		$fp = fopen("log.txt","a");
		flock($fp, LOCK_EX) ;
		fwrite($fp,"执行日期：".strftime("%Y%m%d%H%M%S",time())."\n".$word."\n");
		flock($fp, LOCK_UN);
		fclose($fp);
    }

	public function stripslashes_array(&$array) {
		while(list($key,$var) = each($array)) {
			if ($key != 'argc' && $key != 'argv' && (strtoupper($key) != $key || ''.intval($key) == "$key")) {
				if (is_string($var)) {
					$array[$key] = stripslashes($var);
					}
					if (is_array($var)) {
						$array[$key] = stripslashes_array($var);
						}
						}
						}
				return $array;
	}



}



?>