﻿$(function() {
    IsLogin();
    var headerH = $(".header-content").height()+20
    var warningH = $(".warning").height()
    var scrollTimer = null
    window.onscroll = function(){
        if(scrollTimer != null ) clearTimeout(scrollTimer) 
        scrollTimer = setTimeout(function(){
            var top2 = $(document).scrollTop()
            var flagWarn = $(".warning").is(":hidden")
            var trueH = flagWarn == true ? headerH : headerH + warningH
            if(top2 > 0) {
                $(".header").attr("class", "header fixed").parent().css({"height":trueH})
            }else{
                $(".header").attr("class", "header").parent().css({"height":"auto"})
            }
        },200)
    }
})

function IsLogin() {
    $.ajax({
        url: "/Home/IsLogin",
        type: "post",
        cache: false,
        data: {},
        dataType: "json",
        success: function (data) {
            if (data != null && typeof (data) == "object" && data.RequestLimit != undefined) {
                window.location.href = "/PubliCode.html"; return;
            }
            else if (typeof (data) == "string" && data.indexOf("RequestLimit") > -1) {
                window.location.href = "/PubliCode.html"; return;
            }
            var path = window.location.href.toLowerCase();
            if (data.Success) {
                if (path.indexOf("register.html") > -1 || path.indexOf("login.html") > -1 || path.indexOf("forgotpwd.html") > -1 || path.indexOf("setpwd.html") > -1) {
                    window.location.href = "/";
                } else {
                    $("#register_a").remove();
                    $("#login_a").remove();
                    $("#user_a").attr("class", "relative").find("em.user-name").eq(0).html(data.ShowName);
                    $("#logx_nick").html(data.Nickname);
                    $("#logx_name").html(data.ShowName);
                    $("#logx_userId").html(data.IsFormal==1?("ID："+data.Id):"临时会员");
                }
            }
        },
        error: function (a, b, c) {
        }
    });
}
function SetHome(obj, url) {
    try {
        obj.style.behavior = 'url(#default#homepage)';
        obj.setHomePage(url);
    } catch (e) {
        if (window.netscape) {
            try {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
            } catch (e) {
                alert("抱歉，此操作被浏览器拒绝！\n\n请在浏览器地址栏输入“about:config”并回车然后将[signed.applets.codebase_principal_support]设置为'true'");
            }
        } else {
            alert("抱歉，您所使用的浏览器无法完成此操作。\n\n您需要手动将【" + url + "】设置为首页。");
        }
    }
}
function AddFavorite(title, url) {
    try {
        window.external.addFavorite(url, title);
    }
    catch (e) {
        try {
            window.sidebar.addPanel(title, url, "");
        }
        catch (e) {
            alert("抱歉，您所使用的浏览器无法完成此操作。\n\n加入收藏失败，请进入新网站后使用Ctrl+D进行添加");
        }
    }
}

function tipsfx(msg, obj) {
    layer.tips(msg, obj, { tips: 1, time: 3000 });
}
function tipsfx(msg, obj, tips) {
    layer.tips(msg, obj, { tips: 1, time: 3000 });
}
function clea_error(obj){
    $(obj).find("input[type=text]").attr("class", "input")
}
function tipsfx2(msg, obj) {
    layer.tips(msg, obj, { tips: 1, time: 3000 });
}
function clea_error2(obj) {}
//发送验证码成功提示
function ShowChkTips(msg,config) {
    layer.msg(msg,config)
}