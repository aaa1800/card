var userAgent = navigator.userAgent.toLowerCase(),
	is_ie = -1 != userAgent.indexOf("msie") && userAgent.substr(userAgent.indexOf("msie") + 5, 3),
	pos_tip = "top",
	url_path = "",
	$thisdata = [];

function isJSON(a) {
	if ("string" == typeof a) try {
		if (JSON.parse(a), -1 < a.indexOf("{")) return !0
	} catch (c) {}
	return !1
}(function(a) {
	a.fn.extend({
		ajaxsubmit: function() {
			this.find("[data-dialog]").on("click", function(b) {
				b.preventDefault();
				loading(!0, 3);
				var d = a(this).attr("data-dialog").split(",");
				b = d[0];
				"tip" == b ? out_json({
					content: a(this).attr("tip"),
					dialog: {
						width: d[1] ? d[1] : 500,
						title: d[2] ? d[2] : "系统提示",
						timeout: d[3] ? d[3] : 0
					}
				}) : a.get(-1 == b.indexOf("?") ? b + "?ajax=1" : b + "&ajax=1", function(a) {
					isJSON(a) ? out_json(JSON.parse(a)) : out_json({
						content: a,
						dialog: {
							width: d[1] ? d[1] : 500,
							title: d[2] ? d[2] : "系统提示",
							timeout: d[3] ? d[3] : 0
						}
					});
					loading(!1)
				})
			});
			this.find("[data-href]").on("click", function(b) {
				b.preventDefault();
				var d = {
					init: function(b) {
						loading(!0, 3);
						var d = a(b).attr("data-href").split(","),
							c = d[1] ? d[1] : "json",
							e = d[0];
						if (d[3]) {
							var f = a(b).parentsUntil("form"),
								f = a(f[f.length - 1]).parent();
							"href" == c ? (window.location.href = -1 == e.indexOf("?") ? e + "?" + f.serialize() : e + "&" + f.serialize(), loading(!1, 3)) : "post" == d[3] ? ajax_get(b, e, c, d[2], f.serializeArray()) : ajax_get(b, -1 == e.indexOf("?") ? e + "?" + f.serialize() : e + "&" + f.serialize(), c, d[2])
						} else ajax_get(b, e, c, d[2])
					}
				};
				b = a(this).attr("data-confirm");
				var c = this;
				b ? (b = b.split(","), openconfirm({
					name: b[0],
					width: b[1],
					content: b[2],
					callback: function() {
						d.init(c);
						a("#modal-dialog").hide();
						a("#modal-cover").hide()
					}
				})) : d.init(this)
			});
			this.find("[frame-href]").on("click", function(b) {
				b.preventDefault();
				b = a(this).attr("frame-href");
				loading(!0, 3);
				ajax_get(this, b, "html", "ajaxpage");
				history.replaceState && history.replaceState(null, null, b);
				a(".tooltip").hide()
			});
			this.find("[data-form]").on("click", function(b) {
				b.preventDefault();
				b = a(this).attr("data-confirm");
				var d = {
					init: function(b) {
						var d = a(b).attr("data-form").split(","),
							c = d[1] ? d[1] : "json";
						if (formtest(a(b), 1, d[0])) {
							b = a("#" + a(b).attr("name"));
							var e = b.attr("action");
							"submit" == c ? b.submit() : (loading(!0), a(".tooltip .tooltip-close").click(), a.ajax({
								dataType: c,
								type: "POST",
								cache: !1,
								url: -1 == e.indexOf("?") ? e + "?ajax=1" : e + "&ajax=1",
								data: b.serializeArray(),
								success: function(b) {
									"json" == c ? out_json(b, d[0]) : a("#" + d[0]).html(b).datatoggle().ajaxsubmit().tooltip();
									loading(!1);
									delete b
								}
							}))
						}
					}
				},
					c = this;
				b ? (b = b.split(","), openconfirm({
					name: b[0],
					width: b[1],
					content: b[2],
					callback: function() {
						d.init(c);
						a("#modal-dialog").hide();
						a("#modal-cover").hide()
					}
				})) : d.init(this)
			});
			var c = !1;
			this.find("[data-page]").each(function() {
				var b = this,
					d = a(this).attr("data-page").split(",");
				a(this).attr("data-path") && (url_path = a(this).attr("data-path"));
				var e = d[3] ? d[3] : 1,
					f = d[1],
					g = 0,
					m = function() {
						if (0 == c) if (e > g && 0 != g) a(b).find(".panel-more").show();
						else {
							c = !0;
							loading(!0);
							var h = d[0].replace("{page}", e);
							a.get(-1 == h.indexOf("?") ? h + "?ajax=1" : h + "&ajax=1", function(d) {
								0 < d.number ? (g = Math.ceil(d.number / d.perpage), g >= d.current ? (eval(f + "(obj," + e + ",content)"), e = d.current + 1) : a(b).find(".panel-more").show()) : a(b).find(".panel-more").html("未发现任何数据").show();
								c = !1;
								loading(!1)
							}, "json")
						}
					};
				if ("scroll" == d[2]) 1 == e && m(), a(window).scroll(function() {
					a(document).height() <= parseFloat(a(window).height()) + parseFloat(a(window).scrollTop()) && m()
				});
				else if ("page" == d[2]) {
					c = !0;
					loading(!0);
					var h = d[0].replace("{page}", e);
					a.get(-1 == h.indexOf("?") ? h + "?ajax=1" : h + "&ajax=1", function(d) {
						0 < d.number && (eval(f + "(obj," + e + ",content)"), a(b).find(".page").page({
							pageCount: data.number,
							current: data.current,
							perpage: data.perpage,
							backFn: function(a) {
								eval(f + "(obj," + a + ",content)")
							}
						}));
						loading(!1);
						c = !1
					}, "json")
				}
			});
			return this
		},
		datatoggle: function() {
			this.find("[data-time]").each(function() {
				var c = a(this).attr("data-time").split(",");
				a(this).attr("id", "time_" + c[0]);
				timer(c[0], c[1], c[2]);
				timerint[c[0]] = setInterval(function() {
					timer(c[0], c[1], c[2])
				}, 1E3)
			});
			this.find("[data-time]").each(function() {
				var c = a(this).attr("data-time").split(",");
				a(this).attr("id", "time_" + c[0]);
				timer(c[0], c[1], c[2]);
				timerint[c[0]] = setInterval(function() {
					timer(c[0], c[1], c[2])
				}, 1E3)
			});
			this.find("[data-input]").each(function() {
				var c = a(this),
					b = c.attr("data-input");
				"radio" == b ? (c.find("li:not(.dropdown):not(.divider)").on("click", function(b) {
					b.preventDefault();
					a(this).find(">a").is(".disabled, :disabled") || a(this).hasClass("active") || (b = a(this).parents(".dropdown-menu"), c.find(".active").removeClass("active"), a(this).addClass("active"), b && a(this).closest("li.dropdown").addClass("active"), c.find("input").val(a(this).attr("data-value")))
				}), a(this).append('<input class="sr-only" type="radio" name="' + a(this).attr("data-name") + '" value="' + a(this).find("li.active").attr("data-value") + '" />')) : "checkbox" == b && c.find("li:not(.dropdown):not(.divider)").each(function() {
					var b = a(this).parents(".dropdown-menu");
					a(this).hasClass("active") ? (b && a(this).closest("li.dropdown").addClass("active"), a(this).append('<input class="sr-only" checked name="' + c.attr("data-name") + '" type="checkbox" value="' + a(this).attr("data-value") + '" />')) : a(this).append('<input class="sr-only" name="' + c.attr("data-name") + '" type="checkbox" value="' + a(this).attr("data-value") + '" />');
					a(this).on("click", function(d) {
						d.preventDefault();
						a(this).find(">a").is(".disabled, :disabled") || (a(this).hasClass("active") ? (a(this).removeClass("active").find("input").attr("checked", !1), d = a(this).parents(".dropdown-menu").find(".active").length, b && 0 == d && a(this).closest("li.dropdown").removeClass("active")) : (a(this).addClass("active").find("input").attr("checked", !0), b && a(this).closest("li.dropdown").addClass("active")))
					})
				})
			});
			this.find('[data-toggle="dropdown"]').on("click", function(c) {
				c.preventDefault();
				if (!a(this).is(".disabled, :disabled")) {
					var b = a(this),
						d = a();
					c = b.parent();
					var e = c.children(".dropdown-menu"),
						f = a('[data-toggle="dropdown"]').parent(),
						e = c.children(".dropdown-menu"),
						d = d.add(a('[data-toggle="dropdown"]').parent()),
						d = c.hasClass("open"),
						g = b.data("animation"),
						m = b.offset().top,
						b = b.outerHeight() + e.outerHeight(),
						h = a(window).height(),
						k = a(window).scrollTop();
					d ? (c.removeClass("open"), e.removeClass("dropdown-menu-up")) : (f.removeClass("open"), c.addClass("open"), m + b > h && m - k > b ? e.addClass("dropdown-menu-up") : e.removeClass("dropdown-menu-up"), g && (e.addClass("animated " + g), e.one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
						e.removeClass("animated " + g)
					})));
					return !1
				}
			});
			this.find('[data-hover="dropdown"]').each(function() {
				var c = a(),
					b, c = c.add(a('[data-hover="dropdown"]'));
				a(this).hover(function() {
					window.clearTimeout(b);
					var d = a(this);
					$son = d.children(".dropdown-toggle");
					if (!$son.is(".disabled, :disabled")) {
						var e = d.children(".dropdown-menu"),
							f = d.hasClass("open"),
							g = d.data("animation"),
							m = d.offset().top,
							h = d.outerHeight() + e.outerHeight(),
							k = a(window).height(),
							n = a(window).scrollTop();
						f || (c.removeClass("open"), d.addClass("open"), m + h > k && m - n > h ? e.addClass("dropdown-menu-up") : e.removeClass("dropdown-menu-up"), g && (e.addClass("animated " + g), e.one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
							e.removeClass("animated " + g)
						})))
					}
				}, function() {
					var d = a(this);
					$time = d.data("outime") || 500;
					b = window.setTimeout(function() {
						d.removeClass("open");
						d.children(".dropdown-menu").removeClass("dropdown-menu-up")
					}, $time)
				})
			});
			this.find('[data-toggle="select"] > li > a').on("click", function(c) {
				c.preventDefault();
				c = a(this);
				var b = c.html();
				c.closest(".dropdown-menu").prev(".dropdown-toggle").find(".btn-text").html(b);
				c.parent("li").addClass("active").siblings().removeClass("active")
			});
			this.find('[data-toggle="tab"]').on("click", function(c) {
				c.preventDefault();
				if (!a(this).is(".disabled, :disabled") && !a(this).parent("li").hasClass("active")) {
					c = a(this);
					$ul = c.closest("ul:not(.dropdown-menu)");
					$li = c.closest("li");
					selector = c.data("target");
					dropdown = $li.parent(".dropdown-menu");
					animation = c.data("animation");
					selector || (selector = (selector = c.attr("href")) && selector.replace(/.*(?=#[^\s]*$)/, ""));
					var b = a(selector);
					$ul.find(".active").removeClass("active");
					$li.addClass("active");
					b.parent().find("> .tab-panel-active").removeClass("tab-panel-active");
					b.addClass("tab-panel-active");
					dropdown && c.closest("li.dropdown").addClass("active");
					animation && (b.addClass("animated " + animation), b.one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
						b.removeClass("animated " + animation)
					}))
				}
			});
			this.find('[data-toggle="collapse"]').on("click", function(c) {
				c.preventDefault();
				if (!a(this).is(".disabled, :disabled")) {
					c = a(this);
					var b = c.data("target");
					b || (b = c.next(".collapse"), b.length || (b = c.parent().next(".collapse")));
					b = a(b);
					b.is(":visible") ? (b.slideUp("fast"), c.removeClass("collapsed")) : (b.slideDown("fast"), c.addClass("collapsed"));
					return !1
				}
			});
			this.find('[data-dismiss="alert"]').on("click", function(c) {
				c.preventDefault();
				c = a(this);
				selector = c.attr("data-target");
				selector || (selector = (selector = c.attr("href")) && selector.replace(/.*(?=#[^\s]*$)/, ""));
				var b = a(selector);
				b.length || (b = c.closest(".alert"));
				b.fadeTo(400, 0, function() {
					a(this).slideUp(400, function() {
						a(this).remove()
					})
				});
				return !1
			});
			delete $this;
			return this
		},
		tooltip: function(c) {
			var b = a(this);
			options = {
				id: "tooltip",
				type: 3,
				time: 0,
				title: "",
				position: "top-left",
				draggable: !0
			};
			a.extend(options, c);
			0 == a("#" + options.id).length && a("body").append('<div class="tooltip" id="' + options.id + '" style="display: none;"><div class="tooltip-arrow"></div><a class="tooltip-close close" href="javascript:;">&times;</a><div class="tooltip-inner"></div></div>');
			var d = a("#" + options.id);
			1 == options.type ? (tipopen(b, d, options.content, options.position), options.time ? d.delay(options.time).slideUp("slow") : this.bind({
				mouseleave: function() {
					d.hide()
				}
			})) : 2 == options.type ? ($thisdata.tip && $thisdata.tip.attr("open", !1), $thisdata.tip = b, tipopen(b, d, options.content, options.position), d.find(".tooltip-close").show().unbind("click").click(function() {
				d.fadeOut();
				$thisdata.tip.attr("open", !1)
			}), d.find(".tooltip-inner").datatoggle().ajaxsubmit().tooltip()) : (pos_tip = options.position, this.find("[data-tip]").bind({
				mouseenter: function() {
					var b = a(this).attr("data-tip").split("|");
					"js" == b[1] ? (Return = eval("(" + b[0] + ")"), tipopen(a(this), d, Return, pos_tip)) : b[1] ? tipopen(a(this), d, b[0], b[1]) : tipopen(a(this), d, b[0], pos_tip)
				},
				mouseleave: function() {
					d.hide()
				}
			}));
			is_ie && setTimeout(CollectGarbage, 1);
			delete options;
			b;
			return this
		},
		modal: function(c, b, d) {
			function e() {
				var b = f[0].style;
				f.css({
					left: "50%",
					marginLeft: f.outerWidth() / 2 * -1,
					zIndex: options.zIndex + 3
				}).show();
				if (f.height() + 80 >= a(window).height() && ("absolute" != f.css("position") || g)) {
					var d = a(document).scrollTop() + 40;
					f.css({
						position: "absolute",
						top: d + "px",
						marginTop: 0
					});
					g && b.removeExpression("top")
				} else f.height() + 80 < a(window).height() && (g ? (b.position = "absolute", options.centered ? (b.setExpression("top", '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"'), b.marginTop = 0) : (d = options.modalCSS && options.modalCSS.top ? parseInt(options.modalCSS.top) : 0, b.setExpression("top", "((blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + " + d + ') + "px"'))) : options.centered ? f.css({
					position: "fixed",
					top: "50%",
					marginTop: f.outerHeight() / 2 * -1
				}) : f.css({
					position: "fixed"
				}).css(options.modalCSS))
			}
			var f = a(this),
				g = 7 > parseInt(is_ie);
			switch (c) {
			case "close":
				a(".tooltip").hide();
				f.hide();
				a("#modal-cover").attr("c") == f.attr("id") && a("#modal-cover").hide();
				b && b("close");
				break;
			case "title":
				this.find(".modal-header h3").html(b);
				break;
			case "content":
				this.find(".modal-body").html(b);
				a("#modal-cover").show();
				e();
				0 < d && setTimeout(function() {
					f.find(".modal-close").click()
				}, 1E3 * d);
				break;
			case "location":
				e();
				break;
			default:
				0 == this.find(".modal-body").length ? (options = {
					hide: !1,
					cover: !0,
					centered: !0,
					selector: "modal animated bounceIn",
					zIndex: 5E3,
					location: "middle",
					title: "系统提示",
					modalCSS: {
						top: "40px"
					},
					header: !0,
					timeout: 0
				}, a.extend(options, c), f.width(options.width), f.attr("class", options.selector), options.header ? f.wrapInner('<div class="modal-body"></div>').prepend('<div class="modal-header"><h3>' + options.title + '</h3><a class="modal-close close" data-dismiss="modal">&times;</a></div>') : f.wrapInner('<div class="modal-body"></div>').prepend('<a class="modal-close close" data-dismiss="modal">&times;</a>'), f.unbind("click").find('[data-dismiss="modal"]').click(function() {
					f.hide();
					a("#modal-cover").attr("c") == f.attr("id") && a("#modal-cover").hide();
					a("#tooltip").hide();
					b && b("close")
				}), options.cover && (0 == a("#modal-cover").length ? a("body").prepend('<div class="modal-cover" id="modal-cover"></div>') : a("#modal-cover").show(), a("#modal-cover").attr("c", f.attr("id")).height(a(document).height()).click(function() {
					f.find('[data-dismiss="modal"]').click()
				})), e(), b && b("load"), options.hide ? (f.hide(), a("#modal-cover").hide()) : f.show(), 0 < options.timeout && setTimeout(function() {
					f.find('[data-dismiss="modal"]').click()
				}, 1E3 * options.timeout)) : (f.show(), options.cover && a("#modal-cover").show()), delete c
			}
			delete f;
			is_ie && setTimeout(CollectGarbage, 1);
			return this
		},
		formtip: function(c, b, d, e) {
			this.tooltip({
				type: 1,
				content: c,
				position: d
			}).blur(function() {
				a("#tooltip").hide()
			});
			1 == b && (a(".tooltip").addClass("tooltip-error").show(), this.addClass("form-control-error").focus());
			0 < e && setTimeout(function() {
				a("#tooltip").fadeOut()
			}, 1E3 * e);
			return this
		},
		page: function(c) {
			c = a.extend({
				number: 20,
				pageCount: 0,
				current: 1,
				perpage: 20,
				backFn: function() {}
			}, c);
			var b = {
				init: function(a, c) {
					b.fillHtml(a, c);
					b.bindEvent(a, c)
				},
				fillHtml: function(a, b) {
					b.pageCount = Math.ceil(b.number / b.perpage);
					if (1 < b.pageCount && b.pageCount >= b.current) {
						a.empty();
						a.append('<span class="num">' + b.number + "</span>");
						1 < b.current ? a.append('<a href="javascript:;" class="last">上一页</a>') : (a.remove(".last"), a.append('<span class="last disabled">上一页</span>'));
						1 != b.current && 4 <= b.current && 4 != b.pageCount && a.append('<a href="javascript:;" class="item">1</a>');
						2 < b.current - 2 && b.current <= b.pageCount && 5 < b.pageCount && a.append("<span>...</span>");
						var d = b.current - 2,
							c = b.current + 2;
						(1 < d && 4 > b.current || 1 == b.current) && c++;
						for (b.current > b.pageCount - 4 && b.current >= b.pageCount && d--; d <= c; d++) d <= b.pageCount && 1 <= d && (d != b.current ? a.append('<a href="javascript:;" class="item">' + d + "</a>") : a.append('<span class="active">' + d + "</span>"));
						b.current + 2 < b.pageCount - 1 && 1 <= b.current && 5 < b.pageCount && a.append("<span>...</span>");
						b.current != b.pageCount && b.current < b.pageCount - 2 && 4 != b.pageCount && a.append('<a href="javascript:;" class="item">' + b.pageCount + "</a>");
						b.current < b.pageCount ? a.append('<a href="javascript:;" class="next">下一页</a>') : (a.remove(".next"), a.append('<span class="disabled">下一页</span>'))
					}
				},
				bindEvent: function(d, c) {
					d.find("a.item").on("click", function() {
						var e = parseInt(a(this).text());
						b.fillHtml(d, {
							current: e,
							pageCount: c.pageCount
						});
						"function" == typeof c.backFn && c.backFn(e)
					});
					d.find("a.last").on("click", function() {
						var a = parseInt(d.children("span.active").text());
						b.fillHtml(d, {
							current: a - 1,
							pageCount: c.pageCount
						});
						"function" == typeof c.backFn && c.backFn(a - 1)
					});
					d.find("a.next").on("click", function() {
						var a = parseInt(d.children("span.active").text());
						b.fillHtml(d, {
							current: a + 1,
							pageCount: c.pageCount
						});
						"function" == typeof c.backFn && c.backFn(a + 1)
					})
				}
			};
			b.init(this, c);
			return this
		}
	})
})(jQuery);
$.tmpl = function(a, c) {
	return doT.template(document.getElementById(a).innerHTML).apply(null, [c])
};
$.formatFloat = function(a, c) {
	return parseFloat(a).toFixed(2)
};

function ajax_get(a, c, b, d, e) {
	"tip" == b && $(a).attr("open") ? ($(".tooltip .tooltip-close").click(), parent.loading(!1)) : $.ajax({
		dataType: "json" == b ? "json" : "html",
		type: e ? "POST" : "GET",
		cache: !1,
		data: e,
		url: -1 == c.indexOf("?") ? c + "?ajax=1" : c + "&ajax=1",
		success: function(c) {
			if ("json" == b) out_json(c, d);
			else if ("tip" == b) if ("{" == c[0]) out_json($.parseJSON(c), d);
			else {
				var e = $(a).attr("data-name") ? $(a).attr("data-name") : "modaltip";
				$(a).attr("open", !0).tooltip({
					type: 2,
					id: e,
					content: c,
					position: d
				})
			} else "dialog" == b ? (e = d.split("|"), out_json({
				content: c,
				dialog: {
					width: e[0] ? e[0] : 500,
					title: e[1] ? e[1] : "系统提示",
					timeout: e[2] ? e[2] : 0
				}
			})) : $("#" + d).html(c).datatoggle().ajaxsubmit().tooltip();
			loading(!1);
			delete c
		}
	})
}
var timerint = [];

function timer(a, c, b) {
	var d = (new Date).getTime();
	c -= d / 1E3;
	0 < c ? (b = parseInt(c / 60 / 60 % 24, 10), d = parseInt(c / 60 % 60, 10), c = parseInt(c % 60, 10), b = checkTime(b), d = checkTime(d), c = checkTime(c), $("#time_" + a).html("<b>" + b + "</b> <span>:</span> <b>" + d + "</b> <span>:</span> <b>" + c + "</b>")) : ("1" == b ? window.location.reload() : "2" != b && $("#time_" + a).html('<div class="countdown-ing">正在开奖中，请稍候 ..<br /><i class="iconimg iconimg-ing"></i></div>'), clearInterval(timerint[a]))
}

function checkTime(a) {
	10 > a && (a = "0" + a);
	return a
}

function isUndefined(a) {
	return "" == a || null == a || "undefined" == typeof a ? !0 : !1
}

function inputbox(a, c) {
	var b = a.value.split("%"),
		d = parseFloat(b[0]);
	isNaN(d) && (d = 0);
	0 < c && (d = d.toFixed(c));
	if ("" == b[1]) 1 > d ? d = 0 : 100 < d && (d = 100), d += "%";
	else {
		var e = $(a).attr("num").split("|"),
			b = parseFloat(e[0]),
			e = parseFloat(e[1]);
		d < b ? d = b : d > e && (d = e)
	}
	$(a).val(d);
	return d
}

function loading(a, c, b) {
	var d = $("#loading");
	a ? (0 == d.length && ($("body").append('<div id="loading"><i class="iconimg"></i><span></span></div>'), d = $("#loading")), 1 == c ? c = "loading loading-transparent" : 2 == c ? c = "loading loading-notext" : 3 == c ? c = "loading loading-inverse loading-notext" : (c = 4 == c ? "loading loading-inverse" : "loading", d.find("span").text(b ? b : "请稍后 ...")), d.attr("class", c).show()) : d.hide()
}

function openconfirm(a) {
	option = {
		id: "modal-dialog",
		width: 420,
		modal: !0,
		selector: "modal",
		title: "系统提示",
		prompt: "info",
		name: "",
		content: "",
		callback: "",
		button: "确定",
		cancel: "取消",
		timeout: 0,
		url: ""
	};
	$.extend(option, a);
	0 == $("#" + option.id).length && $("body").append('<div id="' + option.id + '"></div>');
	$self = $("#" + option.id);
	var c = option.name ? "<h4>" + option.name + "</h4>" : "",
		b = "hide" != option.prompt ? '<div class="prompt prompt-' + option.prompt + '"><div class="prompt-icon"><i class="iconimg png"></i></div>' : "<div>";
	$self.width(option.width).html('<div class="modal-prompt">' + b + '<div class="prompt-cont">' + c + "<p>" + option.content + '</p><div class="action"><a class="btn btn-sm btn-primary btn-ok" href="###">' + option.button + '</a><a class="btn btn-sm btn-default btn-cancel" href="###" data-dismiss="modal">' + option.cancel + "</a></div></div></div></div>").modal({
		title: option.title,
		selector: option.selector,
		modal: option.modal
	}).find(".btn-ok").click(function() {
		option.callback ? "function" == typeof option.callback ? option.callback($self) : eval(option.callback) : option.url ? "reload" == option.url ? window.location.reload() : window.location.href = option.url : $("#modal-dialog").modal("close")
	});
	0 < option.timeout && setTimeout(function() {
		$self.find(".btn-ok").click()
	}, 1E3 * option.timeout);
	delete a
}

function formtest(a, c, b) {
	isUndefined(b) && (b = "top-left");
	a = $("#" + $(a).attr("name"));
	try {
		return a.find("input[null],select[null],textarea[null]").each(function() {
			if ("" == $(this).val() || 0 == $(this).val()) {
				var a = $(this).attr("null");
				a || (a = "不可为空，请进行选择……");
				$(this).is("select") ? $(this).formtip(a, c, b) : ($(this).formtip(a, c, b), $(this).addClass("form-control-error"));
				throw !0;
			}
			a = $(this).attr("reg");
			if ("" != a) if (a = new RegExp(a, "g"), a.test($(this).val())) $(this).removeClass("form-control-error");
			else throw (a = $(this).attr("err")) || (a = "格式不正确或包含禁止字符…"), $(this).formtip(a, c, b), !0;
			else $(this).removeClass("form-control-error")
		}), !0
	} catch (d) {
		return !1
	}
}

function tipopen(a, c, b, d) {
	a.attr("data-placement") && (d = a.attr("data-placement"));
	c.find(".tooltip-inner").html(b);
	b = a.offset();
	var e = l = "",
		f = a.outerHeight(),
		g = c.outerHeight(),
		m = $(window).height(),
		h = a.outerWidth(),
		k = c.outerWidth(),
		n = $(window).width();
	"top" == d ? (e = b.top - g, l = b.left + h / 2 - k / 2, 0 > e && b.top + f < m && (e = b.top + f, d = "bottom")) : "right" == d ? (e = b.top + f / 2 - g / 2, l = b.left + h, l + k > n && (l = b.left - k, d = "left")) : "bottom" == d ? (e = b.top + f, l = b.left + h / 2 - k / 2, e + g > m && 0 < b.top - g && (e = b.top - g, d = "top")) : "left" == d ? (e = b.top + f / 2 - g / 2, l = b.left - k, 0 > l && (l = b.left + h, d = "right")) : "top-left" == d ? (e = b.top - g, l = b.left, 0 > e && b.top + f < m && (e = b.top + f, d = "bottom-left")) : "top-right" == d ? (e = b.top - g, l = b.left + h - k, 0 > e && b.top + f < m && (e = b.top + f, d = "bottom-right")) : "bottom-left" == d ? (e = b.top + f, l = b.left, e + g > m && 0 < b.top - g && (e = b.top - g, d = "top-left")) : "bottom-right" == d ? (e = b.top + f, l = b.left + h - k, e + g > m && 0 < b.top - g && (e = b.top - g, d = "top-right")) : "right-top" == d ? (e = b.top, l = b.left + h, l + k > n && (l = b.left - k, d = "left-top")) : "right-bottom" == d ? (e = b.top + f - g, l = b.left + h, l + k > n && (l = b.left - k, d = "left-bottom")) : "left-top" == d ? (e = b.top, l = b.left - k - 10, 0 > l && (l = b.left + h, d = "right-top")) : "left-bottom" == d && (e = b.top + f - g, l = b.left - k, 0 > l && (l = b.left + h, d = "right-bottom"));
	c.attr("class", "tooltip " + d);
	c.fadeIn("fast").removeClass("tooltip-error").css({
		left: l + "px",
		top: e + "px"
	}, "slow");
	delete a;
	c;
	f;
	h;
	g;
	k
}

function setTab(a, c, b) {
	for (i = 1; i <= b; i++) {
		var d = document.getElementById(a + i),
			e = document.getElementById("con_" + a + "_" + i);
		d.className = i == c ? "active" : "";
		e.style.display = i == c ? "block" : "none"
	}
}

function round(a, c) {
	for (var b = 1; 0 < c; b *= 10, c--);
	for (; 0 > c; b /= 10, c++);
	return Math.round(a * b) / b
}

function out_json(a, c) {
		console.log(a,c);
		if(a.miyao!="" && a.miyao!=null){
			$("#modal-dialog").modal("close")
			$("#apikey").text(a.key);
			$("#3des").text(a.des);
		}
	if (!a) return !1;
	a.eval && ("function" == typeof a.eval ? a.eval() : -1 == a.eval.indexOf("(") ? eval(a.eval + "(a)") : eval(a.eval));
	"login" == a.run && $.get("/signlogin.html", function(a) {
		$("#modal-dialog").html(a).width(420).modal({
			title: "用户登录！",
			modal: !0,
			header: !1
		}).datatoggle().ajaxsubmit().tooltip()
	});
	if(a.token!=null && a.token!="")$("input[name=__token__]").val(a.token);
	a.url ? "reload" == a.url ? window.location.reload() : window.location.href = a.url : a.tip ? $(a.tip).formtip(a.content, a.type ? a.type : 1, a.pos ? a.pos : c) : a.id ? "val" == a.type ? $(a.id).val(a.content) : $(a.id).html(a.content) : a.dialog ? (0 == $("#modal-dialog").length && $("body").append('<div id="modal-dialog"></div>'), $self = $("#modal-dialog"), a.dialog.width && $self.width(a.dialog.width), a.content ? $self.hide().html(a.content).modal({
		hide: a.dialog.hide,
		title: a.dialog.title,
		header: a.dialog.header,
		location: a.dialog.location,
		zIndex: a.dialog.zIndex,
		timeout: a.dialog.timeout,
		selector: a.dialog.selector
	}).datatoggle().ajaxsubmit().tooltip() : a.dialog.url && (loading(!0), url = a.dialog.url, $.get(-1 == url.indexOf("?") ? url + "?ajax=1" : url + "&ajax=1", function(b) {
		$self.hide().html(b).modal({
			hide: a.dialog.hide,
			title: a.dialog.title,
			header: a.dialog.header,
			location: a.dialog.location,
			zIndex: a.dialog.zIndex,
			timeout: a.dialog.timeout,
			selector: a.dialog.selector
		}).datatoggle().ajaxsubmit().tooltip();
		loading(!1)
	})), 0 < a.dialog.time && setTimeout(function() {
		$("#modal-dialog").modal("close")
	}, 1E3 * a.dialog.time)) : a.confirm && openconfirm({
		width: a.confirm.width,
		title: a.confirm.title,
		name: a.confirm.name,
		content: a.content,
		prompt: a.confirm.prompt,
		button: a.confirm.button,
		callback: a.confirm.callback,
		url: a.confirm.url,
		timeout: a.confirm.time
	})
};