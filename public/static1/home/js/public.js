var scrollTop = 0; //页面滚动条滚动的高度
var timer = null;
var tabSwiper = $("[tab-title='true']").find("li");
var conSwiper = $("[category-swiper='true']");

//卡类滚动切换
conSwiper.each(function (i) {
    var obj = "obj" + i
    var swiper = ".swiper" + i
    obj = new Swiper(swiper, {
        pagination: swiper + ' .pagination',
        // slidesPerView: 1,
        paginationClickable: true
    })
    $(swiper).find('.arrow-left').on('click', function (e) {
        e.preventDefault()
        obj.swipePrev()
    })
    $(swiper).find('.arrow-right').on('click', function (e) {
        e.preventDefault()
        obj.swipeNext()
    })
})

//隐藏不显示的card Swiper
window.setTimeout(function () {
    conSwiper.each(function () {
        conSwiper.hide().eq(0).show()
    })
}, 0)

//点击切换card的标题，切换分类card的swiper
tabSwiper.each(function (i) {
    tabSwiper.eq(i).click(function () {
        $(this).addClass("hover").siblings().removeClass("hover")
        conSwiper.hide().eq(i).show()
    })
})

/*右侧工具栏*/
var lowVersion = false
if(navigator.userAgent.indexOf("MSIE") > 0){
    var ua = navigator.userAgent
    if(ua.indexOf("MSIE 8.0") > 0 || ua.indexOf("MSIE 9.0") > 0 ) lowVersion = true
}
if(lowVersion){
    $(".right-bar .move-left").hover(function(){
        $(this).find(".hide-menu").stop().animate({"right":"82px"},300)
        $(this).find(".hide-menu2").stop().animate({"right":"62px"},300)
    },function(){
        $(this).find(".hide-menu").stop().animate({"right":"-282px"},300)
        $(this).find(".hide-menu2").stop().animate({"right":"-142px"},300)
    })
}
//滚动到顶部
$("[data-click='back-top']").click(function (e) {
    $('html').animate({ scrollTop: 0 }, 400);
    e.preventDefault();
})

//监听页面滚动，获取滚动条高度判断是否显示返回顶部的图标
// $(document).scroll(function () {
//     var backTop = $("[data-click='back-top']");
//     if (timer !== null) clearTimeout(timer);
//     timer = setTimeout(function () {
//         scrollTop = $(document).scrollTop();
//         if (scrollTop > 500) {
//             backTop.show()
//         } else {
//             backTop.hide()
//         }
//     }, 400)
// })

//顶部警告栏是否显示
function isWarnCLose(){
    var warningClose = $.cookie('_warningClose')
    if(warningClose != 1 && $(".header>.warning")) $(".header>.warning").show()
}

$(".header a.close").bind("click",function(){
    $(this).parents(".warning").hide()
    $(".header-wrap").css({"height":"72px"})
    var expiresDate = new Date()
    expiresDate.setTime(expiresDate.getTime() + (240 * 60 * 1000)) //240分钟
    $.cookie('_warningClose','1',{
        path : '/',
        expires : expiresDate
    })
})
isWarnCLose()

//公告滚动
if($("#notify-list") && $("#notify-list li").length > 1) notifyScroll()
function notifyScroll(){
    var curNotify = 0
    var notifySize = $("#notify-list li").length
    $("#notify-list ul").append($("#notify-list li:first").clone())
    var timer = setInterval(function(){
        curNotify++
        $("#notify-list ul").animate({"margin-top":curNotify*-25+"px"},1000,function(){
            if(curNotify == notifySize){
                curNotify = 0
                $(this).css({"margin-top":0})
            }
        })
    },5000)
}

// function getBrowerVersion() {
//     var userAgent = navigator.userAgent;
//     var has = userAgent.indexOf("MSIE 8.0") > -1 || userAgent.indexOf("MSIE 9.0") > -1
//     if (has) {//如果是IE8或者IE9追加DIV模拟输入框的placeholder
//         $("[placeEmpty='true']").each(function (i) {
//             var val = $("[placeEmpty='true']").eq(i).attr("placeholder");
//             $("[placeEmpty='true']").eq(i).after("<div class='placeholder'>" + val + "</div>")
//         })
//     }
// }
//点击模拟的DIV使输入框获得聚焦事件
// $(".relative").on("click", ".placeholder", function () {
//     $(this).parent().find("input").focus()
// })
//输入框模拟聚焦和失焦点
// $("[placeEmpty='true']").each(function (i) {
//     $("[placeEmpty='true']").eq(i).focus(function () {
//         if ($(this).val() === "") {
//             $(this).next(".placeholder").hide()
//         }
//     })
//     $("[placeEmpty='true']").eq(i).blur(function () {
//         if ($(this).val() === "") {
//             $(this).next(".placeholder").show()
//         }
//     })
// })
// getBrowerVersion();


//售卡提交方式切换
var tabSellType = $("[sub-type-title]").find("li");
var conSellType = $("[sub-type-content]");

tabSellType.each(function (i) {
    tabSellType.eq(i).click(function () {
        $(this).addClass("cur").siblings().removeClass("cur")
        conSellType.hide().eq(i).show()
    })
})

/*账号密码登录和手机验证码登录切换*/
var checkBtn = $("[data-tab='check'] a");
var checkContent = $("[data-type='login'");

checkBtn.each(function (i) {
    checkBtn.eq(i).click(function () {
        $(this).addClass("cur").siblings().removeClass("cur")
        checkContent.hide().eq(i).show();
    })
})

/*切换登录方式*/
var loginBtn = $("[type='changeLoginType']");
loginBtn.click(function () {
    var loginType = loginBtn.attr("class");
    if (loginType === "code") {
        $("[login-type='pc']").hide();
        $("[login-type='code']").show();
        loginBtn.attr("class", "pc");
    } else {
        $("[login-type='pc']").show();
        $("[login-type='code']").hide();
        loginBtn.attr("class", "code");
    }
})